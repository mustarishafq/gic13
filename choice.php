<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess3.php';

 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
  </head>
  <body class="body">
    <div class="background padding-bottom50">
    <?php  include 'agentHeader.php'; ?>
    <div class="center-content-div">
    	<a href="">
        	<div class="white-n-div left-n-div">
            	<h2 class="project-h2">New Project</h2>
                <div class="short-red-border mid-red-border"></div>
                <img src="img/new-project.png" class="project-img" alt="New Project" title="New Project">
            </div>
        </a>
        <a href="">
        	<div class="white-n-div">
            	<h2 class="project-h2">Subsale</h2>
                <div class="short-red-border mid-red-border"></div>
                <img src="img/subsale.png" class="project-img" alt="Subsale" title="Subsale">
            </div>             
        </a>
        <div class="clear"></div>
    	<div class="big-n-div">
            	<h2 class="project-h2">Announcement</h2>
                <div class="short-red-border mid-red-border"></div>    
    			<div class="width100 announcement-scroll-div">
                	<table class="announcement-table">
                      <thead>
                          <tr>
                              <th class="th">Project</th>
                              <th class="th">Details</th>
                              <th class="th">Date</th>
                              <th class="th">File</th>
    
                              <!-- <th>INVOICE</th> -->
                          </tr>
                      </thead>                
                	  <tbody>
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr>
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr> 
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr>                        
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr>
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr> 
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr> 
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr>
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr> 
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr>                        
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr>
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr> 
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr>                        
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr>
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr> 
                      	<tr>
                            <td>Arte S</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra nisl quis volutpat vestibulum.</td>
                            <td>30/12/2019</td>
                            <td><a href="" class="hover1" download><img src="img/download.png" class="hover1a download-icon"><img src="img/download2.png" class="hover1b download-icon"></a></td>
                      	</tr>                                                                       
                      </tbody>
                	</table>
                </div>
        
        </div>
    </div>
    
    
    
    </div>
    <?php require_once dirname(__FILE__) . '/footer.php'; ?>
    <?php include 'jsAdmin.php'; ?>
  </body>
</html>
