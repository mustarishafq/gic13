<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/Address.php';

require_once dirname(__FILE__) . '/utilities/ringgitMalaysia.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$finalAmountFinal = 0;

$addressDetails = getAddress($conn, "WHERE branch_type = ?",array("branch_type"),array($_SESSION['branch_type']), "s");

if ($addressDetails) {
  $logo = $addressDetails[0]->getLogo();
  $companyName = $addressDetails[0]->getCompanyName();
  $addressNo = $addressDetails[0]->getAddressNo();
  $companyBranch = $addressDetails[0]->getCompanyBranch();
  $companyAddress = $addressDetails[0]->getCompanyAddress();
  $contact = $addressDetails[0]->getContact();
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
if (isset($_POST['idImplode'])) {
  // $id = $_POST['selectedAdvanceslip'];
  $idImplode = $_POST['idImplode'];
  $idExplode = explode(",",$idImplode);
  for ($cnt=0; $cnt <count($idExplode) ; $cnt++) {
  $advancedDetails = getAdvancedSlip($conn, "WHERE id =?",array("id"),array($idExplode[$cnt]), "s");
  $finalAmount = $advancedDetails[0]->getAmount();
  $finalAmountFinal +=$finalAmount;
  $claimType = 'Multi';
  $claimId = md5(uniqid());
  $agentName = $advancedDetails[0]->getAgent();
  $agentNameArray[] = $agentName;
  }
  $agentNameImplode = implode(",",$agentNameArray);
  $agentNameExplode = explode(",",$agentNameImplode);
  for ($cnt=0; $cnt <count($agentNameExplode) ; $cnt++) {
  $cntNotAdd = $cnt;
    $cntADD = $cnt+1;
    if (isset($agentNameExplode[$cntADD])) {
      if ($agentNameExplode[$cntNotAdd] != $agentNameExplode[$cntADD]) {
        $_SESSION['messageType'] = 1;
        header('location: ./adminAdvanced.php?type=7');
      }
    }

  }

  // $loanDetails = getLoanStatus($conn, "WHERE loan_uid = ?", array("loan_uid"), array($_POST['loan_uid']), "s");
}elseif (isset($_POST['headerCheck']) && !isset($_POST['selectedAdvanceslip'])) {
  header('Location: ./adminAdvanced.php?type=3');
  $_SESSION['messageType'] = 1;
}
else {
  $advancedDetails = getAdvancedSlip($conn, "WHERE loan_uid =?",array("loan_uid"),array($_POST['loan_uid']), "s");
  $loanDetails = getLoanStatus($conn, "WHERE loan_uid = ?", array("loan_uid"), array($_POST['loan_uid']), "s");
  $nettPrice  =$loanDetails[0]->getNettPrice();
  // $advancedDetails = getInvoice($conn, "WHERE id =? ORDER BY id DESC LIMIT 1",array("id"),array($_POST['invoice']), "s");
  $loanUid = $_POST['loan_uid'];
  $id = $advancedDetails[0]->getID();
  $claimType = 'Single';
  $claimId = md5(uniqid());
}


$status = 'COMPLETED';

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Edit Advanced/Commission Slip | GIC" />
    <title>Edit Advanced/Commission Slip | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  input,select{
    border-top-style: hidden;
     border-right-style: hidden;
     border-left-style: hidden;
     border-bottom-style: groove;
  }
  input:focus, textarea:focus, select:focus{
      outline: none;
  }
</style>
<body class="body">

<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1"  onclick="goBack()">
    	<a  class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back3.png" class="back-btn2 hover1b" alt="back" title="back">
        	Advanced / Commission Slip
        </a>
    </h1>
    <div class="spacing-left short-red-border"></div>
    <!-- This is a filter for the table result -->


    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->

    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="width100 print-div">
      <div class="text-center">
              <img src="<?php echo "logo/".$logo ?>" class="invoice-logo" alt="GIC Consultancy Sdn. Bhd." title="GIC Consultancy Sdn. Bhd.">
              <p id="companyName" class="invoice-address company-name" value="<?php echo $companyName ?>"><b><?php echo $companyName ?></b></p>
              <p id="companyNameInput" class="invoice-address company-name" style="display: none" ><input id="companyNameInput2" type="text" class="input-text clean" name="" value="<?php echo $companyName ?>"></p>
              <p id="addressNo" class="invoice-small-p"><?php echo $addressNo ?></p>
              <p id="addressNoInput" class="invoice-small-p" style="display: none" ><input id="addressNoInput2" type="text" class="input-text clean" name="" value="<?php echo $addressNo ?>"></p>

              <?php if ($companyBranch) {
              ?><p id="companyBranch" class="invoice-address"><?php echo $companyBranch ?></p>
              <p id="companyBranchInput" class="invoice-address" style="display: none" ><input id="companyBranchInput2" type="text" class="input-text clean" name="" value="<?php echo $companyBranch ?>"></p><?php
              } ?>

              <p id="companyAddress" class="invoice-address"><?php echo $companyAddress ?></p>
              <p id="companyAddressInput" class="invoice-address" style="display: none" ><input id="companyAddressInput2" type="text" class="input-text clean" name="" value="<?php echo $companyAddress ?>"></p>
              <p id="info" class="invoice-address"><?php echo $contact ?></p>
              <p id="infoInput" class="invoice-address" style="display: none" ><input id="infoInput2" type="text" class="input-text" name="" value="<?php echo $contact ?>"></p>
          </div>
		<h1 class="invoice-title">COMMISSION / ALLOWANCE</h1>
    <form class="" action="utilities/editAdvancedSlipFunction.php" method="post">
        <div class="invoice-width50 top-invoice-w50 edit-table-div1">
			<table class="invoice-top-small-table left-table edit-table1">
            	<tr>
                	<td class="first-td">Date</td>
                    <td>:</td>
                    <td><?php if ($advancedDetails[0]->getDateCreated()) {
                      ?><input type="date" class="clean input-style" name="request_date" value="<?php echo date('Y-m-d',strtotime($advancedDetails[0]->getDateCreated())); ?>"> <?php
                    }else {
                      ?><input type="date" class="clean input-style"  name="request_date" value="<?php echo date('Y-m-d'); ?>"> <?php
                    } ?> </td>
                </tr>
                <tr>
                	<td class="first-td">Pay to</td>
                    <td>:</td>
                    <td><?php if ($advancedDetails[0]->getAgent()) {
                        ?><input type="text" class="clean input-style"  name="agent" value="<?php echo $advancedDetails[0]->getAgent() ?>"> <?php

                        } ?></td>
                </tr>
            </table>
        </div>
        <div class="invoice-width50 top-invoice-w50 edit-table-div1">
			<table class="invoice-top-small-table edit-table1">
            	<tr>
                	<td class="first-td">Payslip No</td>
                    <td>:</td>
                    <td><?php echo date('Ymd', strtotime($advancedDetails[0]->getDateCreated())).$advancedDetails[0]->getID() ?></td>
                </tr>
                <tr>
                	<td class="first-td">NRIC</td>
                    <td>:</td>
                    <td><input type="text" class="clean input-style"  name="ic_no" value="<?php echo $advancedDetails[0]->getIcNo() ?>"> </td>
                </tr>
            </table>
        </div>
        <div class="clear"></div>
        <table class="invoice-printing-table">
        	<thead>
                    <tr>
                      <th>Project</th>
                      <th>Details</th>
                      <th>Unit</th>
                      <th>Nett Price (RM)</th>
                      <th>Amount (RM)</th>
                    </tr>
            </thead>

<?php
if (isset($_POST['idImplode'])) {
  for ($cnt=0; $cnt <count($idExplode) ; $cnt++) {
    $advancedMulti = getAdvancedSlip($conn, "WHERE id = ?", array("id"), array($idExplode[$cnt]), "s");
    $loanUid = $advancedMulti[0]->getLoanUid();
    $loanDetails = getLoanStatus($conn, "WHERE loan_uid = ?", array("loan_uid"),array($loanUid), "s");
    $nettPrice = $loanDetails[0]->getNettPrice();

        ?>                      <tr>
                                <td class="td"><?php echo $advancedMulti[0]->getProjectName()  ?></td>
                                <td class="td">1st Part Commission</td>
                                <td class="td"><?php echo $advancedMulti[0]->getUnitNo()  ?></td>
                                <td class="td"><?php echo number_format($nettPrice, 2)  ?></td>
                                <td class="td"><?php echo number_format($advancedMulti[0]->getAmount(), 2)  ?></td>
                            </tr><?php
  }
                                for ($i=count($idExplode); $i < 5 ; $i++) {
                                  ?><tr>
                                    <td class="td"></td>
                                      <td class="td"></td>
                                    <td class="td"></td>
                                    <td class="td"></td>
                                    <td class="td"></td>
                                </tr><?php
                                }

}else {
  ?><tr>
                          <td class="td"><?php echo $advancedDetails[0]->getProjectName()  ?></td>
                          <td class="td">1st Part Commission</td>
                          <td class="td"><?php echo $advancedDetails[0]->getUnitNo()  ?></td>
                          <td class="td"><?php echo number_format($nettPrice, 2)  ?></td>
                          <td class="td"><?php echo number_format($advancedDetails[0]->getAmount(), 2)  ?></td>
                      </tr>
                      <tr>
                        <td class="td"></td>
                          <td class="td"></td>
                        <td class="td"></td>
                        <td class="td"></td>
                        <td class="td"></td>
                    </tr>
                    <tr>
                      <td class="td"></td>
                        <td class="td"></td>
                      <td class="td"></td>
                      <td class="td"></td>
                      <td class="td"></td>
                  </tr>
                  <tr>
                    <td class="td"></td>
                      <td class="td"></td>
                    <td class="td"></td>
                    <td class="td"></td>
                    <td class="td"></td>
                </tr><?php
}

 ?>



              <tr>
                <td class="td"></td>
                  <td class="td"></td>
                  <td class="td"></td>
                <td class="td"><b>Total :</b></td>
                <td class="td"><b><?php
                if (isset($_POST['selectedAdvanceslip']) && isset($_POST['headerCheck'])) {
                   echo number_format($finalAmountFinal, 2); ?></b></td><?php
                }else {
                  echo number_format($advancedDetails[0]->getAmount(), 2)  ?></b></td>
                <?php } ?>
            </tr>
        </table>
		<div class="clear"></div>
        <div class="width100">
			<table class="vtop-data" >
            	<tr>
                	<td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                	<td>Total in Ringggit Malaysia</td>
                    <td><b>:</b></td>
                    <td><?php
                    if (isset($_POST['headerCheck']) && isset($_POST['selectedAdvanceslip'])) {
                      $num = number_format($finalAmountFinal,2);
                      $num = str_replace(",","",$num);
                      $split = explode('.',$num);
                      $whole = convertNumber($split[0].".0");
                      $cents = convertNumber($split[1].".0");
                      if ($split[1] == '.00') {
                      echo mb_strtoupper($whole." Ringgit ".$cents." only");
                      }else {
                      echo mb_strtoupper($whole." Ringgit and ".$cents." cents only");
                      }
                    }else {
                      $num = number_format($advancedDetails[0]->getAmount(),2);
                      $num = str_replace(",","",$num);
                      $split = explode('.',$num);
                      $whole = convertNumber($split[0].".0");
                      $cents = convertNumber($split[1].".0");
                      if ($split[1] == '.00') {
                      echo mb_strtoupper($whole." Ringgit ".$cents." only");
                      }else {
                      echo mb_strtoupper($whole." Ringgit and ".$cents." cents only");
                      }
                    }?></td>
                </tr>
                <tr>
                	<td><select   class="clean input-style"  name="payment_method">
                    <?php if ($advancedDetails[0]->getPaymentMethod() == 'Online Transfer') {
                      ?><option value="<?php echo $advancedDetails[0]->getPaymentMethod() ?>"><?php echo $advancedDetails[0]->getPaymentMethod() ?></option>
                      <option value="Cheque No.">Cheque No.</option> <?php
                    }elseif ($advancedDetails[0]->getPaymentMethod() == 'Cheque No.') {
                      ?><option value="<?php echo $advancedDetails[0]->getPaymentMethod() ?>"><?php echo $advancedDetails[0]->getPaymentMethod() ?></option>
                      <option value="Online Transfer">Online Transfer</option> <?php
                    }else {
                      ?><option value="Online Transfer">Online Transfer</option>
                      <option value="Cheque No.">Cheque No.</option><?php
                    } ?>

                  </select> </td>
                    <td><b>:</b></td>
                    <td><?php if ($advancedDetails[0]->getCheckID()) {
                      ?><input type="text"  class="clean" name="check_id" value="<?php echo $advancedDetails[0]->getCheckID() ?>"><?php
                    }else {
                    ?><input type="text"  class="clean" name="check_id" value=""><?php
                    } ?> </td>
                </tr>

            </table>
        </div>
        <div class="clear"></div>
        <!--<div class="invoice-width50 right-w50">
			<table class="invoice-bottom-small-table">
            	<tr>
                	<td>Sum Amount (excluding Service Tax)</td>
                    <td>:</td>
                    <td></td>
                </tr>
                <tr>
                	<td>Service Tax 6%</td>
                    <td>:</td>
                    <td>--</td>

                </tr>
                <tr>
                	<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                	<td><b>Total Amount</b></td>
                    <td>:</td>
                    <td class="bottom-3rd-td border-td"><b></b></td>
                </tr>
            </table>

        </div>

        <div class="invoice-width50 left-w50">
			<table class="invoice-small-table">
            	<tr>
                	<td><b><u>Payee Details:</u></b></td>
                    <td><b>:</b></td>
                    <td></td>
                </tr>
                <tr>
                	<td>Name</td>
                    <td><b>:</b></td>
                    <td><b>GIC HOLDING Sdn. Bhd.</b></td>
                </tr>
                <tr>
                	<td>Bank</td>
                    <td><b>:</b></td>
                    <td><b>Public Bank</b></td>
                </tr>
                <tr>
                	<td>Account No.</td>
                    <td><b>:</b></td>
                    <td><b>123456789</b></td>
                </tr>
            </table>
        </div>-->
        <div class="invoice-print-spacing"></div>
        <div style="margin-bottom: 0 auto" class="signature-div left-sign">
        	<div class="signature-border"></div>
            <p class="invoice-p">Approved by:</p>
            <p class="invoice-p"><b>Eddie Song</b></p>
        </div>
        <div style="margin-bottom: 0 auto" class="signature-div right-sign">
        	<div class="signature-border"></div>
            <p class="invoice-p">Received by:</p>
            <p class="invoice-p"><b><?php echo $advancedDetails[0]->getAgent(); ?> </b></p>
        </div>
    </div>
	<div class="clear"></div>
  <?php if ($_SESSION['usertype_level'] == 3) {
    ?>
    <div class="width100 overflow text-center margin-top20">
  
            <button class="mid-button red-btn clean ow-margin-auto"  onclick="window.print()">
                Print
            </button>

    </div>
    <div class="clear"></div>
	<?php
  }elseif(isset($_POST['submit']) || isset($_POST['idImplode'])) {
    ?><div class="width100 overflow text-center margin-top20">
        <input type="hidden" name="id" value="<?php echo $_POST['idImplode'] ?>">

          <button class="mid-button red-btn clean ow-margin-auto" name="editBtn" type="submit">
              Save
          </button>
  
      </form>
    </div>
	<div class="clear"></div><?php
  }else {
    ?><div class="width100 overflow text-center margin-top20">
        <input type="hidden" name="id" value="<?php echo $advancedDetails[0]->getID() ?>">

          <button class="mid-button red-btn clean ow-margin-auto" name="editBtn" type="submit">
              Save
          </button>

      </form>
    </div>
	<div class="clear"></div><?php
  }
}else {
    $_SESSION['messageType'] = 1;
    header('location: ./adminAdvanced.php');
  } ?>



</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>
