<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$downlineSelect = $_POST['downlineSelect'];
$conn = connDB();

$userRefererDetails = getReferralHistory($conn, "WHERE referral_id =?",array("referral_id"),array($uid), "s");
if ($userRefererDetails) {
  $userCurrentLevel = $userRefererDetails[0]->getCurrentLevel(); // user current level
}else {
  $userCurrentLevel = 0;
}

$getWho = getWholeDownlineTree($conn, $uid, false);
 ?>
 <table id="myTable" class="shipping-table">
     <thead>
         <tr>
             <th class="th">LEVEL</th>
             <!-- <th class="th">DOWNLINE 2</th>
             <th class="th">DOWNLINE 3</th>
             <th class="th">DOWNLINE 4</th>
             <th class="th">DOWNLINE 5</th> -->
             <th class="th">FULL NAME</th>
             <th class="th">NICKNAME</th>
             <th class="th">POSITION</th>
             <th class="th">DOWNLINE 1</th>
             <th class="th">DOWNLINE 2</th>
             <th class="th">DOWNLINE 3</th>
             <th class="th">DOWNLINE 4</th>
             <th class="th">DOWNLINE 5</th>
         </tr>
     </thead>
     <tbody>
         <?php
           if ($getWho) {
             for ($i=0; $i <count($getWho) ; $i++) {
               $fullName = $getWho[$i]->getReferralName();
               $downlineCurrentLevel = $getWho[$i]->getCurrentLevel();
               $currentLevel = $downlineCurrentLevel - $userCurrentLevel;
               $userDetails = getUser($conn, "WHERE full_name =?",array("full_name"),array($fullName), "s");

               if ($currentLevel == $downlineSelect) {
                 ?>
                 <tr>
                   <td class="td"><?php echo $currentLevel; ?></td>
                   <td class="td"><?php echo $getWho[$i]->getReferralName(); ?></td>
                   <td class="td"><?php echo $userDetails[0]->getUsername(); ?></td>
                   <?php
                   if ($userDetails[0]->getPosition()) {
                     ?><td class="td"><?php echo $userDetails[0]->getPosition(); ?></td><?php
                   }else {
                     ?><td class="td">-</td><?php
                   }
                   $downlineRefererDetails = getReferralHistory($conn, "WHERE referral_id =?",array("referral_id"),array($userDetails[0]->getUid()), "s");
                   if ($downlineRefererDetails) {
                     $userDownlineCurrentLevel = $downlineRefererDetails[0]->getCurrentLevel(); // user current level
                   }else {
                     $userCurrentLevel = 0;
                   }
                   $getWhoII = getWholeDownlineTree($conn, $userDetails[0]->getUid(), false);
                   if ($getWhoII) {
                     ?>
                       <td class="td">
                         <?php
                         for ($j=0; $j <count($getWhoII) ; $j++) {
                           $downlineCurrentLevelII = $getWhoII[$j]->getCurrentLevel();
                           $currentLevelII = $downlineCurrentLevelII - $userDownlineCurrentLevel;
                           if ($currentLevelII == 1) {
                             echo $getWhoII[$j]->getReferralName()."<br>";
                           }
                         }
                          ?>
                       </td>
                       <td class="td">
                         <?php
                         for ($j=0; $j <count($getWhoII) ; $j++) {
                           $downlineCurrentLevelII = $getWhoII[$j]->getCurrentLevel();
                           $currentLevelII = $downlineCurrentLevelII - $userDownlineCurrentLevel;
                           if ($currentLevelII == 2) {
                             echo $getWhoII[$j]->getReferralName()."<br>";
                           }
                         }
                          ?>
                       </td>
                       <td class="td">
                         <?php
                         for ($j=0; $j <count($getWhoII) ; $j++) {
                           $downlineCurrentLevelII = $getWhoII[$j]->getCurrentLevel();
                           $currentLevelII = $downlineCurrentLevelII - $userDownlineCurrentLevel;
                           if ($currentLevelII == 3) {
                             echo $getWhoII[$j]->getReferralName()."<br>";
                           }
                         }
                          ?>
                       </td>
                       <td class="td">
                         <?php
                         for ($j=0; $j <count($getWhoII) ; $j++) {
                           $downlineCurrentLevelII = $getWhoII[$j]->getCurrentLevel();
                           $currentLevelII = $downlineCurrentLevelII - $userDownlineCurrentLevel;
                           if ($currentLevelII == 4) {
                             echo $getWhoII[$j]->getReferralName()."<br>";
                           }
                         }
                          ?>
                       </td>
                       <td class="td">
                         <?php
                         for ($j=0; $j <count($getWhoII) ; $j++) {
                           $downlineCurrentLevelII = $getWhoII[$j]->getCurrentLevel();
                           $currentLevelII = $downlineCurrentLevelII - $userDownlineCurrentLevel;
                           if ($currentLevelII == 5) {
                             echo $getWhoII[$j]->getReferralName()."<br>";
                           }
                         }
                          ?>
                       </td>
                     <?php
                   }else {
                     ?><td></td><td></td><td></td><td></td><td></td><?php
                   }
                    ?>
                 </tr>
                 <?php
               }
             }
           }
          ?>
     </tbody>
 </table>
