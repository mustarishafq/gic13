<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/Address.php';

require_once dirname(__FILE__) . '/utilities/ringgitMalaysia.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$branch = $_SESSION['branch_type'];
$address = getAddress($conn, "WHERE branch_type =?",array("branch_type"),array($branch), "s");
if ($address) {
  $companyName = $address[0]->getCompanyName();
  $addressNo = $address[0]->getAddressNo();
  $companyBranch = $address[0]->getCompanyBranch();
  $companyAddress = $address[0]->getCompanyAddress();
  $contact = $address[0]->getContact();
  $logo = $address[0]->getLogo();
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Edit Slip Address | GIC" />
    <title>Edit Slip Address | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  .input-text{
    font-weight: bold;
    width: 400px;
    text-align: center;
    font-size: 15px;
    border-top: none;
    border-left: none;
    border-right: none;
    border-color: black;
  }
  .input-text:focus{
    outline: none;
  }
  .margin-top15{
    margin-top: 15px;
  }
  .upload-css{
  width: 0.1px;
	height: 0.1px;
	opacity: 0;
	overflow: hidden;
	position: absolute;
	z-index: -1;
  }
  .uploadBtn{
    text-align: center;
    margin-top: 20px;
  }
  #saveAddress{
    background-color: maroon;
    color: white;
    font-size: 15px;
    width: 100px;
    outline-offset: -2px;
    outline-style: solid;
    outline-width: thin;
    outline-color: black;
  }
  .text-center{
    margin-top: 120px;
  }
  a{
    color: maroon;
  }
</style>
<body class="body">

<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<div class="yellow-body same-padding">
  <h1 class="h1-title h1-before-border shipping-h1"><a href="summertonSettings.php">Edit Profile</a> | Edit Slip Address </h1>

<form class="" action="utilities/addressSummertonFunction.php" method="post" enctype="multipart/form-data">
<div class="text-center">
        <input id="upload" class="upload-css" type="file" name="logo">
        <label for="upload"><img src="<?php echo "logo/".$logo; ?>" class="invoice-logo" alt="GIC Consultancy Sdn. Bhd." title="GIC Consultancy Sdn. Bhd."></label>
        <p id="companyNameInput" class="invoice-address company-name" ><input id="companyNameInput2" type="text" class="input-text" name="companyName" value="<?php echo $companyName ?>"></p>
        <p id="addressNoInput" class="invoice-small-p" ><input id="addressNoInput2" type="text" class="input-text" name="addressNo" value="<?php echo $addressNo ?>"></p>

        <?php if ($companyBranch) {
        ?>
        <p id="companyBranchInput" class="invoice-address" ><input id="companyBranchInput2" type="text" class="input-text" name="companyBranch" value="<?php echo $companyBranch ?>"></p><?php
        } ?>

        <p id="companyAddressInput" class="invoice-address" ><input id="companyAddressInput2" type="text" class="input-text" name="companyAddress" value="<?php echo $companyAddress ?>"></p>
        <p id="infoInput" class="invoice-address" ><input id="infoInput2" type="text" class="input-text" name="contact" value="<?php echo $contact ?>"></p>
    </div>

    <div class="uploadBtn">
      <input type="hidden" name="branch" value="<?php echo $branch ?>">
  <button id="saveAddress" type="submit" name="upload">Save</button>
    </div>
</form>
</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Updated Slip Address. ";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Wrong Current Password.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully Updated The Project Details";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>
