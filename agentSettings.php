<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess3.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn," WHERE uid=?",array("uid"),array($uid), "s");

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Profile | GIC" />
    <title>Profile | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
#alert{
  font-size: 13px;
}
  input:focus, textarea:focus, select:focus{
    outline: none;
  }
  #addNewProjectBtn{
    color: black;
  }
  #changePasswordCheck{
    width :15px;
    height :15px;
    vertical-align: middle;
    outline: 2px solid maroon;
    outline-offset: -2px;
  }

  input{
    color: maroon;
    font-weight: bold;
  }
  .dual-input-div .maroon-text,.dual-input-div .label{
    font-size: 16px;
  }
  .dual-input-div .maroon-text{
    color: maroon;
    font-weight: bold;

  }
  .positionStyle{
    color: maroon;
    font-weight: bold;

  }
  .positionStyle:hover{
    color: black;
  }

  button:hover{
    width: 155px;
    height: 47px;
  }

</style>
<body class="body">
<?php  include 'agentHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Profile </h1>
    <div class="short-red-border"></div><br>
    <div class="clear"></div>
<div id="AddNewProject">
    <form method="POST" action="utilities/editProfileFunction.php">
      <p>POSITION : <a class="positionStyle"><?php echo mb_strtoupper($userDetails[0]->getPosition()) ?></a> </p>
      <div class="dual-input-div">
        <p class="label">Full Name </p>
        <p class="maroon-text"><?php echo $userDetails[0]->getFullName() ?></p>
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">Username </p>
        <p class="maroon-text"><?php echo $userDetails[0]->getUsername() ?></p>
      </div>
      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p class="label">IC No. </p>
        <p class="maroon-text"><?php echo $userDetails[0]->getIcNo() ?></p>
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">Contact No. </p>
        <p class="maroon-text"><?php echo $userDetails[0]->getPhoneNo() ?></p>
      </div>
      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p class="label">E-mail </p>
        <p class="maroon-text"><?php echo $userDetails[0]->getEmail() ?></p>
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">Address </p>
        <p class="maroon-text"><?php echo str_replace(",","<br>",$userDetails[0]->getAddress()) ?></p>
      </div>
      <div class="tempo-two-input-clear"></div><br>

        <input type="hidden" name="id" value="<?php echo $userDetails[0]->getId() ?>">
        <input id="changePasswordCheck" style="vertical-align: middle" type="checkbox" name="changePassword" value=""> Change Password ?

        <div style="display: none" id="changePasswordd"><br>

          <label class="labelSize">Current Password : </label>
          <input required class="inputSize input-pattern" type="password" placeholder="Current Password" name="password" id="password">

          <label class="labelSize">New Password : </label>
          <input class="inputSize input-pattern" type="password" placeholder="New Password" name="new_password" id="newPassword"><br>

          <label class="labelSize">Confirm Password : </label>
          <input class="inputSize input-pattern" type="password" placeholder="Confirm Password" name="confirm_password" id="confirmPassword">
          <a id="alert"></a> <br>
          <br><br>

                  <!-- <input type="hidden" name="add_by" id="add_by" value="<?php //echo $userDetails->getUsername(); ?>"> -->

        <button id="noEnterSubmit" class="button" type="submit" name="agentSettings">Change Password</button><br>
        </div>


    </form>
  </div>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login to The System. ";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Wrong Current Password.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully Updated The Project Details";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
<?php echo '<script src="jsPhp/adminAddNewProject.js" type="text/javascript"></script>'; ?>
</html>
<script type="text/javascript">
  $(document).ready(function(){
    $('#changePasswordCheck').change(function(){
      if ($('#changePasswordCheck').is(':checked')) {
        $('#changePasswordd').slideDown(function(){
          $(this).show();
        });
      }
      if ($('#changePasswordCheck').is(':not(:checked)')) {
        $('#changePasswordd').slideUp(function(){
          $(this).hide();
        });
      }
    });

    $("#noEnterSubmit").click(function(){

      var newPassword = $("#newPassword").val();
      var confirmPassword = $("#confirmPassword").val();
      // $("#noEnterSubmit").removeAttr('disabled');

      if (newPassword == confirmPassword) {
        // $("#noEnterSubmit").removeAttr('disabled');
        return true;
      }else {
        $("#alert").text("Password Missmatch");
        // $("#noEnterSubmit").prop('disabled',true);
        return false;
      }
  });
});
</script>
