<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$username = $_SESSION['username'];

$conn = connDB();

$userRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
$userDetails = $userRows[0];
$userUsername = $userDetails->getUsername();
$people = [];

$projectDetails = getProject($conn);
// $projectName = "";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="My Group | GIC" />
    <title>My Group | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<style media="screen">

  #author,#up1,#up2,#up3,#up4,#up5{
    list-style-image: url('img/li.png');
    font-weight: bold;
    font-size: 16px;
  }
  #up6{
    font-weight: bold;
    font-size: 16px;
  }
  li:hover{
    animation-duration: 0.5s;
    animation-name: li;
    color: maroon;
    font-size: 16px;
  }
  @keyframes li {
    0% {font-size: 15px;}
    100% {font-size: 16px;}
  }
</style>
<body class="body">
<?php  include 'agentHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">

  <h1 class="h1-title h1-before-border shipping-h1">Downline</h1>
  <div class="short-red-border"></div>
  <div class="section-divider width100 overflow">
    <?php
    // if ($userDetails->getPosition()) {
    //   $position = $userDetails->getPosition();
    // }else {
    //   $position = "";
    // }
    // if ($userDetails->getUpline1() != 'N/A' && $userDetails->getUpline1() !="" && $userDetails->getUpline1() != 'null' && $userDetails->getUpline1() != 'Null' && $userDetails->getUpline1() != 'NULL') {
    //   $upline1Details = getUser($conn, "WHERE username = ?",array("username"),array($userDetails->getUpline1()), "s");
    //   $upline1Position =  $upline1Details[0]->getPosition();
    //   $upline1Username = $upline1Details[0]->getUsername();
    // }else {
    //   $upline1Position = "-";
    //   $upline1Username = "-";
    // }
    // if ($userDetails->getUpline2() != 'N/A' && $userDetails->getUpline2() !="" && $userDetails->getUpline2() != 'null' && $userDetails->getUpline2() != 'Null' && $userDetails->getUpline2() != 'NULL') {
    //   $upline2Details = getUser($conn, "WHERE username = ?",array("username"),array($userDetails->getUpline2()), "s");
    //   $upline2Position =  $upline2Details[0]->getPosition();
    //   $upline2Username = $upline2Details[0]->getUsername();
    // }else {
    //   $upline2Position = "-";
    //   $upline2Username = "-";
    // }
    // if ($userDetails->getUpline3() != 'N/A' && $userDetails->getUpline3() !="" && $userDetails->getUpline3() != 'null' && $userDetails->getUpline3() != 'Null' && $userDetails->getUpline3() != 'NULL') {
    //   $upline3Details = getUser($conn, "WHERE username = ?",array("username"),array($userDetails->getUpline3()), "s");
    //   $upline3Position =  $upline3Details[0]->getPosition();
    //   $upline3Username = $upline3Details[0]->getUsername();
    // }else {
    //   $upline3Position = "-";
    //   $upline3Username = "-";
    // }
    // if ($userDetails->getUpline4() != 'N/A' && $userDetails->getUpline4() !="" && $userDetails->getUpline4() != 'null' && $userDetails->getUpline4() != 'Null' && $userDetails->getUpline4() != 'NULL') {
    //   $upline4Details = getUser($conn, "WHERE username = ?",array("username"),array($userDetails->getUpline4()), "s");
    //   $upline4Position =  $upline4Details[0]->getPosition();
    //   $upline4Username = $upline4Details[0]->getUsername();
    // }else {
    //   $upline4Position = "-";
    //   $upline4Username = "-";
    // }
    // if ($userDetails->getUpline5() != 'N/A' && $userDetails->getUpline5() !="" && $userDetails->getUpline5() != 'null' && $userDetails->getUpline5() != 'Null' && $userDetails->getUpline5() != 'NULL') {
    //   $upline5Details = getUser($conn, "WHERE username = ?",array("username"),array($userDetails->getUpline5()), "s");
    //   $upline5Position =  $upline5Details[0]->getPosition();
    //   $upline5Username = $upline5Details[0]->getUsername();
    // }else {
    //   $upline5Position = "-";
    //   $upline5Username = "-";
    // }
    ?>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
        <table id="myTable" class="shipping-table">
            <thead>
                <tr>
                    <th class="th">UPLINE 5</th>
                    <th class="th">UPLINE 4</th>
                    <th class="th">UPLINE 3</th>
                    <th class="th">UPLINE 2</th>
                    <th class="th">UPLINE 1</th>
                    <th class="th">NICKNAME</th>
                    <th class="th">FULL NAME</th>
                    <th class="th">POSITION</th>
                </tr>
            </thead>
            <tbody>
                <!-- <tr>
                  <td class="td">UP5</td>
                  <td class="td"><?php //echo $upline5Username; ?></td>
                  <td class="td"><?php //echo $upline5Position; ?></td>
                </tr>
                <tr>
                  <td class="td">UP4</td>
                  <td class="td"><?php //echo $upline4Username; ?></td>
                  <td class="td"><?php //echo $upline4Position; ?></td>
                </tr>
                <tr>
                  <td class="td">UP3</td>
                <td class="td"><?php //echo $upline3Username; ?></td>
                  <td class="td"><?php //echo $upline3Position; ?></td>
                </tr>
                <tr>
                  <td class="td">UP2</td>
                  <td class="td"><?php //echo $upline2Username; ?></td>
                  <td class="td"><?php //echo $upline2Position; ?></td>
                </tr>
                <tr>
                  <td class="td">UP1</td>
                  <td class="td"><?php //echo $upline1Username; ?></td>
                  <td class="td"><?php //echo $upline1Position; ?></td>
                </tr>
                <tr>
                  <td class="td">ME</td>
                  <td class="td"><?php //echo $userUsername; ?></td>
                  <td class="td"><?php //echo $position; ?></td>
                </tr> -->
                <?php
                $downline1Details = getUser($conn, "WHERE upline1 = ?",array("upline1"),array($userUsername), "s");
                if ($downline1Details) {
                  // for ($i=0; $i <count($downline1Details) ; $i++) {
                    if ($downline1Details[0]->getUsername() != "-" && $downline1Details[0]->getUsername() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline1Details[0]->getUsername()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline1Details[0]->getUsername(), $people)){
                          $people[] = $downline1Details[0]->getUsername();
                        ?>
                        <tr>
                          <!-- <td class="td">1</td> -->
                          <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                          <?php if ($downline5Det[0]->getPosition()) {
                            ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                          }else {
                            ?><td class="td">-</td> <?php
                          } ?>

                        </tr>
                          <?php
                        }
                      }
                    }
                  // }
                }
                $downline2Details = getUser($conn, "WHERE upline2 = ?",array("upline2"),array($userUsername), "s");
                if ($downline2Details) {
                  // for ($i=0; $i <count($downline2Details) ; $i++) {
                    if ($downline2Details[0]->getUsername() != "-" && $downline2Details[0]->getUsername() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline2Details[0]->getUsername()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline2Details[0]->getUsername(), $people)){
                          $people[] = $downline2Details[0]->getUsername();
                        ?>
                        <tr>
                          <!-- <td class="td">2</td> -->
                          <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                          <?php if ($downline5Det[0]->getPosition()) {
                            ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                          }else {
                            ?><td class="td">-</td> <?php
                          } ?>
                        </td>
                          <?php
                        }
                      }
                    }
                    if ($downline2Details[0]->getUpline1() != "-" && $downline2Details[0]->getUpline1() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline2Details[0]->getUpline1()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline2Details[0]->getUpline1(), $people)){
                          $people[] = $downline2Details[0]->getUpline1();
                        ?>
                        <tr>
                          <!-- <td class="td">1</td> -->
                          <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                          <?php if ($downline5Det[0]->getPosition()) {
                            ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                          }else {
                            ?><td class="td">-</td> <?php
                          } ?>
                        </td>
                          <?php
                        }
                      }
                    }
                  // }
                }
                $downline3Details = getUser($conn, "WHERE upline3 = ?",array("upline3"),array($userUsername), "s");
                if ($downline3Details) {
                  // for ($i=0; $i <count($downline3Details) ; $i++) {
                    if ($downline3Details[0]->getUpline2() != "-" && $downline3Details[0]->getUpline2() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline3Details[0]->getUpline2()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline3Details[0]->getUpline2(), $people)){
                          $people[] = $downline3Details[0]->getUpline2();
                        ?>
                        <tr>
                          <!-- <td class="td">1</td> -->
                          <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                          <?php if ($downline5Det[0]->getPosition()) {
                            ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                          }else {
                            ?><td class="td">-</td> <?php
                          } ?>
                        </td>
                          <?php
                        }
                      }
                    }
                    if ($downline3Details[0]->getUpline1() != "-" && $downline3Details[0]->getUpline1() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline3Details[0]->getUpline1()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline3Details[0]->getUpline1(), $people)){
                          $people[] = $downline3Details[0]->getUpline1();
                        ?>
                        <tr>
                          <!-- <td class="td">2</td> -->
                          <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                          <?php if ($downline5Det[0]->getPosition()) {
                            ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                          }else {
                            ?><td class="td">-</td> <?php
                          } ?>
                        </td>
                          <?php
                        }
                      }
                    }
                    if ($downline3Details[0]->getUsername() != "-" && $downline3Details[0]->getUsername() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline3Details[0]->getUsername()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline3Details[0]->getUsername(), $people)){
                          $people[] = $downline3Details[0]->getUsername();
                        ?>
                        <tr>
                          <!-- <td class="td">3</td> -->
                          <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                          <?php if ($downline5Det[0]->getPosition()) {
                            ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                          }else {
                            ?><td class="td">-</td> <?php
                          } ?>
                        </td>
                          <?php
                        }
                      }
                    }
                  // }
                }
                $downline4Details = getUser($conn, "WHERE upline4 = ?",array("upline4"),array($userUsername), "s");
                if ($downline4Details) {
                  // for ($i=0; $i <count($downline4Details) ; $i++) {
                    if ($downline4Details[0]->getUpline3() != "-" && $downline4Details[0]->getUpline3() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline4Details[0]->getUpline3()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline4Details[0]->getUpline3(), $people)){
                          $people[] = $downline4Details[0]->getUpline3();
                        ?>
                        <tr>
                          <!-- <td class="td">1</td> -->
                          <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                          <?php if ($downline5Det[0]->getPosition()) {
                            ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                          }else {
                            ?><td class="td">-</td> <?php
                          } ?>
                        </td>
                          <?php
                        }
                      }
                    }
                    if ($downline4Details[0]->getUpline2() != "-" && $downline4Details[0]->getUpline2() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline4Details[0]->getUpline2()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline4Details[0]->getUpline2(), $people)){
                          $people[] = $downline4Details[0]->getUpline2();

                          ?>
                          <tr>
                            <!-- <td class="td">2</td> -->
                            <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                            <?php if ($downline5Det[0]->getPosition()) {
                              ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                            }else {
                              ?><td class="td">-</td> <?php
                            } ?>
                          </td>
                            <?php
                        }
                      }
                    }
                    if ($downline4Details[0]->getUpline1() != "-" && $downline4Details[0]->getUpline1() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline4Details[0]->getUpline1()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline4Details[0]->getUpline1(), $people)){
                          $people[] = $downline4Details[0]->getUpline1();
                        ?>
                        <tr>
                          <!-- <td class="td">3</td> -->
                          <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                          <?php if ($downline5Det[0]->getPosition()) {
                            ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                          }else {
                            ?><td class="td">-</td> <?php
                          } ?>
                        </td>
                          <?php
                        }
                      }
                    }

                    if ($downline4Details[0]->getUsername() != "-" && $downline4Details[0]->getUsername() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline4Details[0]->getUsername()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline4Details[0]->getUsername(), $people)){
                          $people[] = $downline4Details[0]->getUsername();

                          ?>
                          <tr>
                            <!-- <td class="td">4</td> -->
                            <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                            <?php if ($downline5Det[0]->getPosition()) {
                              ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                            }else {
                              ?><td class="td">-</td> <?php
                            } ?>
                          </td>
                            <?php
                        }
                      }
                    }
                  // }
                }
                $downline5Details = getUser($conn, "WHERE upline5 = ?",array("upline5"),array($userUsername), "s");
                if ($downline5Details) {
                  // for ($i=0; $i <count($downline5Details) ; $i++) {
                    if ($downline5Details[0]->getUpline4() != "-" && $downline5Details[0]->getUpline4() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline5Details[0]->getUpline4()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline5Details[0]->getUpline4(), $people)){
                          $people[] = $downline5Details[0]->getUpline4();
                        ?>
                        <tr>
                          <!-- <td class="td">1</td> -->
                          <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                          <?php if ($downline5Det[0]->getPosition()) {
                            ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                          }else {
                            ?><td class="td">-</td> <?php
                          } ?>
                        </td>
                          <?php
                        }
                      }
                    }
                    if ($downline5Details[0]->getUpline3() != "-" && $downline5Details[0]->getUpline3() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline5Details[0]->getUpline3()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline5Details[0]->getUpline3(), $people)){
                          $people[] = $downline5Details[0]->getUpline3();

                          ?>
                          <tr>
                            <!-- <td class="td">2</td> -->
                            <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                            <?php if ($downline5Det[0]->getPosition()) {
                              ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                            }else {
                              ?><td class="td">-</td> <?php
                            } ?>
                          </td>
                            <?php
                        }
                      }
                    }
                    if ($downline5Details[0]->getUpline2() != "-" && $downline5Details[0]->getUpline2() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline5Details[0]->getUpline2()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline5Details[0]->getUpline2(), $people)){
                          $people[] = $downline5Details[0]->getUpline2();
                        ?>
                        <tr>
                          <!-- <td class="td">3</td> -->
                          <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                          <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                          <?php if ($downline5Det[0]->getPosition()) {
                            ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                          }else {
                            ?><td class="td">-</td> <?php
                          } ?>
                        </td>
                          <?php
                        }
                      }
                    }
                    if ($downline5Details[0]->getUpline1() != "-" && $downline5Details[0]->getUpline1() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline5Details[0]->getUpline1()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline5Details[0]->getUpline1(), $people)){
                          $people[] = $downline5Details[0]->getUpline1();

                          ?>
                          <tr>
                            <!-- <td class="td">4</td> -->
                            <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                            <?php if ($downline5Det[0]->getPosition()) {
                              ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                            }else {
                              ?><td class="td">-</td> <?php
                            } ?>
                          </td>
                            <?php
                        }
                      }
                    }
                    if ($downline5Details[0]->getUsername() != "-" && $downline5Details[0]->getUsername() != "N/A") {
                      $downline5Det = getUser($conn, "WHERE username =?",array("username"),array($downline5Details[0]->getUsername()), "s");
                      if ($downline5Det) {
                        if (!in_array($downline5Details[0]->getUsername(), $people)){
                          $people[] = $downline5Details[0]->getUsername();
                          ?>
                          <tr>
                            <!-- <td class="td">5</td> -->
                            <td class="td"><?php echo $downline5Det[0]->getUpline5() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline4() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline3() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline2() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUpline1() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getUsername() ?></td>
                            <td class="td"><?php echo $downline5Det[0]->getFullName() ?></td>
                            <?php if ($downline5Det[0]->getPosition()) {
                              ?><td class="td"><?php echo $downline5Det[0]->getPosition() ?></td><?php
                            }else {
                              ?><td class="td">-</td> <?php
                            } ?>
                          </td>
                            <?php
                        }
                      }
                    }
                  // }
                }
                 ?>
            </tbody>
        </table><br>
    </div>

</body>
</html>
