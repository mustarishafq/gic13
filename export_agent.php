<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$output = '';
if(isset($_POST["export"]))
{
 $query = "SELECT * FROM user";
 $result = mysqli_query($conn, $query);
 if(mysqli_num_rows($result) > 0)
 {
  $output .= '
   <table border = 1 >
                    <tr>
                    <tr>

										<th bgcolor= #C2C2C2>FULL NAME</th>
										<th bgcolor= #C2C2C2>USERNAME</th>
										<th bgcolor= #C2C2C2>IC NO</th>
										<th bgcolor= #C2C2C2>BIRTH MONTH</th>
										<th bgcolor= #C2C2C2>CONTACT NO</th>
										<th bgcolor= #C2C2C2>E-MAIL</th>
										<th bgcolor= #C2C2C2>BANK NAME</th>
										<th bgcolor= #C2C2C2>BANK NO.</th>
										<th bgcolor= #C2C2C2>ADDRESS</th>
										<th bgcolor= #C2C2C2>UP1</th>
										<th bgcolor= #C2C2C2>UP2</th>
                    <th bgcolor= #C2C2C2>UP3</th>
                    <th bgcolor= #C2C2C2>UP4</th>
                    <th bgcolor= #C2C2C2>UP5</th>
										<th bgcolor= #C2C2C2>STATUS</th>
                    </tr>
                    </tr>
  ';
  $cnt = 0;
  while($row = mysqli_fetch_array($result))
  {
   $output .= '
										    <tr>

												<td style="text-align: center;vertical-align: middle">'.$row["full_name"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["username"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["ic"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["birth_month"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["contact"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["email"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["bank"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["bank_no"].'</td>
												<td style="text-align: center;vertical-align: middle">'.str_replace("","",wordwrap($row["address"],20,"<br>")).'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["upline1"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["upline2"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["upline3"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["upline4"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["upline5"].'</td>
                        <td style="text-align: center;vertical-align: middle">'.$row["status"].'</td>

                    </tr>
   ';
  }
	$date = date('d-m-Y');
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=GIC Agent '.$date.'.xls');
  echo $output;
 }
}
?>
