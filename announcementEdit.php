<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$username = $_SESSION['username'];

$conn = connDB();

$announcementDetails = getAnnouncement($conn, "WHERE id =?",array("id"),array($_POST['edit']), "s");

$projectList = getProject($conn, "WHERE display = 'Yes'");

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Announcement | GIC" />
    <title>Announcement | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
#alert{
  font-size: 13px;
}
  input:focus, textarea:focus, select:focus{
    outline: none;
  }
  #addNewProjectBtn{
    color: black;
  }
  #changePasswordCheck{
    width :15px;
    height :15px;
    vertical-align: middle;
    outline: 2px solid maroon;
    outline-offset: -2px;
  }
  .dual-input-div .maroon-text:hover{
    color: maroon;
    font-weight: bold;
    transition: 0.1s;
    /* transition-delay: 0.1s; */
    transform: scale(1.01);
    /* font-size: 17px; */
  }
  input{
    color: maroon;
    font-weight: bold;
  }
  .dual-input-div .maroon-text,.dual-input-div .label{
    font-size: 16px;
  }
</style>
<body class="body">
<?php if ($_SESSION['usertype_level'] %4 == 1) {
  include 'admin1Header.php';
}elseif ($_SESSION['usertype_level'] %4 == 2) {
  include 'admin2Header.php';
} ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Announcement </h1>
    <div class="short-red-border"></div><br>
    <div class="clear"></div>
<div id="AddNewProject">
    <form method="POST" action="utilities/announcementEditFunction.php" enctype="multipart/form-data">
      <div class="dual-input-div">
        <p class="label">Project Name </p>
        <select class="dual-input clean" id="project_name" name="project_name">
          <option value="<?php echo $announcementDetails[0]->getProjectName() ?>"><?php echo $announcementDetails[0]->getProjectName() ?></option>
          <?php if ($projectList) {
            for ($i=0; $i <count($projectList) ; $i++) {
              if ($projectList[$i]->getProjectName() != $announcementDetails[0]->getProjectName()) {
              ?><option value="<?php echo $projectList[$i]->getProjectName() ?>"><?php echo $projectList[$i]->getProjectName() ?></option><?php
              }
            }
          } ?>
        </select>
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">Project Leader </p>
        <input class="dual-input clean" readonly type="text" placeholder="auto fill" id="projectLeader" name="project_leader" value="<?php echo $announcementDetails[0]->getPLName() ?>">
      </div>
      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p class="label">Commission </p>
        <input class="dual-input clean" type="number" step="any" placeholder="Commission" id="commission" name="commission" value="<?php echo $announcementDetails[0]->getCommission() ?>" >
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">Packages </p>
        <input class="dual-input clean" type="text" placeholder="Packages" id="packages" name="packages" value="<?php echo $announcementDetails[0]->getPackage() ?>" >
      </div>
      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p class="label">Presentation File </p>
        <div id="presentUpload" class="form-group">
        <?php
          if ($announcementDetails[0]->getPresentFile()) {
            ?>
                <a href="<?php echo "announcement/".$announcementDetails[0]->getPresentFile() ?>">
                  <button class="view-download-btn" type="button" name="button">View/Download</button>
                </a>
                  <button class="delete-btn" id="deletePresentFile" type="button" name="delete_id" value="<?php echo $announcementDetails[0]->getId() ?>">Delete</button>
            <?php
          }else {
            ?><input type="file" name="present_file" /><?php
          }
         ?>
       </div>
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">E-Brochure </p>
        <?php
          if ($announcementDetails[0]->getEBroshure()) {
            ?>
            <div id="broshureUpload" class="form-group">
                <a href="<?php echo "announcement/".$announcementDetails[0]->getEBroshure() ?>">
                  <button class="view-download-btn" type="button" name="button">View/Download</button>
                </a>
                  <button class="delete-btn" id="deleteEBroshure" type="button" name="delete_id" value="<?php echo $announcementDetails[0]->getId() ?>">Delete</button>
            </div>
            <?php
          }else {
            ?><input type="file" name="broshure" /><?php
          }
         ?>
      </div>
      <div class="tempo-two-input-clear"></div><br>
      <div class="dual-input-div">
        <p class="label">Booking Form </p>
        <?php
          if ($announcementDetails[0]->getBookingForm()) {
            ?>
            <div id="bookingFormUpload" class="form-group">
                <a href="<?php echo "announcement/".$announcementDetails[0]->getBookingForm() ?>">
                  <button class="view-download-btn" type="button" name="button">View/Download</button>
                </a>
                  <button class="delete-btn" id="deleteBookingForm" type="button" name="delete_id" value="<?php echo $announcementDetails[0]->getId() ?>">Delete</button>
            </div>
            <?php
          }else {
            ?><input type="file" name="booking_form" /><?php
          }
         ?>
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">Video Link </p>
        <input class="dual-input clean" type="text" placeholder="Video Link" id="video_link" name="video_link" value="<?php echo $announcementDetails[0]->getVideoLink() ?>" >
      </div>
      <div class="tempo-two-input-clear"></div><br>
      <a class="support">* Support Uploaded File pdf ,doc, docx, png, jpg, jpeg.</a><br>
      <input type="hidden" name="id" value="<?php echo $announcementDetails[0]->getId() ?>">
        <button id="noEnterSubmit" class="button" type="submit" name="announcement">Submit</button>
    </form>
  </div>
</div>
<script>
$("#deletePresentFile").click(function(){
  var deleteId = $(this).val();
  // var deleteItem = 'present';
  $("#presentUpload").empty();
  $("#presentUpload").html('<input type="file" name="present_file" />');
  $("input[name='present_file']").val(null);
  // $.ajax({
  //   url: 'utilities/announcementEditFunction.php',
  //   data: {deleteId:deleteId,deleteItem:deleteItem},
  //   type: 'post',
  //   success:function(data){
  //     $("#presentUpload").empty();
  //     $("#presentUpload").html('<input type="file" name="present_file" />');
  //   }
  // });
});
$("#deleteBookingForm").click(function(){
  var deleteId = $(this).val();
  // var deleteItem = 'booking';
  $("#bookingFormUpload").empty();
  $("#bookingFormUpload").html('<input type="file" name="booking_form" />');
  $("input[name='booking_form']").val(null);
  // $.ajax({
  //   url: 'utilities/announcementEditFunction.php',
  //   data: {deleteId:deleteId,deleteItem:deleteItem},
  //   type: 'post',
  //   success:function(data){
  //     $("#bookingFormUpload").empty();
  //     $("#bookingFormUpload").html('<input type="file" name="booking_form" />');
  //   }
  // });
});
$("#deleteEBroshure").click(function(){
  var deleteId = $(this).val();
  // var deleteItem = 'broshure';
  $("#broshureUpload").empty();
  $("#broshureUpload").html('<input type="file" name="broshure" />');
  $("input[name='broshure']").val(null);
  // $.ajax({
  //   url: 'utilities/announcementEditFunction.php',
  //   data: {deleteId:deleteId,deleteItem:deleteItem},
  //   type: 'post',
  //   success:function(data){
  //     $("#broshureUpload").empty();
  //     $("#broshureUpload").html('<input type="file" name="broshure" />');
  //   }
  // });
});
</script>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
</body>
</html>
