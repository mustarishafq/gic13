<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$fullName = $_SESSION['fullname'];

$conn = connDB();

$userRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($fullName),"s");
$userDetails = $userRows[0];

$projectDetails = getProject($conn);
// $projectName = "";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="My Performance | GIC" />
    <title>My Performance | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
    <?php include 'css.php'; ?>
</head>
<style media="screen">
th:hover{
  background-color: maroon;
}
th.headerSortUp{
  background-color: black;
}
th.headerSortDown{
  background-color: black;
}
.exportBtn{
  float: right;
  background-color: #991414;
  color: white;
  border-radius: 5px;
  font-weight: bold;
  border-color: transparent;
}
.exportBtn:hover{
  background-color: maroon;
}
</style>
<body class="body">
<?php  include 'agentHeader.php'; ?>
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div class="yellow-body same-padding">
  <div class="modal">

  </div>

  <h1 class="h1-title h1-before-border shipping-h1">My Performance</h1>
  <div class="short-red-border"></div>
<!-- <h3 class="h1-title m-margin-top"> Personal Sales | <a href="personalOverriding.php" class="h1-title">Personal Overriding</a></h3> -->
<h3 class="h1-title m-margin-top"> Personal Sales | <a href="myTeamPerformance.php" class="red-color-swicthing">Downline Performance</a> </h3><br>
<p style="color: maroon;font-size: 18px" class="h1-title m-margin-top"> Position : <?php echo $userDetails->getPosition() ?></a>
  <div class="section-divider width100 overflow">

    <select id="projectChoose" name="admin2Product" class="clean-select clean pointer">
      <option value="">Select a project</option>
      <?php if ($projectDetails) {
        for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
          $projectNameArr[] = $projectDetails[$cnt]->getProjectName();
          if ($projectDetails[$cnt]->getProjectName()) {
            ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
            }
            }
            ?><option value="">SHOW ALL</option><?php
          } ?>
    </select>
      <input class="clean search-btn" type="text" name="" value="" placeholder="Search.." id="searchInput" autocomplete="off">
      <input class="clean search-btn gap" type="text" name="" value="" placeholder="End Date.." id="searchEndDate" autocomplete="off">
      <input class="clean search-btn gap" type="text" name="" value="" placeholder="Start Date.." id="searchStartDate" autocomplete="off">
  </div>
  <a href="excel/agentExportExcel.php"><button class="exportBtn red-bg-swicthing" type="button" name="button">Export</button></a>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
        <table id="myTable" class="shipping-table pointer-th">
            <thead>
                <tr>
                    <th class="th">NO. <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("PROJECT NAME",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("PURCHASER NAME",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <!-- <th>STOCK</th> -->
                    <th class="th">IC <img src="img/sort.png" class="sort"></th>
                    <th class="th">CONTACT <img src="img/sort.png" class="sort"></th>
                    <th class="th">E-MAIL <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("BOOKING DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th">SQ FT <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("SPA PRICE",8,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th">PACKAGE <img src="img/sort.png" class="sort"></th>
                    <th class="th">DISCOUNT <img src="img/sort.png" class="sort"></th>
                    <th class="th">REBATE <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("EXTRA REBATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("NETT PRICE",8,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("TOTAL DEVELOPER COMMISSION",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th">AGENT <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("LOAN STATUS",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>

                    <th class="th">REMARK <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("FORM COLLECTED",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("PAYMENT METHOD",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th">LAWYER <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("PENDING APPROVAL STATUS",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("BANK APPROVED",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("LO SIGNED DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("LA SIGNED DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("SPA SIGNED DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("FULLSET COMPLETED",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th">CASH BUYER <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("CANCELLED BOOKING",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("CASE STATUS",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("EVENT PERSONAL",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th">RATE <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("AGENT COMMISSION",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("BOOKING FORM",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                </tr>
            </thead>
            <tbody id="myFilter">
                <?php
                // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                // {
                    $orderDetails = getLoanStatus($conn, "WHERE display = 'Yes' and cancelled_booking != 'YES' AND agent = ?",array("agent"),array($fullName), "s");
                    if($orderDetails != null)
                    {
                        for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                        {?>
                        <tr>
                            <td class="td"><?php echo ($cntAA+1)?></td>
                            <!-- <td class="td"><?php echo $orderDetails[$cntAA]->getId();?></td> -->
                            <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getUnitNo());?></td>
                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getPurchaserName());?></td>
                            <!-- <td><?php //echo $orderDetails[$cntAA]->getStock();?></td> -->
                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getIc());?></td>
                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getContact());?></td>

                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getEmail());?></td>
                            <td class="td"><?php echo date('d-m-Y', strtotime($orderDetails[$cntAA]->getBookingDate()));?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getSqFt();?></td>

                            <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getSpaPrice();?></td> -->

                            <!-- show , inside value -->
                            <?php $spaPrice = $orderDetails[$cntAA]->getSpaPrice();?>
                            <td class="td"><?php echo $spaPriceValue = number_format($spaPrice, 2); ?></td>

                            <td class="td"><?php echo $orderDetails[$cntAA]->getPackage();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getDiscount();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getRebate();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getExtraRebate();?></td>

                            <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getNettPrice();?></td>
                            <td class="td"><?php //echo $orderDetails[$cntAA]->getTotalDeveloperComm();?></td> -->

                            <!-- show , inside value -->
                            <?php $nettPrice = $orderDetails[$cntAA]->getNettPrice();?>
                            <td class="td"><?php echo $nettPriceValue = number_format($nettPrice, 2); ?></td>
                            <?php $totalDevComm = $orderDetails[$cntAA]->getTotalDeveloperComm();?>
                            <td class="td"><?php echo $totalDevCommValue = number_format($totalDevComm, 2); ?></td>

                            <td class="td"><?php echo $orderDetails[$cntAA]->getAgent();?></td>
                            <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getRemark();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getBFormCollected();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getPaymentMethod();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getLawyer();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getPendingApprovalStatus();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getBankApproved();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getLoSignedDate();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getLaSignedDate();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getSpaSignedDate();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getFullsetCompleted();?></td>

                            <td class="td"><?php echo $orderDetails[$cntAA]->getCashBuyer();?></td>

                            <td class="td"><?php echo $orderDetails[$cntAA]->getCancelledBooking();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getCaseStatus();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getEventPersonal();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getRate();?></td>

                            <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getAgentComm();?></td> -->

                            <?php $agentComm = explode(",",$orderDetails[$cntAA]->getAgentComm());?>
                            <td class="td"><?php echo $agentCommValue = number_format($agentComm[0], 2); ?></td>
                            <td class="td">
                              <?php if ($orderDetails[$cntAA]->getPdf()) {
                                ?>
                                <a href="<?php echo "uploads/".$orderDetails[$cntAA]->getPdf(); ?>">Download
                                </a>
                                <?php
                              } ?>
                            </td>
                        </tr>
                        <?php
                        }
                    }else {
                      ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="35">No Personal Sales Found.</td> <?php
                    }
                //}
                ?>
            </tbody>
        </table><br>
    </div>

    <?php //$conn->close();?>

</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<!-- ***********************************************-->
<script>
$("#searchStartDate,#searchEndDate").datepicker({
  dateFormat: 'dd/mm/yy',
});

$("#searchStartDate,#searchEndDate,#projectChoose").on("change",function(){
  var bookingStartDate = $("#searchStartDate").val();
  var bookingEndDate = $("#searchEndDate").val();
    var project = $("#projectChoose").val();
  $.ajax({
    url: 'myPerformanceFilter.php',
    data: {bookingStartDate:bookingStartDate,bookingEndDate:bookingEndDate,project:project},
    type: 'post',
    success:function(data){
      $(document).ready(function(){
        $(document).ajaxStart(function(){
          $(".modal").show();
        });
        $(document).ajaxStop(function(){
          setTimeout(function(){
            $(".modal").hide();
              $("#myTable").empty();
              $("#myTable").html(data);
          },500);
        });
      });
    }
  });
});
</script>
<!-- ***********************************************-->
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
          if ($announcementDetails)
          {
          for ($cnt=0; $cnt <count($announcementDetails) ; $cnt++)
          {
            $dateCreated = date('Y-m-d H:i:s',strtotime($announcementDetails[$cnt]->getDateCreated()."+ 1 days"));
            $now = date('Y-m-d H:i:s');
            if ($dateCreated >= $now ) {
              $messageType = $announcementDetails[$cnt]->getDetails()."<br>";
              echo '
              <script>
                  putNoticeJavascript("Announcement !! ","'.$messageType.'");
              </script>';
              $_SESSION['messageType'] = 0;
            }

          }
          ?>

          <?php
          }
        }
        // echo '
        // <script>
        //     putNoticeJavascript("Announcement !! ","'.$messageType.'");
        // </script>
        // ';
        $_SESSION['messageType'] = 0;
      }}
 ?>
</body>
<script type="text/javascript">
function showFrontLayer() {
  document.getElementById('bg_mask').style.visibility='visible';
  document.getElementById('frontlayer').style.visibility='visible';
}
function hideFrontLayer() {
  document.getElementById('bg_mask').style.visibility='hidden';
  document.getElementById('frontlayer').style.visibility='hidden';
}
</script>
<script type="text/javascript">
  $(document).ready( function(){
    $("#getAnnouncementTable").click( function(){
      $("#announcementTable").slideToggle();
    });
  });
</script>
<script>
  $(function(){
    $("#myTable").tablesorter( {dateFormat: 'pt'} );
  });
</script>
</html>
