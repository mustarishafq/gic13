<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();
$totalClaims = 0;$totalDevComm = 0;$totalGicProfit = 0;

$UnitSoldDetails = getLoanStatus($conn, "WHERE case_status = 'COMPLETED' AND cancelled_booking != 'YES' ");
if ($UnitSoldDetails) {
  $totalUnitSold = count($UnitSoldDetails);
}else {
  $totalUnitSold = 0;
}
$totalUnitDetails = getLoanStatus($conn, "WHERE cancelled_booking != 'YES' ");
if ($totalUnitDetails) {
  $totalUnit = count($totalUnitDetails);
}else {
  $totalUnit = 0;
}
$agentDetails = getUser($conn, "WHERE user_type = 3 AND status = 'Active'");
if ($agentDetails) {
  $totalAgent = count($agentDetails);
}else {
  $totalAgent = 0;
}
$claimsDetails = getLoanStatus($conn, "WHERE cancelled_booking != 'YES' ");
if ($claimsDetails) {
  for ($i=0; $i <count($claimsDetails) ; $i++) {
    if ($claimsDetails[$i]->getTotalDeveloperComm()) {
      $totalDevComm += $claimsDetails[$i]->getTotalDeveloperComm();
    }
    if ($claimsDetails[$i]->getGicProfit()) {
      $totalGicProfit += $claimsDetails[$i]->getGicProfit();
    }
    if ($claimsDetails[$i]->getClaimAmt1st() && $claimsDetails[$i]->getCheckNo1()) {
      $totalClaims += $claimsDetails[$i]->getClaimAmt1st();
    }
    if ($claimsDetails[$i]->getClaimAmt2nd() && $claimsDetails[$i]->getCheckNo2()) {
      $totalClaims += $claimsDetails[$i]->getClaimAmt1st();
    }
    if ($claimsDetails[$i]->getClaimAmt3rd() && $claimsDetails[$i]->getCheckNo3()) {
      $totalClaims += $claimsDetails[$i]->getClaimAmt1st();
    }
    if ($claimsDetails[$i]->getClaimAmt4th() && $claimsDetails[$i]->getCheckNo4()) {
      $totalClaims += $claimsDetails[$i]->getClaimAmt1st();
    }
    if ($claimsDetails[$i]->getClaimAmt5th() && $claimsDetails[$i]->getCheckNo5()) {
      $totalClaims += $claimsDetails[$i]->getClaimAmt1st();
    }
  }
}
 ?>
 <style media="screen">
   .circle-box{
     position: relative;
     display: inline-block;
     border: 2px solid black;
     width: 200px;
     height: 200px;
     border-radius: 50%;
     text-align: center;
     padding: 15px;
     margin-right: 20px;
     margin-bottom: 20px;
     font-size: 40px;
   }
   .big-text{
     font-size: 100%;
     margin-top: 48px;
   }
   .small-text{
     font-size: 40%;
     margin-top: -20px;
   }
   .center{
     text-align: center;
   }
   .left{
     text-align: left;
   }
 </style>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
  <?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
  <body class="body">
    <?php
    if ($_SESSION['usertype_level'] == 1) {
      include 'admin1Header.php';
    }else {
      include 'admin2Header.php';
    }
     ?>
    <div class="yellow-body padding-from-menu same-padding center">
      <h1 class="h1-title h1-before-border shipping-h1 left">Dashboard</h1>
      <div class="short-red-border"></div><br>
      <div class="circle-box">
          <p class="big-text"><b><?php echo $totalAgent ?></b></p>
          <p class="small-text">Agents</p>
      </div>

      <div class="circle-box">
          <p class="big-text"><b><?php echo $totalUnitSold ?></b></p>
          <p class="small-text">/<?php echo $totalUnit ?></p>
      </div>

      <div class="circle-box">
          <p class="big-text"><b><?php echo number_format($totalClaims); ?></b></p>
          <p class="small-text">/<?php echo number_format($totalDevComm) ?></p>
      </div>

      <div class="circle-box">
          <p class="big-text"><b><?php echo number_format($totalGicProfit) ?></b></p>
          <p class="small-text">/<?php echo number_format($totalDevComm) ?></p>
      </div>

    </div>
  </body>
  <?php include 'js.php'; ?>
</html>
