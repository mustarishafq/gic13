<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$branchType = $_SESSION['branch_type'];
$projectID = $_POST['idProject'];

// $projectDetails = getProject($conn, "WHERE id =? AND branch_type =?", array("id,branch_type"), array($projectID,$branchType), "ss");
$projectDetails = getProject($conn, "WHERE id =?",array("id"),array($projectID),"s");

$projectName = $projectDetails[0]->getProjectName();
$personInCharge = $projectDetails[0]->getAddProjectPpl();


$project[] = array("project_name" => $projectName,"pic" => $personInCharge);

echo json_encode($project);

 ?>
