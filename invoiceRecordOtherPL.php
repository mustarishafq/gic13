<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/OtherInvoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$finalAmountFinal = 0;
$af = 0;
$aff = 0;
// $loanDetails = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$invoiceDetails = getInvoice($conn);
$projectDetails = getProject($conn);
// $projectName = "WHERE case_status = 'COMPLETED'";
$projectName = "WHERE case_status = 'COMPLETED'";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Invoice Record | GIC" />
    <title>Invoice Record-Other | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style>
input#checkboxToDisplayTableMulti{
    width :20px;
    height :20px;
    vertical-align: middle;
    outline: 2px solid maroon;
    outline-offset: -2px;
}
      body {

      }
      input {

      font-weight: bold;
      /* color: white; */
      text-align: center;
      border-radius: 15px;
      }
      .no-outline:focus {
      outline: none;

      }
      button{
        border-radius: 15px;
        color: white;

      }
      .button-add{
        background-color: green;
      }
      .button-remove{
        background-color: maroon;
      }
      th:hover{
        background-color: maroon;
      }
      th.headerSortUp{
        background-color: black;
      }
      th.headerSortDown{
        background-color: black;
      }
    </style>
    <script type="text/javascript">
      $(document).ready( function(){
        $("#checkEmpty").click( function(){
          $("#overallTable").toggle();
          $("#emptyTable").toggle();
        });
      });
    </script>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'agentHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Invoice Record-Other</h1>
    <div class="short-red-border"></div>
    <h3 class="h1-title"><a href="invoiceRecordProformaPL.php" class="h1-title red-color-swicthing">Proforma Invoice</a> | <a href="invoiceRecordPL.php" class="h1-title red-color-swicthing">Single Invoice</a> | <a href="invoiceRecordMultiPL.php" class="h1-title red-color-swicthing">Multi Invoice</a> | Other Invoice | <a href="invoiceRecordCredit.php" class="h1-title red-color-swicthing">Credit Note Invoice</a></h3>
    <!-- <input type="checkbox" id="checkEmpty" value=""> -->


        <!-- <td><p class="h1-title">Fill Check Number Only</p></td>
        <td><input type="checkBox" id="checkboxToDisplayTableMultiFill"></td> -->

    <!-- This is a filter for the table result -->
  <div class="section-divider width100 overflow">

    <?php $projectDetails = getProject($conn, "WHERE display = 'Yes' AND project_leader =?",array("project_leader"),array($_SESSION['username']), "s"); ?>

      <select id="sel_id" class="clean-select clean pointer">
        <option value="">Select a project</option>
        <?php if ($projectDetails) {
          for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
            $projectNameArr[] = $projectDetails[$cnt]->getProjectName();
            if ($projectDetails[$cnt]->getProjectName() != $types) {
              ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
              }
              }
              ?><option value="ALL">SHOW ALL</option><?php
            } ?>
      <!-- <option value="-1">Select</option>
      <option value="VIDA">kasper </option>
      <option value="VV">adad </option> -->
      <!-- <option value="14">3204 </option> -->
      </select>
      <input class="clean search-btn" type="text" name="" value="" placeholder="Search.." id="searchInput">
  </div>

  <div class="clear"></div>
  <a href="excel/invoiceRecordOtherExport.php"><button class="exportBtn red-bg-swicthing" type="button" name="button">Export</button></a>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table id="overallTable" class="shipping-table pointer-th">
                <thead>
                    <tr>
                        <th class="th">NO. <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("PROJECT NAME",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th">DATE <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("INVOICE NO.",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th">AMOUNT (RM) <img src="img/sort.png" class="sort"></th>
                        <th class="th">TOTAL AMOUNT (RM) <img src="img/sort.png" class="sort"></th>
                        <th class="th">SST <img src="img/sort.png" class="sort"></th>
                        <th class="th">1ST STATUS <img src="img/sort.png" class="sort"></th>
                        <th class="th">RECEIVED DATE <img src="img/sort.png" class="sort"></th>
                        <th class="th">CHECK NO. <img src="img/sort.png" class="sort"></th>
                        <!-- <th class="th">DATE MODIFIED <img src="img/sort.png" class="sort"></th> -->
                        <!-- <th class="th">ACTION <img src="img/sort.png" class="sort"></th> -->
                        <!-- <th class="th">ARCHIVED <img src="img/sort.png" class="sort"></th> -->

                        <!-- <th>STOCK</th> -->


                        <!-- <th class="th">BANK APPROVED</th>
                        <th class="th">LO SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th> -->
                        <!-- <th>INVOICE</th> -->
                    </tr>
                </thead>
                <tbody id="myFilter">
                    <?php
                    $projectNameArrImp = implode(",",$projectNameArr);
                    $projectNameArrExp = explode(",",$projectNameArrImp);
                    for ($i=0; $i <count($projectNameArrExp) ; $i++) {
                    $otherInvoiceDetails = getOtherInvoice($conn, "WHERE branch_type = ? and display = 'Yes' AND project_name =?",
                    array("branch_type","project_name"),array($_SESSION['branch_type'],$projectNameArrExp[$i]), "ss");

                    if ($otherInvoiceDetails) {
                      for ($cnt=0; $cnt <count($otherInvoiceDetails); $cnt++) {
                        // $claimStatus = $otherInvoiceDetails[$cnt]->getClaimsStatus();
                        // $claimStatusExplode = explode(",",$claimStatus);
                        ?>
                        <tr>
                        <td class="td"><?php echo $cnt+1; ?>
                        <td class="td"><?php echo $otherInvoiceDetails[$cnt]->getProjectName(); ?></td>
                        <td class="td"><?php echo str_replace(",","<br>",$otherInvoiceDetails[$cnt]->getUnitNo()); ?></td>
                        <td class="td"><?php echo date('d-m-Y', strtotime($otherInvoiceDetails[$cnt]->getBookingDate())); ?></td>
                        <td class="td"><?php echo date('ymd', strtotime($otherInvoiceDetails[$cnt]->getDateCreated())).$otherInvoiceDetails[$cnt]->getID(); ?></td>
                        <td class="td"><?php $getAmountExp = explode(",",$otherInvoiceDetails[$cnt]->getFinalAmount());
                        for ($cntAA=0; $cntAA <count($getAmountExp) ; $cntAA++) {
                          $finalAm = $getAmountExp[$cntAA];
                          echo number_format($finalAm,2)."<br>";
                          }
                        ?>
                        </td>
                        <td class="td"><?php $getAmountExplode = explode(",",$otherInvoiceDetails[$cnt]->getFinalAmount());
                        for ($cntAA=0; $cntAA <count($getAmountExplode) ; $cntAA++) {
                          $finalAmount = $getAmountExplode[$cntAA];
                          $finalAmountFinal += $finalAmount;
                        }
                        echo number_format($finalAmountFinal,2);
                        $finalAmountFinal = 0;  ?></td>
                        <td class="td"><?php echo $otherInvoiceDetails[$cnt]->getCharges(); ?></td>
                        <td class="td"><?php echo $otherInvoiceDetails[$cnt]->getReceiveStatus(); ?></td>
                        <td class="td"><?php
                        if ($otherInvoiceDetails[$cnt]->getReceiveDate()) {
                          ?><td class="td"><?php echo $otherInvoiceDetails[$cnt]->getReceiveDate(); ?></td><?php
                        }
                        ?>
                      </td>
                      <td class="td"><?php echo $otherInvoiceDetails[$cnt]->getCheckID(); ?></td>
                      </tr> <?php
                      }
                    }else {
                      ?>
                      <td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="14" >No Invoice Record Yet.</td>
                      <?php
                    }
                  }


                    ?>
                </tbody>
            </table>

    </div>
</div>



<?php unset($_SESSION['idPro']);
      unset($_SESSION['invoice_id']);
      unset($_SESSION['idComm']);
      unset($_SESSION['loan_uid']);
      unset($_SESSION['commission_id']);
      unset($_SESSION['idImp']);?>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Issue Other Invoice.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Updated Invoice Details.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $("#overallTable").tablesorter( {dateFormat: 'pt'} );
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("button[name='loan_uid_archive']").click(function(){
      var invoiceID = $(this).val();

      $.ajax({
        url: "utilities/archiveInvoiceOther.php",
        type: "post",
        data: {invoiceID:invoiceID},
        dataType: "json",
        success:function(response){

          var unit = response[0]['unit_no'];
          var test = " Has Been Add To Archive";
          // $("#notyElDiv").fadeIn(function(){
            // $("#notyEl").html(unit+test);
                putNoticeJavascriptReload("Notice !!",""+unit+" Has Been Add To Archive.");
          // });
          // alert(unit);
          // location.reload();
        }
      });
    });
  });
</script>
</body>
</html>
