<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn," WHERE uid=?",array("uid"),array($uid), "s");

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Edit Profile | GIC" />
    <title>Edit Profile | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
#alert{
  font-size: 13px;
}
  a{
    color: red;
  }
  input:focus, textarea:focus, select:focus{
    outline: none;
  }
  #addNewProjectBtn{
    color: black;
  }
  #changePasswordCheck{
    width :15px;
    height :15px;
    vertical-align: middle;
    outline: 2px solid #0075ff;
    outline-offset: -2px;
  }
  input{
    color: maroon;
    font-weight: bold;
  }
  .dual-input-div .maroon-text,.dual-input-div .label{
    font-size: 16px;
  }
  .dual-input-div .maroon-text{
    color: maroon;
    font-weight: bold;
  }
  a{
    color: maroon;
  }
</style>
<body class="body">
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Edit Profile | <a href="addCompanyBranch.php" class="red-color-swicthing">Add Company Branch</a> | <a href="editSlipAddress.php" class="red-color-swicthing">Edit Slip Address</a> </h1>
    <div class="short-red-border"></div><br>
    <div class="clear"></div>
<div id="AddNewProject">
    <form method="POST" action="utilities/editProfileFunction.php">
      <div class="dual-input-div">
        <p class="label">Username </p>
        <input class="dual-input clean" type="text" placeholder="Username" id="username" name="username" value="<?php echo $userDetails[0]->getUsername() ?>" >
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">Full Name </p>
        <input class="dual-input clean" type="text" placeholder="Full Name" id="full_name" name="full_name" value="<?php echo $userDetails[0]->getFullName() ?>" >
      </div>
      <div class="tempo-two-input-clear"></div>

      <div class="dual-input-div">
        <p class="label">I/C No. </p>
        <input class="dual-input clean" type="text" placeholder="I/C No." id="ic_no" name="ic_no" value="<?php echo $userDetails[0]->getIcNo() ?>" >
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">Contact No. </p>
        <input class="dual-input clean" type="text" placeholder="Contact No." id="contact" name="contact" value="<?php echo $userDetails[0]->getPhoneNo() ?>" >
      </div>
      <div class="tempo-two-input-clear"></div>

      <div class="dual-input-div">
        <p class="label">E-mail </p>
        <input class="dual-input clean" type="text" placeholder="E-mail" id="email" name="email" value="<?php echo $userDetails[0]->getEmail() ?>" >
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">Password </p>
        <input class="dual-input clean" type="password" placeholder="Password" id="password" name="password">
      </div>
      <div class="tempo-two-input-clear"></div><br><br>

        <input type="hidden" name="id" value="<?php echo $userDetails[0]->getId() ?>">
        <input id="changePasswordCheck" style="vertical-align: middle" type="checkbox" name="changePassword" value="" class="pointer"> Change Password ?

        <div style="display: none" id="changePasswordd"><br>
		<div class="dual-input-div">
          <p class="label">New Password</p>
          <input class="dual-input clean" type="password" placeholder="New Password" name="new_password" id="newPassword">
		</div>
        <div class="dual-input-div second-dual-input">
          <p class="label">Confirm Password</p>
          <input class="dual-input clean" type="password" placeholder="Confirm Password" name="confirm_password" id="confirmPassword">
          <a id="alert"></a>
        </div>
        </div>
		<div class="clear"></div>
        <!-- <input type="hidden" name="add_by" id="add_by" value="<?php //echo $userDetails->getUsername(); ?>"> -->

        <button id="noEnterSubmit" class="button clean red-bg-swicthing margin-top30" type="submit" name="loginButton">Submit</button><br>
    </form>
  </div>
</div>
</div>
<div class="clear"></div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login to The System. ";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Wrong Current Password.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully Updated The Project Details";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
<?php echo '<script src="jsPhp/adminAddNewProject.js" type="text/javascript"></script>'; ?>
</html>
<script type="text/javascript">
  $(document).ready(function(){
    $('#changePasswordCheck').change(function(){
      if ($('#changePasswordCheck').is(':checked')) {
        $('#changePasswordd').slideDown(function(){
          $(this).show();
        });
      }
      if ($('#changePasswordCheck').is(':not(:checked)')) {
        $('#changePasswordd').slideUp(function(){
          $(this).hide();
        });
      }
    });

    $("#noEnterSubmit").click(function(){

      var newPassword = $("#newPassword").val();
      var confirmPassword = $("#confirmPassword").val();
      // $("#noEnterSubmit").removeAttr('disabled');

      if (newPassword == confirmPassword) {
        // $("#noEnterSubmit").removeAttr('disabled');
        return true;
      }else {
        $("#alert").text("Password Missmatch");
        // $("#noEnterSubmit").prop('disabled',true);
        return false;
      }
  });
});
</script>
