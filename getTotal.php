<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$claimStatus = "";

$branchType = $_SESSION['branch_type'];
$unitNo = $_POST['unitt'];   // department id
$b = $_POST['unitElement']; // no unit element selected
$c = str_replace("unit","",$b); // remove unit to get no only, eg unit1 become 1

$loanDetails = getLoanStatus($conn, "WHERE unit_no = ? AND branch_type =?", array("unit_no,branch_type"), array($unitNo,$branchType), "ss");
$totalDevAmt = $loanDetails[0]->getTotalDeveloperComm();
$projectName = $loanDetails[0]->getProjectName();
// $projectDetails = getProject($conn, "WHERE project_name =? AND branch_type =?", array("project_name,branch_type"), array($projectName,$branchType), "ss");
$projectDetails = getProject($conn, "WHERE project_name =? AND display ='Yes'",array("project_name"),array($projectName),"s");
$projectClaims = $projectDetails[0]->getProjectClaims();

$totalDevAmtFinal = $totalDevAmt / $projectClaims;
$totalDevAmtFinal = number_format($totalDevAmtFinal,2);
$totalDevAmtFinal = str_replace(",","",$totalDevAmtFinal);

if (!$loanDetails[0]->getClaimAmt1st()) {
  $claimStatus = 1;
}elseif (!$loanDetails[0]->getClaimAmt2nd()) {
  $claimStatus = 2;
}elseif (!$loanDetails[0]->getClaimAmt3rd()) {
  $claimStatus = 3;
}elseif (!$loanDetails[0]->getClaimAmt4th()) {
  $claimStatus = 4;
}elseif (!$loanDetails[0]->getClaimAmt5th()) {
  $claimStatus = 5;
}
   // department id
  // $sql = "SELECT totaldevelopercomm FROM loan_status WHERE unit_no='$unitNo'";
  //
  // $result = mysqli_query($conn,$sql);
  //
  // $loanDetails = array();
  //
  // while( $row = mysqli_fetch_array($result) ){
  //     $unit = $row['totaldevelopercomm'] / $projectClaims;


      $total[] = array("totaldevelopercomm" => $totalDevAmtFinal, "claim_status" => $claimStatus, "c" => $c);


  // }



  // encoding array to json format
  echo json_encode($total);


 ?>
