<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$output = '';
if(isset($_POST["export"]))
{
 $query = "SELECT * FROM loan_status";
 $result = mysqli_query($conn, $query);
 if(mysqli_num_rows($result) > 0)
 {
  $output .= '
   <table border = 1 >
                    <tr>
                    <th bgcolor= #C2C2C2>NO.</th>
										<th bgcolor= #C2C2C2>PROJECT NAME</th>
										<th bgcolor= #C2C2C2>UNIT NO.</th>
										<th bgcolor= #C2C2C2>NAME</th>
										<!-- <th>STOCK</th> -->
										<th bgcolor= #C2C2C2>IC</th>
										<th bgcolor= #C2C2C2>CONTACT</th>
										<th bgcolor= #C2C2C2>E-MAIL</th>
										<th bgcolor= #C2C2C2>BOOKING DATE</th>
										<th bgcolor= #C2C2C2>SQ FT</th>
										<th bgcolor= #C2C2C2>SPA PRICE</th>
										<th bgcolor= #C2C2C2>PACKAGE</th>
										<th bgcolor= #C2C2C2>DISCOUNT</th>
										<th bgcolor= #C2C2C2>REBATE</th>
										<th bgcolor= #C2C2C2>EXTRA REBATE</th>
										<th bgcolor= #C2C2C2>NETT PRICE</th>
										<th bgcolor= #C2C2C2>TOTAL DEVELOPER COMMISSION</th>
										<th bgcolor= #C2C2C2>AGENT</th>
										<th bgcolor= #C2C2C2>LOAN STATUS</th>

										<th bgcolor= #C2C2C2>REMARK</th>
										<th bgcolor= #C2C2C2>B FORM COLLECTED</th>
										<th bgcolor= #C2C2C2>PAYMENT METHOD</th>
										<th bgcolor= #C2C2C2>LAWYER</th>
										<th bgcolor= #C2C2C2>BANK APPROVED</th>
                    <th bgcolor= #C2C2C2>LOAN AMOUNT</th>
										<th bgcolor= #C2C2C2>LO SIGNED DATE</th>
										<th bgcolor= #C2C2C2>LA SIGNED DATE</th>
										<th bgcolor= #C2C2C2>SPA SIGNED DATE</th>
										<th bgcolor= #C2C2C2>FULLSET COMPLETED</th>
										<th bgcolor= #C2C2C2>CANCELLED BOOKING</th>
										<th bgcolor= #C2C2C2>CASE STATUS</th>
										<th bgcolor= #C2C2C2>EVENT/PERSONAL</th>
										<th bgcolor= #C2C2C2>RATE</th>
										<th bgcolor= #C2C2C2>AGENT COMMISSION</th>
										<th bgcolor= #C2C2C2>UPLINE1</th>
										<th bgcolor= #C2C2C2>UPLINE2</th>
										<th bgcolor= #C2C2C2>PL NAME</th>
										<th bgcolor= #C2C2C2>HOS NAME</th>
										<th bgcolor= #C2C2C2>LISTER NAME</th>
										<th bgcolor= #C2C2C2>UPLINE1</th>
										<th bgcolor= #C2C2C2>UPLINE2</th>
										<th bgcolor= #C2C2C2>PL OVERRIDE</th>
										<th bgcolor= #C2C2C2>HOS OVERRIDE</th>
										<th bgcolor= #C2C2C2>LISTER OVERRIDE</th>

										<th bgcolor= #C2C2C2>ADMIN1 OVERRIDE</th>
										<th bgcolor= #C2C2C2>ADMIN2 OVERRIDE</th>
                    <th bgcolor= #C2C2C2>ADMIN3 OVERRIDE</th>
										<th bgcolor= #C2C2C2>GIC PROFIT</th>
										<th bgcolor= #C2C2C2>TOTAL CLAIMED DEV AMT</th>
										<th bgcolor= #C2C2C2>TOTAL BAL UNCLAIMED DEV AMT</th>


                    </tr>
  ';
  $cnt = 0;
  while($row = mysqli_fetch_array($result))
  {
    $cnt++;
    if ($row['sq_ft']) {
      if (is_numeric($row['sq_ft'])) {
        $sqFt = number_format($row['sq_ft'],2);
      }else {
        $sqFt = $row['sq_ft'];
      }
    }else {
      $sqFt = "";
    }
    if ($row['spa_price']) {
      $spaPrice = number_format($row['spa_price'],2);
    }else {
      $spaPrice = "";
    }
    if ($row['package']) {
      if (is_numeric($row['package'])) {
      $package = number_format($row['package'],2);
    }else {
    $package = $row['package'];
    }
    }else {
      $package = "";
    }
    if ($row['nettprice']) {
      $nettPrice = number_format($row['nettprice'],2);
    }else {
      $nettPrice = "";
    }
    if ($row['totaldevelopercomm']) {
      $totalDevComm = number_format($row['totaldevelopercomm'],2);
    }else {
      $totalDevComm = "";
    }
    if ($row['loan_amount']) {
      if (is_numeric($row['loan_amount'])) {
          $loanAmount = number_format($row['loan_amount'],2);
      }else {
        $loanAmount = $row['loan_amount'];
      }
    }else {
      $loanAmount = "";
    }
    if ($row['agent_comm']) {
      $agentComm = number_format($row['agent_comm'],2);
    }else {
      $agentComm = "";
    }
    if ($row['ul_override']) {
      $ulOverride = number_format($row['ul_override'],2);
    }else {
      $ulOverride = "";
    }
    if ($row['uul_override']) {
      $uulOverride = number_format($row['uul_override'],2);
    }else {
      $uulOverride = "";
    }
    if ($row['pl_override']) {
      $plOverride = number_format($row['pl_override'],2);
    }else {
      $plOverride = "";
    }
    if ($row['hos_override']) {
      $hosOverride = number_format($row['hos_override'],2);
    }else {
      $hosOverride = "";
    }
    if ($row['lister_override']) {
      $listerOverride = number_format($row['lister_override'],2);
    }else {
      $listerOverride = "";
    }
    if ($row['admin1_override']) {
      $admin1Override = number_format($row['admin1_override'],2);
    }else {
      $admin1Override = "";
    }
    if ($row['admin2_override']) {
      $admin2Override = number_format($row['admin2_override'],2);
    }else {
      $admin2Override = "";
    }
    if ($row['admin3_override']) {
      $admin3Override = number_format($row['admin3_override'],2);
    }else {
      $admin3Override = "";
    }
    if ($row['gic_profit']) {
      $gicProfit = number_format($row['gic_profit'],2);
    }else {
      $gicProfit = "";
    }
    if ($row['total_claimed_dev_amt']) {
      $totalClaimedDevAmt = number_format($row['total_claimed_dev_amt'],2);
    }else {
      $totalClaimedDevAmt = "";
    }
    if ($row['total_bal_unclaim_amt']) {
      $totalBalUnclaimAmt = number_format($row['total_bal_unclaim_amt'],2);
    }else {
      $totalBalUnclaimAmt = "";
    }
    // if ($row['sq_ft'] == "" != "") {
    //   $package = $row['sq_ft'] = "";
    // }else {
    //   $package = number_format($row['sq_ft'],2);
    // }
    // if ($row['spa_price'] == "" != "") {
    //   $package = $row['spa_price'] = "";
    // }else {
    //   $package = number_format($row['spa_price'],2);
    // }
    // if ($row['package'] == "" != "") {
    //   $package = $row['package'] = "";
    // }else {
    //   $package = number_format($row['package'],2);
    // }
   $output .= '
										    <tr>
                        <td style="text-align: center;vertical-align: middle">'.$cnt.'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["project_name"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["unit_no"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["purchaser_name"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["ic"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["contact"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["email"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["booking_date"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$sqFt.'</td>
												<td style="text-align: center;vertical-align: middle">'.$spaPrice.'</td>
												<td style="text-align: center;vertical-align: middle">'.$package.'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["discount"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["rebate"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["extra_rebate"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$nettPrice.'</td>
												<td style="text-align: center;vertical-align: middle">'.$totalDevComm.'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["agent"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["loanstatus"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["remark"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["bform_Collected"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["payment_method"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["lawyer"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["bank_approved"].'</td>
                        <td style="text-align: center;vertical-align: middle">'.$loanAmount.'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["lo_signed_date"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["la_signed_date"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["spa_signed_date"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["fullset_completed"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["cancelled_booking"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["case_status"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["event_personal"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["rate"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$agentComm.'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["upline1"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["upline2"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["pl_name"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["hos_name"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$row["lister_name"].'</td>
												<td style="text-align: center;vertical-align: middle">'.$ulOverride.'</td>
												<td style="text-align: center;vertical-align: middle">'.$uulOverride.'</td>
												<td style="text-align: center;vertical-align: middle">'.$plOverride.'</td>
												<td style="text-align: center;vertical-align: middle">'.$hosOverride.'</td>
												<td style="text-align: center;vertical-align: middle">'.$listerOverride.'</td>
												<td style="text-align: center;vertical-align: middle">'.$admin1Override.'</td>
												<td style="text-align: center;vertical-align: middle">'.$admin2Override.'</td>
                        <td style="text-align: center;vertical-align: middle">'.$admin3Override.'</td>
												<td style="text-align: center;vertical-align: middle">'.$gicProfit.'</td>
												<td style="text-align: center;vertical-align: middle">'.$totalClaimedDevAmt.'</td>
												<td style="text-align: center;vertical-align: middle">'.$totalBalUnclaimAmt.'</td>


                    </tr>
   ';
  }
	$date = date('d-m-Y');
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=GIC Loan Status '.$date.'.xls');
  echo $output;
 }
}
?>
