<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$branchType = $_SESSION['branch_type'];
$bankDetails = getBankName($conn);
// $projectList = getProject($conn, "WHERE display = 'Yes' and branch_type =?",array("branch_type"),array($branchType), "s");
$projectList = getProject($conn, "WHERE display = 'Yes'");
if (isset($_POST['product_name'])) {
$loanDetails = getLoanStatus($conn, "WHERE project_name = ? AND branch_type =?", array("project_name,branch_type"), array($_POST['product_name'],$branchType), "ss");
}
 ?>
<form action="utilities/addNewInvoiceGeneralFunction.php" method="POST">
<div>
    <div class="three-input-div dual-input-div">
    <p>Project <a>*</a> </p>
      <select class="dual-input clean input-style" placeholder="Project" id="product_name" name="project_name">
        <option value="">Select a Project</option>
        <?php for ($cntPro=0; $cntPro <count($projectList) ; $cntPro++)
        {
        ?>
        <option value="<?php echo $projectList[$cntPro]->getProjectName(); ?>">
        <?php echo $projectList[$cntPro]->getProjectName(); ?>
        </option>
        <?php
        }
        ?>
      </select>
    <!-- </form> -->

    </div>

  <div class="three-input-div dual-input-div second-three-input">
    <p>To</p>
    <input required id="toWho" class="dual-input clean" type="text" placeholder="Auto Generated" readonly>
  </div>

    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div"> -->

    <div class="three-input-div dual-input-div">
      <p>Project Total Claims</p>
      <!-- <select class="dual-input clean" id="claimsNo" name="claims_no" >
        <option value="">Select an Unit</option>
      </select> -->
      <input class="dual-input clean" id="claimsNo" type="text" name="claims_no" value="" placeholder="auto generated" readonly>
    </div>

    <div class="tempo-two-input-clear"></div>

    <!-- <div class="dual-input-div second-dual-input"> -->
    <div class="three-input-div dual-input-div">
      <p>Date</p>
      <input class="dual-input clean input-style" type="date" id="date_of_claims" name="date_of_claims" value="<?php echo date('Y-m-d') ?>" required>
    </div>

    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div"> -->
    <div class="three-input-div dual-input-div second-three-input">
      <p>Invoice <a>*</a></p>
      <select id="noneInvoiceType" class="dual-input clean input-style" name="invoice_name" >
        <option value="">Please Select an Option</option>
        <option value="Proforma">Proforma</option>
        <option value="Invoice">Invoice</option>
        <!-- <option value="Credit Note">Credit Note</option> -->
      </select>
    </div>
    <!-- <div class="dual-input-div second-dual-input"> -->
    <div class="three-input-div dual-input-div">
      <p>Invoice Type <a>*</a></p>
      <select class="dual-input clean input-style" name="" >
        <option value="None">None</option>
      </select>
    </div>

    <div class="tempo-two-input-clear"></div>

    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a><img style="cursor: pointer" id="remBtn" width="13px" align="right" src="img/mmm.png"><img style="cursor: pointer" id="addBtn" width="13px" align="right" src="img/ppp.png"></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean input-style" id="unit" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any"  placeholder="Amount (RM)" id="amount" value="" name="amount[]">
  </div>

  <div class="three-input-div dual-input-div">
    <p>Status</p>
    <input class="dual-input clean" type="text" id="status" name="" placeholder="Status" value="">
  </div>
  <div id="input1" style="display: none" >
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit1" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount1" value="" name="amount[]">
  </div>

  <div class="three-input-div dual-input-div">
    <p>Status</p>
    <input class="dual-input clean" type="text" id="status1" name="" placeholder="Status" value="" readonly>
  </div>
  </div>

<div id="input2" style="display: none">
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit2" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount2" value="" name="amount[]">
  </div>

  <div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status2" name="" placeholder="Status" value="" readonly>
  </div>
</div>

<div id="input3" style="display: none">
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit3" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount3" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status3" name="" placeholder="Status" value="" readonly>
</div>
</div>

<div id="input4" style="display: none">
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit4" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount4" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status4" name="" placeholder="Status" value="" readonly>
</div>
</div>

<div id="input5" style="display: none" >
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit5" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount5" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status5" name="" placeholder="Status" value="" readonly>
</div>
</div>

<div id="input6" style="display: none" >
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit6" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount6" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status6" name="" placeholder="Status" value="" readonly>
</div>
</div>

<div id="input7" style="display: none" >
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit7" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount7" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status7" name="" placeholder="Status" value="" readonly>
</div>
</div>

<div id="input8" style="display: none" >
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit8" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount8" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status8" name="" placeholder="Status" value="" readonly>
</div>
</div>

<div id="input9" style="display: none" >
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit9" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount9" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status9" name="" placeholder="Status" value="" readonly>
</div>
</div>

<div id="input10" style="display: none" >
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit10" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount10" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status10" name="" placeholder="Status" value="" readonly>
</div>
</div>

<div id="input11" style="display: none" >
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit11" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount11" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status11" name="" placeholder="Status" value="" readonly>
</div>
</div>

<div id="input12" style="display: none" >
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit12" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount12" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status12" name="" placeholder="Status" value="" readonly>
</div>
</div>

<div id="input13" style="display: none" >
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit13" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount13" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status13" name="" placeholder="Status" value="" readonly>
</div>
</div>

<div id="input14" style="display: none" >
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit14" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount14" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status14" name="" placeholder="Status" value="" readonly>
</div>
</div>

<div id="input15" style="display: none" >
  <div class="three-input-div dual-input-div">
    <p>Unit <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <select class="dual-input clean" id="unit15" name="unit[]" >
        <option value="">Select an Unit</option>
      </select>
</div>
<div class="three-input-div second-three-input dual-input-div">
  <p>Amount (RM) <a>*</a></p>
  <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount15" value="" name="amount[]">
</div>

<div class="three-input-div dual-input-div">
  <p>Status</p>
  <input class="dual-input clean" type="text" id="status15" name="" placeholder="Status" value="" readonly>
</div>
</div>
  <!-- <p id="addIn"></p> -->
  <div class="tempo-two-input-clear"></div>

  <div class="three-input-div dual-input-div">
    <p>Change Payee Details <input id="checkBoxPayee" type="checkbox" name="" value=""></p>
        <!-- <form class="" action="" method="post"> -->
</div>
<div class="tempo-two-input-clear"></div>
<div style="display: none" id = "payeeDetails">
    <div class="three-input-div dual-input-div">
      <p>Bank Name Account Holder</p>
      <input  disabled class="dual-input clean" type="text" name="bank_account_holder" placeholder="Bank Name Account Holder">
    </div>
      <div class="three-input-div second-three-input dual-input-div">
        <p>Bank Name</p>
        <select  disabled class="dual-input clean input-style" name="bank_name">
          <option class="dual-input clean" value="">Select a option</option>
          <?php if ($bankDetails) {
            foreach ($bankDetails as $bankName) {
              ?><option class="dual-input clean" value="<?php echo $bankName->getBankName() ?>"><?php echo $bankName->getBankName() ?></option> <?php
            }
          } ?>
        </select>
      </div>
        <div class="three-input-div dual-input-div">
          <p>Bank Account No.</p>
          <input  disabled class="dual-input clean" type="number" id="status" name="bank_account_no" placeholder="Bank Account No." value="">
        </div>
</div>

    <div class="dual-input-div">
      <p>Include Service Tax (6%) <a>*</a></p>
      <input style="cursor: pointer" required class="yes" type="radio" value = "YES" name="charges" ><a class="yes-a">Yes</a>
      <input style="cursor: pointer" required class="no" type="radio" value = "NO" name="charges" ><a class="no-a">No</a>
    </div>
    <div class="tempo-two-input-clear"></div>

	<div class="width100 overflow text-center">
    	<button type="submit" name="upload" value="Upload" class="ow-margin-auto confirm-btn text-center white-text clean black-button">Confirm</button>
	</div>
  </form>
  </div>
  <script src="jsPhp/none.js" charset="utf-8"></script>
