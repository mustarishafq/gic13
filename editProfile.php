<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess1.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://gic.asia/editProfile.php" />
    <meta property="og:title" content="editProfile | GIC" />
    <title>Edit Details</title>
    <meta property="og:description" content="GIC" />
    <meta name="description" content="GIC" />
    <meta name="keywords" content="GIC,etc">
    <link rel="canonical" href="https://gic.asia/editProfile.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php  include 'allAdminHeader.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
<form method="POST" action="utilities/editProfileFunction.php">

    <div class="edit-profile-div2">
        <h1 class="username" id="username" name="username" style="margin-top: 50px">Edit Details</h1>
        <h2 class="profile-title">BASIC INFORMATION</h2>
        <table class="edit-profile-table">

            <tr class="profile-tr">
                <td class="profile-td1">Full Name</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_fullname" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getFullName();?>" name="update_fullname"></td>
            </tr>
            <tr class="profile-tr">
                <td class="profile-td1">Username</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getUsername();?>" readonly></td>
                <!-- <td class="profile-td3"><input id="update_username" class="clean edit-profile-input" type="text" value="<?php //echo $userDetails->getUsername();?>" name="update_username" readonly></td> -->
            </tr>
            <tr class="profile-tr">
                <td class="profile-td1">NRIC</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_icno" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getIcNo();?>" name="update_icno"></td>
            </tr>
        </table>

        <h2 class="profile-title">CONTACT INFORMATION</h2>
        <table class="edit-profile-table">
        	<tr class="profile-tr">
                <td class="profile-td1">Contact</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_phoneno" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getPhoneNo();?>" name="update_phoneno"></td>
            </tr>
        	<tr class="profile-tr">
                <td class="profile-td1">Address</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_address" class="clean edit-profile-input" type="text" placeholder="" value="<?php echo $userDetails->getAddress();?>" name="update_address"></td>
            </tr>
            <tr class="profile-tr">
                <td class="profile-td1">Email</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_email" class="clean edit-profile-input" type="text" placeholder="" value="<?php echo $userDetails->getEmail();?>" name="update_email"></td>
            </tr>
            <tr class="profile-tr">
                <td class="profile-td1">Birth Month</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_birthday" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getBirthday();?>" name="update_birthday"></td>
            </tr>
            <tr class="profile-tr">
                <td class="profile-td1">Bank</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_bankname" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getBankName();?>" name="update_bankname"></td>
            </tr>
            <tr class="profile-tr">
                <td class="profile-td1">Bank Acc No</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_bankaccountnumber" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getBankAccountNo();?>" name="update_bankaccountnumber"></td>
            </tr>
        </table>
        <button input type="submit" name="submit" value="Submit" class="confirm-btn text-center white-text clean black-button">Save</button>
        <p class="change-password-p"><a href="editPassword.php" class="edit-password-a black-link"></a></p>


    </div>
</form>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Profile Successfully !";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Unable to Update Profile Details";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "There was an error toupdate your profile <br>  Please Retry ！";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>
