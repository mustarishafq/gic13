<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];
$conn = connDB();

$id = rewrite($_POST['project_id']);

$projectDetails = getProject($conn, "WHERE id = '$id'");
$projectName = $projectDetails[0]->getProjectName();
$pic = $projectDetails[0]->getAddProjectPpl();
$projectClaims = $projectDetails[0]->getProjectClaims();
$projectLeader = $projectDetails[0]->getProjectLeader();
$projectLeaderExp = explode(",",$projectLeader);
$companyName = $projectDetails[0]->getCompanyName();
$companyAddress = $projectDetails[0]->getCompanyAddress();

$conn = connDB();

$userRows = getUser($conn," WHERE id = ? ",array("id"),array($uid),"s");
$userDetails = $userRows[0];
$agentDetails = getUser($conn,"WHERE user_type = 3");
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Edit Project | GIC" />
    <title>Edit Project | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'admin1Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
  <h1 class="h1-title h1-before-border shipping-h1"  onclick="goBack()">
    	<a  class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back3.png" class="back-btn2 hover1b" alt="back" title="back">
        	Edit Project
        </a>
    </h1>
    <div class="short-red-border"></div>

    <form method="POST" action="utilities/editProjectFunction.php" class="margin-top30">
        <label class="labelSize">Project Name :</label>
        <input  class="inputSize input-pattern" type="text"  placeholder="Project Name" name="project_name" id="project_name" value="<?php echo $projectName ?>"><br>

        <label class="labelSize">Person In Charge (PIC) :</label>
        <input  class="inputSize input-pattern" type="text" placeholder="Person In Charge (PIC)" name="add_by" id="add_by" value="<?php echo $pic ?>"><br>

        <label class="labelSize">Project Leader :</label><img style="cursor: pointer" id="remBtn" width="13px" align="right" src="img/mmm.png"><img style="cursor: pointer" id="addBtn" width="13px" align="right" src="img/ppp.png">
        <?php if ($projectLeader) {
          for ($aa=0; $aa < count($projectLeaderExp) ; $aa++) {
            ?>
        <select id="<?php echo "project_leader".$aa ?>" class="inputSize input-pattern" name="project_leader[]">
          <option value="<?php echo $projectLeaderExp[$aa]; ?>"><?php echo $projectLeaderExp[$aa] ?></option> <?php
          if ($projectLeader) {
              for ($i=0; $i <count($agentDetails) ; $i++) {
                  ?><option value="<?php echo $agentDetails[$i]->getUsername() ?>"><?php echo $agentDetails[$i]->getUsername() ?></option> <?php
              }
          }else {
            ?><option value="">Select an option</option> <?php
            for ($i=0; $i <count($agentDetails) ; $i++) {
              ?><option value="<?php echo $agentDetails[$i]->getUsername() ?>"><?php echo $agentDetails[$i]->getUsername() ?></option> <?php
            }
          } ?>
        </select>
      <?php }}
      for ($k= count($projectLeaderExp); $k < 5 ; $k++) {
        ?><select id="<?php echo "project_leader".$k ?>" disabled style="display: none" class="inputSize input-pattern" name="project_leader[]">
          <option value="">Select a option</option>
          <?php if ($agentDetails) {
            for ($cnt=0; $cnt <count($agentDetails) ; $cnt++) {
              ?><option value="<?php echo $agentDetails[$cnt]->getUsername() ?>"><?php echo $agentDetails[$cnt]->getUsername() ?></option> <?php
            }
          } ?>
        </select> <?php
        }
       ?>
        <label class="labelSize">Company Name :</label>
        <input  class="inputSize input-pattern" type="text" placeholder="Company Name" name="company_name" id="add_by" value="<?php echo $companyName ?>"><br>

        <label class="labelSize">Company Address :</label>
        <?php if ($companyAddress) {
          $companyAddressExp = explode(",",$companyAddress);
          for ($i=0; $i <count($companyAddressExp) ; $i++) {
            ?><input  class="inputSize input-pattern" type="text" placeholder="Company Address" name="company_address[]" id="add_by" value="<?php echo $companyAddressExp[$i] ?>"><br><?php
          }
          for ($j = count($companyAddressExp); $j < 3; $j++) {
            $a = $j + 1;
            ?><input  class="inputSize input-pattern" type="text" placeholder="<?php echo "Line".$a ?>" name="company_address[]" id="add_by"><?php
          }
        } ?>

        <label class="labelSize">No. of Interest Claims</label>
        <input class="inputSize input-pattern" type="number" placeholder="No. of Interest Claims" name="claims_times" id="claims_times" value="<?php echo $projectClaims ?>"><br>

        <!-- <input type="hidden" name="add_by" id="add_by" value="<?php //echo $userDetails->getUsername(); ?>"> -->
        <input type="hidden" name="id" value="<?php echo $id ?>">
        <button class="button" type="submit" name="editButton">Submit</button><br>
    </form>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
</body>
</html>
<script type="text/javascript">
$(document).ready(function(){
var a = <?php echo count($projectLeaderExp); ?>;
if (a > 0) {
  $("#remBtn").show();
}else {
  $("#remBtn").hide();
}

  $("#addBtn").click( function(){
    if (a == 1) {
      $(".labelSize1").fadeIn(500);
      $("#project_leader1").fadeIn(500);
      $("#project_leader1").removeAttr('disabled');
      $("#remBtn").show();
    }
    if (a == 2) {
      $(".labelSize2").fadeIn(500);
      $("#project_leader2").fadeIn(500);
      $("#project_leader2").removeAttr('disabled');
    }
    if (a == 3) {
      $(".labelSize3").fadeIn(500);
      $("#project_leader3").fadeIn(500);
      $("#project_leader3").removeAttr('disabled');
    }
    if (a == 4) {
      $(".labelSize4").fadeIn(500);
      $("#project_leader4").fadeIn(500);
      $("#project_leader4").removeAttr('disabled');
    }
    if (a > 3) {
      $("#addBtn").hide();
      $("#remBtn").show();
    }
      a++;

  });
  // var b = 0;
  $("#remBtn").click( function(){
    a--;
    if (a < 0) {
      a = 1;
    }
    if (a == 1) {
      $(".labelSize1").fadeOut(500);
      $("#project_leader1").fadeOut(500);
      $("#project_leader1").prop('disabled',true);
      $("#addBtn").show();
      $("#remBtn").hide();
    }
    if (a == 2) {
      $(".labelSize2").fadeOut(500);
      $("#project_leader2").fadeOut(500);
      $("#project_leader2").prop('disabled',true);
    }
    if (a == 3) {
      $(".labelSize3").fadeOut(500);
      $("#project_leader3").fadeOut(500);
      $("#project_leader3").prop('disabled',true);
    }
    if (a == 4) {
      $(".labelSize4").fadeOut(500);
      $("#project_leader4").fadeOut(500);
      $("#project_leader4").prop('disabled',true);
      $("#addBtn").show();
    }

  });
});
</script>
<script>
function goBack() {

  window.history.back();
}
</script>