<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();
$a = "";

$projectName = $_POST['projectName'];   // department id
$branchType = $_SESSION['branch_type'];
// $projectName = 'Project A1';
   // department id
// $projectDetails = getProject($conn, "WHERE project_name =? AND branch_type =?", array("project_name,branch_type"), array($projectName,$branchType), "ss");
$projectDetails = getProject($conn, "WHERE project_name =? AND display ='Yes'",array("project_name"),array($projectName),"s");
$projectClaims = $projectDetails[0]->getProjectClaims();
$projectPeople = $projectDetails[0]->getAddProjectPpl();
$loanDetails = getLoanStatus($conn,"WHERE project_name =? AND case_status='COMPLETED' AND cancelled_booking != 'YES' AND display = 'Yes' AND branch_type =?", array("project_name,branch_type"), array($projectName,$branchType), "ss");
if ($loanDetails) {
  for ($cnt=0; $cnt <count($loanDetails) ; $cnt++) {

      $a = $loanDetails[$cnt]->getUnitNo();

            if ($a) {
              $f[] = array("unit_no" => $a, "add_projectppl" => $projectPeople, "project_claims" => $projectClaims);
            }
  }
}

  // encoding array to json format
  echo json_encode($f);


 ?>
