<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/OtherInvoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$finalAmountFinal = 0;
$af = 0;
$aff = 0;
// $loanDetails = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$invoiceDetails = getInvoice($conn);
$projectDetails = getProject($conn);
// $projectName = "WHERE case_status = 'COMPLETED'";
$projectName = "WHERE case_status = 'COMPLETED'";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Invoice Record | GIC" />
    <title>Invoice Record-Other | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style>
input#checkboxToDisplayTableMulti{
    width :20px;
    height :20px;
    vertical-align: middle;
    outline: 2px solid maroon;
    outline-offset: -2px;
}
      body {

      }
      input {

      font-weight: bold;
      /* color: white; */
      text-align: center;
      border-radius: 15px;
      }
      .no-outline:focus {
      outline: none;

      }
      button{
        border-radius: 15px;
        color: white;

      }
      .button-add{
        background-color: green;
      }
      .button-remove{
        background-color: maroon;
      }
      th:hover{
        background-color: maroon;
      }
      th.headerSortUp{
        background-color: black;
      }
      th.headerSortDown{
        background-color: black;
      }
    </style>
    <script type="text/javascript">
      $(document).ready( function(){
        $("#checkEmpty").click( function(){
          $("#overallTable").toggle();
          $("#emptyTable").toggle();
        });
      });
    </script>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Invoice Record-Other</h1>
    <div class="short-red-border"></div>
    <h3 class="h1-title"><a href="invoiceRecordProforma.php" class="h1-title red-color-swicthing">Proforma Invoice</a> | <a href="invoiceRecord.php" class="h1-title red-color-swicthing">Single Invoice</a> | <a href="invoiceRecordMulti.php" class="h1-title red-color-swicthing">Multi Invoice</a> | Other Invoice | <a href="invoiceRecordCredit.php" class="h1-title red-color-swicthing">Credit Note Invoice</a></h3>
    <!-- <input type="checkbox" id="checkEmpty" value=""> -->


        <!-- <td><p class="h1-title">Fill Check Number Only</p></td>
        <td><input type="checkBox" id="checkboxToDisplayTableMultiFill"></td> -->

    <!-- This is a filter for the table result -->
  <div class="section-divider width100 overflow">

    <?php $projectDetails = getProject($conn, "WHERE display = 'Yes'"); ?>

      <select id="sel_id" class="clean-select clean pointer">
        <option value="">Select a project</option>
        <?php if ($projectDetails) {
          for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
            if ($projectDetails[$cnt]->getProjectName() != $types) {
              ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
              }
              }
              ?><option value="ALL">SHOW ALL</option><?php
            } ?>
      <!-- <option value="-1">Select</option>
      <option value="VIDA">kasper </option>
      <option value="VV">adad </option> -->
      <!-- <option value="14">3204 </option> -->
      </select>
      <input class="clean search-btn" type="text" name="" value="" placeholder="Search.." id="searchInput">
  </div>

  <div class="clear"></div>
  <a href="excel/invoiceRecordOtherExport.php"><button class="exportBtn red-bg-swicthing" type="button" name="button">Export</button></a>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table id="overallTable" class="shipping-table pointer-th">
                <thead>
                    <tr>
                        <th class="th">NO. <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("PROJECT NAME",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th">DATE <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("INVOICE NO.",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th">AMOUNT (RM) <img src="img/sort.png" class="sort"></th>
                        <th class="th">TOTAL AMOUNT (RM) <img src="img/sort.png" class="sort"></th>
                        <th class="th">SST <img src="img/sort.png" class="sort"></th>
                        <th class="th">1ST STATUS <img src="img/sort.png" class="sort"></th>
                        <th class="th">RECEIVED DATE <img src="img/sort.png" class="sort"></th>
                        <th class="th">CHECK NO. <img src="img/sort.png" class="sort"></th>
                        <th class="th">DATE MODIFIED <img src="img/sort.png" class="sort"></th>
                        <th class="th">ACTION <img src="img/sort.png" class="sort"></th>
                        <th class="th">ARCHIVED <img src="img/sort.png" class="sort"></th>

                        <!-- <th>STOCK</th> -->


                        <!-- <th class="th">BANK APPROVED</th>
                        <th class="th">LO SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th> -->
                        <!-- <th>INVOICE</th> -->
                    </tr>
                </thead>
                <tbody id="myFilter">
                    <?php
                    $otherInvoiceDetails = getOtherInvoice($conn, "WHERE branch_type = ? and display = 'Yes'",array("branch_type"),array($_SESSION['branch_type']), "s");

                    if ($otherInvoiceDetails) {
                      for ($cnt=0; $cnt <count($otherInvoiceDetails); $cnt++) {
                        // $claimStatus = $otherInvoiceDetails[$cnt]->getClaimsStatus();
                        // $claimStatusExplode = explode(",",$claimStatus);
                        ?>
                        <tr>
                        <td class="td"><?php echo $cnt+1; ?>
                        <td class="td"><?php echo $otherInvoiceDetails[$cnt]->getProjectName(); ?></td>
                        <td class="td"><?php echo str_replace(",","<br>",$otherInvoiceDetails[$cnt]->getUnitNo()); ?></td>
                        <td class="td"><?php echo date('d-m-Y', strtotime($otherInvoiceDetails[$cnt]->getBookingDate())); ?></td>
                        <td class="td"><?php echo date('ymd', strtotime($otherInvoiceDetails[$cnt]->getDateCreated())).$otherInvoiceDetails[$cnt]->getID(); ?></td>
                        <td class="td"><?php $getAmountExp = explode(",",$otherInvoiceDetails[$cnt]->getFinalAmount());
                        for ($cntAA=0; $cntAA <count($getAmountExp) ; $cntAA++) {
                          $finalAm = $getAmountExp[$cntAA];
                          echo number_format($finalAm,2)."<br>";
                          }
                        ?>
                        </td>
                        <td class="td"><?php $getAmountExplode = explode(",",$otherInvoiceDetails[$cnt]->getFinalAmount());
                        for ($cntAA=0; $cntAA <count($getAmountExplode) ; $cntAA++) {
                          $finalAmount = $getAmountExplode[$cntAA];
                          $finalAmountFinal += $finalAmount;
                        }
                        echo number_format($finalAmountFinal,2);
                        $finalAmountFinal = 0;  ?></td>
                        <td class="td"><?php echo $otherInvoiceDetails[$cnt]->getCharges(); ?></td>
                        <td class="td"><?php echo $otherInvoiceDetails[$cnt]->getReceiveStatus(); ?></td>
                        <td class="td"><?php
                        ?><form class="" action="utilities/checkNoOther.php" method="post"><?php
                        if (!$otherInvoiceDetails[$cnt]->getReceiveDate()) {
                          ?><input type="date" name="receive_date" value="<?php echo date('Y-m-d'); ?>"> <?php
                        }else {
                           echo date('d-m-Y', strtotime($otherInvoiceDetails[$cnt]->getReceiveDate()));
                        }
                        ?></td>
                        <td class="td">
                          <?php if ($otherInvoiceDetails[$cnt]->getCheckID() && $otherInvoiceDetails[$cnt]->getReceiveDate()) {
                            echo $otherInvoiceDetails[$cnt]->getCheckID();
                            ?><button class="button-remove" type="submit" name="removeCheckID">Edit</button><?php
                          }else {
                            ?><input required class="no-outline" type="text" name="check_id" placeholder="CHECK NUMBER" value="<?php echo $otherInvoiceDetails[$cnt]->getCheckID(); ?>">
                            <button class="button-add" type="submit" name="addCheckID">Submit</button><?php
                          } ?>
                          <?php

                            ?>
                          <!-- <input type="hidden" name="loan_uid" value="<?php //echo $orderDetails[$cntAA]->getLoanUid() ?>"> -->
                          <input type="hidden" name="id" value="<?php echo $otherInvoiceDetails[$cnt]->getId() ?>">
                        </form>
                      </td>
                      <td class="td">
                        <form class="" action="historyInvoiceOther.php" method="post">
                          <button class="clean edit-anc-btn hover1" value="<?php echo $otherInvoiceDetails[$cnt]->getInvoiceId() ?>" type="submit" name="invoice_id">
                            <a style="color: blue"><?php if ($otherInvoiceDetails[$cnt]->getDateUpdated()) {
                              echo date('d/m/Y',strtotime($otherInvoiceDetails[$cnt]->getDateUpdated()));
                            } ?></a>
                          </button>
                        </form>
                      </td>
                          <td class="td">
                          <form action="invoiceOther.php" method="POST">
                            <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="invoice_id" value="<?php echo $otherInvoiceDetails[$cnt]->getInvoiceId();?>">Print Invoice
                              <input type="hidden" name="claim_status" value="1">
                              <input type="hidden" name="invoice_no" value="<?php echo date('ymd', strtotime($otherInvoiceDetails[$cnt]->getDateCreated())).$otherInvoiceDetails[$cnt]->getID(); ?>">
                              <input type="hidden" name="booking_date" value="<?php echo date('d-m-Y', strtotime($otherInvoiceDetails[$cnt]->getBookingDate())); ?>">
                                  <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                  <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                              </button></a>
                          </form> </td>
                          <td class="td">
                              <!-- <form action="utilities/archiveLoan.php" method="POST"> -->
                                  <button id="archiveBtn" class="clean edit-anc-btn hover1" type="submit" name="loan_uid_archive" value="<?php echo $otherInvoiceDetails[$cnt]->getInvoiceId();?>">
                                      <img src="img/archiv0.png" class="edit-announcement-img hover1a" >
                                      <img src="img/archiv3.png" class="edit-announcement-img hover1b" >
                                  </button>
                              <!-- </form> -->
                          </td>
                      </tr> <?php
                      }
                    }else {
                      ?>
                      <td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="14" >No Invoice Record Yet.</td>
                      <?php
                    }


                    ?>
                </tbody>
            </table>

    </div>
</div>



<?php unset($_SESSION['idPro']);
      unset($_SESSION['invoice_id']);
      unset($_SESSION['idComm']);
      unset($_SESSION['loan_uid']);
      unset($_SESSION['commission_id']);
      unset($_SESSION['idImp']);?>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Issue Other Invoice.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Updated Invoice Details.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $("#overallTable").tablesorter( {dateFormat: 'pt'} );
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("button[name='loan_uid_archive']").click(function(){
      var invoiceID = $(this).val();

      $.ajax({
        url: "utilities/archiveInvoiceOther.php",
        type: "post",
        data: {invoiceID:invoiceID},
        dataType: "json",
        success:function(response){

          var unit = response[0]['unit_no'];
          var test = " Has Been Add To Archive";
          // $("#notyElDiv").fadeIn(function(){
            // $("#notyEl").html(unit+test);
                putNoticeJavascriptReload("Notice !!",""+unit+" Has Been Add To Archive.");
          // });
          // alert(unit);
          // location.reload();
        }
      });
    });
  });
</script>
</body>
</html>
