<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$fullName = $_SESSION['fullname'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE full_name =?",array("full_name"),array($fullName), "s");
$uid = $userDetails[0]->getUid();

$getWho = getWholeDownlineTree($conn, $uid, false);
$no = 0;

$projectDetails = getProject($conn);
// $projectName = "";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:title" content="My Team Performance | GIC" />
    <title>My Team Performance | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
  <link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
    <?php include 'css.php'; ?>
</head>
<style media="screen">
th:hover{
  background-color: maroon;
}
th.headerSortUp{
  background-color: black;
}
th.headerSortDown{
  background-color: black;
}
</style>
<body class="body">
<?php  include 'agentHeader.php'; ?>
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div class="yellow-body same-padding">
  <div class="modal">

  </div>

  <h1 class="h1-title h1-before-border shipping-h1">My Team Performance</h1>
  <div class="short-red-border"></div>
<!-- <h3 class="h1-title m-margin-top"> Personal Sales | <a href="personalOverriding.php" class="h1-title">Personal Overriding</a></h3> -->
<h3 class="h1-title m-margin-top"> <a href="myPerformance.php" class="red-color-swicthing">Personal Sales</a> | Downline Performance</h3><br>
<p style="color: maroon;font-size: 18px" class="h1-title m-margin-top"> Position : <?php echo $userDetails[0]->getPosition() ?></a>
  <div class="section-divider width100 overflow">

  <?php
  $conn = connDB();

//   $projectName = "";
  // $projectName = "WHERE agent = '$userUsername' ";

  $projectDetails = getProject($conn);
  ?>

  <?php $projectDetails = getProject($conn, "WHERE display = 'Yes'"); ?>

    <select id="projectChoose" class="clean-select clean pointer">
      <option value="">Select a project</option>
      <?php if ($projectDetails) {
        for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
          if ($projectDetails[$cnt]->getProjectName() != $types) {
            ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
            }
            }
            ?><option value="">SHOW ALL</option><?php
          } ?>
    </select>
      <input class="clean search-btn" type="text" name="" value="" placeholder="Search.." id="searchInput" autocomplete="off">
  </div>
  <a href="excel/myTeamPerformanceExport.php"><button class="exportBtn red-bg-swicthing" type="button" name="button">Export</button></a>


    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
        <table id="myTable" class="shipping-table pointer-th">
            <thead>
                <tr>
                    <th class="th">NO. <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("PROJECT NAME",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("BOOKING DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("NETT PRICE",8,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("AGENT",8,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("LO SIGNED DATE",15,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("SPA SIGNED DATE",15,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                    <th class="th"><?php echo wordwrap("OVERRIDE COMMISSION (RM)",15,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                </tr>
            </thead>
            <tbody id="myFilter">
              <?php
                if ($getWho) {
                  for ($i=0; $i <count($getWho) ; $i++) {
                    $loanDetails = getLoanStatus($conn, "WHERE agent =?",array("agent"),array($getWho[$i]->getReferralName()), "s");
                    if ($loanDetails) {
                      $no += 1;
                      ?>
                      <tr>
                        <td class="td"><?php echo $no; ?></td>
                        <td class="td"><?php echo $loanDetails[0]->getProjectName(); ?></td>
                        <td class="td"><?php echo $loanDetails[0]->getUnitNo(); ?></td>
                        <td class="td"><?php echo date('d/m/Y',strtotime($loanDetails[0]->getBookingDate())); ?></td>
                        <td class="td"><?php echo $loanDetails[0]->getNettPrice(); ?></td>
                        <td class="td"><?php echo $loanDetails[0]->getAgent(); ?></td>
                        <?php
                        if ($loanDetails[0]->getLoSignedDate()) {
                          ?><td class="td"><?php echo date('d/m/Y',strtotime($loanDetails[0]->getLoSignedDate())); ?></td><?php
                        }else {
                          ?><td class="td"></td><?php
                        }
                        if ($loanDetails[0]->getSpaSignedDate()) {
                          ?><td class="td"><?php echo date('d/m/Y',strtotime($loanDetails[0]->getSpaSignedDate())); ?></td><?php
                        }else {
                          ?><td class="td"></td><?php
                        }
                         ?>
                        <td class="td"><?php echo number_format($loanDetails[0]->getAgentComm(),2); ?></td>
                      </tr>
                      <?php
                    }
                  }
                }
               ?>
            </tbody>
        </table><br>
    </div>

    <?php //$conn->close();?>

</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<!-- ***********************************************-->
<script>
$("#searchStartDate,#searchEndDate").datepicker({
  dateFormat: 'dd/mm/yy',
});

$("#projectChoose").on("change",function(){
    var project = $("#projectChoose").val();
  $.ajax({
    url: 'myTeamPerformanceFilter.php',
    data: {project:project},
    type: 'post',
    success:function(data){
      $("#myTable").empty();
      $("#myTable").html(data);
    }
  });
});
$("#searchInput").on("change",function(){
  var project = $(this).val().toLowerCase();
  if (project != 'all') {
    $("#myFilter tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(project) > -1)
    });
  }else if(project == 'all') {
    $("#myFilter tr").show();
  }
});
$(document).ready(function() {
  $(document).ajaxStart(function() {
      $(".modal").show();
  });
  $(document).ajaxStop(function() {
      $(".modal").hide();
  });
});
</script>
<!-- ***********************************************-->
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
          if ($announcementDetails)
          {
          for ($cnt=0; $cnt <count($announcementDetails) ; $cnt++)
          {
            $dateCreated = date('Y-m-d H:i:s',strtotime($announcementDetails[$cnt]->getDateCreated()."+ 1 days"));
            $now = date('Y-m-d H:i:s');
            if ($dateCreated >= $now ) {
              $messageType = $announcementDetails[$cnt]->getDetails()."<br>";
              echo '
              <script>
                  putNoticeJavascript("Announcement !! ","'.$messageType.'");
              </script>';
              $_SESSION['messageType'] = 0;
            }

          }
          ?>

          <?php
          }
        }
        // echo '
        // <script>
        //     putNoticeJavascript("Announcement !! ","'.$messageType.'");
        // </script>
        // ';
        $_SESSION['messageType'] = 0;
      }}
 ?>
</body>
<script type="text/javascript">
function showFrontLayer() {
  document.getElementById('bg_mask').style.visibility='visible';
  document.getElementById('frontlayer').style.visibility='visible';
}
function hideFrontLayer() {
  document.getElementById('bg_mask').style.visibility='hidden';
  document.getElementById('frontlayer').style.visibility='hidden';
}
</script>
<script type="text/javascript">
  $(document).ready( function(){
    $("#getAnnouncementTable").click( function(){
      $("#announcementTable").slideToggle();
    });
  });
</script>
<script>
  $(function(){
    $("#myTable").tablesorter( {dateFormat: 'pt'} );
  });
</script>
</html>
