<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
$userUsername = $userDetails->getUsername();

$projectDetails = getProject($conn);
// $projectName = "";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="My Team | GIC" />
    <title>My Team | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  th:hover{
    background-color: maroon;
  }
  th.headerSortUp{
    background-color: black;
  }
  th.headerSortDown{
    background-color: black;
  }
  #author,#up1,#up2,#up3,#up4,#up5{
    list-style-image: url('img/li.png');
    font-weight: bold;
    font-size: 16px;
  }
  #up6{
    font-weight: bold;
    font-size: 16px;
  }
  li:hover{
    animation-duration: 0.5s;
    animation-name: li;
    color: maroon;
    font-size: 16px;
  }
  @keyframes li {
    0% {font-size: 15px;}
    100% {font-size: 16px;}
  }
</style>
<body class="body">
<?php  include 'agentHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">

  <h1 class="h1-title h1-before-border shipping-h1">My Group</h1>
  <div class="short-red-border"></div>
  <div class="section-divider width100 overflow">
    <?php if ($userDetails->getPosition()) {
      $position = " (".$userDetails->getPosition().")";
    }else {
      $position = "";
    }
    if ($userDetails->getUpline1() != 'N/A' && $userDetails->getUpline1() !="" && $userDetails->getUpline1() != 'null' && $userDetails->getUpline1() != 'Null' && $userDetails->getUpline1() != 'NULL') {
      $upline1Details = getUser($conn, "WHERE username = ?",array("username"),array($userDetails->getUpline1()), "s");
      $upline1Position =  " (".$upline1Details[0]->getPosition().")";
    }else {
      $upline1Position = "";
    }
    if ($userDetails->getUpline2() != 'N/A' && $userDetails->getUpline2() !="" && $userDetails->getUpline2() != 'null' && $userDetails->getUpline2() != 'Null' && $userDetails->getUpline2() != 'NULL') {
      $upline2Details = getUser($conn, "WHERE username = ?",array("username"),array($userDetails->getUpline2()), "s");
      $upline2Position =  " (".$upline2Details[0]->getPosition().")";
    }else {
      $upline2Position = "";
    }
    if ($userDetails->getUpline3() != 'N/A' && $userDetails->getUpline3() !="" && $userDetails->getUpline3() != 'null' && $userDetails->getUpline3() != 'Null' && $userDetails->getUpline3() != 'NULL') {
      $upline3Details = getUser($conn, "WHERE username = ?",array("username"),array($userDetails->getUpline3()), "s");
      $upline3Position =  " (".$upline3Details[0]->getPosition().")";
    }else {
      $upline3Position = "";
    }
    if ($userDetails->getUpline4() != 'N/A' && $userDetails->getUpline4() !="" && $userDetails->getUpline4() != 'null' && $userDetails->getUpline4() != 'Null' && $userDetails->getUpline4() != 'NULL') {
      $upline4Details = getUser($conn, "WHERE username = ?",array("username"),array($userDetails->getUpline4()), "s");
      $upline4Position =  " (".$upline4Details[0]->getPosition().")";
    }else {
      $upline4Position = "";
    }
    if ($userDetails->getUpline5() != 'N/A' && $userDetails->getUpline5() !="" && $userDetails->getUpline5() != 'null' && $userDetails->getUpline5() != 'Null' && $userDetails->getUpline5() != 'NULL') {
      $upline5Details = getUser($conn, "WHERE username = ?",array("username"),array($userDetails->getUpline5()), "s");
      $upline5Position =  " (".$upline5Details[0]->getPosition().")";
    }else {
      $upline5Position = "";
    }


    ?>

    <ul>
      <li id="author">Eddie Song (Founder)</li>
      <ul>
        <li id="up1"><?php echo $userDetails->getUpline1().$upline1Position ?></li>
        <ul>
          <li id="up2"><?php
          if (!$userDetails->getUpline2()) {
            echo "No Upline.";
          }else {
            echo $userDetails->getUpline2().$upline2Position;
          }
           ?></li>
          <ul>
            <li id="up3"><?php
            if (!$userDetails->getUpline3()) {
              echo "No Upline.";
            }else {
              echo $userDetails->getUpline3().$upline3Position;
            }
             ?></li>
            <ul>
              <li id="up4"><?php
              if (!$userDetails->getUpline4()) {
                echo "No Upline.";
              }else {
                echo $userDetails->getUpline4().$upline4Position;
              }
               ?></li>
              <ul>
                <li id="up5"><?php
                if (!$userDetails->getUpline5()) {
                  echo "No Upline.";
                }else {
                  echo $userDetails->getUpline5().$upline5Position;
                }
                 ?></li>
                <ul>
                  <li id="up6"><?php echo $userDetails->getUsername().$position ?></li>
                </ul>
              </ul>
            </ul>
          </ul>
        </ul>
      </ul>
    </ul>
</body>
<script>
  $(function(){
    for (var i = 1; i < 7; i++) {
      $("#up"+i+"").hide();
    }
    $("#author").click(function(){

      if ($("#up1").css('display') == 'none') {
        $("#up1").slideDown(function(){
          $("#up1").show();
        });
      }else {
        $("#up1,#up2,#up3,#up4,#up5,#up6").slideUp(function(){
          $(this).hide();
        });
      }
    });
    $("#up1").click(function(){
      if ($("#up2").css('display') == 'none') {
        $("#up2").slideDown(function(){
          $("#up2").show();
        });
      }else {
        $("#up2,#up3,#up4,#up5,#up6").slideUp(function(){
          $(this).hide();
        });
      }
    });
    $("#up2").click(function(){
      if ($("#up3").css('display') == 'none') {
        $("#up3").slideDown(function(){
          $("#up3").show();
        });
      }else {
        $("#up3,#up4,#up5,#up6").slideUp(function(){
          $(this).hide();
        });
      }
    });
    $("#up3").click(function(){
      if ($("#up4").css('display') == 'none') {
        $("#up4").slideDown(function(){
          $("#up4").show();
        });
      }else {
        $("#up4,#up5,#up6").slideUp(function(){
          $(this).hide();
        });
      }
    });
    $("#up4").click(function(){
      if ($("#up5").css('display') == 'none') {
        $("#up5").slideDown(function(){
          $("#up5").show();
        });
      }else {
        $("#up5,#up6").slideUp(function(){
          $(this).hide();
        });
      }
    });
    $("#up5").click(function(){
      if ($("#up6").css('display') == 'none') {
        $("#up6").slideDown(function(){
          $("#up6").show();
        });
      }else {
        $("#up6").slideUp(function(){
          $(this).hide();
        });
      }
    });
  });
</script>
</html>
