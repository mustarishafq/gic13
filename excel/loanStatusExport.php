<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Project.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/../utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/languageFunction.php';
// require_once dirname(__FILE__) . '/../vendor/autoload.php';

$conn = connDB();
$cell = 6;
$branchType = $_SESSION['branch_type'];
$projectNameArr = [];

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($_SESSION['uid']), "s");
$fullName = $userDetails[0]->getFullName();

$projectDetails = getProject($conn, "WHERE display = 'Yes' AND project_leader =?",array("project_leader"),array($_SESSION['username']), "s");
if ($projectDetails) {
  for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
    $projectNameArr[] = $projectDetails[$cnt]->getProjectName();
  }
}
  $projectNameArrImp = implode(",",$projectNameArr);
  $projectNameArrExp = explode(",",$projectNameArrImp);
  for ($j=0; $j <count($projectNameArrExp) ; $j++) {
    if ($branchType == 1) {
        $loanDetails = getLoanStatus($conn,"WHERE display = 'Yes' AND project_name =?",array("project_name"),array($projectNameArrExp[$j]), "s");
    }else {
        $loanDetails = getLoanStatus($conn,"WHERE display = 'Yes' and branch_type = ? AND project_name =?",array("branch_type","project_name"),array($_SESSION['branch_type'],$projectNameArrExp[$j]), "ss");
    }
  }

//call the autoload
require '../vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;

//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();

//color th
$spreadsheet->getActiveSheet()->getStyle('A6:AR6')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');
$spreadsheet->getActiveSheet()->getStyle('A2:B2')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');
$spreadsheet->getActiveSheet()->getStyle('A3:B3')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');

    $styleArray = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'borders' => [
          'allBorders' => [
           'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
           'color' => ['argb' => '000000'],
         ],
      ],
    ];

//border line and center
    $spreadsheet->getActiveSheet()->getStyle('A6:AR6')->applyFromArray($styleArray);
    $spreadsheet->getActiveSheet()->getStyle('A2:E2')->applyFromArray($styleArray);
    $spreadsheet->getActiveSheet()->getStyle('A3:E3')->applyFromArray($styleArray);

//set the value of cell a1 to "Hello World!"
// $sheet->setCellValue('A1', 'Hello World !');
$sheet->setCellValue('A6', 'NO.');
$sheet->setCellValue('B6', 'UNIT NO.');
$sheet->setCellValue('C6', 'PROJECT NAME');
$sheet->setCellValue('D6', 'PURCHASER NAME');
$sheet->setCellValue('E6', 'IC');
$sheet->setCellValue('F6', 'CONTACT');
$sheet->setCellValue('G6', 'E-MAIL');
$sheet->setCellValue('H6', 'BOOKING DATE');
$sheet->setCellValue('I6', 'SQ FT');
$sheet->setCellValue('J6', 'SPA PRICE');
$sheet->setCellValue('K6', 'PACKAGE');
$sheet->setCellValue('L6', 'DISCOUNT');
$sheet->setCellValue('M6', 'REBATE');
$sheet->setCellValue('N6', 'NETT PRICE');
$sheet->setCellValue('O6', 'TOTAL DEVELOPER COMMISSION');
$sheet->setCellValue('P6', 'AGENT');
$sheet->setCellValue('Q6', 'LOAN STATUS');
$sheet->setCellValue('R6', 'REMARK');
$sheet->setCellValue('S6', 'FORM COLLECTED');
$sheet->setCellValue('T6', 'PAYMENT METHOD');
$sheet->setCellValue('U6', 'LAWYER');
$sheet->setCellValue('V6', 'BANK APPROVED');
$sheet->setCellValue('W6', 'LOAN AMOUNT');
$sheet->setCellValue('X6', 'LO SIGNED DATE');
$sheet->setCellValue('Y6', 'LA SIGNED DATE');
$sheet->setCellValue('Z6', 'SPA SIGNED DATE');
$sheet->setCellValue('AA6', 'FULLSET COMPLETED');
$sheet->setCellValue('AB6', 'CASH BUYER');
$sheet->setCellValue('AC6', 'CANCELLED BOOKING');
$sheet->setCellValue('AD6', 'CASE STATUS');
$sheet->setCellValue('AE6', 'EVENT/PERSONAL');
$sheet->setCellValue('AF6', 'RATE');
$sheet->setCellValue('AG6', 'AGENT COMMISSION');
$sheet->setCellValue('AH6', 'UP1 NAME');
$sheet->setCellValue('AI6', 'UP2 NAME');
$sheet->setCellValue('AJ6', 'UP3 NAME');
$sheet->setCellValue('AK6', 'UP4 NAME');
$sheet->setCellValue('AL6', 'UP5 NAME');
$sheet->setCellValue('AM6', 'PL NAME');
$sheet->setCellValue('AN6', 'HOS NAME');
$sheet->setCellValue('AO6', 'LISTER NAME');
$sheet->setCellValue('AP6', 'TOTAL CLAIMED DEV AMT');
$sheet->setCellValue('AQ6', 'TOTAL BALANCED DEV AMT');
$sheet->setCellValue('AR6', 'DATE MODIFIED');

if ($loanDetails) {
  for ($i=0; $i <count($loanDetails) ; $i++) {
    $cell++;

    if ($loanDetails[$i]->getBookingDate()) {
      $dateCreated = date('d/m/Y',strtotime($loanDetails[$i]->getBookingDate()));
    }else {
      $dateCreated = "";
    }
    if ($loanDetails[$i]->getLoSignedDate()) {
      $loDate = date('d/m/Y',strtotime($loanDetails[$i]->getLoSignedDate()));
    }else {
      $loDate = "";
    }
    if ($loanDetails[$i]->getLaSignedDate()) {
      $laDate = date('d/m/Y',strtotime($loanDetails[$i]->getLaSignedDate()));
    }else {
      $laDate = "";
    }
    if ($loanDetails[$i]->getSpaSignedDate()) {
      $spaDate = date('d/m/Y',strtotime($loanDetails[$i]->getSpaSignedDate()));
    }else {
      $spaDate = "";
    }if ($loanDetails[$i]->getDateUpdated()) {
      $dateModified = date('d/m/Y',strtotime($loanDetails[$i]->getDateUpdated()));
    }else {
      $dateModified = "";
    }
    if ($loanDetails[$i]->getTotalBalUnclaimAmt()) {
      $unclaimed = number_format($loanDetails[$i]->getTotalBalUnclaimAmt(),2);
    }else {
      $unclaimed = 0;
    }
    if ($loanDetails[$i]->getTotalClaimDevAmt()) {
      $claimed = number_format($loanDetails[$i]->getTotalClaimDevAmt(),2);
    }else {
      $claimed = 0;
    }
    if ($loanDetails[$i]->getAgentComm()) {
      $agentComm = number_format($loanDetails[$i]->getAgentComm(),2);
    }else {
      $agentComm = 0;
    }
    if ($loanDetails[$i]->getLoanAmount()) {
      $loanAmount = number_format($loanDetails[$i]->getLoanAmount(),2);
    }else {
      $loanAmount = 0;
    }
    if ($loanDetails[$i]->getTotalDeveloperComm()) {
      $totalDevComm = number_format($loanDetails[$i]->getTotalDeveloperComm(),2);
    }else {
      $totalDevComm = 0;
    }
    if ($loanDetails[$i]->getNettPrice()) {
      $nettPrice = number_format($loanDetails[$i]->getNettPrice(),2);
    }else {
      $nettPrice = 0;
    }
    if ($loanDetails[$i]->getSpaPrice()) {
      $spaPrice = number_format($loanDetails[$i]->getSpaPrice(),2);
    }else {
      $spaPrice = 0;
    }
    if ($loanDetails[$i]->getSqFt()) {
      $sqFt = number_format($loanDetails[$i]->getSqFt(),2);
    }else {
      $sqFt = 0;
    }
    $sheet->setCellValue('A2', 'Name :');
    $sheet->mergeCells("A2:B2");
    $sheet->setCellValue('C2', $fullName);
    $sheet->mergeCells("C2:E2");
    $sheet->setCellValue('A3', 'Position :');
    $sheet->mergeCells("A3:B3");
    $sheet->setCellValue('C3', $userDetails[0]->getPosition());
    $sheet->mergeCells("C3:E3");
    $sheet->setCellValue('A'.$cell, $i+1);
    $sheet->setCellValue('B'.$cell, str_replace(",","\n",$loanDetails[$i]->getUnitNo()));
    $sheet->setCellValue('C'.$cell, $loanDetails[$i]->getProjectName());
    $sheet->setCellValue('D'.$cell, $loanDetails[$i]->getPurchaserName());
    $sheet->setCellValue('E'.$cell, $loanDetails[$i]->getIc());
    $sheet->setCellValue('F'.$cell, $loanDetails[$i]->getContact());
    $sheet->setCellValue('G'.$cell, $loanDetails[$i]->getEmail());
    $sheet->setCellValue('H'.$cell, $dateCreated);
    $sheet->setCellValue('I'.$cell, $sqFt);
    $sheet->setCellValue('J'.$cell, $spaPrice);
    $sheet->setCellValue('K'.$cell, $loanDetails[$i]->getPackage());
    $sheet->setCellValue('L'.$cell, $loanDetails[$i]->getDiscount());
    $sheet->setCellValue('M'.$cell, $loanDetails[$i]->getRebate());
    $sheet->setCellValue('N'.$cell, $nettPrice);
    $sheet->setCellValue('O'.$cell, $totalDevComm);
    $sheet->setCellValue('P'.$cell, $loanDetails[$i]->getAgent());
    $sheet->setCellValue('Q'.$cell, $loanDetails[$i]->getLoanStatus());
    $sheet->setCellValue('R'.$cell, $loanDetails[$i]->getRemark());
    $sheet->setCellValue('S'.$cell, $loanDetails[$i]->getBFormCollected());
    $sheet->setCellValue('T'.$cell, $loanDetails[$i]->getPaymentMethod());
    $sheet->setCellValue('U'.$cell, $loanDetails[$i]->getLawyer());
    $sheet->setCellValue('V'.$cell, $loanDetails[$i]->getBankApproved());
    $sheet->setCellValue('W'.$cell, $loanAmount);
    $sheet->setCellValue('X'.$cell, $loDate);
    $sheet->setCellValue('Y'.$cell, $laDate);
    $sheet->setCellValue('Z'.$cell, $spaDate);
    $sheet->setCellValue('AA'.$cell, $loanDetails[$i]->getFullsetCompleted());
    $sheet->setCellValue('AB'.$cell, $loanDetails[$i]->getCashBuyer());
    $sheet->setCellValue('AC'.$cell, $loanDetails[$i]->getCancelledBooking());
    $sheet->setCellValue('AD'.$cell, $loanDetails[$i]->getCaseStatus());
    $sheet->setCellValue('AE'.$cell, $loanDetails[$i]->getEventPersonal());
    $sheet->setCellValue('AF'.$cell, $loanDetails[$i]->getRate());
    $sheet->setCellValue('AG'.$cell, $agentComm);
    $sheet->setCellValue('AH'.$cell, $loanDetails[$i]->getUpline1());
    $sheet->setCellValue('AI'.$cell, $loanDetails[$i]->getUpline2());
    $sheet->setCellValue('AJ'.$cell, $loanDetails[$i]->getUpline3());
    $sheet->setCellValue('AK'.$cell, $loanDetails[$i]->getUpline4());
    $sheet->setCellValue('AL'.$cell, $loanDetails[$i]->getUpline5());
    $sheet->setCellValue('AM'.$cell, $loanDetails[$i]->getPlName());
    $sheet->setCellValue('AN'.$cell, $loanDetails[$i]->getHosName());
    $sheet->setCellValue('AO'.$cell, $loanDetails[$i]->getListerName());
    $sheet->setCellValue('AP'.$cell, $claimed);
    $sheet->setCellValue('AQ'.$cell, $unclaimed);
    $sheet->setCellValue('AR'.$cell, $dateModified);
  }
}

$styleArray = [
    'borders' => [
      'allBorders' => [
       'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
       'color' => ['argb' => '000000'],
     ],
  ],
    'alignment' => [
      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
      'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
    ],
];

// border and center
$spreadsheet->getActiveSheet()->getStyle('A6:AR'.$cell.'')->applyFromArray($styleArray);

$range = range('A','Z');

foreach ($range as $ranges) {
  $spreadsheet->getActiveSheet()->getColumnDimension($ranges)->setAutoSize(true);
}

$rangeCont = range('A','R');

foreach ($range as $ranges) {
  $spreadsheet->getActiveSheet()->getColumnDimension('A'.$ranges)->setAutoSize(true);
}

$spreadsheet->getActiveSheet()->getStyle('A6:AR'.$cell.'')
    ->getAlignment()->setWrapText(true);


//set the header first, so the result will be treated as an xlsx file.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

//make it an attachment so we can define filename
$date = date('d-m-Y');
header('Content-Disposition: attachment;filename="Loan Status '.$_SESSION['username']." ".$date.'.xlsx"');

//create IOFactory object
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//save into php output
$writer->save('php://output');
