<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/ProformaInvoice.php';
require_once dirname(__FILE__) . '/../classes/Commission.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/../utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/languageFunction.php';

$conn = connDB();
$cell = 1;
$finalAmount = 0;
$proformaInvoiceDetails = getProformaInvoice($conn);
//call the autoload
require '../vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;

//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();

//color th
$spreadsheet->getActiveSheet()->getStyle('A1:K1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');

    $styleArray = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'borders' => [
          'allBorders' => [
           'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
           'color' => ['argb' => '000000'],
         ],
      ],
    ];

//border line and center
    $spreadsheet->getActiveSheet()->getStyle('A1:K1')->applyFromArray($styleArray);

//set the value of cell a1 to "Hello World!"
// $sheet->setCellValue('A1', 'Hello World !');
$sheet->setCellValue('A1', 'NO.');
$sheet->setCellValue('B1', 'PROJECT NAME');
$sheet->setCellValue('C1', 'UNIT NO.');
$sheet->setCellValue('D1', 'DATE');
$sheet->setCellValue('E1', 'INVOICE NO.');
$sheet->setCellValue('F1', 'AMOUNT (RM)');
$sheet->setCellValue('G1', 'TOTAL AMOUNT (RM)');
$sheet->setCellValue('H1', 'SST');
$sheet->setCellValue('I1', 'STATUS');
$sheet->setCellValue('J1', 'RECEIVED DATE');
$sheet->setCellValue('K1', 'CHECK NO.');

if ($proformaInvoiceDetails) {
  for ($i=0; $i <count($proformaInvoiceDetails) ; $i++) {
    $cell++;

    if ($proformaInvoiceDetails[$i]->getBookingDate()) {
      $dateCreated = date('d/m/Y',strtotime($proformaInvoiceDetails[$i]->getBookingDate()));
    }else {
      $dateCreated = "";
    }
    $amountExp = explode(",",$proformaInvoiceDetails[$i]->getFinalAmount());
    for ($h=0; $h <count($amountExp) ; $h++) {
      $finalAmount += $amountExp[$h];
    }
    if ($finalAmount) {
      $finalAmount = number_format($finalAmount,2);
    }
    if ($proformaInvoiceDetails[$i]->getReceiveDate()) {
      $receiveDate = date('d/m/Y',strtotime($proformaInvoiceDetails[$i]->getReceiveDate()));
    }else{
    $receiveDate = "";
    }
    if ($proformaInvoiceDetails[$i]->getCheckID()) {
      $status = "COMPLETED";
    }else {
        $status = "PENDING";
      }
    $sheet->setCellValue('A'.$cell, $i+1);
    $sheet->setCellValue('B'.$cell, $proformaInvoiceDetails[$i]->getProjectName());
    $sheet->setCellValue('C'.$cell, str_replace(",","\n",$proformaInvoiceDetails[$i]->getUnitNo()));
    $sheet->setCellValue('D'.$cell, $dateCreated);
    $sheet->setCellValue('E'.$cell, date('ymd', strtotime($proformaInvoiceDetails[$i]->getDateCreated())).$proformaInvoiceDetails[$i]->getID());

    if ($amountExp) {
      for ($j=0; $j <count($amountExp) ; $j++) {
        $newAmount[] = number_format($amountExp[$j],2);
      }
      $newAmountImp = implode(",,",$newAmount);
      $sheet->setCellValue('F'.$cell, str_replace(",,","\n",$newAmountImp));
    }else {
      $sheet->setCellValue('F'.$cell, 0);
    }
    $sheet->setCellValue('G'.$cell, $finalAmount);
    $sheet->setCellValue('H'.$cell, $proformaInvoiceDetails[$i]->getCharges());
    $sheet->setCellValue('I'.$cell, $status);
    $sheet->setCellValue('J'.$cell, $receiveDate);
    $sheet->setCellValue('K'.$cell, $proformaInvoiceDetails[$i]->getCheckID());
  }
}

$styleArray = [
    'borders' => [
      'allBorders' => [
       'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
       'color' => ['argb' => '000000'],
     ],
  ],
    'alignment' => [
      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
      'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
    ],
];

// border and center
$spreadsheet->getActiveSheet()->getStyle('A2:K'.$cell.'')->applyFromArray($styleArray);

$range = range('A','K');

foreach ($range as $ranges) {
  $spreadsheet->getActiveSheet()->getColumnDimension($ranges)->setAutoSize(true);
}

$spreadsheet->getActiveSheet()->getStyle('A2:K'.$cell.'')
    ->getAlignment()->setWrapText(true);


//set the header first, so the result will be treated as an xlsx file.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

//make it an attachment so we can define filename
$date = date('d-m-Y');
header('Content-Disposition: attachment;filename="Invoice Record Proforma '.$date.'.xlsx"');

//create IOFactory object
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//save into php output
$writer->save('php://output');
