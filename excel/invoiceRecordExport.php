<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Invoice.php';
// require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/../utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/languageFunction.php';

$conn = connDB();
$cell = 1;
$loanDetails = getLoanStatus($conn,"WHERE display = 'Yes' and cancelled_booking != 'YES' ");
//call the autoload
require '../vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;

//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();

$spreadsheet->getActiveSheet()->getStyle('A1:AL1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');

    $styleArray = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'borders' => [
          'allBorders' => [
           'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
           'color' => ['argb' => '000000'],
         ],
      ],
    ];

    $spreadsheet->getActiveSheet()->getStyle('A1:AL1')->applyFromArray($styleArray);

//set the value of cell a1 to "Hello World!"
// $sheet->setCellValue('A1', 'Hello World !');

$sheet->setCellValue('A1', 'NO.');
$sheet->setCellValue('B1', 'PROJECT NAME');
$sheet->setCellValue('C1', 'UNIT NO.');
$sheet->setCellValue('D1', 'DATE');
$sheet->setCellValue('E1', 'INVOICE NO');
$sheet->setCellValue('F1', 'AMOUNT (RM)');
$sheet->setCellValue('G1', 'SST');
$sheet->setCellValue('H1', '1ST STATUS');
$sheet->setCellValue('I1', 'RECEIVED DATE');
$sheet->setCellValue('J1', 'CHECK NO');
$sheet->setCellValue('K1', 'DATE');
$sheet->setCellValue('L1', 'INVOICE NO');
$sheet->setCellValue('M1', 'AMOUNT (RM)');
$sheet->setCellValue('N1', 'SST');
$sheet->setCellValue('O1', '2ND STATUS');
$sheet->setCellValue('P1', 'RECEIVED DATE');
$sheet->setCellValue('Q1', 'CHECK NO');
$sheet->setCellValue('R1', 'DATE');
$sheet->setCellValue('S1', 'INVOICE NO');
$sheet->setCellValue('T1', 'AMOUNT (RM)');
$sheet->setCellValue('U1', 'SST');
$sheet->setCellValue('V1', '3RD STATUS');
$sheet->setCellValue('W1', 'RECEIVED DATE');
$sheet->setCellValue('X1', 'CHECK NO');
$sheet->setCellValue('Y1', 'DATE');
$sheet->setCellValue('Z1', 'INVOICE NO');
$sheet->setCellValue('AA1', 'AMOUNT (RM)');
$sheet->setCellValue('AB1', 'SST');
$sheet->setCellValue('AC1', '4TH STATUS');
$sheet->setCellValue('AD1', 'RECEIVED DATE');
$sheet->setCellValue('AE1', 'CHECK NO');
$sheet->setCellValue('AF1', 'DATE');
$sheet->setCellValue('AG1', 'INVOICE NO');
$sheet->setCellValue('AH1', 'AMOUNT (RM)');
$sheet->setCellValue('AI1', 'SST');
$sheet->setCellValue('AJ1', '5TH STATUS');
$sheet->setCellValue('AK1', 'RECEIVED DATE');
$sheet->setCellValue('AL1', 'CHECK NO');

if ($loanDetails) {
  for ($i=0; $i <count($loanDetails) ; $i++) {
    $cell++;

    if ($loanDetails[$i]->getClaimAmt1st()) {
      $claim1st = number_format($loanDetails[$i]->getClaimAmt1st(),2);
    }else {
      $claim1st="";
    }
    if ($loanDetails[$i]->getClaimAmt2nd()) {
      $claim2nd = number_format($loanDetails[$i]->getClaimAmt2nd(),2);
    }else {
      $claim2nd="";
    }
    if ($loanDetails[$i]->getClaimAmt3rd()) {
      $claim3rd = number_format($loanDetails[$i]->getClaimAmt3rd(),2);
    }else {
      $claim3rd="";
    }
    if ($loanDetails[$i]->getClaimAmt4th()) {
      $claim4th = number_format($loanDetails[$i]->getClaimAmt4th(),2);
    }else {
      $claim4th="";
    }
    if ($loanDetails[$i]->getClaimAmt5th()) {
      $claim5th = number_format($loanDetails[$i]->getClaimAmt5th(),2);
    }else {
      $claim5th="";
    }
    if ($loanDetails[$i]->getRequestDate1()) {
      $requestDate1 = date('d/m/Y',strtotime($loanDetails[$i]->getRequestDate1()));
      $invoiceNo1 = date('ymd',strtotime($loanDetails[$i]->getRequestDate1()))."1".$loanDetails[$i]->getId();
    }else {
      $requestDate1 = "";
      $invoiceNo1 = "";
    }
    if ($loanDetails[$i]->getReceiveDate1()) {
      $receiveDate1 = date('d/m/Y',strtotime($loanDetails[$i]->getReceiveDate1()));
    }
    else {
      $receiveDate1 = "";
    }
    if ($loanDetails[$i]->getRequestDate2()) {
      $requestDate2 = date('d/m/Y',strtotime($loanDetails[$i]->getRequestDate2()));
      $invoiceNo2 = date('ymd',strtotime($loanDetails[$i]->getRequestDate2()))."2".$loanDetails[$i]->getId();
    }
    else {
      $requestDate2 = "";
      $invoiceNo2 = "";
    }
    if ($loanDetails[$i]->getReceiveDate2()) {
      $receiveDate2 = date('d/m/Y',strtotime($loanDetails[$i]->getReceiveDate2()));
    }
    else {
      $receiveDate2 = "";
    }
    if ($loanDetails[$i]->getRequestDate3()) {
      $requestDate3 = date('d/m/Y',strtotime($loanDetails[$i]->getRequestDate3()));
      $invoiceNo3 = date('ymd',strtotime($loanDetails[$i]->getRequestDate3()))."3".$loanDetails[$i]->getId();
    }else {
      $requestDate3 = "";
      $invoiceNo3 = "";
    }
    if ($loanDetails[$i]->getReceiveDate3()) {
      $receiveDate3 = date('d/m/Y',strtotime($loanDetails[$i]->getReceiveDate3()));
    }
    else {
      $receiveDate3 = "";
    }
    if ($loanDetails[$i]->getRequestDate4()) {
      $requestDate4 = date('d/m/Y',strtotime($loanDetails[$i]->getRequestDate4()));
      $invoiceNo4 = date('ymd',strtotime($loanDetails[$i]->getRequestDate4()))."4".$loanDetails[$i]->getId();
    }else {
      $requestDate4 = "";
      $invoiceNo4 = "";
    }
    if ($loanDetails[$i]->getReceiveDate4()) {
      $receiveDate4 = date('d/m/Y',strtotime($loanDetails[$i]->getReceiveDate4()));
    }else {
      $receiveDate4 = "";
    }
    if ($loanDetails[$i]->getRequestDate5()) {
      $requestDate5 = date('d/m/Y',strtotime($loanDetails[$i]->getRequestDate5()));
      $invoiceNo5 = date('ymd',strtotime($loanDetails[$i]->getRequestDate5()))."5".$loanDetails[$i]->getId();
    }else {
      $requestDate5 = "";
      $invoiceNo5 = "";
    }
    if ($loanDetails[$i]->getReceiveDate5()) {
      $receiveDate5 = date('d/m/Y',strtotime($loanDetails[$i]->getReceiveDate5()));
    }else {
      $receiveDate5 = "";
    }
    if ($loanDetails[$i]->getCheckNo1()) {
      $status1 = "COMPLETED";
    }else {
      $status1 = "PENDING";
    }
    if ($loanDetails[$i]->getCheckNo2()) {
      $status2 = "COMPLETED";
    }else {
      $status2 = "PENDING";
    }
    if ($loanDetails[$i]->getCheckNo3()) {
      $status3 = "COMPLETED";
    }else {
      $status3 = "PENDING";
    }
    if ($loanDetails[$i]->getCheckNo4()) {
      $status4 = "COMPLETED";
    }else {
      $status4 = "PENDING";
    }
    if ($loanDetails[$i]->getCheckNo5()) {
      $status5 = "COMPLETED";
    }else {
      $status5 = "PENDING";
    }

    $sheet->setCellValue('A'.$cell, $i+1);
    $sheet->setCellValue('B'.$cell, $loanDetails[$i]->getProjectName());
    $sheet->setCellValue('C'.$cell, $loanDetails[$i]->getUnitNo());
    $sheet->setCellValue('D'.$cell, $requestDate1);
    $sheet->setCellValue('E'.$cell, $invoiceNo1);
    $sheet->setCellValue('F'.$cell, $claim1st);
    $sheet->setCellValue('G'.$cell, $loanDetails[$i]->getSst1());
    $sheet->setCellValue('H'.$cell, $status1);
    $sheet->setCellValue('I'.$cell, $receiveDate1);
    $sheet->setCellValue('J'.$cell, $loanDetails[$i]->getCheckNo1());
    $sheet->setCellValue('K'.$cell, $requestDate2);
    $sheet->setCellValue('L'.$cell, $invoiceNo2);
    $sheet->setCellValue('M'.$cell, $claim2nd);
    $sheet->setCellValue('N'.$cell, $loanDetails[$i]->getSst2());
    $sheet->setCellValue('O'.$cell, $status2);
    $sheet->setCellValue('P'.$cell, $receiveDate2);
    $sheet->setCellValue('Q'.$cell, $loanDetails[$i]->getCheckNo2());
    $sheet->setCellValue('R'.$cell, $requestDate3);
    $sheet->setCellValue('S'.$cell, $invoiceNo3);
    $sheet->setCellValue('T'.$cell, $claim3rd);
    $sheet->setCellValue('U'.$cell, $loanDetails[$i]->getSst3());
    $sheet->setCellValue('V'.$cell, $status3);
    $sheet->setCellValue('W'.$cell, $receiveDate3);
    $sheet->setCellValue('X'.$cell, $loanDetails[$i]->getCheckNo3());
    $sheet->setCellValue('Y'.$cell, $requestDate4);
    $sheet->setCellValue('Z'.$cell, $invoiceNo4);
    $sheet->setCellValue('AA'.$cell, $claim4th);
    $sheet->setCellValue('AB'.$cell, $loanDetails[$i]->getSst4());
    $sheet->setCellValue('AC'.$cell, $status4);
    $sheet->setCellValue('AD'.$cell, $receiveDate4);
    $sheet->setCellValue('AE'.$cell, $loanDetails[$i]->getCheckNo4());
    $sheet->setCellValue('AF'.$cell, $requestDate5);
    $sheet->setCellValue('AG'.$cell, $invoiceNo5);
    $sheet->setCellValue('AH'.$cell, $claim5th);
    $sheet->setCellValue('AI'.$cell, $loanDetails[$i]->getSst5());
    $sheet->setCellValue('AJ'.$cell, $status5);
    $sheet->setCellValue('AK'.$cell, $receiveDate5);
    $sheet->setCellValue('AL'.$cell, $loanDetails[$i]->getCheckNo5());
  }
}

$styleArray = [
    'borders' => [
      'allBorders' => [
       'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
       'color' => ['argb' => '000000'],
     ],
  ],
    'alignment' => [
      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
      'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
    ],
];

$spreadsheet->getActiveSheet()->getStyle('A2:AL'.$cell.'')->applyFromArray($styleArray);

$range = range('A','Z');

foreach ($range as $ranges) {
  $spreadsheet->getActiveSheet()->getColumnDimension($ranges)->setAutoSize(true);
}

$rangeCont = range('A','L');

foreach ($range as $ranges) {
  $spreadsheet->getActiveSheet()->getColumnDimension('A'.$ranges)->setAutoSize(true);
}

$spreadsheet->getActiveSheet()->getStyle('A2:AL'.$cell.'')
    ->getAlignment()->setWrapText(true);


// set the header first, so the result will be treated as an xlsx file.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

//make it an attachment so we can define filename
$date = date('d-m-Y');
header('Content-Disposition: attachment;filename=" Single Invoice Record '.$date.'.xlsx"');

//create IOFactory object
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//save into php output
$writer->save('php://output');
