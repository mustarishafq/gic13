<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Project.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/../utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/languageFunction.php';
// require_once dirname(__FILE__) . '/../vendor/autoload.php';

$fullName = $_SESSION['fullname'];

$conn = connDB();
$cell = 6;
$a = 0;
$adv = 0;
$comm1 = 0;
$comm2 = 0;
$comm3 = 0;
$comm4 = 0;
$comm5 = 0;
$userRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($fullName),"s");
$userDetails = $userRows[0];

//call the autoload
require '../vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;

//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();

//color th
$spreadsheet->getActiveSheet()->getStyle('A6:Q6')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');
$spreadsheet->getActiveSheet()->getStyle('A2:B2')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');
$spreadsheet->getActiveSheet()->getStyle('A3:B3')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');

    $styleArray = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'borders' => [
          'allBorders' => [
           'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
           'color' => ['argb' => '000000'],
         ],
      ],
    ];

//border line and center
    $spreadsheet->getActiveSheet()->getStyle('A6:Q6')->applyFromArray($styleArray);
    $spreadsheet->getActiveSheet()->getStyle('A2:E2')->applyFromArray($styleArray);
    $spreadsheet->getActiveSheet()->getStyle('A3:E3')->applyFromArray($styleArray);

//set the value of cell a1 to "Hello World!"
// $sheet->setCellValue('A1', 'Hello World !');
$sheet->setCellValue('A6', 'NO.');
$sheet->setCellValue('B6', 'UNIT NO.');
$sheet->setCellValue('C6', 'PROJECT NAME');
$sheet->setCellValue('D6', 'TOTAL COMMISSION');
$sheet->setCellValue('E6', '1ST CLAIM COMMISSION (RM)');
$sheet->setCellValue('F6', 'PAYSLIP DATE');
$sheet->setCellValue('G6', '2ND CLAIM COMMISSION (RM)');
$sheet->setCellValue('H6', 'PAYSLIP DATE');
$sheet->setCellValue('I6', '3RD CLAIM COMMISSION (RM)');
$sheet->setCellValue('J6', 'PAYSLIP DATE');
$sheet->setCellValue('K6', '4TH CLAIM COMMISSION (RM)');
$sheet->setCellValue('L6', 'PAYSLIP DATE');
$sheet->setCellValue('M6', '5TH CLAIM COMMISSION (RM)');
$sheet->setCellValue('N6', 'PAYSLIP DATE');
$sheet->setCellValue('O6', '6TH CLAIM COMMISSION (RM)');
$sheet->setCellValue('P6', 'PAYSLIP DATE');
$sheet->setCellValue('Q6', 'UNCLAIMED BALANCE');

$orderDetails = getLoanStatus($conn, "WHERE agent = ? and display = 'Yes' and cancelled_booking != 'YES' ",array("agent"),array($fullName), "s");
if($orderDetails != null)
{
    for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
    {
      $cell++;
      $advancedDetails = getAdvancedSlip($conn, "WHERE status = 'COMPLETED' AND loan_uid = ?", array("loan_uid"), array($orderDetails[$cntAA]->getLoanUid()), "s");

      $sheet->setCellValue('A2', 'Name :');
      $sheet->mergeCells("A2:B2");
      $sheet->setCellValue('C2', $fullName);
      $sheet->mergeCells("C2:E2");
      $sheet->setCellValue('A3', 'Position :');
      $sheet->mergeCells("A3:B3");
      $sheet->setCellValue('C3', $userDetails[0]->getPosition());
      $sheet->mergeCells("C3:E3");
      $sheet->setCellValue('A'.$cell, $cntAA+1);
      $sheet->setCellValue('B'.$cell, $orderDetails[$cntAA]->getUnitNo());
      $sheet->setCellValue('C'.$cell, $orderDetails[$cntAA]->getProjectName());
      $sheet->setCellValue('D'.$cell, number_format($orderDetails[$cntAA]->getAgentComm(), 2));
      $sheet->setCellValue('E'.$cell, number_format($orderDetails[$cntAA]->getAmount(), 2));
      $sheet->setCellValue('F'.$cell, date('d-m-Y', strtotime($advancedDetails[0]->getDateUpdated())));

          if ($advancedDetails) {
            $commissionDetails = getCommission($conn, "WHERE loan_uid = ? AND upline_default = ? AND details = '2nd Part Commission' AND receive_status = 'COMPLETED'", array("loan_uid","upline_default"), array($advancedDetails[0]->getLoanUid(),$advancedDetails[0]->getAgentDefault()), "ss");
          }
      if ($commissionDetails) {
        if ($commissionDetails[0]->getClaimType() == 'Multi') {
          $claimId = $commissionDetails[0]->getClaimID();
          $commissionDetails = getCommission($conn, "WHERE claim_id = ? AND unit_no =?", array("claim_id","unit_no"), array($claimId,$orderDetails[$cntAA]->getUnitNo()), "ss");
        }

        unset($commission2nd); // remove previous array file

        for ($i=0; $i <count($commissionDetails) ; $i++) {
          $commission2nd[] = number_format($commissionDetails[$i]->getCommission(),2);
        }
        $commission2ndImp = implode(",,",$commission2nd);

        $sheet->setCellValue('G'.$cell, str_replace(",,","<br>",$commission2ndImp));
        $sheet->setCellValue('H'.$cell, date('d-m-Y', strtotime($commissionDetails[0]->getDateCreated())));
      }
         if ($advancedDetails) {
           $commissionDetails = getCommission($conn, "WHERE loan_uid = ? AND upline_default = ? AND details = '3rd Part Commission' AND receive_status = 'COMPLETED'", array("loan_uid","upline_default"), array($advancedDetails[0]->getLoanUid(),$advancedDetails[0]->getAgentDefault()), "ss");
         }
         if ($commissionDetails) {
           if ($commissionDetails[0]->getClaimType() == 'Multi') {
             $claimId = $commissionDetails[0]->getClaimID();
             $commissionDetails = getCommission($conn, "WHERE claim_id = ? AND unit_no =?", array("claim_id","unit_no"), array($claimId,$orderDetails[$cntAA]->getUnitNo()), "ss");
           }

           unset($commission3rd); // remove previous array file
       for ($i=0; $i <count($commissionDetails) ; $i++) {
         $commission3rd[] = number_format($commissionDetails[$i]->getCommission(),2);
         $commission3rdImp = implode(",,",$commission3rd);
       }

       $sheet->setCellValue('I'.$cell, str_replace(",,","<br>",$commission3rdImp));
       $sheet->setCellValue('J'.$cell, date('d-m-Y', strtotime($commissionDetails[0]->getDateCreated())));
     }
         if ($advancedDetails) {
           $commissionDetails = getCommission($conn, "WHERE loan_uid = ? AND upline_default = ? AND details = '4th Part Commission' AND receive_status = 'COMPLETED'", array("loan_uid","upline_default"), array($advancedDetails[0]->getLoanUid(),$advancedDetails[0]->getAgentDefault()), "ss");
         }
         if ($commissionDetails) {
           if ($commissionDetails[0]->getClaimType() == 'Multi') {
             $claimId = $commissionDetails[0]->getClaimID();
             $commissionDetails = getCommission($conn, "WHERE claim_id = ? AND unit_no =?", array("claim_id","unit_no"), array($claimId,$orderDetails[$cntAA]->getUnitNo()), "ss");
           }

           unset($commission4th); // remove previous array file
       for ($i=0; $i <count($commissionDetails) ; $i++) {
         $commission4th[] = number_format($commissionDetails[$i]->getCommission(),2);
         $commission4thImp = implode(",,",$commission4th);
       }

      $sheet->setCellValue('K'.$cell, str_replace(",,","<br>",$commission4thImp));
      $sheet->setCellValue('L'.$cell, date('d-m-Y', strtotime($commissionDetails[0]->getDateCreated())));
    }
       if ($advancedDetails) {
         $commissionDetails = getCommission($conn, "WHERE loan_uid = ? AND upline_default = ? AND details = '5th Part Commission' AND receive_status = 'COMPLETED'", array("loan_uid","upline_default"), array($advancedDetails[0]->getLoanUid(),$advancedDetails[0]->getAgentDefault()), "ss");
       }
       if ($commissionDetails) {
         if ($commissionDetails[0]->getClaimType() == 'Multi') {
           $claimId = $commissionDetails[0]->getClaimID();
           $commissionDetails = getCommission($conn, "WHERE claim_id = ? AND unit_no =?", array("claim_id","unit_no"), array($claimId,$orderDetails[$cntAA]->getUnitNo()), "ss");
         }

         unset($commission5th); // remove previous array file
     for ($i=0; $i <count($commissionDetails) ; $i++) {
       $commission5th[] = number_format($commissionDetails[$i]->getCommission(),2);
       $commission5thImp = implode(",,",$commission5th);
     }
    $sheet->setCellValue('M'.$cell, str_replace(",,","<br>",$commission5thImp));
    $sheet->setCellValue('N'.$cell, date('d-m-Y', strtotime($commissionDetails[0]->getDateCreated())));
  }
     if ($advancedDetails) {
       $commissionDetails = getCommission($conn, "WHERE loan_uid = ? AND upline_default = ? AND details = '6th Part Commission'", array("loan_uid","upline_default"), array($advancedDetails[0]->getLoanUid(),$advancedDetails[0]->getAgentDefault()), "ss");
     }
     if ($commissionDetails) {
       if ($commissionDetails[0]->getClaimType() == 'Multi') {
         $claimId = $commissionDetails[0]->getClaimID();
         $commissionDetails = getCommission($conn, "WHERE claim_id = ? AND unit_no =?", array("claim_id","unit_no"), array($claimId,$orderDetails[$cntAA]->getUnitNo()), "ss");
       }

       unset($commission6th); // remove previous array file
   for ($i=0; $i <count($commissionDetails) ; $i++) {
     $commission6th[] = number_format($commissionDetails[$i]->getCommission(),2);
     $commission6thImp = implode(",,",$commission6th);
   }
   $agentComm = explode(",",$orderDetails[$cntAA] ->getAgentBalance());

  $sheet->setCellValue('O'.$cell, str_replace(",,","<br>",$commission6thImp));
  $sheet->setCellValue('P'.$cell, date('d-m-Y', strtotime($commissionDetails[0]->getDateCreated())));
  $sheet->setCellValue('Q'.$cell, date('d-m-Y', number_format($agentComm[0],2)));
}

}

}else{
  $sheet->setCellValue('A2', 'Name :');
  $sheet->mergeCells("A2:B2");
  $sheet->setCellValue('C2', $fullName);
  $sheet->mergeCells("C2:E2");
  $sheet->setCellValue('A3', 'Position :');
  $sheet->mergeCells("A3:B3");
  $sheet->setCellValue('C3', $userDetails->getPosition());
  $sheet->mergeCells("C3:E3");
  $sheet->mergeCells("A7:Q7");
  $sheet->setCellValue('A7', "No Related Data");
  $styleQray = [
      'borders' => [
        'allBorders' => [
         'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
         'color' => ['Qgb' => '000000'],
       ],
    ],
      'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
      ],
  ];
  $spreadsheet->getActiveSheet()->getStyle('A7:Q7')->applyFromArray($styleArray);
}
$styleQray = [
    'borders' => [
      'allBorders' => [
       'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
       'color' => ['Qgb' => '000000'],
     ],
  ],
    'alignment' => [
      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
      'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
    ],
];

// border and center
$spreadsheet->getActiveSheet()->getStyle('A6:Q'.$cell.'')->applyFromArray($styleArray);

$range = range('A','Q');

foreach ($range as $ranges) {
  $spreadsheet->getActiveSheet()->getColumnDimension($ranges)->setAutoSize(true);
}

// $rangeCont = range('A','R');
//
// foreach ($range as $ranges) {
//   $spreadsheet->getActiveSheet()->getColumnDimension('A'.$ranges)->setAutoSize(true);
// }

$spreadsheet->getActiveSheet()->getStyle('A6:Q'.$cell.'')
    ->getAlignment()->setWrapText(true);


//set the header first, so the result will be treated as an xlsx file.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

//make it an attachment so we can define filename
$date = date('d-m-Y');
header('Content-Disposition: attachment;filename="Loan Status '.$_SESSION['username']." ".$date.'.xlsx"');

//create IOFactory object
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//save into php output
$writer->save('php://output');
