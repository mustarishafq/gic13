<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Project.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/../utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/languageFunction.php';
// require_once dirname(__FILE__) . '/../vendor/autoload.php';

$fullName = $_SESSION['fullname'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE full_name =?",array("full_name"),array($fullName), "s");
$uid = $userDetails[0]->getUid();

$getWho = getWholeDownlineTree($conn, $uid, false);
$no = 0;
$cell = 6;

//call the autoload
require '../vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;

//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();

//color th
$spreadsheet->getActiveSheet()->getStyle('A6:I6')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');
$spreadsheet->getActiveSheet()->getStyle('A2:B2')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');
$spreadsheet->getActiveSheet()->getStyle('A3:B3')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');

    $styleArray = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'borders' => [
          'allBorders' => [
           'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
           'color' => ['argb' => '000000'],
         ],
      ],
    ];

//border line and center
    $spreadsheet->getActiveSheet()->getStyle('A6:I6')->applyFromArray($styleArray);
    $spreadsheet->getActiveSheet()->getStyle('A2:E2')->applyFromArray($styleArray);
    $spreadsheet->getActiveSheet()->getStyle('A3:E3')->applyFromArray($styleArray);

//set the value of cell a1 to "Hello World!"
// $sheet->setCellValue('A1', 'Hello World !');
$sheet->setCellValue('A6', 'NO.');
$sheet->setCellValue('C6', 'PROJECT NAME');
$sheet->setCellValue('B6', 'UNIT NO.');
$sheet->setCellValue('D6', 'BOOKING DATE');
$sheet->setCellValue('E6', 'NETT PRICE');
$sheet->setCellValue('F6', 'AGENT');
$sheet->setCellValue('G6', 'LO SIGNED DATE');
$sheet->setCellValue('H6', 'SPA SIGNED DATE');
$sheet->setCellValue('I6', 'OVERRIDE COMMISSION (RM)');

if ($getWho) {
  for ($i=0; $i <count($getWho) ; $i++) {
    $loanDetails = getLoanStatus($conn, "WHERE agent =?",array("agent"),array($getWho[$i]->getReferralName()), "s");
    if ($loanDetails) {
      $no += 1;
      $cell++;

      if ($loanDetails[0]->getBookingDate()) {
        $bookingDate = date('d/m/Y',strtotime($loanDetails[0]->getBookingDate()));
      }else {
        $bookingDate = "";
      }
      if ($loanDetails[0]->getLoSignedDate()) {
        $loDate = date('d/m/Y',strtotime($loanDetails[0]->getLoSignedDate()));
      }else {
        $loDate = "";
      }
      if ($loanDetails[0]->getSpaSignedDate()) {
        $spaDate = date('d/m/Y',strtotime($loanDetails[0]->getSpaSignedDate()));
      }else {
        $spaDate = "";
      }
      if ($loanDetails[0]->getAgentComm()) {
        $agentComm = number_format($loanDetails[0]->getAgentComm(),2);
      }else {
        $agentComm = 0;
      }
      if ($loanDetails[0]->getNettPrice()) {
        $nettPrice = number_format($loanDetails[0]->getNettPrice(),2);
      }else {
        $nettPrice = 0;
      }

      $sheet->setCellValue('A2', 'Name :');
      $sheet->mergeCells("A2:B2");
      $sheet->setCellValue('C2', $fullName);
      $sheet->mergeCells("C2:E2");
      $sheet->setCellValue('A3', 'Position :');
      $sheet->mergeCells("A3:B3");
      $sheet->setCellValue('C3', $userDetails[0]->getPosition());
      $sheet->mergeCells("C3:E3");
      $sheet->setCellValue('A'.$cell, $no);
      $sheet->setCellValue('B'.$cell, $loanDetails[0]->getProjectName());
      $sheet->setCellValue('C'.$cell, str_replace(",","\n",$loanDetails[0]->getUnitNo()));
      $sheet->setCellValue('D'.$cell, $bookingDate);
      $sheet->setCellValue('E'.$cell, $nettPrice);
      $sheet->setCellValue('F'.$cell, $loanDetails[0]->getAgent());
      $sheet->setCellValue('G'.$cell, $loDate);
      $sheet->setCellValue('H'.$cell, $spaDate);
      $sheet->setCellValue('I'.$cell, $agentComm);
    }
  }
}

$styleArray = [
    'borders' => [
      'allBorders' => [
       'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
       'color' => ['argb' => '000000'],
     ],
  ],
    'alignment' => [
      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
      'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
    ],
];

// border and center
$spreadsheet->getActiveSheet()->getStyle('A6:I'.$cell.'')->applyFromArray($styleArray);

$range = range('A','Z');

foreach ($range as $ranges) {
  $spreadsheet->getActiveSheet()->getColumnDimension($ranges)->setAutoSize(true);
}

// $rangeCont = range('A','R');
//
// foreach ($range as $ranges) {
//   $spreadsheet->getActiveSheet()->getColumnDimension('A'.$ranges)->setAutoSize(true);
// }

$spreadsheet->getActiveSheet()->getStyle('A6:I'.$cell.'')
    ->getAlignment()->setWrapText(true);


//set the header first, so the result will be treated as an xlsx file.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

//make it an attachment so we can define filename
$date = date('d-m-Y');
header('Content-Disposition: attachment;filename="Downline Performance '.$_SESSION['username']." ".$date.'.xlsx"');

//create IOFactory object
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//save into php output
$writer->save('php://output');
