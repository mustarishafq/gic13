<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Invoice.php';
require_once dirname(__FILE__) . '/../classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/../utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/languageFunction.php';

$conn = connDB();
$cell = 1;
$advancedDetails = getAdvancedSlip($conn,"WHERE status = 'COMPLETED'");
//call the autoload
require '../vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;

//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();

$spreadsheet->getActiveSheet()->getStyle('A1:G1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');

    $styleArray = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'borders' => [
          'allBorders' => [
           'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
           'color' => ['argb' => '000000'],
         ],
      ],
    ];

    $spreadsheet->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);

//set the value of cell a1 to "Hello World!"
// $sheet->setCellValue('A1', 'Hello World !');

$sheet->setCellValue('A1', 'NO.');
$sheet->setCellValue('B1', 'PROJECT NAME');
$sheet->setCellValue('C1', 'UNIT NO.');
$sheet->setCellValue('D1', 'AGENT NAME');
$sheet->setCellValue('E1', 'STATUS');
$sheet->setCellValue('F1', 'DATE');
$sheet->setCellValue('G1', 'TIME');

if ($advancedDetails) {
  for ($i=0; $i <count($advancedDetails) ; $i++) {
    $cell++;

    $sheet->setCellValue('A'.$cell, $i+1);
    $sheet->setCellValue('B'.$cell, $advancedDetails[$i]->getProjectName());
    $sheet->setCellValue('C'.$cell, $advancedDetails[$i]->getUnitNo());
    $sheet->setCellValue('D'.$cell, $advancedDetails[$i]->getAgent());
    $sheet->setCellValue('E'.$cell, $advancedDetails[$i]->getStatus());
    $sheet->setCellValue('F'.$cell, date('d/m/Y',strtotime($advancedDetails[$i]->getDateCreated())));
    $sheet->setCellValue('G'.$cell, date('h:i a',strtotime($advancedDetails[$i]->getDateCreated())));
  }
}

$styleArray = [
    'borders' => [
      'allBorders' => [
       'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
       'color' => ['argb' => '000000'],
     ],
  ],
    'alignment' => [
      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
      'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
    ],
];

$spreadsheet->getActiveSheet()->getStyle('A2:G'.$cell.'')->applyFromArray($styleArray);

$range = range('A','G');

foreach ($range as $ranges) {
  $spreadsheet->getActiveSheet()->getColumnDimension($ranges)->setAutoSize(true);
}

$spreadsheet->getActiveSheet()->getStyle('A2:G'.$cell.'')
    ->getAlignment()->setWrapText(true);


//set the header first, so the result will be treated as an xlsx file.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

//make it an attachment so we can define filename
$date = date('d-m-Y');
header('Content-Disposition: attachment;filename="Advance History '.$date.'.xlsx"');

//create IOFactory object
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//save into php output
$writer->save('php://output');
