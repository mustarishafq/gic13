<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Invoice.php';
// require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/../utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/languageFunction.php';

$conn = connDB();
$cell = 1;
$loanDetails = getLoanStatus($conn,"WHERE display = 'Yes' and cancelled_booking != 'YES' And agent =?",array("agent"),array($_SESSION['username']), "s");
//call the autoload
require '../vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;

//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();

$spreadsheet->getActiveSheet()->getStyle('A1:AG1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');

    $styleArray = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'borders' => [
          'allBorders' => [
           'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
           'color' => ['argb' => '000000'],
         ],
      ],
    ];

    $spreadsheet->getActiveSheet()->getStyle('A1:AG1')->applyFromArray($styleArray);

//set the value of cell a1 to "Hello World!"
// $sheet->setCellValue('A1', 'Hello World !');

$sheet->setCellValue('A1', 'NO.');
$sheet->setCellValue('B1', 'PROJECT NAME');
$sheet->setCellValue('C1', 'UNIT NO.');
$sheet->setCellValue('D1', 'PURCHASER NAME');
$sheet->setCellValue('E1', 'IC');
$sheet->setCellValue('F1', 'CONTACT');
$sheet->setCellValue('G1', 'E-MAIL');
$sheet->setCellValue('H1', 'BOOKING DATE');
$sheet->setCellValue('I1', 'SQ FT');
$sheet->setCellValue('J1', 'SPA PRICE');
$sheet->setCellValue('K1', 'PACKAGE');
$sheet->setCellValue('L1', 'DISCOUNT');
$sheet->setCellValue('M1', 'REBATE');
$sheet->setCellValue('N1', 'NETT PRICE');
$sheet->setCellValue('O1', 'TOTAL DEVELOPER COMMISSION');
$sheet->setCellValue('P1', 'AGENT');
$sheet->setCellValue('Q1', 'LOAN STATUS');
$sheet->setCellValue('R1', 'REMARK');
$sheet->setCellValue('S1', 'FORM COLLECTED');
$sheet->setCellValue('T1', 'PAYMENT METHOD');
$sheet->setCellValue('U1', 'LAWYER');
$sheet->setCellValue('V1', 'PENDING APPROVAL STATUS');
$sheet->setCellValue('W1', 'BANK APPROVED');
$sheet->setCellValue('X1', 'LO SIGNED DATE');
$sheet->setCellValue('Y1', 'LA SIGNED DATE');
$sheet->setCellValue('Z1', 'SPA SIGNED DATE');
$sheet->setCellValue('AA1', 'FULLSET COMPLETED');
$sheet->setCellValue('AB1', 'CASH BUYER');
$sheet->setCellValue('AC1', 'CANCELLED BOOKING');
$sheet->setCellValue('AD1', 'CASE STATUS');
$sheet->setCellValue('AE1', 'EVENT/PERSONAL');
$sheet->setCellValue('AF1', 'RATE');
$sheet->setCellValue('AG1', 'AGENT COMMISSION');

if ($loanDetails) {
  for ($i=0; $i <count($loanDetails) ; $i++) {
    $cell++;

    if ($loanDetails[$i]->getBookingDate()) {
      $bookingDate = date('d/m/Y',strtotime($loanDetails[$i]->getBookingDate()));
    }else {
      $bookingDate = "";
    }
    if ($loanDetails[$i]->getLoSignedDate()) {
      $loSignedDate = date('d/m/Y',strtotime($loanDetails[$i]->getLoSignedDate()));
    }else {
      $loSignedDate = "";
    }
    if ($loanDetails[$i]->getLaSignedDate()) {
      $laSignedDate = date('d/m/Y',strtotime($loanDetails[$i]->getLaSignedDate()));
    }else {
      $laSignedDate = "";
    }
    if ($loanDetails[$i]->getSpaSignedDate()) {
      $spaSignedDate = date('d/m/Y',strtotime($loanDetails[$i]->getSpaSignedDate()));
    }else {
      $spaSignedDate = "";
    }
    if ($loanDetails[$i]->getUnitNo()) {
      $unitNo = str_replace("&amp; ","\n",$loanDetails[$i]->getUnitNo());
    }
    if ($loanDetails[$i]->getPurchaserName()) {
      $purchaserName = str_replace(",","\n",$loanDetails[$i]->getPurchaserName());
    }
    if ($loanDetails[$i]->getIc()) {
      $ic = str_replace(",","\n",$loanDetails[$i]->getIc());
    }
    if ($loanDetails[$i]->getContact()) {
      $contact = str_replace(",","\n",$loanDetails[$i]->getContact());
    }
    if ($loanDetails[$i]->getEmail()) {
      $email = str_replace(",","\n",$loanDetails[$i]->getEmail());
    }

    $sheet->setCellValue('A'.$cell, $i+1);
    $sheet->setCellValue('B'.$cell, $loanDetails[$i]->getProjectName());
    $sheet->setCellValue('C'.$cell, $unitNo);
    $sheet->setCellValue('D'.$cell, $purchaserName);
    $sheet->setCellValue('E'.$cell, $ic);
    $sheet->setCellValue('F'.$cell, $contact);
    $sheet->setCellValue('G'.$cell, $email);
    $sheet->setCellValue('H'.$cell, $bookingDate);
    $sheet->setCellValue('I'.$cell, $loanDetails[$i]->getSqFt());
    $sheet->setCellValue('J'.$cell, $loanDetails[$i]->getSpaPrice());
    $sheet->setCellValue('K'.$cell, $loanDetails[$i]->getPackage());
    $sheet->setCellValue('L'.$cell, $loanDetails[$i]->getDiscount());
    $sheet->setCellValue('M'.$cell, $loanDetails[$i]->getRebate());
    $sheet->setCellValue('N'.$cell, $loanDetails[$i]->getNettPrice());
    $sheet->setCellValue('O'.$cell, $loanDetails[$i]->getTotalDeveloperComm());
    $sheet->setCellValue('P'.$cell, $loanDetails[$i]->getAgent());
    $sheet->setCellValue('Q'.$cell, $loanDetails[$i]->getLoanStatus());
    $sheet->setCellValue('R'.$cell, $loanDetails[$i]->getRemark());
    $sheet->setCellValue('S'.$cell, $loanDetails[$i]->getBFormCollected());
    $sheet->setCellValue('T'.$cell, $loanDetails[$i]->getPaymentMethod());
    $sheet->setCellValue('U'.$cell, $loanDetails[$i]->getLawyer());
    $sheet->setCellValue('V'.$cell, $loanDetails[$i]->getPendingApprovalStatus());
    $sheet->setCellValue('W'.$cell, $loanDetails[$i]->getBankApproved());
    $sheet->setCellValue('X'.$cell, $loSignedDate);
    $sheet->setCellValue('Y'.$cell, $laSignedDate);
    $sheet->setCellValue('Z'.$cell, $spaSignedDate);
    $sheet->setCellValue('AA'.$cell, $loanDetails[$i]->getFullsetCompleted());
    $sheet->setCellValue('AB'.$cell, $loanDetails[$i]->getCashBuyer());
    $sheet->setCellValue('AC'.$cell, $loanDetails[$i]->getCancelledBooking());
    $sheet->setCellValue('AD'.$cell, $loanDetails[$i]->getCaseStatus());
    $sheet->setCellValue('AE'.$cell, $loanDetails[$i]->getEventPersonal());
    $sheet->setCellValue('AF'.$cell, $loanDetails[$i]->getRate());
    $sheet->setCellValue('AG'.$cell, $loanDetails[$i]->getAgentComm());
  }
}

$styleArray = [
    'borders' => [
      'allBorders' => [
       'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
       'color' => ['argb' => '000000'],
     ],
  ],
    'alignment' => [
      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
      'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
    ],
];

$spreadsheet->getActiveSheet()->getStyle('A2:AG'.$cell.'')->applyFromArray($styleArray);

$range = range('A','Z');

foreach ($range as $ranges) {
  $spreadsheet->getActiveSheet()->getColumnDimension($ranges)->setAutoSize(true);
}

$rangeCont = range('A','G');

foreach ($range as $ranges) {
  $spreadsheet->getActiveSheet()->getColumnDimension('A'.$ranges)->setAutoSize(true);
}

$spreadsheet->getActiveSheet()->getStyle('A2:AG'.$cell.'')
    ->getAlignment()->setWrapText(true);


//set the header first, so the result will be treated as an xlsx file.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

//make it an attachment so we can define filename
$date = date('d-m-Y');
header('Content-Disposition: attachment;filename="Agent Info '.$date.'.xlsx"');

//create IOFactory object
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//save into php output
$writer->save('php://output');
