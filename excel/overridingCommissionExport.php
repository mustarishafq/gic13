<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Project.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Commission.php';

require_once dirname(__FILE__) . '/../utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/languageFunction.php';
// require_once dirname(__FILE__) . '/../vendor/autoload.php';

$fullName = $_SESSION['fullname'];
$claimIdAll = [];
$no = 0;
$cell = 6;
$conn = connDB();

$userRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($fullName),"s");
$userDetails = $userRows[0];

//call the autoload
require '../vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;

//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();

//color th
$spreadsheet->getActiveSheet()->getStyle('A6:F6')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');
$spreadsheet->getActiveSheet()->getStyle('A2:B2')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');
$spreadsheet->getActiveSheet()->getStyle('A3:B3')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('C0C0C0');

    $styleArray = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'borders' => [
          'allBorders' => [
           'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
           'color' => ['argb' => '000000'],
         ],
      ],
    ];

//border line and center
    $spreadsheet->getActiveSheet()->getStyle('A6:F6')->applyFromArray($styleArray);
    $spreadsheet->getActiveSheet()->getStyle('A2:E2')->applyFromArray($styleArray);
    $spreadsheet->getActiveSheet()->getStyle('A3:E3')->applyFromArray($styleArray);

//set the value of cell a1 to "Hello World!"
// $sheet->setCellValue('A1', 'Hello World !');
$sheet->setCellValue('A6', 'NO.');
$sheet->setCellValue('B6', 'PROJECT NAME');
$sheet->setCellValue('C6', 'UNIT NO.');
$sheet->setCellValue('D6', 'BOOKING DATE');
$sheet->setCellValue('E6', 'SPA PRICE (RM)');
$sheet->setCellValue('F6', 'NETT PRICE (RM)');

$uplineDetails = getCommission($conn," WHERE upline_default = ? and upline_type != 'Agent' ",array("upline_default"),array($fullName), "s");
$no = 1;
// $uplineDetails = getLoanStatus($conn, $projectName);
if($uplineDetails != null)
{
    for($cntAA = 0;$cntAA < count($uplineDetails) ;$cntAA++)
    {
      $no += 1;
      $cell++;
      if ($uplineDetails[$cntAA]->getClaimId()) {
        $loanDetails = getLoanStatus($conn,"WHERE loan_uid =?",array("loan_uid"),array($uplineDetails[$cntAA]->getLoanUid()),"s");
      }else {
        $loanDetails = "";
      }

      $sheet->setCellValue('A2', 'Name :');
      $sheet->mergeCells("A2:B2");
      $sheet->setCellValue('C2', $fullName);
      $sheet->mergeCells("C2:E2");
      $sheet->setCellValue('A3', 'Position :');
      $sheet->mergeCells("A3:B3");
      $sheet->setCellValue('C3', $userDetails[0]->getPosition());
      $sheet->mergeCells("C3:E3");
      $sheet->setCellValue('A'.$cell, $no);
      $sheet->setCellValue('B'.$cell, $uplineDetails[$cntAA]->getProjectName());
      $sheet->setCellValue('C'.$cell, $uplineDetails[$cntAA]->getUnitNo());
      $sheet->setCellValue('D'.$cell, date('d-m-Y', strtotime($uplineDetails[$cntAA]->getBookingDate())));
      $sheet->setCellValue('E'.$cell, number_format($uplineDetails[$cntAA]->getSpaPrice()));
      $sheet->setCellValue('F'.$cell, number_format($uplineDetails[$cntAA]->getNettPrice()));
    // }
  }
}else{
  $sheet->setCellValue('A2', 'Name :');
  $sheet->mergeCells("A2:B2");
  $sheet->setCellValue('C2', $fullName);
  $sheet->mergeCells("C2:E2");
  $sheet->setCellValue('A3', 'Position :');
  $sheet->mergeCells("A3:B3");
  $sheet->setCellValue('C3', $userDetails->getPosition());
  $sheet->mergeCells("C3:E3");
  $sheet->mergeCells("A7:F7");
  $sheet->setCellValue('A7', "No Related Data");
  $styleQray = [
      'borders' => [
        'allBorders' => [
         'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
         'color' => ['Qgb' => '000000'],
       ],
    ],
      'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
      ],
  ];
  $spreadsheet->getActiveSheet()->getStyle('A7:F7')->applyFromArray($styleArray);
}

$styleArray = [
    'borders' => [
      'allBorders' => [
       'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
       'color' => ['argb' => '000000'],
     ],
  ],
    'alignment' => [
      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
      'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
    ],
];

// border and center
$spreadsheet->getActiveSheet()->getStyle('A6:F'.$cell.'')->applyFromArray($styleArray);

$range = range('A','F');

foreach ($range as $ranges) {
  $spreadsheet->getActiveSheet()->getColumnDimension($ranges)->setAutoSize(true);
}

// $rangeCont = range('A','R');
//
// foreach ($range as $ranges) {
//   $spreadsheet->getActiveSheet()->getColumnDimension('A'.$ranges)->setAutoSize(true);
// }

$spreadsheet->getActiveSheet()->getStyle('A6:F'.$cell.'')
    ->getAlignment()->setWrapText(true);


//set the header first, so the result will be treated as an xlsx file.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

//make it an attachment so we can define filename
$date = date('d-m-Y');
header('Content-Disposition: attachment;filename="Overriding Commission '.$_SESSION['username']." ".$date.'.xlsx"');

//create IOFactory object
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//save into php output
$writer->save('php://output');
