<?php require_once dirname(__FILE__) . '/sessionLoginChecker.php';
 ?>
<div class="headerline"></div>
<div class="width100 same-padding header-div">
	<a href="admin2.php"><div class="logo"><img src="img/gic.png" class="logo-size" alt="GIC Group" title="GIC Group"></div></a>
	<div class="right-menu">
    <div class="dropdown hover1">
				<a  class="menu-right-margin dropdown-btn">
						<img src="img/history.png" class="gic-menu-icon hover1a" alt="Slip History" title="Slip History">
						<img src="img/history.png" class="gic-menu-icon hover1b" alt="Slip History" title="Slip History">
					</a>
					<div class="dropdown-content yellow-dropdown-content">
            <p class="dropdown-p"><a href="advancedHistory.php"  class="dropdown-a">Advance History</a></p>
            <p class="dropdown-p"><a href="commissionHistory.php"  class="dropdown-a">Commission History</a></p>
					</div>
		</div>

		<a href="announcementCurrent.php"  class="menu-right-margin hover1">
			<img src="img/announce1.png" class="hover1a gic-menu-icon" alt="Make announcement " title="Make announcement ">
				<img src="img/announce2.png" class="hover1b gic-menu-icon" alt="Make announcement " title="Make announcement ">
		</a>
		<div class="dropdown hover1">
				<a  class="menu-right-margin dropdown-btn">
						<img src="img/user1.png" class="gic-menu-icon hover1a" alt="Agent Info" title="Agent Info">
						<img src="img/user2.png" class="gic-menu-icon hover1b" alt="Agent Info" title="Agent Info">
					</a>
					<div class="dropdown-content yellow-dropdown-content">
            <?php if ($_SESSION['branch_type'] == 1) {
              ?><p class="dropdown-p"><a href="addAdmin.php"  class="dropdown-a">Add Admin & Agent</a></p>
            <p class="dropdown-p"><a href="adminInfo.php"  class="dropdown-a">Admin & Agent Info</a></p><?php
            }else {
              ?><p class="dropdown-p"><a href="addAgent.php"  class="dropdown-a">Add Agent</a></p>
    			<p class="dropdown-p"><a href="agentInfo.php"  class="dropdown-a">Edit Agent Info</a></p><?php
            } ?>

					</div>
		</div>
       	<a href="invoiceStatus.php"  class="menu-right-margin hover1">
        	<img src="img/invoice-coin.png" class="hover1a gic-menu-icon" alt="Invoice Status" title="Invoice Status">
            <img src="img/invoice-coin2.png" class="hover1b gic-menu-icon" alt="Invoice Status" title="Invoice Status">
        </a>
   		<a href="admin2Product.php"  class="menu-right-margin hover1">
        	<img src="img/invoice-only.png" class="hover1a gic-menu-icon" alt="Loan Status Invoice" title="Loan Status">
            <img src="img/invoice-only2.png" class="hover1b gic-menu-icon" alt="Loan Status Invoice" title="Loan Status">
        </a>
				<?php if ($_SESSION['branch_type'] == 1) {
					?><a href="settings.php"  class="menu-right-margin hover1">
							<img src="img/setting1.png" class="hover1a gic-menu-icon" alt="Setting" title="Setting">
								<img src="img/setting2.png" class="hover1b gic-menu-icon" alt="Setting" title="Setting">
						</a><?php
				}else {
					?><a href="summertonSettings.php"  class="menu-right-margin hover1">
							<img src="img/setting1.png" class="hover1a gic-menu-icon" alt="Setting" title="Setting">
								<img src="img/setting2.png" class="hover1b gic-menu-icon" alt="Setting" title="Setting">
						</a><?php
				} ?>
    	<a href="logout.php" class="hover1">
        	<img src="img/logout.png" class="hover1a gic-menu-icon" alt="logout" title="logout">
            <img src="img/logout2.png" class="hover1b gic-menu-icon" alt="logout" title="logout">
        </a>
    </div>
</div>
<div class="clear"></div>
