<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Commission.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$fullName = $_SESSION['fullname'];
$claimIdAll = [];
$conn = connDB();

$userRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($fullName),"s");
$userDetails = $userRows[0];

$project = $_POST['project'];

if (isset($_POST['bookingStartDate'])) {
$dateStart = $_POST['bookingStartDate'];
}else {
$dateStart = "";
}
if (isset($_POST['bookingEndDate'])) {
$dateEnd = $_POST['bookingEndDate'];
}else {
$dateEnd = "";
}
// $bookingDate = date('Y-m-d',strtotime($_POST['bookingDate']));
if ($dateStart) {
  $dateStartN = str_replace("/","-",$dateStart);
  $bookingStartDate = date('Y-m-d',strtotime($dateStartN));
}else{
  $bookingStartDate = '1970-01-01';
}
if ($dateEnd) {
  $dateEndN = str_replace("/","-",$dateEnd);
  $bookingEndDate = date('Y-m-d',strtotime($dateEndN));
}else{
  $bookingEndDate = date('Y-m-d');
}

 ?>
<table id="myTable" class="shipping-table pointer-th">
    <thead>
        <tr>
            <th class="th">NO. <img src="img/sort.png" class="sort"></th>
            <th class="th">PROJECT NAME <img src="img/sort.png" class="sort"></th>
            <th class="th">UNIT NO. <img src="img/sort.png" class="sort"></th>
            <th class="th">BOOKING DATE <img src="img/sort.png" class="sort"></th>
            <th class="th">SPA PRICE (RM) <img src="img/sort.png" class="sort"></th>
            <th class="th">NETT PRICE (RM) <img src="img/sort.png" class="sort"></th>
            <!-- <th class="th">OVERRIDING COMMISSION (RM)</th> -->
            <th class="th">ACTION <img src="img/sort.png" class="sort"></th>

        </tr>
    </thead>
    <tbody id="myFilter">
        <?php
        $conn = connDB();
            if ($project) {
              $uplineDetails = getCommission($conn," WHERE upline_default = ? and upline_type != 'Agent' AND booking_date >=? AND booking_date <=? AND project_name =?",array("upline_default","booking_date","booking_date","project_name"),array($fullName,$bookingStartDate,$bookingEndDate,$project), "ssss");
            }else{
              $uplineDetails = getCommission($conn," WHERE upline_default = ? and upline_type != 'Agent' AND booking_date >=? AND booking_date <=? ",array("upline_default","booking_date","booking_date"),array($fullName,$bookingStartDate,$bookingEndDate), "sss");
            }
            $no = 1;
            // $uplineDetails = getLoanStatus($conn, $projectName);
            if($uplineDetails != null)
            {
                for($cntAA = 0;$cntAA < count($uplineDetails) ;$cntAA++)
                {
                  if ($uplineDetails[$cntAA]->getClaimId()) {
                    $loanDetails = getLoanStatus($conn,"WHERE loan_uid =?",array("loan_uid"),array($uplineDetails[$cntAA]->getLoanUid()),"s");
                  }else {
                    $loanDetails = "";
                  }
                  ?>
                <tr>
                    <td class="td"><?php echo ($no)?></td>
                    <td class="td"><?php echo $uplineDetails[$cntAA]->getProjectName();?></td>
                    <td class="td"><?php echo $uplineDetails[$cntAA]->getUnitNo();?></td>
                    <td class="td"><?php echo date('d-m-Y', strtotime($uplineDetails[$cntAA]->getBookingDate()));?></td>
                    <td class="td"><?php if ($loanDetails) {
                      echo number_format($loanDetails[0]->getSpaPrice(),2);
                    }?></td>
                    <td class="td"><?php if ($loanDetails) {
                      echo number_format($loanDetails[0]->getNettPrice(),2);
                    }?></td>
                    <!-- <td class="td"><?php //echo $totalAmountFinal;?></td> -->
                    <td class="td">
                      <form class="" action="commissionAgent.php" method="post">
                        <input type="hidden" name="claim_id" value="<?php echo $uplineDetails[$cntAA]->getClaimId() ?>">
                        <button class="clean edit-anc-btn hover1" type="submit" name="upline" value="<?php echo $fullName ?>">
                          <a><?php echo "Print Commission" ?></a>
                        </button>
                      </form>
                    </td>

                </tr>
                <?php
                $no += 1;
                // }
              }
          }else {
            ?><td colspan="7" style="text-align: center;font-weight: bold;font-size: 14px" >No Commission Found !</td> <?php
          }
        // }
        ?>
    </tbody>
</table>
