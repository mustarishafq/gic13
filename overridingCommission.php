<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Commission.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
$userUsername = $userDetails->getUsername();
$upline1stCom = 0;
$upline2ndCom = 0;
$upline3rdCom = 0;
$upline4thCom = 0;
$upline5thCom = 0;
$upline21stCom = 0;
$upline22ndCom = 0;
$upline23rdCom = 0;
$upline24thCom = 0;
$upline25thCom = 0;
$pl1stCom = 0;
$pl2ndCom = 0;
$pl3rdCom = 0;
$pl4thCom = 0;
$pl5thCom = 0;
$hos1stCom = 0;
$hos2ndCom = 0;
$hos3rdCom = 0;
$hos4thCom = 0;
$hos5thCom = 0;
$lister1stCom = 0;
$lister2ndCom = 0;
$lister3rdCom = 0;
$lister4thCom = 0;
$lister5thCom = 0;
// echo $userUsername ;

// $projectDetails = getProject($conn);
// $projectName = "";
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'agentHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">

<h1 class="h1-title h1-before-border shipping-h1">My Performance</h1>
  <!-- <h1 class="h1-title h1-before-border shipping-h1">Personal Sales (SPA Signed)</h1> -->


  <div class="short-red-border"></div>

  <h3 class="h1-title"><a href="personalCommission.php" class="h1-title"> Personal Commission</a> | Overriding Commission</h3>

  <div class="clear"></div>


  <div class="width100 shipping-div2">
      <?php $conn = connDB();?>
      <table class="shipping-table">
          <thead>
              <tr>
                  <th class="th">NO.</th>

                  <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?></th>
                  <th class="th"><?php echo wordwrap("TOTAL COMMISSION",10,"</br>\n");?></th>
                  <th class="th"><?php echo wordwrap("1ST CLAIM COMMISSION (RM)",10,"</br>\n");?></th>
                  <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?></th>
                  <th class="th"><?php echo wordwrap("2ND CLAIM COMMISSION (RM)",10,"</br>\n");?></th>
                  <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?></th>
                  <th class="th"><?php echo wordwrap("3RD CLAIM COMMISSION (RM)",10,"</br>\n");?></th>
                  <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?></th>
                  <th class="th"><?php echo wordwrap("4TH CLAIM COMMISSION (RM)",10,"</br>\n");?></th>
                  <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?></th>
                  <th class="th"><?php echo wordwrap("5TH CLAIM COMMISSION (RM)",10,"</br>\n");?></th>
                  <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?></th>
                  <th class="th"><?php echo wordwrap("UNCLAIMED BALANCE (RM)",10,"</br>\n");?></th>
              </tr>
          </thead>
          <tbody>
              <?php
              // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
              // {
              $upline1Details = getLoanStatus($conn," WHERE upline1 = ?",array("upline1"),array($userUsername), "s");
              $upline2Details = getLoanStatus($conn," WHERE upline2 = ?",array("upline2"),array($userUsername), "s");
              $plNameDetails = getLoanStatus($conn," WHERE pl_name = ?",array("pl_name"),array($userUsername), "s");
              $hosNameDetails = getLoanStatus($conn," WHERE hos_name = ?",array("hos_name"),array($userUsername), "s");
              $listerNameDetails = getLoanStatus($conn," WHERE lister_name = ?",array("lister_name"),array($userUsername), "s");
              $no = 1;
              // $upline1Details = getLoanStatus($conn, $projectName);
              if($upline1Details != null)
              {
                  for($cntAA = 0;$cntAA < count($upline1Details) ;$cntAA++)
                  {
                  $upline1CommDetails = getCommission($conn, "WHERE upline = ? AND loan_uid = ?", array("upline", "loan_uid"),
                  array($upline1Details[$cntAA]->getUpline1(), $upline1Details[$cntAA]->getLoanUid()), "ss");
                  if ($upline1CommDetails) {
                  for ($cntBB=0; $cntBB <count($upline1CommDetails) ; $cntBB++) {
                    ?>
                  <tr>
                      <td class="td"><?php echo ($no)?></td>
                      <td class="td"><?php echo $upline1Details[$cntAA]->getUnitNo();?></td>
                      <?php if ( $upline1Details[$cntAA]->getUlOverride()) {
                        ?><td class="td"><?php echo $upline1Details[$cntAA]->getUlOverride();?></td><?php
                      }else {
                        ?><td></td> <?php
                      } ?> <?php
                      if ( $upline1CommDetails[$cntBB]->getDetails() == '2nd Part Commission') {
                        ?><td class="td"><?php echo $upline2ndCom = $upline1CommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $upline1CommDetails[$cntBB]->getDetails() == '3rd Part Commission') {
                        ?><td class="td"><?php echo $upline3rdCom = $upline1CommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $upline1CommDetails[$cntBB]->getDetails() == '4th Part Commission') {
                        ?><td class="td"><?php echo $upline4thCom = $upline1CommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $upline1CommDetails[$cntBB]->getDetails() == '5th Part Commission') {
                        ?><td class="td"><?php echo $upline5thCom = $upline1CommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $upline1CommDetails[$cntBB]->getDetails() == '6th Part Commission') {
                        ?><td class="td"><?php echo $upline5thCom = $upline1CommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $upline1Details[$cntAA]->getUlOverride()) {
                        ?><td class="td"><?php echo $upline1Details[$cntAA]->getUlOverride() - $upline1stCom - $upline2ndCom - $upline3rdCom - $upline4thCom - $upline5thCom;?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      \
                      ?>


                  </tr>
                  <?php
                  $no += 1;
                  }
                }
                }
              }
              if($upline2Details != null)
              {
                  for($cntAA = 0;$cntAA < count($upline2Details) ;$cntAA++)
                  {
                    $upline2CommDetails = getCommission($conn, "WHERE upline = ? AND loan_uid = ?", array("upline", "loan_uid"),
                    array($upline2Details[$cntAA]->getUpline2(), $upline2Details[$cntAA]->getLoanUid()), "ss");
                    if ($upline2CommDetails) {
                    for ($cntBB=0; $cntBB <count($upline2CommDetails) ; $cntBB++) {
                      ?>
                  <tr>
                      <td class="td"><?php echo ($no)?></td>
                      <td class="td"><?php echo $upline2Details[$cntAA]->getUnitNo();?></td>
                      <?php if ( $upline2Details[$cntAA]->getUulOverride()) {
                        ?><td class="td"><?php echo $upline2Details[$cntAA]->getUulOverride();?></td><?php
                      }else {
                        ?><td></td> <?php
                      } ?>
                      <?php if ( $upline2CommDetails[$cntBB]->getDetails() == '2nd Part Commission' && $upline2CommDetails[$cntBB]->getDateUpdated()) {
                        ?><td class="td"><?php echo $upline21stCom = $upline2CommDetails[$cntBB]->getCommission();?></td>
                        <td><form action="commission.php" method="POST">
                          <input type="hidden" name="id" value="<?php echo $upline2CommDetails[$cntBB]->getID(); ?>">
                          <input type="hidden" name="project_name" value="<?php echo $upline2CommDetails[$cntBB]->getProjectName(); ?>">
                          <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit"><?php echo date('d-m-Y', strtotime($upline2CommDetails[$cntBB]->getDateUpdated()))  ?>
                                <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                            </button></a>
                        </form></td><?php
                      }else {
                        ?><td></td>
                          <td></td> <?php
                      }
                      ?>

                      <?php
                      if ( $upline2CommDetails[$cntBB]->getDetails() == '3rd Part Commission' && $upline2CommDetails[$cntBB]->getDateUpdated()) {
                        ?><td class="td"><?php echo $upline22ndCom =  $upline2CommDetails[$cntBB]->getCommission();?></td>
                        <td><form action="commission.php" method="POST">
                          <input type="hidden" name="id" value="<?php echo $upline2CommDetails[$cntBB]->getID(); ?>">
                          <input type="hidden" name="project_name" value="<?php echo $upline2CommDetails[$cntBB]->getProjectName(); ?>">
                          <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit"><?php echo date('d-m-Y', strtotime($upline2CommDetails[$cntBB]->getDateUpdated()))  ?>
                                <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                            </button></a>
                        </form></td><?php
                      }else {
                        ?><td></td> <?php
                          ?><td></td> <?php
                      }
                      if ( $upline2CommDetails[$cntBB]->getDetails() == '4th Part Commission' && $upline2CommDetails[$cntBB]->getDateUpdated()) {
                        ?><td class="td"><?php echo $upline23rdCom =  $upline2CommDetails[$cntBB]->getCommission();?></td>
                        <td><form action="commission.php" method="POST">
                          <input type="hidden" name="id" value="<?php echo $upline2CommDetails[$cntBB]->getID(); ?>">
                          <input type="hidden" name="project_name" value="<?php echo $upline2CommDetails[$cntBB]->getProjectName(); ?>">
                          <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit"><?php echo date('d-m-Y', strtotime($upline2CommDetails[$cntBB]->getDateUpdated()))  ?>
                                <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                            </button></a>
                        </form></td><?php
                      }else {
                        ?><td></td> <?php
                          ?><td></td> <?php
                      }
                      if ( $upline2CommDetails[$cntBB]->getDetails() == '5th Part Commission' && $upline2CommDetails[$cntBB]->getDateUpdated()) {
                        ?><td class="td"><?php echo $upline24thCom =  $upline2CommDetails[$cntBB]->getCommission();?></td>
                        <td><form action="commission.php" method="POST">
                          <input type="hidden" name="id" value="<?php echo $upline2CommDetails[$cntBB]->getID(); ?>">
                          <input type="hidden" name="project_name" value="<?php echo $upline2CommDetails[$cntBB]->getProjectName(); ?>">
                          <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit"><?php echo date('d-m-Y', strtotime($upline2CommDetails[$cntBB]->getDateUpdated()))  ?>
                                <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                            </button></a>
                        </form></td><?php
                      }else {
                        ?><td></td> <?php
                          ?><td></td> <?php
                      }
                      if ( $upline2CommDetails[$cntBB]->getDetails() == '6th Part Commission' && $upline2CommDetails[$cntBB]->getDateUpdated()) {
                        ?><td class="td"><?php echo $upline25thCom =  $upline2CommDetails[$cntBB]->getCommission();?></td>
                        <td><form action="commission.php" method="POST">
                          <input type="hidden" name="id" value="<?php echo $upline2CommDetails[$cntBB]->getID(); ?>">
                          <input type="hidden" name="project_name" value="<?php echo $upline2CommDetails[$cntBB]->getProjectName(); ?>">
                          <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit"><?php echo date('d-m-Y', strtotime($upline2CommDetails[$cntBB]->getDateUpdated()))  ?>
                                <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                            </button></a>
                        </form></td><?php
                      }else {
                        ?><td></td> <?php
                          ?><td></td> <?php
                      }
                      if ( $upline2Details[$cntAA]->getUlOverride()) {
                        ?><td class="td"><?php echo $upline2Details[$cntAA]->getUulOverride() - $upline21stCom - $upline22ndCom - $upline23rdCom - $upline24thCom - $upline25thCom;?></td><?php
                      }else {
                        ?><td></td> <?php
                      }?>

                  </tr>
                  <?php
                  $no += 1;
                  }
                }
              }
              }
              if($plNameDetails != null)
              {
                  for($cntAA = 0;$cntAA < count($plNameDetails) ;$cntAA++)
                  {
                    $plCommDetails = getCommission($conn, "WHERE upline = ? AND loan_uid = ?", array("upline", "loan_uid"),
                    array($plNameDetails[$cntAA]->getPlName(), $plNameDetails[$cntAA]->getLoanUid()), "ss");
                    if ($plCommDetails) {
                    for ($cntBB=0; $cntBB <count($plCommDetails) ; $cntBB++) {
                    ?>
                  <tr>
                      <td class="td"><?php echo ($no)?></td>
                      <td class="td"><?php echo $plNameDetails[$cntAA]->getUnitNo();?></td>
                      <?php if ( $plNameDetails[$cntAA]->getPlOverride()) {
                        ?><td class="td"><?php echo $plNameDetails[$cntAA]->getPlOverride();?></td><?php
                      }else {
                        ?><td></td> <?php
                      } ?>
                      <?php if ( $plCommDetails[$cntBB]->getDetails() == '2nd Part Commission') {
                        ?><td class="td"><?php echo $pl1stCom =  $plCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $plCommDetails[$cntBB]->getDetails() == '3rd Part Commission') {
                        ?><td class="td"><?php echo $pl3rdCom = $plCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $plCommDetails[$cntBB]->getDetails() == '4th Part Commission') {
                        ?><td class="td"><?php echo $pl4thCom = $plCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $plCommDetails[$cntBB]->getDetails() == '5th Part Commission') {
                        ?><td class="td"><?php echo $pl5thCom = $plCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $plCommDetails[$cntBB]->getDetails() == '6th Part Commission') {
                        ?><td class="td"><?php echo $pl2ndCom = $plCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $plNameDetails[$cntAA]->getUlOverride()) {
                        ?><td class="td"><?php echo $plNameDetails[$cntAA]->getPlOverride() - $pl1stCom - $pl2ndCom - $pl3rdCom - $pl4thCom - $pl5thCom;?></td><?php
                      }else {
                        ?><td></td> <?php
                      }

                      ?>


                  </tr>
                  <?php
                  $no += 1;
                  }
                }
              }
              }
              if($listerNameDetails != null)
              {
                  for($cntAA = 0;$cntAA < count($listerNameDetails) ;$cntAA++)
                  {
                    $listerCommDetails = getCommission($conn, "WHERE upline = ? AND loan_uid = ?", array("upline", "loan_uid"),
                    array($listerNameDetails[$cntAA]->getListerName(), $listerNameDetails[$cntAA]->getLoanUid()), "ss");
                    if ($listerCommDetails) {
                    for ($cntBB=0; $cntBB <count($listerCommDetails) ; $cntBB++) {
                    ?>
                  <tr>
                      <td class="td"><?php echo ($no)?></td>
                      <td class="td"><?php echo $listerNameDetails[$cntAA]->getUnitNo();?></td>
                      <?php if ( $listerNameDetails[$cntAA]->getListerOverride()) {
                        ?><td class="td"><?php echo $listerNameDetails[$cntAA]->getListerOverride();?></td><?php
                      }else {
                        ?><td></td> <?php
                      } ?>
                      <?php if ( $listerCommDetails[$cntBB]->getDetails() == '2nd Part Commission') {
                        ?><td class="td"><?php echo $lister1stCom = $listerCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $listerCommDetails[$cntBB]->getDetails() == '3rd Part Commission') {
                        ?><td class="td"><?php echo $lister3rdCom = $listerCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $listerCommDetails[$cntBB]->getDetails() == '4th Part Commission') {
                        ?><td class="td"><?php echo $lister4thCom = $listerCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $listerCommDetails[$cntBB]->getDetails() == '5th Part Commission') {
                        ?><td class="td"><?php echo $lister5thCom = $listerCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $listerCommDetails[$cntBB]->getDetails() == '6th Part Commission') {
                        ?><td class="td"><?php echo $lister2ndCom = $listerCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $listerNameDetails[$cntAA]->getListerOverride()) {
                        ?><td class="td"><?php echo $listerNameDetails[$cntAA]->getListerOverride() - $lister1stCom - $lister2ndCom - $lister3rdCom - $lister4thCom - $lister5thCom;?></td><?php
                      }else {
                        ?><td></td> <?php
                      }

                      ?>


                  </tr>
                  <?php
                  $no += 1;
                  }
                }
              }
              }
              if($hosNameDetails != null)
              {
                  for($cntAA = 0;$cntAA < count($hosNameDetails) ;$cntAA++)
                  {
                    $hosCommDetails = getCommission($conn, "WHERE upline = ? AND loan_uid = ?", array("upline", "loan_uid"),
                    array($hosNameDetails[$cntAA]->getHosName(), $hosNameDetails[$cntAA]->getLoanUid()), "ss");
                    if ($hosCommDetails) {
                    for ($cntBB=0; $cntBB <count($hosCommDetails) ; $cntBB++) {
                    ?>
                  <tr>
                      <td class="td"><?php echo ($no)?></td>
                      <td class="td"><?php echo $hosNameDetails[$cntAA]->getUnitNo();?></td>
                      <?php if ( $hosNameDetails[$cntAA]->getUlOverride()) {
                        ?><td class="td"><?php echo $hosNameDetails[$cntAA]->getHosOverride();?></td><?php
                      }else {
                        ?><td></td> <?php
                      } ?>
                      <?php if ( $hosCommDetails[$cntBB]->getDetails() == '2nd Part Commission') {
                        ?><td class="td"><?php echo $hos1stCom = $hosCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $hosCommDetails[$cntBB]->getDetails() == '3rd Part Commission') {
                        ?><td class="td"><?php echo $hos3rdCom = $hosCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $hosCommDetails[$cntBB]->getDetails() == '4th Part Commission') {
                        ?><td class="td"><?php echo $hos4thCom = $hosCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $hosCommDetails[$cntBB]->getDetails() == '5th Part Commission') {
                        ?><td class="td"><?php echo $hos5thCom = $hosCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $hosCommDetails[$cntBB]->getDetails() == '6th Part Commission') {
                        ?><td class="td"><?php echo $hos2ndCom = $hosCommDetails[$cntBB]->getCommission();?></td><?php
                      }else {
                        ?><td></td> <?php
                      }
                      if ( $hosNameDetails[$cntAA]->getUlOverride()) {
                        ?><td class="td"><?php echo $hosNameDetails[$cntAA]->getHosOverride() - $hos1stCom - $hos2ndCom - $hos3rdCom - $hos4thCom - $hos5thCom;?></td><?php
                      }else {
                        ?><td></td> <?php
                      }

                      ?>



                  </tr>
                  <?php
                  $no += 1;
                  }
                }
              }
              }
                      ?>
                      </tr>
                      <?php

              //}
              ?>
          </tbody>
      </table><br>
  </div>

    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
