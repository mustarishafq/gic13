<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$branchType = $_SESSION['branch_type'];
$projectName = $_POST['projectName'];

// $projectDetails = getProject($conn, "WHERE project_name = ? AND branch_type =? ",array("project_name,branch_type"),array($projectName,$branchType), "ss");
$projectDetails = getProject($conn, "WHERE project_name =? AND display ='Yes'",array("project_name"),array($projectName),"s");

$plName = $projectDetails[0]->getProjectLeader();
  $plNameArray[] = array("totalPLName" => $plName);
;

echo json_encode($plNameArray);
 ?>
