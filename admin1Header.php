<?php require_once dirname(__FILE__) . '/sessionLoginChecker.php';
 ?>
<div class="headerline"></div>
<div class="width100 same-padding header-div">
	<a href="admin1Product.php"><div class="logo"><img src="img/gic.png" class="logo-size" alt="GIC Group" title="GIC Group"></div></a>
	<div class="right-menu">
		<a href="announcementCurrent.php"  class="menu-right-margin hover1">
			<img src="img/announce1.png" class="hover1a gic-menu-icon" alt="Make announcement " title="Make announcement ">
				<img src="img/announce2.png" class="hover1b gic-menu-icon" alt="Make announcement " title="Make announcement ">
		</a>
		<div class="dropdown hover1">
				<a  class="menu-right-margin dropdown-btn">
						<img src="img/user1.png" class="gic-menu-icon hover1a" alt="Agent Info" title="Agent Info">
						<img src="img/user2.png" class="gic-menu-icon hover1b" alt="Agent Info" title="Agent Info">
					</a>
					<div class="dropdown-content yellow-dropdown-content">
            <?php if ($_SESSION['branch_type'] == 1) {
              ?><p class="dropdown-p"><a href="addAdmin.php"  class="dropdown-a">Add Admin & Agent</a></p>
            <p class="dropdown-p"><a href="adminInfo.php"  class="dropdown-a">Admin & Agent Info</a></p><?php
            }else {
              ?><p class="dropdown-p"><a href="addAgent.php"  class="dropdown-a">Add Agent</a></p>
    			<p class="dropdown-p"><a href="agentInfo.php"  class="dropdown-a">Edit Agent Info</a></p><?php
            } ?>
					</div>
		</div>
    	<a href="adminAddNewProject.php" class="menu-right-margin hover1">
        	<img src="img/project.png" class="hover1a gic-menu-icon" alt="Add New Project" title="Add New Project">
            <img src="img/project2.png" class="hover1b gic-menu-icon" alt="Add New Project" title="Add New Project">
        </a>
    	<div class="dropdown hover1">
        	<a  class="menu-right-margin dropdown-btn">
            	<img src="img/invoice.png" class="gic-menu-icon hover1a" alt="Loan Status" title="Loan Status">
            	<img src="img/invoice2.png" class="gic-menu-icon hover1b" alt="Loan Status" title="Loan Status">
            </a>
            <div class="dropdown-content yellow-dropdown-content">
				<p class="dropdown-p"><a href="admin1Product.php"  class="dropdown-a">Loan Status Form</a></p>
				<p class="dropdown-p"><a href="adminAddNewProduct.php"  class="dropdown-a">Add New Loan</a></p>
				<p class="dropdown-p"><a href="uploadBookingForm.php"  class="dropdown-a">Upload Loan Form</a></p>
            </div>
    	</div>
			<?php if ($_SESSION['branch_type'] == 1) {
				?><a href="settings.php"  class="menu-right-margin hover1">
						<img src="img/setting1.png" class="hover1a gic-menu-icon" alt="Setting" title="Setting">
							<img src="img/setting2.png" class="hover1b gic-menu-icon" alt="Setting" title="Setting">
					</a><?php
			}else {
				?><a href="summertonSettings.php"  class="menu-right-margin hover1">
						<img src="img/setting1.png" class="hover1a gic-menu-icon" alt="Setting" title="Setting">
							<img src="img/setting2.png" class="hover1b gic-menu-icon" alt="Setting" title="Setting">
					</a><?php
			} ?>
    	<a href="logout.php" class="hover1">
        	<img src="img/logout.png" class="hover1a gic-menu-icon" alt="logout" title="logout">
            <img src="img/logout2.png" class="hover1b gic-menu-icon" alt="logout" title="logout">
        </a>
    </div>
</div>
<div class="clear"></div>
