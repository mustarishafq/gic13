<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/EditHistory.php';
require_once dirname(__FILE__) . '/classes/ProformaInvoice.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
// $loanUid = rewrite($_POST['loan_uid']);
// $sql = "select pdf from loan_status";
// $result = mysqli_query($conn, $sql);
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Booking Form Upload | GIC" />
    <title>Edit History | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'admin1Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Edit History</h1>

    <div class="short-red-border"></div>


    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->
    <!-- End of Filter -->


    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();
          ?><table class="shipping-table">
              <thead>
                  <tr>
                      <th class="th">NO.</th>
                      <th class="th">PROJECT NAME</th>
                      <!-- <th class="th">UNIT NO.</th> -->
                      <th class="th">DETAILS</th>
                      <th class="th">DATA EDIT</th>
                      <th class="th">EDIT TO</th>
                      <th class="th">USERNAME</th>
                      <th class="th">DATE MODIFIED</th>
                  </tr>
              </thead>
              <tbody>
                  <?php
                  // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                  //
                      $loanUid = rewrite($_POST['proforma_id']);
                      $editHistoryDetails = getEditHistory($conn, "WHERE loan_uid = '$loanUid' ORDER BY date_created DESC" );
                      // $editHistoryDetails = getLoanStatus($conn);
                      if($editHistoryDetails != null)
                      {
                          for($cntAA = 0;$cntAA < count($editHistoryDetails) ;$cntAA++)
                          { $loanUidHistory = $editHistoryDetails[$cntAA]->getLoanUid();
                            $loanDetails = getProformaInvoice($conn,"WHERE proforma_id = '$loanUidHistory'");
                            // if ($loanDetails) {
                              ?>
                            <tr>
                                <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                <td class="td"><?php echo $cntAA + 1?></td>
                                <td class="td"><?php echo $loanDetails[0]->getProjectName();?></td>
                                <!-- <td class="td"><?php //echo $loanDetails[0]->getUnitNo();?></td> -->
                                <td class="td"><?php echo $editHistoryDetails[$cntAA]->getDetails();?></td>
                                <td class="td"><?php
                                if ($editHistoryDetails[$cntAA]->getDataBefore()) {
                                  echo str_replace(",","<br>",$editHistoryDetails[$cntAA]->getDataBefore());
                                } ?></td>
                                <td class="td"><?php
                                if ($editHistoryDetails[$cntAA]->getDataAfter()) {
                                  echo str_replace(",","<br>",$editHistoryDetails[$cntAA]->getDataAfter());
                                } ?></td>
                                <td class="td"><?php echo $editHistoryDetails[$cntAA]->getUsername() ?></td>
                                <td class="td"><?php echo date('d-m-Y',strtotime($editHistoryDetails[$cntAA]->getDateCreated())).
                                                      "<br>".date('h:i A',strtotime($editHistoryDetails[$cntAA]->getDateCreated())) ?></td>

                            </tr>
                            <?php
                            // }

                          }

                  }
                  ?>
              </tbody>
          </table>


    </div>


    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
