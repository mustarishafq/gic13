<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Commission.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$claimIdAll = [];
$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
$userUsername = $userDetails->getUsername();
$userFullName = $userDetails->getFullName();

// echo $userUsername ;

// $projectDetails = getProject($conn);
// $projectName = "";
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'agentHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">

<h1 class="h1-title h1-before-border shipping-h1">My Performance</h1>
  <!-- <h1 class="h1-title h1-before-border shipping-h1">Personal Sales (SPA Signed)</h1> -->


  <div class="short-red-border"></div>

<h3 class="h1-title"><a href="agentDashboard.php"> Personal Sales</a> | Personal Overriding</h3>

  <div class="clear"></div>
  <div class="section-divider width100 overflow">

  <?php
  $conn = connDB();

//   $projectName = "";
  $projectName = " ";
  $projectDetails = getProject($conn, "WHERE display = 'Yes'"); ?>

    <select id="sel_id" class="clean-select">
      <option value="">Select a project</option>
      <?php if ($projectDetails) {
        for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
          if ($projectDetails[$cnt]->getProjectName() != $types) {
            ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
            }
            }
            ?><option value="ALL">SHOW ALL</option><?php
          } ?>
    <!-- <option value="-1">Select</option>
    <option value="VIDA">kasper </option>
    <option value="VV">adad </option> -->
    <!-- <option value="14">3204 </option> -->
    </select>
  </div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th class="th">NO.</th>
                        <th class="th">PROJECT NAME</th>
                        <th class="th">UNIT NO.</th>
                        <th class="th">BOOKING DATE</th>
                        <th class="th">SPA PRICE (RM)</th>
                        <th class="th">NETT PRICE (RM)</th>
                        <!-- <th class="th">OVERRIDING COMMISSION (RM)</th> -->
                        <th class="th">Action</th>

                        <!-- <th class="th"><?php //echo wordwrap("NO.",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("PROJECT NAME",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("UNIT NO.",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("BOOKING DATE",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("NETT PRICE",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("AGENT",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("CASE STATUS",10,"</br>\n");?></th> -->

                    </tr>
                </thead>
                <tbody id="myFilter">
                    <?php
                    $conn = connDB();
                        $uplineDetails = getCommission($conn," WHERE upline_default = ?",array("upline_default"),array($userFullName), "s");
                        $no = 1;
                        // $uplineDetails = getLoanStatus($conn, $projectName);
                        if($uplineDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($uplineDetails) ;$cntAA++)
                            {
                              if ($uplineDetails[$cntAA]->getClaimId()) {
                                $loanDetails = getLoanStatus($conn,"WHERE loan_uid =?",array("loan_uid"),array($uplineDetails[$cntAA]->getLoanUid()),"s");
                              }
                              ?>
                            <tr>
                                <td class="td"><?php echo ($no)?></td>
                                <td class="td"><?php echo $uplineDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $uplineDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php echo date('d-m-Y', strtotime($uplineDetails[$cntAA]->getBookingDate()));?></td>
                                <td class="td"><?php echo number_format($loanDetails[0]->getSpaPrice(),2);?></td>
                                <td class="td"><?php echo number_format($loanDetails[0]->getNettPrice(),2);?></td>
                                <!-- <td class="td"><?php //echo $totalAmountFinal;?></td> -->
                                <td class="td">
                                  <form class="" action="commissionAgent.php" method="post">
                                    <input type="hidden" name="claim_id" value="<?php echo $uplineDetails[$cntAA]->getClaimId() ?>">
                                    <button class="clean edit-anc-btn hover1" type="submit" name="upline" value="<?php echo $userFullName ?>">
                                      <a><?php echo "Print Commission" ?></a>
                                    </button>
                                  </form>
                                </td>

                            </tr>
                            <?php
                            $no += 1;
                            // }
                          }
                        // $claimIdAll[] = $uplineDetails[$cntAA]->getClaimId();
                        // $totalAmount = $uplineDetails[$cntAA]->getCommission();
                      }
                    // }
                    ?>
                </tbody>
            </table><br>
    </div>

    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
