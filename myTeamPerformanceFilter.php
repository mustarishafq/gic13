<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$project = $_POST['project'];

$conn = connDB();

$fullName = $_SESSION['fullname'];

$userDetails = getUser($conn, "WHERE full_name =?",array("full_name"),array($fullName), "s");
$uid = $userDetails[0]->getUid();

$getWho = getWholeDownlineTree($conn, $uid, false);
$no = 0;
$downlineDet = '';
$downlineDet2 = '';
$downlineDet3 = '';
$downlineDet4 = '';
$downlineDet5 = '';

$projectDetails = getProject($conn);
?>
<table id="myTable" class="shipping-table pointer-th">
    <thead>
        <tr>
            <th class="th">NO. <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("PROJECT NAME",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("BOOKING DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("NETT PRICE",8,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("AGENT",8,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("LO SIGNED DATE",15,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("SPA SIGNED DATE",15,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("OVERRIDE COMMISSION (RM)",15,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
        </tr>
    </thead>
    <tbody id="myFilter">
      <?php
        if ($getWho) {
          for ($i=0; $i <count($getWho) ; $i++) {
            $loanDetails = getLoanStatus($conn, "WHERE agent =? AND project_name =?",array("agent","project_name"),array($getWho[$i]->getReferralName(),$project), "ss");
            if ($loanDetails) {
              $no += 1;
              ?>
              <tr>
                <td class="td"><?php echo $no; ?></td>
                <td class="td"><?php echo $loanDetails[0]->getProjectName(); ?></td>
                <td class="td"><?php echo $loanDetails[0]->getUnitNo(); ?></td>
                <td class="td"><?php echo date('d/m/Y',strtotime($loanDetails[0]->getBookingDate())); ?></td>
                <td class="td"><?php echo $loanDetails[0]->getNettPrice(); ?></td>
                <td class="td"><?php echo $loanDetails[0]->getAgent(); ?></td>
                <?php
                if ($loanDetails[0]->getLoSignedDate()) {
                  ?><td class="td"><?php echo date('d/m/Y',strtotime($loanDetails[0]->getLoSignedDate())); ?></td><?php
                }else {
                  ?><td class="td"></td><?php
                }
                if ($loanDetails[0]->getSpaSignedDate()) {
                  ?><td class="td"><?php echo date('d/m/Y',strtotime($loanDetails[0]->getSpaSignedDate())); ?></td><?php
                }else {
                  ?><td class="td"></td><?php
                }
                 ?>
                <td class="td"><?php echo number_format($loanDetails[0]->getAgentComm(),2); ?></td>
              </tr>
              <?php
            }
          }
        }
       ?>
    </tbody>
</table>
