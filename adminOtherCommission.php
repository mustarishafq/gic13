<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/timezone.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/IssuePayroll.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
// require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $otherCommission = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$otherCommission = getIssuePayroll($conn, "WHERE receive_status = 'PENDING' ");
$invoiceDetails = getInvoice($conn);
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Commission To Be Issue | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
input#HistoryBtn {
    width :25px;
    height :25px;
    vertical-align: middle;
    outline: 2px solid maroon;
    outline-offset: -2px;
}
input#checkboxClick{
  width :20px;
  height :20px;
  outline: 2px solid maroon;
  outline-offset: -2px;
}
</style>
<style media="screen">
table {
text-align: left;
position: relative;
}

th {
background: white;
position: sticky;
top: 0;
}
</style>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
  <?php if ($otherCommission) {
    ?><h1 class="h1-title h1-before-border shipping-h1"><a style="color: maroon" href="adminCommission.php">Commission To Be Issue</a> | Other Commission</h1>
      <div class="short-red-border"></div><br>
  <form action="issuePayrollSlip.php" method="POST">
      <!-- <p id="addBack">Issue Multiple Commission Slip :</p> -->
  <!-- <div style="display: none" class="advancedIssue"> -->
    <!-- <button style="color: white;background-color: maroon; border-radius: 5px;padding: 5px" name="submit" type=submit>Issue Multiple Advanced</button> -->
    <!-- <button class="advancedIssue" style="display: none;color: white;background-color: maroon; border-radius: 5px;padding: 5px" name="preview" type=submit>Preview</button> -->
  <!-- </div> -->
      <!-- This is a filter for the table result -->


      <!-- <select class="filter-select clean">
      	<option class="filter-option">Latest Shipping</option>
          <option class="filter-option">Oldest Shipping</option>
      </select> -->

      <!-- End of Filter -->
      <div class="clear"></div>

      <div class="width100 shipping-div2">
          <?php $conn = connDB();?>
              <table id="tableSingleComm" class="shipping-table">
                  <thead>
                      <tr>
                          <th class="th">NO.</th>
                          <th class="th">PROJECT NAME</th>
                          <th class="th">UNIT NO.</th>
                          <th class="th">Agent Name</th>
                          <th class="th">DATE</th>
                          <th class="th">TIME</th>
                          <th class="th">ACTION</th>

                          <!-- <th>INVOICE</th> -->
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                      // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                      // {
                          $otherCommission = getIssuePayroll($conn, "WHERE receive_status = 'PENDING' and branch_type = ?",array("branch_type"),array($_SESSION['branch_type']), "s");
                          if($otherCommission != null)
                          {
                              for($cnt = 0;$cnt < count($otherCommission) ;$cnt++)
                              {
                                // $loanDetails = getLoanStatus($conn, "WHERE project_name = ?",array("project_name"),array($otherCommission[$cnt]->getProjectName()), "s");
                                // if ($loanDetails) {
                                  // for ($cntAA=0; $cntAA <count($loanDetails) ; $cntAA++) {
                                    ?>

                                  <tr>
                                      <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                      <td class="td"><?php echo $cnt + 1;?></td>
                                      <td class="td"><?php echo $otherCommission[$cnt]->getProjectName();?></td>
                                      <td class="td"><?php echo str_replace(",","<br>",$otherCommission[$cnt]->getUnitNo());?></td>
                                      <td class="td"><?php echo $otherCommission[$cnt]->getProjectHandler();?></td>
                                      <td class="td"><?php echo date('d/m/Y', strtotime($otherCommission[$cnt]->getDateCreated())) ?></td>
                                      <td class="td"><?php echo date('h:i a', strtotime($otherCommission[$cnt]->getDateCreated())) ?></td>

                                      <td class="td">
                                            <input type="hidden" name="invoice_id" value="<?php echo $otherCommission[$cnt]->getInvoiceId(); ?>">
                                            <input type="hidden" name="invoice_no" value="<?php echo date('ymd', strtotime($otherCommission[$cnt]->getDateCreated())).$otherCommission[$cnt]->getID(); ?>">
                                            <input type="hidden" name="booking_date" value="<?php echo date('d-m-Y', strtotime($otherCommission[$cnt]->getBookingDate())); ?>">
                                            <a><button class="clean edit-anc-btn hover1 red-link" type="submit" name="id" value="<?php echo $otherCommission[$cnt]->getID();?>">Issue Commission
                                                  <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                                  <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                              </button></a>

                                      </td>

                                  </tr>
                                  <?php
                                  // }
                                // }
                              }
                          }else {
                            ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="7">
                                No Commission Found.
                              </td> <?php
                          }
                      //}
                      ?>
                  </tbody>
              </table>
              <!-- <table style="display: none" id="tableMultipleComm" class="shipping-table"> -->
                  <!-- <thead> -->
                      <!-- <tr> -->
                          <!-- <th class="th">SELECT</th>
                          <th class="th">NO.</th>
                          <th class="th">UNIT NO.</th>
                          <th class="th">Upline Name</th> -->
                          <!-- <th class="th">DATE</th> -->
                          <!-- <th class="th">TIME</th> -->
                      <!-- </tr> -->
                  <!-- </thead> -->
                  <!-- <tbody> -->
                      <?php
                          // $otherCommission = getIssuePayroll($conn, "WHERE receive_status = 'PENDING' and branch_type = ?",array("branch_type"),array($_SESSION['branch_type']), "s");
                          // if($otherCommission != null)
                          // {
                              // for($cnt = 0;$cnt < count($otherCommission) ;$cnt++)
                              // {
                                // $loanDetails = getLoanStatus($conn, "WHERE project_name = ?",array("project_name"),array($otherCommission[$cnt]->getProjectName()), "s");
                                // if ($loanDetails) {
                                  // for ($cntAA=0; $cntAA <count($loanDetails) ; $cntAA++) {
                                    ?>

                                  <!-- <tr> -->
                                      <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                      <!-- <td class="td"><input type="checkbox" name="selectedCommSlip[]" id="checkboxClick" value="<?php //echo $otherCommission[$cnt]->getID() ?>"> </td> -->
                                      <!-- <td class="td"><?php //echo $cnt + 1;?></td> -->
                                      <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getUnitNo();?></td> -->
                                      <!-- <td class="td"><?php //echo $otherCommission[$cnt]->getProjectHandler();?></td> -->
                                      <!-- <td class="td"><?php //echo date('d/m/Y', strtotime($otherCommission[$cnt]->getDateCreated())) ?></td> -->
                                      <!-- <td class="td"><?php //echo date('h:i a', strtotime($otherCommission[$cnt]->getDateCreated())) ?></td> -->
                                      <!-- <input type="hidden" name="uplineName" value="<?php //echo $otherCommission[$cnt]->getProjectHandler() ?>"> -->
                                  <!-- </tr> -->
                                  <?php
                                  // }
                                // }
                              // }
                          // }
                      ?>
                  <!-- </tbody> -->
              <!-- </table> -->
                </form>

      </div>
      <?php $conn->close();?><?php
  }else {
    ?><h2 style="text-align: center">No Commission Slip Found.</h2>
    <h2 style="text-align: center">Make Sure To Completed The Cheque Number Process Before Issue Commission.</h2> <?php
  } ?>


</div>



<?php unset($_SESSION['invoice_id']); ?>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Send Other Commission To Agent.";
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Successfully Updated Other Commission Details.";
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Successfully Updated Commission Slip Details";
        }
        elseif($_GET['type'] == 6)
        {
            $messageType = "Commission Slip Not Selected.";
        }
        elseif($_GET['type'] == 7)
        {
            $messageType = "Upline Name Selected Are Not Same.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script type="text/javascript">
$(document).ready( function(){
  // $("#checkboxToDisplayTableMulti").remove();
  // $("#addBack").append('<input id="checkboxToDisplayTableMulti" type="checkbox" name="headerCheck">');
  // $("#checkboxToDisplayTableMulti").click( function(){
  //   $(".advancedIssue").slideToggle();
  //   $("#tableMultipleComm").toggle();
  //   $("#tableSingleComm").toggle()
  // });
});
</script>
</body>
</html>
