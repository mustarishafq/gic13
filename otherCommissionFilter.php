<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/Commission.php';
require_once dirname(__FILE__) . '/classes/IssuePayroll.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$fullName = $_SESSION['fullname'];

$conn = connDB();
$a = 0;
$adv = 0;
$comm1 = 0;
$comm2 = 0;
$comm3 = 0;
$comm4 = 0;
$comm5 = 0;
$totalAmountFinal = 0;
$userRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($fullName),"s");
$userDetails = $userRows[0];

$project = $_POST['project'];

if (isset($_POST['bookingStartDate'])) {
$dateStart = $_POST['bookingStartDate'];
}else {
$dateStart = "";
}
if (isset($_POST['bookingEndDate'])) {
$dateEnd = $_POST['bookingEndDate'];
}else {
$dateEnd = "";
}
// $bookingDate = date('Y-m-d',strtotime($_POST['bookingDate']));
if ($dateStart) {
  $dateStartN = str_replace("/","-",$dateStart);
  $bookingStartDate = date('Y-m-d',strtotime($dateStartN));
}else{
  $bookingStartDate = '1970-01-01';
}
if ($dateEnd) {
  $dateEndN = str_replace("/","-",$dateEnd);
  $bookingEndDate = date('Y-m-d',strtotime($dateEndN));
}else{
  $bookingEndDate = date('Y-m-d');
}
 ?>
 <table id="myTable" class="shipping-table pointer-th">
     <thead>
         <tr>
             <th class="th">NO. <img src="img/sort.png" class="sort"></th>
             <!-- <th class="th"><?php //echo wordwrap("PROJECT NAME",7,"</br>\n");?></th> -->
             <th class="th">UNIT NO. <img src="img/sort.png" class="sort"></th>
             <th class="th">PROJECT NAME <img src="img/sort.png" class="sort"></th>
             <!-- <th class="th"><?php //echo wordwrap("UNIT NO.",7,"</br>\n");?></th> -->
             <!-- <th class="th"><?php //echo wordwrap("TOTAL COMMISSION",10,"</br>\n");?></th> -->
             <th class="th">TOTAL COMMISSION <img src="img/sort.png" class="sort"></th>
             <!-- <th class="th"><?php// echo wordwrap("1ST CLAIM COMMISSION (RM)",10,"</br>\n");?></th> -->
             <!-- <th class="th"><?php //echo wordwrap("PAYSLIP DATE",10,"</br>\n");?></th> -->
             <th class="th">PAYSLIP DATE <img src="img/sort.png" class="sort"></th>
         </tr>
     </thead>
     <tbody>
         <?php
         // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
         // {
             if ($project) {
               $otherCommission = getIssuePayroll($conn, "WHERE project_handler_default = ? AND receive_date != '' AND date_updated >= ? AND date_updated <= ? AND project_name =?",array("project_handler_default","date_updated","date_updated","project_name"),array($fullName,$bookingStartDate,$bookingEndDate,$project), "ssss");
             }else{
               $otherCommission = getIssuePayroll($conn, "WHERE project_handler_default = ? AND receive_date != '' AND date_updated >= ? AND date_updated <= ?",array("project_handler_default","date_updated","date_updated"),array($fullName,$bookingStartDate,$bookingEndDate), "sss");
             }
             if ($otherCommission) {
               for ($cnt=0; $cnt< count($otherCommission); $cnt++) {
                 $amountExp = explode(",",$otherCommission[$cnt]->getAmount());
                 for ($i=0; $i <count($amountExp) ; $i++) {
                   $totalAmount = $amountExp[$i];
                   $totalAmountFinal = $totalAmountFinal + $totalAmount;
                 }
                 // if ($otherCommission[$cnt]->getReceiveDate()) {
                 ?><tr>
                   <td class="td"><?php echo $cnt + 1?></td>
                   <td class="td"><?php echo $otherCommission[$cnt]->getProjectName() ?></td>
                   <td class="td"><?php echo str_replace(",","<br>",$otherCommission[$cnt]->getUnitNo()) ?></td>
                   <td class="td"><?php echo number_format(str_replace(",","<br>",$totalAmountFinal),2);$totalAmountFinal = 0; ?></td>
                   <td class="td">
                     <form class="" action="issuePayrollSlip.php" method="post">
                       <input type="hidden" name="invoice_no" value="<?php echo date('ymd').$otherCommission[$cnt]->getID() ?>">
                       <input type="hidden" name="invoice_id" value="<?php echo $otherCommission[$cnt]->getID() ?>">
                       <input type="hidden" name="booking_date" value="<?php echo date('d/m/Y',strtotime($otherCommission[$cnt]->getReceiveDate())) ?>">
                       <button class="clean edit-anc-btn hover1" type="submit" name="invoice_id" value="<?php echo $otherCommission[$cnt]->getInvoiceId() ?>">
                         <a><?php echo date('d/m/Y',strtotime($otherCommission[$cnt]->getDateUpdated())) ?></a>
                       </button>
                     </form>
                   </td>
                 </tr> <?php
               // }
             }
             }else {
               ?><td colspan="5" style="text-align: center;font-weight: bold;font-size: 14px" >No Commission Found !</td> <?php
             }

         //}
         ?>
     </tbody>
 </table>
