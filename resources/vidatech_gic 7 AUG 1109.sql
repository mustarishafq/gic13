-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2020 at 05:08 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_gic`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `address_no` varchar(255) NOT NULL,
  `company_branch` varchar(255) DEFAULT NULL,
  `company_address` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `logo`, `company_name`, `address_no`, `company_branch`, `company_address`, `contact`, `branch_type`, `date_created`, `date_updated`) VALUES
(1, 'gic-logo.png', '', '1189044-D (SST No : P11-1808-31038566)', NULL, '1-2-42, Elit Avenue, Jalan Mayang Pasir 3, Bayan Lepas 11950 Penang.', 'Email: gicpenang@gmail.com  Tel:04-6374698', 1, '2020-05-11 10:47:10.5', '2020-05-31 12:11:50.9'),
(2, 'close.png', 'GIC Wealthy Homes International Sdn. Bhd.', '1363274-D (SST No : P11-1808-31038566)', 'Summerton Branch', '110-2-2 Summerton, Persiaran Bayan Indah, 11900 Penang.', 'Email: gicsummerton@gmail.com  Tel:04-6681263', 2, '2020-05-11 10:47:10.5', '2020-05-12 10:32:08.7');

-- --------------------------------------------------------

--
-- Table structure for table `advance_slip`
--

CREATE TABLE `advance_slip` (
  `id` int(255) NOT NULL,
  `advance_id` varchar(255) NOT NULL,
  `unit_no` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `booking_date` varchar(255) DEFAULT NULL,
  `loan_uid` varchar(255) NOT NULL,
  `agent` varchar(255) NOT NULL,
  `agent_default` varchar(255) NOT NULL,
  `ic_no` varchar(255) NOT NULL,
  `amount` decimal(65,2) NOT NULL,
  `status` varchar(255) NOT NULL,
  `check_id` varchar(255) DEFAULT NULL,
  `receive_status` varchar(255) NOT NULL,
  `claim_type` varchar(255) NOT NULL,
  `claim_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NULL DEFAULT NULL ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `advance_slip`
--

INSERT INTO `advance_slip` (`id`, `advance_id`, `unit_no`, `project_name`, `booking_date`, `loan_uid`, `agent`, `agent_default`, `ic_no`, `amount`, `status`, `check_id`, `receive_status`, `claim_type`, `claim_id`, `payment_method`, `branch_type`, `date_created`, `date_updated`) VALUES
(1, 'ca60d1625cf4a566d096024bffa7cf9c', '3-9', 'Quay West', '2020-08-06', 'c1155d54b0cddeedd3d102b998a9f9a9', 'Choong Chee How', 'Choong Chee How', '941217-07-5543', '2000.00', 'PENDING', NULL, 'PENDING', '', '', 'Online Transfer', 1, '2020-08-06 06:33:58.9', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(255) NOT NULL,
  `project_name` varchar(50) NOT NULL,
  `pl_name` varchar(255) NOT NULL,
  `commission` varchar(50) NOT NULL,
  `packages` varchar(255) NOT NULL,
  `present_file` varchar(255) NOT NULL,
  `broshure` varchar(255) NOT NULL,
  `booking_form` varchar(255) NOT NULL,
  `video_link` varchar(255) NOT NULL,
  `createdBy` varchar(50) NOT NULL,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bank_list`
--

CREATE TABLE `bank_list` (
  `id` int(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank_list`
--

INSERT INTO `bank_list` (`id`, `bank_name`) VALUES
(1, 'Affin Bank Berhad'),
(2, 'Affin Hwang Investment Bank Berhad'),
(3, 'Affin Islamic Bank Berhad'),
(4, 'Al Rajhi Banking & Investment Corporation (Malaysia) Berhad'),
(5, 'Alliance Bank Malaysia Berhad'),
(6, 'Alliance Investment Bank Berhad'),
(7, 'Alliance Islamic Bank Berhad'),
(8, 'AmBank (M) Berhad'),
(9, 'AmInvestment Bank Berhad'),
(10, 'AmIslamic Bank Berhad'),
(11, 'Asian Finance Bank Berhad'),
(12, 'Bangkok Bank Berhad'),
(13, 'Bank Islam Malaysia Berhad'),
(14, 'Bank Muamalat Malaysia Berhad'),
(15, 'Bank of America Malaysia Berhad'),
(16, 'Bank of China (Malaysia) Berhad'),
(17, 'Bank of Tokyo-Mitsubishi UFJ (Malaysia) Berhad'),
(18, 'BNP Paribas Malaysia Berhad'),
(19, 'China Construction Bank (Malaysia) Berhad'),
(20, 'CIMB Bank Berhad'),
(21, 'CIMB Investment Bank Berhad'),
(22, 'CIMB Islamic Bank Berhad'),
(23, 'Citibank Berhad'),
(24, 'Deutsche Bank (Malaysia) Berhad'),
(25, 'Hong Leong Bank Berhad'),
(26, 'Hong Leong Investment Bank Berhad'),
(27, 'Hong Leong Islamic Bank Berhad'),
(28, 'HSBC Amanah Malaysia Berhad'),
(29, 'HSBC Bank Malaysia Berhad'),
(30, 'India International Bank Malaysia Berhad'),
(31, 'Industrial and Commercial Bank of China (Malaysia) Berhad'),
(32, 'J.P. Morgan Chase Bank Berhad'),
(33, 'KAF Investment Bank Berhad'),
(34, 'Kenanga Investment Bank Berhad'),
(35, 'Kuwait Finance House (Malaysia) Berhad'),
(36, 'Malayan Banking Berhad'),
(37, 'Maybank Investment Bank Berhad'),
(38, 'Maybank Islamic Berhad'),
(39, 'MIDF Amanah Investment Bank Berhad'),
(40, 'Mizuho Corporate Bank (Malaysia) Berhad'),
(41, 'National Bank of Abu Dhabi Malaysia Berhad'),
(42, 'OCBC Al-Amin Bank Berhad'),
(43, 'OCBC Bank (Malaysia) Berhad'),
(44, 'Public Bank Berhad'),
(45, 'Public Investment Bank Berhad'),
(46, 'Public Islamic Bank Berhad'),
(47, 'RHB Bank Berhad'),
(48, 'RHB Investment Bank Berhad'),
(49, 'RHB Islamic Bank Berhad'),
(50, 'Standard Chartered Bank Malaysia Berhad'),
(51, 'Standard Chartered Saadiq Berhad'),
(52, 'Sumitomo Mitsui Banking Corporation Malaysia Berhad'),
(53, 'The Bank of Nova Scotia Berhad'),
(54, 'The Royal Bank of Scotland Berhad'),
(55, 'United Overseas Bank (Malaysia) Berhad');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` int(255) NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `branch_type` int(255) NOT NULL,
  `creator` varchar(50) NOT NULL,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `branch_name`, `branch_type`, `creator`, `date_created`, `date_updated`) VALUES
(1, 'PG HQ', 1, 'admin1', '2020-05-13 03:29:42.6', '2020-05-13 03:30:14.4'),
(2, 'Summerton', 2, 'admin1', '2020-05-13 03:29:58.8', '2020-05-13 03:30:18.2'),
(3, 'Mension', 3, 'admin1', '2020-05-13 03:29:58.8', '2020-05-13 03:30:18.2');

-- --------------------------------------------------------

--
-- Table structure for table `commission`
--

CREATE TABLE `commission` (
  `id` int(11) NOT NULL,
  `commission_id` varchar(255) NOT NULL,
  `loan_uid` varchar(255) NOT NULL,
  `upline` varchar(255) NOT NULL,
  `upline_default` varchar(255) NOT NULL,
  `upline_type` varchar(10) NOT NULL,
  `ic_no` varchar(255) NOT NULL,
  `def_commission` varchar(255) NOT NULL,
  `charges` varchar(10) NOT NULL,
  `commission` decimal(65,2) NOT NULL,
  `purchaser_name` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `receive_status` varchar(255) NOT NULL,
  `check_id` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `unit_no` varchar(255) DEFAULT NULL,
  `booking_date` varchar(255) DEFAULT NULL,
  `claim_type` varchar(255) NOT NULL,
  `claim_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `edit_history`
--

CREATE TABLE `edit_history` (
  `id` int(255) NOT NULL,
  `loan_uid` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `data_before` varchar(255) NOT NULL,
  `data_after` varchar(255) NOT NULL,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `date_created` timestamp(3) NOT NULL DEFAULT current_timestamp(3)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `edit_history`
--

INSERT INTO `edit_history` (`id`, `loan_uid`, `username`, `details`, `data_before`, `data_after`, `branch_type`, `date_created`) VALUES
(1, 'c1155d54b0cddeedd3d102b998a9f9a9', 'admin2', 'Created', '-', '-', 1, '2020-08-06 06:33:58.930'),
(2, 'ca60d1625cf4a566d096024bffa7cf9c', 'admin2', 'Issued Advance', '-', '-', 1, '2020-08-06 06:33:59.027'),
(3, 'd595b167b70b97fcd2b4529ccc11d5cc', 'admin2', 'Created', '-', '-', 1, '2020-08-07 02:21:32.666'),
(4, 'd595b167b70b97fcd2b4529ccc11d5cc', 'admin2', 'SQ FT', '', '1200', 1, '2020-08-07 02:22:35.002'),
(5, 'd595b167b70b97fcd2b4529ccc11d5cc', 'admin2', 'Rate', '3', '4', 1, '2020-08-07 02:22:35.102'),
(6, 'd595b167b70b97fcd2b4529ccc11d5cc', 'admin2', 'Agent Comm', '10500', '14000', 1, '2020-08-07 02:22:35.193'),
(7, 'd595b167b70b97fcd2b4529ccc11d5cc', 'admin2', 'Upline 1', 'Teh Han Hoe', 'Kenny Teh', 1, '2020-08-07 02:22:35.313'),
(8, 'd595b167b70b97fcd2b4529ccc11d5cc', 'admin2', 'Upline 2', 'Oon Liang Siang', 'Patrick Oon', 1, '2020-08-07 02:22:35.346'),
(9, 'd595b167b70b97fcd2b4529ccc11d5cc', 'admin2', 'Upline 3', 'Song Meng Wai', 'Eddie Song', 1, '2020-08-07 02:22:35.377'),
(10, 'd595b167b70b97fcd2b4529ccc11d5cc', 'admin2', 'Upline 4', '', '-', 1, '2020-08-07 02:22:35.402'),
(11, 'd595b167b70b97fcd2b4529ccc11d5cc', 'admin2', 'Upline 5', '', '-', 1, '2020-08-07 02:22:35.435'),
(12, 'd595b167b70b97fcd2b4529ccc11d5cc', 'admin2', 'GIC Profit', '7035', '2100', 1, '2020-08-07 02:22:35.478');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(255) NOT NULL,
  `loan_uid` varchar(255) NOT NULL,
  `invoice_id` varchar(255) NOT NULL,
  `purchaser_name` varchar(255) NOT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `unit_no` varchar(255) DEFAULT NULL,
  `booking_date` varchar(255) DEFAULT NULL,
  `project_handler` varchar(255) DEFAULT NULL,
  `project_handler_default` varchar(255) NOT NULL,
  `claims_status` varchar(255) DEFAULT NULL,
  `claims_date` varchar(255) DEFAULT NULL,
  `invoice_name` varchar(255) DEFAULT NULL,
  `invoice_type` varchar(255) DEFAULT NULL,
  `item` varchar(255) NOT NULL,
  `item2` varchar(255) DEFAULT NULL,
  `item3` varchar(255) DEFAULT NULL,
  `item4` varchar(255) DEFAULT NULL,
  `item5` varchar(255) DEFAULT NULL,
  `remark` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `amount2` decimal(65,0) DEFAULT NULL,
  `amount3` decimal(65,0) DEFAULT NULL,
  `amount4` decimal(65,0) DEFAULT NULL,
  `amount5` decimal(65,0) DEFAULT NULL,
  `unit` varchar(255) NOT NULL,
  `unit2` varchar(255) NOT NULL,
  `unit3` varchar(255) NOT NULL,
  `unit4` varchar(255) NOT NULL,
  `unit5` varchar(255) NOT NULL,
  `project` varchar(255) NOT NULL,
  `charges` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `final_amount` varchar(255) NOT NULL,
  `bank_account_holder` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_account_no` varchar(255) NOT NULL,
  `check_id` varchar(255) NOT NULL,
  `receive_status` varchar(255) NOT NULL,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `credit_status` varchar(3) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_credit`
--

CREATE TABLE `invoice_credit` (
  `id` int(11) NOT NULL,
  `credit_id` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `unit_no` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `project_handler` varchar(255) NOT NULL,
  `project_handler_default` varchar(255) NOT NULL,
  `invoice_name` varchar(255) NOT NULL,
  `invoice_type` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `charges` varchar(255) NOT NULL,
  `receive_status` varchar(255) NOT NULL,
  `claims_status` varchar(255) NOT NULL,
  `receive_date` varchar(255) DEFAULT NULL,
  `check_id` varchar(255) NOT NULL,
  `final_amount` varchar(255) NOT NULL,
  `bank_account_holder` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_account_no` varchar(255) NOT NULL,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `credit_status` varchar(3) NOT NULL,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_multi`
--

CREATE TABLE `invoice_multi` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(255) NOT NULL,
  `loan_id` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `unit_no` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `project_handler` varchar(255) NOT NULL,
  `project_handler_default` varchar(255) NOT NULL,
  `invoice_name` varchar(255) NOT NULL,
  `invoice_type` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `charges` varchar(255) NOT NULL,
  `receive_status` varchar(255) NOT NULL,
  `claims_status` varchar(255) NOT NULL,
  `receive_date` varchar(255) DEFAULT NULL,
  `check_id` varchar(255) NOT NULL,
  `final_amount` varchar(255) NOT NULL,
  `bank_account_holder` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_account_no` varchar(255) NOT NULL,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `credit_status` varchar(3) NOT NULL,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_other`
--

CREATE TABLE `invoice_other` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `unit_no` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `project_handler` varchar(255) NOT NULL,
  `project_handler_default` varchar(255) NOT NULL,
  `invoice_name` varchar(255) NOT NULL,
  `invoice_type` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `charges` varchar(255) NOT NULL,
  `receive_status` varchar(255) NOT NULL,
  `claims_status` varchar(255) NOT NULL,
  `receive_date` varchar(255) DEFAULT NULL,
  `check_id` varchar(255) NOT NULL,
  `final_amount` varchar(255) NOT NULL,
  `bank_account_holder` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_account_no` varchar(255) NOT NULL,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `display` varchar(5) NOT NULL DEFAULT 'Yes',
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_proforma`
--

CREATE TABLE `invoice_proforma` (
  `id` int(11) NOT NULL,
  `proforma_id` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `unit_no` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `project_handler` varchar(255) NOT NULL,
  `project_handler_default` varchar(255) NOT NULL,
  `invoice_name` varchar(255) NOT NULL,
  `invoice_type` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `charges` varchar(255) NOT NULL,
  `receive_status` varchar(255) NOT NULL,
  `claims_status` varchar(255) NOT NULL,
  `receive_date` varchar(255) DEFAULT NULL,
  `check_id` varchar(255) NOT NULL,
  `final_amount` varchar(255) NOT NULL,
  `bank_account_holder` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_account_no` varchar(255) NOT NULL,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `issue_payroll`
--

CREATE TABLE `issue_payroll` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `unit_no` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `project_handler` varchar(255) NOT NULL,
  `project_handler_default` varchar(255) NOT NULL,
  `invoice_name` varchar(255) NOT NULL,
  `invoice_type` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `charges` varchar(255) NOT NULL,
  `receive_status` varchar(255) NOT NULL,
  `claims_status` varchar(255) NOT NULL,
  `receive_date` varchar(255) DEFAULT NULL,
  `check_id` varchar(255) NOT NULL,
  `final_amount` varchar(255) NOT NULL,
  `bank_account_holder` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_account_no` varchar(255) NOT NULL,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `loan_status`
--

CREATE TABLE `loan_status` (
  `id` bigint(25) NOT NULL,
  `display` varchar(255) NOT NULL DEFAULT 'Yes',
  `project_name` varchar(255) NOT NULL,
  `loan_uid` varchar(255) NOT NULL,
  `unit_no` varchar(255) NOT NULL,
  `purchaser_name` varchar(255) NOT NULL,
  `ic` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `booking_date` varchar(255) DEFAULT NULL,
  `sq_ft` varchar(255) NOT NULL,
  `spa_price` varchar(255) NOT NULL,
  `package` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL,
  `rebate` varchar(255) NOT NULL,
  `extra_rebate` varchar(255) NOT NULL,
  `nettprice` varchar(255) NOT NULL,
  `totaldevelopercomm` varchar(255) NOT NULL,
  `agent` varchar(255) NOT NULL,
  `loanstatus` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `bform_Collected` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `lawyer` varchar(255) NOT NULL,
  `pending_approval_status` varchar(255) NOT NULL,
  `bank_approved` varchar(255) NOT NULL,
  `loan_amount` varchar(255) NOT NULL,
  `lo_signed_date` varchar(255) NOT NULL,
  `la_signed_date` varchar(255) NOT NULL,
  `spa_signed_date` varchar(255) NOT NULL,
  `fullset_completed` varchar(255) NOT NULL,
  `cash_buyer` varchar(255) NOT NULL,
  `cancelled_booking` varchar(255) NOT NULL,
  `case_status` varchar(255) NOT NULL,
  `event_personal` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `agent_comm` varchar(255) NOT NULL,
  `upline1` varchar(255) NOT NULL,
  `upline2` varchar(255) NOT NULL,
  `upline3` varchar(50) NOT NULL,
  `upline4` varchar(50) NOT NULL,
  `upline5` varchar(50) NOT NULL,
  `pl_name` varchar(255) NOT NULL,
  `hos_name` varchar(255) NOT NULL,
  `lister_name` varchar(255) NOT NULL,
  `ul_override` varchar(255) NOT NULL,
  `uul_override` varchar(255) NOT NULL,
  `uuul_override` varchar(50) NOT NULL,
  `uuuul_override` varchar(50) NOT NULL,
  `uuuuul_override` varchar(50) NOT NULL,
  `pl_override` varchar(255) NOT NULL,
  `hos_override` varchar(255) NOT NULL,
  `lister_override` varchar(255) NOT NULL,
  `admin1_override` varchar(255) NOT NULL,
  `admin2_override` varchar(255) NOT NULL,
  `admin3_override` varchar(255) NOT NULL,
  `gic_profit` varchar(255) NOT NULL,
  `total_claimed_dev_amt` int(255) NOT NULL,
  `total_bal_unclaim_amt` int(255) NOT NULL,
  `pdf` varchar(255) DEFAULT NULL,
  `pdf_date` varchar(255) DEFAULT NULL,
  `status_1st` varchar(255) NOT NULL,
  `1st_claim_amt` varchar(255) NOT NULL,
  `sst1` varchar(255) DEFAULT NULL,
  `request_date1` varchar(255) DEFAULT NULL,
  `check_no1` varchar(255) DEFAULT NULL,
  `receive_date1` varchar(255) DEFAULT NULL,
  `status_2nd` varchar(255) NOT NULL,
  `2nd_claim_amt` varchar(255) NOT NULL,
  `sst2` varchar(255) DEFAULT NULL,
  `request_date2` varchar(255) DEFAULT NULL,
  `check_no2` varchar(255) DEFAULT NULL,
  `receive_date2` varchar(255) DEFAULT NULL,
  `status_3rd` varchar(255) NOT NULL,
  `3rd_claim_amt` varchar(255) NOT NULL,
  `sst3` varchar(255) DEFAULT NULL,
  `request_date3` varchar(255) DEFAULT NULL,
  `check_no3` varchar(255) DEFAULT NULL,
  `receive_date3` varchar(255) DEFAULT NULL,
  `status_4th` varchar(255) NOT NULL,
  `4th_claim_amt` varchar(255) NOT NULL,
  `sst4` varchar(25) DEFAULT NULL,
  `request_date4` varchar(255) DEFAULT NULL,
  `check_no4` varchar(255) DEFAULT NULL,
  `receive_date4` varchar(255) DEFAULT NULL,
  `status_5th` varchar(255) NOT NULL,
  `5th_claim_amt` varchar(255) NOT NULL,
  `sst5` varchar(25) DEFAULT NULL,
  `agent_balance` varchar(255) NOT NULL,
  `ul_balance` varchar(20) NOT NULL,
  `uul_balance` varchar(20) NOT NULL,
  `uuul_balance` varchar(20) NOT NULL,
  `uuuul_balance` varchar(20) NOT NULL,
  `uuuuul_balance` varchar(20) NOT NULL,
  `pl_balance` varchar(20) NOT NULL,
  `lister_balance` varchar(20) NOT NULL,
  `hos_balance` varchar(20) NOT NULL,
  `request_date5` varchar(255) DEFAULT NULL,
  `check_no5` varchar(255) DEFAULT NULL,
  `receive_date5` varchar(255) DEFAULT NULL,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loan_status`
--

INSERT INTO `loan_status` (`id`, `display`, `project_name`, `loan_uid`, `unit_no`, `purchaser_name`, `ic`, `contact`, `email`, `booking_date`, `sq_ft`, `spa_price`, `package`, `discount`, `rebate`, `extra_rebate`, `nettprice`, `totaldevelopercomm`, `agent`, `loanstatus`, `remark`, `bform_Collected`, `payment_method`, `lawyer`, `pending_approval_status`, `bank_approved`, `loan_amount`, `lo_signed_date`, `la_signed_date`, `spa_signed_date`, `fullset_completed`, `cash_buyer`, `cancelled_booking`, `case_status`, `event_personal`, `rate`, `agent_comm`, `upline1`, `upline2`, `upline3`, `upline4`, `upline5`, `pl_name`, `hos_name`, `lister_name`, `ul_override`, `uul_override`, `uuul_override`, `uuuul_override`, `uuuuul_override`, `pl_override`, `hos_override`, `lister_override`, `admin1_override`, `admin2_override`, `admin3_override`, `gic_profit`, `total_claimed_dev_amt`, `total_bal_unclaim_amt`, `pdf`, `pdf_date`, `status_1st`, `1st_claim_amt`, `sst1`, `request_date1`, `check_no1`, `receive_date1`, `status_2nd`, `2nd_claim_amt`, `sst2`, `request_date2`, `check_no2`, `receive_date2`, `status_3rd`, `3rd_claim_amt`, `sst3`, `request_date3`, `check_no3`, `receive_date3`, `status_4th`, `4th_claim_amt`, `sst4`, `request_date4`, `check_no4`, `receive_date4`, `status_5th`, `5th_claim_amt`, `sst5`, `agent_balance`, `ul_balance`, `uul_balance`, `uuul_balance`, `uuuul_balance`, `uuuuul_balance`, `pl_balance`, `lister_balance`, `hos_balance`, `request_date5`, `check_no5`, `receive_date5`, `branch_type`, `date_created`, `date_updated`) VALUES
(1, 'Yes', 'Quay West', 'c1155d54b0cddeedd3d102b998a9f9a9', '3-9', 'Clark Kent', '965207-02-8569', '+6019-242342', 'email@email.com', '2020-08-06', '', '350000', '', '', '', '', '350000', '21000', 'Choong Chee How', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'COMPLETED', '', '3', '10500', 'Hng Chun Siang', 'Wee Guan Seng', 'Teh Han Hoe', 'Oon Liang Siang', 'Song Meng Wai', 'Kirby Lee', 'Jonathan', '', '525', '525', '315', '105', '105', '1575', '525', '0', '0', '0', '0', '6825', 0, 21000, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, '8500,0', '525,0', '525,0', '315,0', '315,0', '105,0', '1575,0', ',0', '525,0', NULL, NULL, NULL, 1, '2020-08-06 06:33:58', '2020-08-06 06:33:58'),
(2, 'Yes', 'Mont Residences', 'd595b167b70b97fcd2b4529ccc11d5cc', '16-05', 'Clark Kent', '12', '+6019-242342', 'email@email.com', '2020-08-07', '1200', '350000', '', '', '', '', '350000', '21000', 'Tay Poh Leng', '', '', '', '', '', '', 'Bank of Tokyo-Mitsubishi UFJ (Malaysia) Berhad', '', '', '', '', '', '', '', '', '', '4', '14000', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '-', '-', 'Patrick Oon', 'Jonathan', '', '700', '700', '420', '140', '140', '2100', '700', '0', '0', '0', '0', '2100', 0, 21000, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '', '', NULL, '14000,0', '700,0', '420,0', '315,0', '140,0', '140,0', '2100,0', ',0', '700,0', NULL, NULL, NULL, 1, '2020-08-07 02:21:32', '2020-08-07 02:22:35');

-- --------------------------------------------------------

--
-- Table structure for table `payment_list`
--

CREATE TABLE `payment_list` (
  `id` int(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_list`
--

INSERT INTO `payment_list` (`id`, `payment_method`) VALUES
(1, 'Developer Merchant'),
(2, 'GIC Merchant'),
(3, 'TT to Developer'),
(4, 'TT to GIC'),
(5, 'Cheque');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` bigint(20) NOT NULL,
  `project_id` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `add_projectppl` varchar(255) NOT NULL,
  `project_leader` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `claims_no` varchar(255) NOT NULL,
  `other_claim` varchar(255) NOT NULL,
  `display` varchar(11) NOT NULL DEFAULT 'Yes',
  `creator_name` varchar(255) NOT NULL,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `data_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `data_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `project_id`, `project_name`, `add_projectppl`, `project_leader`, `company_name`, `company_address`, `claims_no`, `other_claim`, `display`, `creator_name`, `branch_type`, `data_created`, `data_updated`) VALUES
(1, 'bff3cf4778de943db52ab2a1143848c9', 'Quay West', 'Account Department', 'Kirby Lee', 'Triple Three Properties Sdn Bhd', '36A Jalan Sultan Ahmad Shah,10050 Georgetown Penang,Malaysia.', '1', '0', 'Yes', 'admin2', 1, '2020-03-05 03:21:46', '2020-06-13 12:13:42'),
(2, '6ca7b4d2d958a3e5042e47d55b7e04af', 'Zen 6', 'Jeffrey Tan', 'Jojo Cheah', 'Hemat Tuah Sdn Bhd ', '81-11-6 Ivory Tower,Jalan Dato Keramat ,10150 Georgetown Penang. ', '3', '0', 'Yes', 'admin2', 1, '2020-03-05 03:32:47', '2020-04-01 09:11:40'),
(3, 'f9fdbd8ab36b6d03896b157ecaaaaccf', 'M3 (GIM)', 'Noor Azrin', 'Jeffrey Chan', 'Galeri Tropika Sdn Bhd', 'Menara NCT, No2 Jalan BP 4/9, Bandar Bukit Puchong, 47100 Puchong, Selangor.', '', '0', 'Yes', 'admin2', 1, '2020-03-05 03:33:17', '2020-03-25 06:11:37'),
(4, '5ab1dcb31a1f70cb36b3af686eea7808', 'Mont Residences', 'Joanne', 'Patrick Oon', 'Visentak Capital Sdn Bhd', 'Wisma VST,No. 37 Jalan Anson,10400 Penang.', '1', '0', 'Yes', 'admin2', 1, '2020-03-05 03:41:14', '2020-03-25 06:18:37'),
(5, '581c6b360d440a41b86973fae66a5aee', 'Riveria City', 'Mr. Tan', 'Patrick Oon', 'Avant Garde Interior Designers Sdn Bhd', '52a Lebuh Enggang,41150 Klang Selangor.,', '', '0', 'Yes', 'admin2', 1, '2020-03-05 03:43:24', '2020-03-25 06:17:30'),
(6, '0d244a00236993150e4fce3e5e10c446', 'VOS', 'Jowin', 'Eddie Song', 'Inma Development Sdn Bhd', '3A Tingkat Bukit Dumbar, 11600 Penang,', '', '0', 'Yes', 'admin2', 1, '2020-03-05 03:43:56', '2020-03-21 08:31:59'),
(7, 'f0e303c595dbb2c58d75415f252723cf', 'Golden Triangle 2', 'K.K', 'Jeffrey Chan', 'GSV Development Sdn. Bhd.', 'No. 72-2-36, Arena Curve, Jalan Mahsuri, Bayan Baru, 11950 Bayan Lepas, Penang.', '1', '0', 'Yes', 'admin2', 1, '2020-03-05 03:44:59', '2020-03-25 06:13:11'),
(8, '9c7fd440859ffe867e6a13c1e5c40dfb', 'Arte S', 'Jerror', '', '', '\"\"', '', '0', 'No', 'admin2', 1, '2020-03-05 03:45:18', '2020-03-18 08:09:27'),
(9, '7ffc8c84679d21222ca8a377246d722d', 'Arte S', 'Mr Phun', 'Jonathan Ch\'ng', 'Ceria Development Sdn Bhd', 'No. 1-L6-09 Metro Avenue ,Lebuhraya Dr Lim Chong Eu,Lintang Hajjah Rehmah ,11600 Jelutong Penang,', '2', '0', 'Yes', 'admin2', 1, '2020-03-05 03:48:06', '2020-03-25 06:15:52'),
(10, 'fa644ead004f4b222a175af9473debb9', 'Queens Waterfront', 'Account Department', 'Jonathan Ch\'ng', 'Ideal GIM Venture Sdn Bhd', 'No.36 & 38 Lorong Perda Selatan 2,Bandar Perda ,14000 Bukit Mertajam Penang', '1', '0', 'Yes', 'admin2', 1, '2020-03-05 03:59:03', '2020-03-25 06:14:31'),
(11, '29b9ca4e162dceef6c810c5a00042eb3', 'N-City', '', 'Patrick Oon', 'Galeri Tropika Sdn Bhd', 'Menara NCT No2 Jalan BP 4/9,Bandar Bukit Puchong 47100 Puchong,Selangor.', '', '0', 'Yes', 'admin1', 1, '2020-03-27 10:25:13', '2020-03-27 10:25:13'),
(12, 'f29f1cbffd15e5bc3701aa318ab4f46d', 'The Zen Affordable', '', 'Kirby Lee', '', ',,', '', '0', 'Yes', 'admin1', 1, '2020-03-27 10:28:34', '2020-03-27 10:28:34'),
(13, '58bae9885f99e9580c966f900ba5681e', 'Project X', 'Mr Xmas', 'Jonathan Chng', 'X Group Ltd', 'X Building,Jalan Xmas,11500', '3', '0', 'Yes', 'admin2', 1, '2020-06-16 03:48:40', '2020-06-25 06:35:05'),
(14, 'a50c26044545db358032d0593fd892ac', 'Project Y', 'Mr Y', 'Jonathan Chng,Kirby Lee', '1-10-12A Building Y ', 'Jalan Y Alibaba North South,11500 Penang Malaysia,', '2', '0', 'Yes', 'admin2', 1, '2020-06-18 02:09:17', '2020-06-18 02:09:17'),
(15, 'dd14d4438dc8361534c0caffee2a6d8b', 'The Stone', 'Mr Cheng', 'Jeffrey Chan', 'Stone', 'Stone Stone,,', '1', '0', 'Yes', 'summerton1', 2, '2020-07-07 12:48:26', '2020-07-07 12:48:26');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '94db2be13431a5e704eeafa9f095d387', 'Chan Jiang Liang', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:03:15', '2020-08-05 06:56:28'),
(2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '5013c34fa2dd942d183206e6180ce31a', 'Chng Zhi Fong', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:03:16', '2020-08-05 06:56:28'),
(3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '92d5de2ab4e6663f8e12c43edda816a7', 'Oon Liang Siang', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:03:16', '2020-08-05 04:27:07'),
(4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', 'a078a1fb7bedbebda56f02cac8b78640', 'Lee Kong Beng', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:03:16', '2020-08-05 06:56:28'),
(5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '63a2880c352d5ecdbe87be559fcaf684', 'Choi Yu Nen', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:03:16', '2020-08-05 06:56:32'),
(6, '94db2be13431a5e704eeafa9f095d387', '9b7d208a25b02b55fa23046c37cbdc57', 'Ho Pin Chuan', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:08:59', '2020-08-05 06:56:27'),
(7, '94db2be13431a5e704eeafa9f095d387', 'f4ab21f4b78e7a32d858e5e9017aaa0c', 'Foo Yi Heng', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:08:59', '2020-08-05 06:56:28'),
(8, '94db2be13431a5e704eeafa9f095d387', '13b28386a93b9c9fcca3cf0f8ddff3e7', 'Lee Siew Ling', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:00', '2020-08-05 06:56:32'),
(9, '94db2be13431a5e704eeafa9f095d387', '7290db07da57fa6acc3f5a3b60fd9111', 'Lim Jeng Hui', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:00', '2020-08-05 06:56:33'),
(10, '5013c34fa2dd942d183206e6180ce31a', 'f891940dd0798daaf05a9206062b6bb6', 'Yeap Chia Heng', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:00', '2020-08-05 06:56:28'),
(11, '5013c34fa2dd942d183206e6180ce31a', '6da630e86dcfe2d3c6b8f700d9260c5e', 'Eddy Wong Chee Wei', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:00', '2020-08-05 06:56:31'),
(12, '5013c34fa2dd942d183206e6180ce31a', '7af526fb362c7f6fadd06ffb1b6f2fec', 'Goh Hooi Choon', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:00', '2020-08-05 06:56:35'),
(13, '92d5de2ab4e6663f8e12c43edda816a7', 'ff62753dbe4f4a2543fe2519a5502fbd', 'Chng Lay Pheng', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:00', '2020-08-05 06:56:27'),
(14, '92d5de2ab4e6663f8e12c43edda816a7', '52e11e75ec312a3aad98e7def5ac9d36', 'Tan Beng Hwa', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:00', '2020-08-05 06:56:27'),
(15, '92d5de2ab4e6663f8e12c43edda816a7', 'd250bfabf80727bdbb2944910beba57a', 'Teh Kok Teng', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:00', '2020-08-05 06:56:27'),
(16, '92d5de2ab4e6663f8e12c43edda816a7', 'db8758e22aa9296baad4a2976c763f0e', 'Chew Choon Seng', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:01', '2020-08-05 06:56:27'),
(17, '92d5de2ab4e6663f8e12c43edda816a7', '80226560a485b3db7104636655e93d9d', 'Tan Chia Inn', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:01', '2020-08-05 06:56:28'),
(18, '92d5de2ab4e6663f8e12c43edda816a7', 'b16041d5ac9d536cf171e376a19ecd40', 'Teh Han Hoe', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:01', '2020-08-05 06:56:28'),
(19, '92d5de2ab4e6663f8e12c43edda816a7', '24c069d7ef24cdd18c2459e43857268b', 'Loy Ter Ren', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:01', '2020-08-05 06:56:28'),
(20, '92d5de2ab4e6663f8e12c43edda816a7', '6862b34b0c2111cd6bc47eafadb23d38', 'Ooi Chin Aun', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:01', '2020-08-05 06:56:28'),
(21, '92d5de2ab4e6663f8e12c43edda816a7', '5f53420800398abe8854294cee49b43a', 'Law Kim Leong', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:01', '2020-08-05 06:56:28'),
(22, '92d5de2ab4e6663f8e12c43edda816a7', 'b8d69647e0b37332bec9ea91c13aa89b', 'Tan Moh Loon', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:01', '2020-08-05 06:56:30'),
(23, '92d5de2ab4e6663f8e12c43edda816a7', 'b08ecee761bf76ce702167d781ad2043', 'Ong Hock Lye', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:01', '2020-08-05 06:56:33'),
(24, 'a078a1fb7bedbebda56f02cac8b78640', 'a3210a0f35f5761cd0e1e6118cbae2bf', 'Lim Sions Hock', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:02', '2020-08-05 06:56:30'),
(25, 'a078a1fb7bedbebda56f02cac8b78640', '2acce25e5f509361b41159b5b719bb46', 'Tan Yeong Chuan', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:02', '2020-08-05 06:56:30'),
(26, 'a078a1fb7bedbebda56f02cac8b78640', '526f6422922b0149fdc0a5938f313a07', 'Khor Yu Nong', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:02', '2020-08-05 06:56:30'),
(27, 'a078a1fb7bedbebda56f02cac8b78640', '876e51de1a904687496ea73e8cbeb25e', 'Tan Horng Lin', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:02', '2020-08-05 06:56:30'),
(28, 'a078a1fb7bedbebda56f02cac8b78640', 'aae13497bbbb45f0ebc7ce7e22f03be4', 'Kuhen A/L Kacinathan', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:03', '2020-08-05 06:56:30'),
(29, 'a078a1fb7bedbebda56f02cac8b78640', '3fd3161fb94f6c7378395ea0fa809c0f', 'Ang Hwei Li', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:03', '2020-08-05 06:56:32'),
(30, 'a078a1fb7bedbebda56f02cac8b78640', 'ee9474dfc566c665f8886d3bf9ded2ed', 'Law Choon Keat', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:09:03', '2020-08-05 06:56:32'),
(31, 'f4ab21f4b78e7a32d858e5e9017aaa0c', 'fac3e2951fdd3614d3af6437c63db412', 'Chuah Eng Hong', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:52', '2020-08-05 06:56:27'),
(32, 'f4ab21f4b78e7a32d858e5e9017aaa0c', '7bafa37724fd38e79b05b346e9f10c0f', 'Lim Chun Ti', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:52', '2020-08-05 06:56:30'),
(33, 'f4ab21f4b78e7a32d858e5e9017aaa0c', 'e070c0529480ecd35ca4a4a9566a05de', 'Lim Ean Ean', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:52', '2020-08-05 06:56:31'),
(34, 'f891940dd0798daaf05a9206062b6bb6', '9e668e3fb20ef8711584501e53464c9a', 'Loe Yew Lee', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:52', '2020-08-05 06:56:27'),
(35, 'f891940dd0798daaf05a9206062b6bb6', 'c1758a259b8f2aa742f445857d54b52d', 'Yeoh Chin Yong', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:52', '2020-08-05 06:56:28'),
(36, 'f891940dd0798daaf05a9206062b6bb6', 'c2f00eda1baee189eea8a581f744cd31', 'Yeap Tan Kheng', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:52', '2020-08-05 06:56:28'),
(37, 'f891940dd0798daaf05a9206062b6bb6', '4fc96a3395b46b89fb30c1722dfb1196', 'Tan Soo Shean', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:29'),
(38, 'f891940dd0798daaf05a9206062b6bb6', 'ca7473adc1cd6887e5b5387fc83e9ba5', 'Lee Kok Seong', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:31'),
(39, '6da630e86dcfe2d3c6b8f700d9260c5e', '72d9c75df25b8e5eb536883f6507de3f', 'Lim Jin Tek', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:31'),
(40, '7af526fb362c7f6fadd06ffb1b6f2fec', 'e152cd997de7e161a06f1e469bf24333', 'Goh Hooi Li', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:34'),
(41, '7af526fb362c7f6fadd06ffb1b6f2fec', 'fa9fa734ff77ce79395277e8bafeaf55', 'Liew Chee Hiang', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:35'),
(42, '7af526fb362c7f6fadd06ffb1b6f2fec', '6decb52a7f9790318a44cc1aca50d192', 'Chew Khai Lyn', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:35'),
(43, '7af526fb362c7f6fadd06ffb1b6f2fec', 'd8c2732d0378a1a7835c1e4260866e7d', 'Ng Yong Qiang', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:35'),
(44, '7af526fb362c7f6fadd06ffb1b6f2fec', 'b535e8f9f4879d7de5008c3f0708626b', 'Teoh Li Liang', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:35'),
(45, '7af526fb362c7f6fadd06ffb1b6f2fec', 'a4518d0a930828f45a57d4e6dd2107e8', 'Ong Siew Suan', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:35'),
(46, '7af526fb362c7f6fadd06ffb1b6f2fec', 'aa51ed180a43499a37e1a9e972b0cbc5', 'Chew Chee Ming', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:35'),
(47, '7af526fb362c7f6fadd06ffb1b6f2fec', '836d753ed4e852ee1493714648f2d055', 'Chee Hong Wei', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:36'),
(48, 'd250bfabf80727bdbb2944910beba57a', '7bd22a2ce76f3e0acbb7eb815138ed85', 'Caron Wong ', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:27'),
(49, 'd250bfabf80727bdbb2944910beba57a', '5d3bb097f481195b4525e185b3b4d12e', 'Ong Beng Fung', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:31'),
(50, '80226560a485b3db7104636655e93d9d', '3696b5240d9919babffe0d520cb8c8fa', 'Ong Jo Yee', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:31'),
(51, 'b16041d5ac9d536cf171e376a19ecd40', 'cd4fa46fe669724f4d7cd9342cec2702', 'Lim Siew Hua', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:53', '2020-08-05 06:56:29'),
(52, 'b16041d5ac9d536cf171e376a19ecd40', '81816798b89aa643d83cab1efcab877b', 'Ooi Yeong Jiunn', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:54', '2020-08-05 06:56:29'),
(53, 'b16041d5ac9d536cf171e376a19ecd40', '25afe8e85c61914754d64eeb4ffc71ce', 'Wee Guan Seng', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:54', '2020-08-05 06:56:33'),
(54, 'b16041d5ac9d536cf171e376a19ecd40', '0b3caa5bc6f5c6e1175af1f16272e7ad', 'Tay Poh Leng', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:54', '2020-08-05 06:56:37'),
(55, 'b8d69647e0b37332bec9ea91c13aa89b', 'b668d014c8510380fdef27c192208446', 'Tan Chin Eng', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:54', '2020-08-05 06:56:32'),
(56, '3fd3161fb94f6c7378395ea0fa809c0f', '63e3a437bedb60381c8408918c0d9873', 'Chan Moy Chin', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:54', '2020-08-05 06:56:33'),
(57, '3fd3161fb94f6c7378395ea0fa809c0f', 'fe0c8970817adb1235f948f3d6108606', 'Chiam Hooi Chai', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:14:54', '2020-08-05 06:56:34'),
(58, 'fac3e2951fdd3614d3af6437c63db412', '7e15a52fad7f687a387db70e99d52365', 'Ng Dian Sheng', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:29'),
(59, 'fac3e2951fdd3614d3af6437c63db412', '57a628f9aeb9bfa926a2ca30639fa742', 'Ben Liau Tze Chen', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:29'),
(60, 'fac3e2951fdd3614d3af6437c63db412', 'c465428dc88ccf3d6311eb5001f5603c', 'Ooi Hong Seng', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:29'),
(61, 'fac3e2951fdd3614d3af6437c63db412', '6cafc733e24f24f8786003bbc86d4390', 'Lim Beng See', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:30'),
(62, 'e070c0529480ecd35ca4a4a9566a05de', '42beebadca755ea38ad7f6f595440817', 'Yeap Bee Chin', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:33'),
(63, '9e668e3fb20ef8711584501e53464c9a', '451fdc7d8656f8e9aef524b516b901f1', 'Tang Wei Ther', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:31'),
(64, '9e668e3fb20ef8711584501e53464c9a', 'fdc7a6516c7a4d1ed8bed14c39797f8b', 'Tan Lay Khim', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:32'),
(65, '72d9c75df25b8e5eb536883f6507de3f', '51121ba7070971637922fb78b751d2b5', 'Khoo Lit Wei', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:31'),
(66, '72d9c75df25b8e5eb536883f6507de3f', '9de5b19a420f03c4895a5eeeeddd7bd0', 'Chong Ming Yee', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:31'),
(67, '72d9c75df25b8e5eb536883f6507de3f', 'bb90608bab6f022475f5fd07b479c1f0', 'Lim Poh Suan', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:31'),
(68, '72d9c75df25b8e5eb536883f6507de3f', 'e6c3897b7f3e0f0e62521293e1a80b42', 'Kum Kok Leong', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 07:22:56'),
(69, 'e152cd997de7e161a06f1e469bf24333', 'd405cb8094f86403894e8826609ca012', 'Tan Hooi Ping', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:34'),
(70, 'e152cd997de7e161a06f1e469bf24333', '2283dc4a74c736f21066ecae16a62c50', 'Chua Siew Im', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:34'),
(71, 'e152cd997de7e161a06f1e469bf24333', 'e7dacd7d47a7563c0977e2053d6b0807', 'Goh Peek Goon', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:35'),
(72, 'fa9fa734ff77ce79395277e8bafeaf55', '8727fe9dfd7312e617c131981f2921d1', 'Lee Sun Hor', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:35'),
(73, 'fa9fa734ff77ce79395277e8bafeaf55', 'e5b7f71f524c67801dc40f952fb3e379', 'Loh Jia How', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:35'),
(74, 'fa9fa734ff77ce79395277e8bafeaf55', 'e8a12b51f9cadbbb37596a61f2450607', 'Lau Pei Leng', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:35'),
(75, 'fa9fa734ff77ce79395277e8bafeaf55', 'db52518210831bb54ab3c62f1a34738c', 'Tan Chiew Yen', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:45', '2020-08-05 06:56:35'),
(76, 'fa9fa734ff77ce79395277e8bafeaf55', '0d41824a0e1e28443638706987e74953', 'Ewe Chang Chieh', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:35'),
(77, 'fa9fa734ff77ce79395277e8bafeaf55', 'd3440d96134b65c62fe8078164d3031b', 'Tiong Shiau Fung', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:35'),
(78, 'fa9fa734ff77ce79395277e8bafeaf55', '4d14810f6f437854909be4f430e2223e', 'Chin Pei Jiun', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:35'),
(79, 'fa9fa734ff77ce79395277e8bafeaf55', '575e99da63d1dff92bd6193591ddbf64', 'Cheong Moon Peng', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:35'),
(80, 'fa9fa734ff77ce79395277e8bafeaf55', 'c79ae3a578fecfbf244951fabdd683fe', 'Pong Yen Lin', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:36'),
(81, 'fa9fa734ff77ce79395277e8bafeaf55', '05a17d36c07c8a908c4091261a470a33', 'Cheah Lye Chin', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:37'),
(82, 'fa9fa734ff77ce79395277e8bafeaf55', 'a0a0a39fa80f2ef35a2f48c20ce9d43b', 'Khor Wei Loon', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:40'),
(83, 'fa9fa734ff77ce79395277e8bafeaf55', '35d1ced7511e0e456152a5ebd7ec5658', 'Khor Inn Hooi', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 07:22:57'),
(84, '6decb52a7f9790318a44cc1aca50d192', 'c812b37b54a11900e6f3fe6e86ade4ba', 'Tia Tow Kheng', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-06 10:03:56'),
(85, 'd8c2732d0378a1a7835c1e4260866e7d', 'ce0ba4aeebe67862b5e9b41b9346df48', 'Ow Yang Yeow Cong', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:35'),
(86, 'd8c2732d0378a1a7835c1e4260866e7d', '7a97b0f7ca78b1898148099df26e7df5', 'Loh Qiao Rou', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:35'),
(87, 'd8c2732d0378a1a7835c1e4260866e7d', 'c93a3ee678301cbed7fa1ba82cd0d679', 'Hor Hou Teck', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 07:22:57'),
(88, '7bd22a2ce76f3e0acbb7eb815138ed85', '41973415464aab9f7a11ff7b5d7d82da', 'Catherine Wong', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:27'),
(89, '7bd22a2ce76f3e0acbb7eb815138ed85', 'f0f7302ec9084b2692d6a3f366e081fe', 'Choo Hon Kiat', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:27'),
(90, '7bd22a2ce76f3e0acbb7eb815138ed85', 'd6c1ea63640018dff36f2070743d1e16', 'Ooi Suki', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:29'),
(91, '7bd22a2ce76f3e0acbb7eb815138ed85', 'd021497fa170e4f98f90b8cc44be179b', 'Jacelyn Mok Mei Suet', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:29'),
(92, '7bd22a2ce76f3e0acbb7eb815138ed85', 'e27e657a1ebfd2df55f4d2a91a9aafd1', 'Lynn Seow Lii Ting', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:29'),
(93, '7bd22a2ce76f3e0acbb7eb815138ed85', 'a23fa2232fbc428b4133e5ee144163df', 'Lim Chien Her', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:30'),
(94, '7bd22a2ce76f3e0acbb7eb815138ed85', '51dc5c0a874dde299fd1da02d2794e58', 'Bryan Khoo Tze Yong', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:30'),
(95, '7bd22a2ce76f3e0acbb7eb815138ed85', '2a82fc295d1db1ca9f7a9c035a3bb272', 'Gino Lee Chun Wey', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:31'),
(96, '7bd22a2ce76f3e0acbb7eb815138ed85', 'c314334c708246c53cf1e12f6ea170fc', 'Kua Xue Qi', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:31'),
(97, '836d753ed4e852ee1493714648f2d055', '0c9d7888dee0e981b004dd54d4ba071f', 'Lim Wei Giap', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:36'),
(98, '836d753ed4e852ee1493714648f2d055', '60b915ec91fcd7f2e13e578176e56bca', 'Teh Boon Kheng', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:36'),
(99, '836d753ed4e852ee1493714648f2d055', '9b1068084ee2d9658ec4837daa6a1506', 'Mohamed Zaid bin Aboo Bakar', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:36'),
(100, '25afe8e85c61914754d64eeb4ffc71ce', 'ddfce2f98b0d44a5865856be09bbaec1', 'Bong Shu Mei', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:46', '2020-08-05 06:56:33'),
(101, '25afe8e85c61914754d64eeb4ffc71ce', '8d223a5365d930d45db03dbc09915d40', 'Lee Kah Guan', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:33'),
(102, '25afe8e85c61914754d64eeb4ffc71ce', '6c8d4d7286e9e0dd4410764ac5df344d', 'Teh Chin Woei', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:33'),
(103, '25afe8e85c61914754d64eeb4ffc71ce', 'f79879b0eb60e846a9bd80710f3c6898', 'Lee Teck Chye', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:33'),
(104, '25afe8e85c61914754d64eeb4ffc71ce', '4983d1fb14ea48b129a383f9001e7a63', 'Hng Chun Siang', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:33'),
(105, '25afe8e85c61914754d64eeb4ffc71ce', 'd0ac8e3bab8669dad8889d6aa40f6f61', 'Ooi Li Ling', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:33'),
(106, '25afe8e85c61914754d64eeb4ffc71ce', 'd2ddd7192014ae661a8faa32227b351f', 'Teh Joo Tatt', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:34'),
(107, '25afe8e85c61914754d64eeb4ffc71ce', '6b7f3d70877a6db10ac329b22db13afc', 'Ooi Guit Ngoh', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 07:22:56'),
(108, 'aa51ed180a43499a37e1a9e972b0cbc5', '7dd189c1e2706a9116a42d04a03ca345', 'Tan Ee Vern', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:35'),
(109, 'aa51ed180a43499a37e1a9e972b0cbc5', 'd1f1da4588e43b778621550e49d22c6e', 'Chang Wei Lun', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:36'),
(110, 'aa51ed180a43499a37e1a9e972b0cbc5', 'bf84cf863a6f9ed8817b8bb1ba3bcf93', 'Khor Kheng Kok', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:36'),
(111, 'aa51ed180a43499a37e1a9e972b0cbc5', '6c1a0606117848ef3e15f458604634f6', 'Lim Jay Vee', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 07:22:56'),
(112, 'aa51ed180a43499a37e1a9e972b0cbc5', '3cf8a3182507923020c1cb4b3583b618', 'Wong Keng Nang', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 07:22:56'),
(113, '81816798b89aa643d83cab1efcab877b', '3b767bc628c9775a3c5e0fb2547a882e', 'Pua Kooi Khim', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:29'),
(114, '0b3caa5bc6f5c6e1175af1f16272e7ad', '80a54da40f9120a35791d17eb9e88b96', 'Tay Wei Min', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:37'),
(115, '0b3caa5bc6f5c6e1175af1f16272e7ad', '93e0e067031667f3ec9a8f6c6d4080b6', 'Yeong Kah Poh', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:37'),
(116, '0b3caa5bc6f5c6e1175af1f16272e7ad', '0a476227426f37ed492cadf3676c88a3', 'Chai Cheah Tzun', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:37'),
(117, 'b535e8f9f4879d7de5008c3f0708626b', '70e705f36eb41c6cbd69cf85fe97ad33', 'Quah Mei Chel', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:34'),
(118, 'b535e8f9f4879d7de5008c3f0708626b', 'e1b4c35e2903f21a77b595aa176349c7', 'Lim Kah Lee', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:35'),
(119, 'b535e8f9f4879d7de5008c3f0708626b', 'd7b101570a054305f75555aee491ce93', 'Meow Yong Yew', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:35'),
(120, 'b535e8f9f4879d7de5008c3f0708626b', '122775d2ffea0aa72e9f1556f75f5715', 'Kang Chiew Sean', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:35'),
(121, 'b535e8f9f4879d7de5008c3f0708626b', '3e272f9ed251c9126e2f23d20168ae51', 'Beh Shun Xiar', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:35'),
(122, 'b535e8f9f4879d7de5008c3f0708626b', '08b9b48c00037487362d28cd230398e3', 'Chew Siew Pheng', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:35'),
(123, 'b535e8f9f4879d7de5008c3f0708626b', '882b9a9577208abc59ca424bb5e148d9', 'Chan Wei Chao', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:36'),
(124, 'b535e8f9f4879d7de5008c3f0708626b', '29ef321c2d24e829bd7052df44ac1fc2', 'Tan Peay Hoon', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:36'),
(125, 'b535e8f9f4879d7de5008c3f0708626b', '42abdfd36b9e8e44231605dc24b27070', 'Chong Sook Chin', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:36'),
(126, 'b535e8f9f4879d7de5008c3f0708626b', 'd3e57a896f679ebe7c514f1386595113', 'Chan Qing Bao', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:36'),
(127, 'b535e8f9f4879d7de5008c3f0708626b', '9ccb62577a91748f4d4b13b3b756c24b', 'Fang Seow Ling', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:36'),
(128, 'b535e8f9f4879d7de5008c3f0708626b', '6215e29562c40ea0a3b0e6cde971d459', 'Foo Weng Chen', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:36'),
(129, 'a4518d0a930828f45a57d4e6dd2107e8', '57fa1dc5d5a80025eb05f493f509b80c', 'Tan Sok Huang', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:22:47', '2020-08-05 06:56:34'),
(130, 'd2ddd7192014ae661a8faa32227b351f', '5a1152513b3cbb1ab183dfed7851d7a0', 'Ang Hui Peng', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:13', '2020-08-05 06:56:34'),
(131, 'd2ddd7192014ae661a8faa32227b351f', '29b68c61e48ee2084feef4e1f4831a13', 'Teh Chun Eong', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:13', '2020-08-05 06:56:34'),
(132, 'd2ddd7192014ae661a8faa32227b351f', 'aa96fcac51524c169f3f16e1ca9e7887', 'Kng Lai Ming', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:13', '2020-08-05 06:56:34'),
(133, 'd2ddd7192014ae661a8faa32227b351f', '7f234ef7dfb4a90ee2ee719042ba9c71', 'Lee Xian Hua', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:14', '2020-08-05 06:56:34'),
(134, '3e272f9ed251c9126e2f23d20168ae51', '184f4b9cfdf3b78bdf39ed595a117fe2', 'Tan Ai Heong', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:14', '2020-08-05 06:56:36'),
(135, '57a628f9aeb9bfa926a2ca30639fa742', '2a84ba18cc8db0c0e6ae5c9a4053f2a1', 'Poh Shan Yi', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:14', '2020-08-05 06:56:29'),
(136, '57a628f9aeb9bfa926a2ca30639fa742', '33429f1ca074be3e3d699b42e0d88ff7', 'Loh Yao Ming', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:14', '2020-08-05 06:56:30'),
(137, '57a628f9aeb9bfa926a2ca30639fa742', '0c91156052b271ff7677d4fe1ce9bb05', 'Loh Yao Guang', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:14', '2020-08-05 06:56:30'),
(138, '4983d1fb14ea48b129a383f9001e7a63', 'd1438261084c11c20492c5b95cea7522', 'Lee Quang Hao', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:14', '2020-08-05 06:56:33'),
(139, '4983d1fb14ea48b129a383f9001e7a63', '4e24501c5e98cf06b17687ee6c4370c9', 'Chng Yi Sheng', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:14', '2020-08-05 06:56:33'),
(140, '4983d1fb14ea48b129a383f9001e7a63', '1a6645f5d15a3e645a0dbd72f360818b', 'Choong Chee How', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:14', '2020-08-05 06:56:33'),
(141, '4983d1fb14ea48b129a383f9001e7a63', '30d4049adc94d71bed769ed7fea42f92', 'Chong Kai Gin', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:15', '2020-08-05 06:56:33'),
(142, '4983d1fb14ea48b129a383f9001e7a63', '69d986ad43c3225046cdd5f95a031349', 'Lee Wen Khang', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:15', '2020-08-05 07:22:56'),
(143, '0a476227426f37ed492cadf3676c88a3', '0a8cbe9ff22b9cf3f35bcc9946e0b658', 'Saw Boon Beng', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:15', '2020-08-05 06:56:39'),
(144, '0a476227426f37ed492cadf3676c88a3', '398a604aaf73379c3f057f15114dab57', 'Toh Keng Sheng', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:15', '2020-08-05 06:56:39'),
(145, '0a476227426f37ed492cadf3676c88a3', 'cb4c8ec641a61f1f6a29e2a247a4cdd1', 'Chin Tian Kai', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:15', '2020-08-05 06:56:39'),
(146, '0a476227426f37ed492cadf3676c88a3', '68f57d7108014ec7ec284dd207843350', 'Tan Woei Yang', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:15', '2020-08-05 06:56:39'),
(147, '882b9a9577208abc59ca424bb5e148d9', '07074a6ad0732fb9be1436a3a76cd49a', 'Toh Kim Chin', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:15', '2020-08-05 07:22:57'),
(148, 'f0f7302ec9084b2692d6a3f366e081fe', '0791fe9c4db516952c94c938a4be75d9', 'Kong Ming Yee', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:15', '2020-08-05 06:56:32'),
(149, 'f0f7302ec9084b2692d6a3f366e081fe', 'a710b4d8967468109fd9a3b8fab3652c', 'Goh Wei Xiang', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:15', '2020-08-05 06:56:32'),
(150, 'f0f7302ec9084b2692d6a3f366e081fe', '7fe2145da36c71a5fbd1f2dcd84c64d3', 'Chong Chun How', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:15', '2020-08-05 06:56:33'),
(151, '8d223a5365d930d45db03dbc09915d40', '6cf706172fd1b611690b356886d638f8', 'Ong Gaik Khim', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:15', '2020-08-05 06:56:33'),
(152, 'f79879b0eb60e846a9bd80710f3c6898', '246f9d302bf03016e2076d1af6b4d519', 'Ong Yoong', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:16', '2020-08-05 07:22:56'),
(153, '7e15a52fad7f687a387db70e99d52365', 'd54df9a5e74dcf43349ab2c3879dfb48', 'Pang Chong Min', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:16', '2020-08-05 06:56:30'),
(154, '7e15a52fad7f687a387db70e99d52365', '227217c0c5a8ef8de3c715e67b954f86', 'Lee Xi Hong', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:16', '2020-08-05 06:56:31'),
(155, 'c812b37b54a11900e6f3fe6e86ade4ba', 'adc7f14fb1b1d1bb2e29818737bddec4', 'Wong Xian Jin', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:16', '2020-08-05 06:56:35'),
(156, 'a0a0a39fa80f2ef35a2f48c20ce9d43b', '0167544eb85efa37b1d61400cda0436f', 'Lim Chai Ying', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:16', '2020-08-05 07:22:57'),
(157, 'a0a0a39fa80f2ef35a2f48c20ce9d43b', '2ce7349cc903298f2fe59f19d3c508b1', 'Kho Pei Kuan', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:16', '2020-08-05 07:22:57'),
(158, 'a0a0a39fa80f2ef35a2f48c20ce9d43b', 'a0201ac885605e4247e1d7786dca8b42', 'Khor Tong Jin', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:16', '2020-08-05 07:22:57'),
(159, 'a0a0a39fa80f2ef35a2f48c20ce9d43b', '6b27bb9378e474146a3cf243c7354082', 'Wong Jin Sern', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:16', '2020-08-05 07:22:57'),
(160, 'a0a0a39fa80f2ef35a2f48c20ce9d43b', 'abcccfd2e67493a5ca92ac11b115a704', 'Chng Hui Sim', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:16', '2020-08-05 06:56:40'),
(161, 'a0a0a39fa80f2ef35a2f48c20ce9d43b', '54672e213fba3632e2ff002b0fd09ca6', 'Chuah Peh En', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:16', '2020-08-05 07:22:57'),
(162, '0c9d7888dee0e981b004dd54d4ba071f', '93c725d60503c41a28db41cc28d9bb0a', 'Lee Chee Leang', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:16', '2020-08-05 06:56:36'),
(163, '0c9d7888dee0e981b004dd54d4ba071f', '4fd725f34d65ee065d53ad4f4a5573b3', 'Rozana binti Abdul Razak', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:16', '2020-08-05 06:56:36'),
(164, '80a54da40f9120a35791d17eb9e88b96', '1539343fae2219fc72b32f1d8af2496b', 'Poh Swee Xiong', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:16', '2020-08-05 06:56:37'),
(165, '80a54da40f9120a35791d17eb9e88b96', '8fd0e56fd68b8a21ec1d97a9b1217772', 'Tan Hui Peng', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:37'),
(166, '80a54da40f9120a35791d17eb9e88b96', '1da88e683792387461b44a44a15e82d6', 'Tan Chin Sze', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:37'),
(167, '80a54da40f9120a35791d17eb9e88b96', '70ce5a9ec6de6560dd3c139501c5d87b', 'Khor Xin Fu', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:37'),
(168, '80a54da40f9120a35791d17eb9e88b96', 'ee52741a10ce1a7a05155416f34979aa', 'Koay Whey Ping', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:37'),
(169, '80a54da40f9120a35791d17eb9e88b96', 'ba516d68cb48a7d4b27698098b34a70e', 'Ooi Zhi Hong', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:37'),
(170, '80a54da40f9120a35791d17eb9e88b96', 'd2db196084cd3f7883f84225fad229fc', 'Ooi Yeen Chao', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:38'),
(171, '93e0e067031667f3ec9a8f6c6d4080b6', '58ed64106fe6bd8f2d50f70038aec1f2', 'Lim Wai Kit', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:37'),
(172, '93e0e067031667f3ec9a8f6c6d4080b6', 'e13d85ff4f0bd93560efbd7a838d2ce5', 'Lee Kah Yee', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:38'),
(173, '93e0e067031667f3ec9a8f6c6d4080b6', '53e551f516a3d6c3538e3835dadb9b4f', 'Goh Chern Yi', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:38'),
(174, '93e0e067031667f3ec9a8f6c6d4080b6', 'bc0e5fa0718aae0b48d334a997a38dee', 'Choo Yee Hoon', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:38'),
(175, '93e0e067031667f3ec9a8f6c6d4080b6', '86374e776a7cda813a116c69c0e9f237', 'Ang Wei Chieh', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:38'),
(176, '93e0e067031667f3ec9a8f6c6d4080b6', 'b6244418012b346de9af42541f5c25af', 'Saw Wai Lee', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:38'),
(177, '93e0e067031667f3ec9a8f6c6d4080b6', '2a3f5913da4294b3056957773f9d3967', 'Lee Seng Hor', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:38'),
(178, '9ccb62577a91748f4d4b13b3b756c24b', '24786a14e0a2dd80c666b7efcd4b6670', 'Lim Shevern', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:36'),
(179, '9ccb62577a91748f4d4b13b3b756c24b', '8e120f09620761c7526a1f7e42729357', 'Wong Wai Fung', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:17', '2020-08-05 06:56:36'),
(180, '9ccb62577a91748f4d4b13b3b756c24b', '63cacb64d53d5d335f8d686117ed9674', 'Tan Chen Hong', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 06:56:36'),
(181, '9ccb62577a91748f4d4b13b3b756c24b', '5663e91f196702da4dcf542301520fc3', 'Wong Lim Huat', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 06:56:37'),
(182, '9ccb62577a91748f4d4b13b3b756c24b', '703db8333c0b12536ea11b78fbc0f2a0', 'Wong Kok Yee', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 06:56:37'),
(183, '4d14810f6f437854909be4f430e2223e', 'd38fdc3b1481e698292b89c4d4a734dd', 'Lai Wei Sheng', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 06:56:35'),
(184, '4d14810f6f437854909be4f430e2223e', 'c8c76ae3ee1691b62a0b9dc8d6850559', 'Ong Seok Thng', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-07 01:52:01'),
(185, '4d14810f6f437854909be4f430e2223e', '42bcdcf1fe981e77d17fe8cf4e802b5d', 'Tung Joo Shuang', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 06:56:37'),
(186, 'd3e57a896f679ebe7c514f1386595113', '8247f5ed5951c0647b6ada642351a224', 'Chong Weng Thong', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 06:56:36'),
(187, 'c465428dc88ccf3d6311eb5001f5603c', 'd7c1681f61bcbe7ad758d2c47e41cfea', 'Hng Kah Liang', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 06:56:29'),
(188, 'c465428dc88ccf3d6311eb5001f5603c', '18d043e3295ec6128e073a9c8cdb33d2', 'Ooi Suet Yean', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 06:56:30'),
(189, 'c465428dc88ccf3d6311eb5001f5603c', '3860cc4295beb56cb0d47ff6d3fab34e', 'Tan Hooi Yin', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 06:56:33'),
(190, 'c465428dc88ccf3d6311eb5001f5603c', '088eeb4887f294d404214e614f0bad08', 'Ng Khai Xien', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 07:22:56'),
(191, 'c465428dc88ccf3d6311eb5001f5603c', '9ffb9543e36b7b49ea65e152bb5b6522', 'Lau Giap Poey', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 07:22:56'),
(192, '6c8d4d7286e9e0dd4410764ac5df344d', '80b061bf64edba474b88be0e68fca563', 'Yap Yi Xiang', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 06:56:33'),
(193, '6c8d4d7286e9e0dd4410764ac5df344d', '016a91879c50fd7575a7bf78727114ec', 'Teng Jun Chung', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 06:56:33'),
(194, '7dd189c1e2706a9116a42d04a03ca345', 'f51e488c8edcf0e371c0cf1e3af82f52', 'Chew Wen Jun', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 02:53:18', '2020-08-05 06:56:36'),
(195, '30d4049adc94d71bed769ed7fea42f92', '205a8c593f5a8c3409763b256527e85c', 'Lim Sze Er', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:51', '2020-08-05 06:56:33'),
(196, '30d4049adc94d71bed769ed7fea42f92', '342f78b1d84bf9681b6e429aba0c36bb', 'Chen Ling Tze', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:51', '2020-08-05 07:22:56'),
(197, '18d043e3295ec6128e073a9c8cdb33d2', '05d7b32475bc62f47924b723d0b2d7ca', 'Tan Boon Kwang', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:51', '2020-08-05 06:56:31'),
(198, '18d043e3295ec6128e073a9c8cdb33d2', '7477756f71d78e4fba0a8c0bf295921d', 'Eng Yan Joe', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:31'),
(199, '18d043e3295ec6128e073a9c8cdb33d2', '5a2d45bbad81490638ebbd2121064e22', 'Loh De Wei', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:32'),
(200, '18d043e3295ec6128e073a9c8cdb33d2', '64a30857d0cfc01938a69688fb7968c9', 'Su Jing Leong', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:32'),
(201, '8247f5ed5951c0647b6ada642351a224', 'f6ab32ce75ebd43806b42b18196e60b4', 'Ooi Meng Haw', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:36'),
(202, '1a6645f5d15a3e645a0dbd72f360818b', '44d15e138b17766b60b31c4fd22b0742', 'Lee Meng Keat', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:33'),
(203, '1a6645f5d15a3e645a0dbd72f360818b', '2b6c45000a8f8f43cd4edd9b66f15794', 'Tan Boo Guat', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:40'),
(204, 'd38fdc3b1481e698292b89c4d4a734dd', '4e7902139ca0ef9a8cf9b44faf6f7386', 'Kwek Wei Jun', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:37'),
(205, 'd38fdc3b1481e698292b89c4d4a734dd', '87d06c5c2e1e811dc634ce60333fcf05', 'Lim Yu Keat', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 07:22:57'),
(206, 'd2db196084cd3f7883f84225fad229fc', '1155abf7f0885f4581a68bd12687976f', 'Koid Soon Huat', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:39'),
(207, 'd2db196084cd3f7883f84225fad229fc', 'ab1bdde00105e555e2f484c3b034a915', 'Khor Ying Meng', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:39'),
(208, 'd2db196084cd3f7883f84225fad229fc', '7de3b7779f067ca6685dfb340059bb30', 'Saw Boon Loong', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:39'),
(209, 'd54df9a5e74dcf43349ab2c3879dfb48', '221d5fdeb4b93809d596404d1e393c7a', 'Tan Khai Shian', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:29'),
(210, 'd54df9a5e74dcf43349ab2c3879dfb48', 'ca24ca881e5058379d242693fd3b758e', 'Ang Min Tzer', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:31'),
(211, 'd54df9a5e74dcf43349ab2c3879dfb48', 'f35c29effed5128a44ece643b6a812db', 'Kang Lie Ye', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:32'),
(212, 'd54df9a5e74dcf43349ab2c3879dfb48', 'b3e38f3d81b10e11cf79a0df27b4e7ed', 'Yap Li Min', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:32'),
(214, 'ba516d68cb48a7d4b27698098b34a70e', '8072e5cdfb0a86e1d953bfd5ef8312a8', 'Ng Weng Sheng', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:39'),
(215, 'ba516d68cb48a7d4b27698098b34a70e', '7e74f1ba6d0cd4a0812e5bf5865f9885', 'Teow Hui Ming', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:39'),
(216, 'ba516d68cb48a7d4b27698098b34a70e', '03d5411e04d13910cc47c7521d5a4a07', 'Ong Xin Yean', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:39'),
(217, '70ce5a9ec6de6560dd3c139501c5d87b', 'a7226ced8452348e4fbe7daa50521398', 'Cheah Chong Yung', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:37'),
(218, 'ee52741a10ce1a7a05155416f34979aa', 'ee4971ffb4aff1a237fbde35ac9b0973', 'Kee Wei Lam', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:38'),
(219, 'ee52741a10ce1a7a05155416f34979aa', '393bdf781efec3e545d90cf86ecba6ce', 'Tan Wei Liang', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:52', '2020-08-05 06:56:39'),
(220, 'b6244418012b346de9af42541f5c25af', 'd1baecff072bd6053437394fbc92c316', 'Tay Kean Seon', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:39'),
(221, 'b6244418012b346de9af42541f5c25af', '27a9480c10209eed6e8e520ff9b14a37', 'Ang Zhi Liang', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:39'),
(222, '58ed64106fe6bd8f2d50f70038aec1f2', '9c0f5cf3d0667d2de6eca651b1d5cbf7', 'Yeoh Lee Yin', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:38'),
(223, '58ed64106fe6bd8f2d50f70038aec1f2', 'fbd53785180055e74950bc279b866f71', 'Cheah Yong Sing', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:39'),
(224, '58ed64106fe6bd8f2d50f70038aec1f2', '29e2e32178dbd571a0b24dcc09987c47', 'Chan Sim Chai', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:39'),
(225, '58ed64106fe6bd8f2d50f70038aec1f2', '5a55b795c0c349b2b6161056cd938a29', 'Huan Szu Yong', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:39'),
(226, '58ed64106fe6bd8f2d50f70038aec1f2', 'b9e17d426064507624c68141e25027b6', 'Oh Chee Seong', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:39'),
(227, 'e13d85ff4f0bd93560efbd7a838d2ce5', '3369be14e4bd274620225d5e715f9ef2', 'Yeong Kah Yuen', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:39'),
(228, '1539343fae2219fc72b32f1d8af2496b', 'c54ac25bbc15c25ada140d3e35647c4f', 'Yeoh Kok Shen', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:37'),
(229, 'abcccfd2e67493a5ca92ac11b115a704', 'c87a0d08d43c83f3bd9b4e4c6085c04f', 'Garry Hong Kah Siang', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 07:22:57'),
(230, '5a1152513b3cbb1ab183dfed7851d7a0', '10bdcbc3cd0446e13879e70947a4168b', 'Dylan Tang Miang Yen', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:34'),
(231, '5a1152513b3cbb1ab183dfed7851d7a0', '1406f9c9a81bd08f7f7ab7bcbdad1ddc', 'Elaine Chan Kim Kee', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:34'),
(232, '5a1152513b3cbb1ab183dfed7851d7a0', '65a68e0f993455812946630c71821dfc', 'Bong Kuan Yew', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:34'),
(233, '5a1152513b3cbb1ab183dfed7851d7a0', '4c3eaff6b7d51126427088efac73b8c0', 'Vannies Teoh Pei Ching', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:34'),
(234, '5a1152513b3cbb1ab183dfed7851d7a0', '5dd3f580ba72c06950f8c8d66792bc16', 'Beh Wei Keat', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:34'),
(235, '5a1152513b3cbb1ab183dfed7851d7a0', 'fba36f5349a1107ed3f007868b5851cd', 'Ang Lean Nee', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:34'),
(236, '5a1152513b3cbb1ab183dfed7851d7a0', 'b521b99ad0bbfcf94e921e5bf696aeea', 'Chai Kar Jin', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:34'),
(237, 'bc0e5fa0718aae0b48d334a997a38dee', '1e04eea187fea1a584dd36e25c01d5f4', 'Sim Ah Kim', 6, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:04:53', '2020-08-05 06:56:39'),
(238, 'f35c29effed5128a44ece643b6a812db', 'c0a32c3241a3b6047f0749230cb36afe', 'Teoh Peng Tatt', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:33'),
(239, '8072e5cdfb0a86e1d953bfd5ef8312a8', 'be00c671742aff13b7d16132c7e64edf', 'Lim Jia Wei', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:39'),
(240, '8072e5cdfb0a86e1d953bfd5ef8312a8', 'c51b6462a97fb29845600dbf7bfc07a8', 'Keoh Kheng Seang', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:39'),
(241, '10bdcbc3cd0446e13879e70947a4168b', 'd0b0eef5666c038c5769b4be11f84c1a', 'Ung Chin Sen', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:34'),
(242, '65a68e0f993455812946630c71821dfc', '62818c31d772b266f668971bb4f4e1c1', 'Bong Kuan Chean', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:34'),
(243, '2b6c45000a8f8f43cd4edd9b66f15794', 'f5947116f6f8f4fa0622d9314a230f62', 'Chen Min Li', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:40'),
(244, '3369be14e4bd274620225d5e715f9ef2', '1ab172d34822f0b328fcb71e3a5b3623', 'Lim Zhi Hao', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:38'),
(245, '7477756f71d78e4fba0a8c0bf295921d', 'f7a12939a1586243844dad71295ea4e8', 'Tan Yong Chun', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:31'),
(246, '7477756f71d78e4fba0a8c0bf295921d', '3625e4300dbcf1476a4adc444d057e9f', 'Liw Sue Wen', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:32'),
(247, '7477756f71d78e4fba0a8c0bf295921d', '360d7a7c7842ddf853ec6c3dec4fa4f5', 'Ang Choon Kee', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:32'),
(248, '6cf706172fd1b611690b356886d638f8', '4f0082bd01b16d8aef49f099d2eb80a1', 'Yeoh Sze Hung', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:40'),
(249, '7e74f1ba6d0cd4a0812e5bf5865f9885', 'dd45160cecddc32d47754dd17a444396', 'Boh Cui Wei', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:38'),
(250, '7e74f1ba6d0cd4a0812e5bf5865f9885', '40039314520fab8c1866e4c1066f4c5a', 'Poh Soo Yong', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:39'),
(251, 'ee4971ffb4aff1a237fbde35ac9b0973', '9d55102feb759f7810581be3b6d13363', 'Tan Kean Boon', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:38'),
(252, '1155abf7f0885f4581a68bd12687976f', '42522b53c076ec797079bded782eea55', 'Lee Yee Seong', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:40'),
(253, '393bdf781efec3e545d90cf86ecba6ce', '53c7c6f4ccbdc7b08f767068b83c3930', 'Teoh Ryh Yaw', 7, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:38'),
(254, 'c51b6462a97fb29845600dbf7bfc07a8', '420b377c1eb2ad409f8341ca9e118199', 'Goh Wei San', 8, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:39'),
(255, 'f5947116f6f8f4fa0622d9314a230f62', '43392017eb07f3c9c2d0bf9f5f357b5b', 'Tan Siew Ling', 8, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 07:22:56'),
(256, 'f7a12939a1586243844dad71295ea4e8', 'cde09375cb33843c50059dde03fbeeb6', 'Looi Wei Chun', 8, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 03:16:17', '2020-08-05 06:56:31'),
(257, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '4c34653a8266a7772fd93d7945699d34', 'N/A 1', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 04:29:40', '2020-08-05 06:56:40'),
(258, 'd5e718b5b05ff1acc08ecc9fc9dc6894', 'c113d67b3415b2dcbd3696a6fd0c6031', 'N/A 2', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 04:29:40', '2020-08-05 06:56:40'),
(259, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '41c1bfca0d2f9ff0e22e307560515e81', 'N/A 4', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 04:29:40', '2020-08-05 06:56:40'),
(260, 'd5e718b5b05ff1acc08ecc9fc9dc6894', 'c1e152a1e17dc4587d80425d02e62ffa', 'N/A 5', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 04:29:40', '2020-08-05 06:56:40'),
(261, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '42152cbe237dd9b726747fbe00c295b9', 'N/A 6', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 04:29:40', '2020-08-05 06:56:40'),
(262, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '9dd2c7ada345bb7326fdcdf667fc07c9', 'N/A 7', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 04:29:40', '2020-08-05 06:56:40'),
(263, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '1debcf1fdbc4962963a114669f63d82a', 'N/A 8', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 04:29:40', '2020-08-05 06:56:40'),
(264, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2e02f8b818164194afc17cad9b619bb2', 'N/A 9', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 04:29:40', '2020-08-05 06:56:40'),
(265, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '07dad2f3c8bf72bd2d3d351ef5067aeb', 'N/A 11', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 04:29:41', '2020-08-05 06:56:40'),
(266, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '9106ebc0045f4b0535dbad3f82635103', 'N/A 12', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 04:29:41', '2020-08-05 06:56:40'),
(267, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '7d23fe4ffd93cc5c195bbb22a2b3d6b4', 'N/A 13', 1, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 04:29:41', '2020-08-05 06:56:40'),
(268, '4c34653a8266a7772fd93d7945699d34', '3d74a4d7904efaa1cd501e8786ea19d0', 'Neoh Jia Huey', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:18:19', '2020-08-05 06:56:27'),
(269, 'c113d67b3415b2dcbd3696a6fd0c6031', 'c07f42892f7ee5cdb201140fe84487f1', 'Tan Ai Pheng', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:18:19', '2020-08-05 06:56:27'),
(270, '92d5de2ab4e6663f8e12c43edda816a7', '2ae63f138a304de434153bc4af5b6708', 'N/A 3', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:18:19', '2020-08-05 06:56:40'),
(271, '41c1bfca0d2f9ff0e22e307560515e81', '5cd9988919a6170d2dbdd990fdf62d2d', 'Heng Bei Kuang', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:18:19', '2020-08-05 06:56:27'),
(272, 'c1e152a1e17dc4587d80425d02e62ffa', 'c747ece9ca712eb7a46b6ced282dd3c3', 'Koay Teng Koo', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:18:20', '2020-08-05 06:56:28'),
(273, '42152cbe237dd9b726747fbe00c295b9', '6323c16e9d69b0e382f904f1d19559b8', 'Cheah Mei Qing', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:18:20', '2020-08-05 06:56:28'),
(274, '9dd2c7ada345bb7326fdcdf667fc07c9', 'e13acadff32a82d42b19e8226b003a63', 'Ong Shian Rong', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:18:20', '2020-08-05 06:56:28'),
(275, '1debcf1fdbc4962963a114669f63d82a', '25e2d7fff8fcd55a4380e383aa670124', 'Chew Lai Kuen', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:18:20', '2020-08-05 06:56:28'),
(276, '2e02f8b818164194afc17cad9b619bb2', '33ce05b7b616acc27d4e5ea25c5c0447', 'Tok Pei Chi', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:18:20', '2020-08-05 06:56:28'),
(277, '92d5de2ab4e6663f8e12c43edda816a7', 'b515a68713acfdbb8f518d04afab6871', 'N/A 10', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:18:20', '2020-08-05 06:56:40'),
(278, '07dad2f3c8bf72bd2d3d351ef5067aeb', '5c5791681ea2024fbc925988ef62a82f', 'Lim Bee Ley', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:18:20', '2020-08-05 06:56:28'),
(279, '9106ebc0045f4b0535dbad3f82635103', 'c403d57890e79393d11623a87088f0a6', 'Lim Ewe Jin', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:18:20', '2020-08-05 06:56:28'),
(280, '7d23fe4ffd93cc5c195bbb22a2b3d6b4', '096b6e3afbfd5c8ed3c683c93b102d11', 'Tan Chang Looi', 2, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:18:20', '2020-08-05 06:56:28');
INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(281, '3d74a4d7904efaa1cd501e8786ea19d0', 'a3e1f5dc6b6aa7021c0aa88720149b9b', 'Khoo Xu Liang', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:23:37', '2020-08-05 06:56:31'),
(282, 'c07f42892f7ee5cdb201140fe84487f1', '4c2ae09bf6c334b63e4fd4cfe9f8a8d4', 'Khaw Guat Ling', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:23:37', '2020-08-05 07:22:56'),
(283, '2ae63f138a304de434153bc4af5b6708', '790fffa2f7e4e4e6237996d453c104e1', 'Khoo Ai Hong', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:23:37', '2020-08-05 06:56:28'),
(284, 'c747ece9ca712eb7a46b6ced282dd3c3', '2331def2d10d84e173cb54446775e60d', 'Cynthia Ann Martin', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:23:37', '2020-08-05 06:56:29'),
(285, '6323c16e9d69b0e382f904f1d19559b8', '78f8b552536572e04314313a42ad5a08', 'Mark Wei Jen', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:23:37', '2020-08-05 06:56:31'),
(286, 'e13acadff32a82d42b19e8226b003a63', '009261096de8ab902e6366cb6b2ada62', 'Chan Wei Meng', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:23:37', '2020-08-05 06:56:32'),
(287, '5cd9988919a6170d2dbdd990fdf62d2d', '2d610a323cb8b59aa72e0a91683547fe', 'Lim Swee Hua', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:23:37', '2020-08-05 06:56:28'),
(288, 'b515a68713acfdbb8f518d04afab6871', 'b64c810b74e7e9fbeb75f267acde3b19', 'Ang Kok Lim', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:23:38', '2020-08-05 06:56:28'),
(289, 'c07f42892f7ee5cdb201140fe84487f1', '02f2c46db0dfd5e0327ce648c9e47923', 'Teoh Kai Yuan', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:29:22', '2020-08-05 07:22:56'),
(290, 'c07f42892f7ee5cdb201140fe84487f1', '084000cb95380ce0ba85a25d807707d1', 'Yeoh Chee Teck', 3, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:29:22', '2020-08-05 07:22:56'),
(291, '78f8b552536572e04314313a42ad5a08', '8fd573832243d33581a90c8c46fd0aae', 'Tan Yang Shing', 4, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 06:29:23', '2020-08-05 06:56:31'),
(292, '3cf8a3182507923020c1cb4b3583b618', 'dd9837e0663e459bd910334bb7cc8d75', 'Hoo Yuan Zheng', 5, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '2020-08-05 07:22:57', '2020-08-07 03:07:59');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `upline1` varchar(255) DEFAULT NULL,
  `upline2` varchar(255) DEFAULT NULL,
  `upline3` varchar(255) DEFAULT NULL,
  `upline4` varchar(255) DEFAULT NULL,
  `upline5` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT 'NULL',
  `full_name` varchar(255) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  `user_type` int(11) DEFAULT 3,
  `branch_type` int(1) NOT NULL DEFAULT 1,
  `password` varchar(255) NOT NULL DEFAULT '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057',
  `salt` varchar(255) NOT NULL DEFAULT '5e29f9c1c505e3e6c954240573a4c4d15c5acf93',
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `ic` varchar(255) DEFAULT NULL,
  `birth_month` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `bank_no` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Active',
  `sub_sales_comm` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `upline1`, `upline2`, `upline3`, `upline4`, `upline5`, `username`, `full_name`, `position`, `user_type`, `branch_type`, `password`, `salt`, `date_created`, `ic`, `birth_month`, `contact`, `email`, `bank`, `bank_no`, `address`, `status`, `sub_sales_comm`) VALUES
(1, '24786a14e0a2dd80c666b7efcd4b6670', 'Joverine Fang', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Shevern Lim', 'Lim Shevern', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-21 16:00:00.0', '010702-07-0580', 'Jul', '011-2015 4269', 'shevernlim@gmail.com', 'PBB', '4832 704 220', '106, Jln Kerapu, Bworth 13400.', 'Active', ''),
(2, '2283dc4a74c736f21066ecae16a62c50', 'Hooi Li', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Angie Chua', 'Chua Siew Im', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '640729-07-5142', 'Jul', '016-4852888', 'angiechua.3229@gmail.com', 'PBB', '3129879605', '7A-4-13 Lebuhraya Thean Teik.', 'Active', ''),
(3, 'e7dacd7d47a7563c0977e2053d6b0807', 'Hooi Li', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Louis Goh', 'Goh Peek Goon', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-26 16:00:00.0', '650718-08-5387', 'Jul', '010-3906833', 'louisgohpg@gmail.com', 'HLB', '17800020386', '2F, Cangkat Sg Ara 2, Desa Ara, Bayan Lepas, 11900, Penang.', 'Active', ''),
(4, 'e152cd997de7e161a06f1e469bf24333', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', '', 'Hooi Li', 'Goh Hooi Li', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '660916-07-5218', 'Sept', '012-4107780', 'hl_goh@yahoo.com', '', '', '114, Free School Road, 11600 Penang.', 'Active', ''),
(5, '60b915ec91fcd7f2e13e578176e56bca', 'Edward Chee', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Steve Teh', 'Teh Boon Kheng', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-24 16:00:00.0', '701106-07-5265', 'Nov', '012-4830501', 'stuteh8080@gmail.com', 'MBB', '5.07321E+11', '39, Lrg Desa Cahaya 4, Taman Desa Cahaya, 14000 Bukit Mertajam Penang.', 'Active', ''),
(6, '575e99da63d1dff92bd6193591ddbf64', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Janice Cheong', 'Cheong Moon Peng', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-14 16:00:00.0', '710311-07-5496', 'Mar', '012-4017417', 'janmoon88@gmail.com', 'CIMB', '8004235883', '107-19-4 Gambier Hgt, 11700 Pg.', 'Active', ''),
(7, '42abdfd36b9e8e44231605dc24b27070', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Eldssa Chong', 'Chong Sook Chin', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-31 16:00:00.0', '720123-08-5688', 'Jan', '012-4311838', 'elsa864@gmail.com', '', '', '27-16-5 Parkuru Towers, Tingkat Bukit jambul 1, 11950 Penang.', 'Active', ''),
(8, '8e120f09620761c7526a1f7e42729357', 'Joverine Fang', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Shalsha Wong', 'Wong Wai Fung', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-21 16:00:00.0', '740323-07-5300', 'Mar', '012-4735592', '23shasha5555@gmail.com', 'PBB', '6416 6321 35', '39, Lorong Nepal 5, Lip Sin 11900.', 'Active', ''),
(9, '29ef321c2d24e829bd7052df44ac1fc2', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Kriss Tan', 'Tan Peay Hoon', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-31 16:00:00.0', '750717-10-5276', 'Jul', '013-3411443', 'kriss3139@gmail.com', '', '', '32, Lintang Bukit Kecil 4, Taman Sri Nibong.', 'Active', ''),
(10, '4fd725f34d65ee065d53ad4f4a5573b3', 'Jeff Lim', 'Edward Chee', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Rozana', 'Rozana binti Abdul Razak', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '761116-07-5546', 'Nov', '017-4597076', 'abdulrazakrozana@gmail.com', 'MBB', '1573-800-32518', 'Blok B-8-5 Taman Bukit Jambul, 11900 Bayan Lepas Pulau Pinang.', 'Active', ''),
(11, '08b9b48c00037487362d28cd230398e3', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Siew Pheng', 'Chew Siew Pheng', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-26 16:00:00.0', '770129-07-5638', 'Jan', '011-62462884', 'pheng21@gmail.com', '', '', '59 A Chew Jetty, Weld Quay, 10300 Georgetown, Pulau Pinang.', 'Active', ''),
(12, '7af526fb362c7f6fadd06ffb1b6f2fec', 'Jonathan Chng', 'Eddie Song', '', '', '', 'June Goh', 'Goh Hooi Choon', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '770415-07-5380', 'Apr', '012-4278545', 'wealthyhomes4u@gmail.com', '', '', '114 Jalan Free School, 11600 Georgetown.', 'Active', ''),
(13, 'd405cb8094f86403894e8826609ca012', 'Hooi Li', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Vivienne Tan', 'Tan Hooi Ping', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '770525-07-6472', 'May', '018-3934336', 'vtan7595@gmail.com', '', '', '308-8-9 Krystal Villa Jln Sultan Azlan Shah Sungai Nibong.', 'Active', ''),
(14, 'e8a12b51f9cadbbb37596a61f2450607', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Lenny Lau', 'Lau Pei Leng', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '770608-07-5504', 'Jun', '012-8556329', 'lennylaupl77@gmail.com', '', '', '20, Persiaran Kelicap 3, Sungai Ara.', 'Active', ''),
(15, '8727fe9dfd7312e617c131981f2921d1', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Michael Lee', 'Lee Sun Hor', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '771013-07-5809', 'Oct', '012-4596773', 'sunhormy@gmail.com', 'PBB', '4605351936', '4, Solok Bunga Kekwa 2, Off Jalan Chan Ewe Pin, 14000 Bukit Mertajam.', 'Active', ''),
(16, 'cf1887040b21910a1e576abea57fa743', 'Wong Wai Fung', 'Joverine Fang', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Sharon Png', 'Png Seok Ling', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-21 16:00:00.0', '771112-07-6292', 'Nov', '016-2143648', 'sharonpng77@gmail.com', 'PBB', '6301 73 1216', '12B Jln Satu Ayer Itam 11400 Penang.', 'Active', ''),
(17, 'fa9fa734ff77ce79395277e8bafeaf55', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', '', 'Nelson Liew', 'Liew Chee Hiang', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '790826-07-5543', 'Aug', '016-9768888', 'nelson7988@hotmail.com', 'MBB', '1.07041E+11', '383 Mk7, Pulau Betong, 11020 Balik Pulau, Pulau Pinang.', 'Active', ''),
(18, '5663e91f196702da4dcf542301520fc3', 'Joverine Fang', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Zellim Wong', 'Wong Lim Huat', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-21 16:00:00.0', '791227-07-5415', 'Dec', '016-4373136', 'huatko5823@gmail.com', 'PBB', '4957540809', '62-5-2 Solok Rawana 10460 Penang.', 'Active', ''),
(19, '0167544eb85efa37b1d61400cda0436f', 'Jasper Khor', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Renee Lim', 'Lim Chai Ying', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-09 16:00:00.0', '800629-07-5516', 'Jun', '017-4374349', 'reneelim5516@gmail.com', 'MBB', '1.02037E+11', '487, Jalan BPJ 1/3A, BAndar Puteri Jaya, 08000 Sungai Petani, Kedah.', 'Active', ''),
(20, 'a0201ac885605e4247e1d7786dca8b42', 'Jasper Khor', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Alan Khor', 'Khor Tong Jin', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-09 16:00:00.0', '810402-07-5297', 'Apr', '016-4476913', 'alankhor.property@gmail.com', 'PBB', '4516968600', '1-2-03, Solok Paya Terubong 4, Taman Sayang 11060 Penang.', 'Active', ''),
(21, 'a3ec4b416c4ab372fa9506ec0f14eedf', 'Wong Wai Fung', 'Joverine Fang', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Choon Siang', 'Chow Choon Siang', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-21 16:00:00.0', '810723-07-5853', 'Jul', '012-4180626', 'francesmei84@gmail.com', 'HLB', '054000 72891', '49, Lorong Pondok Upih 3, 11000 Balik Pulau.', 'Active', ''),
(22, '9ccb62577a91748f4d4b13b3b756c24b', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Joverine Fang', 'Fang Seow Ling', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-21 16:00:00.0', '811113-07-5666', 'Nov', '011-20082002', 'jovyine@gmail.com', 'MBB', '5071 7005 0608', '106, Jln Kerapu, Bworth 13400.', 'Active', ''),
(23, '96648759158cbc91a61e9e0104418dfe', 'Wong Wai Fung', 'Joverine Fang', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Yee Ling', 'Cheah Yee Ling', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-21 16:00:00.0', '820112-07-5642', 'Jan', '019-4297223', 'xiaoxie7223@gmail.com', 'MBB', '157063 970 330', '21, Jalan Mandalay 10400', 'Active', ''),
(24, '703db8333c0b12536ea11b78fbc0f2a0', 'Joverine Fang', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Gavin Wong', 'Wong Kok Yee', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-21 16:00:00.0', '820125-07-5107', 'Jan', '010-3237272', 'wky1138@gmail.com', 'MBB', '1070 32113 965', '62-5-2 Solok Rawana 10460 Penang.', 'Active', ''),
(25, '57fa1dc5d5a80025eb05f493f509b80c', 'SS Ong', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Faith Tan', 'Tan Sok Huang', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '820504-01-5462', 'May', '012-5481282', 'faithtansh@gmail.com', 'PBB', '6907233605', '1D-18-01 Summerskye Residences, Jln Sg Tiram 8, 11900 Bayan Lepas.', 'Active', ''),
(26, 'e5b7f71f524c67801dc40f952fb3e379', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Kenny Loh', 'Loh Jia How', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '830408-07-5229', 'Apr', '012-4218973', 'kennylohJH@gmail.com', '', '', 'No 25, Lrg Seri Permai 1/1. Tmn Seri Permai, BM 14000 Pulau Pinang.', 'Active', ''),
(27, '93c725d60503c41a28db41cc28d9bb0a', 'Jeff Lim', 'Edward Chee', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Danny Lee', 'Lee Chee Leang', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-09 16:00:00.0', '830620-08-5125', 'Jun', '012-6171911', 'danny83pei@hotmail.com\nproperty.2126@gmail.com', 'MBB', '1.57037E+11', '31-1, Lintang Sunway Wellesley 1, Taman Sunway Wellesley 14000 Bukit Mertajam Penang.', 'Active', ''),
(28, 'c79ae3a578fecfbf244951fabdd683fe', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Cath Pong', 'Pong Yen Lin', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-07 16:00:00.0', '841211-02-5782', 'Dec', '1115478278', 'cathproper@gmail.com', 'PBB', '6348037021', '6A-21-15, I-Santorini, Seri Tg Pinang, Tg Tokong 10470 PG.', 'Active', ''),
(29, '9b1068084ee2d9658ec4837daa6a1506', 'Edward Chee', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'M. Zack', 'Mohamed Zaid bin Aboo Bakar', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-24 16:00:00.0', '850526-07-5703', 'May', '016-4899301', 'sparkplanner85@gmail.com', 'BSN', '07144-41-00000629-2', 'No 7, Lebuhraya Gelugor, Jelutong, 11600 Georgetown Penang.', 'Active', ''),
(30, '71899f072d5b2a98879f202a77dd08f2', 'Wong Wai Fung', 'Joverine Fang', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eileen Lim', 'Lim Sook Pheng', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-21 16:00:00.0', '850809-07-5180', 'Aug', '016-4378808', '', '', '', '', 'Active', ''),
(31, '836d753ed4e852ee1493714648f2d055', 'June Goh', 'Jonathan Chng', 'Eddie Song', '-', '', 'Edward Chee', 'Chee Hong Wei', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-24 16:00:00.0', '850813-07-5677', 'Aug', '018-2897835\n010-8043168', 'edwardwei313@gmail.com', 'PBB', '4924907617', '33-5-8, Halaman York 10450 Georgetown Pulau Pinang.', 'Active', ''),
(32, '63cacb64d53d5d335f8d686117ed9674', 'Joverine Fang', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Wilson Tan', 'Tan Chen Hong', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-21 16:00:00.0', '860616-35-5935', 'Jun', '016-411 9578', 'wilson.0616@gmail.com', 'MBB', '1072 4602 1787', 'E1-08-9 Lrg Gangsa 11600 P. Pinang.', 'Active', ''),
(33, 'bf84cf863a6f9ed8817b8bb1ba3bcf93', 'Jimmy Chew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Jimmy Khor', 'Khor Kheng Kok', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-10 16:00:00.0', '861208-35-5439', 'Dec', '014-3449362', 'jimmykhor86@gmail.com', 'PBB', '4596-4333-11', '5677C, Blok A, Mak Mandin 13400 Bworth.', 'Active', '80%'),
(34, '05a17d36c07c8a908c4091261a470a33', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Joey Cheah', 'Cheah Lye Chin', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-30 16:00:00.0', '861213-35-5572', 'Dec', '013-5353459', 'jcjoeycheah88@gmail.com', 'MBB', '1072-4608-8824', 'Block 5-13-4, Lintang Macallum 2, 10300 Georgetown, Penang', 'Active', ''),
(35, '70e705f36eb41c6cbd69cf85fe97ad33', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Veronique Quah', 'Quah Mei Chel', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '870301-06-5780', 'Mar', '016-4110024', 'quah.veronique@gmail.com', '', '', '61A-15-3A Shineville Park. 11400 Penang.', 'Active', ''),
(36, 'e1b4c35e2903f21a77b595aa176349c7', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Kelly Lim', 'Lim Kah Lee', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '870304-07-5300', 'Mar', '016-4023999', 'kahlee99@gmail.com', 'HSBC', '3.71508E+11', '42A-14-4 Sri Saujana Gat Lebuh Macallum 10300 Georgetown Penang.', 'Active', ''),
(37, '6b27bb9378e474146a3cf243c7354082', 'Jasper Khor', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Jason Wong', 'Wong Jin Sern', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-09 16:00:00.0', '870602-35-5343', 'Jun', '012-4470452', 'jason.jinsern@gmail.com', 'HLB', '5350266427', '12A-4-1, Azuria Condominium, Jalan Lembah Permai 11200 Tanjung Bungah, Penang', 'Active', ''),
(38, '122775d2ffea0aa72e9f1556f75f5715', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Queenie Kang', 'Kang Chiew Sean', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-27 16:00:00.0', '870701-02-5390', 'Jul', '017-5387194', 'queeniekang87@gmail.com', '', '', 'No.7, Lorong Hijauan Hills 3, Hijauan Hills, 14120 Simpang Ampat.', 'Active', ''),
(39, '184f4b9cfdf3b78bdf39ed595a117fe2', 'Beh', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Ai Heong', 'Tan Ai Heong', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-21 16:00:00.0', '870812-35-5165', 'Aug', '016-4445729', '', 'CIMB', '702 908 1957', '1B-33A-01, The Clover, Lengkok Merbah 3, Sg Ara, 11900 Bayan Lepas, Penang.', 'Active', ''),
(40, 'a4518d0a930828f45a57d4e6dd2107e8', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', '', 'SS Ong', 'Ong Siew Suan', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '870822-35-5062', 'Aug', '018-9888177', 'ssongproperty@gmail.com', '', '', '114, Jalan Perwira, Taman Perwira, 14000 Bukit Mertajam, Penang.', 'Active', ''),
(41, 'b535e8f9f4879d7de5008c3f0708626b', 'June Goh', 'Jonathan', 'Eddie Song', '', '', 'Richard Teoh', 'Teoh Li Liang', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '870908-35-5151', 'Sept', '017-4950965', 'liliang.teoh1987@gmail.com', '', '', '171-B, Lebuh Victoria 10300 Penang.', 'Active', ''),
(42, '8247f5ed5951c0647b6ada642351a224', 'Kelly Chan', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Dickson Chong', 'Chong Weng Thong', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-11 16:00:00.0', '871006-07-5147', 'Oct', '016-4713001', 'dickson_chong@live.com\ndicksonchong1987@gmail.com', 'PBB', '45466-12707', '2-25-18 Solaria Residences 11900 Penang.', 'Active', ''),
(43, 'a0a0a39fa80f2ef35a2f48c20ce9d43b', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Jasper Khor', 'Khor Wei Loon', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-05 16:00:00.0', '880622-07-5153', 'Jun', '016-4522772', 'jasperkhor88@gmail.com', 'HLB', '2251132017', 'M3-17-20, Solok Angsana Ixora, Bandar Baru, 11500 Ayer Itam, Penang.', 'Active', ''),
(44, '87d06c5c2e1e811dc634ce60333fcf05', 'Eric Lai', 'Joyce Chin', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Benson Lim', 'Lim Yu Keat', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-15 16:00:00.0', '880921-02-5041', 'Sept', '012-3816938', 'bensonhedgeslim@gmail.com', 'CIMB', '7071815443', '26,Iabm Eettes 11200 Tanjong Bunga, Penang.', 'Active', ''),
(45, '35d1ced7511e0e456152a5ebd7ec5658', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Bebe Khor', 'Khor Inn Hooi', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-19 16:00:00.0', '881021-07-5224', 'Oct', '012-7421066', 'junpei5796@gmail.com', 'CIMB', '', 'no 2, Lorong Sentul 9/2 Taman Sentul Indah 14000 Bukit Mertajam.', 'Active', ''),
(46, 'aa51ed180a43499a37e1a9e972b0cbc5', 'June Goh', 'Jonathan Chng', 'Eddie Song', '-', '', 'Jimmy Chew', 'Chew Chee Ming', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-12 16:00:00.0', '890219-07-5187', 'Feb', '012-4472165', 'jimmychew.ccm@gmail.com', 'PBB', '33050207324', '165 Kg Juru, 14000 Bkt Mertajam, Pulau Pinang.', 'Active', ''),
(47, '3cf8a3182507923020c1cb4b3583b618', 'Jimmy Chew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Keng Nang', 'Wong Keng Nang', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-05 16:00:00.0', '890303-09-5017', 'Mar', '017-5535989', 'knwong33@gmail.com', '', '', '2, Lorong Duku 11, Taman Duku, 14000 BM, Pulau Pinang.', 'Active', ''),
(48, 'db52518210831bb54ab3c62f1a34738c', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Vinnie Tan', 'Tan Chiew Yen', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '890731-07-5362', 'Jul', '017-5155388', 'vinnieyen89@gmail.com', '', '', '6784, Jalan Raja Uda, 12300 Butterworth.', 'Active', ''),
(49, '42bcdcf1fe981e77d17fe8cf4e802b5d', 'Joyce Chin', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Joanne Tung', 'Tung Joo Shuang', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-28 16:00:00.0', '890824-02-5786', 'Aug', '016-4565428', 'jojoannetjs@gmail.com', 'CIMB', '7046 055 663', '3-30-12 Persiaran Halia 3, Taman Bukit Erskine, Mt Erskine Tg Tokong, 10470 Georgetown Penang.', 'Active', ''),
(50, 'dd9837e0663e459bd910334bb7cc8d75', 'Wong Keng Nang', 'Jimmy Chew', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Yuan Zheng', 'Hoo Yuan Zheng', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-05 16:00:00.0', '891206-07-5879', 'Dec', '017-4788365', 'hooyuanzheng1206@gmail.com', 'PBB', '6461 2723 25', '28, Lorong 13, Taman Cendana, 14100 Simpang Ampat, Pulau Pinang.', 'Active', ''),
(51, '6decb52a7f9790318a44cc1aca50d192', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', '', 'Clovis Chew', 'Chew Khai Lyn', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '900419-07-5604', 'Apr', '016-4963110', 'clovis.homme90@gmail.com', 'PBB', '6912569606', 'No. 66, Weld Quay, Chew Jetty, 10300 Georgetown Penang.', 'Active', ''),
(52, '0d41824a0e1e28443638706987e74953', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Ewe', 'Ewe Chang Chieh', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '900921-07-5837', 'Sept', '012-5152915', 'ewechangchieh@gmail.com', 'PBB', '6362509506', '110-F Jalan Batu Gajah 11600 Georgetown Penang.', 'Active', ''),
(53, 'abcccfd2e67493a5ca92ac11b115a704', 'Jasper Khor', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Vicky Chng', 'Chng Hui Sim', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-09 16:00:00.0', '910619-07-5456', 'Jun', '012-4046526', 'vickysim91@gmail.com', 'PBB', '4663411429', '1-13-6, Jelutong Park, Lintang Madrasah, 11600 Jelutong, Penang.', 'Active', ''),
(54, 'd3e57a896f679ebe7c514f1386595113', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Kelly Chan', 'Chan Qing Bao', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-11 16:00:00.0', '911011-07-6084', 'Oct', '016-4523619', 'kelly1991chan@gmail.com\nproperty.kelly@my.com', 'HSBC', '374-553-0710-25', '25-18, Solaria Residency, Medan Rajawali, 11900 Bayan Lepas.', 'Active', ''),
(55, '7a97b0f7ca78b1898148099df26e7df5', 'Nicholas Ng', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Jenn Loh', 'Loh Qiao Rou', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '911109-07-5426', 'Nov', '017-4380917', 'qflexibel@gmail.com', '', '', 'Blk 5-3-5, Tingkat Paya Terubong 3, 11060 Penang.', 'Active', ''),
(56, 'adc7f14fb1b1d1bb2e29818737bddec4', 'Evan Tia', 'Clovis Chew', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Advance Wong', 'Wong Xian Jin', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '920124-07-5111', 'Jan', '017-8682823', 'xwjin20@gmail.com', '', '', '6, Lrg Kolam Ikan 2, 11900 Bayan Lepas, Pulau Pinang.', 'Active', ''),
(57, '3e272f9ed251c9126e2f23d20168ae51', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Beh', 'Beh Shun Xiar', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-26 16:00:00.0', '920211-02-6123', 'Feb', '017-5592032', 'beh0211@gmail.com', 'MBB', '1.02055E+11', 'No. 1485, Lorong Kempas 6/4, Taman Kempas 08000 Sungai Petani.', 'Active', ''),
(58, '6c1a0606117848ef3e15f458604634f6', 'Jimmy Chew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Jay Vee', 'Lim Jay Vee', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-05 16:00:00.0', '920421-02-5401', 'Apr', '017-4312009', 'Jayv92@hotmail.com', 'PBB', '4622 7769 32', '2-13A-15, Taman Ria, 11920 Teluk Kumbar, Bayan Lepas.', 'Active', ''),
(59, '4e7902139ca0ef9a8cf9b44faf6f7386', 'Eric Lai', 'Joyce Chin', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Alvin Kwek', 'Kwek Wei Jun', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-30 16:00:00.0', '920611-07-5389', 'Jun', '016-4212863', 'alvinkwek1106@gmail.com', '', '', 'Blok 86-18-07, 86 Avenue Residence, Lengkok Perak, 10150 Georgetown, Penang', 'Active', ''),
(60, 'd3440d96134b65c62fe8078164d3031b', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Janice Tiong', 'Tiong Shiau Fung', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '920809-13-6298', 'Aug', '017-4878296', 'shiaufungtiong@gmail.com', 'MBB', '1.57429E+11', '2C-16-10, Imperial Residences, Lintang Sg. Ara 14, 11900 Bayan Lepas, Penang.', 'Active', ''),
(61, 'c812b37b54a11900e6f3fe6e86ade4ba', 'Clovis Chew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Evan Tia', 'Tia Tow Kheng', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '921229-07-5059', 'Dec', '012-5183159', 'towkheng@gmail.com', '', '', '159-13-8, Wisma Lam Hong, Jalan Sg. Pinang, 11500, Pulau Pinang.', 'Active', ''),
(62, 'd1f1da4588e43b778621550e49d22c6e', 'Jimmy Chew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Alan Chang', 'Chang Wei Lun', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-04 16:00:00.0', '930401-07-5611', 'Apr', '011-55051240', 'alanchang1493@gmail.com', 'HLB', '3305-0090-483', '20, Tingkat Binjai 34, Taman Sri Rambai, 14000 BM Pulau Pinang.', 'Active', ''),
(63, 'c87a0d08d43c83f3bd9b4e4c6085c04f', 'Vicky Chng', 'Jasper Khor', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Garry Hong', 'Garry Hong Kah Siang', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-09 16:00:00.0', '930427-07-5261', 'Apr', '016-4442615', 'garry1507@hotmail.com', 'MBB', '1.57055E+11', 'Blok 3-22-10, Lengkok Angsana, Sri Ivory, Bandar Baru, 11500 Ayer Itam, Penang.', 'Active', ''),
(64, '2edab7339c8bda13d36a2b6e3d127696', 'Wong Wai Fung', 'Joverine Fang', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Chrissy Lee', 'Lee Yoke Yin', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-21 16:00:00.0', '930507-07-5038', 'May', '016-4244441', '', 'PBB', '4626 63 9225', '', 'Active', ''),
(65, '7dd189c1e2706a9116a42d04a03ca345', 'Jimmy Chew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Yvonne Tan', 'Tan Ee Vern', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-12 16:00:00.0', '930605-02-5050', 'Jun', '016-4247987', 'yvonneevern93@gmail.com', 'MBB', '1.57326E+11', '30, Lorong 2/2, Tmn Bandar Perdana.', 'Active', ''),
(66, '07074a6ad0732fb9be1436a3a76cd49a', 'Chan', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'KC Toh', 'Toh Kim Chin', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-12 16:00:00.0', '931126-07-5258', 'Nov', '012-4130875', 'kctoh93@gmail.com', 'PBB', '4608669600', '51, Lebuh Kurau 5, Taman Chai Leng, 13700 Perai, Penang.', 'Active', ''),
(67, 'f51e488c8edcf0e371c0cf1e3af82f52', 'Yvonne Tan', 'Jimmy Chew', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Wen Jun', 'Chew Wen Jun', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-16 16:00:00.0', '931231-02-5981', 'Dec', '016-5070921', 'wenjunchew1@gmail.com', '', '', 'E465, Jalan Indah 4, Taman Sejatio Indah, 08000 Sg Petani, Kedah.', 'Active', ''),
(68, '4d14810f6f437854909be4f430e2223e', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Joyce Chin', 'Chin Pei Jiun', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '940123-02-5540', 'Jan', '011-26871642', 'peijiun94@gmail.com', '', '', 'B-21-2 Sandilands Condo. Lebuh Sandilands, 10300 Georgetown Penang.', 'Active', ''),
(69, '54672e213fba3632e2ff002b0fd09ca6', 'Jasper Khor', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Sherine Chuah', 'Chuah Peh En', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-09 16:00:00.0', '940619-07-5170', 'Jun', '017-4913588', 'sherinechuah0619@gmail.com', 'PBB', '6468893103', '12A-4-1, Azuria Condominium, Jalan Lembah Permai 11200 Tanjung Bungah, Penang.', 'Active', ''),
(70, 'd38fdc3b1481e698292b89c4d4a734dd', 'Joyce Chin', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Eric Lai', 'Lai Wei Sheng', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-26 16:00:00.0', '940917-07-5283', 'Sept', '017-4474167', 'ericlai194@gmail.com', '', '', '11 Cangkat Sungai Ara 19, 11900 Bayan Lepas, Pulau Pinang.', 'Active', ''),
(71, 'f6ab32ce75ebd43806b42b18196e60b4', 'Dickson Chong', 'Kelly Chan', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Minho Ooi', 'Ooi Meng Haw', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-11 16:00:00.0', '941019-07-5307', 'Oct', '017-4538682', 'minho941019@hotmail.com\nminho941019@gmail.com', 'PBB', '47800-74111', '8, Lorong Nenas, Taman Eng Seng, 14000 Bukit Mertajam, Pulau Pinang.', 'Active', ''),
(72, 'd8c2732d0378a1a7835c1e4260866e7d', 'June Goh', 'Jonathan', 'Eddie Song', '', '', 'Nicholas Ng', 'Ng Yong Qiang', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '950112-07-5357', 'Jan', '014-3389321', 'yongqiangng@gmail.com', '', '', '18, Lorong Budiman 8, Taman Budiman, 14000 BM.', 'Active', ''),
(73, 'ce0ba4aeebe67862b5e9b41b9346df48', 'Nicholas Ng', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Hugo Ow Yang', 'Ow Yang Yeow Cong', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '950118-07-5091', 'Jan', '018-4666958', 'hugoyang003@gmail.com', 'MBB', '1.57326E+11', '16, Lorong Budiman 2 Taman Budiman Alma 14000 Bukit Mertajam Pulau Pinang.', 'Active', ''),
(74, '6215e29562c40ea0a3b0e6cde971d459', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Weng Chen', 'Foo Weng Chen', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-21 16:00:00.0', '950302-07-5729', 'Mar', '017-4330005', 'wengchen950302@gmail.com', 'PBB', '6823 7387 33', '11, Jalan Ceri Off, Jalan Raja Uda, 12300 Bworth.', 'Active', ''),
(75, 'd7b101570a054305f75555aee491ce93', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Francis Meow', 'Meow Yong Yew', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '950614-07-5189', 'Jun', '016-5574697', 'myongyew@gmail.com', '', '', '15, Jalan Bagan 53, Taman Bagan, 13400 Butterworth, Pulau Pinang.', 'Active', ''),
(76, 'c93a3ee678301cbed7fa1ba82cd0d679', 'Nicholas Ng', 'June Goh', 'Jonathan', 'Eddie Song', '', 'Hou Teck', 'Hor Hou Teck', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-09 16:00:00.0', '960619-07-5857', 'Jun', '019-4751413', 'houteckhor@gmail.com', 'RHB', '1.18159E+13', '77, Jalan I, Taman Saujana Permai, 14000 Bukit Mertajam, Penang', 'Active', ''),
(77, '2ce7349cc903298f2fe59f19d3c508b1', 'Jasper Khor', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Sheryl Kho', 'Kho Pei Kuan', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-09 16:00:00.0', '961231-26-5034', 'Dec', '013-5338378', 'peikuankho@gmail.com', 'HLB', '33750083343', '1462, Jalan Inai, Taman Ria Jaya, 08000 Sungai Petani, Kedah.', 'Active', ''),
(78, 'c8c76ae3ee1691b62a0b9dc8d6850559', 'Joyce Chin', 'Nelson Liew', 'June Goh', 'Jonathan Chng', 'Eddie Song', 'Stephanie Ong', 'Ong Seok Thng', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-12-05 16:00:00.0', '970708-07-5532', 'Jul', '017-4138061', 'ongseokthng97@gmail.com', 'PBB', '1', '46-13-18, Desa Green, Van Praagh, Jelutong, 11600 George Town, Pulau Pinang', 'Active', ''),
(79, '0c9d7888dee0e981b004dd54d4ba071f', 'Edward Chee', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Jeff Lim', 'Lim Wei Giap', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-24 16:00:00.0', '990906-07-5603', 'Sept', '016-6645930', 'jeff.lim.5930@gmail.com', 'HLB', '6250204636', '86, Tingkat Muhibbah 4, Taman Alma, 14000 Bukit Mertajam, Penang.', 'Active', ''),
(80, '882b9a9577208abc59ca424bb5e148d9', 'Richard Teoh', 'June Goh', 'Jonathan Chng', 'Eddie Song', '', 'Chan', 'Chan Wei Chao', '', 3, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-31 16:00:00.0', '991123-02-5225', 'Nov', '011-28493866', 'chanweichao5225@gmail.com', '', '', '356, Taman Saga, Jalan Alor Mengkudu, 05400 Alor Setar, Kedah.', 'Active', ''),
(81, '80a54da40f9120a35791d17eb9e88b96', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '-', 'Jen Tay', 'Tay Wei Min', 'Admin Manager', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '891218-07-5526', 'Dec', '016-4621575', 'speedywm@hotmail.com\nmansionpropertiespenang@gmail.com', 'PBB', '3218-9336-26', '264-6-4 Desa Jelutong West Jelutong 11600 Penang.', 'Active', '70%'),
(82, 'd5e718b5b05ff1acc08ecc9fc9dc6894', '', '', '', '', '', 'Eddie Song', 'Song Meng Wai', 'Group Founder & Chairman', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '751023-07-5775', 'Oct', '013-4888833', 'lycyge@gmail.com', 'PBB', '3984-8163-30', '501-G, 15-03 Diamond Villa, Jalan Tanjong Bungah, 11200 Tanjong Bungah, Pulau Pinang', 'Active', '70%'),
(83, '13b28386a93b9c9fcca3cf0f8ddff3e7', 'Jeffrey Chan', 'Eddie Song', '', '', '', 'Lyn Lee', 'Lee Siew Ling', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-06 16:00:00.0', '760118-02-6022', 'Jan', '012-4433094', 'renelee6022@gmail.com', 'PBB', '4917933109', '63A-19-19, Jln Lenggong, 11600 Jelutong, Georgetown.', 'Active', ''),
(84, 'fdc7a6516c7a4d1ed8bed14c39797f8b', 'Eunice Loe', 'Joe Yeap', 'Jonathan Chng', 'Eddie Song', '', 'Lay Khim', 'Tan Lay Khim', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-06 16:00:00.0', '811015-07-5590', 'Oct', '016-4964476', 'whitedovetlk@yahoo.com', '', '', '42C-22-3 Sri Saujana Block C, Gat Lebuh Macallum, 10300 Penang.', 'Active', ''),
(85, '93e0e067031667f3ec9a8f6c6d4080b6', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '-', 'Johnson Yeong', 'Yeong Kah Poh', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-19 16:00:00.0', '950703-07-5279', 'Jul', '016-4969915', 'johnsonyeong95@gmail.com', 'PBB', '3218-9336-26', '264-6-6 Desa Jelutong West Jelutong 11600 Penang.', 'Active', '65%'),
(86, '0a476227426f37ed492cadf3676c88a3', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '-', 'Chai', 'Chai Cheah Tzun', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '940119-07-5289', 'Jan', '013-6777557', 'ctchai557@gmail.com', 'CIMB', '7055621852', 'E1-07-10 Lintang Paya Terubong 1, Air Hitam 11060 Penang.', 'Active', '65%'),
(87, '1539343fae2219fc72b32f1d8af2496b', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Swee Xiong', 'Poh Swee Xiong', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-18 16:00:00.0', '980602-07-5265', 'Jun', '016-4907665', 'alexispoh35@gmail.com', 'PBB', '6427 0939 00', '321-G, Jalan Perak 11600 Penang.', 'Active', '65%'),
(88, '8fd0e56fd68b8a21ec1d97a9b1217772', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Helen Tan', 'Tan Hui Peng', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-17 16:00:00.0', '951022-07-5382', 'Oct', '016-4583520', 'xiiaocha1995@gmail.com', 'PBB', '4660725214', 'No 17, Solok Relau 1 Taman Hijau 11900 Bayan Lepas, Pulau Pinang.', 'Active', '60%'),
(89, '1da88e683792387461b44a44a15e82d6', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Niccassa Tan', 'Tan Chin Sze', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-18 16:00:00.0', '880828-35-5134', 'Aug', '012-2436376', 'niccassatan24@gmail.com', 'PBB', '6889186414', '1, Jalan Semarak Api, 11500 Bandar Baru, Air Itam, Penang.', 'Active', '60%'),
(90, '70ce5a9ec6de6560dd3c139501c5d87b', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Mason Khor', 'Khor Xin Fu', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '920725-07-5395', 'Jul', '012-5503543', 'khor725@hotmail.com \nxinfu5395@gmail.com', 'MBB', '1.07032E+11', '11 Tingkat Binjai 22, Taman Sri Rambai 14000 Bukit Mertajam.', 'Active', '70%'),
(91, 'ee52741a10ce1a7a05155416f34979aa', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Ping Koay', 'Koay Whey Ping', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-17 16:00:00.0', '960309-07-5309', 'Mar', '016-4459075', 'entenkoay@hotmail.com\nwpkoaybuss@gmail.com', 'PBB', '6362320425', '1-12-3, Summer Place Condo, Lebuh Sg Pinang 8, 11600 Jelutong.', 'Active', '65%'),
(92, 'c54ac25bbc15c25ada140d3e35647c4f', 'Swee Xiong', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Kok Shen', 'Yeoh Kok Shen', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-17 16:00:00.0', '970624-07-5201', 'Jun', '010-3867847', 'yeohkokshen0624@gmail.com', 'MBB', '1073-3007-8391', '1-20-09, Jalan Dr Wu Lien Teh, Taman Harapan, 10150 Penang.', 'Active', '60%'),
(93, '58ed64106fe6bd8f2d50f70038aec1f2', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Richard Lim', 'Lim Wai Kit', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '970120-08-6169', 'Jan', '016-7780120', 'richardjacck@gmail.com', 'PBB', '3218-9336-26', '264-6-4 Desa Jelutong West Jelutong 11600 Penang.', 'Active', '65%'),
(94, 'a7226ced8452348e4fbe7daa50521398', 'Mason Khor', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Chong Yung', 'Cheah Chong Yung', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '930908-11-5475', 'Sep', '014-2111316', 'chongyung_93@hotmail.com\nchongyung93@gmail.com', 'CIMB', '7062-0933-74', '2-2-5 Sri Nipah, 2 Medan Nipah 11900 Penang.', 'Active', '60%'),
(95, 'e13d85ff4f0bd93560efbd7a838d2ce5', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Shaun Lee', 'Lee Kah Yee', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '900430-07-5189', 'Apr', '017-9763963', 'shaun.rock.lee@gmail.com', 'MBB', '1.57055E+11', '30, Lintang Glugor, 11600 Jelutong, Penang.', 'Active', '60%'),
(96, '53e551f516a3d6c3538e3835dadb9b4f', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Joven Goh', 'Goh Chern Yi', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '980430-35-5139', 'Apr', '016-4173556', 'jovengcy@hotmail.com\nJovenwaterheater@gmail.com', 'PBB', '4681514007', '6H 2-1, Jln Semarak Api', 'Active', '65%'),
(97, '9d55102feb759f7810581be3b6d13363', 'Ronald Kee', 'Ping Koay', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Steve Tan', 'Tan Kean Boon', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '960528-07-5157', 'May', '017-6168725', 'steve_fat-fat@hotmail.com\nmalaysiaboon96@gmail.com', 'PBB', '5004-5220-13', 'Blk 10-28-06 Jalan Gangsa, Greenlane Height 11600 Penang.', 'Active', '60%'),
(98, 'bc0e5fa0718aae0b48d334a997a38dee', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Winnie Choo', 'Choo Yee Hoon', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-17 16:00:00.0', '830204-02-5334', 'Feb', '012-4169623', 'cyhwinnie@gmail.com', 'PBB', '4837479516', '282 Jalan Air Itam 11400 Pulau Pinang.', 'Active', '70%'),
(99, '86374e776a7cda813a116c69c0e9f237', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'William Ang', 'Ang Wei Chieh', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '871030-35-5253', 'Oct', '012-4267123', 'property7123@gmail.com', 'HLB', '29301007686', '28, Kim Bian Aik Road', 'Active', '60%'),
(100, '53c7c6f4ccbdc7b08f767068b83c3930', 'Wei Liang', 'Ping Koay', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'George Teoh', 'Teoh Ryh Yaw', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-17 16:00:00.0', '951003-07-5715', 'Oct', '016-4343540', 'georgeyaw777@gmail.com', 'HLB', '3835-0123-031', 'Blk H-5-4, Padang Tembak, Air Itam, 11400 Penang.', 'Active', '60%'),
(101, 'dd45160cecddc32d47754dd17a444396', 'Mint Teow', 'Kenny Ooi', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Eric Boh', 'Boh Cui Wei', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '941201-07-5725', 'Dec', '012-4389079', 'ericboh121@gmail.com', 'PBB', '4645-1725-20', '100-5-16, Taman Selatan, Medan Penanga, 11600 Penang.', 'Active', '65%'),
(102, '2a3f5913da4294b3056957773f9d3967', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'James Lee', 'Lee Seng Hor', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-19 16:00:00.0', '981012-07-5095', 'Oct', '0111-2500957', 'jlsh11004@gmail.com', 'MBB', '1.57148E+11', '102-1-2 Jalan Kampar, Taman Jelimas,10460 Penang.', 'Active', '60%'),
(103, 'ee4971ffb4aff1a237fbde35ac9b0973', 'Ping Koay', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Ronald Kee', 'Kee Wei Lam', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-17 16:00:00.0', '970828-35-5301', 'Aug', '010-5600273', 'weilam_1997@outlook.com\nronaldkee.1997@gmail.com', 'PBB', '6906-3919-03', '488L-22-10 Condo Berjaya, Jalan Burma, 10350 Georgetown, Penang.', 'Active', '65%'),
(104, '1ab172d34822f0b328fcb71e3a5b3623', 'Jessica Yeong', 'Shaun Lee', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Adwin Lim', 'Lim Zhi Hao', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '960513-07-5445', 'May', '012-4498684', 'adwin_lim@yahoo.com\nAdwinlim0513@gmail.com', 'RHB', '1.07019E+13', '1-18-7 The Rise Collection 2, Jalan Dr Wu Lien Teh Taman Harapan 10150 PG.', 'Active', '60%'),
(105, '393bdf781efec3e545d90cf86ecba6ce', 'Ping Koay', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Wei Liang', 'Tan Wei Liang', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-17 16:00:00.0', '960831-07-5133', 'Aug', '012-5150831', 'tanweiliang6@icloud.com\ntweiliang168@gmail.com', 'MBB', '1571-2022-2406', 'Blk H-5-4 Rumah Pangsa Padang Tembak, 11400 Ayer Itam Penang.', 'Active', '60%'),
(106, '1155abf7f0885f4581a68bd12687976f', 'Joe Ooi', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Wayne Koid', 'Koid Soon Huat', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '870824-07-5339', 'Aug', '010-3858468', 'wayne1636@hotmail.com\nwayne8468@gmail.com', 'PBB', '4503307427', 'Block 2-10-2 Lengkok Nipah 3 Sg Dua 11700 Gelugor Pulau Pinang.', 'Active', '60%'),
(107, '29e2e32178dbd571a0b24dcc09987c47', 'Richard Lim', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Steve Chan', 'Chan Sim Chai', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '930702-07-5295', 'Jul', '017-7456412', 'stevechansimchai@gmail.com', 'PBB', '4590-3238-18', '19 Jalan Berapit Indah 1, Taman Berapit, 11600 BM.', 'Active', '70%'),
(108, '5a55b795c0c349b2b6161056cd938a29', 'Richard Lim', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Cherry Huan', 'Huan Szu Yong', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-17 16:00:00.0', '770821-07-5992', 'Aug', '016-4144821', 'huancherry@yahoo.com\nhuancherry2016@gmail.com', 'PBB', '6867-3150-07', '56-C, Jalan Matang Kuching, 11500 Ayer Itam, Penang.', 'Active', '65%'),
(109, '7de3b7779f067ca6685dfb340059bb30', 'Joe Ooi', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Saw', 'Saw Boon Loong', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '910705-07-5153', 'Jul', '012-6892873', 'sawboonloong@hotmail.com\nsawboonloong@gmail.com', 'PBB', '6352068407', '4A-12-21 Lorong Semarak Api 1 11500', 'Active', '70%'),
(110, '420b377c1eb2ad409f8341ca9e118199', 'Calvin Keoh', 'Cris Ng', 'Kenny Ooi', 'Jen Tay', 'Max Tay', 'Sanny Goh', 'Goh Wei San', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-17 16:00:00.0', '930523-07-5097', 'May', '016-4879670', 'GWSan.93@hotmail.com\nGWSan.93@gmail.com', 'PBB', '6348276632', '60 Solok Titi Teras 1, 11000 Balik Pulau, Penang.', 'Active', '60%'),
(111, '1e04eea187fea1a584dd36e25c01d5f4', 'Winnie Choo', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Kelly Sim', 'Sim Ah Kim', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-17 16:00:00.0', '581031-02-5592', 'Oct', '016-4877941', 'kellsim88@yahoo.com\nbellysim88@gmail.com', 'CIMB', '7020708084', '5-4-6 Edgecumbe Court', 'Active', '60%');
INSERT INTO `user` (`id`, `uid`, `upline1`, `upline2`, `upline3`, `upline4`, `upline5`, `username`, `full_name`, `position`, `user_type`, `branch_type`, `password`, `salt`, `date_created`, `ic`, `birth_month`, `contact`, `email`, `bank`, `bank_no`, `address`, `status`, `sub_sales_comm`) VALUES
(112, '40039314520fab8c1866e4c1066f4c5a', 'Mint Teow', 'Kenny Ooi', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Albee Poh', 'Poh Soo Yong', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '881021-35-5746', 'Oct', '012-5532520', 'albeepoh88@gmail.com', 'MBB', '1.07108E+11', '87 Lorong Jawi Jaya 7, Taman Jawi Jaya, 14200 Jawi PG.', 'Active', '60%'),
(113, '0a8cbe9ff22b9cf3f35bcc9946e0b658', 'Chai', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Eric Saw', 'Saw Boon Beng', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '980512-07-5599', 'May', '012-4503019', 'ericsaw98@gmail.com', 'MBB', '1573-3509-9428', '1-7-7 Taman Lone Pine, Lebuh Rambai 14, Paya Terubong 11060 Penang.', 'Active', '6000%'),
(114, '3369be14e4bd274620225d5e715f9ef2', 'Shaun Lee', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Jessica Yeong', 'Yeong Kah Yuen', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '860509-35-5454', 'May', '012-6776397', 'jessicayo509@gmail.com', 'HLB', '38300011124', '30, Lintang Glugor, 11600 Jelutong, Penang.', 'Active', '60%'),
(115, '398a604aaf73379c3f057f15114dab57', 'Chai', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Kelvin Toh', 'Toh Keng Sheng', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '000416-07-0169', 'Apr', '017-4345431', 'kelvintoh101@gmail.com', 'MBB', '1574-2916-5893', '1-28-8 Sierra Residence, halaman Sungai Ara 11900 Penang.', 'Active', '60%'),
(116, 'be00c671742aff13b7d16132c7e64edf', 'Cris Ng', 'Kenny Ooi', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'William Lim', 'Lim Jia Wei', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '940904-02-5025', 'Sept', '017-4542012', 'showlim9839@gmail.com', 'PBB', '4606-2095-04', '78-5-1, Casa Impian, Jalan Ooi Thiam Siew, 11600 Georgetown.', 'Active', '70%'),
(117, 'cb4c8ec641a61f1f6a29e2a247a4cdd1', 'Chai', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Jayden Chin', 'Chin Tian Kai', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '950706-07-5477', 'Jul', '010-5669243', 'jaydenchin95@gmail.com', 'PBB', '4646-8861-26', 'B-2-2 Flat LC Jalan Paya Terubong, Ayer Itam 11060 Penang.', 'Active', '65%'),
(118, 'c51b6462a97fb29845600dbf7bfc07a8', 'Cris Ng', 'Kenny Ooi', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Calvin Keoh', 'Keoh Kheng Seang', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '930804-07-5173', 'Aug', '016-4645468', 'keoh5173@gmail.com', 'PBB', '4647943533', '486-H, MK 12 Jalan Batu Maung 11960 Bayan Lepas Penang.', 'Active', '70%'),
(119, '68f57d7108014ec7ec284dd207843350', 'Chai', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Ian Tan', 'Tan Woei Yang', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '940219-02-5043', 'Feb', '012-4920623', 'ygtan824@gmail.com', 'MBB', '1571-3999-5333', 'L-12-2, Desa Permai Indah Apt, Jalan Helang 11700 Penang.', 'Active', '60%'),
(120, 'd1baecff072bd6053437394fbc92c316', 'Raven Saw', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Lucas Tay', 'Tay Kean Seon', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '820727-07-5673', 'Jul', '011-15985996', 'taykeanseon@outlook.com\ntaykeanseon@gmail.com', 'MBB', '1572-4101-2108', 'E1-06-07 Lorong Gangsa, Greenlane 11600 Georgeown Penang.', 'Active', '60%'),
(121, '27a9480c10209eed6e8e520ff9b14a37', 'Raven Saw', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Albert Ang', 'Ang Zhi Liang', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-17 16:00:00.0', '870108-07-5143', 'Jan', '016-4474523', 'angzhiliang@gmail.com', 'PBB', '6325-1095-05', '2, Solok SLIM Georgetown, 11600 Penang.', 'Active', '60%'),
(122, '03d5411e04d13910cc47c7521d5a4a07', 'Kenny Ooi', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Katherine Ong', 'Ong Xin Yean', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-24 16:00:00.0', '950404-07-5084', 'Apr', '016-5589369', 'cat.ong@outlook.com\nkath26.properties@gmail.com', 'PBB', '6327061525', '1-12-3A Taman Bukit Dumbar 11700 Gelugor Pulau Pinang.', 'Active', '60%'),
(123, '42522b53c076ec797079bded782eea55', 'Wayne Koid', 'Joe Ooi', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'YS Lee', 'Lee Yee Seong', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-09 16:00:00.0', '820522-08-6125', 'May', '012-4276523', 'leeys16888@gmail.com\ngic.ivanchong@gmail.com', 'CIMB', '7071645452', '1062-0947 Taman Hijau Paya Terubong.', 'Active', '60%'),
(124, '4f0082bd01b16d8aef49f099d2eb80a1', 'Katherine Ong', 'Kenny Ooi', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Jane Yeoh', 'Yeoh Sze Hung', 'Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-12 16:00:00.0', '940804-07-5586', 'Aug', '014-3077919', 'yeohsh94@gmail.com', 'CIMB', '7023126313', 'Blk 16-4A-8, Jalan Tun Dr. Awang 11900 Bayan Lepas Penang.', 'Active', '65%'),
(125, 'e7fc30539cd134b2e08da33f67daa876', NULL, NULL, NULL, NULL, NULL, 'admin1', 'admin1', NULL, 1, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Active', ''),
(126, '9c1bf2ce9a0d9ac4166e2941147fb925', NULL, NULL, NULL, NULL, NULL, 'admin2', 'admin2', NULL, 2, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Active', ''),
(127, '2cf82c90790c7ec427a7dfe4f48bfaa3', NULL, NULL, NULL, NULL, NULL, 'summerton1', 'summerton1', NULL, 1, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Active', ''),
(128, '32b0184a89206ecf2215c626c4f4428c', NULL, NULL, NULL, NULL, NULL, 'summerton2', 'summerton2', NULL, 2, 2, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Active', ''),
(129, '94db2be13431a5e704eeafa9f095d387', 'Eddie Song', '', '', '', '', 'Jeffrey Chan', 'Chan Jiang Liang', 'Project Leader', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '820824-07-5181', 'Aug', '012-5812903', 'chanjiangliang@gmail.com', 'PBB', '3187-7341-08', '694-A Mukim 16, Jalan Lintang, 11500 Ayer Itam, Pulau Pinang', 'Active', '75%'),
(130, '33ce05b7b616acc27d4e5ea25c5c0447', '', 'Eddie Song', '', '', '', 'Jocelyn Tok', 'Tok Pei Chi', 'Project Leader', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '850319-07-5620', 'Mar', '016-5573696', 'Jocelynchi7296@gmail.com ', 'PBB', '6390-2563-00', '1H, Jalan Beriksa Satu, 11500 Ayer Itam, Penang', 'Active', '75%'),
(131, '6323c16e9d69b0e382f904f1d19559b8', '', 'Eddie Song', '', '', '', 'Jojo Cheah', 'Cheah Mei Qing', 'Project Leader', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '851106-07-5218', 'Nov', '016-4105611', 'jocelyn85991@gmail.com', 'PBB', '4910-2518-04', '5-05-5 Jalan Arratoon, 10050 Pulau Pinang', 'Active', '70%'),
(132, '5013c34fa2dd942d183206e6180ce31a', 'Eddie Song', '', '', '', '', 'Jonathan Chng', 'Chng Zhi Fong', 'Project Leader', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '831107-07-5297', 'Nov', '016-4441232', 'gicjonathan6@gmail.com', 'PBB', '3169-5826-35', 'Blok 14-16-4, Tingkat Paya Terubong 3, 11060 Ayer Itam, Pulau Pinang.', 'Active', '70%'),
(133, 'a078a1fb7bedbebda56f02cac8b78640', 'Eddie Song', '', '', '', '', 'Kirby Lee', 'Lee Kong Beng', 'Project Leader', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-01-01 16:00:00.0', '760319-07-5269', 'Mar', '019-4222112', 'leekirby36@gmail.com', 'PBB', '4937-6500-12', '9-5-3 Melody Vila, Lintang P.Ramlee, 10460 Penang.', 'Active', '70%'),
(134, '0bc124161e38e0c86c20e4f2eb872781', 'Joe Yeap', 'Jonathan Chng', 'Eddie Song', '', '', 'James Boon', 'Lim Chin Boon', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-12-03 16:00:00.0', '921217-02-5687', 'Dec', '012-4418613', 'gicjamesboon@gmail.com', 'PBB', '4616-9361-27', 'C-04-01, Rifle Range, Jalan Padang Tembak, Ayer Itam, 11400, Penang', 'Active', ''),
(135, '98077d52447dec22385782439903edd0', 'Jeffrey Chan', 'Eddie Song', '', '', '', 'Chanel Hoe', 'Hoe Mei Ling', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '840612-01-6292', 'Jun', '018-2248586', 'chanelhoe612@gmail.com', 'PBB', '6405-7018-22', '37-5-9 Lebuh Nipah, Taman Lip Sin, Sungai Nibong, 11900 Bayan Lepas, Pulau Pinang.', 'Active', ''),
(136, '3fd025897299aefe082ab55b23468d5c', 'Berth Heng', '', 'Eddie Song', '', '', 'Angelyn Seng', 'Angelyn Seng Hsiao Ching', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-09-03 16:00:00.0', '870722-07-5274', 'Jul', '012-6440722', 'angelynching1234@gmail.com', 'PBB', '', 'A2-9 Lintang Batu Lanchang, 11600 Penang', 'Active', ''),
(137, '158758791d09355a5f600b0e010991a5', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '', '', 'Wynee Ong', 'Ong Wan Mei', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-04-28 16:00:00.0', '920701-07-5024', 'Jul', '016-4450032', 'hello.wynee@gmail.com', 'PBB', '6490-8776-35', '9, Lorong Rozhan 1, Taman Rozhan, 14000 BM.', 'Active', ''),
(138, '350758679f96e2e60b2c3dbb33dc3893', 'Jack Hng', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Christopher Khoo', 'Khoo Shu Qi', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-05-26 16:00:00.0', '971106-07-5855', 'Nov', '011-11042185', 's6uqizzz@gmail.com', '', '', '2A-17-7, Imperial Residences, Lintang Sg Ara 14, 11900 Penang.', 'Active', ''),
(139, '833ecf09d7c73de8e0f44895d5ea1111', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', '', 'Chris Teoh', 'Teoh Tze Terng', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-18 16:00:00.0', '930527-07-5537', 'May', '016-6665823', 'teohtterng@gmail.com', 'PBB', '4743-6384-10', '15, Lebuh Rambal 9, Paya Terubong 11060 Penang.', 'Active', ''),
(140, '2f0a78b8e26bca3784bfa61e4d7c1066', 'Khai Shian', 'Jovin Pang', 'Dyson Ng', 'Carson Chuah', 'Jordan Foo', 'Yow Huah', 'Ong Yow Huah', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-24 16:00:00.0', '990309-07-6325', 'Mar', '012-4700083', 'decaderedone@outlook.com', '', '', '112A, Taman Sri Mewah Indah, 11960, Bayan Lepas, Penang.', 'Active', ''),
(141, 'a269d5cb2a7d2ad24d3b9ed8f250078b', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', '', 'Kah Seng', 'Wong Kah Seng', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-27 16:00:00.0', '900126-07-5387', 'Jan', '012-4198088', 'wongkasen@gmail.com', 'PBB', '4657-5737-33', '149 Gerbang Bukit Kecil 2, 11900 Sg Nibong Penang.', 'Active', ''),
(142, '435685b4ad61538d95678d9405721318', 'Ben Liau', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', 'Sharon Tan', 'Tan Yi Yi', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-25 16:00:00.0', '980414-07-5288', 'Apr', '018-2253001', 'yiyitan0414@gmail.com', '', '', '19-06B,  Lintang Sg Ara 2, 11900 Bayan Lepas Penang.', 'Active', ''),
(143, '62e46f26cf8ab1adcd2d7a7c1efba458', 'Pui Yin', 'Darien Loy', 'Patrick Oon', 'Eddie Song', '', 'Yik Chia', 'Phang Yik Chia', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-07-14 16:00:00.0', '920227-07-5181', 'Feb', '016-4353341', 'yikchia@gmail.com', 'PBB', '6316-9955-26', '105B-2-7 Baystar Condo, Persiaran Bayan Indah 1, 11900 Penang.', 'Active', ''),
(144, '1c9698f5adc361315066a5a80236beda', '', 'Jeffrey Chan', 'Eddie Song', '', '', 'Kai Xiang', 'Lim Kai Xiang', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-01-07 16:00:00.0', '880830-35-5133', 'Aug', '012-7980506', 'kaixiang.lim12@gmail.com', 'PBB', '4854308221', '6591-F, Jalan Melur, Bagan Ajam 1300 Bworth, Penang', 'Active', ''),
(145, '48af21e8131a91acb48258ed4453bd92', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', 'Jia Kit', 'Chng Jia Kit', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-07 16:00:00.0', '961109-07-5229', 'Nov', '011-11109587', 'jkchng96@gmail.com', 'PBB', '6482025126', '97, Persiaran mahsuri 2/4, 11900 Bayan Lepas, Penang', 'Active', ''),
(146, '1f93dd227b6bd3938d2083c564140604', 'Patrick Oon', 'Eddie Song', '', '', '', 'Michelle Tan', 'Tan Sok Kean', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-09-06 16:00:00.0', '830226-07-5228', 'Feb', '012-6781927', 'michelletan1927@gmail.com', '', '', '10, Jalan Satu Air Itam 11400 Penang', 'Active', ''),
(147, '2f5b71fd8d0257227f584ca84485671f', 'Harry Lim', 'Berth Heng', '', 'Eddie Song', '', 'Kenny Soo', 'Soo Choon Kuen', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-08-15 16:00:00.0', '961201-08-5163', 'Dec', '018-4007975', 'choonkuen23@gmail.com', 'PBB', '6494796622', 'No. 20, Persiaran Pakatan Jaya 11, Taman Pakatan Jayam 31150 Ulu Kinta, Perak', 'Active', ''),
(148, '0a3d82095bf54288360086225976e3f1', '', 'Sam Tan', 'Kirby Lee', 'Eddie Song', '', 'Kang Jie', 'Khor Khang Jian', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-08-19 16:00:00.0', '930503-07-5583', 'May', '016-4277056', 'Jie5583@gmail.com', 'PBB', '6371421211', '2-3A-02, Taman Sinar Pelangi, Lorong Penawar 2, 11600 P.P', 'Active', ''),
(149, '00ff6ecd2d3924ca7c48423977cfc073', 'Kang Jie', 'Sam Tan', 'Eddie Song', 'Eddie Song', '', 'Seng Chung', 'Chong Seng Chung', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-08-19 16:00:00.0', '720513-07-5397', 'May', '017-5682281', 'chongsengchung@gmail.com', 'PBB', '4547081619', '483, Jalan Pokok Cheri, 11500 Air Itam, PG', 'Active', ''),
(150, '2e4c24675c4eedeb469439a506775f28', 'Yao Guang', 'Ben Liau', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Lin Khai', 'Chen Lin Khai', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-07-09 16:00:00.0', '990915-07-5381', 'Sep', '016-5637223', 'ckkai168@gmail.com', '', '', '90-8-19 Kota Emas Lorong Serembang 10150 Georgetown Pulau Pinang.', 'Active', ''),
(151, '8e2fe8d4a5062117c3e23f7c02a5bc8f', 'Dyson Ng', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', 'Charity Neoh', 'Neoh Yi Ping', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-02-11 16:00:00.0', '960530-07-5068', 'May', '016-4214777', 'neohcharity@gmail.com', 'PBB', '6492531201', '18, Persiaran Mahsuri 216, Sunway Tunas 11900 Penang.', 'Active', ''),
(152, 'f858bb4834884d02240cf23985b1da12', 'Ai Pheng', '', 'Eddie Song', '', '', 'SK Oi', 'Oi Siou Kia', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-11-26 16:00:00.0', '710510-07-5667', 'Jul', '012-4898778', 'skoi777111@gmail.com', '', '', '38, Jalan Pinhorn, 11600 Penang.', 'Active', ''),
(153, '53100b956c71a7cec5edc67e3e58dabe', 'Daniel Poh', 'Ben Liau', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Austin Yeoh', 'Yeoh Wei Jiang', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-10-01 16:00:00.0', '931122-12-5123', 'Nov', '014-3559660', 'yeohweijiang@gmail.com', 'PBB', '6887388020', 'D-2-9 Lorong Nakhoda Taman Telok Air Tawar 13050 Butterworth Penang.', 'Active', ''),
(154, '821debe44f79d970a0ce8fa8716e521f', 'Patrick Lim', 'Eddy Wong', 'Jonathan', 'Eddie Song', '', 'Chester Kam', 'Kam Kok Leong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-12 16:00:00.0', '891223-07-5475', 'Dec', '012-2080225', 'Chester89leong@gmail.com', '', '', '146-A Jalan Padang Tembao, 11400 Ayer Itam, Penang.', 'Active', ''),
(155, 'd250bfabf80727bdbb2944910beba57a', 'Patrick Oon', 'Eddie Song', '', '', '', 'Aric Teh', 'Teh Kok Teng', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '920704-07-5329', 'Jul', '012-9488378', 'aricteng@gmail.com', 'PBB', '6423-8506-16', '', 'Active', '75%'),
(156, '5cd9988919a6170d2dbdd990fdf62d2d', '', 'Eddie Song', '', '', '', 'Berth Heng', 'Heng Bei Kuang', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2017-09-19 16:00:00.0', '810925-02-5352', 'Sep', '011-62462868', 'berthaheng@gmail.com ', 'PBB', '4936-9535-36', '12A, Lorong Seri Senangan 2, Taman Seri Senangan, 13050, Butterworth, Penang.', 'Active', '70%'),
(157, '9b7d208a25b02b55fa23046c37cbdc57', 'Jeffrey Chan', 'Eddie Song', '', '', '', 'Brian Ho', 'Ho Pin Chuan', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '820115-07-5277', 'Jan', '012-4280115', 'brianhpc82@gmail.com', 'PBB', '4803-7399-28', '62-4-8 Lebuh Sg Pinang 1, Pinang Court 2 ,11600 Penang.', 'Active', '70%'),
(158, '7bd22a2ce76f3e0acbb7eb815138ed85', 'Aric Teh', 'Patrick Oon', 'Eddie Song', '', '', 'Caron Wong', 'Caron Wong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '920515-07-5725', 'May', '016-5515470', 'caronwong1505@gmail.com', 'PBB', '4658-7247-28', 'Q1-5, Desa Permai Indah , Jalan Helang, Sungai Dua 11700 Pulau Pinang.', 'Active', '75%'),
(159, 'fac3e2951fdd3614d3af6437c63db412', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', '', '', 'Carson Chuah', 'Chuah Eng Hong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-08-14 16:00:00.0', '960106-08-5611', 'Jan', '018-4096321', 'Enghong96c@gmail.com', 'PBB', '6467-2118-16', '63B-6-11, Jalan Lenggong Jelutong, 11600 Penang.', 'Active', '75%'),
(160, '41973415464aab9f7a11ff7b5d7d82da', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', '', 'Catherine Wong', 'Catherine Wong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '981121-07-6468', 'Nov', '016-4673181', 'wongcatherine3@gmail.com', 'PBB', '6483-3356-32', 'Q1-5, Desa Permai Indah , Jalan Helang, Sungai Dua 11700 Pulau Pinang.', 'Active', '60%'),
(161, 'db8758e22aa9296baad4a2976c763f0e', 'Patrick Oon', 'Eddie Song', '', '', '', 'Chew', 'Chew Choon Seng', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '510901-07-5027', 'Sep', '016-4882793', 'manntitanuim427@gmail.com', '', '', '', 'Active', '70%'),
(162, 'f0f7302ec9084b2692d6a3f366e081fe', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', '', 'Choo', 'Choo Hon Kiat', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-01-08 16:00:00.0', '920528-08-5885', 'May', '016-5686126', 'gic.choo5885@gmail.com ', 'PBB', '4629-9982-27', 'Blok 2-22B-08, One World Tingkat Mahsuri 2, Bayan Baru, 11950 Bayan Lepas, Penang.', 'Active', '75%'),
(163, '3d74a4d7904efaa1cd501e8786ea19d0', '', 'Eddie Song', '', '', '', 'Clover Neoh', 'Neoh Jia Huey', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2017-08-08 16:00:00.0', '970220-07-5504', 'Feb', '012-4280220', 'Theofficialcloverjay@gmail.com', 'MBB', '1570-3667-6319', 'Block 9A-07-02 Taman Kheng Tean, Jalan Van Pragh 11600 Penang.', 'Active', '70%'),
(164, '9e668e3fb20ef8711584501e53464c9a', 'Joe Yeap', 'Jonathan Chng', 'Eddie Song', '', '', 'Eunice Loe', 'Loe Yew Lee', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-02-20 16:00:00.0', '850807-07-5654', 'Aug', '016-4642480', 'euniceloe85@gmail.com', 'PBB', '4676-7465-02', '6C-29-03, All Season Park, Autumn Tower, 11500 Bandar Baru, Ayer Itam, Penang', 'Active', '70%'),
(165, '25e2d7fff8fcd55a4380e383aa670124', '', 'Eddie Song', '', '', '', 'Joanne Chew', 'Chew Lai Kuen', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '580512-07-5596', 'May', '012-4991505', 'joannechew1991@gmail.com', 'PBB', '6464-3329-22', '19-12-12 Taman Seri Sari Hilir Paya Terubong 1, Bayan Lepas, 11900 Penang', 'Active', '75%'),
(166, 'f4ab21f4b78e7a32d858e5e9017aaa0c', 'Jeffrey Chan', 'Eddie Song', '', '', '', 'Jordan Foo', 'Foo Yi Heng', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '940426-07-5633', 'Apr', '017-4048577', 'jordanfoo729@gmail.com', 'PBB', '4766-8349-31', 'Block 99-7-3A, Lintang Sg Pinang, Taman Pelangi Indah, 11600 Penang.', 'Active', '75%'),
(167, '5c5791681ea2024fbc925988ef62a82f', '', 'Eddie Song', '', '', '', 'Kimberlyn Lim', 'Lim Bee Ley', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '680313-07-5364', 'Mar', '012-4545226', 'kimberlyn6868@gmail.com', 'PBB', '3176-4305-05', '6E-15-12, Melody Homes, Lebuhraya Thean Teik, 11500 Pulau Pinang', 'Active', '70%'),
(168, 'c403d57890e79393d11623a87088f0a6', '', 'Eddie Song', '', '', '', 'Lim Ewe Jin', 'Lim Ewe Jin', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '660706-07-5519', 'Jul', '016-6027799', 'limewejin@gmail.com', 'PBB', '6800-3239-28', '2D-01-06 Medan Angsana 1, 11500 Ayer Itam, Pulau Pinang.', 'Active', '70%'),
(169, '096b6e3afbfd5c8ed3c683c93b102d11', '', 'Eddie Song', '', '', '', 'Louise Tan', 'Tan Chang Looi', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '750809-07-5262', 'Aug', '019-5088986', 'louisetan75@gmail.com', 'PBB', '6465-4000-19', '56 Jalan Tembusu, Bayan Lalang, 13400 Butterworth', 'Active', '70%'),
(170, '24c069d7ef24cdd18c2459e43857268b', 'Patrick Oon', 'Eddie Song', '', '', '', 'Loy', 'Loy Ter Ren', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '851002-07-5547', 'Oct', '012-4918110', 'darienloy@gmail.com ', 'PBB', '6909-9803-02', 'Blok 9-15-7 Tkt Paya Terubong 2, Ayer Itam, 11060 Pulau Pinang.', 'Active', '70%'),
(171, 'c747ece9ca712eb7a46b6ced282dd3c3', '', 'Eddie Song', '', '', '', 'Marcus Koay', 'Koay Teng Koo', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '821005-07-5123', 'Oct', '011-28487887', 'marcuskoay7887@gmail.com', 'PBB', '4927-2036-01', '36-9-9, Sri Bukit Jambul, 11900 Penang', 'Active', '70%'),
(172, 'c1758a259b8f2aa742f445857d54b52d', 'Joe Yeap', 'Jonathan Chng', 'Eddie Song', '', '', 'Philip Yeoh', 'Yeoh Chin Yong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-07-18 16:00:00.0', '930102-07-5523', 'Jan', '012-2579985', 'Philipyong93@gmail.com ', 'PBB', '4613-5886-13', '7-G-1, Jalan Padang Tembak, 11400 Ayer Itam, Penang', 'Active', '60%'),
(173, '6862b34b0c2111cd6bc47eafadb23d38', 'Patrick Oon', 'Eddie Song', '', '', '', 'Robert Ooi', 'Ooi Chin Aun', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '830405-07-5645', 'Apr', '012-4928363', 'gicrobertooi@gmail.com', 'PBB', '4540-2536-25', '', 'Active', '75%'),
(174, 'c2f00eda1baee189eea8a581f744cd31', 'Joe Yeap', 'Jonathan Chng', 'Eddie Song', '', '', 'Yeap', 'Yeap Tan Kheng', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2018-02-12 16:00:00.0', '700904-07-5337', 'Sep', '011-62816886', 'gic.yeap@gmail.com', 'PBB', '3118-0290-28', '118-B5-07-03, Putra Place Condo, Persiaran Bayan Indah, Penang', 'Active', '70%'),
(175, 'b64c810b74e7e9fbeb75f267acde3b19', 'Ewe Kok Tai', 'Patrick Oon', 'Eddie Song', '', '', 'Josh Ang', 'Ang Kok Lim', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-04-02 16:00:00.0', '760510-07-5603', 'May', '012-6523899', 'joshang78@gmail.com', '', '', '17-09-09 Serina Bay, Hilir Sungai Pinang, 11600 Jelutong, Penang.', 'Active', '70%'),
(176, '790fffa2f7e4e4e6237996d453c104e1', 'Ewe Kok Tai', 'Patrick Oon', 'Eddie Song', '', '', 'Angie Khoo', 'Khoo Ai Hong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-04-02 16:00:00.0', '780606-07-5390', 'Jun', '012-4290229', 'angiekhoo06@gmail.com', 'PBB', '4008 3674 15', '17-09-09 Serina Bay, Hilir Sungai Pinang, 11600 Jelutong, Penang.', 'Active', '80%'),
(177, '2d610a323cb8b59aa72e0a91683547fe', 'Berth Heng', '', 'Eddie Song', '', '', 'Harry Lim', 'Lim Swee Hua', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-02-18 16:00:00.0', '740817-07-5319', 'Aug', '012-4521783', 'lim.sweehua@gmail.com', '', '', '12A, Lorong Seri Senangan 2, Taman Seri Senangan, 13050, Butterworth, Penang.', 'Active', '70%'),
(178, '5f53420800398abe8854294cee49b43a', 'Patrick Oon', 'Eddie Song', '', '', '', 'Donald Leong', 'Law Kim Leong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '821019-07-5693', 'Oct', '016-4750621', 'lawleong1982@gmail.com', 'PBB', '4572255321', 'Blk 0-16-7, Taman Desa Relau 2, 11900 Pulau Pinang.', 'Active', '70%'),
(179, '7e15a52fad7f687a387db70e99d52365', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', '', 'Dyson Ng', 'Ng Dian Sheng', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-04-22 16:00:00.0', '960213-07-5021', 'Feb', '011-36298210', 'dianshengng@gmail.com', 'PBB', '6486-8064-34', 'No.3 Medan Mudan 1, Sungai Dua, 11700 Gelugor, Pulau Pinang.', 'Active', '75%'),
(180, 'd6c1ea63640018dff36f2070743d1e16', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', '', 'Suki Ooi', 'Ooi Suki', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '850216-07-5644', 'Feb', '016-4415383', 'miyuki.ooi123@gmail.com', 'PBB', '', 'M10-4, Jalan Helang, 11700, Sg Dua, Penang', 'Active', '70%'),
(181, '57a628f9aeb9bfa926a2ca30639fa742', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', '', 'Ben Liau', 'Ben Liau Tze Chen', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-04-24 16:00:00.0', '991209-02-6173', 'Dec', '013-533-9546', 'liautzechen@gmail.com', 'PBB', '', '160, Darulaman Heights, 06000 Jitra, Kedah.', 'Active', '60%'),
(182, 'cd4fa46fe669724f4d7cd9342cec2702', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '', '', 'Agnes Lim', 'Lim Siew Hua', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-04-28 16:00:00.0', '930222-02-5248', 'Feb', '017-4366940', 'siewhua_22@hotmail.my', 'PBB', '4559-5258-11', '23, Tmn Tunku Hosna, Fasa 2, 05300 Alor Star, Kedah.', 'Active', '70%'),
(183, '4fc96a3395b46b89fb30c1722dfb1196', 'Joe Yeap', 'Jonathan Chng', 'Eddie Song', '', '', 'Janet Tan', 'Tan Soo Shean', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-05-02 16:00:00.0', '691126-07-5100', 'Nov', '019-5259111', 'janettss2611@gmail.com', '', '', 'B-5-12 Lintang Hajjah Rehmah, Mutiara Heights, 11600 Jelutong, Penang.', 'Active', '60%'),
(184, '2331def2d10d84e173cb54446775e60d', 'Marcus Koay', '', 'Eddie Song', '', '', 'Cynthia Ann', 'Cynthia Ann Martin', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-05-02 16:00:00.0', '860415-35-5330', 'Apr', '013-6775553', 'cynthia415ann@gmail.com', 'MBB', '1570-6392-0653', '7E-03-09 Jalan Thean Teik, 11500 Penang.', 'Active', '60%'),
(185, 'c465428dc88ccf3d6311eb5001f5603c', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', '', 'Kelvin Ooi', 'Ooi Hong Seng', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-05-12 16:00:00.0', '960713-07-5431', 'Jul', '016-4120713', 'sengooi0713@gmail.com', 'PBB', '6464-9149-34', '60-05-06, Jalan Slim, 11600 Penang.', 'Active', '70%'),
(186, 'd7c1681f61bcbe7ad758d2c47e41cfea', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', 'Jack Hng', 'Hng Kah Liang', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-05-12 16:00:00.0', '961108-07-5585', 'Nov', '016-4365838', 'kahliang.hng.htp@gmail.com', 'PBB', '6489-2131-04', '6-2-8, Desa Bukit Jambul, Persiaran Bukit Jambul 6, 11900 Penang.', 'Active', '70%'),
(187, '81816798b89aa643d83cab1efcab877b', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '', '', 'Jun Ooi', 'Ooi Yeong Jiunn', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-05-23 16:00:00.0', '870926-35-5577', 'Sep', '016-4219268', 'yjooi87@hotmaill.com', '', '', '', 'Active', '70%'),
(188, 'd021497fa170e4f98f90b8cc44be179b', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', '', 'Jacelyn Mok', 'Jacelyn Mok Mei Suet', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-05-27 16:00:00.0', '991005-07-5460', 'Oct', '011-21989468', 'jacelyn27@yahoo.com', '', '', '266 MK1, Pantai Acheh, 11010 Balik Pulau, Pulau Pinang.', 'Active', '60%'),
(189, '3b767bc628c9775a3c5e0fb2547a882e', 'Jun Ooi', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '', 'Vivian Pua', 'Pua Kooi Khim', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-02 16:00:00.0', '911220-07-5600', 'Dec', '017-5283864', 'vivianqing1220@gmail.com', 'PBB', '4627-5253-10', '63B-6-12, Symphony Park, Jalan Lenggong, 11600 Penang.', 'Active', '70%'),
(190, '2a84ba18cc8db0c0e6ae5c9a4053f2a1', 'Ben Liau', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', 'Daniel Poh', 'Poh Shan Yi', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-11 16:00:00.0', '981124-07-5989', 'Nov', '014-3070518', 'gicpohshanyi@gmail.com', '', '', '18, Lengkok Kenari 2, Taman Desari, 11900 Bayan Lepas, Sungai Ara, Penang.', 'Active', '60%'),
(191, 'e27e657a1ebfd2df55f4d2a91a9aafd1', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', '', 'Lynn Seow', 'Lynn Seow Lii Ting', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-18 16:00:00.0', '000517-07-0686', 'May', '017-4501686', 'lynnslt517@gmail.com', '', '', '30, Lorong Permai 5, 11700 Penang.', 'Active', '60%'),
(192, '221d5fdeb4b93809d596404d1e393c7a', 'Jovin Pang', 'Dyson Ng', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Khai Shian', 'Tan Khai Shian', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-05-21 16:00:00.0', '990910-02-5123', 'Sep', '018-9486401', 'khaishiantan@gmail.com', 'CIMB', '7063-4496-12', '72A-23A-3A, Jalan Mahsuri, Arena Residence, 11950, Bayan Lepas, Penang.', 'Active', '60%'),
(193, '7bafa37724fd38e79b05b346e9f10c0f', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', '', '', 'Dean Lim', 'Lim Chun Ti', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-27 16:00:00.0', '950620-07-5441', 'Jun', '016-4846517', 'chuntilim95@gmail.com', 'PBB', '4530-9506-17', '90-8-15 Lorong Seremban, Kota Emas, 10150 Penang.', 'Active', '60%'),
(194, 'a23fa2232fbc428b4133e5ee144163df', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', '', 'David Lim', 'Lim Chien Her', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-27 16:00:00.0', '990202-07-5277', 'Feb', '012-4788187', 'davidlim6271@gmail.com', 'PBB', '6445-4322-16', '18-10-12B Menara Greenview, Halaman Tembaga, 11600 Penang.', 'Active', '60%'),
(195, '33429f1ca074be3e3d699b42e0d88ff7', 'Ben Liau', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', 'Yao Ming', 'Loh Yao Ming', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-25 16:00:00.0', '990724-07-5709', 'Jul', '012-4578800', 'lohyaoming1999@gmail.com', 'MBB', '1572-4108-3025', '5-8-5 Taman Lembah Hijau, Lorong Gangsa, 11600 Penang.', 'Active', '60%'),
(196, '0c91156052b271ff7677d4fe1ce9bb05', 'Ben Liau', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', 'Yao Guang', 'Loh Yao Guang', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-06-25 16:00:00.0', '990724-07-5661', 'Jul', '012-4178800', 'yaoguangloh@yahoo.com', '', '', '5-8-5 Taman Lembah Hijau, Lorong Gangsa, 11600 Penang.', 'Active', '60%'),
(197, 'a3210a0f35f5761cd0e1e6118cbae2bf', 'Kirby Lee', 'Eddie Song', '', '', '', 'Louis Lim', 'Lim Sions Hock', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-07-21 16:00:00.0', '800130-07-5285', 'Jan', '017-9008952', 'pgllouis@gmail.com', 'PBB', '4048-3503-29', 'Block 9B-17-02, Taman Kheng Tian, Jalan Van Praagh, 11600 Penang.', 'Active', ''),
(198, '2acce25e5f509361b41159b5b719bb46', 'Kirby Lee', 'Eddie Song', '', '', '', 'YC Tan', 'Tan Yeong Chuan', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-07-21 16:00:00.0', '740826-07-5177', 'Aug', '010-3888111', 'tanyeongchuan1@gmail.com', 'PBB', '4330-6968-24', '18 Jalan Bukit Minyak, Taman Kota Permai, 14000 BM Penang.', 'Active', ''),
(199, '526f6422922b0149fdc0a5938f313a07', 'Kirby Lee', 'Eddie Song', '', '', '', 'Khor', 'Khor Yu Nong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-07-21 16:00:00.0', '860704-35-5011', 'Jul', '012-4018315', 'ynkhor86@gmail.com', 'MBB', '1071-0804-8942', '18, Lorong Tekukur Indah 6, Taman Tekukur Indah', 'Active', ''),
(200, '876e51de1a904687496ea73e8cbeb25e', 'Kirby Lee', 'Eddie Song', '', '', '', 'Sam Tan', 'Tan Horng Lin', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-07-21 16:00:00.0', '740808-07-5069', 'Aug', '019-4815955', 'sjesuccess168@gmail.com', 'PBB', '6338-8373-10', '23-16-01 Alpine Tower, Tingkat Bukit Jambul 1, Bayan Lepas Penang.', 'Active', ''),
(201, 'd54df9a5e74dcf43349ab2c3879dfb48', 'Dyson Ng', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', 'Jovin Pang', 'Pang Chong Min', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-07-14 16:00:00.0', '991116-07-5312', 'Nov', '016-4196366', 'jovinpg@gmail.com', 'CIMB', '7065-1432-15', '47, Medan Sungkai, 10100 Pulau Pinang.', 'Active', '70%'),
(202, 'aae13497bbbb45f0ebc7ce7e22f03be4', 'Kirby Lee', 'Eddie Song', '', '', '', 'Kuhen', 'Kuhen A/L Kacinathan', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-08-03 16:00:00.0', '910603-07-5857', 'Jun', '018-2040945', 'Kuhen5025@gmail.com', '', '', '47-6-30 Paya Terubong, Jalan Air Itam 11060 Pulau Pinang', 'Active', ''),
(203, '51dc5c0a874dde299fd1da02d2794e58', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', '', 'Bryan Khoo', 'Bryan Khoo Tze Yong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-08-13 16:00:00.0', '991208-07-5475', 'Dec', '016-4939366', 'bryankhoo44@gmail.com', '', '', 'B3-7-4 Putra Place Persiaran Bayan Indah 3 11900 Sg Nibong', 'Active', '60%'),
(204, '18d043e3295ec6128e073a9c8cdb33d2', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', 'Crystal Ooi', 'Ooi Suet Yean', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-09-30 16:00:00.0', '930113-07-5254', 'Jan', '016-4204712', 'osy0113@gmail.com', 'PBB', '4639209816', '25, Jalan Hijau 4, 11600 Penang.', 'Active', '70%'),
(205, '6cafc733e24f24f8786003bbc86d4390', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', '', 'Agnes Lim', 'Lim Beng See', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-08-31 16:00:00.0', '761118-07-5558', 'Nov', '016-4059887', 'agneslim9887@gmail.com', '', '', '1-L, Medan Tembaga, Island Park, 11600 Pg.', 'Active', '707%'),
(206, '2a82fc295d1db1ca9f7a9c035a3bb272', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', '', 'Gino Lee', 'Gino Lee Chun Wey', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-09-13 16:00:00.0', '000401-07-0051', 'Apr', '010-2165562', 'ginoleechunwey@gmail.com', 'PBB', '6417436609', '99-11-15 Taman Pelangi Indah Lintang Sungai Pinang 11600 Georgetown Penang.', 'Active', '60%'),
(207, 'c314334c708246c53cf1e12f6ea170fc', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', '', 'Jasmine Kua', 'Kua Xue Qi', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-09-13 16:00:00.0', '001016-07-0164', 'Oct', '018-6631638', 'jaskua1234@gmail.com', 'PBB', '6930743814', 'Blk 15-9-8 Desa Permata, Paya Terubong 11060 Pulau Pinang', 'Active', '60%'),
(208, '451fdc7d8656f8e9aef524b516b901f1', 'Eunice Loe', 'Joe Yeap', 'Jonathan Chng', 'Eddie Song', '', 'Wei Ther', 'Tang Wei Ther', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-12-05 16:00:00.0', '940321-07-5231', 'Mar', '016-2535231', 'thertang21@gmail.com', '', '', '590 MK 4, Ayer Putih 11000 Balik Pulau, Penang', 'Active', '60%'),
(209, '227217c0c5a8ef8de3c715e67b954f86', 'Dyson Ng', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', 'Jordan Lee', 'Lee Xi Hong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-01-14 16:00:00.0', '960916-07-5327', 'Sep', '012-4108270', 'jordanleexh@gmail.com', '', '', '118B-7-1, Pearl Garden, Jalan Dato Ismail Hashim, 11900 Bayan Lepas Pulau Pinang.', 'Active', '60%'),
(210, '6da630e86dcfe2d3c6b8f700d9260c5e', 'Jonathan Chng', 'Eddie Song', '', '', '', 'Eddy Wong', 'Eddy Wong Chee Wei', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-01-29 16:00:00.0', '851205-07-5483', 'Dec', '012-5012777', 'eddyestate@gmail.com', 'PBB', '3206426023', '86-15-03, 86 Avenue Residences, Lengkok Perak, 11600 Jelutong Penang.', 'Active', '70%'),
(211, 'a3e1f5dc6b6aa7021c0aa88720149b9b', 'Clover Neoh', '', 'Eddie Song', '', '', 'Aaron Khoo', 'Khoo Xu Liang', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-01-29 16:00:00.0', '961102-07-5859', 'Nov', '016-5214150', 'aaronxu112@gmail.com', 'CIMB', '7066056836', '8-19-7, Sri Kristal, Lengkok Angsana, Bandar Baru, 11500 Pulau Pinang.', 'Active', '60%'),
(212, '4521e07a51b1e1c5faf69784dc6a1615', 'Gino', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', 'John Lim', 'Lim Kai Sen', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-02 16:00:00.0', '000811-07-0441', 'Aug', '016-4149903', 'roxenlim@gmail.com', '', '', '8-09-3A Sri Kristal Lengkok Angsana 11500 Ayer Itam Penang.', 'Active', '60%'),
(213, '78f8b552536572e04314313a42ad5a08', 'Jojo Cheah', '', 'Eddie Song', '', '', 'Danson Mark', 'Mark Wei Jen', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-03 16:00:00.0', '970802-35-5119', 'Aug', '012-4247699', 'dansonmarkwj@gmail.com', '', '', '', 'Active', '60%'),
(214, '05d7b32475bc62f47924b723d0b2d7ca', 'Crystal Ooi', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Alan Tan', 'Tan Boon Kwang', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-04 16:00:00.0', '800808-07-5087', 'Aug', '019-5704010', 'alanwin1980@gmail.com', '', '', '61-18-01 Suria Vista, Tingkat Paya Terubong 4.', 'Active', ''),
(215, '3696b5240d9919babffe0d520cb8c8fa', 'Jane Tan', 'Patrick Oon', 'Eddie Song', '', '', 'Joey Ong', 'Ong Jo Yee', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-29 16:00:00.0', '940128-14-5170', 'Jan', '016-5188014', 'joyee0128@gmail.com', 'PBB', '6935311824', '28, Persiaran Perdana 7, Pinji Perdana, 31500 Lahat, Perak.', 'Active', '60%'),
(216, '5d3bb097f481195b4525e185b3b4d12e', 'Aric Teh', 'Patrick Oon', 'Eddie Song', '', '', 'Bieber Ong', 'Ong Beng Fung', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-09 16:00:00.0', '940427-07-5264', 'Apr', '016-4143427', 'bieberong94@gmail.com', 'PBB', '4782517519', '75, Lorong 1, Taman Perkasa Permatang Tengah, 13000 Butterworth Penang.', 'Active', '70%'),
(217, 'ca24ca881e5058379d242693fd3b758e', 'Jovin Pang', 'Dyson Ng', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Edson Ang', 'Ang Min Tzer', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-23 16:00:00.0', '991105-07-6459', 'Nov', '016-4128608', '99mintzer@gmail.com', '', '', '13, Solok Birch, 10250, Georgetown, Penang.', 'Active', '60%'),
(218, '72d9c75df25b8e5eb536883f6507de3f', 'Eddy Wong', 'Jonathan Chng', 'Eddie Song', '', '', 'Patrick Lim', 'Lim Jin Tek', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-23 16:00:00.0', '840623-07-5677', 'Jun', '016-4416113', 'patricklim88@gmail.com', 'PBB', '4913821600', '', 'Active', ''),
(219, 'e070c0529480ecd35ca4a4a9566a05de', 'Jordan Foo', 'Patrick Oon', 'Eddie Song', '', '', 'Christine Lim', 'Lim Ean Ean', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-23 16:00:00.0', '751026-07-5332', 'Oct', '012-4801026', 'christinelim727@yahoo.com', 'MBB', '1.07135E+11', '57, Persiaran Mahsuri 2/1, Bandar Sunway Tunas 11900 Penang.', 'Active', '75%'),
(220, '51121ba7070971637922fb78b751d2b5', 'Patrick Lim', 'Eddy Wong', 'Jonathan Chng', 'Eddie Song', '', 'Andrew Khoo', 'Khoo Lit Wei', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-25 16:00:00.0', '900917-07-6001', 'Sep', '016-4339154', 'litweikhoo@hotmail.com', 'CIMB', '7630662286', '2C-23A-8, Lintang Sungai Ara 14, Imperial Resident, Bayan Lepas, Penang.', 'Active', ''),
(221, 'ca7473adc1cd6887e5b5387fc83e9ba5', 'Joe Yeap', 'Jonathan Chng', 'Eddie Song', '', '', 'Branden Lee', 'Lee Kok Seong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-26 16:00:00.0', '760706-07-5699', 'Jul', '011-35493867', 'geniusbear2u@gmail.com', 'PBB', '3156-831430', '16 Jalan Pisang Emas, Kampung Melayu 11500 Penang.', 'Active', ''),
(222, '9de5b19a420f03c4895a5eeeeddd7bd0', 'Patrick Lim', 'Eddy Wong', 'Jonathan Chng', 'Eddie Song', '', 'Adeline Chong', 'Chong Ming Yee', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-02-27 16:00:00.0', '840314-07-5016', 'Mar', '011-59219889', 'mingyee143@gmail.com', 'PBB', '6982768618', '17, Lorong Gamelan 3, Taman Gamelan 14200 Sg Bakap, Penang.', 'Active', '');
INSERT INTO `user` (`id`, `uid`, `upline1`, `upline2`, `upline3`, `upline4`, `upline5`, `username`, `full_name`, `position`, `user_type`, `branch_type`, `password`, `salt`, `date_created`, `ic`, `birth_month`, `contact`, `email`, `bank`, `bank_no`, `address`, `status`, `sub_sales_comm`) VALUES
(223, 'bb90608bab6f022475f5fd07b479c1f0', 'Patrick Lim', 'Eddy Wong', 'Jonathan Chng', 'Eddie Song', '', 'Alice Lim', 'Lim Poh Suan', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-01 16:00:00.0', '830223-07-5636', 'Feb', '012-5853113', 'alice.limps@gmail.com', '', '', '23 Lebuh Batu Maung 11, 11960 Penang.', 'Active', ''),
(224, '8fd573832243d33581a90c8c46fd0aae', 'Danson Mark', 'Jojo Cheah', 'N/A', 'Eddie Song', '', 'Edward Tan', 'Tan Yang Shing', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-01 16:00:00.0', '970101-07-5499', 'Jan', '011-36255159', 'ystan5499@gmail.com', 'PBB', '6497517134', '37 Lebuh Nipah Taman Lip Sin 11900 Penang.', 'Active', ''),
(225, '7477756f71d78e4fba0a8c0bf295921d', 'Crystal Ooi', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Jojo Eng', 'Eng Yan Joe', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-06 16:00:00.0', '951102-02-5166', 'Nov', '017-5358516', 'yanjoe1995@gmail.com', 'PBB', '6331915114', '281, Tmn Cengkih Simpang Empat 06650 Alor Setar Kedah.', 'Active', ''),
(226, 'f7a12939a1586243844dad71295ea4e8', 'Jojo Eng', 'Crystal Ooi', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Yong Chun', 'Tan Yong Chun', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-06 16:00:00.0', '980214-12-5279', 'Feb', '016-4127014', 'yongchun407@gmail.com', 'HLB', '29300034090', '2-2-12 Desa Mawar Kampung Melayu 11500 Ayer Itam Pulau Pinang.', 'Active', ''),
(227, 'cde09375cb33843c50059dde03fbeeb6', 'Yong Chun', 'Jojo Eng', 'Crystal Ooi', 'Kelvin Ooi', 'Carson Chuah', 'Wei Chun', 'Looi Wei Chun', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-06 16:00:00.0', '000116-07-0263', 'Jan', '018-2544263', 'chunlooi888@gmail.com', 'HLB', '29300034100', 'Lintang Melayu 2 11500 Ayer Itam P.P.', 'Active', ''),
(228, '5a2d45bbad81490638ebbd2121064e22', 'Crystal Ooi', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Wayne Loh', 'Loh De Wei', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-05 16:00:00.0', '950731-07-5321', 'Jul', '016-5279032', 'dewei950731@gmail.com', '', '', '1545, Taman Tan Sai Gin 14000 Bukit Mertajam.', 'Active', ''),
(229, '3fd3161fb94f6c7378395ea0fa809c0f', 'Kirby Lee', 'Eddie Song', '', '', '', 'Melissa Ang', 'Ang Hwei Li', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-08 16:00:00.0', '820828-07-5488', 'Aug', '016-6688363', 'hapigal2828@gmail.com', 'PBB', '4484994017', '163B-18-07, Centrio Avenue, Jalan Permai, 11700 Penang.', 'Active', ''),
(230, 'ee9474dfc566c665f8886d3bf9ded2ed', 'Kirby Lee', 'Eddie Song', '', '', '', 'CK Law', 'Law Choon Keat', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-08 16:00:00.0', '791116-07-5455', 'Nov', '012-4701116', 'andrislck@gmail.com', 'PBB', '6933 4311 09', '7-11-25, Ltg Paya Terubong 3, 11500 P. Pinang.', 'Active', ''),
(231, '009261096de8ab902e6366cb6b2ada62', 'Sharon Ong', '', 'Eddie Song', '', '', 'Francis Chan', 'Chan Wei Meng', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-11 16:00:00.0', '700716-07-5049', 'Jul', '013-4318893', 'francis.chanmas@gmail.com', 'PBB', '4331 5589 00', '145 Jalan Permai, 11700 Gelugor Penang.', 'Active', '60%'),
(232, '64a30857d0cfc01938a69688fb7968c9', 'Crystal Ooi', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Jing Leong', 'Su Jing Leong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-12 16:00:00.0', '960517-07-5063', 'May', '011-36118891', 'ahhleong96@gmail.com', '', '', '100, Persiaran Mayang Pasir 4, Bayan Lepas, 11900 Penang.', 'Active', ''),
(233, '0791fe9c4db516952c94c938a4be75d9', 'Choo', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', 'Fish Kong', 'Kong Ming Yee', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-12 16:00:00.0', '920308-08-6528', 'Mar', '016-5191829', 'gic.fishkong@gmail.com', 'PBB', '4586 8465 30', '25, Jalan Besar, Rantau Panjang 34140 Selama, Perak', 'Active', ''),
(234, '63a2880c352d5ecdbe87be559fcaf684', 'Eddie Song', '', '', '', '', 'Yu Nen', 'Choi Yu Nen', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-03-15 16:00:00.0', '880703-07-5075', 'Jul', '016-4880687', 'cynen88@gmail.com', '', '', '76, Jalan Sultan Azlan Shah 11950 Penang.', 'Active', ''),
(235, 'e6c3897b7f3e0f0e62521293e1a80b42', 'Patrick Lim', 'Eddy Wong', 'Jonathan', 'Eddie Song', '', 'Chester Kum', 'Kum Kok Leong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-12 16:00:00.0', '891223-07-5475', 'Dec', '012-2080225', 'Chester89leong@gmail.com', '', '', '146-A Jalan Padang Tembao, 11400 Ayer Itam, Penang.', 'Active', ''),
(236, 'a710b4d8967468109fd9a3b8fab3652c', 'Choo', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', 'Adren Goh', 'Goh Wei Xiang', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-28 16:00:00.0', '901024-07-5639', 'Oct', '016-4699655', 'adrengoh.property@gmail.com', '', '', '', 'Active', ''),
(237, 'b668d014c8510380fdef27c192208446', 'Ivan Tan', 'Patrick Oon', 'Eddie Song', '', '', 'Jason Tan', 'Tan Chin Eng', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-31 16:00:00.0', '790314-07-5099', 'Mar', '010-3082001', 'engtan916@gmail.com', '', '', '13, Seh Tan Court, 10300 Georgetown, Penang.', 'Active', ''),
(238, 'f35c29effed5128a44ece643b6a812db', 'Jovin Pang', 'Dyson Ng', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Allen Kang', 'Kang Lie Ye', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-01 16:00:00.0', '971001-35-5041', 'Oct', '010-5629670', 'kly_allen@yahoo.com\nkly0rallen@gmail.com', 'PBB', '6391-6183-35', '63B-16-12, Jalan Lenggong Jelutong, 11600 Penang.', 'Active', ''),
(239, '3625e4300dbcf1476a4adc444d057e9f', 'Jojo Eng', 'Crystal Ooi', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Joanne Liw', 'Liw Sue Wen', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-01 16:00:00.0', '940527-07-5376', 'May', '016-4272376', 'realjoanneliw@gmail.com', 'MBB', '1.57037E+11', '3-1 Lintang 5 Sunway Wellesley 14000 Bukit Mertajam.', 'Active', ''),
(240, '360d7a7c7842ddf853ec6c3dec4fa4f5', 'Jojo Eng', 'Crystal Ooi', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Andy Ang', 'Ang Choon Kee', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-02 16:00:00.0', '890821-07-5091', 'Aug', '016-4341711', 'andyck_89@hotmail.com\nandyck1989@gmail.com', '', '', '501, Lorong Kenari 4/2A, Taman Kenari, 09000 Kulim, Kedah.', 'Active', ''),
(241, 'b3e38f3d81b10e11cf79a0df27b4e7ed', 'Jovin Pang', 'Dyson Ng', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Li Min', 'Yap Li Min', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-08 16:00:00.0', '010101-07-0286', 'Jan', '010-5434671', 'limin1076@gmail.com', 'PBB', '6456263122', '33-3-2 Tingkat Bukit Jambul 1 Lakeside Towers 11950 Bayan Lepas Pulau Pinang.', 'Active', ''),
(242, '63e3a437bedb60381c8408918c0d9873', 'Melissa Ang', 'Kirby Lee', 'Eddie Song', '', '', 'Grace Chan', 'Chan Moy Chin', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-09 16:00:00.0', '800615-07-5312', 'Jun', '012-5132975', 'grace.chan.mcc@gmail.com', '', '', '9B-18-7 Taman Kheng Tian, Jalan Van Praagh, 11600 Georgetown Penang.', 'Active', ''),
(243, 'd1438261084c11c20492c5b95cea7522', 'Bryan Hng', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Quang Hao', 'Lee Quang Hao', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-09 16:00:00.0', '980609-07-5181', 'Jun', '019-2788021', 'leequanghao21@gmail.com', 'MBB', '1571-4827-9597', '14, Jalan Chee Swee Ee', 'Active', '70%'),
(244, '4e24501c5e98cf06b17687ee6c4370c9', 'Bryan Hng', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Victor Chng', 'Chng Yi Sheng', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-09 16:00:00.0', '980730-07-5167', 'Jul', '018-4001728', 'vsheng771@gmail.com', 'MBB', '1572-4108-0945', '20, Jalan Tunku Kudin, 11700 Gelugor, Penang.', 'Active', '70%'),
(245, '30d4049adc94d71bed769ed7fea42f92', 'Bryan Hng', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Benjamin Chong', 'Chong Kai Gin', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-10 16:00:00.0', '950206-07-5497', 'Feb', '012-4305998', 'kaigin999@gmail.com', 'MBB', '1074-2408-3921', 'B-15-5, Tropicana Bay Residence, Persiaran Bayan Indah', 'Active', '70%'),
(246, '205a8c593f5a8c3409763b256527e85c', 'Benjamin Chong', 'Bryan Hng', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Bernice Lim', 'Lim Sze Er', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-10 16:00:00.0', '960710-07-5700', 'Jul', '011-28412041', 'gekate0710@gmail.com', '', '', 'H-2-2, Lorong Helang, Sungai Dua Gelugor, 11700 Pulau Pinang.', 'Active', '70%'),
(247, '44d15e138b17766b60b31c4fd22b0742', 'Eric Choong', 'Bryan Hng', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Meng Keat', 'Lee Meng Keat', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-11 16:00:00.0', '950512-07-5401', 'May', '016-4976713', 'mengkeat120595@gmail.com', 'HLB', '1415-008-2493', '17-1-4, Taman Batu Bukit Dua, 10470 Tanjung Tokong.', 'Active', '70%'),
(248, '7fe2145da36c71a5fbd1f2dcd84c64d3', 'Choo', 'Caron Wong', 'Aric Teh', 'Patrick Oon', 'Eddie Song', 'Ivan Chong', 'Chong Chun How', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-14 16:00:00.0', '940408-07-5509', 'Apr', '016-5563450', 'cch_940408@hotmail.com\ngic.ivanchong@gmail.com', 'PBB', '6457-302-436', '1-5-1, Tingkat Paya Terubong Indah, 11060 Pulau Pinang.', 'Active', ''),
(249, '42beebadca755ea38ad7f6f595440817', 'Christine Lim', 'Jordan Foo', 'Patrick Oon', 'Eddie Song', '', 'BC Yeap', 'Yeap Bee Chin', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '820101-07-5380', 'Jan', '017-4171619', 'pg.yeapj@gmail.com', 'PBB', '4310-219-407', '267-L, Jalan Burma, 10350 Georgetown Pulau Pinang.', 'Active', ''),
(250, 'c0a32c3241a3b6047f0749230cb36afe', 'Allen Kang', 'Jovin Pang', 'Dyson Ng', 'Carson Chuah', 'Jeffrey Chan', 'Carson Teoh', 'Teoh Peng Tatt', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-16 16:00:00.0', '880819-07-5309', 'Aug', '016-4780661', 'carsonteoh@gmail.com', '', '', '752 MK 16? Jalan Balik Pulau, 11500 Air Itam, Penang.', 'Active', ''),
(251, 'b08ecee761bf76ce702167d781ad2043', 'Patrick Oon', 'Eddie Song', '', '', '', 'Raymond Ong', 'Ong Hock Lye', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-19 16:00:00.0', '790315-07-5365', 'Mar', '018-2872688', 'onghocklye99@gmail.com', '', '', '2-21-9, One World @ The One, Tingkat Mahsuri 2, 11900 Bayan Lepas, Penang', 'Active', '70%'),
(252, '3860cc4295beb56cb0d47ff6d3fab34e', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', 'Bernice Tan', 'Tan Hooi Yin', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-22 16:00:00.0', '990211-07-5084', 'Feb', '012-7211828', 'bernicehy.gic@gmail.com', '', '', '6C-30-03, All Seasons Park, Ibhraya Thean Teik 11500', 'Active', ''),
(253, '7290db07da57fa6acc3f5a3b60fd9111', 'Jeffrey Chan', 'Eddie Song', '', '', '', 'William Lim', 'Lim Jeng Hui', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-28 16:00:00.0', '791114-07-5611', 'Nov', '012-4980082', 'williamlim2829@gmail.com', '', '', 'Blk 3-9-6, Taman Lembah Hijau, Lorong Gangsa, 11600 Penang.', 'Active', ''),
(254, 'fe0c8970817adb1235f948f3d6108606', 'Melissa Ang', 'Kirby Lee', 'Eddie Song', '', '', 'Hooi Chai', 'Chiam Hooi Chai', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-28 16:00:00.0', '900701-07-5317', 'Jul', '016-7905329', 'chai010790@gmail.com', 'PBB', '4684 1800 17', '22, Lorong Kurau 9, Taman Chai Leng 13700 Perai.', 'Active', ''),
(255, '29b68c61e48ee2084feef4e1f4831a13', 'Alvin Teh', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Leon Teh', 'Teh Chun Eong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-28 16:00:00.0', '831018-07-5527', 'Oct', '017-5585086', 'leon.3347@gmail.com', '', '', '1448 MK4, Permatang Pauh 13500 Seberang Perai, Penang.', 'Active', '60%'),
(256, '10bdcbc3cd0446e13879e70947a4168b', 'Vyinnes Ang', 'Alvin Teh', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Dylan Tang', 'Dylan Tang Miang Yen', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '760304-02-5381', 'Mar', '012-5574937', 'dylantmy@gmail.com', 'CIMB', '7055822170', '22-A Lorong Tanjung Aman, Taman Tanjung Aman,12300 BM.', 'Active', '70%'),
(257, '1406f9c9a81bd08f7f7ab7bcbdad1ddc', 'Vyinnes Ang', 'Alvin Teh', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Elaine Chan', 'Elaine Chan Kim Kee', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '741111-02-5226', 'Nov', '016-4809898', 'kkchan.elaine@gmail.com', 'HLB', '5300036786', '501Q, Jalan Tg Bungah, 11200 Penang.', 'Active', '70%'),
(258, '62818c31d772b266f668971bb4f4e1c1', 'Jay Bong', 'Vyinnes Ang', 'Alvin Teh', 'Jackie Wee', 'Kenny Teh', 'Zell Bong', 'Bong Kuan Chean', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '871211-35-5447', 'Dec', '012-5116919', 'zellbong@gmail.com', '', '', '1179, Jalan Empat, Berapt Village, 14000 BM.', 'Active', '60%'),
(259, '65a68e0f993455812946630c71821dfc', 'Vyinnes Ang', 'Alvin Teh', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Jay Bong', 'Bong Kuan Yew', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '820117-07-5019', 'Jan', '012-5619919', 'jaybong9919@gmail.com', '', '', '1179, Jalan Empat, Berapt Village, 14000 BM.', 'Active', '75%'),
(260, '4c3eaff6b7d51126427088efac73b8c0', 'Vyinnes Ang', 'Alvin Teh', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Vannies Teoh', 'Vannies Teoh Pei Ching', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '840902-07-5282', 'Sep', '016-4188810', 'vanniesteoh0902@gmail.com', 'HSBC', '3.74507E+11', '78, Lorong Tanjung Indah 4, Taman Tanjung Indah, 12300 BM', 'Active', '75%'),
(261, '5dd3f580ba72c06950f8c8d66792bc16', 'Vyinnes Ang', 'Alvin Teh', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Quinn Beh', 'Beh Wei Keat', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '900613-07-5077', 'Jun', '012-5213968', 'quinnbeh@gmail.com', 'CIMB', '7022288243', '2659 Jalan Megat Harun, Taman Keenways 14000 BM.', 'Active', '75%'),
(262, 'aa96fcac51524c169f3f16e1ca9e7887', 'Alvin Teh', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'King Kng', 'Kng Lai Ming', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '820331-07-5431', 'Mar', '012-3290857', 'knglaiming@gmail.com', 'HLB', '4300094183', '3-01 Pangsa Bunga Tanjung 4, Jalan Raja Uda, 12300 BM.', 'Active', '70%'),
(263, 'fba36f5349a1107ed3f007868b5851cd', 'Vyinnes Ang', 'Alvin Teh', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Lanny Ang', 'Ang Lean Nee', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '780305-07-5100', 'Mar', '010-4088858', 'lannyang8353@gmail.com', '', '', '812, Jalan Bagan Lallang, 13400 Butterworth, Penang', 'Active', '70%'),
(264, 'b521b99ad0bbfcf94e921e5bf696aeea', 'Vyinnes Ang', 'Alvin Teh', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Jin Chai', 'Chai Kar Jin', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '980720-07-5489', 'Jul', '012-4757058', 'jiajingchai720@gmail.com', 'MBB', '1.57401E+11', 'No. 6121 Permatang Sintok, Penaga 13100 Penang.', 'Active', '60%'),
(265, 'd0b0eef5666c038c5769b4be11f84c1a', 'Dylan Tang', 'Vyinnes Ang', 'Alvin Teh', 'Jackie Wee', 'Kenny Teh', 'Chin Sen', 'Ung Chin Sen', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '920106-07-5433', 'Jan', '013-3613166', 'chinsen16@gmail.com', 'MBB', '1.57055E+11', 'B-22-10, Tropicana Bay Residence, Bayan Baru Indah, 11900 Penang', 'Active', '75%'),
(266, '7f234ef7dfb4a90ee2ee719042ba9c71', 'Alvin Teh', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Chris Lee', 'Lee Xian Hua', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '901231-07-5463', 'Jan', '016-4532818', 'chris.keith5463@gmail.com', 'MBB', '1.07162E+11', '4, Lorong Pauh Jaya 2/6 Taman Pauh Jaya, 13700 Perai Penang.', 'Active', '60%'),
(267, '342f78b1d84bf9681b6e429aba0c36bb', 'Benjamin Chong', 'Bryan Hng', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Jasmine Chen', 'Chen Ling Tze', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-05 16:00:00.0', '960408-07-5398', 'Apr', '016-2214896', 'jasmineCLT0408@gmail.com', 'HLB', '1415-0137-582', '4, Lebuh Bukit Kecil 5, Taman Sri Nibong, 11900 Bayan Lepas, Penang.', 'Active', ''),
(268, '084000cb95380ce0ba85a25d807707d1', 'Ai Pheng', '', 'Eddie Song', '', '', 'Chee Teck', 'Yeoh Chee Teck', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-05 16:00:00.0', '971004-35-5397', 'Oct', '013-4322990', 'CheeTeck1004@hotmail.com\nproperty.yct@gmail.com', 'PBB', '6844 1092 09', '2-02, Jalan Pangsapuri Emas, Taman Emas, 12300 Butterworth.', 'Active', ''),
(269, '69d986ad43c3225046cdd5f95a031349', 'Bryan Hng', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Nelson Lee', 'Lee Wen Khang', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-07 16:00:00.0', '981013-07-5927', 'Oct', '016-5206935', 'nelson3736935@gmail.com', 'CIMB', '7055014147', '8B-18-11, Lorong Semarak Api 3, 11500 Ayer Itam, Penang.', 'Active', '70%'),
(270, '088eeb4887f294d404214e614f0bad08', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', 'Sam Ng', 'Ng Khai Xien', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-12 16:00:00.0', '960207-07-5017', 'Feb', '012-4885343', 'snkx020796@hotmail.com', 'PBB', '4698437223', '60-5-6, Taman Sri Perak, Jalan Slim 11600 Georgetown, Penang.', 'Active', '60%'),
(271, '9ffb9543e36b7b49ea65e152bb5b6522', 'Kelvin Ooi', 'Carson Chuah', 'Jordan Foo', 'Jeffrey Chan', 'Eddie Song', 'Drag Lau', 'Lau Giap Poey', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-12 16:00:00.0', '960516-07-5117', 'May', '016-4263010', 'giappoey@gmail.com', 'CIMB', '7058244314', 'A26-05-11, Lorong Erskine, Taman Kristal, 10470 Tanjung Tokong, Penang.', 'Active', '70%'),
(272, '2b6c45000a8f8f43cd4edd9b66f15794', 'Eric Choong', 'Bryan Hng', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Jennie Tan', 'Tan Boo Guat', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-12 16:00:00.0', '710825-07-5266', 'Aug', '014-5446365', 'jennytanbg@gmail.com', 'HLB', '33751029052', 'Blok B-9-10, Symphony Park, Jalan Lenggong, 11600 Jelutong, Penang.', 'Active', '80%'),
(273, 'f5947116f6f8f4fa0622d9314a230f62', 'Jennie Tan', 'Eric Choong', 'Bryan Hng', 'Jackie Wee', 'Kenny Teh', 'Minleeyl Chen', 'Chen Min Li', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-12 16:00:00.0', '761106-07-5534', 'Nov', '012-9492892', 'min.lichen06@gmail.com', 'PBB', '6452782401', '725D, Jalan Sungai Dua, 11700 Penang.', 'Active', '80%'),
(274, '246f9d302bf03016e2076d1af6b4d519', 'Dylan Lee', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Yoong', 'Ong Yoong', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-13 16:00:00.0', '941125-02-5223', 'Nov', '017-4550464', 'yoong1125@gmail.com', 'HLB', '29350107922', '17, Lintang Gangsa 1, 98 Greenlane, 11600 Penang.', 'Active', '60%'),
(275, '6b7f3d70877a6db10ac329b22db13afc', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '', 'Winnie Ooi', 'Ooi Guit Ngoh', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-15 16:00:00.0', '640902-02-5078', 'Sept', '012-4875275', 'gnooi@hotmail.com \nguitngoh64@gmail.com', 'CIMB', '7020007466', '22A, Lorong Kenari 8, Sungai Ara.', 'Active', '75%'),
(276, '4c2ae09bf6c334b63e4fd4cfe9f8a8d4', 'Ai Pheng', '', 'Eddie Song', '', '', 'Guat Ling', 'Khaw Guat Ling', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-16 16:00:00.0', '990820-07-5282', 'Ogos', '012-4291433', 'guatling123@gmail.com', 'PBB', '6458827209', '42-3-24, Jalan Van Praagh, Desa green Apartment, 11600 Pulau Penang.', 'Active', ''),
(277, '02f2c46db0dfd5e0327ce648c9e47923', 'Ai Pheng', '', 'Eddie Song', '', '', 'Calvin Teoh', 'Teoh Kai Yuan', 'Project Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-16 16:00:00.0', '970429-35-5283', 'April', '011-36091609', 'calvinteoh7@gmail.com', '', '', '11-19-2, Desa Golf, Persiaran Bukit Jambul 2, 11950 Pulau Penang.', 'Active', ''),
(278, 'ddfce2f98b0d44a5865856be09bbaec1', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '', 'Yuki Bong', 'Bong Shu Mei', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-04 16:00:00.0', '970306-02-5428', 'Mar', '016-4021733', 'yukibong9736@gmail.com', 'PBB', '6389-690-925', '4732, Lorong Inang 7, Taman Ria Jaya 08000 Sg Petani, Kedah.', 'Active', '88%'),
(279, '8d223a5365d930d45db03dbc09915d40', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '', 'Desmond Lee', 'Lee Kah Guan', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-04 16:00:00.0', '820805-07-5389', 'Aug', '016-4224825', 'deslee2020@yahoo.com\ndeslee2020@gmail.com', 'MBB', '1570-3659-5998', '9-12-5 Taman Seri Damai, Lebuhraya Batu Lanchang, 11600 Pulau Pinang.', 'Active', '88%'),
(280, '6c8d4d7286e9e0dd4410764ac5df344d', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '', 'Nick Teh', 'Teh Chin Woei', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-05 16:00:00.0', '970524-02-6365', 'May', '016-9017441', 'chinwoei0306@gmail.com', 'Standard Chartered Bank', '8082-9999-2922', '1480 Lorong 27, Taman Ria 08000 Sungai Petani Kedah.', 'Active', '88%'),
(281, '80b061bf64edba474b88be0e68fca563', 'Nick Teh', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Yi Xiang', 'Yap Yi Xiang', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-05 16:00:00.0', '971023-10-6839', 'Oct', '016-4013663', 'yapyixiang1023@gmail.com', 'CIMB', '7065-479-645', '26 Lorong B/8 Taman Intan 08000 Sungai Petani Kedah.', 'Active', '80%'),
(282, 'f79879b0eb60e846a9bd80710f3c6898', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '', 'Dylan Lee', 'Lee Teck Chye', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-07 16:00:00.0', '930807-10-5155', 'Aug', '017-5959424', 'teckchyelee@gmail.com', 'PBB', '461-417-6624', '5-9-14, Lintang Paya Terubong 3', 'Active', '85%'),
(283, '6cf706172fd1b611690b356886d638f8', 'Desmond Lee', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Katherine Ong', 'Ong Gaik Khim', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-07 16:00:00.0', '790314-14-5536', 'Mar', '012-5560448', 'gkkate314@gmail.com', 'CIMB', '7020-729-906', '1-9-2, Lintang Paya Terubong 1, Paya Terubong 11060, P. Pinang.', 'Active', '80%'),
(284, '016a91879c50fd7575a7bf78727114ec', 'Nick Teh', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Bryan Teng', 'Teng Jun Chung', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-07 16:00:00.0', '980912-07-5889', 'Sept', '018-3875119', 'bryanteng98@gmail.com', 'CIMB', '7062-996-950', '11-12-12 Lintang Paya Terubong 4, Desa Permata 11060 Ayer Itam Penang.', 'Active', '80%'),
(285, '1a6645f5d15a3e645a0dbd72f360818b', 'Bryan Hng', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Eric Choong', 'Choong Chee How', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-10 16:00:00.0', '941217-07-5543', 'Dec', '017-2857879', 'ericchoong94@gmail.com', 'HLB', '3830-101-3633', '2C-9-18, Medan Angsana Satu, Ayer Itam, 11500 Pulau Pinang.', 'Active', '85%'),
(286, '4983d1fb14ea48b129a383f9001e7a63', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '-', 'Bryan Hng', 'Hng Chun Siang', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-10 16:00:00.0', '941004-07-5363', 'Oct', '016-5237298', 'chunsiang094@gmail.com', 'HLB', '3835-004-1230', 'No 31, Lebuh Rambai 12, Paya Terubong, 11060 Ayer Itam Pulau Pinang.', 'Active', '90%'),
(287, 'd0ac8e3bab8669dad8889d6aa40f6f61', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '-', 'Zoey Ooi', 'Ooi Li Ling', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-10 16:00:00.0', '850911-02-5892', 'Sept', '012-4522422', 'zoeyooi5796@gmail.com', 'PBB', '4489 3297 21', '79-12B-12B, Penang Times Square, 10150 Penang.', 'Active', '88%'),
(288, '5a1152513b3cbb1ab183dfed7851d7a0', 'Alvin Teh', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Vyinnes Ang', 'Ang Hui Peng', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '761020-07-5074', 'Oct', '016-4009393', 'angvyinn@gmail.com', 'MBB', '1.62312E+12', '22-A Lorong Tanjung Aman, Taman Tanjung Aman,12300 BM.', 'Active', '85 + 3%'),
(289, 'd2ddd7192014ae661a8faa32227b351f', 'Jackie Wee', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '', 'Alvin Teh', 'Teh Joo Tatt', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-06-13 16:00:00.0', '900526-07-5503', 'May', '018-2575717', 'alvinteh5717@gmail.com', '', '', '1448 MK4, Permatang Pauh 13500 Seberang Perai, Penang.', 'Active', '85%'),
(290, '43392017eb07f3c9c2d0bf9f5f357b5b', 'Minleeyl Chen', 'Jennie Tan', 'Eric Choong', 'Bryan Hng', 'Jackie Wee', 'Siew Ling', 'Tan Siew Ling', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-07-15 16:00:00.0', '800128-07-5454', 'Jan', '012-4956287', 'siewling3128@gmail.com', 'PBB', '5007100005', '46-4-21 Desa green van pragh Road 11600 Jelutong.', 'Active', '80%'),
(291, '9c0f5cf3d0667d2de6eca651b1d5cbf7', 'Richard Lim', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Elaine Yeoh', 'Yeoh Lee Yin', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-18 16:00:00.0', '770107-07-5389', 'Jan', '016-4412000', 'elaineyeoh17@yahoo.com\nelaineyeoh17@gmail.com', 'CIMB', '7020761433', 'Blk 11A-5-11, Paya Terubong 2, 11060 Ayer Itam, Penang.', 'Active', '80%'),
(292, 'fbd53785180055e74950bc279b866f71', 'Richard Lim', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'YS Cheah', 'Cheah Yong Sing', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '761017-08-6039', 'Oct', '010-4075682', 'kvncheah@yahoo.com\nkvncheah2@gmail.com', 'CIMB', '7056-0508-76', '1A-19-6 Mira Residence Condo, Lintang Lembah Permai 17, Tg Bungah, Pg.', 'Active', '70%'),
(293, 'ab1bdde00105e555e2f484c3b034a915', 'Joe Ooi', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Meng', 'Khor Ying Meng', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-18 16:00:00.0', '851211-02-5925', 'Dec', '010-4646211', 'meng_design@yahoo.com\nsoka1211@gmail.com', 'CIMB', '7023071148', '4-18-6 Boulevard Solok Thean Teik 11500 Bandar Baru Ayer Itam Pulau Pinang.', 'Active', '70%'),
(294, 'b9e17d426064507624c68141e25027b6', 'Richard Lim', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Gary Oh', 'Oh Chee Seong', 'Senior Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-18 16:00:00.0', '811216-07-5343', 'Dec', '016-4403020', 'ohproperty2u@gmail.com', 'CIMB', '7020-1508-87', '38-3-9 Jalan Pantai Jerjak 1, 11700 Gelugor Penang.', 'Active', '80%'),
(295, '80226560a485b3db7104636655e93d9d', 'Patrick Oon', 'Eddie Song', '', '', '', 'Jane Tan', 'Tan Chia Inn', 'Senior Project Sales', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '800114-07-5234', 'Jan', '016-4395085', 'gicjanetan@gmail.com', 'PBB', '4507-0998-34', '50A-27-5 U Garden, Persiaran Minden 1, 11700 Gelugor Penang.', 'Active', '70%'),
(296, 'f891940dd0798daaf05a9206062b6bb6', 'Jonathan Chng', 'Eddie Song', '', '', '', 'Joe Yeap', 'Yeap Chia Heng', 'Senior Project Sales', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '831011-07-5145', 'Oct', '012-5556956', 'gicjoeyeap@gmail.com', 'PBB', '4608-5387-34', '1-T Jalan Zoo, 11500 Ayer Itam, Pulau Pinang.', 'Active', '70%'),
(297, 'b16041d5ac9d536cf171e376a19ecd40', 'Patrick Oon', 'Eddie Song', '', '', '', 'Kenny Teh', 'Teh Han Hoe', 'Senior Project Sales', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '750308-07-5635', 'Mar', '016-4116363', 'kennytehhh@gmail.com', 'PBB', '3141-5339-09', '75A-15-1 Central Park, Lebuhraya Jelutong, 11600 Penang.', 'Active', '80%'),
(298, 'e13acadff32a82d42b19e8226b003a63', '', 'Eddie Song', '', '', '', 'Sharon Ong', 'Ong Shian Rong', 'Senior Project Sales', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '681111-10-5662', 'Nov', '013-4366670', 'srong04@gmail.com', 'PBB', '6465-7155-13', '7-10-5 Taman Seri Damai, 11600 Penang', 'Active', '75%'),
(299, 'b8d69647e0b37332bec9ea91c13aa89b', 'Patrick Oon', 'Eddie Song', '', '', '', 'Ivan Tan', 'Tan Moh Loon', 'Senior Property Negotiator', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2019-08-05 16:00:00.0', '790726-07-5503', 'Jul', '012-4398359', 'tanmohloon@gmail.com', 'CIMB', '7022-6633-77', '1-10-4, Lengkok Free School 11600 Penang', 'Active', '80%'),
(300, 'c07f42892f7ee5cdb201140fe84487f1', '', 'Eddie Song', '', '', '', 'Ai Pheng', 'Tan Ai Pheng', 'Senior Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '670130-07-5098', 'Jan', '012-5761433', 'aiphengtan123@gmail.com', 'PBB', '6465-7191-05', '42-3-23 Jalan Van Praagh, 11600 Pulau Pinang', 'Active', '70%'),
(301, 'ff62753dbe4f4a2543fe2519a5502fbd', 'Patrick Oon', 'Eddie Song', '', '', '', 'Alison Chng', 'Chng Lay Pheng', 'Senior Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '720623-07-5486', 'Jun', '012-5526793', 'chngalison@gmail.com', 'PBB', '6469-0877-10', '561 Jalan Masjid Negeri, 11600 Greenland, Penang', 'Active', '80%'),
(302, '52e11e75ec312a3aad98e7def5ac9d36', 'Patrick Oon', 'Eddie Song', '', '', '', 'Alvin Tan', 'Tan Beng Hwa', 'Senior Sales Executive', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '701103-07-5833', 'Nov', '012-5325777', 'gicalvin777@gmail.com', 'PBB', '4466-6949-31', '43 Jalan Sungai Ara Satu, Taman Sungai Ara, 11900 Bayan Lepas, Pulau Pinang.', 'Active', '70%'),
(303, '92d5de2ab4e6663f8e12c43edda816a7', 'Eddie Song', '', '', '', '', 'Patrick Oon', 'Oon Liang Siang', 'Sub-sales Division General Manager', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '831030-07-5415', 'Oct', '016-4415503', 'gicpatrick328@gmail.com', 'PBB', '3146-2719-33', '33-1-12 Taman Pekaka, Sungai Dua, 11700 Gelugor, Pulau Pinang', 'Active', '80%'),
(304, '25afe8e85c61914754d64eeb4ffc71ce', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '', '', 'Jackie Wee', 'Wee Guan Seng', 'Subsales Manager', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-31 16:00:00.0', '800912-01-6589', 'Sept', '017-4467796', 'jackiewee57@gmail.com', 'HLB', '15850147761', '79-12B-12B, Birch Regency, 10150 Georgetown.', 'Active', '92%'),
(305, 'ba516d68cb48a7d4b27698098b34a70e', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Kenny Ooi', 'Ooi Zhi Hong', 'Team Leader', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '910411-07-5369', 'Apr', '016-4832127', 'kennyooi91@outlook.com\nkennyooipopular@gmail.com', 'PBB', '4522716434', 'Plot 6 N/T 290 Blok H MK 12, Jalan Batu Maung 11960 Bayan Lepas, Penang.', 'Active', '80%'),
(306, 'b6244418012b346de9af42541f5c25af', 'Johnson Yeong', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Raven Saw', 'Saw Wai Lee', 'Team Leader', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-17 16:00:00.0', '781008-07-5491', 'Oct', '016-4225095', 'mansionraven@hotmail.com\nGicmansionsaw@gmail.com', 'MBB', '1.07126E+11', '46-11-9, Jalan Besi Green Lane Height 11600 Penang.', 'Active', '70%'),
(307, '8072e5cdfb0a86e1d953bfd5ef8312a8', 'Kenny Ooi', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Cris Ng', 'Ng Weng Sheng', 'Team Leader', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '900401-02-5051', 'Apr', '016-4488070', 'crisng070@gmail.com', 'MBB', '1.57438E+11', 'No. 30 Lorong 13, Taman Gurun Kedah 08300 Gurun Kedah.', 'Active', '75%'),
(308, '7e74f1ba6d0cd4a0812e5bf5865f9885', 'Kenny Ooi', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Mint Teow', 'Teow Hui Ming', 'Team Leader', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '890327-02-5574', 'Mar', '017-4666558', 'minteow@gmail.com', 'PBB', '4644521432', '9-17-3, The Waterfront Condo, 9 Persiaran Tg Bungah, 11200 Pulau Pinang.', 'Active', '65%'),
(309, '0b3caa5bc6f5c6e1175af1f16272e7ad', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', '-', '-', 'Max Tay', 'Tay Poh Leng', 'Team Manager', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-20 16:00:00.0', '850304-07-5353', 'Mar', '016-4962304', 'maxtay1208@yahoo.com\naarontaypl2000@gmail.com', 'PBB', '3218-9336-26', '264-6-4 Desa Jelutong West Jelutong 11600 Penang.', 'Active', '93%'),
(310, 'd2db196084cd3f7883f84225fad229fc', 'Jen Tay', 'Max Tay', 'Kenny Teh', 'Patrick Oon', 'Eddie Song', 'Joe Ooi', 'Ooi Yeen Chao', 'Team Manager', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '2020-05-18 16:00:00.0', '800530-07-5399', 'May', '016-4182713', 'yeenchao@yahoo.com\nJoeooi8035@gmail.com', 'Alliance Bank', '0702-0002-0661-361', 'A-2-3A, One Imperial, Lintang Sungai Ara 14, 11900 Bayan Lepas, Penang.', 'Active', '80%'),
(311, '4c34653a8266a7772fd93d7945699d34', '', '', '', '', '', 'N/A 1', 'N/A 1', '', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '', '', '', '', '', '', '', 'Active', ''),
(312, 'c113d67b3415b2dcbd3696a6fd0c6031', '', '', '', '', '', 'N/A 2', 'N/A 2', '', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '', '', '', '', '', '', '', 'Active', ''),
(313, '2ae63f138a304de434153bc4af5b6708', '', '', '', '', '', 'N/A 3', 'N/A 3', '', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '', '', '', '', '', '', '', 'Active', ''),
(314, '41c1bfca0d2f9ff0e22e307560515e81', '', '', '', '', '', 'N/A 4', 'N/A 4', '', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '', '', '', '', '', '', '', 'Active', ''),
(315, 'c1e152a1e17dc4587d80425d02e62ffa', '', '', '', '', '', 'N/A 5', 'N/A 5', '', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '', '', '', '', '', '', '', 'Active', ''),
(316, '42152cbe237dd9b726747fbe00c295b9', '', '', '', '', '', 'N/A 6', 'N/A 6', '', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '', '', '', '', '', '', '', 'Active', ''),
(317, '9dd2c7ada345bb7326fdcdf667fc07c9', '', '', '', '', '', 'N/A 7', 'N/A 7', '', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '', '', '', '', '', '', '', 'Active', ''),
(318, '1debcf1fdbc4962963a114669f63d82a', '', '', '', '', '', 'N/A 8', 'N/A 8', '', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '', '', '', '', '', '', '', 'Active', ''),
(319, '2e02f8b818164194afc17cad9b619bb2', '', '', '', '', '', 'N/A 9', 'N/A 9', '', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '', '', '', '', '', '', '', 'Active', ''),
(320, 'b515a68713acfdbb8f518d04afab6871', '', '', '', '', '', 'N/A 10', 'N/A 10', '', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '', '', '', '', '', '', '', 'Active', ''),
(321, '07dad2f3c8bf72bd2d3d351ef5067aeb', '', '', '', '', '', 'N/A 11', 'N/A 11', '', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '', '', '', '', '', '', '', 'Active', ''),
(322, '9106ebc0045f4b0535dbad3f82635103', '', '', '', '', '', 'N/A 12', 'N/A 12', '', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '', '', '', '', '', '', '', 'Active', ''),
(323, '7d23fe4ffd93cc5c195bbb22a2b3d6b4', '', '', '', '', '', 'N/A 13', 'N/A 13', '', 3, 1, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '0000-00-00 00:00:00.0', '', '', '', '', '', '', '', 'Active', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advance_slip`
--
ALTER TABLE `advance_slip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_list`
--
ALTER TABLE `bank_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commission`
--
ALTER TABLE `commission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `edit_history`
--
ALTER TABLE `edit_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_credit`
--
ALTER TABLE `invoice_credit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_multi`
--
ALTER TABLE `invoice_multi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_other`
--
ALTER TABLE `invoice_other`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_proforma`
--
ALTER TABLE `invoice_proforma`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `issue_payroll`
--
ALTER TABLE `issue_payroll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_status`
--
ALTER TABLE `loan_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_list`
--
ALTER TABLE `payment_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `referral_name` (`referral_name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `full_name` (`full_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `advance_slip`
--
ALTER TABLE `advance_slip`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bank_list`
--
ALTER TABLE `bank_list`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `commission`
--
ALTER TABLE `commission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `edit_history`
--
ALTER TABLE `edit_history`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_credit`
--
ALTER TABLE `invoice_credit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_multi`
--
ALTER TABLE `invoice_multi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_other`
--
ALTER TABLE `invoice_other`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_proforma`
--
ALTER TABLE `invoice_proforma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `issue_payroll`
--
ALTER TABLE `issue_payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loan_status`
--
ALTER TABLE `loan_status`
  MODIFY `id` bigint(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=293;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=324;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
