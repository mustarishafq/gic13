<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$projectList = getProject($conn, "WHERE display = 'Yes' ");

 ?>
 <!--
 <style media="screen">
 .box-inline{
   position: relative;
   display: inline-block;
 }
   .circle-box{
     position: relative;
     border: 2px solid black;
     box-shadow: 0 0 0 3px red;
     width: 200px;
     height: 200px;
     border-radius: 50%;
     text-align: center;
     padding: 15px;
     margin-right: 20px;
     margin-bottom: 20px;
     font-size: 40px;
   }
   .big-text{
     font-size: 100%;
     margin-top: 48px;
   }
   .small-text{
     font-size: 40%;
     margin-top: -20px;
   }
   .center{
     text-align: center;
   }
   .left{
     text-align: left;
   }
   .booking-btn{
     margin-left: -15px;
     margin-bottom: 20px;
     background-color: maroon;
     width: 180px;
     height: 40px;
     color: white;
     font-weight: bold;
     border-radius: 5px;
     border-color: transparent;
   }
   .small-big-text{
     font-size: 40%;
     margin-top: -45px;
     margin-bottom: 20px;
   }
   .unit-sold{
     margin-bottom: 20px;
   }
   .new-project{
     /* margin-left: -15px; */
     margin-top: 25px;
     background-color: maroon;
     width: 150px;
     height: 40px;
     color: white;
     font-weight: bold;
     border-radius: 5px;
     border-color: transparent;
   }
   button{
     cursor: pointer;
   }
 </style>-->
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
  <?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Project | GIC" />
    <title>Project | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
  <body class="body">
    <?php
    if ($_SESSION['usertype_level'] == 1) {
      include 'admin1Header.php';
    }else {
      include 'admin2Header.php';
    }
     ?>
    <div class="yellow-body padding-from-menu same-padding center">
      <h1 class="h1-title h1-before-border shipping-h1 left">Project</h1>
            <button onclick="location.href='adminAddNewProject.php'" class="h1-title h1-before-border shipping-h1 right new-project red-btn" type="button" name="button">NEW PROJECT</button>
      <div class="short-red-border"></div>
      <div class="circle-big-width overflow">

      <?php
      if ($projectList) {
        for ($i=0; $i <count($projectList) ; $i++) {
          $totalUnitSold = 0;
          $totalUnit = 0;
          $UnitSoldDetails = getLoanStatus($conn, "WHERE project_name =? AND case_status = 'COMPLETED' AND cancelled_booking != 'YES'",array("project_name"),array($projectList[$i]->getProjectName()), "s");
          $unitDetails = getLoanStatus($conn, "WHERE project_name =? AND cancelled_booking != 'YES'",array("project_name"),array($projectList[$i]->getProjectName()), "s");
          if ($UnitSoldDetails) {
            $totalUnitSold = count($UnitSoldDetails);
          }
          if ($unitDetails) {
            $totalUnit = count($unitDetails);
          }
          ?>
          <div class="box-inline">
              <div class="square2">
                <div class="circle-box content2">
                    <p class="big-text"><?php echo $totalUnitSold;?></p>
                    <p class="small-big-text">/<?php echo $totalUnit ?></p>
                    <p class="small-text unit-sold">Units Sold</p>
                    <p class="small-text project-name"><?php echo $projectList[$i]->getProjectName() ?></p>
                </div>
              </div>
            <form class="" action="adminAddNewProduct.php" method="post">
              <button class="five-red-btn-div ow-width100 clean circle-bottom-btn" type="submit" name="project_name" value="<?php echo $projectList[$i]->getProjectName() ?>">BOOKING</button>
            </form>
          </div>
          <?php
        }
      }
       ?>

    </div>
</div>
  <?php include 'js.php'; ?>
  </body>
</html>
