<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/PaymentMethod.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Branch.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$adminDetails = getUser($conn, "WHERE id = ? ",array("id"),array($_POST['id']), "s");
$bankNameList = getBankName($conn);
$branchList = getBranch($conn);
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Edit | GIC" />
    <title>Edit | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  a{
    color: red;
  }
</style>
<body class="body">
<?php  include 'admin1Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
  <form method="POST" action="utilities/editAdminFunction.php" enctype="multipart/form-data">

	<h1 class="details-h1" onclick="goBack()"> <!-- instead use goback() -->
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back3.png" class="back-btn2 hover1b" alt="back" title="back">
        	Name : <?php echo $adminDetails[0]->getUsername(); ?>
        </a>
    </h1>

    <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm"></div>
	<div class="big-dual-input-container">

    <div class="dual-input-div">
      <p>Full Name <a>*</a></p>
      <input class="dual-input clean" type="text" placeholder="Full Name" id="full_name" name="full_name" value="<?php echo $adminDetails[0]->getFullName() ?>" >
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Username <a>*</a></p>
      <input class="dual-input clean" type="text" placeholder=" Username" id="username" name="username" value="<?php echo $adminDetails[0]->getUsername() ?>" >
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>IC No. <a>*</a></p>
      <input class="dual-input clean" type="text" placeholder=" IC No." id="ic_no" name="ic_no" value="<?php echo $adminDetails[0]->getIcNo() ?>" >
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Contact <a>*</a></p>
      <input class="dual-input clean" type="text" placeholder=" Contact No." id="contact" name="contact" value="<?php echo $adminDetails[0]->getPhoneNo() ?>" >
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>E-mail <a>*</a></p>
      <input class="dual-input clean" type="text" placeholder=" E-mail" id="email" name="email" value="<?php echo $adminDetails[0]->getEmail() ?>" >
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Birth Month <a>*</a></p>
      <input class="dual-input clean" type="text" placeholder="Birth Month" id="birth_month" name="birth_month" value="<?php echo $adminDetails[0]->getBirthday() ?>">
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Address</p>
      <input class="dual-input clean" type="text" placeholder="Address" id="address" name="address" value="<?php echo $adminDetails[0]->getAddress() ?>">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Status</p>
      <select class="dual-input clean" name="status">
          <?php if ($adminDetails[0]->getStatus()) {
            ?><option class="dual-input clean" value="<?php echo $adminDetails[0]->getStatus() ?>"><?php echo $adminDetails[0]->getStatus() ?></option><?php
          } ?>
          <option value="">Select a Option</option>
          <option value="Active">Active</option>
          <option value="Resign">Resign</option>
      </select>
    </div>
    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Company Branch</p>
      <select class="dual-input clean" name="company_branch">
        <?php if ($adminDetails[0]->getBranchType()) {
          $adminBranch = getBranch($conn,"WHERE branch_type =?",array("branch_type"),array($adminDetails[0]->getBranchType()), "s");
          ?><option value="<?php echo $adminBranch[0]->getBranchType() ?>"><?php echo $adminBranch[0]->getBranchName() ?></option> <?php
          if ($branchList) {
            for ($i=0; $i < count($branchList); $i++) {
            ?><option value="<?php echo $branchList[$i]->getBranchType() ?>"><?php echo $branchList[$i]->getBranchName() ?></option> <?php
            }
          }
        }else {
          ?><option value="">Select a branch</option><?php
          if ($branchList) {
            for ($i=0; $i < count($branchList); $i++) {
            ?><option value="<?php echo $branchList[$i]->getBranchType() ?>"><?php echo $branchList[$i]->getBranchName() ?></option> <?php
            }
          }
        } ?>


      </select>
    </div>

    <div class="clear"></div>
    <div class="tempo-two-input-clear"></div>
    <input type="hidden" name="id" value="<?php echo $adminDetails[0]->getId() ?>">
    <div class="three-btn-container extra-margin-top">
        <!-- <button class="shipout-btn-a red-button three-btn-a" type="submit" id = "deleteProduct" name = "deleteProduct" ><b>DELETE</b></a></button> -->
        <button class="shipout-btn-a black-button three-btn-a" type="submit" id = "editSubmit" name = "editSubmit" ><b>CONFIRM</b></a></button>
    </div>

</div>
</form>
</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
function goBack() {
  window.history.back();
}
</script>
<script type="text/javascript">
  $(document).ready( function(){
    $("#selectAmountType").change( function(){
      var type = $(this).val();

      if (type == '%') {
        $("#loanAmountName").text("Loan Amount (%)");
        $('input[name="loan_amount"]').prop('readonly', false);
      }else if (type == 'RM') {
        $("#loanAmountName").text("Loan Amount (RM)");
        $('input[name="loan_amount"]').prop('readonly', false);
      }
    });
  });
</script>

</body>
</html>
