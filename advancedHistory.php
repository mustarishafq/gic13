<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/timezone.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $loanUidRows = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$loanUidRows = getAdvancedSlip($conn, "WHERE status != 'PENDING' and branch_type =? ",array("branch_type"),array($_SESSION['branch_type']), "s");
$invoiceDetails = getInvoice($conn);
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Advance History | GIC" />
    <title>Advance History | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
input#checkboxToDisplayTableMulti,#HistoryBtn {
    width :25px;
    height :25px;
    vertical-align: middle;
    outline: 2px solid maroon;
    outline-offset: -2px;
}
input#checkboxClick{
  width :20px;
  height :20px;
  outline: 2px solid maroon;
  outline-offset: -2px;
}
.th:hover, .th0:hover{
  background-color: maroon;
}
.th.headerSortUp , .th0.headerSortUp{
  background-color: black;
}
.th.headerSortDown , .th0.headerSortDown{
  background-color: black;
}
</style>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
  <?php if ($loanUidRows) {
    ?><h1 class="h1-title h1-before-border shipping-h1">Advance History </h1>
      <div class="short-red-border"></div>
      <a href="excel/advancedHistoryExport.php"><button class="exportBtn" type="button" name="button">Export</button></a>
      <br>
        <form action="advancedSlipHistory.php" method="POST">

      <!-- This is a filter for the table result -->


      <!-- <select class="filter-select clean">
      	<option class="filter-option">Latest Shipping</option>
          <option class="filter-option">Oldest Shipping</option>
      </select> -->

      <!-- End of Filter -->
      <div class="clear"></div>

      <div class="width100 shipping-div2">
          <?php $conn = connDB();?>
              <table id="singleAdvancedTable" class="shipping-table">
                  <thead>
                      <tr>
                          <th class="th">NO.</th>
                          <th class="th">PROJECT NAME</th>
                          <th class="th">UNIT NO.</th>
                          <th class="th"><?php echo wordwrap("AGENT NAME",10,"</br>\n");?></th>
                          <th class="th">DATE</th>
                          <th class="th">TIME</th>
                          <th class="th">ACTION</th>
                          <th class="th">DATE MODIFIED</th>

                          <!-- <th>INVOICE</th> -->
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                      // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                      // {
                          $orderDetails = getAdvancedSlip($conn, "WHERE status != 'PENDING' and branch_type = ?",array("branch_type"),array($_SESSION['branch_type']), "s");
                          if($orderDetails != null)
                          {
                              for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                              {?>
                              <tr>
                                  <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                  <td class="td"><?php echo $cntAA + 1;?></td>
                                  <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                                  <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                  <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getAgent(),15,"</br>\n");?></td>
                                  <td class="td"><?php echo date('d/m/Y', strtotime($orderDetails[$cntAA]->getDateCreated())) ?></td>
                                  <td class="td"><?php echo date('H:i a', strtotime($orderDetails[$cntAA]->getDateCreated())) ?></td>

                                  <td  class="td">

                                        <a><button class="clean edit-anc-btn hover1 red-link"  type="submit" name="claim_id" value="<?php echo $orderDetails[$cntAA]->getClaimId() ?>">View Advanced Slip
                                          </button></a>
                                      </form>
                                  </td>
                                  <td class="td">
                                    <form class="" action="historyAdvance.php" method="post">
                                      <button class="clean edit-anc-btn hover1" type="submit" name="advance_id" value="<?php echo $orderDetails[$cntAA]->getAdvancedId() ?>">
                                        <a style="color: blue"><?php echo date('d/m/Y',strtotime($orderDetails[$cntAA]->getDateCreated())) ?></a>
                                      </button>
                                    </form>
                                  </td>

                              </tr>
                              <?php
                              }
                          }else {
                            ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="8">
                                No Advance History Found.
                              </td> <?php
                          }
                      //}
                      ?>
                  </tbody>
              </table>

              <table id="MultipleAdvancedTable" style="display: none" class="shipping-table">
                  <thead>
                      <tr>
                          <th class="th">SELECT</th>
                          <th class="th">NO.</th>
                          <th class="th">PROJECT NAME</th>
                          <th class="th">UNIT NO.</th>
                          <th class="th"><?php echo wordwrap("AGENT NAME",10,"</br>\n");?></th>
                          <th class="th">DATE</th>
                          <th class="th">TIME</th>

                          <!-- <th>INVOICE</th> -->
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                      // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                      // {
                          $orderDetails = getAdvancedSlip($conn, "WHERE status = 'PENDING' and branch_type = ?",array("branch_type"), array($_SESSION['branch_type']), "s");
                          if($orderDetails != null)
                          {
                              for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                              {?>
                              <tr>
                                  <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                  <td class="td"><input id="checkboxClick" type="checkbox" name="selectedAdvanceslip[]" value="<?php echo $orderDetails[$cntAA]->getID();?>"> </td>
                                  <td class="td"><?php echo $cntAA + 1;?></td>
                                  <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                                  <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                  <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getAgent(),15,"</br>\n");?></td>
                                  <td class="td"><?php echo date('d/m/Y', strtotime($orderDetails[$cntAA]->getDateCreated())) ?></td>
                                  <td class="td"><?php echo date('H:i a', strtotime($orderDetails[$cntAA]->getDateCreated())) ?></td>

                              </tr>
                              <?php
                              }
                          }
                      //}
                      ?>
                  </tbody>
              </table><br>


      </div>

      <!-- <div class="three-btn-container">
      <!-- <a href="adminRestoreProduct.php" class="add-a"><button name="restore_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side two-button-side1">Restore</button></a> -->
        <!-- <a href="adminAddNewProduct.php" class="add-a"><button name="add_new_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side  two-button-side2">Add</button></a> -->
      <!-- </div> -->
      <?php $conn->close();?><?php
  }else {
    ?><h2 style="text-align: center" class="loan-h2">No Loan Case Status Completed Found.</h2>
    <h2 style="text-align: center" class="loan-h2">Make Sure To Send Advance Slip To Agent.</h2> <?php
  } ?>


</div>



<?php unset($_SESSION['loan_uid']) ?>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Send Slip To Agent.";
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        elseif($_GET['type'] == 3)
        {
          $messageType = "Advance Slip Not Selected.";
        }
        elseif($_GET['type'] == 4)
        {
          $messageType = "Successfully Updated Advance Slip Details";
        }
        elseif($_GET['type'] == 7)
        {
          $messageType = "Agent Name Selected Are Not Same";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(document).ready( function(){
  // $("#checkboxToDisplayTableMulti").remove();
  // $("#addBack").append('<input id="checkboxToDisplayTableMulti" type="checkbox" name="headerCheck">');
  $("#checkboxToDisplayTableMulti").on('change', function(){
    if ($("#checkboxToDisplayTableMulti").is(':checked')) {
      $("#HistoryBtn").prop('checked',false);
      $(".advancedIssue").show();
      $("#History").hide();
      $("#singleAdvancedTable").hide();
      $("#MultipleAdvancedTable").show();
    }
    if ($("#checkboxToDisplayTableMulti").is(':not(:checked)')) {
      $("#History").hide();
      $(".advancedIssue").hide();
      $("#singleAdvancedTable").show();
      $("#MultipleAdvancedTable").hide();
    }
  });
});

</script>
<script>
  $(function(){
    $('#singleAdvancedTable,#MultipleAdvancedTable').tablesorter( {dateFormat: 'pt'} );
  });
</script>

</body>
</html>
