<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$invoiceHistory = getInvoice($conn, "WHERE loan_uid = ?", array("loan_uid"), array($_POST['loan_uid']), "s");

$bankName = getBankName($conn);

$loanDetails = getLoanStatus($conn, "WHERE loan_uid=?",array("loan_uid"),array($_POST['loan_uid']), "s");
$asd = $loanDetails[0]->getProjectName();
$totalDeveloperComm = $loanDetails[0]->getTotalDeveloperComm();
$unitNo = $loanDetails[0]->getUnitNo();
$agentComm = $loanDetails[0]->getAgentComm();
$agentAdvanced = 2000;
$ulOverride = $loanDetails[0]->getUlOverride();
$uulOverride = $loanDetails[0]->getUulOverride();
$plOverride = $loanDetails[0]->getPlOverride();
$hosOverride = $loanDetails[0]->getHosOverride();
$listerOverride = $loanDetails[0]->getListerOverride();
$totalBalUnclaimAmt = $loanDetails[0]->getTotalBalUnclaimAmt();

// $unclaimedAmount = $totalBalUnclaimAmt - ($agentComm - $agentAdvanced) - $ulOverride - $uulOverride - $plOverride - $hosOverride - $listerOverride;

// echo $asd;

$proDetails = getProject($conn, "WHERE project_name=?",array("project_name"),array($asd), "s");
$asdsasd = $proDetails[0]->getAddProjectPpl();
$claimStatus = $proDetails[0]->getProjectClaims();

$amountDivide = $totalDeveloperComm / $claimStatus;
// echo $asdsasd;

$projectList = getProject($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Edit Invoice | GIC" />
    <title>Edit Invoice | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
  <h1 class="h1-title h1-before-border shipping-h1 username"  onclick="goBack()">
    	<a  class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back3.png" class="back-btn2 hover1b" alt="back" title="back">
        	Invoice:   <?php
  // $conn = connDB();

    echo $asdsasd;

  // $conn->close();
  ?>
        </a>
    </h1>

<div class="clear"></div>
<h3>Unclaimed Balanced : RM<?php echo $totalBalUnclaimAmt  ?> </h3>



<form  action="utilities/addNewInvoiceFunction.php" method="POST" enctype="multipart/form-data">
<!-- <table class="edit-profile-table"> -->
  <div class="three-input-div dual-input-div">
  <p>Project</p>
    <input class="dual-input clean" type="text" readonly name="product_name" value="<?php echo $asd ?>">
  </div>
  <div class="three-input-div dual-input-div second-three-input">
    <p>To</p>
    <input required class="dual-input clean" type="text" value="<?php echo $asdsasd ?>" placeholder="Auto Generated" id="to_who" name="to_who" readonly>
  </div>


  <div class="three-input-div dual-input-div">
    <p>Status (No. of Claims)</p>
    <select class="dual-input clean" name="status_of_claims" readonly>
      <?php
       if (!$loanDetails[0]->getRequestDate1() && $proDetails[0]->getProjectClaims() >= 1 ) {
        ?><option value="1">1</option> <?php
      }elseif (!$loanDetails[0]->getRequestDate2() && $proDetails[0]->getProjectClaims() >= 2 ) {
        ?><option value="2">2</option> <?php
      }elseif (!$loanDetails[0]->getRequestDate3() && $proDetails[0]->getProjectClaims() >= 3 ) {
        ?><option value="3">3</option> <?php
      }elseif (!$loanDetails[0]->getRequestDate4() && $proDetails[0]->getProjectClaims() >= 4 ) {
        ?><option value="4">4</option> <?php
      }elseif (!$loanDetails[0]->getRequestDate5() && $proDetails[0]->getProjectClaims() >= 5 ) {
        ?><option value="5">5</option> <?php
      }else {
        ?><option value="exceed">Exceed Limit</option> <?php
      } ?>

    </select>

  </div>

  <div class="tempo-two-input-clear"></div>

  <div class="three-input-div dual-input-div">
    <p>Date</p>
    <input oninput="this.value = this.value.toUpperCase()" class="dual-input clean" type="date" id="date_of_claims" name="date_of_claims" value="<?php echo date('Y-m-d') ?>" required>
  </div>


  <div class="three-input-div dual-input-div second-three-input">
    <p>Invoice</p>
    <select class="dual-input clean" name="invoice_name" required>
      <option value="">Please Select an Option</option>
      <option value="Proforma">Proforma</option>
      <option value="Invoice">Invoice</option>
      <!-- <option value="Credit Note">Credit Note</option> -->
    </select>
  </div>

  <!-- <div class="three-input-div dual-input-div">
    <p>Invoice Type</p>
    <select class="dual-input clean" name="invoice_type" >
      <option value="None">None</option>
    </select>
  </div> -->
	<!-- <div class="tempo-two-input-clear"></div> -->



  <!-- <div class="three-input-div dual-input-div">
    <p>Item</p>
    <input required class="dual-input clean" type="text" placeholder="Item" id="product_name" name="item">
  </div> -->
  <div class="three-input-div dual-input-div">
    <p>Unit</p>
    <input class="dual-input clean" type="text" placeholder="Unit" id="unit" name="unit" value="<?php echo $unitNo ?>" readonly>
  </div>

  <div class="tempo-two-input-clear"></div>

  <div class="three-input-div dual-input-div">
    <p>Amount (RM)</p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="product_name" name="amount" value="<?php echo $amountDivide ?>">
  </div>
  <div class="tempo-two-input-clear"></div>

  <div class="dual-input-div">
    <p>Include Service Tax (6%)</p>
    <input style="cursor: pointer" required type="radio" class="yes" value = "YES" name="charges" ><a class="yes-a">Yes</a>
    <input style="cursor: pointer" required type="radio" class="no" value = "NO" name="charges" ><a class="no-a">No</a>
  </div>

  <div class="tempo-two-input-clear"></div>

  <input type="hidden" name="purchaser_name" value="<?php echo $loanDetails[0]->getPurchaserName() ?>">
  <input type="hidden" id="loanUid" name="loan_uid" value="<?php echo $_POST['loan_uid'] ?>">
  <input type="hidden" name="id" value="<?php echo $loanDetails[0]->getID() ?>">


  <button input type="submit" name="upload" value="Upload" class="confirm-btn text-center white-text clean black-button">Confirm</button>

</form>

<br>
<?php if ($invoiceHistory) { ?>


<h2>Invoice History</h2>

<table class="shipping-table">
<tr>
    <th>No.</th>
    <th>Name</th>
    <th>Item</th>
    <th>Remark</th>
    <th>Amount</th>
    <th>Date Issue</th>
    <th>Invoice</th>
</tr>
<?php for ($cnt=0; $cnt <count($invoiceHistory) ; $cnt++) { ?>
<tr>
<?php
?>  <td class="td"><?php echo $cnt+1 ?></td>
    <td class="td"><?php echo $invoiceHistory[$cnt]->getPurchaserName() ?></td>
    <td class="td"><?php echo $invoiceHistory[$cnt]->getItem() ?></td>
    <td class="td"><?php echo $invoiceHistory[$cnt]->getRemark() ?></td>
    <td class="td"><?php echo $invoiceHistory[$cnt]->getAmount() ?></td>
    <td class="td"><?php echo date('d-m-Y', strtotime($invoiceHistory[$cnt]->getDateCreated())) ?></td>
    <td class="td">  <form action="invoice.php" method="POST">
          <button class="clean edit-anc-btn hover1" type="submit" name="invoice" value="<?php echo $invoiceHistory[$cnt]->getId();?>">
              <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product">
              <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product">
          </button>
      </form></td>
</tr>
<?php
 } ?>
</table>

<?php } ?>
</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
<script src="jsPhp/editInvoice.js" charset="utf-8"></script>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Product Added Successfully";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "There is an error to add the new product";
        }

        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>
