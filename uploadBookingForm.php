<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$projectName = "";
// $sql = "select pdf from loan_status";
// $result = mysqli_query($conn, $sql);
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Booking Form Upload | GIC" />
    <title>Booking Form Upload | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'admin1Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<style>
.th:hover{
  background-color: maroon;
}
.th.headerSortUp , .th0.headerSortUp{
  background-color: black;
}
.th.headerSortDown , .th0.headerSortDown{
  background-color: black;
}
</style>
<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Booking Form Upload</h1>

    <div class="short-red-border"></div>

    <div class="section-divider width100 overflow">
      <?php $projectDetails = getProject($conn, "WHERE display = 'Yes'"); ?>

        <select id="sel_id" class="clean-select">
          <option value="">Select a project</option>
          <?php if ($projectDetails) {
            for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
              if ($projectDetails[$cnt]->getProjectName() != $types) {
                ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
                }
                }
                ?><option value="ALL">SHOW ALL</option><?php
              } ?>
        <!-- <option value="-1">Select</option>
        <option value="VIDA">kasper </option>
        <option value="VV">adad </option> -->
        <!-- <option value="14">3204 </option> -->
        </select>
        <input class="clean search-btn" type="text" name="" value="" placeholder="Search.." id="searchInput">
    </div>

    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->
    <!-- End of Filter -->


    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table id="myTable" class="shipping-table pointer-th">
                <thead>
                    <tr>
                        <th class="th">NO. <img src="img/sort.png" class="sort"></th>
                        <th class="th">PROJECT NAME <img src="img/sort.png" class="sort"></th>
                        <th class="th">UNIT NO. <img src="img/sort.png" class="sort"></th>
                        <th class="th">FILENAME <img src="img/sort.png" class="sort"></th>
                        <th class="th">VIEW/DOWNLOAD <img src="img/sort.png" class="sort"></th>
                        <th class="th">ACTION <img src="img/sort.png" class="sort"></th>
                    </tr>
                </thead>
                <tbody id="myFilter">
                    <?php
                    // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                    // {
                        $orderDetails = getLoanStatus($conn, "WHERE display = 'Yes' and branch_type = ?".$projectName,array("branch_type"),array($_SESSION['branch_type']), "s");
                        // $orderDetails = getLoanStatus($conn);
                        if($orderDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                            {?>
                            <tr>
                                <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                <td class="td"><?php echo $cntAA + 1?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                  <?php if (!$orderDetails[$cntAA]->getPdf()) {
                                    ?><td class="td">No File Upload Yet.</td> <?php
                                  }else {
                                  ?><td class="td"><?php echo $orderDetails[$cntAA]->getPdf();?></td> <?php
                                   } ?>

                                <?php $loanUid = $orderDetails[$cntAA]->getLoanUid() ?>
                                <?php $sql = "select pdf from loan_status WHERE loan_uid = '$loanUid'";
                                $result = mysqli_query($conn, $sql); ?>
                                <?php while($row = mysqli_fetch_array($result)) {  ?>
                                  <?php if ($row['pdf'] != null) {
                                    ?><td class="td"><a href="uploads/<?php echo $row['pdf']; ?>" download>Download</td><?php
                                  }else {
                                    ?><td class="td">No File Upload Yet.</td> <?php
                                  } ?>

                                <?php } ?>

                                <td class="td">
                                    <form action="uploadBookingFormNew.php" method="POST">
                                        <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">
                                            <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product">
                                            <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product">
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            <?php
                            }
                        }else {
                          ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="13"> No Loan Booking Form. </td> <?php
                        }
                    //}
                    ?>
                </tbody>
            </table><br>


    </div>


    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Upload Booking Form.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Error Uploading Booking Form.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully Add New Booking";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Successfully Updated Profile !";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong Current Password Entered !";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Successfully Issue Payroll !";
        }
        // echo '<script>
        // $("#audio")[0].play();
        // putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        echo '<script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
<script>
  $(function(){
    $("#myTable").tablesorter( {dateFormat: 'pt'} );
  });
</script>
</html>
