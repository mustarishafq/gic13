<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:type" content="website" />
<meta property="og:image" content="https://vidatechft.com/hosting-picture/fb-meta.jpg" />
<meta name="author" content="GIC">
<meta property="og:description" content="Property Agency Of The Year in Asean Outstanding Business Award 2017, FMA Golden Globe Enterprise Award 2018 and KWYP Dr. Sun Yat Sen Spirit Awards 2018." />
<meta name="description" content="Property Agency Of The Year in Asean Outstanding Business Award 2017, FMA Golden Globe Enterprise Award 2018 and KWYP Dr. Sun Yat Sen Spirit Awards 2018." />
<meta name="keywords" content="GIC, GIC GROUP, GIC房产投资集团,房产,槟城,马来西亚,penang,malaysia,gicproperty, gicproperty, Property, Malaysia Property, Penang Property, Malaysia Penang Property, Penang Property For Sale and Rent, property for sale, property for rent, buy property, sell property, penang real estate, penang apartment, penang condo, malaysia property, malaysia real estate, apartment, apartments, condo, home, homes, house, property valuation, project marketing, etc">
<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>