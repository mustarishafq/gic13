<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$username = $_SESSION['username'];

$conn = connDB();

$projectList = getProject($conn, "WHERE display = 'Yes'");

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Announcement | GIC" />
    <title>Announcement | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
#alert{
  font-size: 13px;
}
  input:focus, textarea:focus, select:focus{
    outline: none;
  }
  #addNewProjectBtn{
    color: black;
  }
  #changePasswordCheck{
    width :15px;
    height :15px;
    vertical-align: middle;
    outline: 2px solid maroon;
    outline-offset: -2px;
  }
  .dual-input-div .maroon-text:hover{
    color: maroon;
    font-weight: bold;
    transition: 0.1s;
    /* transition-delay: 0.1s; */
    transform: scale(1.01);
    /* font-size: 17px; */
  }
  input{
    color: maroon;
    font-weight: bold;
  }
  .dual-input-div .maroon-text,.dual-input-div .label{
    font-size: 16px;
  }
  /*
  .dual-input-div .maroon-text{
    color: maroon;
    font-weight: bold;
    animation-duration: 4s;
    animation-name: example;
  }
  .positionStyle{
    color: maroon;
    font-weight: bold;
    animation-name: example;
    animation-duration: 4s;
  }
  .positionStyle:hover{
    color: black;
  }
  button{
    transition-duration: 0.1s;
  }
  button:hover{
    width: 155px;
    height: 47px;
  }
  .short-red-border{
    animation-duration: 4s;
    animation-name: examples;
  }
  .support{
    font-size: 12px;
    font-weight: bold;
    color: red;
  }
  .support:hover{
    font-size: 15px;
    color: red;
  }
  @keyframes example {
    0%   {color: black;}
    100% {color: maroon;}
  }
  @keyframes examples {
    0%   {margin-left: 50px;}
    100% {left: 0;}
  }*/
</style>
<body class="body">
<?php if ($_SESSION['usertype_level'] %4 == 1) {
  include 'admin1Header.php';
}elseif ($_SESSION['usertype_level'] %4 == 2) {
  include 'admin2Header.php';
} ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1"><a style="color: maroon" href="announcementCurrent.php">Announcement</a> | New Announcement </h1>
    <div class="short-red-border"></div><br>
    <div class="clear"></div>
<div id="AddNewProject">
    <form method="POST" action="utilities/announcementFunction.php" enctype="multipart/form-data">
      <div class="dual-input-div">
        <p class="label">Project Name </p>
        <select class="dual-input clean" id="project_name" name="project_name">
          <option value="">Select an option</option>
          <?php if ($projectList) {
            for ($i=0; $i <count($projectList) ; $i++) {
              ?><option value="<?php echo $projectList[$i]->getProjectName() ?>"><?php echo $projectList[$i]->getProjectName() ?></option><?php
            }
          } ?>
        </select>
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">Project Leader </p>
        <input class="dual-input clean" readonly type="text" placeholder="auto fill" id="projectLeader" name="project_leader" value="">
      </div>
      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p class="label">Commission </p>
        <input class="dual-input clean" type="number" step="any" placeholder="Commission" id="commission" name="commission"  >
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">Packages </p>
        <input class="dual-input clean" type="text" placeholder="Packages" id="packages" name="packages"  >
      </div>
      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p class="label">Presentation File </p>
        <!-- <form class="form" action="announcementFunction.php" method="post" enctype="multipart/form-data"> -->
            <div class="form-group">
                <input type="file" name="present_file" />
            </div>
          <!-- </form> -->
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">E-Brochure </p>
        <!-- <form class="form" action="announcementFunction.php" method="post" enctype="multipart/form-data"> -->
            <div class="form-group">
                <input type="file" name="broshure" />
            </div>
          <!-- </form> -->
      </div>
      <div class="tempo-two-input-clear"></div><br>
      <div class="dual-input-div">
        <p class="label">Booking Form </p>
        <!-- <form class="form" action="announcementFunction.php" method="post" enctype="multipart/form-data"> -->
            <div class="form-group">
                <input type="file" name="booking_form" />
            </div>
          <!-- </form> -->
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">Video Link </p>
        <input class="dual-input clean" type="text" placeholder="Video Link" id="video_link" name="video_link"  >
      </div>
      <div class="tempo-two-input-clear"></div><br>
      <a class="support">* Support Uploaded File pdf ,doc, docx, png, jpg, jpeg.</a><br>
        <button id="noEnterSubmit" class="button" type="submit" name="announcement">Submit</button>
    </form>
  </div>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Created an Announcement. ";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Wrong Current Password.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully Updated The Project Details";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>
<script>
  $(function(){
    $("#project_name").change(function(){
      var project = $(this).val();

      $.ajax({
        url : 'getPLName.php',
        type : 'post',
        data : {projectName:project},
        dataType : 'json',
        success:function(response){

          var totalPL = response[0]['totalPLName'];
          $("#projectLeader").val(totalPL);
        }
      });
    });
  });
</script>
