<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/Commission.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$fullName = $_SESSION['fullname'];
$project = $_POST['project'];

$conn = connDB();
$a = 0;
$adv = 0;
$comm1 = 0;
$comm2 = 0;
$comm3 = 0;
$comm4 = 0;
$comm5 = 0;
$userRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($fullName),"s");
$userDetails = $userRows[0];

$projectDetails = getProject($conn);
 ?>
<table id="myTable" class="shipping-table pointer-th">
    <thead>
        <tr>
            <th class="th">NO. <img src="img/sort.png" class="sort"></th>

            <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th">PROJECT NAME <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("TOTAL COMMISSION",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("1ST CLAIM COMMISSION (RM)",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("2ND CLAIM COMMISSION (RM)",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("3RD CLAIM COMMISSION (RM)",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("4TH CLAIM COMMISSION (RM)",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("5TH CLAIM COMMISSION (RM)",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("6TH CLAIM COMMISSION (RM)",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("PAYSLIP DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("UNCLAIMED BALANCE (RM)",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
        </tr>
    </thead>
    <tbody id="myFilter">
        <?php
        // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
        // {
            $orderDetails = getLoanStatus($conn, "WHERE agent = ? and display = 'Yes' and cancelled_booking != 'YES' AND project_name =?",array("agent","project_name"),array($fullName,$project), "ss");
            if($orderDetails != null)
            {
                for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                {
                  $advancedDetails = getAdvancedSlip($conn, "WHERE status = 'COMPLETED' AND loan_uid = ?", array("loan_uid"), array($orderDetails[$cntAA]->getLoanUid()), "s");
                  ?>
                <tr>
                    <td class="td"><?php echo ($cntAA+1)?></td>

                    <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getUnitNo(),10,"<br>");?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                    <td class="td"><?php echo $spaPriceValue = number_format($orderDetails[$cntAA]->getAgentComm(), 2); ?></td>
                    <?php if ($advancedDetails && $advancedDetails[0]->getDateUpdated()) {
                      $a = $orderDetails[$cntAA]->getAgentComm() - $advancedDetails[0]->getAmount();
                      ?>

                      <td class="td"><?php echo number_format($advancedDetails[0]->getAmount(), 2); ?></td><?php
                      ?> <td> <form action="advancedSlipAgent.php" method="POST">
                        <input type="hidden" name="loan_uid" value="<?php echo $advancedDetails[0]->getLoanUid(); ?>">
                        <input type="hidden" name="claim_type" value="<?php echo $advancedDetails[0]->getClaimType(); ?>">
                        <input type="hidden" name="claim_id" value="<?php echo $advancedDetails[0]->getClaimID(); ?>">
                        <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="loan_uid" value="<?php echo $advancedDetails[0]->getLoanUid();?>"><?php echo date('d-m-Y', strtotime($advancedDetails[0]->getDateUpdated()))  ?>
                              <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                              <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                          </button></a>
                      </form></td><?php
                      if ($advancedDetails) {
                        $commissionDetails = getCommission($conn, "WHERE loan_uid = ? AND upline_default = ? AND details = '2nd Part Commission' AND receive_status = 'COMPLETED'", array("loan_uid","upline_default"), array($advancedDetails[0]->getLoanUid(),$advancedDetails[0]->getAgentDefault()), "ss");
                      }
                  if ($commissionDetails) {
                    if ($commissionDetails[0]->getClaimType() == 'Multi') {
                      $claimId = $commissionDetails[0]->getClaimID();
                      $commissionDetails = getCommission($conn, "WHERE claim_id = ? AND unit_no =?", array("claim_id","unit_no"), array($claimId,$orderDetails[$cntAA]->getUnitNo()), "ss");
                    }

                    unset($commission2nd); // remove previous array file

                    for ($i=0; $i <count($commissionDetails) ; $i++) {
                      $commission2nd[] = number_format($commissionDetails[$i]->getCommission(),2);
                    }
                    $commission2ndImp = implode(",,",$commission2nd);

                    ?>
                      <td class="td"><?php echo str_replace(",,","<br>",$commission2ndImp); ?> </td>
                      <td> <form action="commissionAgent.php" method="POST">
                        <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="claim_id" value="<?php echo $commissionDetails[0]->getClaimID();?>"><?php echo date('d-m-Y', strtotime($commissionDetails[0]->getDateCreated()))  ?>
                              <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                              <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                          </button></a>
                      </form></td>
                    <?php }else {
                      ?><td></td><td></td> <?php
                    } ?>
                     <?php
                     if ($advancedDetails) {
                       $commissionDetails = getCommission($conn, "WHERE loan_uid = ? AND upline_default = ? AND details = '3rd Part Commission' AND receive_status = 'COMPLETED'", array("loan_uid","upline_default"), array($advancedDetails[0]->getLoanUid(),$advancedDetails[0]->getAgentDefault()), "ss");
                     }
                     if ($commissionDetails) {
                       if ($commissionDetails[0]->getClaimType() == 'Multi') {
                         $claimId = $commissionDetails[0]->getClaimID();
                         $commissionDetails = getCommission($conn, "WHERE claim_id = ? AND unit_no =?", array("claim_id","unit_no"), array($claimId,$orderDetails[$cntAA]->getUnitNo()), "ss");
                       }

                       unset($commission3rd); // remove previous array file
                   for ($i=0; $i <count($commissionDetails) ; $i++) {
                     $commission3rd[] = number_format($commissionDetails[$i]->getCommission(),2);
                     $commission3rdImp = implode(",,",$commission3rd);
                   }

                   ?>
                     <td class="td"><?php echo str_replace(",,","<br>",$commission3rdImp); ?> </td>
                     <td> <form action="commissionAgent.php" method="POST">
                       <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="claim_id" value="<?php echo $commissionDetails[0]->getClaimID();?>"><?php echo date('d-m-Y', strtotime($commissionDetails[0]->getDateCreated()))  ?>
                             <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                             <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                         </button></a>
                     </form></td>
                   <?php }else {
                     ?><td></td><td></td> <?php } ?> <?php
                     if ($advancedDetails) {
                       $commissionDetails = getCommission($conn, "WHERE loan_uid = ? AND upline_default = ? AND details = '4th Part Commission' AND receive_status = 'COMPLETED'", array("loan_uid","upline_default"), array($advancedDetails[0]->getLoanUid(),$advancedDetails[0]->getAgentDefault()), "ss");
                     }
                     if ($commissionDetails) {
                       if ($commissionDetails[0]->getClaimType() == 'Multi') {
                         $claimId = $commissionDetails[0]->getClaimID();
                         $commissionDetails = getCommission($conn, "WHERE claim_id = ? AND unit_no =?", array("claim_id","unit_no"), array($claimId,$orderDetails[$cntAA]->getUnitNo()), "ss");
                       }

                       unset($commission4th); // remove previous array file
                   for ($i=0; $i <count($commissionDetails) ; $i++) {
                     $commission4th[] = number_format($commissionDetails[$i]->getCommission(),2);
                     $commission4thImp = implode(",,",$commission4th);
                   }

                   ?>
                     <td class="td"><?php echo str_replace(",,","<br>",$commission4thImp); ?> </td>
                     <td> <form action="commissionAgent.php" method="POST">
                       <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="claim_id" value="<?php echo $commissionDetails[0]->getClaimID();?>"><?php echo date('d-m-Y', strtotime($commissionDetails[0]->getDateCreated()))  ?>
                             <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                             <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                         </button></a>
                     </form></td>
                   <?php }else {
                     ?><td></td><td></td> <?php } ?><?php
                   if ($advancedDetails) {
                     $commissionDetails = getCommission($conn, "WHERE loan_uid = ? AND upline_default = ? AND details = '5th Part Commission' AND receive_status = 'COMPLETED'", array("loan_uid","upline_default"), array($advancedDetails[0]->getLoanUid(),$advancedDetails[0]->getAgentDefault()), "ss");
                   }
                   if ($commissionDetails) {
                     if ($commissionDetails[0]->getClaimType() == 'Multi') {
                       $claimId = $commissionDetails[0]->getClaimID();
                       $commissionDetails = getCommission($conn, "WHERE claim_id = ? AND unit_no =?", array("claim_id","unit_no"), array($claimId,$orderDetails[$cntAA]->getUnitNo()), "ss");
                     }

                     unset($commission5th); // remove previous array file
                 for ($i=0; $i <count($commissionDetails) ; $i++) {
                   $commission5th[] = number_format($commissionDetails[$i]->getCommission(),2);
                   $commission5thImp = implode(",,",$commission5th);
                 }

                 ?>
                   <td class="td"><?php echo str_replace(",,","<br>",$commission5thImp); ?> </td>
                   <td> <form action="commissionAgent.php" method="POST">
                     <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="claim_id" value="<?php echo $commissionDetails[0]->getClaimID();?>"><?php echo date('d-m-Y', strtotime($commissionDetails[0]->getDateCreated()))  ?>
                           <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                           <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                       </button></a>
                   </form></td>
                 <?php }else {
                   ?><td></td><td></td> <?php } ?><?php
                 if ($advancedDetails) {
                   $commissionDetails = getCommission($conn, "WHERE loan_uid = ? AND upline_default = ? AND details = '6th Part Commission'", array("loan_uid","upline_default"), array($advancedDetails[0]->getLoanUid(),$advancedDetails[0]->getAgentDefault()), "ss");
                 }
                 if ($commissionDetails) {
                   if ($commissionDetails[0]->getClaimType() == 'Multi') {
                     $claimId = $commissionDetails[0]->getClaimID();
                     $commissionDetails = getCommission($conn, "WHERE claim_id = ? AND unit_no =?", array("claim_id","unit_no"), array($claimId,$orderDetails[$cntAA]->getUnitNo()), "ss");
                   }

                   unset($commission6th); // remove previous array file
               for ($i=0; $i <count($commissionDetails) ; $i++) {
                 $commission6th[] = number_format($commissionDetails[$i]->getCommission(),2);
                 $commission6thImp = implode(",,",$commission6th);
               }

               ?>
                 <td class="td"><?php echo str_replace(",,","<br>",$commission6thImp); ?> </td>
                 <td> <form action="commissionAgent.php" method="POST">
                   <a><button class="clean edit-anc-btn hover1" style="color: blue" type="submit" name="claim_id" value="<?php echo $commissionDetails[0]->getClaimID();?>"><?php echo date('d-m-Y', strtotime($commissionDetails[0]->getDateCreated()))  ?>
                         <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                         <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                     </button></a>
                 </form></td>
               <?php }else {
                 ?><td></td><td></td> <?php } ?><?php
                ?>
                <td class="td"><?php $agentComm = explode(",",$orderDetails[$cntAA] ->getAgentBalance());
                                      echo number_format($agentComm[0],2);
                 ?></td>
                </tr><?php
              }else {
                ?><td colspan="13"></td> <?php
              }

          }

        }else {
          ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="17">No Personal Commission.</td> <?php
        }
        //}
        ?>
    </tbody>
</table>
