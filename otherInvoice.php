<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/OtherInvoice.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Address.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$addressDetails = getAddress($conn, "WHERE branch_type = ?",array("branch_type"),array($_SESSION['branch_type']), "s");

if ($addressDetails) {
  $logo = $addressDetails[0]->getLogo();
  $companyName = $addressDetails[0]->getCompanyName();
  $addressNo = $addressDetails[0]->getAddressNo();
  $companyBranch = $addressDetails[0]->getCompanyBranch();
  $companyAddress = $addressDetails[0]->getCompanyAddress();
  $contact = $addressDetails[0]->getContact();
}

// $invoiceDetails = getInvoice($conn, "WHERE loan_uid =? AND claims_status = ?",array("loan_uid","claims_status"),array($_POST['loan_uid'],$_POST['claim_status']), "ss");
$projectDetails = getProject($conn, "WHERE project_name =?",array("project_name"),array($_POST['project_name']),"s");
// echo $projectDetails[0]->getAddProjectPpl();

// $loanDetails = getLoanStatus($conn, "WHERE loan_uid =?", array("loan_uid"), array($_POST['loan_uid']), "s");
// $spaPrice = $loanDetails[0]->getSpaPrice();
// $nettPrice = $loanDetails[0]->getNettPrice();
// $invoiceNo = getInvoice($conn, "WHERE id =? ORDER BY id DESC LIMIT 1",array("id"),array($_POST['id']), "s");
$amount5 = 0;
$amount4 = 0;
$amount3 = 0;
$amount2 = 0;
$amount = 0;

$amount = $_POST['amount'];
$amountImplode = implode(",",$amount);
$amountExplode = explode(",",$amountImplode);

// $invoiceNo = rewrite($_POST['invoice_no']);
// $requestDate = rewrite($_POST['request_date']);
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Invoice | GIC" />
    <title>Other Invoice | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Other Invoice</h1>
    <div class="short-red-border"></div>
    <!-- This is a filter for the table result -->


    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->

    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="width100 print-div">
      <div class="text-center">
              <img src="<?php echo "logo/".$logo ?>" class="invoice-logo" alt="GIC Consultancy Sdn. Bhd." title="GIC Consultancy Sdn. Bhd.">
              <p id="companyName" class="invoice-address company-name" value="<?php echo $companyName ?>"><b><?php echo $companyName ?></b></p>
              <p id="companyNameInput" class="invoice-address company-name" style="display: none" ><input id="companyNameInput2" type="text" class="input-text" name="" value="<?php echo $companyName ?>"></p>
              <p id="addressNo" class="invoice-small-p"><?php echo $addressNo ?></p>
              <p id="addressNoInput" class="invoice-small-p" style="display: none" ><input id="addressNoInput2" type="text" class="input-text" name="" value="<?php echo $addressNo ?>"></p>

              <?php if ($companyBranch) {
              ?><p id="companyBranch" class="invoice-address"><?php echo $companyBranch ?></p>
              <p id="companyBranchInput" class="invoice-address" style="display: none" ><input id="companyBranchInput2" type="text" class="input-text" name="" value="<?php echo $companyBranch ?>"></p><?php
              } ?>

              <p id="companyAddress" class="invoice-address"><?php echo $companyAddress ?></p>
              <p id="companyAddressInput" class="invoice-address" style="display: none" ><input id="companyAddressInput2" type="text" class="input-text" name="" value="<?php echo $companyAddress ?>"></p>
              <p id="info" class="invoice-address"><?php echo $contact ?></p>
              <p id="infoInput" class="invoice-address" style="display: none" ><input id="infoInput2" type="text" class="input-text" name="" value="<?php echo $contact ?>"></p>
          </div>
		<h1 class="invoice-title">OTHER INVOICE</h1>
        <div class="invoice-width50 top-invoice-w50">
        	<p class="invoice-p">
            	<b>Attn: <?php echo $projectDetails[0]->getAddProjectPpl(); ?></b>
            </p>
        </div>
        <div class="invoice-width50 top-invoice-w50">
			<table class="invoice-top-small-table">
            	<tr>
                	<td><b>Invoice No</b></td>
                    <td><b>:</b></td>
                    <td><b><?php echo date('ymd').$projectDetails[0]->getId().$projectDetails[0]->getProjectClaims()   ?></b></td>
                </tr>
                <tr>
                	<td>Project</td>
                    <td>:</td>
                    <td><?php echo $projectDetails[0]->getProjectName()  ?></td>
                </tr>
                <tr>
                	<td>Date</td>
                    <td>:</td>
                    <td><?php echo date('d/m/Y',strtotime($projectDetails[0]->getDateCreated())) ?></td>
                </tr>
            </table>
        </div>
        <div class="clear"></div>
        <table class="invoice-printing-table">
        	<thead>
                    <tr>
                    	<th>No.</th>
                      <th>Item</th>
                        <th>Amount (RM)</th>
                    </tr>
            </thead>
            <tr>
              <td></td>
              <td><?php echo $_POST['project_details'] ?></td>
              <td><?php
              for ($AA=0; $AA < count($amountExplode) ; $AA++) {
                if ($_POST['project_charges'] == 'YES') {
                  echo ($amountExplode[$AA]);
                }elseif ($_POST['project_charges'] == 'NO') {
                 echo ( ($amountExplode[$AA] / 1.06));
                }
              }

               ?></td>
            </tr>






        </table>
		<div class="clear"></div>
        <div class="invoice-width50 right-w50">
			<table class="invoice-bottom-small-table">
            	<tr>
                	<td>Sum Amount (excluding Service Tax)</td>
                    <td>:</td>
                    <?php if ($_POST['project_charges'] == 'YES') {
                    ?>  <td><?php echo number_format(($_POST['project_amount']), 2); ?></td> <?php
                  }elseif ($_POST['project_charges'] == 'NO') {
                    $amountExcluded =  $_POST['project_amount'] / 1.06;
                    ?>  <td><?php echo number_format(($amountExcluded), 2); ?></td> <?php
                  } ?>
                </tr>
                <tr>
                	<td>Service Tax 6%</td>
                    <td>:</td>
                    <?php
                      if ($_POST['project_charges'] == 'YES') {
                        ?><td><?php echo number_format(($_POST['project_amount'] - $_POST['project_amount']), 2) ?></td><?php
                      }elseif ($_POST['project_charges'] == 'NO') {
                        ?><td><?php $amountExcluded =  $_POST['project_amount'] / 1.06;
                        echo number_format($final = $_POST['project_amount'] - $amountExcluded,2);
                         ?></td> <?php
                      }

                  ?>
                </tr>
                <tr>
                	<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                	<td><b>Total Amount</b></td>
                    <td>:</td>
                    <td class="bottom-3rd-td border-td"><b><?php echo number_format($_POST['project_amount'], 2) ?></b></td>
                </tr>
            </table>

        </div>

        <div class="invoice-width50 left-w50">
			<table class="invoice-small-table">
            	<tr>
                	<td><b><u>Payee Details:</u></b></td>
                    <td><b>:</b></td>
                    <td></td>
                </tr>
                <tr>
                	<td>Name</td>
                    <td><b>:</b></td>
                    <td><b><?php
                    if (isset($_POST['project_account_holder'])) {
                      echo $_POST['project_account_holder'];
                    }else {
                      echo "Gic Holding Sdn Bhd";
                    }
                      ?></b></td>
                </tr>
                <tr>
                	<td>Bank</td>
                    <td><b>:</b></td>
                    <td><b><?php   if (isset($_POST['bank_name'])) {
                        echo $_POST['bank_name'];
                      }else {
                        echo "Public Bank Berhad";
                      } ?></b></td>
                </tr>
                <tr>
                	<td>Account No.</td>
                    <td><b>:</b></td>
                    <td><b><?php   if (isset($_POST['acc_no'])) {
                        echo $_POST['acc_no'];
                      }else {
                        echo "012345678";
                      } ?></b></td>
                </tr>
            </table>
        </div>
        <div class="invoice-print-spacing"></div>
        <div class="signature-div">
        	<div class="signature-border"></div>
            <p class="invoice-p"><b>GIC Consultancy Sdn Bhd</b></p>
            <p class="invoice-p">Eddie Song</p>
        </div>
    </div>
	<div class="clear"></div>
    <div class="dual-button-div width100 same-padding">
    	<a href="#">
            <div class="left-button white-red-line-btn">
                Edit
            </div>
        </a>
    	<a href="#">
            <button class="right-button red-btn clean"  onclick="window.print()">
                Print
            </button>
        </a>
    </div>


</div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
</body>
</html>
