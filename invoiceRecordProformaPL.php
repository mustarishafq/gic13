<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/classes/ProformaInvoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $loanDetails = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$invoiceDetails = getProformaInvoice($conn);
$projectDetails = getProject($conn);
$finalAmountFinal = 0;
$projectNameArr = [];
// $projectName = "WHERE case_status = 'COMPLETED'";
$projectName = "WHERE case_status = 'COMPLETED' AND cancelled_booking != 'YES'";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Invoice Record-Proforma | GIC" />
    <title>Invoice Record-Proforma | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'agentHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Invoice Record-Proforma</h1>
    <div class="short-red-border"></div>
    <h3 class="h1-title">Proforma Invoice |<a href="invoiceRecordPL.php" class="h1-title red-color-swicthing">Single Invoice</a> | <a  href="invoiceRecordMultiPL.php" class="h1-title red-color-swicthing">Multi Invoice</a> | <a href="invoiceRecordOtherPL.php" class="h1-title red-color-swicthing">Other Invoice</a> | <a href="invoiceRecordCredit.php" class="h1-title red-color-swicthing">Credit Note Invoice</a></h3>
    <!-- This is a filter for the table result -->
	<!-- <div class="clean"></div> -->
  <div class="section-divider width100 overflow">

    <?php $projectDetails = getProject($conn, "WHERE display = 'Yes' AND project_leader=?",array("project_leader"),array($_SESSION['username']), "s"); ?>

      <select id="sel_id" class="clean-select clean pointer">
        <option value="">Select a project</option>
        <?php if ($projectDetails) {
          for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
            $projectNameArr[] = $projectDetails[$cnt]->getProjectName();
            if ($projectDetails[$cnt]->getProjectName() != $types) {
              ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
              }
              }
              ?><option value="ALL">SHOW ALL</option><?php
            } ?>
      <!-- <option value="-1">Select</option>
      <option value="VIDA">kasper </option>
      <option value="VV">adad </option> -->
      <!-- <option value="14">3204 </option> -->
      </select>
      <input class="clean search-btn" type="text" name="" value="" placeholder="Search.." id="searchInput">
  </div>




    <!-- End of Filter -->
    <div class="clear"></div>
    <a href="excel/invoiceRecordProformaExport.php"><button class="exportBtn red-bg-swicthing" type="button" name="button">Export</button></a>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table id="overallTable" class="shipping-table pointer-th">
                <thead>
                    <tr>
                        <th class="th">NO. <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("PROJECT NAME",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th">DATE <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("INVOICE NO.",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th">AMOUNT (RM) <img src="img/sort.png" class="sort"></th>
                        <th class="th">TOTAL AMOUNT (RM) <img src="img/sort.png" class="sort"></th>
                        <th class="th">SST <img src="img/sort.png" class="sort"></th>
                        <th class="th">DATE MODIFIED <img src="img/sort.png" class="sort"></th>
                        <th class="th">ACTION <img src="img/sort.png" class="sort"></th>
                        <!-- <th>STOCK</th> -->


                        <!-- <th class="th">BANK APPROVED</th>
                        <th class="th">LO SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th> -->
                        <!-- <th>INVOICE</th> -->
                    </tr>
                </thead>
                <tbody id="myFilter">
                    <?php
                    // if ($invoiceDetails) {
                    $projectNameArrImp = implode(",",$projectNameArr);
                    $projectNameArrExp = explode(",",$projectNameArrImp);
                      for ($i=0; $i <count($projectNameArrExp) ; $i++) {
                        $orderDetails = getProformaInvoice($conn, "WHERE invoice_name ='Proforma' and branch_type = ? AND project_name =?",
                        array("branch_type","project_name"),array($_SESSION['branch_type'],$projectNameArrExp[$i]), "ss");
                        if($orderDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                            {
                              $proDetails = getProject($conn, "WHERE project_name=?",array("project_name"),array($orderDetails[$cntAA]->getProjectName()), "s");
                              if ($proDetails) {

?>
                            <tr>
                                <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                <td class="td"><?php echo $cntAA + 1?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getUnitNo()) ;?></td>
                                <td class="td"><?php echo date('d/m/Y',strtotime($orderDetails[$cntAA]->getDateCreated()));?></td>
                                <td class="td"><?php echo date('ymd',strtotime($orderDetails[$cntAA]->getDateCreated()))."1".$orderDetails[$cntAA]->getId();?></td>
                                <td class="td"><?php $getAmountExp = explode(",",$orderDetails[$cntAA]->getFinalAmount());
                                for ($cntas=0; $cntas <count($getAmountExp) ; $cntas++) {
                                  $finalAm = $getAmountExp[$cntas];
                                  echo number_format($finalAm,2)."<br>";
                                } ?>
                              </td>
                                <td class="td"><?php $getAmountExplode = explode(",",$orderDetails[$cntAA]->getFinalAmount());
                                for ($cntas=0; $cntas <count($getAmountExplode) ; $cntas++) {
                                  $finalAmount = $getAmountExplode[$cntas];
                                  $finalAmountFinal += $finalAmount;
                                }
                                echo number_format($finalAmountFinal,2);
                                $finalAmountFinal = 0;  ?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getCharges();?></td>
                                <td class="td">
                                  <form class="" action="historyProforma.php" method="post">
                                    <button class="clean edit-anc-btn hover1 blue-link" value="<?php echo $orderDetails[$cntAA]->getProformaId() ?>" type="submit" name="proforma_id">
                                      <a class="blue-link"><?php if ($orderDetails[$cntAA]->getDateUpdated()) {
                                        echo date('d/m/Y',strtotime($orderDetails[$cntAA]->getDateUpdated()));
                                      } ?></a>
                                    </button>
                                  </form>
                                </td>
                                <td class="td">
                                  <form action="invoiceProforma.php" method="POST">
                                    <a><button class="clean edit-anc-btn hover1 blue-link" type="submit" name="id" value="<?php echo $orderDetails[$cntAA]->getId();?>">Print Proforma
                                      <input type="hidden" name="invoice_name" value="Proforma">
                                      <input type="hidden" name="claim_status" value="2">
                                      <input type="hidden" name="invoice_no" value="<?php echo date('ymd',strtotime($orderDetails[$cntAA]->getDateCreated()))."1".$orderDetails[$cntAA]->getId();?>">
                                      <input type="hidden" name="booking_date" value="<?php echo date('d/m/Y',strtotime($orderDetails[$cntAA]->getDateCreated()))?>">
                                          <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                          <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                      </button></a>
                                    </form>
                                </td>
                              </tr> <?php
                              }
                            }
                          }else {
                            ?>
                            <td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="10" >No Invoice Record Yet.</td>
                            <?php
                          }
                        }?>
                </tbody>
            </table><br>


    </div>

    <!-- <div class="three-btn-container">
    <!-- <a href="adminRestoreProduct.php" class="add-a"><button name="restore_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side two-button-side1">Restore</button></a> -->
      <!-- <a href="adminAddNewProduct.php" class="add-a"><button name="add_new_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side  two-button-side2">Add</button></a> -->
    <!-- </div> -->
    <?php $conn->close();?>

</div>



<?php unset($_SESSION['idPro']);
      unset($_SESSION['invoice_id']);
      unset($_SESSION['idComm']);
      unset($_SESSION['loan_uid']);
      unset($_SESSION['commission_id']);
      unset($_SESSION['idImp']);?>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Updated Proforma Invoice Details.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Issue Proforma Invoice.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<style>
      body {

      }
      input {

      font-weight: bold;
      /* color: white; */
      text-align: center;
      border-radius: 15px;
      }
      .no-outline:focus {
      outline: none;

      }
      button{
        border-radius: 15px;
        color: white;

      }
      .button-add{
        background-color: green;
      }
      .button-remove{
        background-color: maroon;
      }th:hover{
        background-color: maroon;
      }
      th.headerSortUp{
        background-color: black;
      }
      th.headerSortDown{
        background-color: black;
      }
    </style>
</body>
<script>
$(function () {
    $("#overallTable").tablesorter( {dateFormat: 'pt'} );
});
</script>
</html>
