<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/EditHistory.php';
require_once dirname(__FILE__) . '/classes/Branch.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
// $_SESSION['usernameNew'] = "";
$projectName = "";
$upline = [];
// $sql = "select pdf from loan_status";
// $result = mysqli_query($conn, $sql);
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Agent Info | GIC" />
    <title>Agent Info | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style>
.aaa{
  color: maroon;
}
.aaa:hover{
  color: maroon;
  text-decoration: none;
}
.th0 {
    vertical-align: middle;
    text-align: center;
    z-index: 2;
    background-color: #7babff;
}
#agentDetailsBox{
  cursor: pointer;
}
#agentDetailsBox:hover{
  cursor: pointer;
  color: red;
  text-decoration: none;
}
.th:hover, .th0:hover{
  background-color: maroon;
}
.th.headerSortUp , .th0.headerSortUp{
  background-color: maroon;
}
.th.headerSortDown , .th0.headerSortDown{
  background-color: maroon;
}
</style>
<body class="body">
<?php  include 'admin1Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<?php echo '<script type="text/javascript" src="js/jquery.tablesorter.js"></script>'; ?>
<div class="yellow-body same-padding">
	<?php if ($_SESSION['branch_type'] == 1) {
    ?><h1 class="h1-title h1-before-border shipping-h1"><a class="aaa red-color-swicthing" href="adminInfo.php">Admin Info</a> | Agent Info </h1><?php
  }else {
    ?><h1 class="h1-title h1-before-border shipping-h1">Agent Info </h1><?php
  } ?>
    <div class="short-red-border"></div>
    <div class="section-divider width100 overflow">
		<?php $projectDetails = getProject($conn, "WHERE display = 'Yes'"); ?>


        <input class="clean search-btn-left" type="text" name="" value="" placeholder="Search.." id="searchInput">
      <!-- <label>Start Date :</label> <input class="clean-select clean pointer" type="date" name="" value="">
      <label>End Date :</label> <input class="clean-select clean pointer" type="date" name="" value=""> --><a href="excel/agentExportExcel.php">
            <button class="exportBtn-right red-bg-swicthing" type="button" name="button">Export</button>
        </a>
    </div>



    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->
    <!-- End of Filter -->


    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table id="myTable" class="shipping-table pointer-th">
                <thead>
                    <tr>
                        <th class="th0 company-td">NO. <img src="img/sort.png" class="sort"></th>
                        <th class="th0 agent-td1">ACTION <img src="img/sort.png" class="sort"></th>
                        <th class="th0 agent-td2">STATUS <img src="img/sort.png" class="sort"></th>
                        <th class="th0 agent-td2-5"><?php echo wordwrap("COMPANY BRANCH",10,"<br>") ?> <img src="img/sort.png" class="sort"></th>
                        <th class="th0 agent-td3"><?php echo wordwrap("AGENT FULLNAME",10,"<br>") ?> <img src="img/sort.png" class="sort"></th>
                        <th class="th0 agent-td4"><?php echo wordwrap("AGENT USERNAME",10,"<br>") ?> <img src="img/sort.png" class="sort"></th>
                        <th class="th">POSITION <img src="img/sort.png" class="sort"></th>
                        <th class="th">IC NO <img src="img/sort.png" class="sort"></th>
                        <th class="th">BIRTH MONTH <img src="img/sort.png" class="sort"></th>
                        <th class="th">CONTACT NO <img src="img/sort.png" class="sort"></th>
                        <th class="th">E-MAIL <img src="img/sort.png" class="sort"></th>
                        <th class="th">BANK NAME <img src="img/sort.png" class="sort"></th>
                        <th class="th">BANK NO. <img src="img/sort.png" class="sort"></th>
                        <th class="th">ADDRESS <img src="img/sort.png" class="sort"></th>
                        <th class="th">UP1 <img src="img/sort.png" class="sort"></th>
                        <th class="th">UP2 <img src="img/sort.png" class="sort"></th>
                        <th class="th">UP3 <img src="img/sort.png" class="sort"></th>
                        <th class="th">UP4 <img src="img/sort.png" class="sort"></th>
                        <th class="th">UP5 <img src="img/sort.png" class="sort"></th>
                        <th class="th">SUB-SALES COMMISSION <img src="img/sort.png" class="sort"></th>
                        <!-- <th class="th">DATE CREATED</th> -->
                    </tr>
                </thead>
                <tbody id="myFilter">
                    <?php
                    // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                    // {
                        $agentDetails = getUser($conn, "WHERE user_type = 3 ORDER BY date_created DESC" );
                        // $agentDetails = getLoanStatus($conn);
                        if($agentDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($agentDetails) ;$cntAA++){
                              $upline = [];
                              $branchDetails = getBranch($conn, "WHERE branch_type =?",array("branch_type"),array($agentDetails[$cntAA]->getBranchType()), "s");
                              $branchName = $branchDetails[0]->getBranchName();
                                ?>
                              <tr>
                                  <!-- <td class="td"><?php //echo ($cntAA+1)?></td> -->
                                  <td class="td company-td"><?php echo $cntAA + 1?></td>
                                  <td class="td agent-td1">
                                      <form action="editAgent.php" method="POST">
                                        <button class="edit-btn" type="submit" name="uid" value="<?php echo $agentDetails[$cntAA]->getUid();?>">
                                          Edit
                                          <!-- <button class="clean edit-anc-btn hover1" type="submit" name="uid" value="<?php //echo $agentDetails[$cntAA]->getUid();?>"> -->
                                              <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="edit Agent" title="edit Agent">
                                              <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="edit Agent" title="edit Agent"> -->
                                          </button>
                                      </form>
                                  </td>
                                  <td class="td agent-td2"><?php echo $agentDetails[$cntAA]->getStatus() ?></td>
                                  <td class="td agent-td2-5"><?php echo $branchName ?></td>
                                  <td class="td agent-td3"><a id="agentDetailsBox" class="<?php echo "agentDetailsBox".$cntAA ?>" value="<?php echo $agentDetails[$cntAA]->getFullName();?>"><?php echo wordwrap($agentDetails[$cntAA]->getFullName(),20,"<br>");?></a> </td>
                                  <td class="td agent-td4"><?php echo wordwrap($agentDetails[$cntAA]->getUsername(),20,"<br>");?></td>
                                  <td class="td"><?php echo wordwrap($agentDetails[$cntAA]->getPosition(),20,"<br>") ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getIcNo() ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getBirthday() ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getPhoneNo() ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getEmail() ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getBankName() ?></td>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getBankAccountNo() ?></td>
                                  <td class="td"><?php echo str_replace(",","<br>",$agentDetails[$cntAA]->getAddress()) ;?></td>
                                  <?php
                                  $agentReferralDetails = getReferralHistory($conn, "WHERE referral_id=?",array("referral_id"),array($agentDetails[$cntAA]->getUid()), "s");
                                  if ($agentReferralDetails) {
                                    $agentCurrentLevel = $agentReferralDetails[0]->getCurrentLevel();
                                    $agentFirstLevel = $agentCurrentLevel + 1;
                                    $uplineUidDetails = getTop10ReferrerOfUser($conn, $agentDetails[$cntAA]->getUid());
                                    if ($uplineUidDetails) {
                                      for ($i=0; $i <count($uplineUidDetails) ; $i++) {
                                        $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uplineUidDetails[$i]), "s");
                                        $fullName = $userDetails[0]->getFullName();
                                        $upline[] = $userDetails[0]->getFullName();

                                      }
                                    }
                                    $uplineImp = implode(",",$upline);
                                    $uplineExp = explode(",",$uplineImp);
                                    for ($i=0; $i <count($uplineExp) ; $i++) {
                                      if ($i == 0) {
                                        ?><td class="td"><?php echo $uplineExp[$i]; ?></td><?php
                                      }else
                                      if ($i == 1){
                                        ?><td class="td"><?php echo $uplineExp[$i]; ?></td><?php
                                      }else
                                      if ($i == 2){
                                        ?><td class="td"><?php echo $uplineExp[$i]; ?></td><?php
                                      }else
                                      if ($i == 3){
                                        ?><td class="td"><?php echo $uplineExp[$i]; ?></td><?php
                                      }else
                                      if ($i == 4){
                                        ?><td class="td"><?php echo $uplineExp[$i]; ?></td><?php
                                      }
                                    }
                                    for ($i=count($uplineExp); $i < 5 ; $i++) {
                                      ?><td class="td">-</td> <?php
                                    }
                                  }else{
                                    for ($i=0; $i < 5 ; $i++) {
                                      ?><td class="td">-</td> <?php
                                    }
                                  }
                                   ?>
                                  <td class="td"><?php echo $agentDetails[$cntAA]->getSubSalesComm() ?></td>
                                  <!-- <td class="td"><?php //echo date('d-m-Y',strtotime($agentDetails[$cntAA]->getDateCreated())) ?></td> -->
                              </tr>
                              <?php


                            }

                    }
                    ?>
                </tbody>
            </table><br>


    </div>


    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php include 'detailsBox.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Added New Agent. ";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Edit Agent Details.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
 ?>
</body>
<script type="text/javascript">
$(window).load(function () {
  var No = <?php echo count($agentDetails); ?>;
  for (var i = 0; i < No; i++) {
    $(".agentDetailsBox"+i+"").click(function(){
      var agentName = $(this).attr("value");
      // alert(agentName);

      $.ajax({
        url : 'utilities/agentDetailsFunction.php',
        type : 'post',
        data : {agentNames:agentName},
        dataType : 'json',
        success:function(response){
          var agentNamee = response[0]['agentNamed'];
          // alert(agentNamee);
          $("#ss").text(agentNamee);
        },
        error:function(response){
          // alert("failed");
        }
      });

       $('.hover_bkgr_fricc').fadeIn(function(){
         $("#myDiv").load(location.href + " #myDiv");
         $(this).show();
       });
    });
  }

  $('.hover_bkgr_fricc').click(function(){
      $('.hover_bkgr_fricc').fadeOut(function(){
        $("#mydiv").load(location.href + " #mydiv");
        $(this).hide();
      });
  });
  $('.popupCloseButton').click(function(){
      $('.hover_bkgr_fricc').fadeOut(function(){
        $("#mydiv").load(location.href + " #mydiv");
        $(this).hide();
      });
  });
});
</script>
<script>
    $(document).ready( function () {
            $("#myTable").tablesorter( {dateFormat: 'pt'} );
    });
</script>
</html>
