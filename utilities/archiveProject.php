<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter,$branchType)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after","branch_type"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter,$branchType),
     "ssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

$id = $_POST['id'];
$username = $_SESSION['username'];
$detailsBefore = "-";
$detailsAfter = "-";
$branchType = $_SESSION['branch_type'];


$loanStatusDetails = getProject($conn, "WHERE id = ?",array("id"),array($id), "s");
$projectName = $loanStatusDetails[0]->getProjectName();
$projectId = $loanStatusDetails[0]->getProjectId();
$currentDisplay = $loanStatusDetails[0]->getDisplay();
if ($currentDisplay == 'Yes') {
  $display = "No";
  $column = "Archived";
}elseif ($currentDisplay == 'No') {
  $display = "Yes";
  $column = "Un-Archived";
}


$tableName = array();
$tableValue =  array();
$stringType =  "";
// //echo "save to database";
if($loanStatusDetails)
{
    array_push($tableName,"display");
    array_push($tableValue,$display);
    $stringType .=  "s";
}
array_push($tableValue,$id);
$stringType .=  "s";
$withdrawUpdated = updateDynamicData($conn,"project"," WHERE id = ? ",$tableName,$tableValue,$stringType);
    if($withdrawUpdated)
    {
      if(editHistory($conn, $username, $column, $projectId,$detailsBefore, $detailsAfter,$branchType))
           {}
        // $_SESSION['messageType'] = 1;
        // header('Location: ../adminWithdrawal.php?type=1');
    }
    else
    {
        // echo "fail";
    }

    $project[] = array("projectName" => $projectName,"display" => $display);

    echo json_encode($project);
 ?>
