
<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
// require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


function addNewInvoice($conn,$invoiceId,$detailsImplode,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$projectHandlerDefault,$branchType)
{
    if(insertDynamicData($conn,"issue_payroll",array("invoice_id","item","unit_no","amount","project_name","booking_date","project_handler","charges","bank_account_holder","bank_name","bank_account_no","invoice_name","invoice_type","final_amount","receive_status","project_handler_default","branch_type"),
    array(  $invoiceId,$detailsImplode,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$projectHandlerDefault,$branchType),
    "sssssssssssssssss") === null)
    {
    //    echo $finalPassword;
    }
    else
    {
    //   echo "bbbb";
    }
    return true;
}
function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter,$branchType)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after","branch_type"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter,$branchType),
     "ssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    //problem
    // $purchaserName = rewrite($_POST["invoice_target"]);
    //get loan_uid
    // $loanStatusDetails = getLoanStatus($conn, "WHERE purchaser_name = ?",array("purchaser_name"),array($purchaserName), "s");
    // $loanUid = $loanStatusDetails[0]->getLoanUid();



    //add in oriject, to status, date,invoice,invoice type
    $invoiceId = md5(uniqid());
    $branchType = $_SESSION['branch_type'];
    $username = $_SESSION['username'];
    $projectName = rewrite($_POST['project_name']);
    $statusOfClaims = 0;
    $otherClaimsFinal = "";

    //auto get project handler
    $projectDetails = getProject($conn," WHERE project_name = ? ",array("project_name"),array($projectName),"s");
    if (isset($_POST['toWho'])) {
      $projectHandler = rewrite($_POST['toWho']);
      $projectHandlerFullName = getUser($conn,"WHERE full_name = ?",array("full_name"),array($projectHandler),"s");
      if ($projectHandlerFullName) {
        $projectHandler = $projectHandlerFullName[0]->getFullName();
      }
    }else {
      $projectHandler = $projectDetails[0]->getAddProjectPpl();
    }
    $projectClaims = $projectDetails[0]->getProjectClaims();
    $currentOtherClaims = $projectDetails[0]->getOtherClaims();


    $receiveStatus = 'PENDING';
    $dateOfClaims = rewrite($_POST['date_of_claims']);
    $invoiceName = rewrite($_POST['invoice_name']);

    $invoiceType = rewrite($_POST['invoice_type']);
    if (!$invoiceType)
    {
        $invoiceType = "";
    }

    $details = $_POST['details'];
    $remove = array(""); //want to remove null value 1,,3 to 1,3
    $resultDetails = array_diff($details, $remove); //want to remove null value 1,,3 to 1,3
    $detailsLimit = count($resultDetails); //want to remove null value 1,,3 to 1,3
    $detailsSlice = array_slice($details,0,$detailsLimit); //want to remove null value 1,,3 to 1,3
    $detailsImplode = implode(",",$detailsSlice); //want to remove null value 1,,3 to 1,3


    // $item = ($_POST['item']);
    // $remove = array(""); //want to remove null value 1,,3 to 1,3
    // $resultItem = array_diff($item, $remove); //want to remove null value 1,,3 to 1,3
    // $itemLimit = count($resultItem); //want to remove null value 1,,3 to 1,3
    // $itemSlice = array_slice($item,0,$itemLimit); //want to remove null value 1,,3 to 1,3
    // $itemImplode = implode(",",$itemSlice); //want to remove null value 1,,3 to 1,3

    $amount = ($_POST['amount']);
    $remove = array(""); //want to remove null value 1,,3 to 1,3
    $resultAmount = array_diff($amount, $remove); //want to remove null value 1,,3 to 1,3
    $amountLimit = count($resultAmount); //want to remove null value 1,,3 to 1,3
    $amountSlice = array_slice($amount,0,$amountLimit); //want to remove null value 1,,3 to 1,3
    $amountImplode = implode(",",$amountSlice); //want to remove null value 1,,3 to 1,3
    $amountExplode = explode(",",$amountImplode);

    foreach ($amountExplode as $otherClaims) {
      $otherClaimsFinal += $otherClaims;
    }

    // $totalAmountClaim = $amount + $amount2 + $amount3 + $amount4 + $amount5; //claim by admin2
    // $loanUid = rewrite($_POST["loan_uid"]);
    // $loanDetails = getLoanStatus($conn, "WHERE loan_uid = ? ", array("loan_uid"), array($loanUid), "s");
    // $totalClaimedDevAmt = $loanDetails[0]->getTotalClaimDevAmt();
    // $totalBalUnclaimAmt = $loanDetails[0]->getTotalBalUnclaimAmt();
    // $totalBalUnclaimAmt -= $totalAmountClaim;
    // $totalClaimedDevAmt += $totalAmountClaim;

    $unit = ($_POST['unit']);
    $remove = array(""); //want to remove null value 1,,3 to 1,3
    $resultUnit = array_diff($unit, $remove); //want to remove null value 1,,3 to 1,3
    $unitLimit = count($resultUnit); //want to remove null value 1,,3 to 1,3
    $unitSlice = array_slice($unit,0,$unitLimit); //want to remove null value 1,,3 to 1,3
    $unitImplode = implode(",",$unitSlice); //want to remove null value 1,,3 to 1,3
    $unitExplode = explode(",",$unitImplode);

    if (count($unitExplode) > 1) {
    $status = 'MULTI';
  }else {
    $status = 'SINGLE';
  }


    // $project = rewrite($_POST['project']);
    $charges = rewrite($_POST['charges']); // temporary

    foreach ($amountExplode as $otherClaimAmmtExp) {
      if ($charges == 'NO') {

        $otherClaimAmmtExpFinal[] = $otherClaimAmmtExp;
        // $otherClaimAmmtExpFinal[] = $otherClaimAmmtExp / 1.06;

      }elseif ($charges == 'YES') {

        $otherClaimAmmtExpFinal[] = $otherClaimAmmtExp;
        // $otherClaimAmmtExpFinal[] = $otherClaimAmmtExp * 1.06;
      }
    }
    echo $finalAmountExp = implode(",",$otherClaimAmmtExpFinal);

    $bankAccountHolder = rewrite($_POST['bank_account_holder']);
    $bankName = rewrite($_POST['bank_name']);
    $bankAccountNo = rewrite($_POST['bank_account_no']);

    if(!$bankAccountHolder || !$bankName || !$bankAccountNo){
      $bankAccountHolder = 'GIC Holding';
      $bankName = 'Public Bank Sdn Bhd';
      $bankAccountNo = 123456789;
    }



    // $remark = rewrite($_POST["remark"]);

    //   FOR DEBUGGING
    // echo $id;
    // echo $unitNo;
    // echo $purchaserName;
    // echo $ic;
    // echo $contact;
    // echo $email;

    //problem
    // $loanStatus = getLoanStatus($conn, "WHERE loan_uid=?",array("loan_uid"),array($loanUid), "s");
    // $CurrentAmountBal = $loanStatus[0]->getTotalBalUnclaimAmt();
    // $CurrentAmountClaim = $loanStatus[0]->getTotalClaimDevAmt();

    // $totalAmountInvoice = $amount + $amount2 + $amount3 + $amount4 + $amount5;

  if (isset($_POST['upload'])) {

    for ($cnt=0; $cnt <count($unit) ; $cnt++) {
      $unit[$cnt];
      $amount[$cnt];
      // $item[$cnt];

          $loanDetailsUnit1 = getLoanStatus($conn, "WHERE unit_no =? ", array("unit_no"), array($unit[$cnt]), "s");

    if($loanDetailsUnit1){
    $loanUnit1 = $loanDetailsUnit1[0]->getLoanUid();
    $chargeAmount = $amount[$cnt] * $chargesNew;
    $finalAmount = $chargeAmount + $amount[$cnt];

    if ($cnt == 0) {
      $finalAmount0 = $finalAmount;
    }if ($cnt == 1) {
      $finalAmount1 = $finalAmount;
    }if ($cnt == 2) {
      $finalAmount2 = $finalAmount;
    }if ($cnt == 3) {
      $finalAmount3 = $finalAmount;
    }if ($cnt == 4) {
      $finalAmount4 = $finalAmount;
    }

      $date = date('d-m-Y');
        $totalAmountBal = $loanDetailsUnit1[0]->getTotalBalUnclaimAmt() - $amount[$cnt];
        $totalAmountClaim = $loanDetailsUnit1[0]->getTotalClaimDevAmt() + $amount[$cnt];
        if (!$loanDetailsUnit1[0]->getClaimAmt1st() && !$loanDetailsUnit1[0]->getClaimAmt2nd() && !$loanDetailsUnit1[0]->getClaimAmt3rd() && !$loanDetailsUnit1[0]->getClaimAmt4th() && !$loanDetailsUnit1[0]->getClaimAmt5th() && $projectClaims >=1) {
        $statusOfClaims = 1;
        // if ($cnt == 0) {
        //   $statusOfClaims0 = $statusOfClaims;
        // }if ($cnt == 1) {
        //   $statusOfClaims1 = $statusOfClaims;
        // }if ($cnt == 2) {
        //   $statusOfClaims2 = $statusOfClaims;
        // }if ($cnt == 3) {
        //   $statusOfClaims3 = $statusOfClaims;
        // }if ($cnt == 4) {
        //   $statusOfClaims4 = $statusOfClaims;
        // }
        }
        if ($loanDetailsUnit1[0]->getClaimAmt1st() && !$loanDetailsUnit1[0]->getClaimAmt2nd() && !$loanDetailsUnit1[0]->getClaimAmt3rd() && !$loanDetailsUnit1[0]->getClaimAmt4th() && !$loanDetailsUnit1[0]->getClaimAmt5th() && $projectClaims >=2) {
        $statusOfClaims = 2;
        }
        if ($loanDetailsUnit1[0]->getClaimAmt1st() && $loanDetailsUnit1[0]->getClaimAmt2nd() && !$loanDetailsUnit1[0]->getClaimAmt3rd() && !$loanDetailsUnit1[0]->getClaimAmt4th() && !$loanDetailsUnit1[0]->getClaimAmt5th() && $projectClaims >=3) {
        $statusOfClaims = 3;
        }
        if ($loanDetailsUnit1[0]->getClaimAmt1st() && $loanDetailsUnit1[0]->getClaimAmt2nd() && $loanDetailsUnit1[0]->getClaimAmt3rd() && !$loanDetailsUnit1[0]->getClaimAmt4th() && !$loanDetailsUnit1[0]->getClaimAmt5th() && $projectClaims >=4) {
        $statusOfClaims = 4;
        }
        if ($loanDetailsUnit1[0]->getClaimAmt1st() && $loanDetailsUnit1[0]->getClaimAmt2nd() && $loanDetailsUnit1[0]->getClaimAmt3rd() && $loanDetailsUnit1[0]->getClaimAmt4th() && !$loanDetailsUnit1[0]->getClaimAmt5th() && $projectClaims >=5) {
        $statusOfClaims = 5;
        }

        $statusOfClaimsArray[] = $statusOfClaims;

        $tableName = array();
        $tableValue =  array();
        $stringType =  "";

        $otherClaimsFinal += $currentOtherClaims;
        // //echo "save to database";
        if($otherClaimsFinal)
        {
            array_push($tableName,"other_claim");
            array_push($tableValue,$otherClaimsFinal);
            $stringType .=  "s";

        }

        array_push($tableValue,$projectName);
        $stringType .=  "s";
        $withdrawUpdated = updateDynamicData($conn,"project"," WHERE project_name = ? ",$tableName,$tableValue,$stringType);
            if($withdrawUpdated)
            {

                // $_SESSION['messageType'] = 1;
                // header('Location: ../adminDashboard.php?type=1');
            }
            else
            {
                echo "fail";
            }


    }
    else
    {
        echo "dunno";
    }
  }
if ($finalAmount0 && !$finalAmount1 && !$finalAmount2 && !$finalAmount3 && !$finalAmount4) {
$finalAmountImplode = $finalAmount0;
}
elseif ($finalAmount0 && $finalAmount1 && !$finalAmount2 && !$finalAmount3 && !$finalAmount4) {
$finalAmountImplode = $finalAmount0.",".$finalAmount1;
}
elseif ($finalAmount0 && $finalAmount1 && $finalAmount2 && !$finalAmount3 && !$finalAmount4) {
$finalAmountImplode = $finalAmount0.",".$finalAmount1.",".$finalAmount2;
}
elseif ($finalAmount0 && $finalAmount1 && $finalAmount2 && $finalAmount3 && !$finalAmount4) {
$finalAmountImplode = $finalAmount0.",".$finalAmount1.",".$finalAmount2.",".$finalAmount3;
}
elseif ($finalAmount0 && $finalAmount1 && $finalAmount2 && $finalAmount3 && $finalAmount4) {
$finalAmountImplode = $finalAmount0.",".$finalAmount1.",".$finalAmount2.",".$finalAmount3.",".$finalAmount4;
}

$claimStatusImplode = implode(",",$statusOfClaimsArray);


  $remove = array(""); //want to remove null value 1,,3 to 1,3
  $resultAmount = array_diff($amount, $remove); //want to remove null value 1,,3 to 1,3
  $amountLimit = count($resultAmount); //want to remove null value 1,,3 to 1,3
  $amountSlice = array_slice($amount,0,$amountLimit); //want to remove null value 1,,3 to 1,3

  if(addNewInvoice($conn,$invoiceId,$detailsImplode,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountExp,$receiveStatus,$projectHandler,$branchType))
  {
    $column = "Issued";
    $a = "-";
    if(editHistory($conn, $username, $column, $invoiceId,$a, $a,$branchType))
         {}
      $_SESSION['messageType'] = 1;
      $_SESSION['invoice_id'] = $invoiceId;
      header('Location: ../issuePayrollSlip.php?type=7');
      //echo "register success";
  }else {
    $_SESSION['messageType'] = 1;
    header('Location: ../adminDashboard.php?type=8');
  }



}

//============================================================================================================================

if(isset($_POST['preview'])){
echo "sss";
}

//============================================================================================================================

  // }


}
else
{
    //  header('Location: ../index.php');
}
?>
