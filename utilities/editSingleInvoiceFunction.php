<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/../adminAccess1.php';
require_once dirname(__FILE__) . '/../timezone.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/Invoice.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter),
     "sssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

$totalUnclaimedBalance = 0;
$totalClaimedDevAmt = 0;

$username = $_SESSION['username'];
$invoiceId = rewrite($_POST['invoice_id']);
$attn = rewrite($_POST['attn']);
$requestDate = rewrite($_POST['request_date']);
$requestDate = $requestDate;
$bankName = rewrite($_POST['bank_name']);
$bankAccHolder = rewrite($_POST['bank_acc_holder']);
$bankAccNo = rewrite($_POST['bank_acc_no']);
$itemAmount = rewrite($_POST['amount']);
$charges = rewrite($_POST['charges']);
// $amount = str_replace(",","",$amount);
$itemAmount = str_replace(",","",$itemAmount);


$singleInvoiceDetails = getInvoice($conn, "WHERE invoice_id =? ",array("invoice_id"),array($invoiceId),"s");
$singleInvoiceDetails[0]->getInvoiceId();
$id = $singleInvoiceDetails[0]->getId();
$loanUid = $singleInvoiceDetails[0]->getLoanUid();
$claimsStatus = $singleInvoiceDetails[0]->getClaimsStatus();
$attnCurrent = $singleInvoiceDetails[0]->getProjectHandler();
$requestDateCurrent = date('Y-m-d',strtotime($singleInvoiceDetails[0]->getDateCreated()));
$bankNameCurrent = $singleInvoiceDetails[0]->getBankName();
$bankAccHolderCurrent = $singleInvoiceDetails[0]->getBankAccountHolder();
$bankAccNoCurrent = $singleInvoiceDetails[0]->getBankAccountNo();
$chargesCurrent = $singleInvoiceDetails[0]->getCharges();
$finalAmountCurrent = $singleInvoiceDetails[0]->getFinalAmount();
$amountCurrent = $singleInvoiceDetails[0]->getAmount();
$itemAmountCurrent = $singleInvoiceDetails[0]->getItem();
$statusOfClaims = $singleInvoiceDetails[0]->getClaimsStatus();

$loanDetails = getLoanStatus($conn, "WHERE loan_uid=?",array("loan_uid"),array($loanUid), "s");
// if ($loanDetails) {
  $totalUnclaimedBalance = $loanDetails[0]->getTotalBalUnclaimAmt();
  $totalClaimedDevAmt = $loanDetails[0]->getTotalClaimDevAmt();
// }

echo "CurrentAmount :".$itemAmountCurrent."<br>";
echo "New Change :".$itemAmount."<br>";
echo "Current Claimed :".$totalClaimedDevAmt."<br>";
echo "Current Unclaim :".$totalUnclaimedBalance."<br>";


if ($itemAmountCurrent > $itemAmount) {
  $difference = $itemAmountCurrent - $itemAmount;
  $totalClaimedDevAmt = $totalClaimedDevAmt - $difference;
  $totalUnclaimedBalance = $totalUnclaimedBalance + $difference;
}elseif ($itemAmount > $itemAmountCurrent) {
  $difference = $itemAmount - $itemAmountCurrent;
  $totalClaimedDevAmt = $totalClaimedDevAmt + $difference;
  $totalUnclaimedBalance = $totalUnclaimedBalance - $difference;
}else {
  $difference = 0;
  $totalClaimedDevAmt = $totalClaimedDevAmt;
  $totalUnclaimedBalance = $totalUnclaimedBalance;
}

if ($charges == 'NO') {
  $finalAmount = $itemAmount;
  $amount = $itemAmount - ( $itemAmount * 0.06);
}elseif ($charges == 'YES') {
  $finalAmount = $itemAmount * 1.06;
  $amount = $itemAmount;
}

$editDetails = "$attn,$requestDate,$bankName,$bankAccHolder,$bankAccNo,$finalAmount,$charges";
$editDetailsExp = explode(",",$editDetails);

$currentDetails = "$attnCurrent,$requestDateCurrent,$bankNameCurrent,$bankAccHolderCurrent,$bankAccNoCurrent,$finalAmountCurrent,$chargesCurrent";
$currentDetailsExp = explode(",",$currentDetails);

$columnDetails = "Attn,Date,Bank Name,Bank Account Holder,Account No.,Final Amount,Charges";
$columnDetailsExp = explode(",",$columnDetails);



if (isset($_POST['editBtn'])) {

for ($i=0; $i < count($editDetailsExp) ; $i++) {
  if ($editDetailsExp[$i] != $currentDetailsExp[$i]) {
    $column = $columnDetailsExp[$i];
    if(editHistory($conn, $username, $column, $invoiceId,$currentDetailsExp[$i], $editDetailsExp[$i]))
         {}
  }
}


  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  for ($i=1; $i < 6 ; $i++) {
if ($claimsStatus == $i) {
  if($requestDate)
  {
      array_push($tableName,"request_date".$i);
      array_push($tableValue,$requestDate);
      $stringType .=  "s";
  }
  if($charges)
  {
      array_push($tableName,"charges".$i);
      array_push($tableValue,$charges);
      $stringType .=  "s";
  }
}}
  array_push($tableValue,$loanUid);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE loan_uid = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
  }


  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($attn)
  {
      array_push($tableName,"project_handler");
      array_push($tableValue,$attn);
      $stringType .=  "s";
  }
  if($requestDate)
  {
      array_push($tableName,"booking_date");
      array_push($tableValue,$requestDate);
      $stringType .=  "s";
  }
  if($bankName)
  {
      array_push($tableName,"bank_name");
      array_push($tableValue,$bankName);
      $stringType .=  "s";
  }
  if($bankAccHolder)
  {
      array_push($tableName,"bank_account_holder");
      array_push($tableValue,$bankAccHolder);
      $stringType .=  "s";
  }
  if($bankAccNo)
  {
      array_push($tableName,"bank_account_no");
      array_push($tableValue,$bankAccNo);
      $stringType .=  "s";
  }
  if($finalAmount)
  {
      array_push($tableName,"final_amount");
      array_push($tableValue,$finalAmount);
      $stringType .=  "s";
  }
  if($amount)
  {
      array_push($tableName,"amount");
      array_push($tableValue,$amount);
      $stringType .=  "s";
  }
  if($itemAmount)
  {
      array_push($tableName,"item");
      array_push($tableValue,$itemAmount);
      $stringType .=  "s";
  }
  if($charges)
  {
      array_push($tableName,"charges");
      array_push($tableValue,$charges);
      $stringType .=  "s";
  }
  array_push($tableValue,$invoiceId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice"," WHERE invoice_id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    echo "Difference :".$difference."<br>";
    echo "total Claim :".$totalClaimedDevAmt."<br>";
    echo "total unClaim :".$totalUnclaimedBalance."<br>";

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($totalClaimedDevAmt || !$totalClaimedDevAmt)
    {
        array_push($tableName,"total_claimed_dev_amt");
        array_push($tableValue,$totalClaimedDevAmt);
        $stringType .=  "s";
    }
    if($totalUnclaimedBalance || !$totalUnclaimedBalance)
    {
        array_push($tableName,"total_bal_unclaim_amt");
        array_push($tableValue,$totalUnclaimedBalance);
        $stringType .=  "s";
    }
    if($statusOfClaims == 1)
    {
        array_push($tableName,"1st_claim_amt");
        array_push($tableValue,$itemAmount);
        $stringType .=  "i";
    }
    if($statusOfClaims == 2)
    {
        array_push($tableName,"2nd_claim_amt");
        array_push($tableValue,$itemAmount);
        $stringType .=  "i";
    }
    if($statusOfClaims == 3)
    {
        array_push($tableName,"3rd_claim_amt");
        array_push($tableValue,$itemAmount);
        $stringType .=  "i";
    }
    if($statusOfClaims == 4)
    {
        array_push($tableName,"4th_claim_amt");
        array_push($tableValue,$itemAmount);
        $stringType .=  "i";
    }
    if($statusOfClaims == 5)
    {
        array_push($tableName,"5th_claim_amt");
        array_push($tableValue,$itemAmount);
        $stringType .=  "i";
    }
    array_push($tableValue,$loanUid);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE loan_uid = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated){
      $_SESSION['messageType'] = 1;
      $_SESSION['invoice_id'] = $invoiceId;
      header('location: ../invoice.php?type=2');
    }

  }
}
 ?>
