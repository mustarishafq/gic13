<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$currentPassword = $_POST['currentPassword'];
$uid = $_SESSION['uid'];
$userDetails = getUser($conn, "WHERE uid = ?",array("uid"),array($uid), "s");


$tempPass = hash('sha256',$currentPassword);
$finalPassword = hash('sha256', $userDetails[0]->getSalt() . $tempPass);

if($finalPassword == $userDetails[0]->getPassword())
{
  $passwordSuccess = 1;
  $passwordSucceed[] = array("success" => $passwordSuccess);
}

echo json_encode($passwordSucceed);

 ?>
