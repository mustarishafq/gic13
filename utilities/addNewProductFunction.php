<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/../adminAccess1.php';
require_once dirname(__FILE__) . '/../timezone.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

// $commissionDetails = getAdvancedSlip($conn,"WHERE loan_uid = ? ", array("loan_uid"), array($loanUid), "s");

function addAdvancedSlip($conn, $id, $unitNo, $projectName, $bookingDate, $loanUid, $agent, $amount, $status, $receiveStatus,$agentIc,$paymentMethod,$advancedId,$agentDefault,$branchType)
{
     if(insertDynamicData($conn,"advance_slip", array("id", "unit_no", "project_name", "booking_date", "loan_uid","agent","amount","status", "receive_status","ic_no","payment_method","advance_id","agent_default","branch_type"),
     array($id, $unitNo,$projectName, $bookingDate, $loanUid, $agent, $amount, $status, $receiveStatus,$agentIc,$paymentMethod,$advancedId,$agentDefault,$branchType),
     "isssssdsssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter,$branchType)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after","branch_type"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter,$branchType),
     "ssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

function addNewProduct($conn, $id, $projectName, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package,
                         $discount, $rebate, $extraRebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus, $remark, $bFormCollected, $paymentMethod,
                         $lawyer, $bankApproved, $loSignedDate, $laSignedDate, $spaSignedDate, $fullsetCompleted, $cashBuyer, $cancelledBooking, $caseStatus,
                         $eventPersonal, $rate, $agentComm, $upline1, $upline2,$upline3, $upline4,$upline5, $plName, $hosName, $listerName, $ulOverride, $uulOverride,
                         $uuulOverride,$uuuulOverride,$uuuuulOverride,
                         $plOverride, $hosOverride, $listerOverride, $admin1Override, $admin2Override, $admin3Override, $gicProfit, $totalClaimedDevAmt, $totalBalUnclaimAmt, $loanUid,$loanAmount,$branchType,
                         $ul,$uul,$uuul,$uuuul,$uuuuul,$pl,$hos,$lister,$agentBalance)
{
     if(insertDynamicData($conn,"loan_status",
     array("id","project_name","unit_no","purchaser_name","ic","contact","email","booking_date","sq_ft","spa_price","package",
     "discount","rebate","extra_rebate","nettprice","totaldevelopercomm","agent","loanstatus","remark","bform_Collected","payment_method",
     "lawyer","bank_approved","lo_signed_date","la_signed_date","spa_signed_date","fullset_completed","cash_buyer","cancelled_booking","case_status",
     "event_personal","rate","agent_comm","upline1","upline2","upline3","upline4","upline5","pl_name","hos_name","lister_name","ul_override","uul_override","uuul_override","uuuul_override","uuuuul_override",
     "pl_override","hos_override","lister_override","admin1_override","admin2_override","admin3_override","gic_profit","total_claimed_dev_amt","total_bal_unclaim_amt","loan_uid","loan_amount","branch_type",
      "ul_balance","uul_balance","uuul_balance","uuuul_balance","uuuuul_balance","pl_balance","hos_balance","lister_balance","agent_balance"),
     array($id, $projectName, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package,
     $discount, $rebate, $extraRebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus, $remark, $bFormCollected, $paymentMethod,
     $lawyer, $bankApproved, $loSignedDate, $laSignedDate, $spaSignedDate, $fullsetCompleted, $cashBuyer, $cancelledBooking, $caseStatus,
     $eventPersonal, $rate, $agentComm, $upline1, $upline2,$upline3, $upline4,$upline5, $plName, $hosName, $listerName, $ulOverride, $uulOverride,
     $uuulOverride,$uuuulOverride,$uuuuulOverride,
     $plOverride, $hosOverride, $listerOverride, $admin1Override, $admin2Override, $admin3Override, $gicProfit, $totalClaimedDevAmt, $totalBalUnclaimAmt, $loanUid,$loanAmount,$branchType,
     $ul,$uul,$uuul,$uuuul,$uuuuul,$pl,$hos,$lister,$agentBalance),
     "sssssssssissssiissssssssssssssssissssssssiiiiiiiisssiiissssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $branchType = $_SESSION['branch_type'];
     // $id = rewrite($_POST["id"]);
     $advancedId = md5(uniqid());
     $amount = 2000; // advanced
     $totalClaimedDevAmt = 0;
     $status = 'PENDING';
     $receiveStatus = 'PENDING';
     $loanUid = md5(uniqid());
     $projectName = rewrite($_POST["project_name"]);
     $unitNo = rewrite($_POST["unit_no"]);
     $purchaserName = $_POST["purchaser_name"];
     $purchaserName = implode(",",$purchaserName);
     $ic = $_POST["ic"];
     $ic = implode(",",$ic);
     $contact = $_POST["contact"];
     $contact = implode(",",$contact);
     $email = $_POST["email"];
     $email = implode(",",$email);
     $bookingDate = rewrite($_POST["booking_date"]);
     $sqFt = rewrite($_POST["sq_ft"]);
     $paymentMethodAgentSlip = "Online Transfer";

     $str2 = rewrite($_POST["spa_price"]);
     $newSPAPrice = str_replace( ',', '', $str2);
     $spaPrice = $newSPAPrice;


     $package = rewrite($_POST["package"]);

     $discount = rewrite($_POST["discount"]);
     $rebate = rewrite($_POST["rebate"]);
     $extraRebate = rewrite($_POST["extra_rebate"]);

     //remove comma inside value
     $str1 = rewrite($_POST["nettprice"]);
     $newNettPrice = str_replace( ',', '', $str1);
     $nettPrice = $newNettPrice;
     $detailsBefore = "-";
     $detailsAfter = "-";

     $loanDetails = getLoanStatus($conn,"WHERE unit_no =? AND display ='Yes' AND cancelled_booking != 'YES'",array("unit_no"),array($unitNo), "s");

     //choose % or value
     $totalDC = rewrite($_POST["totaldevelopercomm"]);
     if (!$totalDC)
     {
          $totalDCP = rewrite($_POST["totaldevelopercommper"]);
          $tDCP = ($totalDCP / 100);
          $totalDeveloperComm = ($tDCP * $nettPrice);
     }
     else
     {
          $totalDCstr = rewrite($_POST["totaldevelopercomm"]);

          $totalDeveloperComm = $totalDCstr;
     }

     //get upline, up-upline
     $agent = rewrite($_POST["agent"]);

     $getUplineDetails = getUser($conn," WHERE full_name = ? ",array("full_name"),array($agent),"s");
     $agentUid = $getUplineDetails[0]->getUid();
     $agentReferralDetails = getReferralHistory($conn, "WHERE referral_id=?",array("referral_id"),array($agentUid), "s");
     $agentCurrentLevel = $agentReferralDetails[0]->getCurrentLevel();
     $agentFirstLevel = $agentCurrentLevel + 1;
     $uplineUidDetails = getTop10ReferrerOfUser($conn, $agentUid);
     if ($uplineUidDetails) {
       for ($i=0; $i <count($uplineUidDetails) ; $i++) {
         $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uplineUidDetails[$i]), "s");
         $fullName = $userDetails[0]->getFullName();
         $upline[] = $userDetails[0]->getFullName();

       }
     }
     $uplineImp = implode(",",$upline);
     $uplineExp = explode(",",$uplineImp);
     for ($i=0; $i <count($uplineExp) ; $i++) {
       if ($i == 0) {
         $upline1 = $uplineExp[$i];
       }else
       if ($i == 1){
         $upline2 = $uplineExp[$i];
       }else
       if ($i == 2){
         $upline3 = $uplineExp[$i];
       }else
       if ($i == 3){
         $upline4 = $uplineExp[$i];
       }else
       if ($i == 4){
         $upline5 = $uplineExp[$i];
       }
     }
     $agentIc = $getUplineDetails[0]->getIcNo();
     // $upline1 = $getUplineDetails[0]->getUpline1();
     if (!$upline1 || $upline1 == 'N/A' || $upline1 == 'Null' || $upline1 == 'null' || $upline1 == 'NULL') {
       $upline1Name = "";
     }else {
       $upline1Name = $upline1;
     }
     // $upline2 = $getUplineDetails[0]->getUpline2();
     if (!$upline2 || $upline2 == 'N/A' || $upline2 == 'Null' || $upline2 == 'null' || $upline2 == 'NULL') {
       $upline1Name = "";
     }else {
       $upline2Name = $upline2;
     }
     // $upline3 = $getUplineDetails[0]->getUpline3();
     if (!$upline3 || $upline3 == 'N/A' || $upline3 == 'Null' || $upline3 == 'null' || $upline3 == 'NULL') {
       $upline3Name = "";
     }else {
       $upline3Name = $upline3;
     }
     // $upline4 = $getUplineDetails[0]->getUpline4();
     if (!$upline4 || $upline4 == 'N/A' || $upline4 == 'Null' || $upline4 == 'null' || $upline4 == 'NULL') {
       $upline4Name = "";
     }else {
       $upline4Name = $upline4;
     }
     // $upline5 = $getUplineDetails[0]->getUpline5();
     if (!$upline5 || $upline5 == 'N/A' || $upline5 == 'Null' || $upline5 == 'null' || $upline5 == 'NULL') {
       $upline5Name = "";
     }else {
       $upline5Name = $upline5;
     }
     $agentFullName = $getUplineDetails[0]->getFullName();

     $loanStatus = rewrite($_POST["loanstatus"]);
     $remark = rewrite($_POST["remark"]);
     $bFormCollected = rewrite($_POST["bform_Collected"]);
     $paymentMethod = rewrite($_POST["payment_method"]);

     $lawyer = rewrite($_POST["lawyer"]);
     $bankApproved = rewrite($_POST["bank_approved"]);
     $loSignedDate = rewrite($_POST["lo_signed_date"]);
     $laSignedDate = rewrite($_POST["la_signed_date"]);
     $spaSignedDate = rewrite($_POST["spa_signed_date"]);
     $fullsetCompleted = rewrite($_POST["fullset_completed"]);

     //add cash buyer
     $cashBuyer = rewrite($_POST["cash_buyer"]);

     $cancelledBooking = rewrite($_POST["cancelled_booking"]);
     $caseStatus = rewrite($_POST["case_status"]);
     $loanAmountType = rewrite($_POST["loan_amount_type"]);
     $loanAmount = rewrite($_POST["loan_amount"]);

     if ($loanAmountType == '%') {
       $loanAmount = $loanAmount.$loanAmountType;
     }

     $eventPersonal = rewrite($_POST["event_personal"]);

     //get and update upline and upline
     $upline1 = $upline1Name;
     if (!$upline1) {
       $upline1 = "";
     }
     $upline2 = $upline2Name;
     if (!$upline2) {
       $upline2 = "";
     }
     $upline3 = $upline3Name;
     if (!$upline3) {
       $upline3 = "";
     }
     $upline4 = $upline4Name;
     if (!$upline4) {
       $upline4 = "";
     }
     $upline5 = $upline5Name;
     if (!$upline5) {
       $upline5 = "";
     }

     $plName = rewrite($_POST["pl_name"]);
     // $hosName = rewrite($_POST["hos_name"]);
     $hosName = 'Chng Zhi Fong';
     $listerName = rewrite($_POST["lister_name"]);

     $rate = rewrite($_POST["rate"]);
     if (!$rate)
     {
          $rate = "";
          $agentComm = rewrite($_POST["agent_comm"]);

          //auto generate
          if (!$upline1Name) {
            $ulOverride = 0;
          }else {
            $ulOverride = $agentComm * (5/100);
          }
          if (!$upline2Name) {
            $uulOverride = 0;
          }else {
            $uulOverride = $agentComm * (5/100);
          }
          if (!$upline3Name) {
            $uuulOverride = 0;
          }else {
            $uuulOverride = $agentComm * (3/100);
          }
          if (!$upline4Name) {
            $uuuulOverride = 0;
          }else {
            $uuuulOverride = $agentComm * (1/100);
          }
          if (!$upline5Name) {
            $uuuuulOverride = 0;
          }else {
            $uuuuulOverride = $agentComm * (1/100);
          }
          $plOverride = $agentComm * (15/100);
          $hosOverride = $agentComm * (5/100);

          $noRate = $agentComm + $ulOverride + $uulOverride + $uuulOverride + $uuuulOverride + $uuuuulOverride + $plOverride + $hosOverride;

          $listerOverride = rewrite($_POST["lister_override"]);
          // $admin1Override = rewrite($_POST["admin1_override"]);
          // $admin2Override = rewrite($_POST["admin2_override"]);
          // $admin3Override = rewrite($_POST["admin3_override"]);
          $admin1Override = 0;
          $admin2Override = 0;
          $admin3Override = 0;

          // $totalAdminOverride = $listerOverride + $admin1Override + $admin2Override + $admin3Override;
          $totalAdminOverride = $listerOverride;

          $gicProfit = $totalDeveloperComm - $noRate - $totalAdminOverride;

     }
     else
     {
          $ratePercentage =( $rate / 100 );
          $agentComm = $ratePercentage * $nettPrice;

          if (!$upline1Name) {
            $ulOverride = 0;
          }else {
            $ulOverride = $agentComm * (5/100);
          }
          if (!$upline2Name) {
            $uulOverride = 0;
          }else {
            $uulOverride = $agentComm * (5/100);
          }
          if (!$upline3Name) {
            $uuulOverride = 0;
          }else {
            $uuulOverride = $agentComm * (3/100);
          }
          if (!$upline4Name) {
            $uuuulOverride = 0;
          }else {
            $uuuulOverride = $agentComm * (1/100);
          }
          if (!$upline5Name) {
            $uuuuulOverride = 0;
          }else {
            $uuuuulOverride = $agentComm * (1/100);
          }
          $plOverride = $agentComm * (15/100);
          $hosOverride = $agentComm * (5/100);

          $withRate = $agentComm + $ulOverride + $uulOverride + $uuulOverride + $uuuulOverride + $uuuuulOverride + $plOverride + $hosOverride;

          $listerOverride = rewrite($_POST["lister_override"]);
          // $admin1Override = rewrite($_POST["admin1_override"]);
          // $admin2Override = rewrite($_POST["admin2_override"]);
          // $admin3Override = rewrite($_POST["admin3_override"]);
          $admin1Override = 0;
          $admin2Override = 0;
          $admin3Override = 0;

          // $totalAdminOverride = $listerOverride + $admin1Override + $admin2Override + $admin3Override;
          $totalAdminOverride = $listerOverride;

          $gicProfit = $totalDeveloperComm - $withRate - $totalAdminOverride;
     }

          $ul = $ulOverride.",0";
          $uul = $uulOverride.",0";
          $uuul = $uuulOverride.",0";
          $uuuul = $uuulOverride.",0";
          $uuuuul = $uuuuulOverride.",0";
          $pl = $plOverride.",0";
          $hos = $hosOverride.",0";
          $lister = $listerOverride.",0";
          $agentBalance = $agentComm.",0";
          $agentCaseStatusCompleted = $agentComm - 2000;
          $agentBalanceCompleted = $agentCaseStatusCompleted.",0";

    $totalBalUnclaimAmt = $totalDeveloperComm;
if (!$loanDetails) {  // if unit no not same can proceed
  if ($caseStatus == 'COMPLETED') {
    if(addNewProduct($conn, $id, $projectName, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package,
                             $discount, $rebate, $extraRebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus, $remark, $bFormCollected, $paymentMethod,
                             $lawyer, $bankApproved, $loSignedDate, $laSignedDate, $spaSignedDate, $fullsetCompleted, $cashBuyer, $cancelledBooking, $caseStatus,
                             $eventPersonal, $rate, $agentBalance, $upline1, $upline2,$upline3, $upline4,$upline5, $plName, $hosName, $listerName, $ulOverride, $uulOverride,$uuulOverride,$uuuulOverride,$uuuuulOverride,
                             $plOverride, $hosOverride, $listerOverride, $admin1Override, $admin2Override, $admin3Override, $gicProfit, $totalClaimedDevAmt, $totalBalUnclaimAmt, $loanUid,$loanAmount,$branchType,
                             $ul,$uul,$uuul,$uuuul,$uuuuul,$pl,$hos,$lister,$agentBalanceCompleted))
         {
           $username = $_SESSION['username'];
           $column = "Created";
           if(editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsBefore,$branchType))
                {}
                  if ($_SESSION['usertype_level'] == 1) {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../admin1Product.php?type=3');
                  }elseif ($_SESSION['usertype_level'] == 2) {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../admin2Product.php?type=1');
                  }
         }
    if(addAdvancedSlip($conn, $id, $unitNo, $projectName, $bookingDate, $loanUid, $agentFullName, $amount, $status, $receiveStatus,$agentIc,$paymentMethodAgentSlip,$advancedId,$agentFullName,$branchType))
         {
           $username = $_SESSION['username'];
           $column = "Issued Advance";
           if(editHistory($conn, $username, $column, $advancedId,$detailsBefore, $detailsBefore,$branchType))
                {}
                  if ($_SESSION['usertype_level'] == 1) {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../admin1Product.php?type=3');
                  }elseif ($_SESSION['usertype_level'] == 2) {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../admin2Product.php?type=1');
                  }
         }
  }else {
    if(addNewProduct($conn, $id, $projectName, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package,
                             $discount, $rebate, $extraRebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus, $remark, $bFormCollected, $paymentMethod,
                             $lawyer, $bankApproved, $loSignedDate, $laSignedDate, $spaSignedDate, $fullsetCompleted, $cashBuyer, $cancelledBooking, $caseStatus,
                             $eventPersonal, $rate, $agentComm, $upline1, $upline2,$upline3, $upline4,$upline5, $plName, $hosName, $listerName, $ulOverride, $uulOverride,$uuulOverride,$uuuulOverride,$uuuuulOverride,
                             $plOverride, $hosOverride, $listerOverride, $admin1Override, $admin2Override, $admin3Override, $gicProfit, $totalClaimedDevAmt, $totalBalUnclaimAmt, $loanUid,$loanAmount,$branchType,
                             $ul,$uul,$uuul,$uuuul,$uuuuul,$pl,$hos,$lister,$agentBalance))
         {
           $username = $_SESSION['username'];
           $column = "Created";
           if(editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsBefore,$branchType))
                {}
                  if ($_SESSION['usertype_level'] == 1) {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../admin1Product.php?type=3');
                  }elseif ($_SESSION['usertype_level'] == 2) {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../admin2Product.php?type=1');
                  }
         }
       }
  }else {
    $_SESSION['messageType'] = 1;
    header('Location: ../adminAddNewProduct.php?type=2');
  }
}
else
{
     header('Location: ../adminDashboard.php');
}
?>
