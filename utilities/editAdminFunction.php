<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $id = rewrite($_POST["id"]);
    $fullName = rewrite($_POST["full_name"]);
    $username = rewrite($_POST["username"]);
    $ic = rewrite($_POST["ic_no"]);
    $contact = rewrite($_POST["contact"]);
    $email = rewrite($_POST["email"]);
    $birthday = rewrite($_POST["birth_month"]);
    $status = rewrite($_POST["status"]);
    $branchType = rewrite($_POST['company_branch']);

  }

  if(isset($_POST['editSubmit']))
  {
      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      // //echo "save to database";
      if($fullName || !$fullName)
      {
          array_push($tableName,"full_name");
          array_push($tableValue,$fullName);
          $stringType .=  "s";
      }
      if($username)
      {
          array_push($tableName,"username");
          array_push($tableValue,$username);
          $stringType .=  "s";
      }
      if($ic || !$ic)
      {
          array_push($tableName,"ic");
          array_push($tableValue,$ic);
          $stringType .=  "s";
      }
      if($contact || !$contact)
      {
          array_push($tableName,"contact");
          array_push($tableValue,$contact);
          $stringType .=  "s";
      }
      if($email || !$email)
      {
          array_push($tableName,"email");
          array_push($tableValue,$email);
          $stringType .=  "s";
      }
      if($birthday || !$birthday)
      {
          array_push($tableName,"birth_month");
          array_push($tableValue,$birthday);
          $stringType .=  "s";
      }
      if($branchType || !$branchType)
      {
          array_push($tableName,"branch_type");
          array_push($tableValue,$branchType);
          $stringType .=  "s";
      }
    }
      array_push($tableValue,$id);
      $stringType .=  "i";
      $withdrawUpdated = updateDynamicData($conn,"user"," WHERE id = ? ",$tableName,$tableValue,$stringType);

      if($withdrawUpdated)
      {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminInfo.php?type=2');
      }


 ?>
