<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$branchType = rewrite($_POST['branch']);
$companyName = $_POST['companyName'];
$companyBranch = $_POST['companyBranch'];
$companyAddress = $_POST['companyAddress'];
$addressNo = $_POST['addressNo'];
$contact = $_POST['contact'];
$success = 0;



// If upload button is clicked ...
  if (isset($_POST['upload'])) {
  	// Get image name
  	$logo = $_FILES['logo']['name'];
  	// Get text
  	// $image_text = mysqli_real_escape_string($conn, $_POST['image_text']);

  	// image file directory
  	$target = "../logo/".basename($logo);

  	if (move_uploaded_file($_FILES['logo']['tmp_name'], $target)) {
  		$msg = "Image uploaded successfully";
  	}else{
  		$msg = "Failed to upload image";
  	}
  }
// $companyName = "haha";
// $companyAddress = "haha";
// $addressNo = "haha";
// $contact = "haha";

$tableName = array();
$tableValue =  array();
$stringType =  "";
// //echo "save to database";
if($companyName)
{
    array_push($tableName,"company_name");
    array_push($tableValue,$companyName);
    $stringType .=  "s";
}
if($companyBranch || !$companyBranch)
{
    array_push($tableName,"company_name");
    array_push($tableValue,$companyBranch);
    $stringType .=  "s";
}
if($companyAddress)
{
    array_push($tableName,"company_address");
    array_push($tableValue,$companyAddress);
    $stringType .=  "s";
}
if($addressNo)
{
    array_push($tableName,"address_no");
    array_push($tableValue,$addressNo);
    $stringType .=  "s";
}
if($contact)
{
    array_push($tableName,"contact");
    array_push($tableValue,$contact);
    $stringType .=  "s";
}
if($logo)
{
    array_push($tableName,"logo");
    array_push($tableValue,$logo);
    $stringType .=  "s";
}
array_push($tableValue,$branchType);
$stringType .=  "i";
$withdrawUpdated = updateDynamicData($conn,"address"," WHERE branch_type = ? ",$tableName,$tableValue,$stringType);

if($withdrawUpdated)
{
    // $success = 1;
    $_SESSION['messageType'] = 1;
    header('location: ../editSlipAddress.php?type=1');
}

// if ($success) {
//   $successArray[] = array("success" => $success);
// }

// echo json_encode($successArray);
 ?>
