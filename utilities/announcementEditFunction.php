<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Announcement.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

if (isset($_POST['announcement'])) {

  $id = rewrite($_POST['id']);

  $announcementDetails = getAnnouncement($conn, "WHERE id =?",array("id"),array($id), "s");

  $branchType = $_SESSION['branch_type'];
  $createdBy = $_SESSION['username'];
  $projectName = rewrite($_POST['project_name']);
  $projectLeader = rewrite($_POST['project_leader']);
  $commission = rewrite($_POST['commission']);
  $packages = rewrite($_POST['packages']);
  $videoLink = rewrite($_POST['video_link']);

// for ($i=0; $i <count($fileUploadExp) ; $i++) {

  if ($_FILES['present_file']['name']) {
    $presentFile = $_FILES['present_file']['name'];
    $target_dir = "../announcement/";
    $target_file = $target_dir . basename($_FILES['present_file']["name"]);
    //$image_text = mysqli_real_escape_string($conn, $_POST['number']);


    // Select file type
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    // Valid file extensions
    $extensions_arr = ['pdf', 'txt', 'doc', 'docx', 'png', 'jpg', 'jpeg',  'gif'];

    if( in_array($imageFileType,$extensions_arr) ){


     move_uploaded_file($_FILES['present_file']['tmp_name'],$target_dir.$presentFile);
   }
 }else {
   $presentFile = null;
 }
 if ($_FILES['booking_form']['name']) {
   $bookingForm = $_FILES['booking_form']['name'];
   $target_dir = "../announcement/";
   $target_file = $target_dir . basename($_FILES['booking_form']["name"]);
   //$image_text = mysqli_real_escape_string($conn, $_POST['number']);


   // Select file type
   $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

   // Valid file extensions
   $extensions_arr = ['pdf', 'txt', 'doc', 'docx', 'png', 'jpg', 'jpeg',  'gif'];

   if( in_array($imageFileType,$extensions_arr) ){


    move_uploaded_file($_FILES['booking_form']['tmp_name'],$target_dir.$bookingForm);
  }
 }else {
   $bookingForm = null;
 }
if ($_FILES['broshure']['name']) {
  $eBroshure = $_FILES['broshure']['name'];
  $target_dir = "../announcement/";
  $target_file = $target_dir . basename($_FILES['broshure']["name"]);
  //$image_text = mysqli_real_escape_string($conn, $_POST['number']);


  // Select file type
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  // Valid file extensions
  $extensions_arr = ['pdf', 'txt', 'doc', 'docx', 'png', 'jpg', 'jpeg',  'gif'];

  if( in_array($imageFileType,$extensions_arr) ){


   move_uploaded_file($_FILES['broshure']['tmp_name'],$target_dir.$eBroshure);
  }
}else {
  $eBroshure = null;
}

// }
$announcementUpdated = updateDynamicData($conn, "announcement", "WHERE id=?", array("project_name","pl_name","commission","packages","present_file","broshure","booking_form","video_link","createdBy","branch_type"),
                                          array($projectName,$projectLeader,$commission,$packages,$presentFile,$eBroshure,$bookingForm,$videoLink,$createdBy,$branchType,$id), "sssssssssss");
// $announcementUpdated = updateDynamicData($conn, "announcement","WHERE id=?", array("project_name","pl_name","commission","packages","video_link","createdBy","branch_type"),
//                                           array($projectName,$projectLeader,$commission,$packages,$videoLink,$createdBy,$branchType,$id), "ssssssss");

if ($announcementUpdated) {
  $_SESSION['messageType'] = 1;
  header('location: ../announcementCurrent.php?type=2');
}


}
if (isset($_POST['deleteId'])) {
  $id = $_POST['deleteId'];
  $deleteItem = $_POST['deleteItem'];
  $empty = null;

  if ($deleteItem == 'broshure') {
    $deleteBroshure = updateDynamicData($conn, "announcement", "WHERE id=?", array("broshure"),
                                              array($empty,$id), "ss");
  }else
  if ($deleteItem == 'booking') {
    $deleteBroshure = updateDynamicData($conn, "announcement", "WHERE id=?", array("booking_form"),
                                              array($empty,$id), "ss");
  }else
  if ($deleteItem == 'present') {
    $deleteBroshure = updateDynamicData($conn, "announcement", "WHERE id=?", array("present_file"),
                                              array($empty,$id), "ss");
  }
}
 ?>
