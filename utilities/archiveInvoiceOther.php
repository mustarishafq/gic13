<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/OtherInvoice.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function editHistory($conn, $username, $column, $invoiceId,$detailsBefore, $detailsAfter,$branchType)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after","branch_type"),
     array($username, $column, $invoiceId,$detailsBefore,$detailsAfter,$branchType),
     "ssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

$invoiceId = $_POST['invoiceID'];
// $invoiceId = 'a461c1691fe1b1ca52f5f58a8006ce96';
$username = $_SESSION['username'];
$detailsBefore = "-";
$detailsAfter = "-";
$branchType = $_SESSION['branch_type'];


$invoiceotherDetails = getOtherInvoice($conn, "WHERE invoice_id = ?",array("invoice_id"),array($invoiceId), "s");
$unitNo = $invoiceotherDetails[0]->getUnitNo();
$currentDisplay = $invoiceotherDetails[0]->getDisplay();
if ($currentDisplay == 'Yes') {
  $display = "No";
  $column = "Archived";
}elseif ($currentDisplay == 'No') {
  $display = "Yes";
  $column = "Un-Archived";
}


$tableName = array();
$tableValue =  array();
$stringType =  "";
// //echo "save to database";
if($invoiceotherDetails)
{
    array_push($tableName,"display");
    array_push($tableValue,$display);
    $stringType .=  "s";
}
array_push($tableValue,$invoiceId);
$stringType .=  "s";
$withdrawUpdated = updateDynamicData($conn,"invoice_other"," WHERE invoice_id = ? ",$tableName,$tableValue,$stringType);
    if($withdrawUpdated)
    {
      if(editHistory($conn, $username, $column, $invoiceId,$detailsBefore, $detailsAfter,$branchType))
           {}
        // $_SESSION['messageType'] = 1;
        // header('Location: ../adminWithdrawal.php?type=1');
    }
    else
    {
        // echo "fail";
    }

    $project[] = array("unit_no" => $unitNo);

    echo json_encode($project);
 ?>
