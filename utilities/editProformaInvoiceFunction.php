<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/../adminAccess1.php';
require_once dirname(__FILE__) . '/../timezone.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/ProformaInvoice.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter),
     "sssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
$username = $_SESSION['username'];
$proformaId = rewrite($_POST['proforma_id']);
$attn = rewrite($_POST['attn']);
$requestDate = rewrite($_POST['request_date']);
$requestDate = date('Y-m-d',strtotime($requestDate));
$bankName = rewrite($_POST['bank_name']);
$bankAccHolder = rewrite($_POST['bank_acc_holder']);
$bankAccNo = rewrite($_POST['bank_acc_no']);
$itemAmount = $_POST['amount'];
$charges = rewrite($_POST['charges']);

$itemAmountImpp = implode(";",$itemAmount);
$itemAmountImppRem = str_replace(",","",$itemAmountImpp);
$itemAmountImp = str_replace(";",",",$itemAmountImppRem);
$itemAmountExp = explode(",",$itemAmountImp);



$proformaInvoiceDetails = getProformaInvoice($conn, "WHERE proforma_id =? ",array("proforma_id"),array($proformaId),"s");
$proformaInvoiceDetails[0]->getProformaId();
$id = $proformaInvoiceDetails[0]->getId();
$attnCurrent = $proformaInvoiceDetails[0]->getProjectHandler();
$requestDateCurrent = date('Y-m-d',strtotime($proformaInvoiceDetails[0]->getBookingDate()));
$bankNameCurrent = $proformaInvoiceDetails[0]->getBankName();
$bankAccHolderCurrent = $proformaInvoiceDetails[0]->getBankAccountHolder();
$bankAccNoCurrent = $proformaInvoiceDetails[0]->getBankAccountNo();
$chargesCurrent = $proformaInvoiceDetails[0]->getCharges();
// $finalAmountCurrent = $proformaInvoiceDetails[0]->getFinalAmount();
// $amountCurrent = $proformaInvoiceDetails[0]->getAmount();
if ($itemAmountExp) {
  for ($i=0; $i <count($itemAmountExp) ; $i++) {
    if ($charges == 'YES') {
      $finalAmount[] = $itemAmountExp[$i] * 1.06;
      $amount[] = $itemAmountExp[$i];
    }
    if ($charges == 'NO'){
      $finalAmount[] = $itemAmountExp[$i];
      $amount[] = $itemAmountExp[$i] - $itemAmountExp[$i]*(6/100);
    }
  }
}
$finalAmount = implode(",",$finalAmount);
$amount = implode(",",$amount);
echo "Amount :".$amount."<br>";
echo "final Amount :".$finalAmount."<br>";
echo "Item :".$itemAmountImp."<br>";

$editDetails = "$attn,$requestDate,$bankName,$bankAccHolder,$bankAccNo,$charges";
// $editDetails = "$attn,$requestDate,$bankName,$bankAccHolder,$bankAccNo,$finalAmount";
$editDetailsExp = explode(",",$editDetails);

$currentDetails = "$attnCurrent,$requestDateCurrent,$bankNameCurrent,$bankAccHolderCurrent,$bankAccNoCurrent,$chargesCurrent";
// $currentDetails = "$attnCurrent,$requestDateCurrent,$bankNameCurrent,$bankAccHolderCurrent,$bankAccNoCurrent,$finalAmountCurrent";
$currentDetailsExp = explode(",",$currentDetails);

$columnDetails = "Attn,Date,Bank Name,Bank Account Holder,Account No.,Charges";
// $columnDetails = "Attn,Date,Bank Name,Bank Account Holder,Account No.,Final Amount";
$columnDetailsExp = explode(",",$columnDetails);



if (isset($_POST['editBtn'])) {

for ($i=0; $i < count($editDetailsExp) ; $i++) {
  if ($editDetailsExp[$i] != $currentDetailsExp[$i]) {
    $column = $columnDetailsExp[$i];
    if(editHistory($conn, $username, $column, $proformaId,$currentDetailsExp[$i], $editDetailsExp[$i]))
         {}
  }
}

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($attn)
  {
      array_push($tableName,"project_handler");
      array_push($tableValue,$attn);
      $stringType .=  "s";
  }
  if($requestDate)
  {
      array_push($tableName,"booking_date");
      array_push($tableValue,$requestDate);
      $stringType .=  "s";
  }
  if($bankName)
  {
      array_push($tableName,"bank_name");
      array_push($tableValue,$bankName);
      $stringType .=  "s";
  }
  if($bankAccHolder)
  {
      array_push($tableName,"bank_account_holder");
      array_push($tableValue,$bankAccHolder);
      $stringType .=  "s";
  }
  if($bankAccNo)
  {
      array_push($tableName,"bank_account_no");
      array_push($tableValue,$bankAccNo);
      $stringType .=  "s";
  }
  if($finalAmount)
  {
      array_push($tableName,"final_amount");
      array_push($tableValue,$finalAmount);
      $stringType .=  "s";
  }
  if($amount)
  {
      array_push($tableName,"amount");
      array_push($tableValue,$amount);
      $stringType .=  "s";
  }
  if($itemAmountImp)
  {
      array_push($tableName,"item");
      array_push($tableValue,$itemAmountImp);
      $stringType .=  "s";
  }
  if($charges)
  {
      array_push($tableName,"charges");
      array_push($tableValue,$charges);
      $stringType .=  "s";
  }
  array_push($tableValue,$proformaId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice_proforma"," WHERE proforma_id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    $_SESSION['messageType'] = 1;
    $_SESSION['idPro'] = $id;
    header('location: ../invoiceProforma.php?type=1');
  }
}
 ?>
