<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$loanUid = $_POST['loanUid'];

$loanDetails = getLoanStatus($conn,"WHERE loan_uid =?", array("loan_uid"), array($loanUid), "s");
if ($loanDetails) {
    $sst1 = $loanDetails[0]->getSst1();
    $sst2 = $loanDetails[0]->getSst2();
    $sst3 = $loanDetails[0]->getSst3();
    $sst4 = $loanDetails[0]->getSst4();
    $sst5 = $loanDetails[0]->getSst5();
  }
    if ($sst1 == 'YES' || $sst2 == 'YES' || $sst3 == 'YES' || $sst4 == 'YES' || $sst5== 'YES' ) {
      $charges = 'Yes';
    }elseif ($sst1 == 'NO' || $sst2 == 'NO' || $sst3 == 'NO' || $sst4 == 'NO' || $sst5== 'NO' ) {
      $charges = 'No';
    }else {
      $charges = null;
    }

    if ($charges) {
      $f[] = array("charges" => $charges);
    }

    // encoding array to json format
    echo json_encode($f);
 ?>
