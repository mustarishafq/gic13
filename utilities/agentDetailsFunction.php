<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();
$_SESSION['usernameNew'] = null;
$agentName = ($_POST['agentNames']);
// $agentName= "Tan Ai Pheng";

$userDetails = getUser($conn,"WHERE full_name = ?",array("full_name"),array($agentName), "s");

if ($userDetails) {
  $userName = $userDetails[0]->getUsername();
  $_SESSION['usernameNew'] = $userName;
  $userArray[] = array("agentNamed" => $userName);
}

echo json_encode($userArray);

 ?>
