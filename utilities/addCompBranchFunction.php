<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Branch.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$branch = getBranch($conn, "ORDER BY `branch_type` DESC LIMIT 1");
$currentBranchType = $branch[0]->getBranchType();
$branchType = $currentBranchType + 1;

function addNewCompanyBranch($conn,$branchName,$branchType,$createdBy)
{
     if(insertDynamicData($conn,"branch",array("branch_name","branch_type","creator"),
     array($branchName,$branchType,$createdBy),
     "sss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }
     return true;
}

function addNewCompanyAddress($conn,$logo,$companyName,$addressNo,$companyBranch,$companyAddress,$contact,$branchType)
{
     if(insertDynamicData($conn,"address",array("logo","company_name","address_no","company_branch","company_address","contact","branch_type"),
     array($logo,$companyName,$addressNo,$companyBranch,$companyAddress,$contact,$branchType),
     "sssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }
     return true;
}

$createdBy = $_SESSION['username'];
$branchName = rewrite($_POST['companyName']);
$companyName = rewrite($_POST['companyName']);
$addressNo = rewrite($_POST['sst_no']);
$companyBranch = rewrite($_POST['company_branch']);
$companyAddress = rewrite($_POST['address']);
$email = rewrite($_POST['email']);
$contact = rewrite($_POST['contact']);
$contactEmail = "Email: ".$email."  Tel:".$contact;

if (isset($_POST['upload'])) {

// for ($i=0; $i <count($fileUploadExp) ; $i++) {

  $logo = $_FILES['logo']['name'];
  $target_dir = "../logo/";
  $target_file = $target_dir . basename($_FILES['logo']["name"]);
  //$image_text = mysqli_real_escape_string($conn, $_POST['number']);


  // Select file type
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  // Valid file extensions
  $extensions_arr = ['png', 'jpg', 'jpeg',  'gif'];

  if( in_array($imageFileType,$extensions_arr) ){


   move_uploaded_file($_FILES['logo']['tmp_name'],$target_dir.$logo);
 }

  if (addNewCompanyBranch($conn,$branchName,$branchType,$createdBy)) {

  }
  if (addNewCompanyAddress($conn,$logo,$companyName,$addressNo,$companyBranch,$companyAddress,$contactEmail,$branchType)) {
    $_SESSION['messageType'] = 1;
    header('Location: ../addNewCompanyBranch.php?type=1');
  }


}
 ?>
