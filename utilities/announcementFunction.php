<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Announcement.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function addNewAnnouncement($conn,$projectName,$projectLeader,$commission,$packages,$presentFile,$eBroshure,$bookingForm,$videoLink,$createdBy,$branchType)
{
     if(insertDynamicData($conn,"announcement",array("project_name","pl_name","commission","packages","present_file","broshure","booking_form","video_link","createdBy","branch_type"),
     array($projectName,$projectLeader,$commission,$packages,$presentFile,$eBroshure,$bookingForm,$videoLink,$createdBy,$branchType),
     "ssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }
     return true;
}

$branchType = $_SESSION['branch_type'];
$createdBy = $_SESSION['username'];
$projectName = rewrite($_POST['project_name']);
$projectLeader = rewrite($_POST['project_leader']);
$commission = rewrite($_POST['commission']);
$packages = rewrite($_POST['packages']);
// $presentFile = rewrite($_POST['present_file']);
// $eBroshure = "e";
// $bookingForm = "bb";
// $presentFile = "pp";
// $eBroshure = rewrite($_POST['broshure']);
// $bookingForm = rewrite($_POST['booking_form']);
$videoLink = rewrite($_POST['video_link']);

if (isset($_POST['announcement'])) {

// for ($i=0; $i <count($fileUploadExp) ; $i++) {

  $presentFile = $_FILES['present_file']['name'];
  $target_dir = "../announcement/";
  $target_file = $target_dir . basename($_FILES['present_file']["name"]);
  //$image_text = mysqli_real_escape_string($conn, $_POST['number']);


  // Select file type
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  // Valid file extensions
  $extensions_arr = ['pdf', 'txt', 'doc', 'docx', 'png', 'jpg', 'jpeg',  'gif'];

  if( in_array($imageFileType,$extensions_arr) ){


   move_uploaded_file($_FILES['present_file']['tmp_name'],$target_dir.$presentFile);
 }
 $bookingForm = $_FILES['booking_form']['name'];
 $target_dir = "../announcement/";
 $target_file = $target_dir . basename($_FILES['booking_form']["name"]);
 //$image_text = mysqli_real_escape_string($conn, $_POST['number']);


 // Select file type
 $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

 // Valid file extensions
 $extensions_arr = ['pdf', 'txt', 'doc', 'docx', 'png', 'jpg', 'jpeg',  'gif'];

 if( in_array($imageFileType,$extensions_arr) ){


  move_uploaded_file($_FILES['booking_form']['tmp_name'],$target_dir.$bookingForm);
}
$eBroshure = $_FILES['broshure']['name'];
$target_dir = "../announcement/";
$target_file = $target_dir . basename($_FILES['broshure']["name"]);
//$image_text = mysqli_real_escape_string($conn, $_POST['number']);


// Select file type
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Valid file extensions
$extensions_arr = ['pdf', 'txt', 'doc', 'docx', 'png', 'jpg', 'jpeg',  'gif'];

if( in_array($imageFileType,$extensions_arr) ){


 move_uploaded_file($_FILES['broshure']['tmp_name'],$target_dir.$eBroshure);
}

// }

  if (addNewAnnouncement($conn,$projectName,$projectLeader,$commission,$packages,$presentFile,$eBroshure,$bookingForm,$videoLink,$createdBy,$branchType)) {
    $_SESSION['messageType'] = 1;
    header('Location: ../announcement.php?type=1');
  }


}
 ?>
