<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();
$id = rewrite($_POST['id']);
$idExplode = explode(",",$id);
$status = 'COMPLETED';
$claimType = $_POST['claim_type'];
$claimId = $_POST['claim_id'];
for ($cnt=0; $cnt <count($idExplode) ; $cnt++) {
if(isset($_POST['sendToAgent']))
{
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($status)
    {
        array_push($tableName,"receive_status");
        array_push($tableValue,$status);
        $stringType .=  "s";
    }
    if($claimType)
    {
        array_push($tableName,"claim_type");
        array_push($tableValue,$claimType);
        $stringType .=  "s";
    }
    if($claimId)
    {
        array_push($tableName,"claim_id");
        array_push($tableValue,$claimId);
        $stringType .=  "s";
    }
  }
  array_push($tableValue,$idExplode[$cnt]);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"commission"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    header('Location: ../adminCommission.php?type=1');
  }
}
 ?>
