<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {

      if (isset($_POST['agentSettings'])) {

        $id = rewrite($_POST['id']);

        $userDetails = getUser($conn,"WHERE id = ? ",array("id"),array($id),"s");
        $salt = $userDetails[0]->getSalt();
        $userPassword = $userDetails[0]->getPassword();

        $currentPassword = rewrite($_POST['password']);
        $newPassword = rewrite($_POST['new_password']);
        $confirmPassword = rewrite($_POST['confirm_password']);

        $tempPass = hash('sha256',$currentPassword);
        $finalPassword = hash('sha256', $salt . $tempPass);

        //   FOR DEBUGGING
        // echo "<br>";
        // echo $fullname."<br>";
        // echo $register_email."<br>";
        // echo $register_contact."<br>";

        if($finalPassword == $userPassword)
        {

          if ($newPassword && $confirmPassword && $newPassword == $confirmPassword) {
            $password = hash('sha256',$newPassword);
            $salt = substr(sha1(mt_rand()), 0, 100);
            $finalPasswordNew = hash('sha256', $salt.$password); // salt combine with password that being hash
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE id = ? ",array("password","salt"),array($finalPasswordNew,$salt,$id),"sss");
          }

        $user = getUser($conn," id = ?   ",array("id"),array($uid),"s");

        if(!$user)
        {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";

            if($salt)
            {
                array_push($tableName,"salt");
                array_push($tableValue,$salt);
                $stringType .=  "s";
            }
            if($finalPasswordNew)
            {
                array_push($tableName,"password");
                array_push($tableValue,$finalPasswordNew);
                $stringType .=  "s";
            }
            array_push($tableValue,$id);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                // echo "success";
                $_SESSION['messageType'] = 1;
                // $_SESSION['username'] = $username;
                header('Location: ../agentDashboard.php?type=2');
            }
            else
            {
                // echo "fail";
                // $_SESSION['messageType'] = 1;
                // header('Location: ../editProfile.php?type=2');
            }
        }
        else
        {}

    }else {
      $_SESSION['messageType'] = 1;
      header('Location: ../agentSettings.php?type=2');
    }

      }else {

        $id = rewrite($_POST['id']);

        $userDetails = getUser($conn,"WHERE id = ? ",array("id"),array($id),"s");
        $salt = $userDetails[0]->getSalt();
        $userPassword = $userDetails[0]->getPassword();

        $fullname = rewrite($_POST["full_name"]);
        $username = rewrite($_POST["username"]);
        $icNo = rewrite($_POST["ic_no"]);
        $phoneNo = rewrite($_POST["contact"]);
        $update_email = rewrite($_POST['email']);
        $currentPassword = rewrite($_POST['password']);
        $newPassword = rewrite($_POST['new_password']);
        $confirmPassword = rewrite($_POST['confirm_password']);

        $tempPass = hash('sha256',$currentPassword);
        $finalPassword = hash('sha256', $salt . $tempPass);

        //   FOR DEBUGGING
        // echo "<br>";
        // echo $fullname."<br>";
        // echo $register_email."<br>";
        // echo $register_contact."<br>";

        if($finalPassword == $userPassword)
        {

          if ($newPassword && $confirmPassword && $newPassword == $confirmPassword) {
            $password = hash('sha256',$newPassword);
            $salt = substr(sha1(mt_rand()), 0, 100);
            $finalPasswordNew = hash('sha256', $salt.$password); // salt combine with password that being hash
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE id = ? ",array("password","salt"),array($finalPasswordNew,$salt,$id),"sss");
          }

        $user = getUser($conn," id = ?   ",array("id"),array($uid),"s");

        if(!$user)
        {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";

            if($fullname)
            {
                array_push($tableName,"full_name");
                array_push($tableValue,$fullname);
                $stringType .=  "s";
            }
            if($username)
            {
                array_push($tableName,"username");
                array_push($tableValue,$username);
                $stringType .=  "s";
            }
            if($icNo)
            {
                array_push($tableName,"ic");
                array_push($tableValue,$icNo);
                $stringType .=  "s";
            }
            if($phoneNo)
            {
                array_push($tableName,"contact");
                array_push($tableValue,$phoneNo);
                $stringType .=  "s";
            }
            if($salt)
            {
                array_push($tableName,"salt");
                array_push($tableValue,$salt);
                $stringType .=  "s";
            }
            if($finalPasswordNew)
            {
                array_push($tableName,"password");
                array_push($tableValue,$finalPasswordNew);
                $stringType .=  "s";
            }
            if($update_email)
            {
                array_push($tableName,"email");
                array_push($tableValue,$update_email);
                $stringType .=  "s";
            }
            array_push($tableValue,$id);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                // echo "success";
                $_SESSION['messageType'] = 1;
                $_SESSION['username'] = $username;
                header('Location: ../adminDashboard.php?type=5');
            }
            else
            {
                // echo "fail";
                if ($_SESSION['branch_type'] == 1) {
                  $_SESSION['messageType'] = 1;
                  header('Location: ../settings.php?type=2');
                }else {
                  $_SESSION['messageType'] = 1;
                  header('Location: ../summertonSettings.php?type=2');
                }
            }
        }
        else
        {
            //echo "";
            $_SESSION['messageType'] = 1;
            header('Location: ../editProfile.php?type=1');
        }

    }else {
      $_SESSION['messageType'] = 1;
      header('Location: ../settings.php?type=2');
    }

      }

    }
else
{
    //header('Location: ../editprofile.php');
    header('Location: ../index.php');
}
?>
