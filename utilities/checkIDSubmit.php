<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/../adminAccess1.php';
require_once dirname(__FILE__) . '/../timezone.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();


if($_SERVER['REQUEST_METHOD'] == 'POST')
{

  $checkID = rewrite($_POST['check_id']);
  // $loanUid = rewrite($_POST['loan_uid']);
  $id = rewrite($_POST['id']);
  $receiveStatus = 'COMPLETED';

//===============================================================================================

  if (isset($_POST['removeCheckID'])) { // for remove button
    $receiveStatus = 'PENDING';
    $checkID = null;

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if(!$checkID)
    {
        array_push($tableName,"check_id");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($receiveStatus) {
      array_push($tableName,"receive_status");
      array_push($tableValue,$receiveStatus);
      $stringType .=  "s";
    }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"invoice"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      header('Location: ../adminIssueInvoiceHistory.php');
    }
  }

//=============================================================================================================================

  if (isset($_POST['addCheckID'])) { // for add button

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_id");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_status");
      array_push($tableValue,$receiveStatus);
      $stringType .=  "s";
    }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"invoice"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      header('Location: ../adminIssueInvoiceHistory.php');
    }
  }

}
