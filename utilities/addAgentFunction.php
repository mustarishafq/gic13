<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addNewAgent($conn,$uid,$fullName,$username,$ic,$contact,$email,$birthday,$bankName,$bankNo,$address,$upline1,$upline2,$upline3,$upline4,$upline5,$status,$branchType)
{
     if(insertDynamicData($conn,"user",array("uid","full_name","username","ic","contact","email","birth_month","bank","bank_no","address",
   "upline1","upline2","upline3","upline4","upline5","status","branch_type"),
     array($uid,$fullName,$username,$ic,$contact,$email,$birthday,$bankName,$bankNo,$address,$upline1,$upline2,$upline3,$upline4,$upline5,$status,$branchType),
     "sssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = md5(uniqid());
    $id = rewrite($_POST["id"]);
    $fullName = rewrite($_POST["full_name"]);
    $referralName = $fullName;
    $username = rewrite($_POST["username"]);
    $ic = rewrite($_POST["ic_no"]);
    $contact = rewrite($_POST["contact"]);
    $email = rewrite($_POST["email"]);
    $birthday = rewrite($_POST["birth_month"]);
    $bankName = rewrite($_POST["bank_name"]);
    $bankNo = rewrite($_POST["bank_no"]);
    $address = rewrite($_POST["address"]);
    $referrerId = rewrite($_POST["upline"]);
    $status = rewrite($_POST["status"]);
    $branchType = $_SESSION['branch_type'];

    $uplineReferralHistory = getReferralHistory($conn, "WHERE referral_id =?",array("referral_id"),array($referrerId), "s");
    $topReferrerUid = $uplineReferralHistory[0]->getTopReferrerId();
    $uplineCurrentLevel = $uplineReferralHistory[0]->getCurrentLevel();
    $currentLevel = $uplineCurrentLevel + 1;
  }

  if(isset($_POST['editSubmit']))
  {
    if (addNewAgent($conn,$uid,$fullName,$username,$ic,$contact,$email,$birthday,$bankName,$bankNo,$address,$upline1,$upline2,$upline3,$upline4,$upline5,$status,$branchType)) {

      insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
      array($referrerId,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis");
      
      $_SESSION['messageType'] = 1;
      header('Location: ../editAgentInfo.php?type=1');
    }
  }
 ?>
