<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["uid"]);
    $fullName = rewrite($_POST["full_name"]);
    $username = rewrite($_POST["username"]);
    $ic = rewrite($_POST["ic_no"]);
    $contact = rewrite($_POST["contact"]);
    $email = rewrite($_POST["email"]);
    $birthday = rewrite($_POST["birth_month"]);
    $bankName = rewrite($_POST["bank_name"]);
    $bankNo = rewrite($_POST["bank_no"]);
    $address = rewrite($_POST["address"]);
    $upline = rewrite($_POST["upline"]);
    $status = rewrite($_POST["status"]);

    $uplineDetails = getReferralHistory($conn, "WHERE referral_name =?",array("referral_name"),array($upline), "s");
    $uplineUid = $uplineDetails[0]->getreferralId();
    $uplineCurrentLevel = $uplineDetails[0]->getCurrentLevel();
    $downlineLevel = $uplineCurrentLevel + 1;
    $uplineTopReferrerId = $uplineDetails[0]->getTopReferrerId();

  }

  if(isset($_POST['editSubmit']))
  {
      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      // //echo "save to database";
      if($fullName)
      {
          array_push($tableName,"full_name");
          array_push($tableValue,$fullName);
          $stringType .=  "s";
      }
      if($username)
      {
          array_push($tableName,"username");
          array_push($tableValue,$username);
          $stringType .=  "s";
      }
      if($ic)
      {
          array_push($tableName,"ic");
          array_push($tableValue,$ic);
          $stringType .=  "s";
      }
      if($contact)
      {
          array_push($tableName,"contact");
          array_push($tableValue,$contact);
          $stringType .=  "s";
      }
      if($email)
      {
          array_push($tableName,"email");
          array_push($tableValue,$email);
          $stringType .=  "s";
      }
      if($birthday)
      {
          array_push($tableName,"birth_month");
          array_push($tableValue,$birthday);
          $stringType .=  "s";
      }
      if($bankName)
      {
          array_push($tableName,"bank");
          array_push($tableValue,$bankName);
          $stringType .=  "s";
      }
      if($bankNo)
      {
          array_push($tableName,"bank_no");
          array_push($tableValue,$bankNo);
          $stringType .=  "s";
      }
      if($address)
      {
          array_push($tableName,"address");
          array_push($tableValue,$address);
          $stringType .=  "s";
      }
      if($status)
      {
          array_push($tableName,"status");
          array_push($tableValue,$status);
          $stringType .=  "s";
      }
    }
      array_push($tableValue,$uid);
      $stringType .=  "s";
      $withdrawUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);

      if($withdrawUpdated)
      {
          $referrerUpdated = updateDynamicData($conn, "referral_history", "WHERE referral_id =?",array("referrer_id","current_level","top_referrer_id"),array($uplineUid,$downlineLevel,$uplineTopReferrerId,$uid),"ssss");
          $_SESSION['messageType'] = 1;
          header('Location: ../editAgentInfo.php?type=2');
      }


 ?>
