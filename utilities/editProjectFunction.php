<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addNewProject($conn,$projectId,$projectName,$addProjectPpl,$companyName,$companyAddress,$claimTimes,$projectLeader,$display,$otherAmount,$creatorName)
{
     if(insertDynamicData($conn,"project",array("project_id","project_name","add_projectppl","company_name","company_address","claims_no","project_leader","display","other_claim","creator_name"),
     array($projectId,$projectName,$addProjectPpl,$companyName,$companyAddress,$claimTimes,$projectLeader,$display,$otherAmount,$creatorName),"ssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }
     return true;
}
function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter),
     "sssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    if (isset($_POST['id'])) {
      $id = rewrite($_POST['id']);
    }
    $projectId = md5(uniqid());
    $projectName = rewrite($_POST["project_name"]);
    $addProjectPpl = rewrite($_POST["add_by"]);
    $claimTimes = rewrite($_POST["claims_times"]);
    $projectLeader = $_POST["project_leader"];
    $companyName = $_POST["company_name"];
    $companyAddress = $_POST["company_address"];

    $remove = array(""); //want to remove null value 1,,3 to 1,3
    $resultProjectLeader = array_diff($projectLeader, $remove); //want to remove null value 1,,3 to 1,3
    $projectLeaderLimit = count($resultProjectLeader); //want to remove null value 1,,3 to 1,3
    $projectLeaderSlice = array_slice($projectLeader,0,$projectLeaderLimit); //want to remove null value 1,,3 to 1,3
    $projectLeaderImplode = implode(",",$projectLeaderSlice);
    // $display = "Yes";
    $creatorName = $_SESSION['username'];
    $otherAmount = 0;
    // $idNew = rewrite($_POST['project_id']);
    $projectDetails = getProject($conn, "WHERE id = ?",array("id"),array($id),"s");
    $projectNameCurrent = $projectDetails[0]->getProjectName();
    $projectId = $projectDetails[0]->getProjectId();
    $picCurrent = $projectDetails[0]->getAddProjectPpl();
    $plCurrent = $projectDetails[0]->getProjectLeader();
    $compNameCurrent = $projectDetails[0]->getCompanyName();
    $compAddressCurrent = $projectDetails[0]->getCompanyAddress();
    $projectClaimsCurrent = $projectDetails[0]->getProjectClaims();
    // $displayCurrent = $projectDetails[0]->getDisplay();
    $currentProjectDetails = ("$projectNameCurrent,$picCurrent,$compNameCurrent,$projectClaimsCurrent");
    $currentProjectDetailsExp = explode(",",$currentProjectDetails);

    $newProjectDetails = ("$projectName,$addProjectPpl,$companyName,$claimTimes");
    $newProjectDetailsExp = explode(",",$newProjectDetails);

    $projectNameDetails = ("Project Name,Person In Charge(PIC),Company Name,Project Claim");
    $projectNameDetailsExp = explode(",",$projectNameDetails);

    for ($i=0; $i <count($currentProjectDetailsExp) ; $i++) {
      if ($newProjectDetailsExp[$i] != $currentProjectDetailsExp[$i]) {
        $column = $projectNameDetailsExp[$i];
        if(editHistory($conn, $creatorName, $column, $projectId,$currentProjectDetailsExp[$i], $newProjectDetailsExp[$i]))
             {}
      }
    }
    if ($plCurrent != $projectLeaderImplode) {
      $column = "Project Leader";
      if(editHistory($conn, $creatorName, $column, $projectId,$plCurrent, $projectLeaderImplode))
           {}
    }
    if ($compAddressCurrent != $companyAddressImplode) {
      $column = "Company Address";
      if(editHistory($conn, $creatorName, $column, $projectId,$compAddressCurrent, $companyAddressImplode))
           {}
    }



if (isset($_POST['editButton'])) {

  $display = rewrite($_POST["display"]);

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($projectName)
  {
      array_push($tableName,"project_name");
      array_push($tableValue,$projectName);
      $stringType .=  "s";
  }
  if($addProjectPpl)
  {
      array_push($tableName,"add_projectppl");
      array_push($tableValue,$addProjectPpl);
      $stringType .=  "s";
  }
  if($claimTimes)
  {
      array_push($tableName,"claims_no");
      array_push($tableValue,$claimTimes);
      $stringType .=  "s";
  }
  if($projectLeaderImplode)
  {
      array_push($tableName,"project_leader");
      array_push($tableValue,$projectLeaderImplode);
      $stringType .=  "s";
  }
  if($companyName)
  {
      array_push($tableName,"company_name");
      array_push($tableValue,$companyName);
      $stringType .=  "s";
  }
  if($companyAddressImplode)
  {
      array_push($tableName,"company_address");
      array_push($tableValue,$companyAddressImplode);
      $stringType .=  "s";
  }
  // if($display || !$display)
  // {
  //     array_push($tableName,"display");
  //     array_push($tableValue,$display);
  //     $stringType .=  "s";
  // }

  array_push($tableValue,$id);
  $stringType .=  "i";
  $withdrawUpdated = updateDynamicData($conn,"project"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {

      $_SESSION['messageType'] = 1;
      header('Location: ../adminAddNewProject.php?type=3');
  }

}

}
else
{
    //  header('Location: ../index.php');
}
?>
