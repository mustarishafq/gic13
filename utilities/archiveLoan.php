<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter,$branchType)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after","branch_type"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter,$branchType),
     "ssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

$loanUid = $_POST['loanUid'];
$username = $_SESSION['username'];
$detailsBefore = "-";
$detailsAfter = "-";
$branchType = $_SESSION['branch_type'];


$loanStatusDetails = getLoanStatus($conn, "WHERE loan_uid = ?",array("loan_uid"),array($loanUid), "s");
$unitNo = $loanStatusDetails[0]->getUnitNo();
$currentDisplay = $loanStatusDetails[0]->getDisplay();
if ($currentDisplay == 'Yes') {
  $display = "No";
  $column = "Archived";
}elseif ($currentDisplay == 'No') {
  $display = "Yes";
  $column = "Un-Archived";
}


$tableName = array();
$tableValue =  array();
$stringType =  "";
// //echo "save to database";
if($loanStatusDetails)
{
    array_push($tableName,"display");
    array_push($tableValue,$display);
    $stringType .=  "s";
}
array_push($tableValue,$loanUid);
$stringType .=  "s";
$withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE loan_uid = ? ",$tableName,$tableValue,$stringType);
    if($withdrawUpdated)
    {
      if(editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter,$branchType))
           {}
        // $_SESSION['messageType'] = 1;
        // header('Location: ../adminWithdrawal.php?type=1');
    }
    else
    {
        // echo "fail";
    }

    $project[] = array("unit_no" => $unitNo);

    echo json_encode($project);
 ?>
