<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/../adminAccess1.php';
require_once dirname(__FILE__) . '/../timezone.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Invoice.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function addUplineCommission($conn, $idd, $purchaserName, $loanUid,$ulOverrideNew, $ulOverride,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date", "receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd, $purchaserName, $loanUid,$ulOverrideNew, $ulOverride,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid,$uulOverrideNew, $uulOverride,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd2, $purchaserName, $loanUid,$uulOverrideNew, $uulOverride,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addUpup3lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuulOverrideNew, $uuuulOverride,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd2, $purchaserName, $loanUid,$uuuulOverrideNew, $uuuulOverride,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addUpup4lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuulOverrideNew, $uuuulOverride,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd2, $purchaserName, $loanUid,$uuuulOverrideNew, $uuuulOverride,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addUpup5lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuuulOverrideNew, $uulOverride,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd2, $purchaserName, $loanUid,$uuuuulOverrideNew, $uulOverride,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addAgentCommission($conn, $idd3, $purchaserName, $loanUid,$agentCommNew, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd3, $purchaserName, $loanUid,$agentCommNew, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addPlCommission($conn, $idd3, $purchaserName, $loanUid,$plOverrideNew, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd3, $purchaserName, $loanUid,$plOverrideNew, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addHosCommission($conn, $idd3, $purchaserName, $loanUid,$hosOverrideNew, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd3, $purchaserName, $loanUid,$hosOverrideNew, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addListerCommission($conn, $idd3, $purchaserName, $loanUid,$listerOverrideNew, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd3, $purchaserName, $loanUid,$listerOverrideNew, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function CreditNote($conn, $invoiceId,$projectName,$unitNo,$bookingDate,$projectHandler,$projectHandlerDef,$itemAmount,$amount,$charges,
                $receiveStatus,$claimStatus,$checkID,$finalAmount,$bankAccHolder,$bankName,$bankAccNo,$branchType,$creditStatus)
{
     if(insertDynamicData($conn,"invoice_credit", array("credit_id","project_name","unit_no","booking_date","project_handler","project_handler_default","item","amount","charges","receive_status",
                                                          "claims_status","check_id","final_amount","bank_account_holder","bank_name","bank_account_no","branch_type","credit_status"),
     array($invoiceId,$projectName,$unitNo,$bookingDate,$projectHandler,$projectHandlerDef,$itemAmount,$amount,$charges,
                     $receiveStatus,$claimStatus,$checkID,$finalAmount,$bankAccHolder,$bankName,$bankAccNo,$branchType,$creditStatus),
     "ssssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{

  $branchType = $_SESSION['branch_type'];
  $checkID = rewrite($_POST['check_id']);
  // $loanUid = rewrite($_POST['loan_uid']);
  $id = rewrite($_POST['id']);
  $loanDetails = getLoanStatus($conn, "WHERE id =?", array("id"), array($id), "s");
  $purchaserName = $loanDetails[0]->getPurchaserName();
  if (isset($_POST['addCheckID1'])) {
      $sstCharges = $loanDetails[0]->getSst1();
  }
  if (isset($_POST['addCheckID2'])) {
      $sstCharges = $loanDetails[0]->getSst2();
  }
  if (isset($_POST['addCheckID3'])) {
      $sstCharges = $loanDetails[0]->getSst3();
  }
  if (isset($_POST['addCheckID4'])) {
      $sstCharges = $loanDetails[0]->getSst4();
  }
  if (isset($_POST['addCheckID5'])) {
      $sstCharges = $loanDetails[0]->getSst5();
  }
  $loanUid = $loanDetails[0]->getLoanUid();

  $ulOverrideImp = $loanDetails[0]->getUlBalance();
  $ulOverrideExp = explode(",",$ulOverrideImp);
  $ulOverride = $ulOverrideExp[0];
  $ulClaim = $ulOverrideExp[1];

  $uulOverrideImp = $loanDetails[0]->getUulBalance();
  $uulOverrideExp = explode(",",$uulOverrideImp);
  $uulOverride = $uulOverrideExp[0];
  $uulClaim = $uulOverrideExp[1];

  $uuulOverrideImp = $loanDetails[0]->getUuulBalance();
  $uuulOverrideExp = explode(",",$uuulOverrideImp);
  $uuulOverride = $uuulOverrideExp[0];
  $uuulClaim = $uuulOverrideExp[1];

  $uuuulOverrideImp = $loanDetails[0]->getUuuulBalance();
  $uuuulOverrideExp = explode(",",$uuuulOverrideImp);
  $uuuulOverride = $uuuulOverrideExp[0];
  $uuuulClaim = $uuuulOverrideExp[1];

  $uuuuulOverrideImp = $loanDetails[0]->getUuuuulBalance();
  $uuuuulOverrideExp = explode(",",$uuuuulOverrideImp);
  $uuuuulOverride = $uuuuulOverrideExp[0];
  $uuuuulClaim = $uuuuulOverrideExp[1];

  // $upline1 = $loanDetails[0]->getUpline1();
  $upline1ID = md5(uniqid());
  $upline2ID = md5(uniqid());
  $upline3ID = md5(uniqid());
  $upline4ID = md5(uniqid());
  $upline5ID = md5(uniqid());
  $hosId = md5(uniqid());
  $agentId = md5(uniqid());
  $listerId = md5(uniqid());

  $upline1Type = "Upline 1";
  $upline2Type = "Upline 2";
  $upline3Type = "Upline 3";
  $upline4Type = "Upline 4";
  $upline5Type = "Upline 5";
  $agentType = "Agent";
  $plType = "PL";
  $hosType = "HOS";
  $listerType = "Lister";

  $getUplineDetails = getUser($conn," WHERE full_name = ? ",array("full_name"),array($loanDetails[0]->getAgent()),"s");
  $agentUid = $getUplineDetails[0]->getUid();
  $agentReferralDetails = getReferralHistory($conn, "WHERE referral_id=?",array("referral_id"),array($agentUid), "s");
  $agentCurrentLevel = $agentReferralDetails[0]->getCurrentLevel();
  $agentFirstLevel = $agentCurrentLevel + 1;
  $uplineUidDetails = getTop10ReferrerOfUser($conn, $agentUid);
  if ($uplineUidDetails) {
    for ($i=0; $i <count($uplineUidDetails) ; $i++) {
      $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uplineUidDetails[$i]), "s");
      $fullName = $userDetails[0]->getFullName();
      $upline[] = $userDetails[0]->getFullName();

    }
  }
  $uplineImp = implode(",",$upline);
  $uplineExp = explode(",",$uplineImp);
  for ($i=0; $i <count($uplineExp) ; $i++) {
    if ($i == 0) {
      $upline1 = $uplineExp[$i];
    }else
    if ($i == 1){
      $upline2 = $uplineExp[$i];
    }else
    if ($i == 2){
      $upline3 = $uplineExp[$i];
    }else
    if ($i == 3){
      $upline4 = $uplineExp[$i];
    }else
    if ($i == 4){
      $upline5 = $uplineExp[$i];
    }
  }

  $upline1Details = getUser($conn, "WHERE full_name =?", array("full_name"), array($upline1), "s");
  if ($upline1Details) {
    $icNo1 = $upline1Details[0]->getIcNo();
    $upline1Status = $upline1Details[0]->getStatus();
  }

  // $upline2 = $loanDetails[0]->getUpline2();
  $upline2Details = getUser($conn, "WHERE full_name =?", array("full_name"), array($upline2), "s");
  if ($upline2Details) {
    $icNo2 = $upline2Details[0]->getIcNo();
    $upline2Status = $upline2Details[0]->getStatus();
  }
  // $upline4 = $loanDetails[0]->getUpline4();
  $upline4Details = getUser($conn, "WHERE full_name =?", array("full_name"), array($upline4), "s");
  if ($upline4Details) {
    $icNo4 = $upline4Details[0]->getIcNo();
    $upline4Status = $upline4Details[0]->getStatus();
  }
  // $upline3 = $loanDetails[0]->getUpline3();
  $upline3Details = getUser($conn, "WHERE full_name =?", array("full_name"), array($upline3), "s");
  if ($upline3Details) {
    $icNo3 = $upline3Details[0]->getIcNo();
    echo "upline 3 :".$upline3."<br>";
    echo "IC :".$icNo3."<br>";
    $upline3Status = $upline3Details[0]->getStatus();
  }
  // $upline5 = $loanDetails[0]->getUpline5();
  $upline5Details = getUser($conn, "WHERE full_name =?", array("full_name"), array($upline5), "s");
  if ($upline5Details) {
    $icNo5 = $upline5Details[0]->getIcNo();
    $upline5Status = $upline5Details[0]->getStatus();
  }
  $plName = $loanDetails[0]->getPlName();
  $plNameExplode = explode(",",$plName);
  $plNameNo = count($plNameExplode);

  $plOverrideImp = $loanDetails[0]->getPlOverride();
  $plOverrideExp = explode(",",$plOverrideImp);
  $plOverride = $plOverrideExp[0];

  $plOverride = $plOverride / $plNameNo;

  $hosName = $loanDetails[0]->getHosName();
  $hosNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($hosName), "s");
  if ($hosNameDetails) {
    $hosName = $hosNameDetails[0]->getFullName();
    $icNoHos = $hosNameDetails[0]->getIcNo();
    $hosStatus = $hosNameDetails[0]->getStatus();
  }else {
    $icNoHos = "";
    $hosStatus = "Inactive";
  }

  $hosOverrideImp = $loanDetails[0]->getHosBalance();
  $hosOverrideExp = explode(",",$hosOverrideImp);
  $hosOverride = $hosOverrideExp[0];

  $listerName = $loanDetails[0]->getListerName();
  $icNoLister = "123";
  $listerNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($listerName), "s");
  if ($listerNameDetails) {
    $listerName = $listerNameDetails[0]->getFullName();
    $icNoLister = $listerNameDetails[0]->getIcNo();
    $listerNameStatus = $listerNameDetails[0]->getStatus();
    if (!$icNoLister) {
      $icNoLister = "";
    }
  }else {
    $icNoLister = "123";
    $listerName = $loanDetails[0]->getListerName();
    $listerNameStatus = 'Inactive';
  }
  $listerOverrideImp = $loanDetails[0]->getListerOverride();
  $listerOverrideExp = explode(",",$listerOverrideImp);
  $listerOverride = $listerOverrideExp[0];

  $projectName = $loanDetails[0]->getProjectName();
  $unitNo = $loanDetails[0]->getUnitNo();
  $bookingDate = $loanDetails[0]->getBookingDate();
  $totalClaimedDevAmt = $loanDetails[0]->getTotalClaimDevAmt();
  $totalBalUnclaimAmt = $loanDetails[0]->getTotalBalUnclaimAmt();
  $paymentMethod = "Online Transfer";
  // $totalClaimedDevAmtRestore = $totalClaimedDevAmt;
  // $totalBalUnclaimAmtRestore = $totalBalUnclaimAmt;
  $projectName = $loanDetails[0]->getProjectName();
  $projectDetails = getProject($conn, "WHERE project_name = ?", array("project_name"), array($projectName), "s");
  $claimStatus = $projectDetails[0]->getProjectClaims();

  $agent = $loanDetails[0]->getAgent();
  $agentDetails = getUser($conn,"WHERE full_name = ?",array("full_name"),array($agent),"s");
  if ($agentDetails) {
    $agent = $agentDetails[0]->getFullName();
    $icNoAgent = $agentDetails[0]->getIcNo();
  }
  $agentCommImp = $loanDetails[0]->getAgentBalance();
  $agentCommExp = explode(",",$agentCommImp);
  $agentComm = $agentCommExp[0];
  $agentClaim = $agentCommExp[1];
  $claimStatus = $claimStatus - $agentClaim;
  // $agentAdvanced = 2000;
  // $agentComm -= $agentAdvanced; // minus advanced because agent already claimed

  if ($sstCharges == 'NO') {

    $agentCommFinal = ($agentComm / $claimStatus) - ($agentComm / $claimStatus)*(6/100);
    $ulOverrideFinal = ($ulOverride / $claimStatus) - ($ulOverride / $claimStatus)*(6/100);
    $uulOverrideFinal = ($uulOverride / $claimStatus) - ($uulOverride / $claimStatus)*(6/100);
    $uuulOverrideFinal = ($uuulOverride / $claimStatus) - ($uuulOverride / $claimStatus)*(6/100);
    $uuuulOverrideFinal = ($uuuulOverride / $claimStatus) - ($uuuulOverride / $claimStatus)*(6/100);
    $uuuuulOverrideFinal = ($uuuuulOverride / $claimStatus) - ($uuuuulOverride / $claimStatus)*(6/100);
    $plOverrideFinal = ($plOverride / $claimStatus) - ($plOverride / $claimStatus)*(6/100);
    $hosOverrideFinal = ($hosOverride / $claimStatus) - ($hosOverride / $claimStatus)*(6/100);
    $listerOverrideFinal = ($listerOverride / $claimStatus) - ($listerOverride / $claimStatus)*(6/100);

  }elseif ($sstCharges == 'YES') {
    $agentCommFinal = $agentComm / $claimStatus;
    $ulOverrideFinal = $ulOverride / $claimStatus;
    $uulOverrideFinal = $uulOverride / $claimStatus;
    $uuulOverrideFinal = ($uuulOverride / $claimStatus);
    $uuuulOverrideFinal = ($uuuulOverride / $claimStatus);
    $uuuuulOverrideFinal = ($uuuuulOverride / $claimStatus);
    $plOverrideFinal = ($plOverride / $claimStatus);
    $hosOverrideFinal = $hosOverride / $claimStatus;
    $listerOverrideFinal = $listerOverride / $claimStatus;
  }

  $agentCommFinalOri = $agentComm / $claimStatus;
  $ulOverrideFinalOri = $ulOverride / $claimStatus;
  $uulOverrideFinalOri = $uulOverride / $claimStatus;
  $uuulOverrideFinalOri = ($uuulOverride / $claimStatus);
  $uuuulOverrideFinalOri = ($uuuulOverride / $claimStatus);
  $uuuuulOverrideFinalOri = ($uuuuulOverride / $claimStatus);
  $plOverrideFinalOri = ($plOverride / $claimStatus);
  $hosOverrideFinalOri = $hosOverride / $claimStatus;
  $listerOverrideFinalOri = $listerOverride / $claimStatus;

  // echo "Agent Comm Default:".$agentCommFinalOri."<br>";
  // echo "ul Comm Default:".$ulOverrideFinalOri."<br>";
  // echo "uul Comm Default:".$uulOverrideFinalOri."<br>";
  // echo "uuul Comm Default:".$uuulOverrideFinalOri."<br>";
  // echo "uuuul Comm Default:".$uuuulOverrideFinalOri."<br>";
  // echo "uuuuul Comm Default:".$uuuuulOverrideFinalOri."<br>";
  // echo "pl Comm Default:".$plOverrideFinalOri."<br>";
  // echo "hos comm Default:".$hosOverrideFinalOri."<br>";
  // echo "lister comm Default:".$listerOverrideFinalOri."<br>";
  // echo "Agent Comm :".$agentCommFinal."<br>";
  // echo "ul Comm :".$ulOverrideFinal."<br>";
  // echo "uul Comm :".$uulOverrideFinal."<br>";
  // echo "uuul Comm :".$uuulOverrideFinal."<br>";
  // echo "uuuul Comm :".$uuuulOverrideFinal."<br>";
  // echo "uuuuul Comm :".$uuuuulOverrideFinal."<br>";
  // echo "pl Comm :".$plOverrideFinal."<br>";
  // echo "hos comm :".$hosOverrideFinal."<br>";
  // echo "lister comm :".$listerOverrideFinal."<br>";


  $claimStatusNew = $agentClaim + 1;
  $receiveStatus = 'PENDING';

  $agentCommNew = $agentComm - $agentCommFinalOri;
  $agentCommNews = $agentCommNew.",".$claimStatusNew;

  $ulOverrideNew = $ulOverride - $ulOverrideFinalOri;
  $ulOverrideNews = $ulOverrideNew.",".$claimStatusNew;

  $uulOverrideNew = $uulOverride - $uulOverrideFinalOri;
  $uulOverrideNews = $uulOverrideNew.",".$claimStatusNew;

  $uuulOverrideNew = $uuulOverride - $uuulOverrideFinalOri;
  $uuulOverrideNews = $uuulOverrideNew.",".$claimStatusNew;

  $uuuulOverrideNew = $uuuulOverride - $uuuulOverrideFinalOri;
  $uuuulOverrideNews = $uuuulOverrideNew.",".$claimStatusNew;

  $uuuuulOverrideNew = $uuuuulOverride - $uuuuulOverrideFinalOri;
  $uuuuulOverrideNews = $uuuuulOverrideNew.",".$claimStatusNew;

  $plOverrideNew = $plOverride - $plOverrideFinalOri;
  $plOverrideNews = $plOverrideNew.",".$claimStatusNew;

  $listerOverrideNew = $listerOverride - $listerOverrideFinalOri;
  $listerOverrideNews = $listerOverrideNew.",".$claimStatusNew;

  $hosOverrideNew = $hosOverride - $hosOverrideFinalOri;
  $hosOverrideNews = $hosOverrideNew.",".$claimStatusNew;

//===============================================================================================

  if (isset($_POST['removeCheckID1'])) { // for remove button
    // $receiveStatus = 'PENDING';
    $checkID1 = null;
    $receiveDate1 = null;

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    // if(!$checkID1)
    // {
    //     array_push($tableName,"check_no1");
    //     array_push($tableValue,$checkID1);
    //     $stringType .=  "s";
    // }
    if(!$receiveDate1)
    {
        array_push($tableName,"receive_date1");
        array_push($tableValue,$receiveDate1);
        $stringType .=  "s";
    }
    // if($totalClaimedDevAmtRestore || !$totalClaimedDevAmtRestore)
    // {
    //     array_push($tableName,"total_claimed_dev_amt");
    //     array_push($tableValue,$totalClaimedDevAmtRestore);
    //     $stringType .=  "s";
    // }
    // if($totalBalUnclaimAmtRestore || !$totalBalUnclaimAmtRestore)
    // {
    //     array_push($tableName,"total_bal_unclaim_amt");
    //     array_push($tableValue,$totalBalUnclaimAmtRestore);
    //     $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
      header('Location: ../invoiceRecord.php');
    }
  }

//=============================================================================================================================

if (isset($_POST['removeCheckID2'])) { // for remove button
  // $receiveStatus = 'PENDING';
  $checkID2 = null;
  $receiveDate2 = null;

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  // if(!$checkID2)
  // {
  //     array_push($tableName,"check_no2");
  //     array_push($tableValue,$checkID2);
  //     $stringType .=  "s";
  // }
  if(!$receiveDate2)
  {
      array_push($tableName,"receive_date2");
      array_push($tableValue,$receiveDate2);
      $stringType .=  "s";
  }
  // if($totalClaimedDevAmtRestore || !$totalClaimedDevAmtRestore)
  // {
  //     array_push($tableName,"total_claimed_dev_amt");
  //     array_push($tableValue,$totalClaimedDevAmtRestore);
  //     $stringType .=  "s";
  // }
  // if($totalBalUnclaimAmtRestore || !$totalBalUnclaimAmtRestore)
  // {
  //     array_push($tableName,"total_bal_unclaim_amt");
  //     array_push($tableValue,$totalBalUnclaimAmtRestore);
  //     $stringType .=  "s";
  // }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    $_SESSION['messageType'] = 1;
    header('Location: ../invoiceRecord.php');
  }
}

//=============================================================================================================================

if (isset($_POST['removeCheckID3'])) { // for remove button
  // $receiveStatus = 'PENDING';
  $checkID3 = null;
  $receiveDate3 = null;

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  // if(!$checkID3)
  // {
  //     array_push($tableName,"check_no3");
  //     array_push($tableValue,$checkID3);
  //     $stringType .=  "s";
  // }
  if(!$receiveDate3)
  {
      array_push($tableName,"receive_date3");
      array_push($tableValue,$receiveDate3);
      $stringType .=  "s";
  }
  // if($totalClaimedDevAmtRestore || !$totalClaimedDevAmtRestore)
  // {
  //     array_push($tableName,"total_claimed_dev_amt");
  //     array_push($tableValue,$totalClaimedDevAmtRestore);
  //     $stringType .=  "s";
  // }
  // if($totalBalUnclaimAmtRestore || !$totalBalUnclaimAmtRestore)
  // {
  //     array_push($tableName,"total_bal_unclaim_amt");
  //     array_push($tableValue,$totalBalUnclaimAmtRestore);
  //     $stringType .=  "s";
  // }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    $_SESSION['messageType'] = 1;
    header('Location: ../invoiceRecord.php');
  }
}

//=============================================================================================================================

if (isset($_POST['removeCheckID4'])) { // for remove button
  // $receiveStatus = 'PENDING';
  $checkID4 = null;
  $receiveDate4 = null;

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  // if(!$checkID4)
  // {
  //     array_push($tableName,"check_no4");
  //     array_push($tableValue,$checkID4);
  //     $stringType .=  "s";
  // }
  if(!$receiveDate4)
  {
      array_push($tableName,"receive_date4");
      array_push($tableValue,$receiveDate4);
      $stringType .=  "s";
  }
  // if($totalClaimedDevAmtRestore || !$totalClaimedDevAmtRestore)
  // {
  //     array_push($tableName,"total_claimed_dev_amt");
  //     array_push($tableValue,$totalClaimedDevAmtRestore);
  //     $stringType .=  "s";
  // }
  // if($totalBalUnclaimAmtRestore || !$totalBalUnclaimAmtRestore)
  // {
  //     array_push($tableName,"total_bal_unclaim_amt");
  //     array_push($tableValue,$totalBalUnclaimAmtRestore);
  //     $stringType .=  "s";
  // }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    $_SESSION['messageType'] = 1;
    header('Location: ../invoiceRecord.php');
  }
}

//=============================================================================================================================

if (isset($_POST['removeCheckID5'])) { // for remove button
  // $receiveStatus = 'PENDING';
  $checkID5 = null;
  $receiveDate5 = null;

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  // if(!$checkID5)
  // {
  //     array_push($tableName,"check_no5");
  //     array_push($tableValue,$checkID5);
  //     $stringType .=  "s";
  // }
  if(!$receiveDate5)
  {
      array_push($tableName,"receive_date5");
      array_push($tableValue,$receiveDate5);
      $stringType .=  "s";
  }
  // if($totalClaimedDevAmtRestore || !$totalClaimedDevAmtRestore)
  // {
  //     array_push($tableName,"total_claimed_dev_amt");
  //     array_push($tableValue,$totalClaimedDevAmtRestore);
  //     $stringType .=  "s";
  // }
  // if($totalBalUnclaimAmtRestore || !$totalBalUnclaimAmtRestore)
  // {
  //     array_push($tableName,"total_bal_unclaim_amt");
  //     array_push($tableValue,$totalBalUnclaimAmtRestore);
  //     $stringType .=  "s";
  // }
  array_push($tableValue,$id);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    $_SESSION['messageType'] = 1;
    header('Location: ../invoiceRecord.php');
  }
}

//=============================================================================================================================

  if (isset($_POST['addCheckID1'])) { // for add button

    $details = "2nd Part Commission";
    $receiveDate1 = $_POST['receive_date1'];

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no1");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date1");
      array_push($tableValue,$receiveDate1);
      $stringType .=  "s";
    }
    if ($ulOverrideNews) {
      array_push($tableName,"ul_balance");
      array_push($tableValue,$ulOverrideNews);
      $stringType .=  "s";
    }
    if ($uulOverrideNews) {
      array_push($tableName,"uul_balance");
      array_push($tableValue,$uulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuulOverrideNews) {
      array_push($tableName,"uuul_balance");
      array_push($tableValue,$uuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuulOverrideNews) {
      array_push($tableName,"uuuul_balance");
      array_push($tableValue,$uuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuuulOverrideNews) {
      array_push($tableName,"uuuuul_balance");
      array_push($tableValue,$uuuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($listerOverrideNews) {
      array_push($tableName,"lister_balance");
      array_push($tableValue,$listerOverrideNews);
      $stringType .=  "s";
    }
    if ($plOverrideNews) {
      array_push($tableName,"pl_balance");
      array_push($tableValue,$plOverrideNews);
      $stringType .=  "s";
    }
    if ($hosOverrideNews) {
      array_push($tableName,"hos_balance");
      array_push($tableValue,$hosOverrideNews);
      $stringType .=  "s";
    }
    if ($agentCommNews) {
      array_push($tableName,"agent_balance");
      array_push($tableValue,$agentCommNews);
      $stringType .=  "s";
    }

    // if ($totalClaimedDevAmt && !$loanDetails[0]->getCheckNo1()) {
    //   array_push($tableName,"total_claimed_dev_amt");
    //   array_push($tableValue,$totalClaimedDevAmt);
    //   $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
      header('Location: ../invoiceRecord.php?type=1');
    }

if (!$loanDetails[0]->getCheckNo1()) {

    if ($upline1Status == 'Active') {
      if(addUplineCommission($conn, $idd, $purchaserName, $loanUid,$ulOverrideFinalOri, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo1,$upline1ID,$upline1,$branchType,$upline1Type,$sstCharges))
         {
              //$_SESSION['messageType'] = 1;
              // header('Location: ../invoiceRecord.php');
              //echo "register success";
         }
    }

   if ($upline2Status == 'Active') {
     if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid,$uulOverrideFinalOri, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo2,$upline2ID,$upline2,$branchType,$upline2Type,$sstCharges))
      {
      header('Location: ../invoiceRecord.php');
      }
   }
   if ($upline3Status == 'Active') {
     if (addUpup3lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuulOverrideFinalOri, $uuulOverrideFinal,$upline3,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo3,$upline3ID,$upline3,$branchType,$upline3Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if ($upline4Status == 'Active') {
     if (addUpup4lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuulOverrideFinalOri, $uuuulOverrideFinal,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo4,$upline4ID,$upline4,$branchType,$upline4Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if ($upline4Status == 'Active') {
     if (addUpup5lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuuulOverrideFinalOri, $uuuuulOverrideFinal,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo5,$upline5ID,$upline5,$branchType,$upline5Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid,$agentCommFinalOri, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoAgent,$agentId,$agent,$branchType,$agentType,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }

   foreach ($plNameExplode as $plNameFinal) {
     if ($plNameFinal && $plOverrideFinal) {
       $plId = md5(uniqid());
       $plNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($plNameFinal), "s");
       $plNameFinale = $plNameDetails[0]->getFullName();
       $icNoPl = $plNameDetails[0]->getIcNo();
       if (addPlCommission($conn, $idd3, $purchaserName, $loanUid,$plOverrideFinalOri, $plOverrideFinal,$plNameFinale,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoPl,$plId,$plNameFinale,$branchType,$plType,$sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }

   }
}
   if ($hosName && $hosOverrideFinal && $hosStatus == 'Active') {
     if (addHosCommission($conn, $idd3, $purchaserName, $loanUid,$hosOverrideFinalOri, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoHos,$hosId,$hosName,$branchType,$hosType,$sstCharges))
     {
       header('Location: ../invoiceRecord.php');
     }
   }
   if ($listerName && $listerOverrideFinal && $listerNameStatus == 'Active') {
     if (addListerCommission($conn, $idd3, $purchaserName, $loanUid,$listerOverrideFinalOri, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoLister,$listerId,$listerName,$branchType,$listerType,$sstCharges))
     {
       header('Location: ../invoiceRecord.php');
     }
   }


}
  }


//=============================================================================================================================

  if (isset($_POST['addCheckID2'])) { // for add button

    $details = "3rd Part Commission";
    $receiveDate2 = $_POST['receive_date2'];

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no2");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date2");
      array_push($tableValue,$receiveDate2);
      $stringType .=  "s";
    }
    if ($ulOverrideNews) {
      array_push($tableName,"ul_balance");
      array_push($tableValue,$ulOverrideNews);
      $stringType .=  "s";
    }
    if ($uulOverrideNews) {
      array_push($tableName,"uul_balance");
      array_push($tableValue,$uulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuulOverrideNews) {
      array_push($tableName,"uuul_balance");
      array_push($tableValue,$uuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuulOverrideNews) {
      array_push($tableName,"uuuul_balance");
      array_push($tableValue,$uuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuuulOverrideNews) {
      array_push($tableName,"uuuuul_balance");
      array_push($tableValue,$uuuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($listerOverrideNews) {
      array_push($tableName,"lister_balance");
      array_push($tableValue,$listerOverrideNews);
      $stringType .=  "s";
    }
    if ($plOverrideNews) {
      array_push($tableName,"pl_balance");
      array_push($tableValue,$plOverrideNews);
      $stringType .=  "s";
    }
    if ($hosOverrideNews) {
      array_push($tableName,"hos_balance");
      array_push($tableValue,$hosOverrideNews);
      $stringType .=  "s";
    }
    if ($agentCommNews) {
      array_push($tableName,"agent_balance");
      array_push($tableValue,$agentCommNews);
      $stringType .=  "s";
    }
    // if ($totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo2() || !$totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo2()) {
    //   array_push($tableName,"total_bal_unclaim_amt");
    //   array_push($tableValue,$totalBalUnclaimAmt);
    //   $stringType .=  "s";
    // }
    // // if ($uulOverrideTotal || !$uulOverrideTotal) {
    // //   array_push($tableName,"uul_override");
    // //   array_push($tableValue,$uulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // // if ($ulOverrideTotal || !$ulOverrideTotal) {
    // //   array_push($tableName,"ul_override");
    // //   array_push($tableValue,$ulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // if ($totalClaimedDevAmt && !$loanDetails[0]->getCheckNo2()) {
    //   array_push($tableName,"total_claimed_dev_amt");
    //   array_push($tableValue,$totalClaimedDevAmt);
    //   $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
      header('Location: ../invoiceRecord.php?type=1');
    }

  if (!$loanDetails[0]->getCheckNo2()) {

    if ($upline1Status == 'Active') {
      if(addUplineCommission($conn, $idd, $purchaserName, $loanUid,$ulOverrideFinalOri, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo1,$upline1ID,$upline1,$branchType,$upline1Type,$sstCharges))
         {
              //$_SESSION['messageType'] = 1;
              // header('Location: ../invoiceRecord.php');
              //echo "register success";
         }
    }

   if ($upline2Status == 'Active') {
     if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid,$uulOverrideFinalOri, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo2,$upline2ID,$upline2,$branchType,$upline2Type,$sstCharges))
      {
      header('Location: ../invoiceRecord.php');
      }
   }
   if ($upline3Status == 'Active') {
     if (addUpup3lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuulOverrideFinalOri, $uuulOverrideFinal,$upline3,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo3,$upline3ID,$upline3,$branchType,$upline3Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if ($upline4Status == 'Active') {
     if (addUpup4lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuulOverrideFinalOri, $uuuulOverrideFinal,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo4,$upline4ID,$upline4,$branchType,$upline4Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if ($upline4Status == 'Active') {
     if (addUpup5lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuuulOverrideFinalOri, $uuuuulOverrideFinal,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo5,$upline5ID,$upline5,$branchType,$upline5Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid,$agentCommFinalOri, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoAgent,$agentId,$agent,$branchType,$agentType,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }

   foreach ($plNameExplode as $plNameFinal) {
     if ($plNameFinal && $plOverrideFinal) {
       $plId = md5(uniqid());
       $plNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($plNameFinal), "s");
       $plNameFinale = $plNameDetails[0]->getFullName();
       $icNoPl = $plNameDetails[0]->getIcNo();
       if (addPlCommission($conn, $idd3, $purchaserName, $loanUid,$plOverrideFinalOri, $plOverrideFinal,$plNameFinale,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoPl,$plId,$plNameFinale,$branchType,$plType,$sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }

   }
}
   if ($hosName && $hosOverrideFinal && $hosStatus == 'Active') {
     if (addHosCommission($conn, $idd3, $purchaserName, $loanUid,$hosOverrideFinalOri, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoHos,$hosId,$hosName,$branchType,$hosType,$sstCharges))
     {
       header('Location: ../invoiceRecord.php');
     }
   }
   if ($listerName && $listerOverrideFinal && $listerNameStatus == 'Active') {
     if (addListerCommission($conn, $idd3, $purchaserName, $loanUid,$listerOverrideFinalOri, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoLister,$listerId,$listerName,$branchType,$listerType,$sstCharges))
     {
       header('Location: ../invoiceRecord.php');
     }
   }
  }
}


//=============================================================================================================================

  if (isset($_POST['addCheckID3'])) { // for add button

    $details = "4th Part Commission";
    $receiveDate3 = $_POST['receive_date3'];

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no3");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date3");
      array_push($tableValue,$receiveDate3);
      $stringType .=  "s";
    }
    if ($ulOverrideNews) {
      array_push($tableName,"ul_balance");
      array_push($tableValue,$ulOverrideNews);
      $stringType .=  "s";
    }
    if ($uulOverrideNews) {
      array_push($tableName,"uul_balance");
      array_push($tableValue,$uulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuulOverrideNews) {
      array_push($tableName,"uuul_balance");
      array_push($tableValue,$uuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuulOverrideNews) {
      array_push($tableName,"uuuul_balance");
      array_push($tableValue,$uuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuuulOverrideNews) {
      array_push($tableName,"uuuuul_balance");
      array_push($tableValue,$uuuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($listerOverrideNews) {
      array_push($tableName,"lister_balance");
      array_push($tableValue,$listerOverrideNews);
      $stringType .=  "s";
    }
    if ($plOverrideNews) {
      array_push($tableName,"pl_balance");
      array_push($tableValue,$plOverrideNews);
      $stringType .=  "s";
    }
    if ($hosOverrideNews) {
      array_push($tableName,"hos_balance");
      array_push($tableValue,$hosOverrideNews);
      $stringType .=  "s";
    }
    if ($agentCommNews) {
      array_push($tableName,"agent_balance");
      array_push($tableValue,$agentCommNews);
      $stringType .=  "s";
    }
    // if ($totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo3() || !$totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo3()) {
    //   array_push($tableName,"total_bal_unclaim_amt");
    //   array_push($tableValue,$totalBalUnclaimAmt);
    //   $stringType .=  "s";
    // }
    // // if ($uulOverrideTotal || !$uulOverrideTotal) {
    // //   array_push($tableName,"uul_override");
    // //   array_push($tableValue,$uulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // // if ($ulOverrideTotal || !$ulOverrideTotal) {
    // //   array_push($tableName,"ul_override");
    // //   array_push($tableValue,$ulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // if ($totalClaimedDevAmt && !$loanDetails[0]->getCheckNo3()) {
    //   array_push($tableName,"total_claimed_dev_amt");
    //   array_push($tableValue,$totalClaimedDevAmt);
    //   $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
      header('Location: ../invoiceRecord.php?type=1');
    }

    if (!$loanDetails[0]->getCheckNo3()) {

    if ($upline1Status == 'Active') {
      if(addUplineCommission($conn, $idd, $purchaserName, $loanUid,$ulOverrideFinalOri, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo1,$upline1ID,$upline1,$branchType,$upline1Type,$sstCharges))
         {
              //$_SESSION['messageType'] = 1;
              // header('Location: ../invoiceRecord.php');
              //echo "register success";
         }
    }

   if ($upline2Status == 'Active') {
     if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid,$uulOverrideFinalOri, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo2,$upline2ID,$upline2,$branchType,$upline2Type,$sstCharges))
      {
      header('Location: ../invoiceRecord.php');
      }
   }
   if ($upline3Status == 'Active') {
     if (addUpup3lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuulOverrideFinalOri, $uuulOverrideFinal,$upline3,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo3,$upline3ID,$upline3,$branchType,$upline3Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if ($upline4Status == 'Active') {
     if (addUpup4lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuulOverrideFinalOri, $uuuulOverrideFinal,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo4,$upline4ID,$upline4,$branchType,$upline4Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if ($upline4Status == 'Active') {
     if (addUpup5lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuuulOverrideFinalOri, $uuuuulOverrideFinal,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo5,$upline5ID,$upline5,$branchType,$upline5Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid,$agentCommFinalOri, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoAgent,$agentId,$agent,$branchType,$agentType,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }

   foreach ($plNameExplode as $plNameFinal) {
     if ($plNameFinal && $plOverrideFinal) {
       $plId = md5(uniqid());
       $plNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($plNameFinal), "s");
       $plNameFinale = $plNameDetails[0]->getFullName();
       $icNoPl = $plNameDetails[0]->getIcNo();
       if (addPlCommission($conn, $idd3, $purchaserName, $loanUid,$plOverrideFinalOri, $plOverrideFinal,$plNameFinale,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoPl,$plId,$plNameFinale,$branchType,$plType,$sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }

   }
}
   if ($hosName && $hosOverrideFinal && $hosStatus == 'Active') {
     if (addHosCommission($conn, $idd3, $purchaserName, $loanUid,$hosOverrideFinalOri, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoHos,$hosId,$hosName,$branchType,$hosType,$sstCharges))
     {
       header('Location: ../invoiceRecord.php');
     }
   }
   if ($listerName && $listerOverrideFinal && $listerNameStatus == 'Active') {
     if (addListerCommission($conn, $idd3, $purchaserName, $loanUid,$listerOverrideFinalOri, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoLister,$listerId,$listerName,$branchType,$listerType,$sstCharges))
     {
       header('Location: ../invoiceRecord.php');
     }
   }
  }
}

//=============================================================================================================================

  if (isset($_POST['addCheckID4'])) { // for add button

    $details = "5th Part Commission";
    $receiveDate4 = $_POST['receive_date4'];

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no4");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date4");
      array_push($tableValue,$receiveDate4);
      $stringType .=  "s";
    }
    if ($ulOverrideNews) {
      array_push($tableName,"ul_balance");
      array_push($tableValue,$ulOverrideNews);
      $stringType .=  "s";
    }
    if ($uulOverrideNews) {
      array_push($tableName,"uul_balance");
      array_push($tableValue,$uulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuulOverrideNews) {
      array_push($tableName,"uuul_balance");
      array_push($tableValue,$uuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuulOverrideNews) {
      array_push($tableName,"uuuul_balance");
      array_push($tableValue,$uuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuuulOverrideNews) {
      array_push($tableName,"uuuuul_balance");
      array_push($tableValue,$uuuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($listerOverrideNews) {
      array_push($tableName,"lister_balance");
      array_push($tableValue,$listerOverrideNews);
      $stringType .=  "s";
    }
    if ($plOverrideNews) {
      array_push($tableName,"pl_balance");
      array_push($tableValue,$plOverrideNews);
      $stringType .=  "s";
    }
    if ($hosOverrideNews) {
      array_push($tableName,"hos_balance");
      array_push($tableValue,$hosOverrideNews);
      $stringType .=  "s";
    }
    if ($agentCommNews) {
      array_push($tableName,"agent_balance");
      array_push($tableValue,$agentCommNews);
      $stringType .=  "s";
    }
    // if ($totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo4() || !$totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo4()) {
    //   array_push($tableName,"total_bal_unclaim_amt");
    //   array_push($tableValue,$totalBalUnclaimAmt);
    //   $stringType .=  "s";
    // }
    // // if ($uulOverrideTotal || !$uulOverrideTotal) {
    // //   array_push($tableName,"uul_override");
    // //   array_push($tableValue,$uulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // // if ($ulOverrideTotal || !$ulOverrideTotal) {
    // //   array_push($tableName,"ul_override");
    // //   array_push($tableValue,$ulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // if ($totalClaimedDevAmt && !$loanDetails[0]->getCheckNo4()) {
    //   array_push($tableName,"total_claimed_dev_amt");
    //   array_push($tableValue,$totalClaimedDevAmt);
    //   $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
      header('Location: ../invoiceRecord.php?type=1');
    }

    if (!$loanDetails[0]->getCheckNo4()) {

    if ($upline1Status == 'Active') {
      if(addUplineCommission($conn, $idd, $purchaserName, $loanUid,$ulOverrideFinalOri, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo1,$upline1ID,$upline1,$branchType,$upline1Type,$sstCharges))
         {
              //$_SESSION['messageType'] = 1;
              // header('Location: ../invoiceRecord.php');
              //echo "register success";
         }
    }

   if ($upline2Status == 'Active') {
     if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid,$uulOverrideFinalOri, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo2,$upline2ID,$upline2,$branchType,$upline2Type,$sstCharges))
      {
      header('Location: ../invoiceRecord.php');
      }
   }
   if ($upline3Status == 'Active') {
     if (addUpup3lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuulOverrideFinalOri, $uuulOverrideFinal,$upline3,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo3,$upline3ID,$upline3,$branchType,$upline3Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if ($upline4Status == 'Active') {
     if (addUpup4lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuulOverrideFinalOri, $uuuulOverrideFinal,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo4,$upline4ID,$upline4,$branchType,$upline4Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if ($upline4Status == 'Active') {
     if (addUpup5lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuuulOverrideFinalOri, $uuuuulOverrideFinal,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo5,$upline5ID,$upline5,$branchType,$upline5Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid,$agentCommFinalOri, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoAgent,$agentId,$agent,$branchType,$agentType,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }

   foreach ($plNameExplode as $plNameFinal) {
     if ($plNameFinal && $plOverrideFinal) {
       $plId = md5(uniqid());
       $plNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($plNameFinal), "s");
       $plNameFinale = $plNameDetails[0]->getFullName();
       $icNoPl = $plNameDetails[0]->getIcNo();
       if (addPlCommission($conn, $idd3, $purchaserName, $loanUid,$plOverrideFinalOri, $plOverrideFinal,$plNameFinale,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoPl,$plId,$plNameFinale,$branchType,$plType,$sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }

   }
}
   if ($hosName && $hosOverrideFinal && $hosStatus == 'Active') {
     if (addHosCommission($conn, $idd3, $purchaserName, $loanUid,$hosOverrideFinalOri, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoHos,$hosId,$hosName,$branchType,$hosType,$sstCharges))
     {
       header('Location: ../invoiceRecord.php');
     }
   }
   if ($listerName && $listerOverrideFinal && $listerNameStatus == 'Active') {
     if (addListerCommission($conn, $idd3, $purchaserName, $loanUid,$listerOverrideFinalOri, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoLister,$listerId,$listerName,$branchType,$listerType,$sstCharges))
     {
       header('Location: ../invoiceRecord.php');
     }
   }
  }
}

//=============================================================================================================================

  if (isset($_POST['addCheckID5'])) { // for add button

    $details = "6th Part Commission";
    $receiveDate5 = $_POST['receive_date5'];

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no5");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date5");
      array_push($tableValue,$receiveDate5);
      $stringType .=  "s";
    }
    if ($ulOverrideNews) {
      array_push($tableName,"ul_balance");
      array_push($tableValue,$ulOverrideNews);
      $stringType .=  "s";
    }
    if ($uulOverrideNews) {
      array_push($tableName,"uul_balance");
      array_push($tableValue,$uulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuulOverrideNews) {
      array_push($tableName,"uuul_balance");
      array_push($tableValue,$uuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuulOverrideNews) {
      array_push($tableName,"uuuul_balance");
      array_push($tableValue,$uuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuuulOverrideNews) {
      array_push($tableName,"uuuuul_balance");
      array_push($tableValue,$uuuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($listerOverrideNews) {
      array_push($tableName,"lister_balance");
      array_push($tableValue,$listerOverrideNews);
      $stringType .=  "s";
    }
    if ($plOverrideNews) {
      array_push($tableName,"pl_balance");
      array_push($tableValue,$plOverrideNews);
      $stringType .=  "s";
    }
    if ($hosOverrideNews) {
      array_push($tableName,"hos_balance");
      array_push($tableValue,$hosOverrideNews);
      $stringType .=  "s";
    }
    if ($agentCommNews) {
      array_push($tableName,"agent_balance");
      array_push($tableValue,$agentCommNews);
      $stringType .=  "s";
    }
    // if ($totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo5() || !$totalBalUnclaimAmt && !$loanDetails[0]->getCheckNo5()) {
    //   array_push($tableName,"total_bal_unclaim_amt");
    //   array_push($tableValue,$totalBalUnclaimAmt);
    //   $stringType .=  "s";
    // }
    // // if ($uulOverrideTotal || !$uulOverrideTotal) {
    // //   array_push($tableName,"uul_override");
    // //   array_push($tableValue,$uulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // // if ($ulOverrideTotal || !$ulOverrideTotal) {
    // //   array_push($tableName,"ul_override");
    // //   array_push($tableValue,$ulOverrideTotal);
    // //   $stringType .=  "s";
    // // }
    // if ($totalClaimedDevAmt && !$loanDetails[0]->getCheckNo5()) {
    //   array_push($tableName,"total_claimed_dev_amt");
    //   array_push($tableValue,$totalClaimedDevAmt);
    //   $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
      header('Location: ../invoiceRecord.php?type=1');
    }

    if (!$loanDetails[0]->getCheckNo5()) {

    if ($upline1Status == 'Active') {
      if(addUplineCommission($conn, $idd, $purchaserName, $loanUid,$ulOverrideFinalOri, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo1,$upline1ID,$upline1,$branchType,$upline1Type,$sstCharges))
         {
              //$_SESSION['messageType'] = 1;
              // header('Location: ../invoiceRecord.php');
              //echo "register success";
         }
    }

   if ($upline2Status == 'Active') {
     if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid,$uulOverrideFinalOri, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo2,$upline2ID,$upline2,$branchType,$upline2Type,$sstCharges))
      {
      header('Location: ../invoiceRecord.php');
      }
   }
   if ($upline3Status == 'Active') {
     if (addUpup3lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuulOverrideFinalOri, $uuulOverrideFinal,$upline3,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo3,$upline3ID,$upline3,$branchType,$upline3Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if ($upline4Status == 'Active') {
     if (addUpup4lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuulOverrideFinalOri, $uuuulOverrideFinal,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo4,$upline4ID,$upline4,$branchType,$upline4Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if ($upline4Status == 'Active') {
     if (addUpup5lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuuulOverrideFinalOri, $uuuuulOverrideFinal,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo5,$upline5ID,$upline5,$branchType,$upline5Type,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }
   }
   if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid,$agentCommFinalOri, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoAgent,$agentId,$agent,$branchType,$agentType,$sstCharges))
   {
     header('Location: ../invoiceRecord.php');
   }

   foreach ($plNameExplode as $plNameFinal) {
     if ($plNameFinal && $plOverrideFinal) {
       $plId = md5(uniqid());
       $plNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($plNameFinal), "s");
       $plNameFinale = $plNameDetails[0]->getFullName();
       $icNoPl = $plNameDetails[0]->getIcNo();
       if (addPlCommission($conn, $idd3, $purchaserName, $loanUid,$plOverrideFinalOri, $plOverrideFinal,$plNameFinale,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoPl,$plId,$plNameFinale,$branchType,$plType,$sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }

   }
}
   if ($hosName && $hosOverrideFinal && $hosStatus == 'Active') {
     if (addHosCommission($conn, $idd3, $purchaserName, $loanUid,$hosOverrideFinalOri, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoHos,$hosId,$hosName,$branchType,$hosType,$sstCharges))
     {
       header('Location: ../invoiceRecord.php');
     }
   }
   if ($listerName && $listerOverrideFinal && $listerNameStatus == 'Active') {
     if (addListerCommission($conn, $idd3, $purchaserName, $loanUid,$listerOverrideFinalOri, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoLister,$listerId,$listerName,$branchType,$listerType,$sstCharges))
     {
       header('Location: ../invoiceRecord.php');
     }
   }
  }
}
}
//==============================================================================
if (isset($_POST['CreditNote'])) {
  $invoiceId = $_POST['CreditNote'];
  $claimStatus = $_POST['claimStatus'];

  $invoiceDetails = getInvoice($conn, "WHERE loan_uid =? AND claims_status =?",array("loan_uid,claims_status"),array($invoiceId,$claimStatus), "ss");
  if ($invoiceDetails) {
    $creditStatus = "";
    $invoiceId = $invoiceDetails[0]->getInvoiceId();
    $projectName = $invoiceDetails[0]->getProjectName();
    $unitNo = $invoiceDetails[0]->getUnitNo();
    $bookingDate = $invoiceDetails[0]->getBookingDate();
    $projectHandler = $invoiceDetails[0]->getProjectHandler();
    $projectHandlerDef = $invoiceDetails[0]->getProjectHandlerDefault();
    $itemAmount = $invoiceDetails[0]->getItem();
    $amount = $invoiceDetails[0]->getAmount();
    $charges = $invoiceDetails[0]->getCharges();
    $receiveStatus = $invoiceDetails[0]->getReceiveStatus();
    $claimStatus = $invoiceDetails[0]->getClaimsStatus();
    $checkID = $invoiceDetails[0]->getCheckId();
    $finalAmount = $invoiceDetails[0]->getFinalAmount();
    $bankAccHolder = $invoiceDetails[0]->getBankAccountHolder();
    $bankName = $invoiceDetails[0]->getBankName();
    $bankAccNo = $invoiceDetails[0]->getBankAccountNo();
    $branchType = $_SESSION['branch_type'];
    if (CreditNote($conn, $invoiceId,$projectName,$unitNo,$bookingDate,$projectHandler,$projectHandlerDef,$itemAmount,$amount,$charges,
                    $receiveStatus,$claimStatus,$checkID,$finalAmount,$bankAccHolder,$bankName,$bankAccNo,$branchType,$creditStatus)) {
      $_SESSION['messageType'] = 1;
      header('Location: ../invoiceRecordCredit.php');
    }
  }
}
