<?php

function createSimpleNoticeModal(){
    echo '<div id="noticeModal" class="noticeModal">
            <!-- Modal content -->
            <div class="modal-content">
                <span class="closeNoticeModal" id="closeNoticeModal">&times;</span>
                <div class="row">
                    <div class="col"></div>
                    <div class="col-xl-10">
                        <h4 class="modalHeader" id="noticeHeader"></h5>
                        <p id="noticeText" style="color: black;"></p>
                    </div>
                    <div class="col"></div>
                </div>
            </div>
        </div>';
}