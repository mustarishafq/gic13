<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/../adminAccess1.php';
require_once dirname(__FILE__) . '/../timezone.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/MultiInvoice.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function addUplineCommission($conn, $idd, $purchaserName, $loanUid,$ulOverrideNew, $ulOverride,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date", "receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd, $purchaserName, $loanUid,$ulOverrideNew, $ulOverride,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid,$uulOverrideNew, $uulOverride,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd2, $purchaserName, $loanUid,$uulOverrideNew, $uulOverride,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addUpup3lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuulOverrideNew, $uuuulOverride,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd2, $purchaserName, $loanUid,$uuuulOverrideNew, $uuuulOverride,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addUpup4lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuulOverrideNew, $uuuulOverride,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd2, $purchaserName, $loanUid,$uuuulOverrideNew, $uuuulOverride,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addUpup5lineCommission($conn, $idd2, $purchaserName, $loanUid,$uuuuulOverrideNew, $uulOverride,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd2, $purchaserName, $loanUid,$uuuuulOverrideNew, $uulOverride,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addAgentCommission($conn, $idd3, $purchaserName, $loanUid,$agentCommNew, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd3, $purchaserName, $loanUid,$agentCommNew, $agentCommFinal,$agent,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addPlCommission($conn, $idd3, $purchaserName, $loanUid,$plOverrideNew, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd3, $purchaserName, $loanUid,$plOverrideNew, $plOverrideFinal,$plName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addHosCommission($conn, $idd3, $purchaserName, $loanUid,$hosOverrideNew, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd3, $purchaserName, $loanUid,$hosOverrideNew, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function addListerCommission($conn, $idd3, $purchaserName, $loanUid,$listerOverrideNew, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges)
{
     if(insertDynamicData($conn,"commission", array("id","purchaser_name","loan_uid","def_commission","commission","upline","project_name","unit_no","booking_date","receive_status","details","payment_method","ic_no","commission_id","upline_default","branch_type","upline_type","charges"),
     array($idd3, $purchaserName, $loanUid,$listerOverrideNew, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo,$uplineId,$uplineDefault,$branchType,$uplineType,$charges),
     "isssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
function CreditNote($conn, $invoiceId,$projectName,$unitNo,$bookingDate,$projectHandler,$projectHandlerDef,$itemAmount,$amount,$charges,
                $receiveStatus,$claimStatus,$checkID,$finalAmount,$bankAccHolder,$bankName,$bankAccNo,$branchType,$creditStatus)
{
     if(insertDynamicData($conn,"invoice_credit", array("credit_id","project_name","unit_no","booking_date","project_handler","project_handler_default","item","amount","charges","receive_status",
                                                          "claims_status","check_id","final_amount","bank_account_holder","bank_name","bank_account_no","branch_type","credit_status"),
     array($invoiceId,$projectName,$unitNo,$bookingDate,$projectHandler,$projectHandlerDef,$itemAmount,$amount,$charges,
                     $receiveStatus,$claimStatus,$checkID,$finalAmount,$bankAccHolder,$bankName,$bankAccNo,$branchType,$creditStatus),
     "ssssssssssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

//========================================================================================================================================================


if (isset($_POST['addCheckID']) || isset($_POST['removeCheckID'])) {
  $branchType = $_SESSION['branch_type'];
  $checkID = rewrite($_POST['check_id']);
  $receiveDate = rewrite($_POST['receive_date']);
  $receiveStatus = 'PENDING';
  $invoiceMultiId = rewrite($_POST['id']);
  $paymentMethod = "Online Transfer";
  $upline1Type = "Upline 1";
  $upline2Type = "Upline 2";
  $upline3Type = "Upline 3";
  $upline4Type = "Upline 4";
  $upline5Type = "Upline 5";
  $agentType = "Agent";
  $plType = "PL";
  $hosType = "HOS";
  $listerType = "Lister";

  $multiInvoiceDetails = getMultiInvoice($conn, "WHERE id = ?", array("id"), array($_POST['id']), "s");

  echo $unitDetails = $multiInvoiceDetails[0]->getUnitNo(); // value seperated by MongoCommandCursor
  $unitDetailsExplode = explode(",",$unitDetails);
  $sstCharges = $multiInvoiceDetails[0]->getCharges();

  $claimStatusDetails = $multiInvoiceDetails[0]->getClaimsStatus(); // value seperated by MongoCommandCursor
  $claimStatusExplode = explode(",",$claimStatusDetails);


  if ($unitDetailsExplode) {

    for ($cnt=0; $cnt <count($unitDetailsExplode) ; $cnt++) {

      $loanDetails = getLoanStatus($conn, "WHERE unit_no = ?", array("unit_no"), array($unitDetailsExplode[$cnt]), "s");
          $purchaserName = $loanDetails[0]->getPurchaserName();
          $unitNo =  $loanDetails[0]->getUnitNo();
          $loanUid = $loanDetails[0]->getLoanUid();
          $bookingDate = $loanDetails[0]->getBookingDate();

          $id = $loanDetails[0]->getId();
          $unitDetailsExplode[$cnt];
          $claimStatusExplode[$cnt];

      $projectName = $loanDetails[0]->getProjectName();
      $projectDetails = getProject($conn, "WHERE project_name = ?", array("project_name"), array($projectName), "s");
      $claimStatus = $projectDetails[0]->getProjectClaims();

      // $ulOverrideFinal = $loanDetails[0]->getUlOverride() / $projectClaims;
      // $uulOverrideFinal = $loanDetails[0]->getUulOverride() / $projectClaims;
      // $plOverrideFinal = $loanDetails[0]->getPlOverride() / $projectClaims;
      // $hosOverrideFinal = $loanDetails[0]->getHosOverride() / $projectClaims;
      // $listerOverrideFinal = $loanDetails[0]->getListerOverride() / $projectClaims;
      // $agentCommFinal = $loanDetails[0]->getAgentComm() - $agentAdvanced / $projectClaims;

      $agent = $loanDetails[0]->getAgent();
      $icNoAgentDet = getUser($conn,"WHERE full_name =?",array("full_name"),array($agent),"s");
      $icNoAgent = $icNoAgentDet[0]->getIcNo();
      $agentFullName = $icNoAgentDet[0]->getFullName();
      $agentCommImp = $loanDetails[0]->getAgentBalance();
      $agentCommExp = explode(",",$agentCommImp);
      $agentComm = $agentCommExp[0];
      $agentClaim = $agentCommExp[1];
      $claimStatus = $claimStatus - $agentClaim;
      // $agentAdvanced = 2000;
      // $agentCommDeduct = $agentComm - $agentAdvanced;
      // $agentCommFinal = ($agentComm - $agentAdvanced) / $projectClaims;
      $upline1ID = md5(uniqid());
      $upline2ID = md5(uniqid());
      $upline3ID = md5(uniqid());
      $upline4ID = md5(uniqid());
      $upline5ID = md5(uniqid());
      $hosId = md5(uniqid());
      $agentId = md5(uniqid());
      $listerId = md5(uniqid());


      $hosName = $loanDetails[0]->getHosName();
      $hosNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($hosName), "s");
      if ($hosNameDetails) {
        $hosName = $hosNameDetails[0]->getFullName();
        $icNoHos = $hosNameDetails[0]->getIcNo();
        $hosStatus = $hosNameDetails[0]->getStatus();
      }else {
        $icNoHos = "";
        $hosStatus = 'Inactive';
      }
      $listerName = $loanDetails[0]->getListerName();
      $icNoLister = "123";
      $listerNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($listerName), "s");
      if ($listerNameDetails) {
        $listerName = $listerNameDetails[0]->getFullName();
        $icNoLister = $listerNameDetails[0]->getIcNo();
        $listerNameStatus = $listerNameDetails[0]->getStatus();
        if (!$icNoLister) {
          $icNoLister = "";
          $listerNameStatus = 'Inactive';
        }
      }else {
        $icNoLister = "123";
        $listerName = $loanDetails[0]->getListerName();
      }

      $getUplineDetails = getUser($conn," WHERE full_name = ? ",array("full_name"),array($loanDetails[0]->getAgent()),"s");
      $agentUid = $getUplineDetails[0]->getUid();
      $agentReferralDetails = getReferralHistory($conn, "WHERE referral_id=?",array("referral_id"),array($agentUid), "s");
      $agentCurrentLevel = $agentReferralDetails[0]->getCurrentLevel();
      $agentFirstLevel = $agentCurrentLevel + 1;
      $uplineUidDetails = getTop10ReferrerOfUser($conn, $agentUid);
      if ($uplineUidDetails) {
        for ($i=0; $i <count($uplineUidDetails) ; $i++) {
          $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uplineUidDetails[$i]), "s");
          $fullName = $userDetails[0]->getFullName();
          $upline[] = $userDetails[0]->getFullName();

        }
      }
      $uplineImp = implode(",",$upline);
      $uplineExp = explode(",",$uplineImp);
      for ($i=0; $i <count($uplineExp) ; $i++) {
        if ($i == 0) {
          $upline1 = $uplineExp[$i];
        }else
        if ($i == 1){
          $upline2 = $uplineExp[$i];
        }else
        if ($i == 2){
          $upline3 = $uplineExp[$i];
        }else
        if ($i == 3){
          $upline4 = $uplineExp[$i];
        }else
        if ($i == 4){
          $upline5 = $uplineExp[$i];
        }
      }

      // $upline1 = $loanDetails[0]->getUpline1();
      $ulOverride = $loanDetails[0]->getUlOverride();
      $upline1Details = getUser($conn, "WHERE full_name =?", array("full_name"), array($upline1), "s");
      if ($upline1Details) {
        $icNo1 = $upline1Details[0]->getIcNo();
        $upline1Status = $upline1Details[0]->getStatus();
      }
      // $ulOverrideFinal = $ulOverride / $projectClaims;

      // $upline2 = $loanDetails[0]->getUpline2();
      $uulOverride = $loanDetails[0]->getUulOverride();
      $uuulOverride = $loanDetails[0]->getUuulOverride();
      $uuuulOverride = $loanDetails[0]->getUuuulOverride();
      $uuuuulOverride = $loanDetails[0]->getUuuuulOverride();
      $upline2 = $loanDetails[0]->getUpline2();
      $upline2Details = getUser($conn, "WHERE full_name =?", array("full_name"), array($upline2), "s");
      if ($upline2Details) {
        $icNo2 = $upline2Details[0]->getIcNo();
        $upline2Status = $upline2Details[0]->getStatus();
      }
      // $upline4 = $loanDetails[0]->getUpline4();
      $upline4Details = getUser($conn, "WHERE full_name =?", array("full_name"), array($upline4), "s");
      if ($upline4Details) {
        $icNo4 = $upline4Details[0]->getIcNo();
        $upline4Status = $upline4Details[0]->getStatus();
      }
      // $upline3 = $loanDetails[0]->getUpline3();
      $upline3Details = getUser($conn, "WHERE full_name =?", array("full_name"), array($upline3), "s");
      if ($upline3Details) {
        $icNo3 = $upline3Details[0]->getIcNo();
        $upline3Status = $upline3Details[0]->getStatus();
      }
      // $upline5 = $loanDetails[0]->getUpline5();
      $upline5Details = getUser($conn, "WHERE full_name =?", array("full_name"), array($upline5), "s");
      if ($upline5Details) {
        $icNo5 = $upline5Details[0]->getIcNo();
        $upline5Status = $upline5Details[0]->getStatus();
      }
      // $uulOverrideFinal = $uulOverride / $projectClaims;

      $plName = $loanDetails[0]->getPlName();
      $plNameExplode = explode(",",$plName);
      $plNameNo = count($plNameExplode);
      $plOverride = $loanDetails[0]->getPlOverride();
      $plOverride = $plOverride / $plNameNo;
      // $plOverrideFinal = $plOverride / $projectClaims;

      // $hosName = $loanDetails[0]->getHosName();
      $hosOverride = $loanDetails[0]->getHosOverride();
      // $hosOverrideFinal = $hosOverride / $projectClaims;

      // $listerName = $loanDetails[0]->getListerName();
      $listerOverride = $loanDetails[0]->getListerOverride();
      // $listerOverrideFinal = $listerOverride / $projectClaims;

      if ($sstCharges == 'NO') {

        $agentCommFinal = ($agentComm / $claimStatus) - ($agentComm / $claimStatus)*(6/100);
        $ulOverrideFinal = ($ulOverride / $claimStatus) - ($ulOverride / $claimStatus)*(6/100);
        $uulOverrideFinal = ($uulOverride / $claimStatus) - ($uulOverride / $claimStatus)*(6/100);
        $uuulOverrideFinal = ($uuulOverride / $claimStatus) - ($uuulOverride / $claimStatus)*(6/100);
        $uuuulOverrideFinal = ($uuuulOverride / $claimStatus) - ($uuuulOverride / $claimStatus)*(6/100);
        $uuuuulOverrideFinal = ($uuuuulOverride / $claimStatus) - ($uuuuulOverride / $claimStatus)*(6/100);
        $plOverrideFinal = ($plOverride / $claimStatus) - ($plOverride / $claimStatus)*(6/100);
        $hosOverrideFinal = ($hosOverride / $claimStatus) - ($hosOverride / $claimStatus)*(6/100);
        $listerOverrideFinal = ($listerOverride / $claimStatus) - ($listerOverride / $claimStatus)*(6/100);

      }elseif ($sstCharges == 'YES') {
        $agentCommFinal = $agentComm / $claimStatus;
        $ulOverrideFinal = $ulOverride / $claimStatus;
        $uulOverrideFinal = $uulOverride / $claimStatus;
        $uuulOverrideFinal = ($uuulOverride / $claimStatus);
        $uuuulOverrideFinal = ($uuuulOverride / $claimStatus);
        $uuuuulOverrideFinal = ($uuuuulOverride / $claimStatus);
        $hosOverrideFinal = $hosOverride / $claimStatus;
        $listerOverrideFinal = $listerOverride / $claimStatus;
      }

      $agentCommFinalOri = $agentComm / $claimStatus;
      $ulOverrideFinalOri = $ulOverride / $claimStatus;
      $uulOverrideFinalOri = $uulOverride / $claimStatus;
      $uuulOverrideFinalOri = ($uuulOverride / $claimStatus);
      $uuuulOverrideFinalOri = ($uuuulOverride / $claimStatus);
      $uuuuulOverrideFinalOri = ($uuuuulOverride / $claimStatus);
      $plOverrideFinalOri = ($plOverride / $claimStatus);
      $hosOverrideFinalOri = $hosOverride / $claimStatus;
      $listerOverrideFinalOri = $listerOverride / $claimStatus;

      $claimStatusNew = $agentClaim + 1;
      $receiveStatus = 'PENDING';

      $agentCommNew = $agentComm - $agentCommFinalOri;
      $agentCommNews = $agentCommNew.",".$claimStatusNew;

      $ulOverrideNew = $ulOverride - $ulOverrideFinalOri;
      $ulOverrideNews = $ulOverrideNew.",".$claimStatusNew;

      $uulOverrideNew = $uulOverride - $uulOverrideFinalOri;
      $uulOverrideNews = $uulOverrideNew.",".$claimStatusNew;

      $uuulOverrideNew = $uuulOverride - $uuulOverrideFinalOri;
      $uuulOverrideNews = $uuulOverrideNew.",".$claimStatusNew;

      $uuuulOverrideNew = $uuuulOverride - $uuuulOverrideFinalOri;
      $uuuulOverrideNews = $uuuulOverrideNew.",".$claimStatusNew;

      $uuuuulOverrideNew = $uuuuulOverride - $uuuuulOverrideFinalOri;
      $uuuuulOverrideNews = $uuuuulOverrideNew.",".$claimStatusNew;

      $plOverrideNew = $plOverride - $plOverrideFinalOri;
      $plOverrideNews = $plOverrideNew.",".$claimStatusNew;

      $listerOverrideNew = $listerOverride - $listerOverrideFinalOri;
      $listerOverrideNews = $listerOverrideNew.",".$claimStatusNew;

      $hosOverrideNew = $hosOverride - $hosOverrideFinalOri;
      $hosOverrideNews = $hosOverrideNew.",".$claimStatusNew;

    if (isset($_POST['addCheckID'])) {

  if ($claimStatusExplode[$cnt] == 1) {
    $details = "2nd Part Commission";
    // $receiveDate1 = date('d-m-Y');

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no1");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date1");
      array_push($tableValue,$receiveDate);
      $stringType .=  "s";
    }
    if ($ulOverrideNews) {
      array_push($tableName,"ul_balance");
      array_push($tableValue,$ulOverrideNews);
      $stringType .=  "s";
    }
    if ($uulOverrideNews) {
      array_push($tableName,"uul_balance");
      array_push($tableValue,$uulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuulOverrideNews) {
      array_push($tableName,"uuul_balance");
      array_push($tableValue,$uuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuulOverrideNews) {
      array_push($tableName,"uuuul_balance");
      array_push($tableValue,$uuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuuulOverrideNews) {
      array_push($tableName,"uuuuul_balance");
      array_push($tableValue,$uuuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($listerOverrideNews) {
      array_push($tableName,"lister_balance");
      array_push($tableValue,$listerOverrideNews);
      $stringType .=  "s";
    }
    if ($plOverrideNews) {
      array_push($tableName,"pl_balance");
      array_push($tableValue,$plOverrideNews);
      $stringType .=  "s";
    }
    if ($hosOverrideNews) {
      array_push($tableName,"hos_balance");
      array_push($tableValue,$hosOverrideNews);
      $stringType .=  "s";
    }
    if ($agentCommNews) {
      array_push($tableName,"agent_balance");
      array_push($tableValue,$agentCommNews);
      $stringType .=  "s";
    }

    // if ($totalClaimedDevAmt && !$loanDetails[0]->getCheckNo1()) {
    //   array_push($tableName,"total_claimed_dev_amt");
    //   array_push($tableValue,$totalClaimedDevAmt);
    //   $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";

    if ($checkID) {
      array_push($tableName,"check_id");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
    }
    if ($receiveDate) {
      array_push($tableName,"receive_date");
      array_push($tableValue,$receiveDate);
      $stringType .=  "s";
    }
    array_push($tableValue,$invoiceMultiId);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }
    if (!$loanDetails[0]->getCheckNo1()) {

      if ($upline1Status == 'Active') {
        if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinalOri, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo1,$upline1ID,$upline1,$branchType, $upline1Type, $sstCharges))
           {
                //$_SESSION['messageType'] = 1;
                // header('Location: ../invoiceRecordMulti.php?type=1');
                //echo "register success";
           }
      }

       if ($upline2Status == 'Active') {
         if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinalOri, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo2,$upline2ID,$upline2,$branchType, $upline2Type, $sstCharges))
       {
         header('Location: ../invoiceRecordMulti.php?type=1');
       }
       }
       if ($upline3Status == 'Active') {
         if (addUpup3lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuulOverrideFinalOri, $uuulOverrideFinal,$upline3,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo3,$upline3ID,$upline3,$branchType, $upline3Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if ($upline4Status == 'Active') {
         if (addUpup4lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuuulOverrideFinalOri, $uuuulOverrideFinal,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo4,$upline4ID,$upline4,$branchType, $upline4Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if ($upline5Status == 'Active') {
         if (addUpup5lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuuuulOverrideFinalOri, $uuuuulOverrideFinal,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo5,$upline5ID,$upline5,$branchType, $upline5Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinalOri, $agentCommFinal,$agentFullName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoAgent,$agentId,$agentFullName,$branchType, $agentType, $sstCharges))
       {
         header('Location: ../invoiceRecordMulti.php?type=1');
       }
       $plNameExplode = explode(",",$plName);
      for ($k=0; $k <count($plNameExplode) ; $k++){
         if ($plNameExplode[$k] && $plOverrideFinal) {
           $plId = md5(uniqid());
           $plNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($plNameExplode[$k]), "s");
           $plNameFinale = $plNameDetails[0]->getFullName();
           $icNoPl = $plNameDetails[0]->getIcNo();
           if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinalOri, $plOverrideFinal,$plNameFinale,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoPl,$plId,$plNameFinale,$branchType, $plType, $sstCharges))
           {
             header('Location: ../invoiceRecordMulti.php?type=1');
           }

       }
       }
       if ($hosName && $hosOverrideFinal && $hosStatus == 'Active') {
         if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinalOri, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoHos,$hosId,$hosName,$branchType, $hosType, $sstCharges))
         {
           header('Location: ../invoiceRecordMulti.php?type=1');
         }
       }
       if ($listerName && $listerOverrideFinal && $listerNameStatus == 'Active') {
         if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinalOri, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoLister,$listerId,$listerName,$branchType, $hosType, $sstCharges))
         {
           $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
         }
       }


    }
  }
  if ($claimStatusExplode[$cnt] == 2) {
    $details = "3rd Part Commission";
    // $receiveDate1 = date('d-m-Y');

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no2");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date2");
      array_push($tableValue,$receiveDate);
      $stringType .=  "s";
    }
    if ($ulOverrideNews) {
      array_push($tableName,"ul_balance");
      array_push($tableValue,$ulOverrideNews);
      $stringType .=  "s";
    }
    if ($uulOverrideNews) {
      array_push($tableName,"uul_balance");
      array_push($tableValue,$uulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuulOverrideNews) {
      array_push($tableName,"uuul_balance");
      array_push($tableValue,$uuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuulOverrideNews) {
      array_push($tableName,"uuuul_balance");
      array_push($tableValue,$uuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuuulOverrideNews) {
      array_push($tableName,"uuuuul_balance");
      array_push($tableValue,$uuuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($listerOverrideNews) {
      array_push($tableName,"lister_balance");
      array_push($tableValue,$listerOverrideNews);
      $stringType .=  "s";
    }
    if ($plOverrideNews) {
      array_push($tableName,"pl_balance");
      array_push($tableValue,$plOverrideNews);
      $stringType .=  "s";
    }
    if ($hosOverrideNews) {
      array_push($tableName,"hos_balance");
      array_push($tableValue,$hosOverrideNews);
      $stringType .=  "s";
    }
    if ($agentCommNews) {
      array_push($tableName,"agent_balance");
      array_push($tableValue,$agentCommNews);
      $stringType .=  "s";
    }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }
    if ($checkID) {
      array_push($tableName,"check_id");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
    }
    array_push($tableValue,$invoiceMultiId);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";

    if ($checkID) {
      array_push($tableName,"check_id");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
    }
    if ($receiveDate) {
      array_push($tableName,"receive_date");
      array_push($tableValue,$receiveDate);
      $stringType .=  "s";
    }
    array_push($tableValue,$invoiceMultiId);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }
    if (!$loanDetails[0]->getCheckNo2()) {

      if ($upline1Status == 'Active') {
        if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinalOri, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo1,$upline1ID,$upline1,$branchType, $upline1Type, $sstCharges))
           {
                //$_SESSION['messageType'] = 1;
                // header('Location: ../invoiceRecordMulti.php?type=1');
                //echo "register success";
           }
      }

       if ($upline2Status == 'Active') {
         if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinalOri, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo2,$upline2ID,$upline2,$branchType, $upline2Type, $sstCharges))
       {
         header('Location: ../invoiceRecordMulti.php?type=1');
       }
       }
       if ($upline3Status == 'Active') {
         if (addUpup3lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuulOverrideFinalOri, $uuulOverrideFinal,$upline3,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo3,$upline3ID,$upline3,$branchType, $upline3Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if ($upline4Status == 'Active') {
         if (addUpup4lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuuulOverrideFinalOri, $uuuulOverrideFinal,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo4,$upline4ID,$upline4,$branchType, $upline4Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if ($upline5Status == 'Active') {
         if (addUpup5lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuuuulOverrideFinalOri, $uuuuulOverrideFinal,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo5,$upline5ID,$upline5,$branchType, $upline5Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinalOri, $agentCommFinal,$agentFullName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoAgent,$agentId,$agentFullName,$branchType, $agentType, $sstCharges))
       {
         header('Location: ../invoiceRecordMulti.php?type=1');
       }
       $plNameExplode = explode(",",$plName);
      for ($k=0; $k <count($plNameExplode) ; $k++){
         if ($plNameExplode[$k] && $plOverrideFinal) {
           $plId = md5(uniqid());
           $plNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($plNameExplode[$k]), "s");
           $plNameFinale = $plNameDetails[0]->getFullName();
           $icNoPl = $plNameDetails[0]->getIcNo();
           if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinalOri, $plOverrideFinal,$plNameFinale,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoPl,$plId,$plNameFinale,$branchType, $plType, $sstCharges))
           {
             header('Location: ../invoiceRecordMulti.php?type=1');
           }

       }
       }
       if ($hosName && $hosOverrideFinal && $hosStatus == 'Active') {
         if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinalOri, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoHos,$hosId,$hosName,$branchType, $hosType, $sstCharges))
         {
           header('Location: ../invoiceRecordMulti.php?type=1');
         }
       }
       if ($listerName && $listerOverrideFinal && $listerNameStatus == 'Active') {
         if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinalOri, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoLister,$listerId,$listerName,$branchType, $hosType, $sstCharges))
         {
           $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
         }
       }


    }
  }
  if ($claimStatusExplode[$cnt] == 3) {
    $details = "4th Part Commission";
    // $receiveDate1 = date('d-m-Y');

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no3");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date3");
      array_push($tableValue,$receiveDate);
      $stringType .=  "s";
    }
    if ($ulOverrideNews) {
      array_push($tableName,"ul_balance");
      array_push($tableValue,$ulOverrideNews);
      $stringType .=  "s";
    }
    if ($uulOverrideNews) {
      array_push($tableName,"uul_balance");
      array_push($tableValue,$uulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuulOverrideNews) {
      array_push($tableName,"uuul_balance");
      array_push($tableValue,$uuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuulOverrideNews) {
      array_push($tableName,"uuuul_balance");
      array_push($tableValue,$uuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuuulOverrideNews) {
      array_push($tableName,"uuuuul_balance");
      array_push($tableValue,$uuuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($listerOverrideNews) {
      array_push($tableName,"lister_balance");
      array_push($tableValue,$listerOverrideNews);
      $stringType .=  "s";
    }
    if ($plOverrideNews) {
      array_push($tableName,"pl_balance");
      array_push($tableValue,$plOverrideNews);
      $stringType .=  "s";
    }
    if ($hosOverrideNews) {
      array_push($tableName,"hos_balance");
      array_push($tableValue,$hosOverrideNews);
      $stringType .=  "s";
    }
    if ($agentCommNews) {
      array_push($tableName,"agent_balance");
      array_push($tableValue,$agentCommNews);
      $stringType .=  "s";
    }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }
    if ($checkID) {
      array_push($tableName,"check_id");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
    }
    array_push($tableValue,$invoiceMultiId);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";

    if ($checkID) {
      array_push($tableName,"check_id");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
    }
    if ($receiveDate) {
      array_push($tableName,"receive_date");
      array_push($tableValue,$receiveDate);
      $stringType .=  "s";
    }
    array_push($tableValue,$invoiceMultiId);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }
    if (!$loanDetails[0]->getCheckNo3()) {

      if ($upline1Status == 'Active') {
        if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinalOri, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo1,$upline1ID,$upline1,$branchType, $upline1Type, $sstCharges))
           {
                //$_SESSION['messageType'] = 1;
                // header('Location: ../invoiceRecordMulti.php?type=1');
                //echo "register success";
           }
      }

       if ($upline2Status == 'Active') {
         if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinalOri, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo2,$upline2ID,$upline2,$branchType, $upline2Type, $sstCharges))
       {
         header('Location: ../invoiceRecordMulti.php?type=1');
       }
       }
       if ($upline3Status == 'Active') {
         if (addUpup3lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuulOverrideFinalOri, $uuulOverrideFinal,$upline3,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo3,$upline3ID,$upline3,$branchType, $upline3Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if ($upline4Status == 'Active') {
         if (addUpup4lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuuulOverrideFinalOri, $uuuulOverrideFinal,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo4,$upline4ID,$upline4,$branchType, $upline4Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if ($upline5Status == 'Active') {
         if (addUpup5lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuuuulOverrideFinalOri, $uuuuulOverrideFinal,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo5,$upline5ID,$upline5,$branchType, $upline5Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinalOri, $agentCommFinal,$agentFullName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoAgent,$agentId,$agentFullName,$branchType, $agentType, $sstCharges))
       {
         header('Location: ../invoiceRecordMulti.php?type=1');
       }
       $plNameExplode = explode(",",$plName);
      for ($k=0; $k <count($plNameExplode) ; $k++){
         if ($plNameExplode[$k] && $plOverrideFinal) {
           $plId = md5(uniqid());
           $plNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($plNameExplode[$k]), "s");
           $plNameFinale = $plNameDetails[0]->getFullName();
           $icNoPl = $plNameDetails[0]->getIcNo();
           if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinalOri, $plOverrideFinal,$plNameFinale,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoPl,$plId,$plNameFinale,$branchType, $plType, $sstCharges))
           {
             header('Location: ../invoiceRecordMulti.php?type=1');
           }

       }
       }
       if ($hosName && $hosOverrideFinal && $hosStatus == 'Active') {
         if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinalOri, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoHos,$hosId,$hosName,$branchType, $hosType, $sstCharges))
         {
           header('Location: ../invoiceRecordMulti.php?type=1');
         }
       }
       if ($listerName && $listerOverrideFinal && $listerNameStatus == 'Active') {
         if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinalOri, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoLister,$listerId,$listerName,$branchType, $hosType, $sstCharges))
         {
           $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
         }
       }
    }
  }
  if ($claimStatusExplode[$cnt] == 4) {
    $details = "5th Part Commission";
    // $receiveDate = date('d-m-Y');

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no4");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date4");
      array_push($tableValue,$receiveDate);
      $stringType .=  "s";
    }
    if ($ulOverrideNews) {
      array_push($tableName,"ul_balance");
      array_push($tableValue,$ulOverrideNews);
      $stringType .=  "s";
    }
    if ($uulOverrideNews) {
      array_push($tableName,"uul_balance");
      array_push($tableValue,$uulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuulOverrideNews) {
      array_push($tableName,"uuul_balance");
      array_push($tableValue,$uuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuulOverrideNews) {
      array_push($tableName,"uuuul_balance");
      array_push($tableValue,$uuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuuulOverrideNews) {
      array_push($tableName,"uuuuul_balance");
      array_push($tableValue,$uuuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($listerOverrideNews) {
      array_push($tableName,"lister_balance");
      array_push($tableValue,$listerOverrideNews);
      $stringType .=  "s";
    }
    if ($plOverrideNews) {
      array_push($tableName,"pl_balance");
      array_push($tableValue,$plOverrideNews);
      $stringType .=  "s";
    }
    if ($hosOverrideNews) {
      array_push($tableName,"hos_balance");
      array_push($tableValue,$hosOverrideNews);
      $stringType .=  "s";
    }
    if ($agentCommNews) {
      array_push($tableName,"agent_balance");
      array_push($tableValue,$agentCommNews);
      $stringType .=  "s";
    }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }
    if ($checkID) {
      array_push($tableName,"check_id");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
    }
    array_push($tableValue,$invoiceMultiId);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";

    if ($checkID) {
      array_push($tableName,"check_id");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
    }
    if ($receiveDate) {
      array_push($tableName,"receive_date");
      array_push($tableValue,$receiveDate);
      $stringType .=  "s";
    }
    array_push($tableValue,$invoiceMultiId);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }
    if (!$loanDetails[0]->getCheckNo4()) {

      if ($upline1Status == 'Active') {
        if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinalOri, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo1,$upline1ID,$upline1,$branchType, $upline1Type, $sstCharges))
           {
                //$_SESSION['messageType'] = 1;
                // header('Location: ../invoiceRecordMulti.php?type=1');
                //echo "register success";
           }
      }

       if ($upline2Status == 'Active') {
         if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinalOri, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo2,$upline2ID,$upline2,$branchType, $upline2Type, $sstCharges))
       {
         header('Location: ../invoiceRecordMulti.php?type=1');
       }
       }
       if ($upline3Status == 'Active') {
         if (addUpup3lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuulOverrideFinalOri, $uuulOverrideFinal,$upline3,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo3,$upline3ID,$upline3,$branchType, $upline3Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if ($upline4Status == 'Active') {
         if (addUpup4lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuuulOverrideFinalOri, $uuuulOverrideFinal,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo4,$upline4ID,$upline4,$branchType, $upline4Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if ($upline5Status == 'Active') {
         if (addUpup5lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuuuulOverrideFinalOri, $uuuuulOverrideFinal,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo5,$upline5ID,$upline5,$branchType, $upline5Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinalOri, $agentCommFinal,$agentFullName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoAgent,$agentId,$agentFullName,$branchType, $agentType, $sstCharges))
       {
         header('Location: ../invoiceRecordMulti.php?type=1');
       }
       $plNameExplode = explode(",",$plName);
      for ($k=0; $k <count($plNameExplode) ; $k++){
         if ($plNameExplode[$k] && $plOverrideFinal) {
           $plId = md5(uniqid());
           $plNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($plNameExplode[$k]), "s");
           $plNameFinale = $plNameDetails[0]->getFullName();
           $icNoPl = $plNameDetails[0]->getIcNo();
           if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinalOri, $plOverrideFinal,$plNameFinale,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoPl,$plId,$plNameFinale,$branchType, $plType, $sstCharges))
           {
             header('Location: ../invoiceRecordMulti.php?type=1');
           }

       }
       }
       if ($hosName && $hosOverrideFinal && $hosStatus == 'Active') {
         if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinalOri, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoHos,$hosId,$hosName,$branchType, $hosType, $sstCharges))
         {
           header('Location: ../invoiceRecordMulti.php?type=1');
         }
       }
       if ($listerName && $listerOverrideFinal && $listerNameStatus == 'Active') {
         if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinalOri, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoLister,$listerId,$listerName,$branchType, $hosType, $sstCharges))
         {
           $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
         }
       }


    }
  }
  if ($claimStatusExplode[$cnt] == 5) {
    $details = "6th Part Commission";
    // $receiveDate1 = date('d-m-Y');

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($checkID)
    {
        array_push($tableName,"check_no5");
        array_push($tableValue,$checkID);
        $stringType .=  "s";
    }
    if ($checkID) {
      array_push($tableName,"receive_date5");
      array_push($tableValue,$receiveDate);
      $stringType .=  "s";
    }
    if ($ulOverrideNews) {
      array_push($tableName,"ul_balance");
      array_push($tableValue,$ulOverrideNews);
      $stringType .=  "s";
    }
    if ($uulOverrideNews) {
      array_push($tableName,"uul_balance");
      array_push($tableValue,$uulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuulOverrideNews) {
      array_push($tableName,"uuul_balance");
      array_push($tableValue,$uuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuulOverrideNews) {
      array_push($tableName,"uuuul_balance");
      array_push($tableValue,$uuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($uuuuulOverrideNews) {
      array_push($tableName,"uuuuul_balance");
      array_push($tableValue,$uuuuulOverrideNews);
      $stringType .=  "s";
    }
    if ($listerOverrideNews) {
      array_push($tableName,"lister_balance");
      array_push($tableValue,$listerOverrideNews);
      $stringType .=  "s";
    }
    if ($plOverrideNews) {
      array_push($tableName,"pl_balance");
      array_push($tableValue,$plOverrideNews);
      $stringType .=  "s";
    }
    if ($hosOverrideNews) {
      array_push($tableName,"hos_balance");
      array_push($tableValue,$hosOverrideNews);
      $stringType .=  "s";
    }
    if ($agentCommNews) {
      array_push($tableName,"agent_balance");
      array_push($tableValue,$agentCommNews);
      $stringType .=  "s";
    }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }
    if ($checkID) {
      array_push($tableName,"check_id");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
    }
    array_push($tableValue,$invoiceMultiId);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";

    if ($checkID) {
      array_push($tableName,"check_id");
      array_push($tableValue,$checkID);
      $stringType .=  "s";
    }
    if ($receiveDate) {
      array_push($tableName,"receive_date");
      array_push($tableValue,$receiveDate);
      $stringType .=  "s";
    }
    array_push($tableValue,$invoiceMultiId);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
    }
    if (!$loanDetails[0]->getCheckNo5()) {

      if ($upline1Status == 'Active') {
        if(addUplineCommission($conn, $idd, $purchaserName, $loanUid, $ulOverrideFinalOri, $ulOverrideFinal,$upline1,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo1,$upline1ID,$upline1,$branchType, $upline1Type, $sstCharges))
           {
                //$_SESSION['messageType'] = 1;
                // header('Location: ../invoiceRecordMulti.php?type=1');
                //echo "register success";
           }
      }

       if ($upline2Status == 'Active') {
         if (addUpuplineCommission($conn, $idd2, $purchaserName, $loanUid, $uulOverrideFinalOri, $uulOverrideFinal,$upline2,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo2,$upline2ID,$upline2,$branchType, $upline2Type, $sstCharges))
       {
         header('Location: ../invoiceRecordMulti.php?type=1');
       }
       }
       if ($upline3Status == 'Active') {
         if (addUpup3lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuulOverrideFinalOri, $uuulOverrideFinal,$upline3,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo3,$upline3ID,$upline3,$branchType, $upline3Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if ($upline4Status == 'Active') {
         if (addUpup4lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuuulOverrideFinalOri, $uuuulOverrideFinal,$upline4,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo4,$upline4ID,$upline4,$branchType, $upline4Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if ($upline5Status == 'Active') {
         if (addUpup5lineCommission($conn, $idd2, $purchaserName, $loanUid, $uuuuulOverrideFinalOri, $uuuuulOverrideFinal,$upline5,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNo5,$upline5ID,$upline5,$branchType, $upline5Type, $sstCharges))
       {
         header('Location: ../invoiceRecord.php');
       }
       }
       if (addAgentCommission($conn, $idd3, $purchaserName, $loanUid, $agentCommFinalOri, $agentCommFinal,$agentFullName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoAgent,$agentId,$agentFullName,$branchType, $agentType, $sstCharges))
       {
         header('Location: ../invoiceRecordMulti.php?type=1');
       }
       $plNameExplode = explode(",",$plName);
      for ($k=0; $k <count($plNameExplode) ; $k++){
         if ($plNameExplode[$k] && $plOverrideFinal) {
           $plId = md5(uniqid());
           $plNameDetails = getUser($conn, "WHERE full_name =?", array("full_name"), array($plNameExplode[$k]), "s");
           $plNameFinale = $plNameDetails[0]->getFullName();
           $icNoPl = $plNameDetails[0]->getIcNo();
           if (addPlCommission($conn, $idd3, $purchaserName, $loanUid, $plOverrideFinalOri, $plOverrideFinal,$plNameFinale,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoPl,$plId,$plNameFinale,$branchType, $plType, $sstCharges))
           {
             header('Location: ../invoiceRecordMulti.php?type=1');
           }

       }
       }
       if ($hosName && $hosOverrideFinal && $hosStatus == 'Active') {
         if (addHosCommission($conn, $idd3, $purchaserName, $loanUid, $hosOverrideFinalOri, $hosOverrideFinal,$hosName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoHos,$hosId,$hosName,$branchType, $hosType, $sstCharges))
         {
           header('Location: ../invoiceRecordMulti.php?type=1');
         }
       }
       if ($listerName && $listerOverrideFinal && $listerNameStatus == 'Active') {
         if (addListerCommission($conn, $idd3, $purchaserName, $loanUid, $listerOverrideFinalOri, $listerOverrideFinal,$listerName,$projectName,$unitNo,$bookingDate, $receiveStatus, $details,$paymentMethod,$icNoLister,$listerId,$listerName,$branchType, $hosType, $sstCharges))
         {
           $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php?type=1');
         }
       }


    }
  }







    }//========================================================================================================================================================
    if (isset($_POST['removeCheckID'])) { // for remove button
      // $receiveStatus = 'PENDING';
      $checkID = null;
      $receiveDate = null;

      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      // //echo "save to database";
      // if(!$checkID1)
      // {
      //     array_push($tableName,"check_no1");
      //     array_push($tableValue,$checkID1);
      //     $stringType .=  "s";
      // }
      if(!$receiveDate)
      {
          array_push($tableName,"receive_date");
          array_push($tableValue,$receiveDate);
          $stringType .=  "s";
      }
      // if($totalClaimedDevAmtRestore || !$totalClaimedDevAmtRestore)
      // {
      //     array_push($tableName,"total_claimed_dev_amt");
      //     array_push($tableValue,$totalClaimedDevAmtRestore);
      //     $stringType .=  "s";
      // }
      // if($totalBalUnclaimAmtRestore || !$totalBalUnclaimAmtRestore)
      // {
      //     array_push($tableName,"total_bal_unclaim_amt");
      //     array_push($tableValue,$totalBalUnclaimAmtRestore);
      //     $stringType .=  "s";
      // }
      array_push($tableValue,$invoiceMultiId);
      $stringType .=  "s";
      $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE id = ? ",$tableName,$tableValue,$stringType);

      if($withdrawUpdated)
      {
        $_SESSION['messageType'] = 1;
  header('Location: ../invoiceRecordMulti.php');
      }
      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
         $stringType .=  "s";
      // }
      if(!$receiveDate)
      {
          array_push($tableName,"receive_date");
          array_push($tableValue,$receiveDate);
          $stringType .=  "s";
      }
      array_push($tableValue,$id);
      $stringType .=  "s";
      $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

      if($withdrawUpdated)
      {
        $_SESSION['messageType'] = 1;
        header('Location: ../invoiceRecordMulti.php');
      }
    }
  }
  }
}elseif (isset($_POST['CreditNote'])) {
  $invoiceId = $_POST['CreditNote'];

  $invoiceMultiDetails = getMultiInvoice($conn, "WHERE invoice_id =?",array("invoice_id"),array($invoiceId), "s");
  if ($invoiceMultiDetails) {
    $creditStatus = "";
    $projectName = $invoiceMultiDetails[0]->getProjectName();
    $unitNo = $invoiceMultiDetails[0]->getUnitNo();
    $bookingDate = $invoiceMultiDetails[0]->getBookingDate();
    $projectHandler = $invoiceMultiDetails[0]->getProjectHandler();
    $projectHandlerDef = $invoiceMultiDetails[0]->getProjectHandlerDefault();
    $itemAmount = $invoiceMultiDetails[0]->getItem();
    $amount = $invoiceMultiDetails[0]->getAmount();
    $charges = $invoiceMultiDetails[0]->getCharges();
    $receiveStatus = $invoiceMultiDetails[0]->getReceiveStatus();
    $claimStatus = $invoiceMultiDetails[0]->getClaimsStatus();
    $checkID = $invoiceMultiDetails[0]->getCheckId();
    $finalAmount = $invoiceMultiDetails[0]->getFinalAmount();
    $bankAccHolder = $invoiceMultiDetails[0]->getBankAccountHolder();
    $bankName = $invoiceMultiDetails[0]->getBankName();
    $bankAccNo = $invoiceMultiDetails[0]->getBankAccountNo();
    $branchType = $_SESSION['branch_type'];
    if (CreditNote($conn, $invoiceId,$projectName,$unitNo,$bookingDate,$projectHandler,$projectHandlerDef,$itemAmount,$amount,$charges,
                    $receiveStatus,$claimStatus,$checkID,$finalAmount,$bankAccHolder,$bankName,$bankAccNo,$branchType,$creditStatus)) {
      $_SESSION['messageType'] = 1;
      header('Location: ../invoiceRecordCredit.php');
    }
  }
}
?>
