
<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
// require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addNewInvoiceSingle($conn,$invoiceId,$purchaserName,$loanUid,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$projectHandlerDefault,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$claimStatusImplode,$branchType,$item,$creditStatus)
{
    if(insertDynamicData($conn,"invoice",array("invoice_id","purchaser_name","loan_uid","unit_no","amount","project_name","booking_date","project_handler","project_handler_default","charges","bank_account_holder","bank_name","bank_account_no","invoice_name","invoice_type","final_amount","receive_status","claims_status","branch_type","item","credit_status"),
    array($invoiceId,$purchaserName,$loanUid,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$projectHandlerDefault,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$claimStatusImplode,$branchType,$item,$creditStatus),
    "sssssssssssssssssssss") === null)
    {
    //    echo $finalPassword;
    }
    else
    {
    //   echo "bbbb";
    }
    return true;
}
function addNewInvoice($conn,$invoiceId,$idArray,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$projectHandlerDefault,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$claimStatusImplode,$branchType,$item,$creditStatus)
{
    if(insertDynamicData($conn,"invoice_multi",array("invoice_id","loan_id","unit_no","amount","project_name","booking_date","project_handler","project_handler_default","charges","bank_account_holder","bank_name","bank_account_no","invoice_name","invoice_type","final_amount","receive_status","claims_status","branch_type","item","credit_status"),
    array(  $invoiceId,$idArray,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$projectHandlerDefault,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$claimStatusImplode,$branchType,$item,$creditStatus),
    "ssssssssssssssssssss") === null)
    {
    //    echo $finalPassword;
    }
    else
    {
    //   echo "bbbb";
    }
    return true;
}
function addNewInvoiceProforma($conn,$proformaId,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$projectHandlerDefault,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$claimStatusImplode,$branchType,$itemAmount)
{
    if(insertDynamicData($conn,"invoice_proforma",array("proforma_id","unit_no","amount","project_name","booking_date","project_handler","project_handler_default","charges","bank_account_holder","bank_name","bank_account_no","invoice_name","invoice_type","final_amount","receive_status","claims_status","branch_type","item"),
    array(  $proformaId,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$projectHandlerDefault,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$claimStatusImplode,$branchType,$itemAmount),
    "ssssssssssssssssss") === null)
    {
    //    echo $finalPassword;
    }
    else
    {
    //   echo "bbbb";
    }
    return true;
}
function addNewInvoiceCredit($conn,$proformaId,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$projectHandlerDefault,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$claimStatusImplode,$branchType,$itemAmount)
{
    if(insertDynamicData($conn,"invoice_credit",array("credit_id","unit_no","amount","project_name","booking_date","project_handler","project_handler_default","charges","bank_account_holder","bank_name","bank_account_no","invoice_name","invoice_type","final_amount","receive_status","claims_status","branch_type","item"),
    array(  $proformaId,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$projectHandlerDefault,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$claimStatusImplode,$branchType,$itemAmount),
    "ssssssssssssssssss") === null)
    {
    //    echo $finalPassword;
    }
    else
    {
    //   echo "bbbb";
    }
    return true;
}
function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter,$branchType)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after","branch_type"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter,$branchType),
     "ssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();
     $finalAmount0 = 0;
     $finalAmount1 = 0;
     $finalAmount2 = 0;
     $finalAmount3 = 0;
     $finalAmount4 = 0;

    $branchType = $_SESSION['branch_type'];
    $invoiceId = md5(uniqid());
    $username = $_SESSION['username'];
    $creditStatus = "";
    //add in oriject, to status, date,invoice,invoice type
    $projectName = rewrite($_POST['project_name']);
    $statusOfClaims = 0;
    $proformaId = md5(uniqid());

    //auto get project handler
    $projectDetails = getProject($conn," WHERE project_name = ? ",array("project_name"),array($projectName),"s");
    $projectHandler = $projectDetails[0]->getAddProjectPpl();
    $projectClaims = $projectDetails[0]->getProjectClaims();


    $receiveStatus = 'PENDING';
    $dateOfClaims = rewrite($_POST['date_of_claims']);
    $invoiceName = rewrite($_POST['invoice_name']);

    $invoiceType = rewrite($_POST['invoice_type']);
    if (!$invoiceType)
    {
        $invoiceType = "";
    }

    $amount = ($_POST['amount']);
    $remove = array(""); //want to remove null value 1,,3 to 1,3
    $resultAmount = array_diff($amount, $remove); //want to remove null value 1,,3 to 1,3
    $amountLimit = count($resultAmount); //want to remove null value 1,,3 to 1,3
    $amountSlice = array_slice($amount,0,$amountLimit); //want to remove null value 1,,3 to 1,3
    $amountImplode = implode(",",$amountSlice); //want to remove null value 1,,3 to 1,3
    $amountExplode = explode(",",$amountImplode);

    $item = $amountImplode;

    $unit = ($_POST['unit']);
    $remove = array(""); //want to remove null value 1,,3 to 1,3
    $resultUnit = array_diff($unit, $remove); //want to remove null value 1,,3 to 1,3
    $unitLimit = count($resultUnit); //want to remove null value 1,,3 to 1,3
    $unitSlice = array_slice($unit,0,$unitLimit); //want to remove null value 1,,3 to 1,3
    $unitImplode = implode(",",$unitSlice); //want to remove null value 1,,3 to 1,3
    $unitExplode = explode(",",$unitImplode);

    if (count($unitExplode) > 1) {
    $status = 'MULTI';
  }else {
    $status = 'SINGLE';
  }

  for ($l=0; $l <count($unitExplode) ; $l++) {
    $loanUidDetails = getLoanStatus($conn, "WHERE unit_no=?",array("unit_no"),array($unitExplode[$l]), "s");
    $idArray[] = $loanUidDetails[0]->getId();
  }
  $idArray = implode(",",$idArray);


    // $project = rewrite($_POST['project']);
    $charges = rewrite($_POST['charges']); // temporary

    $bankAccountHolder = rewrite($_POST['bank_account_holder']);
    $bankName = rewrite($_POST['bank_name']);
    $bankAccountNo = rewrite($_POST['bank_account_no']);

    if(!$bankAccountHolder || !$bankName || !$bankAccountNo){
      $bankAccountHolder = 'GIC Holding';
      $bankName = 'Public Bank Sdn Bhd';
      $bankAccountNo = 123456789;
    }

      if (isset($_POST['upload'])) {

        for ($cnt=0; $cnt <count($unit) ; $cnt++) {
          $unit[$cnt];
          $amount[$cnt];
          // $item[$cnt];

              $loanDetailsUnit1 = getLoanStatus($conn, "WHERE unit_no =? ", array("unit_no"), array($unit[$cnt]), "s");

        if($loanDetailsUnit1){
        $loanUnit1 = $loanDetailsUnit1[0]->getLoanUid();

        if ($charges == 'YES')
        {
            $chargesNew = 0.06;
            $chargeAmount = $amount[$cnt] * $chargesNew;
            $finalAmountImpplode[] = $amount[$cnt] * 1.06;
            $amountImpplode[] = $amount[$cnt];
        }
        else
        {
            $chargesNew = 0;
            $chargeAmount = $amount[$cnt] * $chargesNew;
            $amountImpplode[] = $amount[$cnt] - $amount[$cnt]*(6/100);
            $finalAmountImpplode[] = $amount[$cnt];
        }

        $amountImplode = implode(",",$amountImpplode);
        $finalAmountImplode = implode(",",$finalAmountImpplode);

          $date = date('d-m-Y');
            $totalAmountBal = $loanDetailsUnit1[0]->getTotalBalUnclaimAmt() - $amount[$cnt];
            $totalAmountClaim = $loanDetailsUnit1[0]->getTotalClaimDevAmt() + $amount[$cnt];
            if (!$loanDetailsUnit1[0]->getClaimAmt1st() && !$loanDetailsUnit1[0]->getClaimAmt2nd() && !$loanDetailsUnit1[0]->getClaimAmt3rd() && !$loanDetailsUnit1[0]->getClaimAmt4th() && !$loanDetailsUnit1[0]->getClaimAmt5th() && $projectClaims >=1) {
            $statusOfClaims = 1;
            // if ($cnt == 0) {
            //   $statusOfClaims0 = $statusOfClaims;
            // }if ($cnt == 1) {
            //   $statusOfClaims1 = $statusOfClaims;
            // }if ($cnt == 2) {
            //   $statusOfClaims2 = $statusOfClaims;
            // }if ($cnt == 3) {
            //   $statusOfClaims3 = $statusOfClaims;
            // }if ($cnt == 4) {
            //   $statusOfClaims4 = $statusOfClaims;
            // }
            }
            if ($loanDetailsUnit1[0]->getClaimAmt1st() && !$loanDetailsUnit1[0]->getClaimAmt2nd() && !$loanDetailsUnit1[0]->getClaimAmt3rd() && !$loanDetailsUnit1[0]->getClaimAmt4th() && !$loanDetailsUnit1[0]->getClaimAmt5th() && $projectClaims >=2) {
            $statusOfClaims = 2;
            }
            if ($loanDetailsUnit1[0]->getClaimAmt1st() && $loanDetailsUnit1[0]->getClaimAmt2nd() && !$loanDetailsUnit1[0]->getClaimAmt3rd() && !$loanDetailsUnit1[0]->getClaimAmt4th() && !$loanDetailsUnit1[0]->getClaimAmt5th() && $projectClaims >=3) {
            $statusOfClaims = 3;
            }
            if ($loanDetailsUnit1[0]->getClaimAmt1st() && $loanDetailsUnit1[0]->getClaimAmt2nd() && $loanDetailsUnit1[0]->getClaimAmt3rd() && !$loanDetailsUnit1[0]->getClaimAmt4th() && !$loanDetailsUnit1[0]->getClaimAmt5th() && $projectClaims >=4) {
            $statusOfClaims = 4;
            }
            if ($loanDetailsUnit1[0]->getClaimAmt1st() && $loanDetailsUnit1[0]->getClaimAmt2nd() && $loanDetailsUnit1[0]->getClaimAmt3rd() && $loanDetailsUnit1[0]->getClaimAmt4th() && !$loanDetailsUnit1[0]->getClaimAmt5th() && $projectClaims >=5) {
            $statusOfClaims = 5;
            }

            $statusOfClaimsArray[] = $statusOfClaims;
            if ($invoiceName == 'Invoice') {

            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            // //echo "save to database";
            if($statusOfClaims == 1)
            {
                array_push($tableName,"1st_claim_amt");
                array_push($tableValue,$amount[$cnt]);
                $stringType .=  "i";

                array_push($tableName,"request_date1");
                array_push($tableValue,$date);
                $stringType .=  "s";

                array_push($tableName,"sst1");
                array_push($tableValue,$charges);
                $stringType .=  "s";

                array_push($tableName,"status_1st");
                array_push($tableValue,$status);
                $stringType .=  "s";
            }
            if($statusOfClaims == 2)
            {
                array_push($tableName,"2nd_claim_amt");
                array_push($tableValue,$amount[$cnt]);
                $stringType .=  "i";

                array_push($tableName,"request_date2");
                array_push($tableValue,$date);
                $stringType .=  "s";

                array_push($tableName,"sst2");
                array_push($tableValue,$charges);
                $stringType .=  "s";

                array_push($tableName,"status_2nd");
                array_push($tableValue,$status);
                $stringType .=  "s";
            }
            if($statusOfClaims == 3)
            {
                array_push($tableName,"3rd_claim_amt");
                array_push($tableValue,$amount[$cnt]);
                $stringType .=  "i";

                array_push($tableName,"request_date3");
                array_push($tableValue,$date);
                $stringType .=  "s";

                array_push($tableName,"sst3");
                array_push($tableValue,$charges);
                $stringType .=  "s";

                array_push($tableName,"status_3rd");
                array_push($tableValue,$status);
                $stringType .=  "s";
            }
            if($statusOfClaims == 4)
            {
                array_push($tableName,"4th_claim_amt");
                array_push($tableValue,$amount[$cnt]);
                $stringType .=  "i";

                array_push($tableName,"request_date4");
                array_push($tableValue,$date);
                $stringType .=  "s";

                array_push($tableName,"sst4");
                array_push($tableValue,$charges);
                $stringType .=  "s";

                array_push($tableName,"status_4th");
                array_push($tableValue,$status);
                $stringType .=  "s";
            }
            if($statusOfClaims == 5)
            {
                array_push($tableName,"5th_claim_amt");
                array_push($tableValue,$amount[$cnt]);
                $stringType .=  "i";

                array_push($tableName,"request_date5");
                array_push($tableValue,$date);
                $stringType .=  "s";

                array_push($tableName,"sst5");
                array_push($tableValue,$charges);
                $stringType .=  "s";

                array_push($tableName,"status_5th");
                array_push($tableValue,$status);
                $stringType .=  "s";
            }
            if($totalAmountBal || !$totalAmountBal)
            {
                array_push($tableName,"total_bal_unclaim_amt");
                array_push($tableValue,$totalAmountBal);
                $stringType .=  "i";
            }
            if($totalAmountClaim || !$totalAmountClaim)
            {
                array_push($tableName,"total_claimed_dev_amt");
                array_push($tableValue,$totalAmountClaim);
                $stringType .=  "i";
            }
            array_push($tableValue,$loanUnit1);
            $stringType .=  "s";
            $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE loan_uid = ? ",$tableName,$tableValue,$stringType);
                if($withdrawUpdated)
                {

                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../adminDashboard.php?type=1');
                }
                else
                {
                    echo "fail";
                }

              }
        }
        else
        {
            echo "dunno";
        }
      }
    // if ($finalAmount0 && !$finalAmount1 && !$finalAmount2 && !$finalAmount3 && !$finalAmount4) {
    // $finalAmountImplode = $finalAmount0;
    // }
    // elseif ($finalAmount0 && $finalAmount1 && !$finalAmount2 && !$finalAmount3 && !$finalAmount4) {
    // $finalAmountImplode = "$finalAmount0,$finalAmount1";
    // }
    // elseif ($finalAmount0 && $finalAmount1 && $finalAmount2 && !$finalAmount3 && !$finalAmount4) {
    // $finalAmountImplode = "$finalAmount0,$finalAmount1,$finalAmount2";
    // }
    // elseif ($finalAmount0 && $finalAmount1 && $finalAmount2 && $finalAmount3 && !$finalAmount4) {
    // $finalAmountImplode = "$finalAmount0,$finalAmount1,$finalAmount2,$finalAmount3";
    // }
    // elseif ($finalAmount0 && $finalAmount1 && $finalAmount2 && $finalAmount3 && $finalAmount4) {
    // $finalAmountImplode = "$finalAmount0,$finalAmount1,$finalAmount2,$finalAmount3,$finalAmount4";
    // }

    $claimStatusImplode = implode(",",$statusOfClaimsArray);


      $remove = array(""); //want to remove null value 1,,3 to 1,3
      $resultAmount = array_diff($amount, $remove); //want to remove null value 1,,3 to 1,3
      $amountLimit = count($resultAmount); //want to remove null value 1,,3 to 1,3
      $amountSlice = array_slice($amount,0,$amountLimit); //want to remove null value 1,,3 to 1,3

if ($invoiceName == 'Invoice') {
    if (count($unitExplode) > 1) {
      if(addNewInvoice($conn,$invoiceId,$idArray,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$projectHandler,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$claimStatusImplode,$branchType,$item,$creditStatus))
      {
          $_SESSION['messageType'] = 1;
          header('Location: ../invoiceRecordMulti.php?type=3');
          //echo "register success";
      }
    }else {
    $loanDetailsxx = getLoanStatus($conn, "WHERE unit_no=?",array("unit_no"),array($unitExplode[0]),"s");
    $loanUid = $loanDetailsxx[0]->getLoanUid();
    $purchaserName = $loanDetailsxx[0]->getPurchaserName();
      if(addNewInvoiceSingle($conn,$invoiceId,$purchaserName,$loanUid,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$projectHandler,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$claimStatusImplode,$branchType,$item,$creditStatus))
      {
          $_SESSION['messageType'] = 1;
          header('Location: ../invoiceRecord.php?type=3');
          //echo "register success";
      }
    }

    $column = "Issued";
    $a = "-";
    if(editHistory($conn, $username, $column, $invoiceId,$a, $a,$branchType))
         {}


    }
  }
  if ($invoiceName == 'Proforma') {
    // if ($status == 'MULTI') {
      if(addNewInvoiceProforma($conn,$proformaId,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$projectHandler,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$claimStatusImplode,$branchType,$item))
      {
          $_SESSION['messageType'] = 1;
          header('Location: ../invoiceRecordProforma.php?type=2');
          //echo "register success";
      }

      $column = "Issued";
      $a = "-";
      if(editHistory($conn, $username, $column, $proformaId,$a, $a,$branchType))
           {}
    // }else {
    // $loanDetailsxx = getLoanStatus($conn, "WHERE unit_no=?",array("unit_no"),array($unitExplode[0]),"s");
    // $loanUid = $loanDetailsxx[0]->getLoanUid();
    // $purchaserName = $loanDetailsxx[0]->getPurchaserName();
    //   if(addNewInvoiceSingle($conn,$invoiceId,$purchaserName,$loanUid,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$claimStatusImplode))
    //   {
    //       //$_SESSION['messageType'] = 1;
    //       header('Location: ../invoiceRecord.php');
    //       //echo "register success";
    //   }
    // }
  }
  if ($invoiceName == 'Credit Note') {
    // if ($status == 'MULTI') {
      if(addNewInvoiceCredit($conn,$proformaId,$unitImplode,$amountImplode,$projectName,$dateOfClaims,$projectHandler,$projectHandler,$charges,$bankAccountHolder,$bankName,$bankAccountNo,$invoiceName,$invoiceType,$finalAmountImplode,$receiveStatus,$claimStatusImplode,$branchType,$item))
      {
          $_SESSION['messageType'] = 1;
          header('Location: ../invoiceRecordCredit.php?type=2');
          //echo "register success";
      }

      $column = "Issued";
      $a = "-";
      if(editHistory($conn, $username, $column, $proformaId,$a, $a,$branchType))
           {}
  }



//============================================================================================================================

  // }


}
else
{
    //  header('Location: ../index.php');
}
?>
