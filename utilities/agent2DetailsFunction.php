<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();
$_SESSION['fullnameNew'] = null;
$agentName = ($_POST['agentNames']);
// $agentName= "Tan Ai Pheng";

$userDetails = getUser($conn,"WHERE full_name = ?",array("full_name"),array($agentName), "s");

if ($userDetails) {
  $fullName = $userDetails[0]->getFullName();
  $_SESSION['fullnameNew'] = $fullName;
  $userArray[] = array("agentNamed" => $fullName);
}

echo json_encode($userArray);

 ?>
