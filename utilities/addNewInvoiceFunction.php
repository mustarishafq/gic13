<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addNewInvoice($conn,$id,$invoiceId,$purchaserName,$projectName,$projectHandler,$projectHandlerDefault,$statusOfClaims,$dateOfClaims,$invoiceName,$invoiceType,
                        $itemAmount,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,
                            $loanUid,$charges,$receiveStatus,$unit,$invoiceUnitNo,$invoiceDateCreated,$branchType,$creditStatus)
{
    if(insertDynamicData($conn,"invoice",array("id","invoice_id","purchaser_name","project_name","project_handler","project_handler_default","claims_status","claims_date","invoice_name","invoice_type",
                            "item","amount","final_amount","bank_account_holder","bank_name","bank_account_no",
                            "loan_uid","charges", "receive_status","unit","unit_no","booking_date","branch_type","credit_status"),
    array($id,$invoiceId,$purchaserName,$projectName,$projectHandler,$projectHandlerDefault,$statusOfClaims,$dateOfClaims,$invoiceName,$invoiceType,
                            $itemAmount,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,
                                $loanUid,$charges,$receiveStatus,$unit,$invoiceUnitNo,$invoiceDateCreated,$branchType,$creditStatus),
    "isssssssssssssssssssssss") === null)
    {
    //    echo $finalPassword;
    }
    else
    {
    //   echo "bbbb";
    }
    return true;
}
function addNewInvoiceProforma($conn,$id,$proformaId,$projectName,$projectHandler,$projectHandlerDefault,$statusOfClaims,$invoiceName,$invoiceType,
                            $itemAmount,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,$charges,$receiveStatus,$invoiceUnitNo,$invoiceDateCreated,$branchType)
{
    if(insertDynamicData($conn,"invoice_proforma",array("id","proforma_id","project_name","project_handler","project_handler_default","claims_status","invoice_name","invoice_type",
                            "item","amount","final_amount","bank_account_holder","bank_name","bank_account_no","charges", "receive_status","unit_no","booking_date","branch_type"),
    array($id,$proformaId,$projectName,$projectHandler,$projectHandlerDefault,$statusOfClaims,$invoiceName,$invoiceType,
            $itemAmount,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,$charges,$receiveStatus,$invoiceUnitNo,$invoiceDateCreated,$branchType),
    "sssssssssssssssssss") === null)
    {
    //    echo $finalPassword;
    }
    else
    {
    //   echo "bbbb";
    }
    return true;
}
function addNewInvoiceCredit($conn,$id,$proformaId,$projectName,$projectHandler,$projectHandlerDefault,$statusOfClaims,$invoiceName,$invoiceType,
                            $itemAmount,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,$charges,$receiveStatus,$invoiceUnitNo,$invoiceDateCreated,$branchType)
{
    if(insertDynamicData($conn,"invoice_credit",array("id","credit_id","project_name","project_handler","project_handler_default","claims_status","invoice_name","invoice_type",
                            "item","amount","final_amount","bank_account_holder","bank_name","bank_account_no","charges", "receive_status","unit_no","booking_date","branch_type"),
    array($id,$proformaId,$projectName,$projectHandler,$projectHandlerDefault,$statusOfClaims,$invoiceName,$invoiceType,
            $itemAmount,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,$charges,$receiveStatus,$invoiceUnitNo,$invoiceDateCreated,$branchType),
    "sssssssssssssssssss") === null)
    {
    //    echo $finalPassword;
    }
    else
    {
    //   echo "bbbb";
    }
    return true;
}
function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter,$branchType)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after","branch_type"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter,$branchType),
     "ssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    // $id = rewrite($_POST["id"]);
    $branchType = $_SESSION['branch_type'];
    $loanUid = rewrite($_POST["loan_uid"]);
    $invoiceId = md5(uniqid());
    $loanStatusDetails = getLoanStatus($conn," WHERE loan_uid = ? ",array("loan_uid"),array($loanUid),"s");
    $invoiceUnitNo = $loanStatusDetails[0]->getUnitNo();
    $invoiceDateCreated = $loanStatusDetails[0]->getDateCreated();

    $purchaserName = rewrite($_POST["purchaser_name"]);

    //add in oriject, to status, date,invoice,invoice type
    $projectName = rewrite($_POST['product_name']);
    $status = 'SINGLE';
    $proformaId = md5(uniqid());
    $username = $_SESSION['username'];

    //auto get project handler
    $projectDetails = getProject($conn," WHERE project_name = ? ",array("project_name"),array($projectName),"s");
    $projectHandler = $projectDetails[0]->getAddProjectPpl();

    $statusOfClaims = rewrite($_POST['status_of_claims']);
    $dateOfClaims = rewrite($_POST['date_of_claims']);
    $invoiceName = rewrite($_POST['invoice_name']);
    $receiveStatus = 'PENDING';

    // $invoiceType = rewrite($_POST['invoice_type']);
    // if (!$invoiceType)
    // {
        $invoiceType = "";
    // }

    // $item = rewrite($_POST['item']);
    $item = "";
    echo $itemAmount = rewrite($_POST['amount']);

    $unit = rewrite($_POST['unit']);

    // $project = rewrite($_POST['project']);
    $charges = rewrite($_POST['charges']); // temporary

    $bankAccountHolder = 'GIC Holding';
    $bankName = 'Public Bank Sdn Bhd';
    $bankAccountNo = 123456789;
    $creditStatus = "";

    // $remark = rewrite($_POST["remark"]);

    $loanStatus = getLoanStatus($conn, "WHERE loan_uid=?",array("loan_uid"),array($loanUid), "s");
    $CurrentAmountBal = $loanStatus[0]->getTotalBalUnclaimAmt();
    $CurrentAmountClaim = $loanStatus[0]->getTotalClaimDevAmt();

    if ($charges == 'YES')
    {
        $amount = $itemAmount;
        $finalAmount = $itemAmount * 1.06;
    }
    else
    {
      $amount = $itemAmount - ($itemAmount * 0.06);
      $finalAmount = $itemAmount;
    }
    echo $amount;
    echo $finalAmount;

    $totalAmountClaim = $CurrentAmountClaim + $itemAmount;
    $totalAmountBal = $CurrentAmountBal - $itemAmount;

  if ($invoiceName == 'Invoice') {

    if(isset($_POST['upload']))
    {
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        // //echo "save to database";
        if($totalAmountBal || !$totalAmountBal)
        {
            array_push($tableName,"total_bal_unclaim_amt");
            array_push($tableValue,$totalAmountBal);
            $stringType .=  "i";
        }
        if($totalAmountClaim || !$totalAmountClaim)
        {
            array_push($tableName,"total_claimed_dev_amt");
            array_push($tableValue,$totalAmountClaim);
            $stringType .=  "i";
        }
        if($name)
        {
            move_uploaded_file($_FILES['file']['tmp_name'],$target_file);
            array_push($tableName,"receipt");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
        array_push($tableValue,$loanUid);
        $stringType .=  "s";
        $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE loan_uid = ? ",$tableName,$tableValue,$stringType);
            if($withdrawUpdated)
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminWithdrawal.php?type=1');
            }
            else
            {
                echo "fail";
            }
    }
    else
    {
        echo "dunno";
    }

    if(isset($_POST['upload']))
    {   $date = date('d-m-Y');
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        // //echo "save to database";
        if($statusOfClaims == 1)
        {
            array_push($tableName,"1st_claim_amt");
            array_push($tableValue,$itemAmount);
            $stringType .=  "i";

            array_push($tableName,"request_date1");
            array_push($tableValue,$date);
            $stringType .=  "s";

            array_push($tableName,"sst1");
            array_push($tableValue,$charges);
            $stringType .=  "s";

            array_push($tableName,"status_1st");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($statusOfClaims == 2)
        {
            array_push($tableName,"2nd_claim_amt");
            array_push($tableValue,$itemAmount);
            $stringType .=  "i";

            array_push($tableName,"request_date2");
            array_push($tableValue,$date);
            $stringType .=  "s";

            array_push($tableName,"sst2");
            array_push($tableValue,$charges);
            $stringType .=  "s";

            array_push($tableName,"status_2nd");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($statusOfClaims == 3)
        {
            array_push($tableName,"3rd_claim_amt");
            array_push($tableValue,$itemAmount);
            $stringType .=  "i";

            array_push($tableName,"request_date3");
            array_push($tableValue,$date);
            $stringType .=  "s";

            array_push($tableName,"sst3");
            array_push($tableValue,$charges);
            $stringType .=  "s";

            array_push($tableName,"status_3rd");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($statusOfClaims == 4)
        {
            array_push($tableName,"4th_claim_amt");
            array_push($tableValue,$itemAmount);
            $stringType .=  "i";

            array_push($tableName,"request_date4");
            array_push($tableValue,$date);
            $stringType .=  "s";

            array_push($tableName,"sst4");
            array_push($tableValue,$charges);
            $stringType .=  "s";

            array_push($tableName,"status_4th");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($statusOfClaims == 5)
        {
            array_push($tableName,"5th_claim_amt");
            array_push($tableValue,$itemAmount);
            $stringType .=  "i";

            array_push($tableName,"request_date5");
            array_push($tableValue,$date);
            $stringType .=  "s";

            array_push($tableName,"sst5");
            array_push($tableValue,$charges);
            $stringType .=  "s";

            array_push($tableName,"status_5th");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        array_push($tableValue,$loanUid);
        $stringType .=  "s";
        $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE loan_uid = ? ",$tableName,$tableValue,$stringType);
            if($withdrawUpdated)
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminWithdrawal.php?type=1');
            }
            else
            {
                echo "fail";
            }

            if(addNewInvoice($conn,$id,$invoiceId,$purchaserName,$projectName,$projectHandler,$projectHandler,$statusOfClaims,$dateOfClaims,$invoiceName,$invoiceType,
                                    $itemAmount,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,
                                        $loanUid,$charges,$receiveStatus,$unit,$invoiceUnitNo,$invoiceDateCreated,$branchType,$creditStatus))
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../invoiceRecord.php?type=3');
                //echo "register success";
            }
            $column = "Issued";
            $a = "-";
            if(editHistory($conn, $username, $column, $invoiceId,$a, $a,$branchType))
                 {}
    }
    else
    {
        echo "dunno";
    }
  }elseif ($invoiceName == 'Proforma') {
    if(addNewInvoiceProforma($conn,$id,$proformaId,$projectName,$projectHandler,$projectHandler,$statusOfClaims,$invoiceName,$invoiceType,
                                $itemAmount,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,$charges,$receiveStatus,$invoiceUnitNo,$invoiceDateCreated,$branchType))
    {
      $column = "Issued";
      $a = "-";
      if(editHistory($conn, $username, $column, $proformaId,$a, $a,$branchType))
           {}
        $_SESSION['messageType'] = 1;
        header('Location: ../invoiceRecordProforma.php?type=2');
        //echo "register success";
    }
  }elseif ($invoiceName == 'Credit Note') {
    if(addNewInvoiceCredit($conn,$id,$proformaId,$projectName,$projectHandler,$projectHandler,$statusOfClaims,$invoiceName,$invoiceType,
                                $itemAmount,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,$charges,$receiveStatus,$invoiceUnitNo,$invoiceDateCreated,$branchType))
    {
      $column = "Issued";
      $a = "-";
      if(editHistory($conn, $username, $column, $proformaId,$a, $a,$branchType))
           {}
        $_SESSION['messageType'] = 1;
        header('Location: ../invoiceRecordCredit.php?type=2');
        //echo "register success";
    }
  }
}
else
{
    //  header('Location: ../index.php');
}
?>
