<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/../adminAccess1.php';
require_once dirname(__FILE__) . '/../timezone.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/MultiInvoice.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter),
     "sssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

$totalUnclaimedBalance = 0;
$totalClaimedDevAmt = 0;

$username = $_SESSION['username'];
$invoiceId = rewrite($_POST['invoice_id']);
$attn = rewrite($_POST['attn']);
$requestDate = rewrite($_POST['request_date']);
$requestDate = $requestDate;
$bankName = rewrite($_POST['bank_name']);
$bankAccHolder = rewrite($_POST['bank_acc_holder']);
$bankAccNo = rewrite($_POST['bank_acc_no']);
$charges = rewrite($_POST['charges']);

$itemAmount = $_POST['amount'];
$itemAmountImpp = implode(";",$itemAmount);
$itemAmountImpRem = str_replace(",","",$itemAmountImpp);
$itemAmountImp = str_replace(";",",",$itemAmountImpRem);
$itemAmountExp = explode(",",$itemAmountImp);
$amount = [];
$finalAmount = [];
$totalClaimedDevAmtArray = [];
$totalUnclaimedBalanceArray = [];


$multiInvoiceDetails = getMultiInvoice($conn, "WHERE invoice_id =? ",array("invoice_id"),array($invoiceId),"s");
$multiInvoiceDetails[0]->getInvoiceId();
$id = $multiInvoiceDetails[0]->getId();
$attnCurrent = $multiInvoiceDetails[0]->getProjectHandler();
$requestDateCurrent = $multiInvoiceDetails[0]->getBookingDate();
$bankNameCurrent = $multiInvoiceDetails[0]->getBankName();
$bankAccHolderCurrent = $multiInvoiceDetails[0]->getBankAccountHolder();
$bankAccNoCurrent = $multiInvoiceDetails[0]->getBankAccountNo();
$chargesCurrent = $multiInvoiceDetails[0]->getCharges();
$finalAmountCurrent = $multiInvoiceDetails[0]->getFinalAmount();
$amountCurrent = $multiInvoiceDetails[0]->getAmount();
$statusClaims = $multiInvoiceDetails[0]->getClaimsStatus();
$itemAmountCurrent = $multiInvoiceDetails[0]->getItem();
$loanIdImplode = $multiInvoiceDetails[0]->getLoanId();
$loanIdImplodeExp = explode(",",$loanIdImplode);
$itemAmountCurrentExp = explode(",",$itemAmountCurrent);
$statusClaimsExp = explode(",",$statusClaims);

if ($itemAmountExp) {
  for ($i=0; $i <count($itemAmountExp) ; $i++) {

    $loanDetails = getLoanStatus($conn, "WHERE id=?",array("id"),array($loanIdImplodeExp[$i]), "s");
    if ($loanDetails) {
      $totalUnclaimedBalance = $loanDetails[0]->getTotalBalUnclaimAmt();
      $totalClaimedDevAmt = $loanDetails[0]->getTotalClaimDevAmt();
    }


    // echo "Total Claim :".$totalClaimedDevAmt."<br>";
    // echo "Total Unclaim :".$totalUnclaimedBalance."<br>";
    // echo "Current Amount :".$itemAmountCurrentExp[$i]."<br>";
    // echo "New  Amount :".$itemAmountExp[$i]."<br>";

    if ($itemAmountCurrentExp[$i] > $itemAmountExp[$i]) {
      $difference = $itemAmountCurrentExp[$i] - $itemAmountExp[$i];
      $totalClaimedDevAmtArray[] = $totalClaimedDevAmt - $difference;
      $totalUnclaimedBalanceArray[] = $totalUnclaimedBalance + $difference;
    }elseif ($itemAmountExp[$i] > $itemAmountCurrentExp[$i]) {
      $difference = $itemAmountExp[$i] - $itemAmountCurrentExp[$i];
      $totalClaimedDevAmtArray[] = $totalClaimedDevAmt + $difference;
      $totalUnclaimedBalanceArray[] = $totalUnclaimedBalance - $difference;
    }else {
      $difference = 0;
      $totalClaimedDevAmtArray[] = $totalClaimedDevAmt;
      $totalUnclaimedBalanceArray[] = $totalUnclaimedBalance;
    }

    if ($charges == 'YES') {
      $finalAmount[] = $itemAmountExp[$i] * 1.06;
      $amount[] = $itemAmountExp[$i];
    }
    if ($charges == 'NO'){
      $finalAmount[] = $itemAmountExp[$i];
      $amount[] = $itemAmountExp[$i] - $itemAmountExp[$i]*(6/100);
    }
    $differenceArray[] = $difference;
  }
}
$finalAmountImp = implode(",",$finalAmount);
$amount = implode(",",$amount);

$differenceArrayImp = implode(",",$differenceArray);
$totalClaimedDevAmt = implode(",",$totalClaimedDevAmtArray);
$totalUnclaimedBalance = implode(",",$totalUnclaimedBalanceArray);
$totalClaimedDevAmtExp = explode(",",$totalClaimedDevAmt);
$totalUnclaimedBalanceExp = explode(",",$totalUnclaimedBalance);

// echo "Difference :".$differenceArrayImp."<br>";
// echo "Total Claim :".$totalClaimedDevAmt."<br>";
// echo "Total Unclaim :".$totalUnclaimedBalance."<br>";

// if ($itemAmountExp) {
//   for ($i=0; $i <count($itemAmountExp) ; $i++) {
//     echo "total claim:".$totalClaimedDevAmtExp[$i]."<br>";
//     echo "total unclaim: ".$totalUnclaimedBalanceExp[$i]."<br>";
//     echo "id :".$loanIdImplodeExp[$i]."<br>";
//   }
// }
echo count($itemAmountExp);

$editDetails = "$attn,$requestDate,$bankName,$bankAccHolder,$bankAccNo,$charges";
$editDetailsExp = explode(",",$editDetails);

$currentDetails = "$attnCurrent,$requestDateCurrent,$bankNameCurrent,$bankAccHolderCurrent,$bankAccNoCurrent,$chargesCurrent";
$currentDetailsExp = explode(",",$currentDetails);

$columnDetails = "Attn,Date,Bank Name,Bank Account Holder,Account No.,Charges";
$columnDetailsExp = explode(",",$columnDetails);



if (isset($_POST['editBtn'])) {

for ($i=0; $i < count($editDetailsExp) ; $i++) {
  if ($editDetailsExp[$i] != $currentDetailsExp[$i]) {
    $column = $columnDetailsExp[$i];
    if(editHistory($conn, $username, $column, $invoiceId,$currentDetailsExp[$i], $editDetailsExp[$i]))
         {}
  }
}

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($attn)
  {
      array_push($tableName,"project_handler");
      array_push($tableValue,$attn);
      $stringType .=  "s";
  }
  if($requestDate)
  {
      array_push($tableName,"booking_date");
      array_push($tableValue,$requestDate);
      $stringType .=  "s";
  }
  if($bankName)
  {
      array_push($tableName,"bank_name");
      array_push($tableValue,$bankName);
      $stringType .=  "s";
  }
  if($bankAccHolder)
  {
      array_push($tableName,"bank_account_holder");
      array_push($tableValue,$bankAccHolder);
      $stringType .=  "s";
  }
  if($bankAccNo)
  {
      array_push($tableName,"bank_account_no");
      array_push($tableValue,$bankAccNo);
      $stringType .=  "s";
  }
  if($finalAmountImp)
  {
      array_push($tableName,"final_amount");
      array_push($tableValue,$finalAmountImp);
      $stringType .=  "s";
  }
  if($amount)
  {
      array_push($tableName,"amount");
      array_push($tableValue,$amount);
      $stringType .=  "s";
  }
  if($itemAmountImp)
  {
      array_push($tableName,"item");
      array_push($tableValue,$itemAmountImp);
      $stringType .=  "s";
  }
  if($charges)
  {
      array_push($tableName,"charges");
      array_push($tableValue,$charges);
      $stringType .=  "s";
  }
  array_push($tableValue,$invoiceId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice_multi"," WHERE invoice_id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    if ($itemAmountExp) {
      for ($m=0; $m <count($itemAmountExp) ; $m++) {

        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        // //echo "save to database";
        if($totalClaimedDevAmtExp[$m] ||  !$totalClaimedDevAmtExp[$m])
        {
            array_push($tableName,"total_claimed_dev_amt");
            array_push($tableValue,$totalClaimedDevAmtExp[$m]);
            $stringType .=  "s";
        }
        if($totalUnclaimedBalanceExp[$m] || !$totalUnclaimedBalanceExp[$m])
        {
            array_push($tableName,"total_bal_unclaim_amt");
            array_push($tableValue,$totalUnclaimedBalanceExp[$m]);
            $stringType .=  "s";
        }
        if($statusClaimsExp[$m] == 1)
        {
            array_push($tableName,"1st_claim_amt");
            array_push($tableValue,$itemAmountExp[$m]);
            $stringType .=  "i";

            array_push($tableName,"sst1");
            array_push($tableValue,$charges);
            $stringType .=  "s";
        }
        if($statusClaimsExp[$m] == 2)
        {
            array_push($tableName,"2nd_claim_amt");
            array_push($tableValue,$itemAmountExp[$m]);
            $stringType .=  "i";

            array_push($tableName,"sst2");
            array_push($tableValue,$charges);
            $stringType .=  "s";
        }
        if($statusClaimsExp[$m] == 3)
        {
            array_push($tableName,"3rd_claim_amt");
            array_push($tableValue,$itemAmountExp[$m]);
            $stringType .=  "i";

            array_push($tableName,"sst3");
            array_push($tableValue,$charges);
            $stringType .=  "s";
        }
        if($statusClaimsExp[$m] == 4)
        {
            array_push($tableName,"4th_claim_amt");
            array_push($tableValue,$itemAmountExp[$m]);
            $stringType .=  "i";

            array_push($tableName,"sst4");
            array_push($tableValue,$charges);
            $stringType .=  "s";
        }
        if($statusClaimsExp[$m] == 5)
        {
            array_push($tableName,"5th_claim_amt");
            array_push($tableValue,$itemAmountExp[$m]);
            $stringType .=  "i";

            array_push($tableName,"sst5");
            array_push($tableValue,$charges);
            $stringType .=  "s";
        }
        array_push($tableValue,$loanIdImplodeExp[$m]);
        $stringType .=  "s";
        $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE id = ? ",$tableName,$tableValue,$stringType);

        if($withdrawUpdated){

          $_SESSION['messageType'] = 1;
          $_SESSION['idComm'] = $id;
          header('location: ../invoiceMulti.php?type=2');
        }
      }
    }
  }
}
 ?>
