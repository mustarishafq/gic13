<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addNewProject($conn,$projectId,$projectName,$addProjectPpl,$companyName,$companyAddress,$claimTimes,$projectLeader,$display,$otherAmount,$creatorName,$branchType)
{
     if(insertDynamicData($conn,"project",array("project_id","project_name","add_projectppl","company_name","company_address","claims_no","project_leader","display","other_claim","creator_name","branch_type"),
     array($projectId,$projectName,$addProjectPpl,$companyName,$companyAddress,$claimTimes,$projectLeader,$display,$otherAmount,$creatorName,$branchType),"sssssssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }
     return true;
}

function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter,$branchType)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after","branch_type"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter,$branchType),
     "ssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    if (isset($_POST['id'])) {
      $id = rewrite($_POST['id']);
    }
    $branchType = $_SESSION['branch_type'];
    $projectId = md5(uniqid());
    $projectName = rewrite($_POST["project_name"]);
    $addProjectPpl = rewrite($_POST["add_by"]);
    $claimTimes = rewrite($_POST["claims_times"]);
    $projectLeader = $_POST["project_leader"];
    $companyName = $_POST["company_name"];
    $companyAddress = $_POST["company_address"];
    $companyAddressImplode = implode(",",str_replace(",","",$companyAddress));
    $projectLeaderImplode = implode(",",$projectLeader);
    $display = "Yes";
    $creatorName = $_SESSION['username'];
    $otherAmount = 0;

    $projectDetails = getProject($conn, "WHERE project_name =? AND display ='Yes'",array("project_name"),array($projectName), "s");
    if ($projectDetails) {
      $existedProject = 1;
    }else {
      $existedProject = 0;
    }

     //   FOR DEBUGGING
     //    echo $name;
     //    echo $price;
     //    echo $type;
if (isset($_POST['loginButton']) && $existedProject == 0) {
  if(addNewProject($conn,$projectId,$projectName,$addProjectPpl,$companyName,$companyAddressImplode,$claimTimes,$projectLeaderImplode,$display,$otherAmount,$creatorName,$branchType))
  {
       // $_SESSION['messageType'] = 1;
       $column = "Created";
       $a = "-";
       if(editHistory($conn, $creatorName, $column, $projectId,$a, $a,$branchType))
            {}
       if ($_SESSION['usertype_level'] == 1) {
         $_SESSION['messageType'] = 1;
         header('Location: ../adminAddNewProject.php?type=2');
       }
       if ($_SESSION['usertype_level'] == 2) {
         $_SESSION['messageType'] = 1;
         header('Location: ../adminAddNewProject.php?type=2');
       }
       // header('Location: ../admin1Product.php?type=4');
       // echo "register success";
       // echo "<script>alert('New Project Created Successfully !');window.location='../admin1Product.php'</script>";
  }

}else {
  $_SESSION['messageType'] = 1;
  header('Location: ../adminAddNewProject.php?type=3');
}
}
else
{
    //  header('Location: ../index.php');
}
?>
