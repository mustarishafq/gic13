<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/../adminAccess1.php';
require_once dirname(__FILE__) . '/../timezone.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/Commission.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter),
     "sssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
$commissionFinal = [];
$uid = rewrite($_POST['uid']);
$upline = rewrite($_POST['upline']);
$username = $_SESSION['username'];
$ids = rewrite($_POST['id']);
$idExplode = explode(",",$ids);
$loanUids = rewrite($_POST['loan_uid']);
$loanUidExplode = explode(",",$loanUids);
// $attn = rewrite($_POST['attn']);
$requestDate = rewrite($_POST['request_date']);
// $requestDate = $requestDate;
$paymentMethod = rewrite($_POST['payment_method']);
$checkId = rewrite($_POST['check_id']);
$icNo = rewrite($_POST['ic_no']);

$newAmount = $_POST['comm'];
$newAmountImpp = implode(";",$newAmount);
$newAmountRem = str_replace(",","",$newAmountImpp);
$newAmountImp = str_replace(";",",",$newAmountRem);
$newAmountExp = explode(",",$newAmountImp);

// $oldAmount = $_POST['oldComm'];
// $oldAmountImpp = implode(";",$oldAmount);
// $oldAmountRem = str_replace(",","",$oldAmountImpp);
// $oldAmountImp = str_replace(";",",",$oldAmountRem);
// $oldAmountExp = explode(",",$oldAmountImp);

// $newAmount = $finalAmount - $oldAmount;

// $commissionFinal = $currentComm - $newAmount;

// $uplineType = $_POST['upline_type'];
// echo $uplineType;
// $uplineTypeExp = explode(",",$uplineType);
//
// for ($m=0; $m <count($uplineTypeExp) ; $m++) {
//   // echo count($uplineTypeExp);
// if ($uplineTypeExp[$m] == 'Agent') {
//   $loanDetails = getLoanStatus($conn, "WHERE loan_uid =?",array("loan_uid"),array($loanUidExplode[$m]), "s");
//   $currentCommImp = $loanDetails[0]->getAgentBalance();
//   $currentCommExp = explode(",",$currentCommImp);
//   $currentComm = $currentCommExp[0];
//   $currentClaim = $currentCommExp[1];
// }
// if ($uplineTypeExp[$m] == 'Upline 1') {
//   $loanDetails = getLoanStatus($conn, "WHERE loan_uid =?",array("loan_uid"),array($loanUidExplode[$m]), "s");
//   $currentCommImp = $loanDetails[0]->getUlBalance();
//   $currentCommExp = explode(",",$currentCommImp);
//   $currentComm = $currentCommExp[0];
//   $currentClaim = $currentCommExp[1];
// }
// if ($uplineTypeExp[$m] == 'Upline 2') {
//   $loanDetails = getLoanStatus($conn, "WHERE loan_uid =?",array("loan_uid"),array($loanUidExplode[$m]), "s");
//   $currentCommImp = $loanDetails[0]->getUulBalance();
//   $currentCommExp = explode(",",$currentCommImp);
//   $currentComm = $currentCommExp[0];
//   $currentClaim = $currentCommExp[1];
// }
// if ($uplineTypeExp[$m] == 'Upline 3') {
//   $loanDetails = getLoanStatus($conn, "WHERE loan_uid =?",array("loan_uid"),array($loanUidExplode[$m]), "s");
//   $currentCommImp = $loanDetails[0]->getUuulBalance();
//   $currentCommExp = explode(",",$currentCommImp);
//   $currentComm = $currentCommExp[0];
//   $currentClaim = $currentCommExp[1];
// }
// if ($uplineTypeExp[$m] == 'Upline 4') {
//   $loanDetails = getLoanStatus($conn, "WHERE loan_uid =?",array("loan_uid"),array($loanUidExplode[$m]), "s");
//   $currentCommImp = $loanDetails[0]->getUuuulBalance();
//   $currentCommExp = explode(",",$currentCommImp);
//   $currentComm = $currentCommExp[0];
//   $currentClaim = $currentCommExp[1];
// }
// if ($uplineTypeExp[$m] == 'Upline 5') {
//   $loanDetails = getLoanStatus($conn, "WHERE loan_uid =?",array("loan_uid"),array($loanUidExplode[$m]), "s");
//   $currentCommImp = $loanDetails[0]->getUuuuulBalance();
//   $currentCommExp = explode(",",$currentCommImp);
//   $currentComm = $currentCommExp[0];
//   $currentClaim = $currentCommExp[1];
// }
// if ($uplineTypeExp[$m] == 'PL') {
//   $loanDetails = getLoanStatus($conn, "WHERE loan_uid =?",array("loan_uid"),array($loanUidExplode[$m]), "s");
//   $currentCommImp = $loanDetails[0]->getPLBalance();
//   $currentCommExp = explode(",",$currentCommImp);
//   $currentComm = $currentCommExp[0];
//   $currentClaim = $currentCommExp[1];
// }
// if ($uplineTypeExp[$m] == 'HOS') {
//   $loanDetails = getLoanStatus($conn, "WHERE loan_uid =?",array("loan_uid"),array($loanUidExplode[$m]), "s");
//   $currentCommImp = $loanDetails[0]->getHosBalance();
//   $currentCommExp = explode(",",$currentCommImp);
//   $currentComm = $currentCommExp[0];
//   $currentClaim = $currentCommExp[1];
// }
// if ($uplineTypeExp[$m] == 'Lister') {
//   $loanDetails = getLoanStatus($conn, "WHERE loan_uid =?",array("loan_uid"),array($loanUidExplode[$m]), "s");
//   $currentCommImp = $loanDetails[0]->getListerBalance();
//   $currentCommExp = explode(",",$currentCommImp);
//   $currentComm = $currentCommExp[0];
//   $currentClaim = $currentCommExp[1];
// }
// // $finalAmount = str_replace(",","",$finalAmount);
//
// if ($finalAmountExp) {
//     // echo count($finalAmountExp)."<br>";
//     // echo $finalAmountExp[$m]."<br>";
//     // echo $oldAmountExp[$m]."<br>";
//     $newAmount = $finalAmountExp[$m] - $oldAmountExp[$m];
//     $commissionFinal[] = $currentComm - $newAmount;
// }
// }
//
// $commissionFinal = implode(",",$commissionFinal);
// $commissionFinalExp = explode(",",$commissionFinal);


for ($j=0; $j <count($idExplode) ; $j++) {

  $commissionDetails = getCommission($conn, "WHERE id =? ",array("id"),array($idExplode[$j]),"s");
  // echo $commissionDetails[0]->getInvoiceId();
  $id = $commissionDetails[0]->getID();
  $loanUid = $commissionDetails[0]->getLoanUid();
  $commissionId = $commissionDetails[0]->getCommissionId();
  $uplineCurrent = $commissionDetails[0]->getUpline();
  $uplineType = $commissionDetails[0]->getUplineType();
  $requestDateCurrent = $commissionDetails[0]->getDateCreated();
  $paymentMethodCurrent = $commissionDetails[0]->getPaymentMethod();
  $checkIdCurrent = $commissionDetails[0]->getCheckID();
  $icNoCurrent = $commissionDetails[0]->getIcNo();
  $defCommission = $commissionDetails[0]->getDefCommission();
  $charges = $commissionDetails[0]->getCharges();

  $loanDetails = getLoanStatus($conn, "WHERE loan_uid =?",array("loan_uid"),array($loanUid), "s");

  if ($uplineType = "Upline 1") {
     $totalClaimedDevAmtImp = $loanDetails[0]->getUlBalance();
     $totalClaimedDevAmtExp = explode(",",$totalClaimedDevAmtImp);
     $totalUnclaimedBalance = $totalClaimedDevAmtExp[0];
     $currentClaim = $totalClaimedDevAmtExp[1];
  }elseif ($uplineType = "Upline 2") {
    $totalClaimedDevAmtImp = $loanDetails[0]->getUulBalance();
    $totalClaimedDevAmtExp = explode(",",$totalClaimedDevAmtImp);
    $totalUnclaimedBalance = $totalClaimedDevAmtExp[0];
    $currentClaim = $totalClaimedDevAmtExp[1];
  }elseif ($uplineType = "Upline 3") {
    $totalClaimedDevAmtImp = $loanDetails[0]->getUuulBalance();
    $totalClaimedDevAmtExp = explode(",",$totalClaimedDevAmtImp);
    $totalUnclaimedBalance = $totalClaimedDevAmtExp[0];
    $currentClaim = $totalClaimedDevAmtExp[1];
  }elseif ($uplineType = "Upline 4") {
    $totalClaimedDevAmtImp = $loanDetails[0]->getUuuulBalance();
    $totalClaimedDevAmtExp = explode(",",$totalClaimedDevAmtImp);
    $totalUnclaimedBalance = $totalClaimedDevAmtExp[0];
    $currentClaim = $totalClaimedDevAmtExp[1];
  }elseif ($uplineType = "Upline 5") {
    $totalClaimedDevAmtImp = $loanDetails[0]->getUuuuulBalance();
    $totalClaimedDevAmtExp = explode(",",$totalClaimedDevAmtImp);
    $totalUnclaimedBalance = $totalClaimedDevAmtExp[0];
    $currentClaim = $totalClaimedDevAmtExp[1];
  }elseif ($uplineType = "Agent") {
    $totalClaimedDevAmtImp = $loanDetails[0]->getAgentBalance();
    $totalClaimedDevAmtExp = explode(",",$totalClaimedDevAmtImp);
    $totalUnclaimedBalance = $totalClaimedDevAmtExp[0];
    $currentClaim = $totalClaimedDevAmtExp[1];
  }elseif ($uplineType = "PL") {
    $totalClaimedDevAmtImp = $loanDetails[0]->getPLBalance();
    $totalClaimedDevAmtExp = explode(",",$totalClaimedDevAmtImp);
    $totalUnclaimedBalance = $totalClaimedDevAmtExp[0];
    $currentClaim = $totalClaimedDevAmtExp[1];
  }elseif ($uplineType = "HOS") {
    $totalClaimedDevAmtImp = $loanDetails[0]->getHosBalance();
    $totalClaimedDevAmtExp = explode(",",$totalClaimedDevAmtImp);
    $totalUnclaimedBalance = $totalClaimedDevAmtExp[0];
    $currentClaim = $totalClaimedDevAmtExp[1];
  }elseif ($uplineType = "Lister") {
    $totalClaimedDevAmtImp = $loanDetails[0]->getListerBalance();
    $totalClaimedDevAmtExp = explode(",",$totalClaimedDevAmtImp);
    $totalUnclaimedBalance = $totalClaimedDevAmtExp[0];
    $currentClaim = $totalClaimedDevAmtExp[1];
  }

  if ($charges == 'NO') {
    $finalAmount[] = $newAmountExp[$j] - ($newAmountExp[$j]*0.06);
    $defAmount[] = $newAmountExp[$j];
  }elseif ($charges == 'YES') {
    $finalAmount[] = $newAmountExp[$j];
    $defAmount[] = $newAmountExp[$j];
  }

  if ($defCommission > $newAmountExp[$j]) {
    $difference = $defCommission - $newAmountExp[$j];
    $totalUnclaimedBalanceArray[] = $totalUnclaimedBalance + $difference;
  }elseif ($newAmountExp[$j] > $defCommission) {
    $difference = $newAmountExp[$j] - $defCommission;
    $totalUnclaimedBalanceArray[] = $totalUnclaimedBalance - $difference;
  }else {
    $difference = 0;
    $totalUnclaimedBalanceArray[] = $totalUnclaimedBalance;
  }

  $differenceArray[] = $difference;
  $currentClaimArray[] = $currentClaim;
  $uplineTypeArray[] = $uplineType;

  $editDetails = "$upline,$requestDate,$paymentMethod,$checkId,$icNo,$newAmountExp[$j]";
  $editDetailsExp = explode(",",$editDetails);

  $currentDetails = "$uplineCurrent,$requestDateCurrent,$paymentMethodCurrent,$checkIdCurrent,$icNoCurrent,$defCommission";
  $currentDetailsExp = explode(",",$currentDetails);

  $columnDetails = "Agent,Date,Payment Method,Cheque No.,I/C No.,Total";
  $columnDetailsExp = explode(",",$columnDetails);
}

  $finalAmountImp = implode(",",$finalAmount);
  $defAmountImp = implode(",",$defAmount);
  $differenceImp = implode(",",$differenceArray);
  $totalBalUnclaimAmtImp = implode(",",$totalUnclaimedBalanceArray);
  $currentClaimImp = implode(",",$currentClaimArray);
  $uplineTypeImp = implode(",",$uplineTypeArray);
  $finalAmountExp = explode(",",$finalAmountImp);
  $defAmountExp = explode(",",$defAmountImp);
  $difference = explode(",",$differenceImp);
  $totalBalUnclaimExp = explode(",",$totalBalUnclaimAmtImp);
  $currentClaimExp = explode(",",$currentClaimImp);
  $uplineTypeExp = explode(",",$uplineTypeImp);

  echo "Upline Type :".$uplineTypeImp."<br>";
  echo "final Amount :".$finalAmountImp."<br>";
  echo "def Amount :".$defAmountImp."<br>";
  echo "Difference :".$differenceImp."<br>";
  echo "Unclaim :".$totalBalUnclaimAmtImp."<br>";
  echo "Current Claim :".$currentClaimImp."<br>";

//   if (isset($_POST['editBtn'])) {
//
//   for ($i=0; $i < count($editDetailsExp) ; $i++) {
//     if ($editDetailsExp[$i] != $currentDetailsExp[$i]) {
//       $column = $columnDetailsExp[$i];
//       if(editHistory($conn, $username, $column, $commissionId,$currentDetailsExp[$i], $editDetailsExp[$i]))
//            {}
//     }
//   }
//
// for ($m=0; $m <count($finalAmountExp) ; $m++) {
//
//     $tableName = array();
//     $tableValue =  array();
//     $stringType =  "";
//     // //echo "save to database";
//     if($upline)
//     {
//         array_push($tableName,"upline");
//         array_push($tableValue,$upline);
//         $stringType .=  "s";
//     }
//     if($requestDate)
//     {
//         array_push($tableName,"booking_date");
//         array_push($tableValue,$requestDate);
//         $stringType .=  "s";
//     }
//     if($paymentMethod)
//     {
//         array_push($tableName,"payment_method");
//         array_push($tableValue,$paymentMethod);
//         $stringType .=  "s";
//     }
//     if($checkId || !$checkId)
//     {
//         array_push($tableName,"check_id");
//         array_push($tableValue,$checkId);
//         $stringType .=  "s";
//     }
//     if($icNo)
//     {
//         array_push($tableName,"ic_no");
//         array_push($tableValue,$icNo);
//         $stringType .=  "s";
//     }
//     if ($finalAmountExp[$m]) {
//         array_push($tableName,"commission");
//         array_push($tableValue,$finalAmountExp[$m]);
//         $stringType .= "s";
//     }
//     if ($defAmountExp[$m]) {
//         array_push($tableName,"def_commission");
//         array_push($tableValue,$defAmountExp[$m]);
//         $stringType .= "s";
//     }
//     // if($amount)
//     // {
//     //     array_push($tableName,"amount");
//     //     array_push($tableValue,$amount);
//     //     $stringType .=  "s";
//     // }
//     array_push($tableValue,$idExplode[$m]);
//     $stringType .=  "s";
//     $withdrawUpdated = updateDynamicData($conn,"commission"," WHERE id = ? ",$tableName,$tableValue,$stringType);
//
//     if($withdrawUpdated)
//     {
//       // $_SESSION['messageType'] = 1;
//       // $_SESSION['idImp'] = $ids;
//       // $_SESSION['commission_id'] = $commissionId;
//       // header('location: ../commission.php?type=4');
//       // $commissionFinalExp = $commissionFinalExp[$m].",".$currentClaim;
//       // echo $commissionFinalExp[$m]."<br>";
//
//       $tableName = array();
//       $tableValue =  array();
//       $stringType =  "";
//
//       if($uplineTypeExp[$m] == 'Agent')
//       {
//           array_push($tableName,"agent_balance");
//           array_push($tableValue,number_format($totalBalUnclaimExp[$m],2).",".$currentClaimExp[$m]);
//           $stringType .=  "s";
//       }
//       if($uplineTypeExp[$m] == 'Upline 1')
//       {
//           array_push($tableName,"ul_balance");
//           array_push($tableValue,number_format($totalBalUnclaimExp[$m],2).",".$currentClaimExp[$m]);
//           $stringType .=  "s";
//       }
//       if($uplineTypeExp[$m] == 'Upline 2')
//       {
//           array_push($tableName,"uul_balance");
//           array_push($tableValue,number_format($totalBalUnclaimExp[$m],2).",".$currentClaimExp[$m]);
//           $stringType .=  "s";
//       }
//       if($uplineTypeExp[$m] == 'Upline 3')
//       {
//           array_push($tableName,"uuul_balance");
//           array_push($tableValue,number_format($totalBalUnclaimExp[$m],2).",".$currentClaimExp[$m]);
//           $stringType .=  "s";
//       }
//       if($uplineTypeExp[$m] == 'Upline 4')
//       {
//           array_push($tableName,"uuuul_balance");
//           array_push($tableValue,number_format($totalBalUnclaimExp[$m],2).",".$currentClaimExp[$m]);
//           $stringType .=  "s";
//       }
//       if($uplineTypeExp[$m] == 'Upline 5')
//       {
//           array_push($tableName,"uuuuul_balance");
//           array_push($tableValue,number_format($totalBalUnclaimExp[$m],2).",".$currentClaimExp[$m]);
//           $stringType .=  "s";
//       }
//       if($uplineTypeExp[$m] == 'PL')
//       {
//           array_push($tableName,"pl_balance");
//           array_push($tableValue,number_format($totalBalUnclaimExp[$m],2).",".$currentClaimExp[$m]);
//           $stringType .=  "s";
//       }
//       if($uplineTypeExp[$m] == 'HOS')
//       {
//           array_push($tableName,"hos_balance");
//           array_push($tableValue,number_format($totalBalUnclaimExp[$m],2).",".$currentClaimExp[$m]);
//           $stringType .=  "s";
//       }
//       if($uplineTypeExp[$m] == 'Lister')
//       {
//           array_push($tableName,"lister_balance");
//           array_push($tableValue,number_format($totalBalUnclaimExp[$m],2).",".$currentClaimExp[$m]);
//           $stringType .=  "s";
//       }
//       array_push($tableValue,$loanUidExplode[$m]);
//       $stringType .=  "s";
//       $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE loan_uid = ? ",$tableName,$tableValue,$stringType);
//
//       if($withdrawUpdated){
//         $_SESSION['messageType'] = 1;
//         $_SESSION['idImp'] = $ids;
//         $_SESSION['commission_id'] = $commissionId;
//         header('location: ../commission.php?type=4');
//         }
//       }
//     }
//   }


 ?>
