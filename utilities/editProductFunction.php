<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../timezone.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Commission.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter,$branchType)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after","branch_type"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter,$branchType),
     "ssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}

function addAdvancedSlip($conn, $id, $unitNo, $projectName, $bookingDate, $loanUid, $agent, $amount, $status, $receiveStatus,$agentIc,$paymentMethod,$advancedId,$agentDefault,$branchType)
{
     if(insertDynamicData($conn,"advance_slip", array("id", "unit_no", "project_name", "booking_date", "loan_uid","agent","amount","status", "receive_status","ic_no","payment_method","advance_id","agent_default","branch_type"),
     array($id, $unitNo,$projectName, $bookingDate, $loanUid, $agent, $amount, $status, $receiveStatus,$agentIc,$paymentMethod,$advancedId,$agentDefault,$branchType),
     "isssssdsssssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}//

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    $branchType = $_SESSION['branch_type'];
    $username = $_SESSION['username'];
    $advancedId = md5(uniqid());
    $detailsBefore = "-";
    $detailsAfter = "-";
    $id = rewrite($_POST["id"]);
    $unitNo = $_POST["unit_no"];
    $unitNoImplode = implode(",",$unitNo);
    $loanUid = rewrite($_POST["loan_uid"]);
    $loanDetails = getLoanStatus($conn, "WHERE loan_uid = ?", array("loan_uid"), array($loanUid), "s");
    $currentAgentComm = $loanDetails[0]->getAgentComm();
    $currentAgentComm = str_replace(",", "", $currentAgentComm);

    $agentBalDetImp = $loanDetails[0]->getAgentBalance();
    $agentBalDetExp = explode(",",$agentBalDetImp);
    if ($agentBalDetExp[1]) {
      $agentClaim = $agentBalDetExp[1];
    }else {
      $agentClaim = 0;
    }

    $advanceDetails = getAdvancedSlip($conn, "WHERE loan_uid = ? ", array("loan_uid"), array($loanUid), "s");

    $amount = 2000;
    $status = 'PENDING';
    $receiveStatus = 'PENDING';
    $purchaserName = $_POST["purchaser_name"];
    $purchaserNameImplode = implode(",",$purchaserName);
    $ic = $_POST["ic"];
    $icImplode = implode(",",$ic);
    $contact = $_POST["contact"];
    $contactImplode = implode(",",$contact);
    $email = $_POST["email"];
    $emailImplode = implode(",",$email);
    $bookingDate = rewrite($_POST["booking_date"]);
    $sqFt = rewrite($_POST["sq_ft"]);
    $sqFt = str_replace(",", "", $sqFt);
    $paymentMethodAgentSlip = "Online Transfer";

    $str2 = rewrite($_POST["spa_price"]);
    $newSPAPrice = str_replace( ',', '', $str2);
    $spaPrice = $newSPAPrice;
    $spaPrice = str_replace(",", "", $spaPrice);

    $package = rewrite($_POST["package"]);
    $package = str_replace(",", "", $package);
    $discount = rewrite($_POST["discount"]);
    $rebate = rewrite($_POST["rebate"]);

    //remove comma inside value
    $str1 = rewrite($_POST["nettprice"]);
    $newNettPrice = str_replace( ',', '', $str1);
    $nettPrice = $newNettPrice;
    $nettPrice = str_replace(",", "", $nettPrice);

    //choose % or value
    $totalDC = rewrite($_POST["totaldevelopercomm"]);
    $totalDC = str_replace(",", "", $totalDC);
    if (!$totalDC)
    {
         $totalDCP = rewrite($_POST["totaldevelopercommper"]);
         $tDCP = ($totalDCP / 100);
         $totalDeveloperComm = ($tDCP * $nettPrice);
    }
    else
    {
         $totalDCstr = rewrite($_POST["totaldevelopercomm"]);
         $totalDCstr = str_replace(",","",$totalDCstr);

         $totalDeveloperComm = $totalDCstr;
    }

    //get upline, up-upline
    $agent = rewrite($_POST["agent"]);
    $getUplineDetails = getUser($conn," WHERE full_name = ? ",array("full_name"),array($agent),"s");
    $agentUid = $getUplineDetails[0]->getUid();
    $agentReferralDetails = getReferralHistory($conn, "WHERE referral_id=?",array("referral_id"),array($agentUid), "s");
    $agentCurrentLevel = $agentReferralDetails[0]->getCurrentLevel();
    $agentFirstLevel = $agentCurrentLevel + 1;
    $uplineUidDetails = getTop10ReferrerOfUser($conn, $agentUid);
    if ($uplineUidDetails) {
      for ($i=0; $i <count($uplineUidDetails) ; $i++) {
        $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uplineUidDetails[$i]), "s");
        $fullName = $userDetails[0]->getFullName();
        $upline[] = $userDetails[0]->getFullName();

      }
    }
    $uplineImp = implode(",",$upline);
    $uplineExp = explode(",",$uplineImp);
    for ($i=0; $i <count($uplineExp) ; $i++) {
      if ($i == 0) {
        $upline1 = $uplineExp[$i];
      }else
      if ($i == 1){
        $upline2 = $uplineExp[$i];
      }else
      if ($i == 2){
        $upline3 = $uplineExp[$i];
      }else
      if ($i == 3){
        $upline4 = $uplineExp[$i];
      }else
      if ($i == 4){
        $upline5 = $uplineExp[$i];
      }
    }
    $agentIc = $getUplineDetails[0]->getIcNo();
    $agentFullName = $getUplineDetails[0]->getFullName();

    $upline1 = $getUplineDetails[0]->getUpline1();
    if (!$upline1 || $upline1 == 'N/A' || $upline1 == 'Null' || $upline1 == 'null' || $upline1 == 'NULL') {
      $upline1Name = "";
    }else {
      $upline1Name = $upline1;
    }
    $upline2 = $getUplineDetails[0]->getUpline2();
    if (!$upline2 || $upline2 == 'N/A' || $upline2 == 'Null' || $upline2 == 'null' || $upline2 == 'NULL') {
      $upline1Name = "";
    }else {
      $upline2Name = $upline2;
    }
    $upline3 = $getUplineDetails[0]->getUpline3();
    if (!$upline3 || $upline3 == 'N/A' || $upline3 == 'Null' || $upline3 == 'null' || $upline3 == 'NULL') {
      $upline3Name = "";
    }else {
      $upline3Name = $upline3;
    }
    $upline4 = $getUplineDetails[0]->getUpline4();
    if (!$upline4 || $upline4 == 'N/A' || $upline4 == 'Null' || $upline4 == 'null' || $upline4 == 'NULL') {
      $upline4Name = "";
    }else {
      $upline4Name = $upline4;
    }
    $upline5 = $getUplineDetails[0]->getUpline5();
    if (!$upline5 || $upline5 == 'N/A' || $upline5 == 'Null' || $upline5 == 'null' || $upline5 == 'NULL') {
      $upline5Name = "";
    }else {
      $upline5Name = $upline5;
    }


    $loanStatus = rewrite($_POST["loanstatus"]);
    $remark = rewrite($_POST["remark"]);
    $bFormCollected = rewrite($_POST["bform_Collected"]);
    $paymentMethod = rewrite($_POST["payment_method"]);
    $lawyer = rewrite($_POST["lawyer"]);
    $bankApproved = rewrite($_POST["bank_approved"]);
    $loSignedDate = rewrite($_POST["lo_signed_date"]);
    $laSignedDate = rewrite($_POST["la_signed_date"]);
    $spaSignedDate = rewrite($_POST["spa_signed_date"]);
    $fullsetCompleted = rewrite($_POST["fullset_completed"]);

    $cashBuyer = rewrite($_POST["cash_buyer"]);

    $cancelledBooking = rewrite($_POST["cancelled_booking"]);
    $caseStatus = rewrite($_POST["case_status"]);
    $projectName = rewrite($_POST['project_name']);
    $loanAmountType = rewrite($_POST['loan_amount_type']);
    $loanAmount = rewrite($_POST['loan_amount']);
    $loanAmount = str_replace(",", "", $loanAmount);
    if ($loanAmountType == '%') {
      $loanAmount = $loanAmount."%";

    }elseif ($loanAmountType == 'RM') {
      $loanAmount = $loanAmount;
    }

    $eventPersonal = rewrite($_POST["event_personal"]);

    // $upline1 = $upline1Name;
    $upline1 = $upline1Name;
    if (!$upline1) {
      $upline1 = "";
    }
    $upline2 = $upline2Name;
    if (!$upline2) {
      $upline2 = "";
    }
    $upline3 = $upline3Name;
    if (!$upline3) {
      $upline3 = "";
    }
    $upline4 = $upline4Name;
    if (!$upline4) {
      $upline4 = "";
    }
    $upline5 = $upline5Name;
    if (!$upline5) {
      $upline5 = "";
    }

    $plName = rewrite($_POST["pl_name"]);
    $hosName = rewrite($_POST["hos_name"]);
    $listerName = rewrite($_POST["lister_name"]);


    $rate = rewrite($_POST["rate"]);
    if (!$rate)
    {
        $rate = "";
        // $rate = rewrite($_POST["rate"]);
        $agentComm = rewrite($_POST["agent_comm"]);
        $agentComm = str_replace(",", "", $agentComm);

        if (!$upline1) {
          $ulOverride = 0;
        }else {
          $ulOverride = $agentComm * (5/100);
        }
        if (!$upline2) {
          $uulOverride = 0;
        }else {
          $uulOverride = $agentComm * (5/100);
        }
        if (!$upline3) {
          $uuulOverride = 0;
        }else {
          $uuulOverride = $agentComm * (3/100);
        }
        if (!$upline4) {
          $uuuulOverride = 0;
        }else {
          $uuuulOverride = $agentComm * (1/100);
        }
        if (!$upline5) {
          $uuuuulOverride = 0;
        }else {
          $uuuuulOverride = $agentComm * (1/100);
        }
        $plOverride = $agentComm * (15/100);
        $hosOverride = $agentComm * (5/100);

        $noRate = $agentComm + $ulOverride + $uulOverride + $uuulOverride + $uuuulOverride + $uuuuulOverride + $plOverride + $hosOverride;

        $listerOverride = rewrite($_POST["lister_override"]);
        $listerOverride = str_replace(",", "", $listerOverride);

        // $admin1Override = rewrite($_POST["admin1_override"]);
        // $admin1Override = str_replace(",", "", $admin1Override);
        // $admin2Override = rewrite($_POST["admin2_override"]);
        // $admin2Override = str_replace(",", "", $admin2Override);
        // $admin3Override = rewrite($_POST["admin3_override"]);
        // $admin3Override = str_replace(",", "", $admin3Override);
          $admin1Override = 0;
          $admin2Override = 0;
          $admin3Override = 0;

        // $totalAdminOverride = $listerOverride + $admin1Override + $admin2Override + $admin3Override;
          $totalAdminOverride = $listerOverride;
        $gicProfit = $totalDeveloperComm - $noRate - $totalAdminOverride;

    }
    else
    {
        $ratePercentage =( $rate / 100 );
        $agentComm = $ratePercentage * $nettPrice;

        if (!$upline1) {
          $ulOverride = 0;
        }else {
          $ulOverride = $agentComm * (5/100);
        }
        if (!$upline2) {
          $uulOverride = 0;
        }else {
          $uulOverride = $agentComm * (5/100);
        }
        if (!$upline3) {
          $uuulOverride = 0;
        }else {
          $uuulOverride = $agentComm * (3/100);
        }
        if (!$upline4) {
          $uuuulOverride = 0;
        }else {
          $uuuulOverride = $agentComm * (1/100);
        }
        if (!$upline5) {
          $uuuuulOverride = 0;
        }else {
          $uuuuulOverride = $agentComm * (1/100);
        }
        $plOverride = $agentComm * (15/100);
        $hosOverride = $agentComm * (5/100);

        $withRate = $agentComm + $ulOverride + $uulOverride + $uuulOverride + $uuuulOverride + $uuuuulOverride + $plOverride + $hosOverride;

        // $listerOverride = rewrite($_POST["lister_override"]);
        // $listerOverride = str_replace(",", "", $listerOverride);
        // $admin1Override = rewrite($_POST["admin1_override"]);
        // $admin1Override = str_replace(",", "", $admin1Override);
        // $admin2Override = rewrite($_POST["admin2_override"]);
        // $admin2Override = str_replace(",", "", $admin2Override);
        // $admin3Override = rewrite($_POST["admin3_override"]);
        // $admin3Override = str_replace(",", "", $admin3Override);
          $admin1Override = 0;
          $admin2Override = 0;
          $admin3Override = 0;

        // $totalAdminOverride = $listerOverride + $admin1Override + $admin2Override + $admin3Override;
          $totalAdminOverride = $listerOverride;
        $gicProfit = $totalDeveloperComm - $withRate - $totalAdminOverride;
    }

    $agentBalance = $agentComm.",".$agentClaim;
    $ulBalance = $ulOverride.",".$agentClaim;
    $uulBalance = $uulOverride.",".$agentClaim;
    $uuulBalance = $uuulOverride.",".$agentClaim;
    $uuuulBalance = $uuuulOverride.",".$agentClaim;
    $uuuuulBalance = $uuuuulOverride.",".$agentClaim;
    $plBalance = $plOverride.",".$agentClaim;
    $hosBalance = $hosOverride.",".$agentClaim;
    $listerBalance = $listerOverride.",".$agentClaim;

    $totalClaimedDevAmt = $loanDetails[0]->getTotalClaimDevAmt();
    $totalBalUnclaimAmt = $loanDetails[0]->getTotalBalUnclaimAmt();

    $commissionDetails = getCommission($conn,"WHERE loan_uid = ? ", array("loan_uid"), array($loanUid), "s");

    //===========================================================================================================================================
    $loanCheck = getLoanStatus($conn, "WHERE loan_uid = ?", array("loan_uid"), array($loanUid), "s");

    $purchaserNameCheck = $loanCheck[0]->getPurchaserName();
    if ($purchaserNameCheck != $purchaserNameImplode) {
      $column = "Purchase Name";
      if(editHistory($conn, $username, $column, $loanUid,$purchaserNameCheck, $purchaserNameImplode,$branchType))
           {}
    }

    $unitNoCheck = $loanCheck[0]->getUnitNo();
    if ($unitNoCheck != $unitNoImplode) {
      $column = "Unit No";
      if(editHistory($conn, $username, $column, $loanUid,$unitNoCheck, $unitNoImplode,$branchType))
           {}
    }
    $icCheck = $loanCheck[0]->getIc();
    if ($icCheck != $icImplode) {
      $column = "IC";
      if(editHistory($conn, $username, $column, $loanUid,$icCheck, $icImplode,$branchType))
           {}
    }
    $contactCheck = $loanCheck[0]->getContact();
    if ($contactCheck != $contactImplode) {
      $column = "Contact";
      if(editHistory($conn, $username, $column, $loanUid,$contactCheck, $contactImplode,$branchType))
           {}
    }
    $emailCheck = $loanCheck[0]->getEmail();
    if ($emailCheck != $emailImplode) {
      $column = "Email";
      if(editHistory($conn, $username, $column, $loanUid,$emailCheck, $emailImplode,$branchType))
           {}
    }
    $bookingDateCheck = $loanCheck[0]->getBookingDate();
    if ($bookingDateCheck != $bookingDate) {
      $column = "Booking Date";
      if(editHistory($conn, $username, $column, $loanUid,$bookingDateCheck, $bookingDate,$branchType))
           {}
    }
    $sqFtCheck = $loanCheck[0]->getSqFt();
    if ($sqFtCheck != $sqFt) {
      $column = "SQ FT";
      if(editHistory($conn, $username, $column, $loanUid,$sqFtCheck, $sqFt,$branchType))
           {}
    }
    $spaPriceCheck = $loanCheck[0]->getSpaPrice();
    if ($spaPriceCheck != $spaPrice) {
      $column = "SPA Price";
      if(editHistory($conn, $username, $column, $loanUid,$spaPriceCheck, $spaPrice,$branchType))
           {}
    }
    $packageCheck = $loanCheck[0]->getPackage();
    if ($packageCheck != $package) {
      $column = "Package";
      if(editHistory($conn, $username, $column, $loanUid,$packageCheck, $package,$branchType))
           {}
    }
    $discountCheck = $loanCheck[0]->getDiscount();
    if ($discountCheck != $discount) {
      $column = "Discount";
      if(editHistory($conn, $username, $column, $loanUid,$discountCheck, $discount,$branchType))
           {}
    }
    $loanAmountCheck = $loanCheck[0]->getLoanAmount();
    if ($loanAmountCheck != $loanAmount) {
      $column = "loan_amount";
      if(editHistory($conn, $username, $column, $loanUid,$loanAmountCheck, $loanAmount,$branchType))
           {}
    }
    $rebateCheck = $loanCheck[0]->getRebate();
    if ($rebateCheck != $rebate) {
      $column = "Rebate";
      if(editHistory($conn, $username, $column, $loanUid,$rebateCheck, $rebate,$branchType))
           {}
    }
    $bankApprovedCheck = $loanCheck[0]->getBankApproved();
    if ($bankApprovedCheck != $bankApproved) {
      $column = "Bank Approved";
      if(editHistory($conn, $username, $column, $loanUid,$bankApprovedCheck, $bankApproved,$branchType))
           {}
    }
    $nettPriceCheck = $loanCheck[0]->getNettPrice();
    if ($nettPriceCheck != $nettPrice) {
      $column = "Nett Price";
      if(editHistory($conn, $username, $column, $loanUid,$nettPriceCheck, $nettPrice,$branchType))
           {}
    }
    $totalDeveloperCommCheck = $loanCheck[0]->getTotalDeveloperComm();
    if ($totalDeveloperCommCheck != $totalDeveloperComm) {
      $column = "Total Developer Comm";
      if(editHistory($conn, $username, $column, $loanUid,$totalDeveloperCommCheck, $totalDeveloperComm,$branchType))
           {}
    }
    $agentCheck = $loanCheck[0]->getAgent();
    if ($agentCheck != $agent) {
      $column = "Agent";
      if(editHistory($conn, $username, $column, $loanUid,$agentCheck, $agent,$branchType))
           {}
    }
    $loanStatusCheck = $loanCheck[0]->getLoanStatus();
    if ($loanStatusCheck != $loanStatus) {
      $column = "Loan Status";
      if(editHistory($conn, $username, $column, $loanUid,$loanStatusCheck, $loanStatus,$branchType))
           {}
    }
    $remarkCheck = $loanCheck[0]->getRemark();
    if ($remarkCheck != $remark) {
      $column = "Remark";
      if(editHistory($conn, $username, $column, $loanUid,$remarkCheck, $remark,$branchType))
           {}
    }
    $bFormCollectedCheck = $loanCheck[0]->getBFormCollected();
    if ($bFormCollectedCheck != $bFormCollected) {
      $column = "B Form Collected";
      if(editHistory($conn, $username, $column, $loanUid,$bFormCollectedCheck, $bFormCollected,$branchType))
           {}
    }
    $paymentMethodCheck = $loanCheck[0]->getPaymentMethod();
    if ($paymentMethodCheck != $paymentMethod) {
      $column = "Payment Method";
      if(editHistory($conn, $username, $column, $loanUid,$paymentMethodCheck, $paymentMethod,$branchType))
           {}
    }
    $lawyerCheck = $loanCheck[0]->getLawyer();
    if ($lawyerCheck != $lawyer) {
      $column = "Lawyer";
      if(editHistory($conn, $username, $column, $loanUid,$lawyerCheck, $lawyer,$branchType))
           {}
    }
    $loSignedDateCheck = $loanCheck[0]->getLoSignedDate();
    if ($loSignedDateCheck != $loSignedDate) {
      $column = "LO Signed Date";
      if(editHistory($conn, $username, $column, $loanUid,$loSignedDateCheck, $loSignedDate,$branchType))
           {}
    }
    $laSignedDateCheck = $loanCheck[0]->getLaSignedDate();
    if ($laSignedDateCheck != $laSignedDate) {
      $column = "LA Signed Date";
      if(editHistory($conn, $username, $column, $loanUid,$laSignedDateCheck, $laSignedDate,$branchType))
           {}
    }
    $spaSignedDateCheck = $loanCheck[0]->getSpaSignedDate();
    if ($spaSignedDateCheck != $spaSignedDate) {
      $column = "SPA Signed Date";
      if(editHistory($conn, $username, $column, $loanUid,$spaSignedDateCheck, $spaSignedDate,$branchType))
           {}
    }
    $fullsetCompletedCheck = $loanCheck[0]->getFullsetCompleted();
    if ($fullsetCompletedCheck != $fullsetCompleted) {
      $column = "Fullset Completed";
      if(editHistory($conn, $username, $column, $loanUid,$fullsetCompletedCheck, $fullsetCompleted,$branchType))
           {}
    }
    $cashBuyerCheck = $loanCheck[0]->getCashBuyer();
    if ($cashBuyerCheck != $cashBuyer) {
      $column = "Cash Buyer";
      if(editHistory($conn, $username, $column, $loanUid,$cashBuyerCheck, $cashBuyer,$branchType))
           {}
    }
    $cancelledBookingCheck = $loanCheck[0]->getCancelledBooking();
    if ($cancelledBookingCheck != $cancelledBooking) {
      $column = "Cancelled Booking";
      if(editHistory($conn, $username, $column, $loanUid,$cancelledBookingCheck, $cancelledBooking,$branchType))
           {}
    }
    $caseStatusCheck = $loanCheck[0]->getCaseStatus();
    if ($caseStatusCheck != $caseStatus) {
      $column = "Case Status";
      if(editHistory($conn, $username, $column, $loanUid,$caseStatusCheck, $caseStatus,$branchType))
           {}
    }
    $eventPersonalCheck = $loanCheck[0]->getEventPersonal();
    if ($eventPersonalCheck != $eventPersonal) {
      $column = "Event/Personal";
      if(editHistory($conn, $username, $column, $loanUid,$eventPersonalCheck, $eventPersonal,$branchType))
           {}
    }
    $rateCheck = $loanCheck[0]->getRate();
    if ($rateCheck != $rate) {
      $column = "Rate";
      if(editHistory($conn, $username, $column, $loanUid,$rateCheck, $rate,$branchType))
           {}
    }
    $agentCommCheck = $loanCheck[0]->getAgentComm();
    if ($agentCommCheck != $agentComm) {
      $column = "Agent Comm";
      if(editHistory($conn, $username, $column, $loanUid,$agentCommCheck, $agentComm,$branchType))
           {}
    }
    $upline1Check = $loanCheck[0]->getUpline1();
    if ($upline1Check != $upline1) {
      $column = "Upline 1";
      if(editHistory($conn, $username, $column, $loanUid,$upline1Check, $upline1,$branchType))
           {}
    }
    $upline2Check = $loanCheck[0]->getUpline2();
    if ($upline2Check != $upline2) {
      $column = "Upline 2";
      if(editHistory($conn, $username, $column, $loanUid,$upline2Check, $upline2,$branchType))
           {}
    }
    $upline3Check = $loanCheck[0]->getUpline3();
    if ($upline3Check != $upline3) {
      $column = "Upline 3";
      if(editHistory($conn, $username, $column, $loanUid,$upline3Check, $upline3,$branchType))
           {}
    }
    $upline4Check = $loanCheck[0]->getUpline4();
    if ($upline4Check != $upline4) {
      $column = "Upline 4";
      if(editHistory($conn, $username, $column, $loanUid,$upline4Check, $upline4,$branchType))
           {}
    }
    $upline5Check = $loanCheck[0]->getUpline5();
    if ($upline5Check != $upline5) {
      $column = "Upline 5";
      if(editHistory($conn, $username, $column, $loanUid,$upline5Check, $upline5,$branchType))
           {}
    }
    $plNameCheck = $loanCheck[0]->getPlName();
    if ($plNameCheck != $plName) {
      $column = "PL Name";
      if(editHistory($conn, $username, $column, $loanUid,$plNameCheck, $plName,$branchType))
           {}
    }
    $hosNameCheck = $loanCheck[0]->getHosName();
    if ($hosNameCheck != $hosName) {
      $column = "HOS Name";
      if(editHistory($conn, $username, $column, $loanUid,$hosNameCheck, $hosName,$branchType))
           {}
    }
    $listerNameCheck = $loanCheck[0]->getListerName();
    if ($listerNameCheck != $listerName) {
      $column = "Lister Name";
      if(editHistory($conn, $username, $column, $loanUid,$listerNameCheck, $listerName,$branchType))
           {}
    }
    $upline1OverrideCheck = $loanCheck[0]->getUlOverride();
    if ($upline1OverrideCheck != $ulOverride) {
      $column = "Upline1 Override";
      // if(editHistory($conn, $username, $column, $loanUid,$upline1OverrideCheck, $ulOverride))
      //      {}
    }
    $upline2OverrideCheck = $loanCheck[0]->getUulOverride();
    if ($upline2OverrideCheck != $uulOverride) {
      $column = "Upline2 Override";
      // if(editHistory($conn, $username, $column, $loanUid,$upline2OverrideCheck, $uulOverride))
      //      {}
    }
    $plOverrideCheck = $loanCheck[0]->getPlOverride();
    if ($plOverrideCheck != $plOverride) {
      $column = "PL Override";
      // if(editHistory($conn, $username, $column, $loanUid,$plOverrideCheck, $plOverride))
      //      {}
    }
    $hosOverrideCheck = $loanCheck[0]->getHosOverride();
    if ($hosOverrideCheck != $hosOverride) {
      $column = "HOS Override";
      // if(editHistory($conn, $username, $column, $loanUid,$hosOverrideCheck, $hosOverride))
      //      {}
    }
    $listerOverrideCheck = $loanCheck[0]->getListerOverride();
    if ($listerOverrideCheck != $listerOverride) {
      $column = "Lister Override";
      // if(editHistory($conn, $username, $column, $loanUid,$listerOverrideCheck, $listerOverride))
      //      {}
    }
    // $admin1OverrideCheck = $loanCheck[0]->getAdmin1Override();
    // if ($admin1OverrideCheck != $admin1Override) {
    //   $column = "Admin1 Override";
    //   // if(editHistory($conn, $username, $column, $loanUid,$admin1OverrideCheck, $admin1Override))
    //   //      {}
    // }
    // $admin2OverrideCheck = $loanCheck[0]->getAdmin2Override();
    // if ($admin2OverrideCheck != $admin2Override) {
    //   $column = "Admin2 Override";
    //   // if(editHistory($conn, $username, $column, $loanUid,$admin2OverrideCheck, $admin2Override))
    //   //      {}
    // }
    // $admin3OverrideCheck = $loanCheck[0]->getAdmin3Override();
    // if ($admin3OverrideCheck != $admin3Override) {
    //   $column = "Admni3 Override";
    //   // if(editHistory($conn, $username, $column, $loanUid,$admin3OverrideCheck, $admin3Override))
    //   //      {}
    // }
    $gicProfitCheck = $loanCheck[0]->getGicProfit();
    if ($gicProfitCheck != $gicProfit) {
      $column = "GIC Profit";
      if(editHistory($conn, $username, $column, $loanUid,$gicProfitCheck, $gicProfit,$branchType))
           {}
    }
    $totalClaimedDevAmtCheck = $loanCheck[0]->getTotalClaimDevAmt();
    if ($totalClaimedDevAmtCheck != $totalClaimedDevAmt) {
      $column = "Total Claim Dev Amount";
      if(editHistory($conn, $username, $column, $loanUid,$totalClaimedDevAmtCheck, $totalClaimedDevAmt,$branchType))
           {}
    }
    $totalBalUnclaimAmtCheck = $loanCheck[0]->getTotalBalUnclaimAmt();
    if ($totalBalUnclaimAmtCheck != $totalBalUnclaimAmt) {
      $column = "Total Unclaimed Dev Amt";
      if(editHistory($conn, $username, $column, $loanUid,$totalBalUnclaimAmtCheck, $totalBalUnclaimAmt,$branchType))
           {}
    }

    //==================================================================================================================================

}
// echo $ulOverride."<br>";
// echo $uulOverride."<br>";
// echo $uuulOverride."<br>";
// echo $uuuulOverride."<br>";
// echo $uuuuulOverride."<br>";
if(isset($_POST['editSubmit']))
// if(1 != 1)
{
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($projectName)
    {
        array_push($tableName,"project_name");
        array_push($tableValue,$projectName);
        $stringType .=  "s";
    }
    if($purchaserNameImplode)
    {
        array_push($tableName,"purchaser_name");
        array_push($tableValue,$purchaserNameImplode);
        $stringType .=  "s";
    }
    if($unitNoImplode)
    {
        array_push($tableName,"unit_no");
        array_push($tableValue,$unitNoImplode);
        $stringType .=  "s";
    }if($icImplode)
    {
        array_push($tableName,"ic");
        array_push($tableValue,$icImplode);
        $stringType .=  "s";
    }if($contactImplode)
    {
        array_push($tableName,"contact");
        array_push($tableValue,$contactImplode);
        $stringType .=  "s";
    }
    if($emailImplode)
    {
        array_push($tableName,"email");
        array_push($tableValue,$emailImplode);
        $stringType .=  "s";
    }
    if($bookingDate)
    {
        array_push($tableName,"booking_date");
        array_push($tableValue,$bookingDate);
        $stringType .=  "s";
    }
    if($sqFt)
    {
        array_push($tableName,"sq_ft");
        array_push($tableValue,$sqFt);
        $stringType .=  "s";
    }
    if($spaPrice)
    {
        array_push($tableName,"spa_price");
        array_push($tableValue,$spaPrice);
        $stringType .=  "s";
    }
    if($package)
    {
        array_push($tableName,"package");
        array_push($tableValue,$package);
        $stringType .=  "s";
    }
    if($discount)
    {
        array_push($tableName,"discount");
        array_push($tableValue,$discount);
        $stringType .=  "s";
    }if($rebate)
    {
        array_push($tableName,"rebate");
        array_push($tableValue,$rebate);
        $stringType .=  "s";
    }
    if($nettPrice)
    {
        array_push($tableName,"nettprice");
        array_push($tableValue,$nettPrice);
        $stringType .=  "s";
    }
    if($totalDeveloperComm)
    {
        array_push($tableName,"totaldevelopercomm");
        array_push($tableValue,$totalDeveloperComm);
        $stringType .=  "s";
    }
    if($agent)
    {
        array_push($tableName,"agent");
        array_push($tableValue,$agent);
        $stringType .=  "s";
    }
    if($loanStatus)
    {
        array_push($tableName,"loanstatus");
        array_push($tableValue,$loanStatus);
        $stringType .=  "s";
    }
    if($remark)
    {
        array_push($tableName,"remark");
        array_push($tableValue,$remark);
        $stringType .=  "s";
    }
    if($bFormCollected)
    {
        array_push($tableName,"bform_Collected");
        array_push($tableValue,$bFormCollected);
        $stringType .=  "s";
    }if($paymentMethod)
    {
        array_push($tableName,"payment_method");
        array_push($tableValue,$paymentMethod);
        $stringType .=  "s";
    }if($lawyer)
    {
        array_push($tableName,"lawyer");
        array_push($tableValue,$lawyer);
        $stringType .=  "s";
    }
    if($bankApproved)
    {
        array_push($tableName,"bank_approved");
        array_push($tableValue,$bankApproved);
        $stringType .=  "s";
    }
    if($loSignedDate)
    {
        array_push($tableName,"lo_signed_date");
        array_push($tableValue,$loSignedDate);
        $stringType .=  "s";
    }
    if($laSignedDate)
    {
        array_push($tableName,"la_signed_date");
        array_push($tableValue,$laSignedDate);
        $stringType .=  "s";
    }
    if($spaSignedDate)
    {
        array_push($tableName,"spa_signed_date");
        array_push($tableValue,$spaSignedDate);
        $stringType .=  "s";
    }
    if($fullsetCompleted)
    {
        array_push($tableName,"fullset_completed");
        array_push($tableValue,$fullsetCompleted);
        $stringType .=  "s";
    }
    if($cashBuyer)
    {
        array_push($tableName,"cash_buyer");
        array_push($tableValue,$cashBuyer);
        $stringType .=  "s";
    }
    if($cancelledBooking)
    {
        array_push($tableName,"cancelled_booking");
        array_push($tableValue,$cancelledBooking);
        $stringType .=  "s";
    }
    if($caseStatus)
    {
        array_push($tableName,"case_status");
        array_push($tableValue,$caseStatus);
        $stringType .=  "s";
    }
    if($eventPersonal)
    {
        array_push($tableName,"event_personal");
        array_push($tableValue,$eventPersonal);
        $stringType .=  "s";
    }
    if($rate)
    {
        array_push($tableName,"rate");
        array_push($tableValue,$rate);
        $stringType .=  "s";
    }
    if($agentComm)
    {
        // $afterDeductAdv = $currentAgentComm - $amount;

        array_push($tableName,"agent_comm");
        array_push($tableValue,$agentComm);
        $stringType .=  "s";
    }
    if($upline1 || !$upline1)
    {
        array_push($tableName,"upline1");
        array_push($tableValue,$upline1);
        $stringType .=  "s";
    }
    if($upline2 || !$upline2)
    {
        array_push($tableName,"upline2");
        array_push($tableValue,$upline2);
        $stringType .=  "s";
    }
    if($upline3 || !$upline3)
    {
        array_push($tableName,"upline3");
        array_push($tableValue,$upline3);
        $stringType .=  "s";
    }
    if($upline4 || !$upline4)
    {
        array_push($tableName,"upline4");
        array_push($tableValue,$upline4);
        $stringType .=  "s";
    }
    if($upline5 || !$upline5)
    {
        array_push($tableName,"upline5");
        array_push($tableValue,$upline5);
        $stringType .=  "s";
    }
    if($plName)
    {
        array_push($tableName,"pl_name");
        array_push($tableValue,$plName);
        $stringType .=  "s";
    }if($hosName)
    {
        array_push($tableName,"hos_name");
        array_push($tableValue,$hosName);
        $stringType .=  "s";
    }if($listerName)
    {
        array_push($tableName,"lister_name");
        array_push($tableValue,$listerName);
        $stringType .=  "s";
    }
    if($ulOverride || !$ulOverride)
    {
        array_push($tableName,"ul_override");
        array_push($tableValue,$ulOverride);
        $stringType .=  "s";
    }
    if($uulOverride || !$uulOverride)
    {
        array_push($tableName,"uul_override");
        array_push($tableValue,$uulOverride);
        $stringType .=  "s";
    }
    if($uuulOverride || !$uuulOverride)
    {
        array_push($tableName,"uuul_override");
        array_push($tableValue,$uuulOverride);
        $stringType .=  "s";
    }
    if($uuuulOverride || !$uuuulOverride)
    {
        array_push($tableName,"uuuul_override");
        array_push($tableValue,$uuuulOverride);
        $stringType .=  "s";
    }
    if($uuuuulOverride || !$uuuuulOverride)
    {
        array_push($tableName,"uuuuul_override");
        array_push($tableValue,$uuuuulOverride);
        $stringType .=  "s";
    }
    if($plOverride)
    {
        array_push($tableName,"pl_override");
        array_push($tableValue,$plOverride);
        $stringType .=  "s";
    }
    if($hosOverride)
    {
        array_push($tableName,"hos_override");
        array_push($tableValue,$hosOverride);
        $stringType .=  "s";
    }
    if($listerOverride)
    {
        array_push($tableName,"lister_override");
        array_push($tableValue,$listerOverride);
        $stringType .=  "s";
    }
    if($ulBalance || !$ulOverride)
    {
        array_push($tableName,"ul_balance");
        array_push($tableValue,$ulBalance);
        $stringType .=  "s";
    }
    if($uulBalance || !$uulBalance)
    {
        array_push($tableName,"uul_balance");
        array_push($tableValue,$uulBalance);
        $stringType .=  "s";
    }
    if($uuulBalance || !$uuulBalance)
    {
        array_push($tableName,"uul_balance");
        array_push($tableValue,$uuulBalance);
        $stringType .=  "s";
    }
    if($uuuulBalance || !$uuuulBalance)
    {
        array_push($tableName,"uuuul_balance");
        array_push($tableValue,$uuuulBalance);
        $stringType .=  "s";
    }
    if($uuuuulBalance || !$uuuuulBalance)
    {
        array_push($tableName,"uuuuul_balance");
        array_push($tableValue,$uuuuulBalance);
        $stringType .=  "s";
    }
    if($agentBalance || !$agentBalance)
    {
        array_push($tableName,"agent_balance");
        array_push($tableValue,$agentBalance);
        $stringType .=  "s";
    }
    if($plBalance)
    {
        array_push($tableName,"pl_balance");
        array_push($tableValue,$plBalance);
        $stringType .=  "s";
    }
    if($hosBalance)
    {
        array_push($tableName,"hos_balance");
        array_push($tableValue,$hosBalance);
        $stringType .=  "s";
    }
    if($listerBalance)
    {
        array_push($tableName,"lister_balance");
        array_push($tableValue,$listerBalance);
        $stringType .=  "s";
    }
    // if($admin1Override)
    // {
    //     array_push($tableName,"admin1_override");
    //     array_push($tableValue,$admin1Override);
    //     $stringType .=  "s";
    // }if($admin2Override)
    // {
    //     array_push($tableName,"admin2_override");
    //     array_push($tableValue,$admin2Override);
    //     $stringType .=  "s";
    // }
    // if($admin3Override)
    // {
    //     array_push($tableName,"admin3_override");
    //     array_push($tableValue,$admin3Override);
    //     $stringType .=  "s";
    // }
    if($loanAmount || $loanAmount)
    {
        array_push($tableName,"loan_amount");
        array_push($tableValue,$loanAmount);
        $stringType .=  "s";
    }
    if($gicProfit)
    {
        array_push($tableName,"gic_profit");
        array_push($tableValue,$gicProfit);
        $stringType .=  "s";
    }
    if($totalClaimedDevAmt && !$advanceDetails)
    {
        // $totalClaimedDevAmt += $amount;
        array_push($tableName,"total_claimed_dev_amt");
        array_push($tableValue,$totalClaimedDevAmt);
        $stringType .=  "s";
    }
    if($totalClaimedDevAmt && $advanceDetails)
    {
        array_push($tableName,"total_claimed_dev_amt");
        array_push($tableValue,$totalClaimedDevAmt);
        $stringType .=  "s";
    }
    if(!$totalClaimedDevAmt && !$advanceDetails)
    {
        // $totalClaimedDevAmt += $amount;
        array_push($tableName,"total_claimed_dev_amt");
        array_push($tableValue,$totalClaimedDevAmt);
        $stringType .=  "s";
    }
    if(!$totalClaimedDevAmt && $advanceDetails)
    {
        array_push($tableName,"total_claimed_dev_amt");
        array_push($tableValue,$totalClaimedDevAmt);
        $stringType .=  "s";
    }
    if($totalBalUnclaimAmt && !$advanceDetails)
    {
        // $totalBalUnclaimAmt -= $amount;
        array_push($tableName,"total_bal_unclaim_amt");
        array_push($tableValue,$totalBalUnclaimAmt);
        $stringType .=  "s";
    }
    if($totalBalUnclaimAmt && $advanceDetails)
    {
        array_push($tableName,"total_bal_unclaim_amt");
        array_push($tableValue,$totalBalUnclaimAmt);
        $stringType .=  "s";
    }

    array_push($tableValue,$loanUid);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE loan_uid = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      if ($caseStatus == 'COMPLETED' && !$advanceDetails) {

       if(addAdvancedSlip($conn, $idd, $unitNoImplode, $projectName, $bookingDate, $loanUid, $agentFullName, $amount, $status, $receiveStatus,$agentIc,$paymentMethodAgentSlip,$advancedId,$agentFullName,$branchType))
            {
              $username = $_SESSION['username'];
              $column = "Issued Advance";
              if(editHistory($conn, $username, $column, $advancedId,$detailsBefore, $detailsBefore,$branchType))
                   {}

                 //$_SESSION['messageType'] = 1;
                 // header('Location: ../admin1Product.php');
                 //echo "register success";
            }
      }
      if ($_SESSION['usertype_level'] == 1) {
        $_SESSION['messageType'] = 1;
        header('Location: ../admin1Product.php?type=3');
      }elseif ($_SESSION['usertype_level'] == 2) {
        $_SESSION['messageType'] = 1;
        header('Location: ../admin2Product.php?type=2');
      }
        // $_SESSION['messageType'] = 1;
        // header('Location: ../admin1Product.php');



    }
    else
    {
        echo "fail";

    }
}
else
{
  //  echo "dunno";

}

  if( isset($_POST['deleteProduct']) )
  	{
  		$id = $_POST['id'];
  		$sql= "UPDATE product SET display=0 WHERE id=$id";

if ($conn->query($sql) === TRUE) {
  header('Location: ../adminProduct.php');
} else {
  header('Location: ../adminProduct.php');
}

  	}

    if( isset($_POST['restoreSubmit']) )
    	{
    		$id = $_POST['id'];
    		$sql= "UPDATE product SET display=1 WHERE id=$id";

  if ($conn->query($sql) === TRUE) {
    header('Location: ../adminProduct.php');
  } else {
    header('Location: ../adminProduct.php');
  }

    	}





?>
