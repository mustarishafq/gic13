<?php
if (session_id() == ""){
     session_start();
 }

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['loginButton'])){
        $fullName = rewrite($_POST['full_name']);
        $password = $_POST['password'];

        $userRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($fullName),"s");
        if($userRows)
        {
            $user = $userRows[0];

            // if($user->getUserType() == 1)
            // {
                $tempPass = hash('sha256',$password);
                $finalPassword = hash('sha256', $user->getSalt() . $tempPass);

                if($finalPassword == $user->getPassword())
                {
                    if(isset($_POST['remember-me']))
                    {

                        setcookie('email-oilxag', $username, time() + (86400 * 30), "/");
                        setcookie('password-oilxag', $password, time() + (86400 * 30), "/");
                        setcookie('remember-oilxag', 1, time() + (86400 * 30), "/");
                        // echo 'remember me';
                    }
                    else
                    {
                        setcookie('email-oilxag', '', time() + (86400 * 30), "/");
                        setcookie('password-oilxag', '', time() + (86400 * 30), "/");
                        setcookie('remember-oilxag', 0, time() + (86400 * 30), "/");
                        // echo 'null';
                    }

                    $_SESSION['uid'] = $user->getUid();
                    $_SESSION['username'] = $user->getUsername();
                    $_SESSION['fullname'] = $user->getFullName();
                    $_SESSION['usertype_level'] = $user->getUserType();
                    $_SESSION['branch_type'] = $user->getBranchType();


                    if($user->getUserType() == 0)
                    {
                        // header('Location: ../adminDashboard.php');
                        // header('Location: ../adminDashboard.php?lang=en');
                    }
                    elseif ($user->getUserType() == 1 )
                    {
                        $_SESSION['messageType'] = 10;
                        header('Location: ../admin1Product.php?type=1');
                    }
                    elseif ($user->getUserType() == 2)
                    {
                        $_SESSION['messageType'] = 10;
                        // header('Location: ../admin2Product.php?type=1');
                        header('Location: ../admin2.php?type=1');
                    }
                    elseif ($user->getUserType() == 3)
                    {
                        $_SESSION['messageType'] = 10;
                        header('Location: ../agentDashboard.php?type=1');
                    }

                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../index.php?type=11');
                    // promptError("Incorrect email or password");
                }
            // }
            // else
            // {
            //         $_SESSION['messageType'] = 1;
            //         header('Location: ../index.php?type=10');
            //    //  promptError("Please confirm your registration inside your email");
            // }

        }
        else
        {
          $_SESSION['messageType'] = 1;
          header('Location: ../index.php?type=7');
          //echo "// no user with this email ";
          //   promptError("This account does not exist");
        }
    }

    $conn->close();
}
?>
