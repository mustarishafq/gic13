<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/../adminAccess1.php';
require_once dirname(__FILE__) . '/../timezone.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/OtherInvoice.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter),
     "sssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
$username = $_SESSION['username'];
$invoiceId = rewrite($_POST['invoice_id']);
$attn = rewrite($_POST['attn']);
$requestDate = rewrite($_POST['request_date']);
$requestDate = $requestDate;
$bankName = rewrite($_POST['bank_name']);
$bankAccHolder = rewrite($_POST['bank_acc_holder']);
$bankAccNo = rewrite($_POST['bank_acc_no']);
$itemAmount = $_POST['amount'];
$charges = rewrite($_POST['charges']);
$itemAmountImpp = implode(";",$itemAmount);
$itemAmountImpRem = str_replace(",","",$itemAmountImpp);
$itemAmountImp = str_replace(";",",",$itemAmountImpRem);
$itemAmountExp = explode(",",$itemAmountImp);
$amount = [];
$finalAmount = [];


$otherInvoiceDetails = getOtherInvoice($conn, "WHERE invoice_id =? ",array("invoice_id"),array($invoiceId),"s");
echo $otherInvoiceDetails[0]->getInvoiceId();
$id = $otherInvoiceDetails[0]->getId();
$attnCurrent = $otherInvoiceDetails[0]->getProjectHandler();
$requestDateCurrent = $otherInvoiceDetails[0]->getBookingDate();
$bankNameCurrent = $otherInvoiceDetails[0]->getBankName();
$bankAccHolderCurrent = $otherInvoiceDetails[0]->getBankAccountHolder();
$bankAccNoCurrent = $otherInvoiceDetails[0]->getBankAccountNo();
$chargesCurrent = $otherInvoiceDetails[0]->getCharges();
$finalAmountCurrent = $otherInvoiceDetails[0]->getFinalAmount();
$amountCurrent = $otherInvoiceDetails[0]->getAmount();

if ($itemAmountExp) {
  for ($i=0; $i <count($itemAmountExp) ; $i++) {
    if ($charges == 'YES') {
      $finalAmount[] = $itemAmountExp[$i] * 1.06;
      $amount[] = $itemAmountExp[$i];
    }
    if ($charges == 'NO'){
      $finalAmount[] = $itemAmountExp[$i];
      $amount[] = $itemAmountExp[$i] - $itemAmountExp[$i]*(6/100);
    }
  }
}
echo $finalAmountImp = implode(",",$finalAmount);
echo $amount = implode(",",$amount);

// $editDetails = "$attn,$requestDate,$bankName,$bankAccHolder,$bankAccNo,$finalAmount";
$editDetails = "$attn,$requestDate,$bankName,$bankAccHolder,$bankAccNo,$charges";
$editDetailsExp = explode(",",$editDetails);

// $currentDetails = "$attnCurrent,$requestDateCurrent,$bankNameCurrent,$bankAccHolderCurrent,$bankAccNoCurrent,$finalAmountCurrent";
$currentDetails = "$attnCurrent,$requestDateCurrent,$bankNameCurrent,$bankAccHolderCurrent,$bankAccNoCurrent,$chargesCurrent";
$currentDetailsExp = explode(",",$currentDetails);

// $columnDetails = "Attn,Date,Bank Name,Bank Account Holder,Account No.,Final Amount";
$columnDetails = "Attn,Date,Bank Name,Bank Account Holder,Account No.,Charges";
$columnDetailsExp = explode(",",$columnDetails);



if (isset($_POST['editBtn'])) {

for ($i=0; $i < count($editDetailsExp) ; $i++) {
  if ($editDetailsExp[$i] != $currentDetailsExp[$i]) {
    $column = $columnDetailsExp[$i];
    if(editHistory($conn, $username, $column, $invoiceId,$currentDetailsExp[$i], $editDetailsExp[$i]))
         {}
  }
}

  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($attn)
  {
      array_push($tableName,"project_handler");
      array_push($tableValue,$attn);
      $stringType .=  "s";
  }
  if($requestDate)
  {
      array_push($tableName,"booking_date");
      array_push($tableValue,$requestDate);
      $stringType .=  "s";
  }
  if($bankName)
  {
      array_push($tableName,"bank_name");
      array_push($tableValue,$bankName);
      $stringType .=  "s";
  }
  if($bankAccHolder)
  {
      array_push($tableName,"bank_account_holder");
      array_push($tableValue,$bankAccHolder);
      $stringType .=  "s";
  }
  if($bankAccNo)
  {
      array_push($tableName,"bank_account_no");
      array_push($tableValue,$bankAccNo);
      $stringType .=  "s";
  }
  if($finalAmountImp)
  {
      array_push($tableName,"final_amount");
      array_push($tableValue,$finalAmountImp);
      $stringType .=  "s";
  }
  if($amount)
  {
      array_push($tableName,"amount");
      array_push($tableValue,$amount);
      $stringType .=  "s";
  }
  if($itemAmountImp)
  {
      array_push($tableName,"claims_status");
      array_push($tableValue,$itemAmountImp);
      $stringType .=  "s";
  }
  if($charges)
  {
      array_push($tableName,"charges");
      array_push($tableValue,$charges);
      $stringType .=  "s";
  }
  array_push($tableValue,$invoiceId);
  $stringType .=  "s";
  $withdrawUpdated = updateDynamicData($conn,"invoice_other"," WHERE invoice_id = ? ",$tableName,$tableValue,$stringType);

  if($withdrawUpdated)
  {
    $_SESSION['messageType'] = 1;
    $_SESSION['invoice_id'] = $invoiceId;
    header('location: ../invoiceOther.php?type=2');
  }
}
 ?>
