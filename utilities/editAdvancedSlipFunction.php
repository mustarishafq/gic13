<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/../adminAccess1.php';
require_once dirname(__FILE__) . '/../timezone.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/AdvancedSlip.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

function editHistory($conn, $username, $column, $loanUid,$detailsBefore, $detailsAfter)
{
     if(insertDynamicData($conn,"edit_history", array( "username","details", "loan_uid","data_before","data_after"),
     array($username, $column, $loanUid,$detailsBefore,$detailsAfter),
     "sssss") === null)
     {
          //    echo $finalPassword;
     }
     else
     {
          //   echo "bbbb";
     }

     return true;
}
$agent = rewrite($_POST['agent']);
$id = $_POST['id'];
$idImplode = $id;
$idExplode = explode(",",$id);
// $attn = rewrite($_POST['attn']);
$requestDate = rewrite($_POST['request_date']);
// $requestDate = $requestDate;
$paymentMethod = rewrite($_POST['payment_method']);
$checkId = rewrite($_POST['check_id']);
$icNo = rewrite($_POST['ic_no']);
$username = $_SESSION['username'];
// $finalAmount = rewrite($_POST['final_amount']);
// $finalAmount = str_replace(",","",$finalAmount);


for ($j=0; $j <count($idExplode) ; $j++) {

  $advancedDetails = getAdvancedSlip($conn, "WHERE id =? ",array("id"),array($idExplode[$j]),"s");
  // echo $advancedDetails[0]->getInvoiceId();
  $id = $advancedDetails[0]->getID();
  $loanUid = $advancedDetails[0]->getLoanUid();
  $advancedId = $advancedDetails[0]->getAdvancedId();
  $agentCurrent = $advancedDetails[0]->getAgent();
  $requestDateCurrent = $advancedDetails[0]->getDateCreated();
  $paymentMethodCurrent = $advancedDetails[0]->getPaymentMethod();
  $checkIdCurrent = $advancedDetails[0]->getCheckID();
  $icNoCurrent = $advancedDetails[0]->getIcNo();
  // $charges = $advancedDetails[0]->getCharges();
  // if ($charges == 'NO') {
  //   $finalAmount = $finalAmount;
  //   $amount = $finalAmount;
  // }elseif ($charges == 'YES') {
  //   $finalAmount = $finalAmount;
  //   $amount = $finalAmount /1.06;
  // }

  $editDetails = "$agent,$requestDate,$paymentMethod,$checkId,$icNo";
  $editDetailsExp = explode(",",$editDetails);

  $currentDetails = "$agentCurrent,$requestDateCurrent,$paymentMethodCurrent,$checkIdCurrent,$icNoCurrent";
  $currentDetailsExp = explode(",",$currentDetails);

  $columnDetails = "Agent,Date,Payment Method,Cheque No.,I/C No.";
  $columnDetailsExp = explode(",",$columnDetails);



  if (isset($_POST['editBtn'])) {

  for ($i=0; $i < count($editDetailsExp) ; $i++) {
    if ($editDetailsExp[$i] != $currentDetailsExp[$i]) {
      $column = $columnDetailsExp[$i];
      if(editHistory($conn, $username, $column, $advancedId,$currentDetailsExp[$i], $editDetailsExp[$i]))
           {}
    }
  }

    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($agent)
    {
        array_push($tableName,"agent");
        array_push($tableValue,$agent);
        $stringType .=  "s";
    }
    if($requestDate)
    {
        array_push($tableName,"booking_date");
        array_push($tableValue,$requestDate);
        $stringType .=  "s";
    }
    if($paymentMethod)
    {
        array_push($tableName,"payment_method");
        array_push($tableValue,$paymentMethod);
        $stringType .=  "s";
    }
    if($checkId || !$checkId)
    {
        array_push($tableName,"check_id");
        array_push($tableValue,$checkId);
        $stringType .=  "s";
    }
    if($icNo)
    {
        array_push($tableName,"ic_no");
        array_push($tableValue,$icNo);
        $stringType .=  "s";
    }
    // if($finalAmount)
    // {
    //     array_push($tableName,"final_amount");
    //     array_push($tableValue,$finalAmount);
    //     $stringType .=  "s";
    // }
    // if($amount)
    // {
    //     array_push($tableName,"amount");
    //     array_push($tableValue,$amount);
    //     $stringType .=  "s";
    // }
    array_push($tableValue,$id);
    $stringType .=  "s";
    $withdrawUpdated = updateDynamicData($conn,"advance_slip"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($withdrawUpdated)
    {
      $_SESSION['messageType'] = 1;
      $_SESSION['loan_uid'] = $loanUid;
      $_SESSION['idImp'] = $idImplode;
      header('location: ../advancedSlip.php?type=4');
    }
  }

}


 ?>
