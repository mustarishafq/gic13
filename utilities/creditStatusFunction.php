<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../timezone.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/CreditNoteInvoice.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$creditStatus = $_POST['credit'];
$creditId = $_POST['credit_id'];
$null = null;

$invoiceMultiDetails = getCreditNoteInvoice($conn, "WHERE credit_id =?",array("credit_id"),array($creditId), "s");
$unitNoImp = $invoiceMultiDetails[0]->getUnitNo();
$UnitNoExp = explode(",",$unitNoImp);
$claimStatusImp = $invoiceMultiDetails[0]->getClaimsStatus();
$claimStatusExp = explode(",",$claimStatusImp);

if (isset($_POST['credit-button'])) {
  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  // //echo "save to database";
  if($creditStatus)
  {
      array_push($tableName,"credit_status");
      array_push($tableValue,$creditStatus);
      $stringType .=  "s";

      array_push($tableValue,$creditId);
      $stringType .=  "s";
      $creditInvoiceUpdated = updateDynamicData($conn,"invoice_credit"," WHERE credit_id = ? ",$tableName,$tableValue,$stringType);

      if($creditInvoiceUpdated){

        if ($claimStatusExp && $creditStatus == 'YES') {
          for ($i=0; $i <count($claimStatusExp) ; $i++) {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            // //echo "save to database";
            if($claimStatusExp[$i] == 1)
            {
                array_push($tableName,"status_1st");
                array_push($tableValue,$null);
                $stringType .=  "s";

                array_push($tableName,"1st_claim_amt");
                array_push($tableValue,$null);
                $stringType .=  "s";

                array_push($tableName,"sst1");
                array_push($tableValue,$null);
                $stringType .=  "s";

                array_push($tableName,"request_date1");
                array_push($tableValue,$null);
                $stringType .=  "s";

                array_push($tableName,"check_no1");
                array_push($tableValue,$null);
                $stringType .=  "s";

                array_push($tableName,"receive_date1");
                array_push($tableValue,$null);
                $stringType .=  "s";
              }
              if($claimStatusExp[$i] == 2)
              {
                  array_push($tableName,"status_2nd");
                  array_push($tableValue,$null);
                  $stringType .=  "s";

                  array_push($tableName,"2nd_claim_amt");
                  array_push($tableValue,$null);
                  $stringType .=  "s";

                  array_push($tableName,"sst2");
                  array_push($tableValue,$null);
                  $stringType .=  "s";

                  array_push($tableName,"request_date2");
                  array_push($tableValue,$null);
                  $stringType .=  "s";

                  array_push($tableName,"check_no2");
                  array_push($tableValue,$null);
                  $stringType .=  "s";

                  array_push($tableName,"receive_date2");
                  array_push($tableValue,$null);
                  $stringType .=  "s";
                }
                if($claimStatusExp[$i] == 3)
                {
                    array_push($tableName,"status_3rd");
                    array_push($tableValue,$null);
                    $stringType .=  "s";

                    array_push($tableName,"3rd_claim_amt");
                    array_push($tableValue,$null);
                    $stringType .=  "s";

                    array_push($tableName,"sst3");
                    array_push($tableValue,$null);
                    $stringType .=  "s";

                    array_push($tableName,"request_date3");
                    array_push($tableValue,$null);
                    $stringType .=  "s";

                    array_push($tableName,"check_no3");
                    array_push($tableValue,$null);
                    $stringType .=  "s";

                    array_push($tableName,"receive_date3");
                    array_push($tableValue,$null);
                    $stringType .=  "s";
                  }
                  if($claimStatusExp[$i] == 4)
                  {
                      array_push($tableName,"status_4th");
                      array_push($tableValue,$null);
                      $stringType .=  "s";

                      array_push($tableName,"4th_claim_amt");
                      array_push($tableValue,$null);
                      $stringType .=  "s";

                      array_push($tableName,"sst4");
                      array_push($tableValue,$null);
                      $stringType .=  "s";

                      array_push($tableName,"request_date4");
                      array_push($tableValue,$null);
                      $stringType .=  "s";

                      array_push($tableName,"check_no4");
                      array_push($tableValue,$null);
                      $stringType .=  "s";

                      array_push($tableName,"receive_date4");
                      array_push($tableValue,$null);
                      $stringType .=  "s";
                    }
                    if($claimStatusExp[$i] == 5)
                    {
                        array_push($tableName,"status_5th");
                        array_push($tableValue,$null);
                        $stringType .=  "s";

                        array_push($tableName,"5th_claim_amt");
                        array_push($tableValue,$null);
                        $stringType .=  "s";

                        array_push($tableName,"sst5");
                        array_push($tableValue,$null);
                        $stringType .=  "s";

                        array_push($tableName,"request_date5");
                        array_push($tableValue,$null);
                        $stringType .=  "s";

                        array_push($tableName,"check_no5");
                        array_push($tableValue,$null);
                        $stringType .=  "s";

                        array_push($tableName,"receive_date5");
                        array_push($tableValue,$null);
                        $stringType .=  "s";
                      }

                array_push($tableValue,$UnitNoExp[$i]);
                $stringType .=  "s";
                $creditInvoiceUpdated = updateDynamicData($conn,"loan_status"," WHERE unit_no = ? AND display = 'Yes' AND cancelled_booking != 'Yes' ",$tableName,$tableValue,$stringType);

                if($creditInvoiceUpdated){
                  $tableName = array();
                  $tableValue =  array();
                  $stringType =  "";
                  // //echo "save to database";
                  if($creditStatus)
                  {
                      array_push($tableName,"credit_status");
                      array_push($tableValue,$creditStatus);
                      $stringType .=  "s";
                    }

                      array_push($tableValue,$creditId);
                      $stringType .=  "s";
                      $creditInvoiceUpdated = updateDynamicData($conn,"invoice_multi"," WHERE invoice_id = ? ",$tableName,$tableValue,$stringType);

                      if($creditInvoiceUpdated){
                        $_SESSION['messageType'] = 1;
                        header('location: ../invoiceRecordCredit.php');
                      }
                      $tableName = array();
                      $tableValue =  array();
                      $stringType =  "";
                      // //echo "save to database";
                      if($creditStatus)
                      {
                          array_push($tableName,"credit_status");
                          array_push($tableValue,$creditStatus);
                          $stringType .=  "s";
                        }

                          array_push($tableValue,$creditId);
                          $stringType .=  "s";
                          $creditInvoiceUpdated = updateDynamicData($conn,"invoice"," WHERE invoice_id = ? ",$tableName,$tableValue,$stringType);

                          if($creditInvoiceUpdated){
                            $_SESSION['messageType'] = 1;
                            header('location: ../invoiceRecordCredit.php');
                          }
                }
          }
          }
        }
  }
}
 ?>
