<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn," WHERE user_type=3");

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Archived Project | GIC" />
    <title>Archived Project | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  a{
    color: maroon;
  }
  input:focus, textarea:focus, select:focus{
    outline: none;
  }
  #addNewProjectBtn{
    color: black;
  }

  th.headerSortUp{
    background-color: black;
  }
  th.headerSortDown{
    background-color: black;
  }
</style>
<body class="body">
<?php  include 'admin1Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1"><a href="adminAddNewProject.php" class="red-color-swicthing">Add New Project</a> | <a href="projectList.php" class="project red-color-swicthing">Project List</a> | Archived </h1>
    <div class="short-red-border"></div>
    <div class="section-divider width100 overflow">
      <?php
      if ($_SESSION['branch_type'] == 1) {
        $projectDetails = getProject($conn, "WHERE display = 'No'");
      }else {
        $projectDetails = getProject($conn, "WHERE display = 'No' AND branch_type =?",array("branch_type"),array($_SESSION['branch_type']), "s");
      }
       ?>

        <select id="sel_id" class="clean-select">
          <option value="">Select a project</option>
          <?php if ($projectDetails) {
            for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
              if ($projectDetails[$cnt]->getProjectName() != $types) {
                ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
                }
                }
                ?><option value="ALL">SHOW ALL</option><?php
              } ?>
        <!-- <option value="-1">Select</option>
        <option value="VIDA">kasper </option>
        <option value="VV">adad </option> -->
        <!-- <option value="14">3204 </option> -->
        </select>
        <input class="clean search-btn" type="text" name="" value="" placeholder="Search.." id="searchInput">
    </div>
<?php
  // $projectDetails = getProject($conn, "WHERE display = 'No' and branch_type = ?",array("branch_type"),array($_SESSION['branch_type']), "s");
  // if ($_SESSION['branch_type'] == 1) {
  //   $projectDetails = getProject($conn, "WHERE display = 'No'");
  // }else {
  //   $projectDetails = getProject($conn, "WHERE display = 'No' AND branch_type =?",array("branch_type"),array($_SESSION['branch_type']), "s");
  // }
 ?>
    <!-- <h2>Project List</h2> -->
<div class="width100 shipping-div2">
    <table id="myTable" class="shipping-table">
    <thead>
      <tr>
          <th>No.</th>
          <th>Project Name</th>
          <th>Person In Charge (PIC)</th>
          <th>Project Leader</th>
          <th>Company Name</th>
          <th>Company Address</th>
          <th>Project Claim</th>
          <th>Other Claim (RM)</th>
          <!-- <th>Display</th> -->
          <th>Date Created</th>
          <th>Date Updated</th>
          <th>Created</th>
          <th>Action</th>
          <th>Restore</th>
      </tr>
    </thead>
    <?php if ($projectDetails) {

     ?>
    <tbody id="myFilter">
      <?php for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {

       ?>
      <tr>
      <?php
      ?>  <td class="td"><?php echo $cnt+1 ?></td>
          <td class="td"><?php echo $projectDetails[$cnt]->getProjectName() ?></td>
          <td class="td"><?php echo $projectDetails[$cnt]->getAddProjectPpl() ?></td>
          <td class="td"><?php echo str_replace(",","<br>",$projectDetails[$cnt]->getProjectLeader()); ?></td>
          <td class="td"><?php echo $projectDetails[$cnt]->getCompanyName() ?></td>
          <td class="td"><?php echo str_replace(",","<br>",$projectDetails[$cnt]->getCompanyAddress()) ?></td>
          <td class="td"><?php echo $projectDetails[$cnt]->getProjectClaims() ?></td>
          <td class="td"><?php echo $projectDetails[$cnt]->getOtherClaims() ?></td>
          <!-- <td class="td"><?php //echo $projectDetails[$cnt]->getDisplay() ?></td> -->
          <td class="td"><?php echo date('d-m-Y', strtotime($projectDetails[$cnt]->getDateCreated())) ?></td>
          <td class="td">
            <form class="" action="historyIndividual.php" method="post">
              <input type="hidden" name="id" value="<?php echo $projectDetails[$cnt]->getProjectId() ?>">
              <button class="clean edit-anc-btn" type="submit" name="button"><a class="blue-link"><?php echo date('d-m-Y', strtotime($projectDetails[$cnt]->getDateUpdated())) ?><a></button></td>
            </form>
          <td class="td"><?php echo $projectDetails[$cnt]->getCreatorName() ?></td>
          <td class="td">  <form action="editProject.php" method="POST">
                <button class="clean edit-anc-btn hover1" type="submit" name="project_id" value="<?php echo $projectDetails[$cnt]->getId();?>">
                    <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product">
                    <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product">
                </button>
            </form></td>
            <td class="td">
                <!-- <form action="utilities/archiveLoan.php" method="POST"> -->
                    <button id="archiveBtn" class="clean edit-anc-btn hover1" type="submit" name="loan_uid_archive" value="<?php echo $projectDetails[$cnt]->getId();?>">
                        <img src="img/restore.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product">
                        <img src="img/restore2.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product">
                    </button>
                <!-- </form> -->
            </td>
      </tr>
      <?php
    }}else {
      ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="13"> No Archived Project. </td> <?php
    } ?>
    </tbody>
    </table>
  </div>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login to The System. ";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Add New Project.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully Updated The Project Details";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
<?php echo '<script src="jsPhp/adminAddNewProject.js" type="text/javascript"></script>'; ?>
</html>
