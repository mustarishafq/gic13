<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$branchType = $_SESSION['branch_type'];
$bankDetails = getBankName($conn);
// $projectList = getProject($conn, "WHERE display = 'Yes' and branch_type =?",array("branch_type"),array($branchType), "s");
$projectList = getProject($conn, "WHERE display = 'Yes'");
if (isset($_POST['product_name'])) {
$loanDetails = getLoanStatus($conn, "WHERE project_name = ? AND branch_type =?", array("project_name,branch_type"), array($_POST['product_name'],$branchType), "ss");
}
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Edit Invoice | GIC" />
    <title>Edit Invoice | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
a{
  color: red;
}
.yes-a, .no-a{
  color: black;
}
input#checkBoxPayee{
  vertical-align: middle;
  width: 18px;
  height: 18px;
  outline: 2px solid #0075ff;
  outline-offset: -2px;
}

</style>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
  <h1 class="h1-title h1-before-border shipping-h1 username"  onclick="goBack()">
    	<a  class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back3.png" class="back-btn2 hover1b" alt="back" title="back">
        	<a id="invoiceTo" style="color: black">Invoice :</a>
        </a>
    </h1>
	<div class="clear"></div>

    <div class="three-input-div dual-input-div">
    <p>Invoice Type<a>*</a> </p>
    <div id="radioType">
      <input type="radio" name="invoice_type" value="None" checked>None
      <input type="radio" name="invoice_type" value="Other">Other
      <input type="radio" name="invoice_type" value="Credit Note">Credit Note
    </div>
    </div>

    <div class="tempo-two-input-clear"></div>

    <div id="ssss">

    </div>



</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
<?php echo '<script src="jsPhp/editInvoiceGeneral.js"></script>' ?>
<script>
function goBack() {
  window.history.back();
}
</script>
</body>
</html>
