<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/adminAccess4.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/Branch.php';

require_once dirname(__FILE__) . '/utilities/ringgitMalaysia.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$address = getBranch($conn);

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Edit Slip Address | GIC" />
    <title>Edit Slip Address | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">

  a{
    color: maroon;
  }
  #branch{
  margin-top: 100px;
  }
  .text-center{
    margin-top: 100px;
  }
  .uploadBtn{
    text-align: center;
  }
</style>
<body class="body">

<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<div class="yellow-body same-padding">
  <h1 class="h1-title h1-before-border shipping-h1"><a href="settings.php" class="red-color-swicthing">Edit Profile</a> | <a href="addCompanyBranch.php" class="red-color-swicthing">Add Company Branch</a> | Edit Slip Address </h1>

<form class="" action="slipAddress.php" method="post" enctype="multipart/form-data">
<div class="clear"></div>
<div class="three-input-div dual-input-div">
  <p></p>
  <!-- <input class="dual-input clean" type="text" placeholder="Nett Price" id="nettprice" name="nettprice"> -->
</div>
  <div id="branch" class="three-input-div second-three-input dual-input-div">
    <p>Select Branch</p>
    <select class="dual-input clean pointer" name="branch">
      <option value="">Select a branch</option>
      <?php
      if ($address) {
        foreach ($address as $addressNew) {
          ?><option value="<?php echo $addressNew->getBranchType() ?>"><?php echo $addressNew->getBranchName() ?></option> <?php
        }
      } ?>
    </select>
  </div>
  <div class="tempo-two-input-clear"></div>
    <div class="width100 text-center overflow">
  		<button id="noEnterSubmit" class="confirm-btn text-center white-text clean black-button ow-margin-auto" type="submit" name="loginButton">Edit Address</button>
    </div>
</form>
</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Updated Slip Address.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Add New Project";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully Add New Booking";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Successfully Updated Profile !";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong Current Password Entered !";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Successfully Issue Payroll !";
        }
        // echo '<script>
        // $("#audio")[0].play();
        // putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        echo '<script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>
