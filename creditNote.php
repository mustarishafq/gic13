<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$branchType = $_SESSION['branch_type'];
$bankDetails = getBankName($conn);
// $projectList = getProject($conn, "WHERE display = 'Yes' and branch_type =?",array("branch_type"),array($branchType), "s");
$projectList = getProject($conn, "WHERE display = 'Yes'");
if (isset($_POST['product_name'])) {
$loanDetails = getLoanStatus($conn, "WHERE project_name = ? AND branch_type =?", array("project_name,branch_type"), array($_POST['product_name'],$branchType), "ss");
}
 ?>


<!-- ====================================================================================================== -->

<form action="utilities/creditNoteFunction.php" method="POST">
  <div>
      <div class="three-input-div dual-input-div">
      <p>Project <a>*</a> </p>
        <select class="dual-input clean" placeholder="Project" id="product_name2" name="project_name">
          <option value="">Select a Project</option>
          <?php for ($cntPro=0; $cntPro <count($projectList) ; $cntPro++)
          {
          ?>
          <option value="<?php echo $projectList[$cntPro]->getProjectName(); ?>">
          <?php echo $projectList[$cntPro]->getProjectName(); ?>
          </option>
          <?php
          }
          ?>
        </select>
      <!-- </form> -->

      </div>

    <div class="three-input-div dual-input-div second-three-input">
      <p>To</p>
      <input required id="toWho2" class="dual-input clean" type="text" placeholder="Auto Generated" name="project_handler" readonly>
    </div>

    <div class="three-input-div dual-input-div">
      <p>Date</p>
      <input class="dual-input clean" type="date" id="date_of_claims" name="date_of_claims" value="<?php echo date('Y-m-d') ?>" required>
    </div>

      <div class="tempo-two-input-clear"></div>

      <div class="tempo-two-input-clear"></div>
<div id="input02" >
      <div class="three-input-div dual-input-div">
        <!-- <p>Unit <a>*</a><img style="cursor: pointer" id="remBtn2" width="13px" align="right" src="img/mmm.png"><img style="cursor: pointer" id="addBtn2" width="13px" align="right" src="img/ppp.png"></p> -->
        <p>Unit <a>*</a></p>
        <!-- <form class="" action="" method="post"> -->
          <select class="dual-input clean" id="unit02" name="unit" >
            <option value="">Select an Unit</option>
          </select>
    </div>
    <div class="three-input-div second-three-input dual-input-div">
      <p>Amount (RM) <a>*</a></p>
      <input class="dual-input clean" type="number" step="any"  placeholder="Amount (RM)" id="amount" value="" name="amount">
    </div>
    <div class="three-input-div dual-input-div">
      <p>Remarks <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <input class="dual-input clean" type="text" id="detailss" placeholder="Details" value="" name="details">
  </div>
    <div class="tempo-two-input-clear"></div>
  </div>


      <div class="tempo-two-input-clear"></div>
	  <div class="width100 text-center overflow">

      	<button type="submit" name="upload" value="Upload" class="ow-margin-auto confirm-btn text-center white-text clean black-button">Confirm</button>
      </div>
    </form>
    </div>
    <script src="jsPhp/creditNote.js" charset="utf-8"></script>
