<?php
// require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/LoanStatus.php';
require_once dirname(__FILE__) . '/../classes/Product2.php';
require_once dirname(__FILE__) . '/../classes/Project.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addNewInvoice($conn,$id,$invoiceId,$purchaserName,$projectName,$projectHandler,$statusOfClaims,$dateOfClaims,$invoiceName,$invoiceType,
                            $item,$remark,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,
                                $loanUid,$charges,$item2,$item3,$item4,$item5,$amount2,$amount3,$amount4,$amount5,$receiveStatus,
                                    $unit,$unit2,$unit3,$unit4,$unit5,$invoiceUnitNo,$invoiceDateCreated)
{
    if(insertDynamicData($conn,"invoice",array("id","invoice_id","purchaser_name","project_name","project_handler","claims_status","claims_date","invoice_name","invoice_type",
                            "item","remark","amount","final_amount","bank_account_holder","bank_name","bank_account_no",
                            "loan_uid","charges","item2","item3","item4","item5","amount2","amount3","amount4","amount5", "receive_status",
                                "unit","unit2","unit3","unit4","unit5","unit_no","booking_date"),
    array($id,$invoiceId,$purchaserName,$projectName,$projectHandler,$statusOfClaims,$dateOfClaims,$invoiceName,$invoiceType,
            $item,$remark,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,
                $loanUid,$charges,$item2,$item3,$item4,$item5,$amount2,$amount3,$amount4,$amount5,$receiveStatus,
                    $unit,$unit2,$unit3,$unit4,$unit5,$invoiceUnitNo,$invoiceDateCreated),
    "issssssssssddsssssssssddddssssssss") === null)
    {
    //    echo $finalPassword;
    }
    else
    {
    //   echo "bbbb";
    }
    return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    // $id = rewrite($_POST["id"]);
    $loanUid = rewrite($_POST["loan_uid"]);
    $invoiceId = md5(uniqid());
    $loanStatusDetails = getLoanStatus($conn," WHERE loan_uid = ? ",array("loan_uid"),array($loanUid),"s");
    $invoiceUnitNo = $loanStatusDetails[0]->getUnitNo();
    $invoiceDateCreated = $loanStatusDetails[0]->getDateCreated();

    $purchaserName = rewrite($_POST["purchaser_name"]);

    //add in oriject, to status, date,invoice,invoice type
    $projectName = rewrite($_POST['product_name']);
    $status = 'SINGLE';

    //auto get project handler
    $projectDetails = getProject($conn," WHERE project_name = ? ",array("project_name"),array($projectName),"s");
    $projectHandler = $projectDetails[0]->getAddProjectPpl();

    $statusOfClaims = rewrite($_POST['status_of_claims']);
    $dateOfClaims = rewrite($_POST['date_of_claims']);
    $invoiceName = rewrite($_POST['invoice_name']);
    $receiveStatus = 'PENDING';

    $invoiceType = rewrite($_POST['invoice_type']);
    if (!$invoiceType)
    {
        $invoiceType = "";
    }

    $item = rewrite($_POST['item']);
    $item2 = rewrite($_POST['item2']);
    $item3 = rewrite($_POST['item3']);
    $item4 = rewrite($_POST['item4']);
    $item5 = rewrite($_POST['item5']);
    $amount = rewrite($_POST['amount']);
    $amount2 = rewrite($_POST['amount2']);
    $amount3 = rewrite($_POST['amount3']);
    $amount4 = rewrite($_POST['amount4']);
    $amount5 = rewrite($_POST['amount5']);

    $unit = rewrite($_POST['unit']);
    $unit2 = rewrite($_POST['unit2']);
    $unit3 = rewrite($_POST['unit3']);
    $unit4 = rewrite($_POST['unit4']);
    $unit5 = rewrite($_POST['unit5']);

    // $project = rewrite($_POST['project']);
    $charges = rewrite($_POST['charges']); // temporary

    if ($charges == 'YES')
    {
        $chargesNew = 0.06;
    }
    else
    {
        $chargesNew = 0;
    }
    $bankAccountHolder = 'GIC Holding';
    $bankName = 'Public Bank Sdn Bhd';
    $bankAccountNo = 123456789;

    $remark = rewrite($_POST["remark"]);

    //   FOR DEBUGGING
    // echo $id;
    // echo $unitNo;
    // echo $purchaserName;
    // echo $ic;
    // echo $contact;
    // echo $email;

    $loanStatus = getLoanStatus($conn, "WHERE loan_uid=?",array("loan_uid"),array($loanUid), "s");
    $CurrentAmountBal = $loanStatus[0]->getTotalBalUnclaimAmt();
    $CurrentAmountClaim = $loanStatus[0]->getTotalClaimDevAmt();
    $totalAmountInvoice = $amount + $amount2 + $amount3 + $amount4 + $amount5;
    $chargeAmount = $totalAmountInvoice * $chargesNew;
    $finalAmount = $chargeAmount + $totalAmountInvoice;
    $totalAmountClaim = $CurrentAmountClaim + $totalAmountInvoice;
    $totalAmountBal = $CurrentAmountBal - $totalAmountInvoice;

    // $totalAmountClaim = $amount + $amount2 + $amount3 + $amount4 + $amount5; //claim by admin2
    // $loanUid = rewrite($_POST["loan_uid"]);
    // $loanDetails = getLoanStatus($conn, "WHERE loan_uid = ? ", array("loan_uid"), array($loanUid), "s");
    // $totalClaimedDevAmt = $loanDetails[0]->getTotalClaimDevAmt();
    // $totalBalUnclaimAmt = $loanDetails[0]->getTotalBalUnclaimAmt();
    // $totalBalUnclaimAmt -= $totalAmountClaim;
    // $totalClaimedDevAmt += $totalAmountClaim;

  if ($invoiceName == 'Invoice') {

    if(isset($_POST['upload']))
    {
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        // //echo "save to database";
        if($totalAmountBal)
        {
            array_push($tableName,"total_bal_unclaim_amt");
            array_push($tableValue,$totalAmountBal);
            $stringType .=  "i";
        }
        if($totalAmountBal == 0)
        {
            array_push($tableName,"total_bal_unclaim_amt");
            array_push($tableValue,$totalAmountBal);
            $stringType .=  "i";
        }
        if($totalAmountClaim)
        {
            array_push($tableName,"total_claimed_dev_amt");
            array_push($tableValue,$totalAmountClaim);
            $stringType .=  "i";
        }
        if($name)
        {
            move_uploaded_file($_FILES['file']['tmp_name'],$target_file);
            array_push($tableName,"receipt");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
        array_push($tableValue,$loanUid);
        $stringType .=  "s";
        $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE loan_uid = ? ",$tableName,$tableValue,$stringType);
            if($withdrawUpdated)
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminWithdrawal.php?type=1');
            }
            else
            {
                echo "fail";
            }
    }
    else
    {
        echo "dunno";
    }

    if(isset($_POST['upload']))
    {   $date = date('d-m-Y');
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        // //echo "save to database";
        if($statusOfClaims == 1)
        {
            array_push($tableName,"1st_claim_amt");
            array_push($tableValue,$finalAmount);
            $stringType .=  "i";

            array_push($tableName,"request_date1");
            array_push($tableValue,$date);
            $stringType .=  "s";

            array_push($tableName,"sst1");
            array_push($tableValue,$charges);
            $stringType .=  "s";

            array_push($tableName,"status_1st");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($statusOfClaims == 2)
        {
            array_push($tableName,"2nd_claim_amt");
            array_push($tableValue,$finalAmount);
            $stringType .=  "i";

            array_push($tableName,"request_date2");
            array_push($tableValue,$date);
            $stringType .=  "s";

            array_push($tableName,"sst2");
            array_push($tableValue,$charges);
            $stringType .=  "s";

            array_push($tableName,"status_2nd");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($statusOfClaims == 3)
        {
            array_push($tableName,"3rd_claim_amt");
            array_push($tableValue,$finalAmount);
            $stringType .=  "i";

            array_push($tableName,"request_date3");
            array_push($tableValue,$date);
            $stringType .=  "s";

            array_push($tableName,"sst3");
            array_push($tableValue,$charges);
            $stringType .=  "s";

            array_push($tableName,"status_3rd");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($statusOfClaims == 4)
        {
            array_push($tableName,"4th_claim_amt");
            array_push($tableValue,$finalAmount);
            $stringType .=  "i";

            array_push($tableName,"request_date4");
            array_push($tableValue,$date);
            $stringType .=  "s";

            array_push($tableName,"sst4");
            array_push($tableValue,$charges);
            $stringType .=  "s";

            array_push($tableName,"status_4th");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($statusOfClaims == 5)
        {
            array_push($tableName,"5th_claim_amt");
            array_push($tableValue,$finalAmount);
            $stringType .=  "i";

            array_push($tableName,"request_date5");
            array_push($tableValue,$date);
            $stringType .=  "s";

            array_push($tableName,"sst5");
            array_push($tableValue,$charges);
            $stringType .=  "s";

            array_push($tableName,"status_5th");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        array_push($tableValue,$loanUid);
        $stringType .=  "s";
        $withdrawUpdated = updateDynamicData($conn,"loan_status"," WHERE loan_uid = ? ",$tableName,$tableValue,$stringType);
            if($withdrawUpdated)
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminWithdrawal.php?type=1');
            }
            else
            {
                echo "fail";
            }

            if(addNewInvoice($conn,$id,$invoiceId,$purchaserName,$projectName,$projectHandler,$statusOfClaims,$dateOfClaims,$invoiceName,$invoiceType,
                                    $item,$remark,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,
                                        $loanUid,$charges,$item2,$item3,$item4,$item5,$amount2,$amount3,$amount4,$amount5,$receiveStatus,
                                            $unit,$unit2,$unit3,$unit4,$unit5,$invoiceUnitNo,$invoiceDateCreated))
            {
                //$_SESSION['messageType'] = 1;
                header('Location: ../invoiceRecord.php');
                //echo "register success";
            }
    }
    else
    {
        echo "dunno";
    }
  }elseif ($invoiceName == 'Proforma') {
    if(addNewInvoice($conn,$id,$invoiceId,$purchaserName,$projectName,$projectHandler,$statusOfClaims,$dateOfClaims,$invoiceName,$invoiceType,
                            $item,$remark,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,
                                $loanUid,$charges,$item2,$item3,$item4,$item5,$amount2,$amount3,$amount4,$amount5,$receiveStatus,
                                    $unit,$unit2,$unit3,$unit4,$unit5,$invoiceUnitNo,$invoiceDateCreated))
    {
        //$_SESSION['messageType'] = 1;
        header('Location: ../invoiceRecord.php');
        //echo "register success";
    }
  }
    // if(addNewInvoice($conn,$id,$purchaserName,$item,$remark,$amount,$project,$finalAmount,$bankAccountHolder,
    //                         $bankName,$bankAccountNo,$loanUid,$charges,$item2,$item3,$item4,$item5,$amount2,$amount3,$amount4,$amount5))

    // if(addNewInvoice($conn,$id,$purchaserName,$projectName,$projectHandler,$statusOfClaims,$dateOfClaims,$invoiceName,$invoiceType,
    //                         $item,$remark,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,
    //                             $loanUid,$charges,$item2,$item3,$item4,$item5,$amount2,$amount3,$amount4,$amount5,$receiveStatus,
    //                                 $unit,$unit2,$unit3,$unit4,$unit5,$invoiceUnitNo,$invoiceDateCreated))
    //
    // {
    //     //$_SESSION['messageType'] = 1;
    //     header('Location: ../admin2Product.php');
    //     //echo "register success";
    // }

}
else
{
    //  header('Location: ../index.php');
}
?>
