<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

include 'css.php';

$fullName = $_SESSION['fullname'];
$project = $_POST['project'];
$conn = connDB();

$userRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($fullName),"s");
$userDetails = $userRows[0];

$projectDetails = getProject($conn);

if (isset($_POST['bookingStartDate'])) {
$dateStart = $_POST['bookingStartDate'];
}else {
$dateStart = "";
}
if (isset($_POST['bookingEndDate'])) {
$dateEnd = $_POST['bookingEndDate'];
}else {
$dateEnd = "";
}
// $bookingDate = date('Y-m-d',strtotime($_POST['bookingDate']));
if ($dateStart) {
  $dateStartN = str_replace("/","-",$dateStart);
  $bookingStartDate = date('Y-m-d',strtotime($dateStartN));
}else{
  $bookingStartDate = '1970-01-01';
}
if ($dateEnd) {
  $dateEndN = str_replace("/","-",$dateEnd);
  $bookingEndDate = date('Y-m-d',strtotime($dateEndN));
}else{
  $bookingEndDate = date('Y-m-d');
}
 ?>

<table id="myTable" class="shipping-table pointer-th">
    <thead>
        <tr>
            <th class="th">NO. <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("PROJECT NAME",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("PURCHASER NAME",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <!-- <th>STOCK</th> -->
            <th class="th">IC <img src="img/sort.png" class="sort"></th>
            <th class="th">CONTACT <img src="img/sort.png" class="sort"></th>
            <th class="th">E-MAIL <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("BOOKING DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th">SQ FT <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("SPA PRICE",8,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th">PACKAGE <img src="img/sort.png" class="sort"></th>
            <th class="th">DISCOUNT <img src="img/sort.png" class="sort"></th>
            <th class="th">REBATE <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("EXTRA REBATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("NETT PRICE",8,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("TOTAL DEVELOPER COMMISSION",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th">AGENT <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("LOAN STATUS",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>

            <th class="th">REMARK <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("FORM COLLECTED",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("PAYMENT METHOD",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th">LAWYER <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("PENDING APPROVAL STATUS",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("BANK APPROVED",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("LO SIGNED DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("LA SIGNED DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("SPA SIGNED DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("FULLSET COMPLETED",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th">CASH BUYER <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("CANCELLED BOOKING",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("CASE STATUS",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("EVENT PERSONAL",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th">RATE <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("AGENT COMMISSION",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
            <th class="th"><?php echo wordwrap("BOOKING FORM",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
        </tr>
    </thead>
    <tbody id="myFilter">
        <?php
        // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
        // {
            if ($project) {
              $orderDetails = getLoanStatus($conn, "WHERE display = 'Yes' and cancelled_booking != 'YES' AND agent = ? AND project_name =? AND booking_date >= ? AND booking_date <= ?",array("agent","project_name","booking_date","booking_date"),array($fullName,$project,$bookingStartDate,$bookingEndDate), "ssss");
            }else {
              $orderDetails = getLoanStatus($conn, "WHERE display = 'Yes' and cancelled_booking != 'YES' AND agent = ? AND booking_date >= ? AND booking_date <= ?",array("agent","booking_date","booking_date"),array($fullName,$bookingStartDate,$bookingEndDate), "sss");
            }
            if($orderDetails != null)
            {
                for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                {?>
                <tr>
                    <td class="td"><?php echo ($cntAA+1)?></td>
                    <!-- <td class="td"><?php echo $orderDetails[$cntAA]->getId();?></td> -->
                    <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                    <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getUnitNo());?></td>
                    <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getPurchaserName());?></td>
                    <!-- <td><?php //echo $orderDetails[$cntAA]->getStock();?></td> -->
                    <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getIc());?></td>
                    <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getContact());?></td>

                    <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getEmail());?></td>
                    <td class="td"><?php echo date('d-m-Y', strtotime($orderDetails[$cntAA]->getBookingDate()));?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getSqFt();?></td>

                    <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getSpaPrice();?></td> -->

                    <!-- show , inside value -->
                    <?php $spaPrice = $orderDetails[$cntAA]->getSpaPrice();?>
                    <td class="td"><?php echo $spaPriceValue = number_format($spaPrice, 2); ?></td>

                    <td class="td"><?php echo $orderDetails[$cntAA]->getPackage();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getDiscount();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getRebate();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getExtraRebate();?></td>

                    <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getNettPrice();?></td>
                    <td class="td"><?php //echo $orderDetails[$cntAA]->getTotalDeveloperComm();?></td> -->

                    <!-- show , inside value -->
                    <?php $nettPrice = $orderDetails[$cntAA]->getNettPrice();?>
                    <td class="td"><?php echo $nettPriceValue = number_format($nettPrice, 2); ?></td>
                    <?php $totalDevComm = $orderDetails[$cntAA]->getTotalDeveloperComm();?>
                    <td class="td"><?php echo $totalDevCommValue = number_format($totalDevComm, 2); ?></td>

                    <td class="td"><?php echo $orderDetails[$cntAA]->getAgent();?></td>
                    <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getRemark();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getBFormCollected();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getPaymentMethod();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getLawyer();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getPendingApprovalStatus();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getBankApproved();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getLoSignedDate();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getLaSignedDate();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getSpaSignedDate();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getFullsetCompleted();?></td>

                    <td class="td"><?php echo $orderDetails[$cntAA]->getCashBuyer();?></td>

                    <td class="td"><?php echo $orderDetails[$cntAA]->getCancelledBooking();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getCaseStatus();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getEventPersonal();?></td>
                    <td class="td"><?php echo $orderDetails[$cntAA]->getRate();?></td>

                    <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getAgentComm();?></td> -->

                    <?php $agentComm = explode(",",$orderDetails[$cntAA]->getAgentComm());?>
                    <td class="td"><?php echo $agentCommValue = number_format($agentComm[0], 2); ?></td>
                    <td class="td">
                      <?php if ($orderDetails[$cntAA]->getPdf()) {
                        ?>
                        <a href="<?php echo "uploads/".$orderDetails[$cntAA]->getPdf(); ?>">Download
                        </a>
                        <?php
                      } ?>
                    </td>
                </tr>
                <?php
                }
            }else {
              ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="35">No Personal Sales Found.</td> <?php
            }
        //}
        ?>
    </tbody>
</table>
<script>
  $(function(){
    $("#myTable").tablesorter( {dateFormat: 'pt'} );
  });
</script>
