<?php
class Commission {
    /* Member variables */
    // var $id, $loanUid, $upline, $commission, $purchaserName, $checkID, $receiveStatus, $dateCreated;
    var $id, $commissionId, $loanUid, $upline,$uplineDefault,$defCommission,$charges, $commission, $purchaserName, $checkID, $receiveStatus, $projectName, $unitNo, $bookingDate,$claimType, $claimId,$paymentMethod, $icNo, $branchType, $dateCreated, $details, $dateUpdated,$uplineType;

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setID($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCommissionId()
    {
        return $this->commission_id;
    }

    /**
     * @param mixed $id
     */
    public function setCommissionId($commissionId)
    {
        $this->commission_id = $commissionId;
    }

    /**
     * @return mixed
     */
    public function getIcNo()
    {
        return $this->ic_no;
    }

    /**
     * @param mixed $id
     */
    public function setIcNo($icNo)
    {
        $this->ic_no = $icNo;
    }

    /**
     * @return mixed
     */
    public function getClaimType()
    {
        return $this->claim_type;
    }

    /**
     * @param mixed $id
     */
    public function setClaimType($claimType)
    {
        $this->claim_type = $claimType;
    }
    /**
     * @return mixed
     */
    public function getClaimID()
    {
        return $this->claim_id;
    }

    /**
     * @param mixed $id
     */
    public function setClaimID($claimId)
    {
        $this->claim_id = $claimId;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    /**
     * @param mixed $id
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->payment_method = $paymentMethod;
    }

    /**
     * @return mixed
     */
    public function getCharges()
    {
        return $this->charges;
    }

    /**
     * @param mixed $id
     */
    public function setCharges($charges)
    {
        $this->charges = $charges;
    }


    /**
     * @return mixed
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @param mixed $id
     */
    public function setDetails($details)
    {
        $this->details = $details;
    }

    /**
     * @return mixed
     */
    public function getCheckID()
    {
        return $this->check_id;
    }

    /**
     * @param mixed $id
     */
    public function setCheckID($checkID)
    {
        $this->check_id = $checkID;
    }

    /**
     * @return mixed
     */
    public function getReceiveStatus()
    {
        return $this->receive_status;
    }

    /**
     * @param mixed $id
     */
    public function setReceiveStatus($receiveStatus)
    {
        $this->receive_status = $receiveStatus;
    }

    /**
     * @return mixed
     */
    public function getDefCommission()
    {
        return $this->def_commission;
    }

    /**
     * @param mixed $id
     */
    public function setDefCommission($defCommission)
    {
        $this->def_commission = $defCommission;
    }

    /**
     * @return mixed
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param mixed $id
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
    }

    /**
     * @return mixed
     */
    public function getUpline()
    {
        return $this->upline;
    }

    /**
     * @param mixed $id
     */
    public function setUpline($upline)
    {
        $this->upline = $upline;
    }

    /**
     * @return mixed
     */
    public function getUplineDefault()
    {
        return $this->upline_default;
    }

    /**
     * @param mixed $id
     */
    public function setUplineDefault($uplineDefault)
    {
        $this->upline_default = $uplineDefault;
    }

    /**
     * @return mixed
     */
    public function getPurchaserName()
    {
        return $this->purchaser_name;
    }

    /**
     * @param mixed $id
     */
    public function setPurchaserName($purchaserName)
    {
        $this->purchaser_name = $purchaserName;
    }

    /**
     * @return mixed
     */
    public function getProjectName()
    {
    return $this->projectName;
    }

    /**
     * @param mixed $projectName
     */
    public function setProjectName($projectName)
    {
    $this->projectName = $projectName;
    }

    /**
     * @return mixed
     */
    public function getUnitNo()
    {
        return $this->unitNo;
    }

    /**
     * @param mixed $unitNo
     */
    public function setUnitNo($unitNo)
    {
        $this->unitNo = $unitNo;
    }

    /**
     * @return mixed
     */
    public function getBookingDate()
    {
        return $this->bookingDate;
    }

    /**
     * @param mixed $bookingDate
     */
    public function setBookingDate($bookingDate)
    {
        $this->bookingDate = $bookingDate;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getLoanUid()
    {
        return $this->loan_uid;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setLoanUid($loanUid)
    {
        $this->loan_uid = $loanUid;
    }

    /**
     * @return mixed
     */
    public function getBranchType()
    {
        return $this->branch_type;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setBranchType($branchType)
    {
        $this->branch_type = $branchType;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getUplineType()
    {
        return $this->upline_type;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setUplineType($uplineType)
    {
        $this->upline_type = $uplineType;
    }

}

function getCommission($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    // $dbColumnNames = array("id","loan_uid","upline","commission","purchaser_name", "check_id", "receive_status", "date_created");
    $dbColumnNames = array("id","commission_id" ,"loan_uid","upline","upline_default","charges","def_commission","commission","purchaser_name", "check_id", "receive_status", "project_name", "unit_no", "booking_date","claim_type","claim_id","payment_method","ic_no", "date_created","details","branch_type","date_updated","upline_type");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"commission");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id, $loanUid, $upline, $commission, $purchaserName, $checkID, $receiveStatus, $dateCreated);
        $stmt->bind_result($id, $commissionId, $loanUid, $upline,$uplineDefault,$charges,$defCommission, $commission, $purchaserName, $checkID, $receiveStatus, $projectName, $unitNo, $bookingDate,$claimType, $claimId, $paymentMethod,$icNo, $dateCreated, $details, $branchType, $dateUpdated,$uplineType);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Commission();
            $class->setID($id);
            $class->setCommissionId($commissionId);
            $class->setLoanUid($loanUid);
            $class->setUpline($upline);
            $class->setUplineDefault($uplineDefault);
            $class->setDefCommission($defCommission);
            $class->setCommission($commission);
            $class->setPurchaserName($purchaserName);
            $class->setCheckID($checkID);
            $class->setReceiveStatus($receiveStatus);
            $class->setCharges($charges);
            $class->setProjectName($projectName);
            $class->setUnitNo($unitNo);
            $class->setBookingDate($bookingDate);
            $class->setClaimType($claimType);
            $class->setClaimID($claimId);
            $class->setPaymentMethod($paymentMethod);
            $class->setIcNo($icNo);
            $class->setDetails($details);
            $class->setBranchType($branchType);
            $class->setUplineType($uplineType);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
