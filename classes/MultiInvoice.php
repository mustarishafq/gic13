<?php
class MultiInvoice {
    /* Member variables */
    var $id,$invoiceId,$loanId,$item,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,$dateCreated,$dateUpdated,$charges,
                                $receiveStatus,$projectName,$projectHandler,$projectHandlerDefault,$invoiceName,$invoiceType,  $unitNo,
                                $bookingDate,$receiveDate,$checkID,$claimsStatus,$branchType,$creditStatus;

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setID($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getInvoiceId()
    {
        return $this->invoice_id;
    }

    /**
     * @param mixed $id
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoice_id = $invoiceId;
    }

    /**
     * @return mixed
     */
    public function getLoanId()
    {
        return $this->loan_id;
    }

    /**
     * @param mixed $id
     */
    public function setLoanId($loanId)
    {
        $this->loan_id = $loanId;
    }

    /**
     * @return mixed
     */
    public function getClaimsStatus()
    {
        return $this->claimsStatus;
    }

    /**
     * @param mixed $claimsStatus
     */
    public function setClaimsStatus($claimsStatus)
    {
        $this->claimsStatus = $claimsStatus;
    }

    /**
     * @return mixed
     */
    public function getReceiveStatus()
    {
        return $this->receive_status;
    }

    /**
     * @param mixed $id
     */
    public function setReceiveStatus($receiveStatus)
    {
        $this->receive_status = $receiveStatus;
    }

    /**
     * @return mixed
     */
    public function getReceiveDate()
    {
        return $this->receive_date;
    }

    /**
     * @param mixed $id
     */
    public function setReceiveDate($receiveDate)
    {
        $this->receive_date = $receiveDate;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $id
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $id
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

      /**
     * @return mixed
     */
    public function getFinalAmount()
    {
        return $this->final_amount;
    }

    /**
     * @param mixed $id
     */
    public function setFinalAmount($finalAmount)
    {
        $this->final_amount = $finalAmount;
    }

    /**
     * @return mixed
     */
    public function getBankAccountHolder()
    {
        return $this->bank_account_holder;
    }

    /**
     * @param mixed $id
     */
    public function setBankAccountHolder($bankAccountHolder)
    {
        $this->bank_account_holder = $bankAccountHolder;
    }

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bank_name;
    }

    /**
     * @param mixed $id
     */
    public function setBankName($bankName)
    {
        $this->bank_name = $bankName;
    }

    /**
     * @return mixed
     */
    public function getBankAccountNo()
    {
        return $this->bank_account_no;
    }

    /**
     * @param mixed $id
     */
    public function setBankAccountNo($bankAccountNo)
    {
        $this->bank_account_no = $bankAccountNo;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getCharges()
    {
        return $this->charges;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setCharges($charges)
    {
        $this->charges = $charges;
    }

    /**
     * @return mixed
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * @param mixed $projectName
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }

    /**
     * @return mixed
     */
    public function getProjectHandler()
    {
        return $this->projectHandler;
    }

    /**
     * @param mixed $projectHandler
     */
    public function setProjectHandler($projectHandler)
    {
        $this->projectHandler = $projectHandler;
    }

    /**
     * @return mixed
     */
    public function getProjectHandlerDefault()
    {
        return $this->projectHandlerDefault;
    }

    /**
     * @param mixed $projectHandler
     */
    public function setProjectHandlerDefault($projectHandlerDefault)
    {
        $this->projectHandlerDefault = $projectHandlerDefault;
    }

    /**
     * @return mixed
     */
    public function getCheckID()
    {
        return $this->check_id;
    }

    /**
     * @param mixed $id
     */
    public function setCheckID($checkID)
    {
        $this->check_id = $checkID;
    }

      /**
     * @return mixed
     */
    public function getInvoiceName()
    {
        return $this->invoiceName;
    }

    /**
     * @param mixed $invoiceName
     */
    public function setInvoiceName($invoiceName)
    {
        $this->invoiceName = $invoiceName;
    }

    /**
     * @return mixed
     */
    public function getInvoiceType()
    {
        return $this->invoiceType;
    }

    /**
     * @param mixed $invoiceType
     */
    public function setInvoiceType($invoiceType)
    {
        $this->invoiceType = $invoiceType;
    }

      /**
     * @return mixed
     */
    public function getUnitNo()
    {
        return $this->unitNo;
    }

    /**
     * @param mixed $unitNo
     */
    public function setUnitNo($unitNo)
    {
        $this->unitNo = $unitNo;
    }

    /**
     * @return mixed
     */
    public function getBookingDate()
    {
        return $this->bookingDate;
    }

    /**
     * @param mixed $bookingDate
     */
    public function setBookingDate($bookingDate)
    {
        $this->bookingDate = $bookingDate;
    }

    /**
     * @return mixed
     */
    public function getBranchType()
    {
        return $this->branch_type;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setBranchType($branchType)
    {
        $this->branch_type = $branchType;
    }

    /**
     * @return mixed
     */
    public function getCreditStatus()
    {
        return $this->credit_status;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setCreditStatus($creditStatus)
    {
        $this->credit_status = $creditStatus;
    }

}

function getMultiInvoice($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){

    $dbColumnNames = array("id","invoice_id","loan_id","item","amount","final_amount","bank_account_holder","bank_name","bank_account_no",
                                "date_created","date_updated","charges",
                                   "receive_status", "project_name","project_handler","project_handler_default","invoice_name","invoice_type",
                                        "unit_no","booking_date","receive_date","check_id","claims_status","branch_type","credit_status");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"invoice_multi");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$invoiceId,$loanId,$item,$amount,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,$dateCreated,$dateUpdated,$charges,
                                    $receiveStatus,$projectName,$projectHandler,$projectHandlerDefault,$invoiceName,$invoiceType,  $unitNo, $bookingDate,
                                    $receiveDate,$checkID,$claimsStatus,$branchType,$creditStatus);


        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new MultiInvoice();
            $class->setID($id);
            $class->setInvoiceId($invoiceId);
            $class->setLoanId($loanId);
            $class->setAmount($amount);
            $class->setitem($item);
            $class->setBankAccountHolder($bankAccountHolder);
            $class->setBankName($bankName);
            $class->setBankAccountNo($bankAccountNo);
            $class->setFinalAmount($finalAmount);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
            $class->setCharges($charges);
            $class->setReceiveStatus($receiveStatus);
            $class->setReceiveDate($receiveDate);
            $class->setProjectName($projectName);
            $class->setProjectHandler($projectHandler);
            $class->setProjectHandlerDefault($projectHandlerDefault);
            $class->setInvoiceName($invoiceName);
            $class->setInvoiceType($invoiceType);
            $class->setUnitNo($unitNo);
            $class->setBookingDate($bookingDate);
            $class->setCheckID($checkID);
            $class->setClaimsStatus($claimsStatus);
            $class->setBranchType($branchType);
            $class->setCreditStatus($creditStatus);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
