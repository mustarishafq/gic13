<?php
class Announcement {
    /* Member variables */
    var $id, $projectName, $plName, $commission, $package,$presentFile, $eBroshure,$bookingForm,$videoLink,$createdBy,$branchType,$dateCreated, $dateUpdated;

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param mixed $projectName
     */
    public function setID($id)
    {
    $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProjectName()
    {
    return $this->projectName;
    }

    /**
     * @param mixed $projectName
     */
    public function setProjectName($projectName)
    {
    $this->projectName = $projectName;
    }

    /**
     * @return mixed
     */
    public function getPlName()
    {
        return $this->plName;
    }

    /**
     * @param mixed $plName
     */
    public function setPlName($plName)
    {
        $this->plName = $plName;
    }

    /**
     * @return mixed
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param mixed $plName
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
    }

    /**
     * @return mixed
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param mixed $package
     */
    public function setPackage($package)
    {
        $this->package = $package;
    }

    /**
     * @return mixed
     */
    public function getPresentFile()
    {
        return $this->presentFile;
    }

    /**
     * @param mixed $package
     */
    public function setPresentFile($presentFile)
    {
        $this->presentFile = $presentFile;
    }

    /**
     * @return mixed
     */
    public function getEBroshure()
    {
        return $this->eBroshure;
    }

    /**
     * @param mixed $package
     */
    public function setEBroshure($eBroshure)
    {
        $this->eBroshure = $eBroshure;
    }

    /**
     * @return mixed
     */
    public function getBookingForm()
    {
        return $this->bookingForm;
    }

    /**
     * @param mixed $package
     */
    public function setBookingForm($bookingForm)
    {
        $this->bookingForm = $bookingForm;
    }

    /**
     * @return mixed
     */
    public function getVideoLink()
    {
        return $this->videoLink;
    }

    /**
     * @param mixed $package
     */
    public function setVideoLink($videoLink)
    {
        $this->videoLink = $videoLink;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $projectName
     */
    public function setCreatedBy($createdBy)
    {
    $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @return mixed
     */
    public function getBranchType()
    {
        return $this->branch_type;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setBranchType($branchType)
    {
        $this->branch_type = $branchType;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }



}

function getAnnouncement($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","project_name","pl_name","commission","packages","present_file","broshure","booking_form","video_link","createdBy", "branch_type","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"announcement");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $projectName, $plName, $commission, $package,$presentFile, $eBroshure,$bookingForm,$videoLink,$createdBy, $branchType,$dateCreated, $dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Announcement();
            $class->setID($id);
            $class->setProjectName($projectName);
            $class->setPlName($plName);
            $class->setCommission($commission);
            $class->setPackage($package);
            $class->setPresentFile($presentFile);
            $class->setEBroshure($eBroshure);
            $class->setBookingForm($bookingForm);
            $class->setVideoLink($videoLink);
            $class->setCreatedBy($createdBy);
            $class->setBranchType($branchType);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);


            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
