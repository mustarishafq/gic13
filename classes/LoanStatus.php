<?php
// class Product{
class LoanStatus{
    var $id, $projectName, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package,
                            $discount, $rebate, $extraRebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus,
                                $remark, $bFormCollected, $paymentMethod, $lawyer, $pendingApprovalStatus, $bankApproved, $loSignedDate, $laSignedDate,
                                    $spaSignedDate, $fullsetCompleted, $cashBuyer, $cancelledBooking, $caseStatus, $eventPersonal, $rate, $agentComm,
                                        $upline1, $upline2,$upline3,$upline4,$upline5, $plName, $hosName, $listerName, $ulOverride, $uulOverride,$uuulOverride,$uuuulOverride,$uuuuulOverride,
                                            $plOverride, $hosOverride, $listerOverride, $admin1Override, $admin2Override, $admin3Override, $gicProfit,
                                                $totalClaimedDevAmt, $totalBalUnclaimAmt, $status1st, $claimAmt1st, $status2nd, $claimAmt2nd,
                                                    $status3rd, $claimAmt3rd, $status4th, $claimAmt4th, $status5th, $claimAmt5th,
                                                        $dateCreated, $dateUpdated,$loanUid,$pdf,$pdfDate,
                                                            $sst1, $sst2, $sst3, $sst4, $sst5, $requestDate1, $requestDate2, $requestDate3, $requestDate4,
                                                                $requestDate5, $checkNo1, $checkNo2, $checkNo3, $checkNo4,
                                                                    $checkNo5, $receiveDate1, $receiveDate2, $receiveDate3,  $receiveDate4, $receiveDate5,$loanAmount,$display,$branchType,
                                                                      $ulBalance, $uulBalance, $uuulBalance, $uuuulBalance, $uuuuulBalance, $listerBalance, $hosBalance,$PLBalance,$agentBalance;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $id
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return mixed
     */
    public function getLoanAmount()
    {
        return $this->loan_amount;
    }

    /**
     * @param mixed $id
     */
    public function setLoanAmount($loanAmount)
    {
        $this->loan_amount = $loanAmount;
    }

    //additional class for loanUid and projectName
    /**
     * @return mixed
     */
    public function getLoanUid()
    {
        return $this->loan_uid;
    }

    /**
     * @param mixed $id
     */
    public function setLoanUid($loanUid)
    {
        $this->loan_uid = $loanUid;
    }

    /**
     * @return mixed
     */
    public function getProjectName()
    {
    return $this->projectName;
    }

    /**
     * @param mixed $projectName
     */
    public function setProjectName($projectName)
    {
    $this->projectName = $projectName;
    }

    /**
     * @return mixed
     */
    public function getUnitNo()
    {
        return $this->unitNo;
    }

    /**
     * @param mixed $unitNo
     */
    public function setUnitNo($unitNo)
    {
        $this->unitNo = $unitNo;
    }

    /**
     * @return mixed
     */
    public function getPurchaserName()
    {
        return $this->purchaserName;
    }

    /**
     * @param mixed $purchaserName
     */
    public function setPurchaserName($purchaserName)
    {
        $this->purchaserName = $purchaserName;
    }

    /**
     * @return mixed
     */
    public function getIc()
    {
        return $this->ic;
    }

    /**
     * @param mixed $ic
     */
    public function setIc($ic)
    {
        $this->ic = $ic;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getBookingDate()
    {
        return $this->bookingDate;
    }

    /**
     * @param mixed $bookingDate
     */
    public function setBookingDate($bookingDate)
    {
        $this->bookingDate = $bookingDate;
    }

    /**
     * @return mixed
     */
    public function getSqFt()
    {
        return $this->sqFt;
    }

    /**
     * @param mixed $sqFt
     */
    public function setSqFt($sqFt)
    {
        $this->sqFt = $sqFt;
    }

    /**
     * @return mixed
     */
    public function getSpaPrice()
    {
        return $this->spaPrice;
    }

    /**
     * @param mixed $spaPrice
     */
    public function setSpaPrice($spaPrice)
    {
        $this->spaPrice = $spaPrice;
    }

    /**
     * @return mixed
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param mixed $package
     */
    public function setPackage($package)
    {
        $this->package = $package;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getRebate()
    {
        return $this->rebate;
    }

    /**
     * @param mixed $rebate
     */
    public function setRebate($rebate)
    {
        $this->rebate = $rebate;
    }

        /**
     * @return mixed
     */
    public function getExtraRebate()
    {
        return $this->extraRebate;
    }

    /**
     * @param mixed $extraRebate
     */
    public function setExtraRebate($extraRebate)
    {
        $this->extraRebate = $extraRebate;
    }

    /**
     * @return mixed
     */
    public function getNettPrice()
    {
        return $this->nettPrice;
    }

    /**
     * @param mixed $nettPrice
     */
    public function setNettPrice($nettPrice)
    {
        $this->nettPrice = $nettPrice;
    }

    /**
     * @return mixed
     */
    public function getTotalDeveloperComm()
    {
        return $this->totalDeveloperComm;
    }

    /**
     * @param mixed $totalDeveloperComm
     */
    public function setTotalDeveloperComm($totalDeveloperComm)
    {
        $this->totalDeveloperComm = $totalDeveloperComm;
    }

    /**
     * @return mixed
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param mixed $agent
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;
    }

    /**
     * @return mixed
     */
    public function getLoanStatus()
    {
        return $this->loanStatus;
    }

    /**
     * @param mixed $loanStatus
     */
    public function setLoanStatus($loanStatus)
    {
        $this->loanStatus = $loanStatus;
    }

    /**
     * @return mixed
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param mixed $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return mixed
     */
    public function getBFormCollected()
    {
        return $this->bFormCollected;
    }

    /**
     * @param mixed $bFormCollected
     */
    public function setBFormCollected($bFormCollected)
    {
        $this->bFormCollected = $bFormCollected;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param mixed $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return mixed
     */
    public function getLawyer()
    {
        return $this->lawyer;
    }

    /**
     * @param mixed $lawyer
     */
    public function setLawyer($lawyer)
    {
        $this->lawyer = $lawyer;
    }

    /**
     * @return mixed
     */
    public function getPendingApprovalStatus()
    {
        return $this->pendingApprovalStatus;
    }

    /**
     * @param mixed $pendingApprovalStatus
     */
    public function setPendingApprovalStatus($pendingApprovalStatus)
    {
        $this->pendingApprovalStatus = $pendingApprovalStatus;
    }

    /**
     * @return mixed
     */
    public function getBankApproved()
    {
        return $this->bankApproved;
    }

    /**
     * @param mixed $bankApproved
     */
    public function setBankApproved($bankApproved)
    {
        $this->bankApproved = $bankApproved;
    }

    /**
     * @return mixed
     */
    public function getLoSignedDate()
    {
        return $this->loSignedDate;
    }

    /**
     * @param mixed $loSignedDate
     */
    public function setLoSignedDate($loSignedDate)
    {
        $this->loSignedDate = $loSignedDate;
    }

    /**
     * @return mixed
     */
    public function getLaSignedDate()
    {
        return $this->laSignedDate;
    }

    /**
     * @param mixed $laSignedDate
     */
    public function setLaSignedDate($laSignedDate)
    {
        $this->laSignedDate = $laSignedDate;
    }

    /**
     * @return mixed
     */
    public function getSpaSignedDate()
    {
        return $this->spaSignedDate;
    }

    /**
     * @param mixed $spaSignedDate
     */
    public function setSpaSignedDate($spaSignedDate)
    {
        $this->spaSignedDate = $spaSignedDate;
    }

    /**
     * @return mixed
     */
    public function getFullsetCompleted()
    {
        return $this->fullsetCompleted;
    }

    /**
     * @param mixed $fullsetCompleted
     */
    public function setFullsetCompleted($fullsetCompleted)
    {
        $this->fullsetCompleted = $fullsetCompleted;
    }

    /**
     * @return mixed
     */
    public function getCashBuyer()
    {
        return $this->cashBuyer;
    }

    /**
     * @param mixed $cashBuyer
     */
    public function setCashBuyer($cashBuyer)
    {
        $this->cashBuyer = $cashBuyer;
    }

    /**
     * @return mixed
     */
    public function getCancelledBooking()
    {
        return $this->cancelledBooking;
    }

    /**
     * @param mixed $cancelledBooking
     */
    public function setCancelledBooking($cancelledBooking)
    {
        $this->cancelledBooking = $cancelledBooking;
    }

    /**
     * @return mixed
     */
    public function getCaseStatus()
    {
        return $this->caseStatus;
    }

    /**
     * @param mixed $caseStatus
     */
    public function setCaseStatus($caseStatus)
    {
        $this->caseStatus = $caseStatus;
    }

    /**
     * @return mixed
     */
    public function getEventPersonal()
    {
        return $this->eventPersonal;
    }

    /**
     * @param mixed $eventPersonal
     */
    public function setEventPersonal($eventPersonal)
    {
        $this->eventPersonal = $eventPersonal;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param mixed $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return mixed
     */
    public function getAgentComm()
    {
        return $this->agentComm;
    }

    /**
     * @param mixed $agentComm
     */
    public function setAgentComm($agentComm)
    {
        $this->agentComm = $agentComm;
    }

    /**
     * @return mixed
     */
    public function getUpline1()
    {
        return $this->upline1;
    }

    /**
     * @param mixed $upline1
     */
    public function setUpline1($upline1)
    {
        $this->upline1 = $upline1;
    }

    /**
     * @return mixed
     */
    public function getUpline2()
    {
        return $this->upline2;
    }

    /**
     * @param mixed $upline2
     */
    public function setUpline2($upline2)
    {
        $this->upline2 = $upline2;
    }

    /**
     * @return mixed
     */
    public function getUpline3()
    {
        return $this->upline3;
    }

    /**
     * @param mixed $upline1
     */
    public function setUpline3($upline3)
    {
        $this->upline3 = $upline3;
    }

    /**
     * @return mixed
     */
    public function getUpline4()
    {
        return $this->upline4;
    }

    /**
     * @param mixed $upline1
     */
    public function setUpline4($upline4)
    {
        $this->upline4 = $upline4;
    }

    /**
     * @return mixed
     */
    public function getUpline5()
    {
        return $this->upline5;
    }

    /**
     * @param mixed $upline1
     */
    public function setUpline5($upline5)
    {
        $this->upline5 = $upline5;
    }

    /**
     * @return mixed
     */
    public function getPlName()
    {
        return $this->plName;
    }

    /**
     * @param mixed $plName
     */
    public function setPlName($plName)
    {
        $this->plName = $plName;
    }

    /**
     * @return mixed
     */
    public function getHosName()
    {
        return $this->hosName;
    }

    /**
     * @param mixed $hosName
     */
    public function setHosName($hosName)
    {
        $this->hosName = $hosName;
    }

    /**
     * @return mixed
     */
    public function getListerName()
    {
        return $this->listerName;
    }

    /**
     * @param mixed $listerName
     */
    public function setListerName($listerName)
    {
        $this->listerName = $listerName;
    }

    /**
     * @return mixed
     */
    public function getUlOverride()
    {
        return $this->ulOverride;
    }

    /**
     * @param mixed $ulOverride
     */
    public function setUlOverride($ulOverride)
    {
        $this->ulOverride = $ulOverride;
    }

    /**
     * @return mixed
     */
    public function getUulOverride()
    {
        return $this->uulOverride;
    }

    /**
     * @param mixed $uulOverride
     */
    public function setUulOverride($uulOverride)
    {
        $this->uulOverride = $uulOverride;
    }

    /**
     * @return mixed
     */
    public function getUuulOverride()
    {
        return $this->uuulOverride;
    }

    /**
     * @param mixed $uulOverride
     */
    public function setUuulOverride($uuulOverride)
    {
        $this->uuulOverride = $uuulOverride;
    }

    /**
     * @return mixed
     */
    public function getUuuulOverride()
    {
        return $this->uuuulOverride;
    }

    /**
     * @param mixed $uulOverride
     */
    public function setUuuulOverride($uuuulOverride)
    {
        $this->uuuulOverride = $uuuulOverride;
    }

    /**
     * @return mixed
     */
    public function getUuuuulOverride()
    {
        return $this->uuuuulOverride;
    }

    /**
     * @param mixed $uulOverride
     */
    public function setUuuuulOverride($uuuuulOverride)
    {
        $this->uuuuulOverride = $uuuuulOverride;
    }

    /**
     * @return mixed
     */
    public function getPlOverride()
    {
        return $this->plOverride;
    }

    /**
     * @param mixed $plOverride
     */
    public function setPlOverride($plOverride)
    {
        $this->plOverride = $plOverride;
    }

    /**
     * @return mixed
     */
    public function getHosOverride()
    {
        return $this->hosOverride;
    }

    /**
     * @param mixed $hosOverride
     */
    public function setHosOverride($hosOverride)
    {
        $this->hosOverride = $hosOverride;
    }

    /**
     * @return mixed
     */
    public function getListerOverride()
    {
        return $this->listerOverride;
    }

    /**
     * @param mixed $listerOverride
     */
    public function setListerOverride($listerOverride)
    {
        $this->listerOverride = $listerOverride;
    }

    /**
     * @return mixed
     */
    public function getAdmin1Override()
    {
        return $this->admin1Override;
    }

    /**
     * @param mixed $admin1Override
     */
    public function setAdmin1Override($admin1Override)
    {
        $this->admin1Override = $admin1Override;
    }

    /**
     * @return mixed
     */
    public function getAdmin2Override()
    {
        return $this->admin2Override;
    }

    /**
     * @param mixed $admin2Override
     */
    public function setAdmin2Override($admin2Override)
    {
        $this->admin2Override = $admin2Override;
    }

    /**
     * @return mixed
     */
    public function getAdmin3Override()
    {
        return $this->admin3Override;
    }

    /**
     * @param mixed $admin3Override
     */
    public function setAdmin3Override($admin3Override)
    {
        $this->admin3Override = $admin3Override;
    }

    /**
     * @return mixed
     */
    public function getGicProfit()
    {
        return $this->gicProfit;
    }

    /**
     * @param mixed $gicProfit
     */
    public function setGicProfit($gicProfit)
    {
        $this->gicProfit = $gicProfit;
    }

    /**
     * @return mixed
     */
    public function getTotalClaimDevAmt()
    {
        return $this->totalClaimedDevAmt;
    }

    /**
     * @param mixed $totalClaimedDevAmt
     */
    public function setTotalClaimDevAmt($totalClaimedDevAmt)
    {
        $this->totalClaimedDevAmt = $totalClaimedDevAmt;
    }

    /**
     * @return mixed
     */
    public function getTotalBalUnclaimAmt()
    {
        return $this->totalBalUnclaimAmt;
    }

    /**
     * @param mixed $totalBalUnclaimAmt
     */
    public function setTotalBalUnclaimAmt($totalBalUnclaimAmt)
    {
        $this->totalBalUnclaimAmt = $totalBalUnclaimAmt;
    }

    /**
     * @return mixed
     */
    public function getStatus1st()
    {
        return $this->status_1st;
    }

    /**
     * @param mixed $claimInvoiceNo1st
     */
    public function setStatus1st($status1st)
    {
        $this->status_1st = $status1st;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt1st()
    {
        return $this->claimAmt1st;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setClaimAmt1st($claimAmt1st)
    {
        $this->claimAmt1st = $claimAmt1st;
    }

    /**
     * @return mixed
     */
    public function getSst1()
    {
        return $this->sst1;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setSst1($sst1)
    {
        $this->sst1 = $sst1;
    }

    /**
     * @return mixed
     */
    public function getRequestDate1()
    {
        return $this->request_date1;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setRequestDate1($requestDate1)
    {
        $this->request_date1 = $requestDate1;
    }

    /**
     * @return mixed
     */
    public function getCheckNo1()
    {
        return $this->check_no1;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setCheckNo1($checkNo1)
    {
        $this->check_no1 = $checkNo1;
    }

    /**
     * @return mixed
     */
    public function getReceiveDate1()
    {
        return $this->receive_date1;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setReceiveDate1($receiveDate1)
    {
        $this->receive_date1 = $receiveDate1;
    }





        /**
     * @return mixed
     */
    public function getStatus2nd()
    {
        return $this->status_2nd;
    }

    /**
     * @param mixed $claimInvoiceNo2nd
     */
    public function setStatus2nd($status2nd)
    {
        $this->status_2nd = $status2nd;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt2nd()
    {
        return $this->claimAmt2nd;
    }

    /**
     * @param mixed $claimAmt2nd
     */
    public function setClaimAmt2nd($claimAmt2nd)
    {
        $this->claimAmt2nd = $claimAmt2nd;
    }

    /**
     * @return mixed
     */
    public function getSst2()
    {
        return $this->sst2;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setSst2($sst2)
    {
        $this->sst2 = $sst2;
    }

    /**
     * @return mixed
     */
    public function getRequestDate2()
    {
        return $this->request_date2;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setRequestDate2($requestDate2)
    {
        $this->request_date2 = $requestDate2;
    }

    /**
     * @return mixed
     */
    public function getCheckNo2()
    {
        return $this->check_no2;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setCheckNo2($checkNo2)
    {
        $this->check_no2 = $checkNo2;
    }

    /**
     * @return mixed
     */
    public function getReceiveDate2()
    {
        return $this->receive_date2;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setReceiveDate2($receiveDate2)
    {
        $this->receive_date2 = $receiveDate2;
    }

        /**
     * @return mixed
     */
    public function getStatus3rd()
    {
        return $this->status_3rd;
    }

    /**
     * @param mixed $claimInvoiceNo3rd
     */
    public function setStatus3rd($status3rd)
    {
        $this->status_3rd = $status3rd;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt3rd()
    {
        return $this->claimAmt3rd;
    }

    /**
     * @param mixed $claimAmt3rd
     */
    public function setClaimAmt3rd($claimAmt3rd)
    {
        $this->claimAmt3rd = $claimAmt3rd;
    }

    /**
     * @return mixed
     */
    public function getSst3()
    {
        return $this->sst3;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setSst3($sst3)
    {
        $this->sst3 = $sst3;
    }

    /**
     * @return mixed
     */
    public function getRequestDate3()
    {
        return $this->request_date3;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setRequestDate3($requestDate3)
    {
        $this->request_date3 = $requestDate3;
    }

    /**
     * @return mixed
     */
    public function getCheckNo3()
    {
        return $this->check_no3;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setCheckNo3($checkNo3)
    {
        $this->check_no3 = $checkNo3;
    }

    /**
     * @return mixed
     */
    public function getReceiveDate3()
    {
        return $this->receive_date3;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setReceiveDate3($receiveDate3)
    {
        $this->receive_date3 = $receiveDate3;
    }

    /**
     * @return mixed
     */
    public function getStatus4th()
    {
        return $this->status_4th;
    }

    /**
     * @param mixed $claimInvoiceNo4th
     */
    public function setStatus4th($status4th)
    {
        $this->status_4th = $status4th;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt4th()
    {
        return $this->claimAmt4th;
    }

    /**
     * @param mixed $claimAmt4th
     */
    public function setClaimAmt4th($claimAmt4th)
    {
        $this->claimAmt4th = $claimAmt4th;
    }

    /**
     * @return mixed
     */
    public function getSst4()
    {
        return $this->sst4;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setSst4($sst4)
    {
        $this->sst4 = $sst4;
    }

    /**
     * @return mixed
     */
    public function getRequestDate4()
    {
        return $this->request_date4;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setRequestDate4($requestDate4)
    {
        $this->request_date4 = $requestDate4;
    }

    /**
     * @return mixed
     */
    public function getCheckNo4()
    {
        return $this->check_no4;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setCheckNo4($checkNo4)
    {
        $this->check_no4 = $checkNo4;
    }

    /**
     * @return mixed
     */
    public function getReceiveDate4()
    {
        return $this->receive_date4;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setReceiveDate4($receiveDate4)
    {
        $this->receive_date4 = $receiveDate4;
    }

    /**
     * @return mixed
     */
    public function getStatus5th()
    {
        return $this->status_5th;
    }

    /**
     * @param mixed $claimInvoiceNo5th
     */
    public function setStatus5th($status5th)
    {
        $this->status_5th = $status5th;
    }

    /**
     * @return mixed
     */
    public function getClaimAmt5th()
    {
        return $this->claimAmt5th;
    }

    /**
     * @param mixed $claimAmt5th
     */
    public function setClaimAmt5th($claimAmt5th)
    {
        $this->claimAmt5th = $claimAmt5th;
    }

    /**
     * @return mixed
     */
    public function getSst5()
    {
        return $this->sst5;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setSst5($sst5)
    {
        $this->sst5 = $sst5;
    }

    /**
     * @return mixed
     */
    public function getRequestDate5()
    {
        return $this->request_date5;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setRequestDate5($requestDate5)
    {
        $this->request_date5 = $requestDate5;
    }

    /**
     * @return mixed
     */
    public function getCheckNo5()
    {
        return $this->check_no5;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setCheckNo5($checkNo5)
    {
        $this->check_no5 = $checkNo5;
    }

    /**
     * @return mixed
     */
    public function getReceiveDate5()
    {
        return $this->receive_date5;
    }

    /**
     * @param mixed $claimAmt1st
     */
    public function setReceiveDate5($receiveDate5)
    {
        $this->receive_date5 = $receiveDate5;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * @return mixed
     */
    public function getPdfDate()
    {
        return $this->pdf_date;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setPdfDate($pdfDate)
    {
        $this->pdf_date = $pdfDate;
    }

    /**
     * @return mixed
     */
    public function getBranchType()
    {
        return $this->branch_type;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setBranchType($branchType)
    {
        $this->branch_type = $branchType;
    }

    /**
     * @return mixed
     */
    public function getHosBalance()
    {
        return $this->hos_balance;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setHosBalance($hosBalance)
    {
        $this->hos_balance = $hosBalance;
    }

    /**
     * @return mixed
     */
    public function getListerBalance()
    {
        return $this->lister_balance;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setListerBalance($listerBalance)
    {
        $this->lister_balance = $listerBalance;
    }

    /**
     * @return mixed
     */
    public function getUlBalance()
    {
        return $this->ul_balance;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setUlBalance($ulBalance)
    {
        $this->ul_balance = $ulBalance;
    }

    /**
     * @return mixed
     */
    public function getUulBalance()
    {
        return $this->uul_balance;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setUulBalance($uulBalance)
    {
        $this->uul_balance = $uulBalance;
    }

    /**
     * @return mixed
     */
    public function getUuulBalance()
    {
        return $this->uuul_balance;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setUuulBalance($uuulBalance)
    {
        $this->uuul_balance = $uuulBalance;
    }

    /**
     * @return mixed
     */
    public function getUuuulBalance()
    {
        return $this->uuuul_balance;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setUuuulBalance($uuuulBalance)
    {
        $this->uuuul_balance = $uuuulBalance;
    }

    /**
     * @return mixed
     */
    public function getUuuuulBalance()
    {
        return $this->uuuuul_balance;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setUuuuulBalance($uuuuulBalance)
    {
        $this->uuuuul_balance = $uuuuulBalance;
    }

    /**
     * @return mixed
     */
    public function getPLBalance()
    {
        return $this->pl_balance;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setPLBalance($PLBalance)
    {
        $this->pl_balance = $PLBalance;
    }

    /**
     * @return mixed
     */
    public function getAgentBalance()
    {
        return $this->agent_balance;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setAgentBalance($agentBalance)
    {
        $this->agent_balance = $agentBalance;
    }
}

function getLoanStatus($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){

    $dbColumnNames = array("id","project_name","unit_no","purchaser_name","ic","contact","email","booking_date","sq_ft","spa_price","package",
                        "discount","rebate","extra_rebate","nettprice","totaldevelopercomm","agent","loanstatus",
                        "remark","bform_Collected","payment_method","lawyer","pending_approval_status","bank_approved","lo_signed_date","la_signed_date",
                        "spa_signed_date","fullset_completed","cash_buyer","cancelled_booking","case_status","event_personal","rate","agent_comm",
                        "upline1","upline2","upline3","upline4","upline5","pl_name","hos_name","lister_name","ul_override","uul_override","uuul_override","uuuul_override","uuuuul_override",
                        "pl_override","hos_override","lister_override","admin1_override","admin2_override","admin3_override","gic_profit",
                        "total_claimed_dev_amt","total_bal_unclaim_amt","status_1st","1st_claim_amt","status_2nd","2nd_claim_amt",
                        "status_3rd","3rd_claim_amt","status_4th","4th_claim_amt","status_5th","5th_claim_amt",
                        "date_created","date_updated","loan_uid","pdf","pdf_date",
                        "sst1","sst2","sst3","sst4","sst5","request_date1","request_date2", "request_date3","request_date4","request_date5","check_no1",
                        "check_no2","check_no3","check_no4","check_no5","receive_date1", "receive_date2","receive_date3","receive_date4","receive_date5","loan_amount","display","branch_type",
                        "ul_balance","uul_balance","uuul_balance","uuuul_balance","uuuuul_balance","lister_balance","hos_balance","pl_balance","agent_balance");


    // $sql = sqlSelectSimpleBuilder($dbColumnNames,"admin_project");
    $sql = sqlSelectSimpleBuilder($dbColumnNames,"loan_status");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package,
        //                         $rebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus,
        //                             $remark, $formCollected, $bankApproved, $loSignedDate, $laSignedDate);

        $stmt->bind_result($id, $projectName, $unitNo, $purchaserName, $ic, $contact, $email, $bookingDate, $sqFt, $spaPrice, $package,
                                $discount, $rebate, $extraRebate, $nettPrice, $totalDeveloperComm, $agent, $loanStatus,
                                    $remark, $bFormCollected, $paymentMethod, $lawyer, $pendingApprovalStatus, $bankApproved, $loSignedDate, $laSignedDate,
                                        $spaSignedDate, $fullsetCompleted, $cashBuyer, $cancelledBooking, $caseStatus, $eventPersonal, $rate, $agentComm,
                                            $upline1, $upline2,$upline3,$upline4,$upline5, $plName, $hosName, $listerName, $ulOverride, $uulOverride,$uuulOverride,$uuuulOverride,$uuuuulOverride,
                                                $plOverride, $hosOverride, $listerOverride, $admin1Override, $admin2Override, $admin3Override, $gicProfit,
                                                    $totalClaimedDevAmt, $totalBalUnclaimAmt, $status1st, $claimAmt1st, $status2nd, $claimAmt2nd,
                                                        $status3rd, $claimAmt3rd, $status4th, $claimAmt4th, $status5th, $claimAmt5th,
                                                            $dateCreated, $dateUpdated,$loanUid,$pdf,$pdfDate,

                                                             $sst1, $sst2, $sst3, $sst4, $sst5, $requestDate1, $requestDate2, $requestDate3, $requestDate4,
                                                                $requestDate5, $checkNo1, $checkNo2, $checkNo3, $checkNo4,
                                                                  $checkNo5, $receiveDate1, $receiveDate2, $receiveDate3,  $receiveDate4, $receiveDate5,$loanAmount,$display,$branchType,
                                                                    $ulBalance, $uulBalance, $uuulBalance, $uuuulBalance, $uuuuulBalance, $listerBalance, $hosBalance,$plBalance,$agentBalance);


        $resultRows = array();
        while ($stmt->fetch()) {
            // $class = new Product();

            $class = new LoanStatus();

            $class->setId($id);
            $class->setProjectName($projectName);
            $class->setUnitNo($unitNo);
            $class->setPurchaserName($purchaserName);
            $class->setIc($ic);
            $class->setContact($contact);
            $class->setEmail($email);
            $class->setBookingDate($bookingDate);
            $class->setSqFt($sqFt);
            $class->setSpaPrice($spaPrice);
            $class->setPackage($package);
            $class->setDisplay($display);
            $class->setDiscount($discount);
            $class->setRebate($rebate);
            $class->setExtraRebate($extraRebate);
            $class->setNettPrice($nettPrice);
            $class->setTotalDeveloperComm($totalDeveloperComm);
            $class->setAgent($agent);
            $class->setLoanStatus($loanStatus);
            $class->setRemark($remark);
            $class->setBFormCollected($bFormCollected);
            $class->setPaymentMethod($paymentMethod);

            $class->setLawyer($lawyer);
            $class->setPendingApprovalStatus($pendingApprovalStatus);
            $class->setBankApproved($bankApproved);
            $class->setLoSignedDate($loSignedDate);
            $class->setLaSignedDate($laSignedDate);
            $class->setSpaSignedDate($spaSignedDate);
            $class->setFullsetCompleted($fullsetCompleted);
            $class->setCashBuyer($cashBuyer);
            $class->setCancelledBooking($cancelledBooking);
            $class->setCaseStatus($caseStatus);

            $class->setEventPersonal($eventPersonal);
            $class->setRate($rate);
            $class->setAgentComm($agentComm);
            $class->setUpline1($upline1);
            $class->setUpline2($upline2);
            $class->setUpline3($upline3);
            $class->setUpline4($upline4);
            $class->setUpline5($upline5);
            $class->setPlName($plName);
            $class->setHosName($hosName);
            $class->setListerName($listerName);
            $class->setUlOverride($ulOverride);
            $class->setUulOverride($uulOverride);
            $class->setUuulOverride($uuulOverride);
            $class->setUuuulOverride($uuuulOverride);
            $class->setUuuuulOverride($uuuuulOverride);

            $class->setPlOverride($plOverride);
            $class->setHosOverride($hosOverride);
            $class->setListerOverride($listerOverride);
            $class->setAdmin1Override($admin1Override);
            $class->setAdmin2Override($admin2Override);
            $class->setAdmin3Override($admin3Override);
            $class->setGicProfit($gicProfit);
            $class->setTotalClaimDevAmt($totalClaimedDevAmt);
            $class->setTotalBalUnclaimAmt($totalBalUnclaimAmt);
            $class->setStatus1st($status1st);

            $class->setClaimAmt1st($claimAmt1st);
            $class->setStatus2nd($status2nd);
            $class->setClaimAmt2nd($claimAmt2nd);
            $class->setStatus3rd($status3rd);
            $class->setClaimAmt3rd($claimAmt3rd);
            $class->setStatus4th($status4th);
            $class->setClaimAmt4th($claimAmt4th);
            $class->setStatus5th($status5th);
            $class->setClaimAmt5th($claimAmt5th);

            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
            $class->setLoanUid($loanUid);
            $class->setPdf($pdf);
            $class->setPdfDate($pdfDate);

            $class->setSst1($sst1);
            $class->setSst2($sst2);
            $class->setSst3($sst3);
            $class->setSst4($sst4);
            $class->setSst5($sst5);

            $class->setRequestDate1($requestDate1);
            $class->setRequestDate2($requestDate2);
            $class->setRequestDate3($requestDate3);
            $class->setRequestDate4($requestDate4);
            $class->setRequestDate5($requestDate5);

            $class->setCheckNo1($checkNo1);
            $class->setCheckNo2($checkNo2);
            $class->setCheckNo3($checkNo3);
            $class->setCheckNo4($checkNo4);
            $class->setCheckNo5($checkNo5);

            $class->setReceiveDate1($receiveDate1);
            $class->setReceiveDate2($receiveDate2);
            $class->setReceiveDate3($receiveDate3);
            $class->setReceiveDate4($receiveDate4);
            $class->setReceiveDate5($receiveDate5);
            $class->setBranchType($branchType);
            $class->setLoanAmount($loanAmount);
            $class->setUlBalance($ulBalance);
            $class->setUulBalance($uulBalance);
            $class->setUuulBalance($uuulBalance);
            $class->setUuuulBalance($uuuulBalance);
            $class->setUuuuulBalance($uuuuulBalance);
            $class->setListerBalance($listerBalance);
            $class->setHosBalance($hosBalance);
            $class->setPLBalance($plBalance);
            $class->setAgentBalance($agentBalance);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
