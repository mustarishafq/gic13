<?php
class Project{
    var $id,$projectId, $projectName, $addProjectPpl, $projectClaims,$companyName,$companyAddress, $dateCreated, $dateUpdated,$projectLeader,$otherClaims,$display,$creatorName,$branchType;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProjectId()
    {
        return $this->project_id;
    }

    /**
     * @param mixed $id
     */
    public function setProjectId($projectId)
    {
        $this->project_id = $projectId;
    }

    /**
     * @return mixed
     */
    public function getCreatorName()
    {
        return $this->creator_name;
    }

    /**
     * @param mixed $id
     */
    public function setCreatorName($creatorName)
    {
        $this->creator_name = $creatorName;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * @param mixed $id
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;
    }

    /**
     * @return mixed
     */
    public function getCompanyAddress()
    {
        return $this->company_address;
    }

    /**
     * @param mixed $id
     */
    public function setCompanyAdress($companyAddress)
    {
        $this->company_address = $companyAddress;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $id
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return mixed
     */
    public function getProjectLeader()
    {
        return $this->project_leader;
    }

    /**
     * @param mixed $id
     */
    public function setProjectLeader($projectLeader)
    {
        $this->project_leader = $projectLeader;
    }

    /**
     * @return mixed
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * @param mixed $projectName
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }

    /**
     * @return mixed
     */
    public function getAddProjectPpl()
    {
        return $this->addProjectPpl;
    }

    /**
     * @param mixed $addProjectPpl
     */
    public function setAddProjectPpl($addProjectPpl)
    {
        $this->addProjectPpl = $addProjectPpl;
    }

    /**
     * @return mixed
     */
    public function getProjectClaims()
    {
        return $this->projectClaims;
    }

    /**
     * @param mixed $projectClaims
     */
    public function setProjectClaims($projectClaims)
    {
        $this->projectClaims = $projectClaims;
    }

    /**
     * @return mixed
     */
    public function getOtherClaims()
    {
        return $this->other_claim;
    }

    /**
     * @param mixed $projectClaims
     */
    public function setOtherClaims($otherClaims)
    {
        $this->other_claim = $otherClaims;
    }

    /**
     * @return mixed
     */
    public function getBranchType()
    {
        return $this->branch_type;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setBranchType($branchType)
    {
        $this->branch_type = $branchType;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getProject($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","project_id","project_name","add_projectppl","claims_no","company_name","company_address","data_created","data_updated","project_leader","other_claim","display","creator_name","branch_type");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"project");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        $stmt->bind_result($id, $projectId, $projectName, $addProjectPpl, $projectClaims,$companyName,$companyAddress, $dateCreated, $dateUpdated,$projectLeader,$otherClaims,$display,$creatorName,$branchType);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Project();
            $class->setId($id);
            $class->setProjectId($projectId);
            $class->setProjectName($projectName);
            $class->setAddProjectPpl($addProjectPpl);
            $class->setProjectClaims($projectClaims);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
            $class->setProjectLeader($projectLeader);
            $class->setOtherClaims($otherClaims);
            $class->setDisplay($display);
            $class->setCompanyName($companyName);
            $class->setCompanyAdress($companyAddress);
            $class->setCreatorName($creatorName);
            $class->setBranchType($branchType);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
