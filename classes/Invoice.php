<?php
class Invoice {
    /* Member variables */
    var $id,$invoiceId,$purchaserName,$item,$remark,$amount,$project,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,
                $dateCreated,$dateUpdated,$loanUid,$charges,$item2,$item3,$item4,$item5,$amount2,$amount3,$amount4,$amount5,
                    $checkID,$receiveStatus,$projectName,$projectHandler,$projectHandlerDefault,$claimsStatus,$claimsDate,$invoiceName,$invoiceType,
                        $unit,$unit2,$unit3,$unit4,$unit5,$status,
                        $unitNo, $bookingDate,$branchType,$creditStatus;

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setID($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getInvoiceId()
    {
        return $this->invoice_id;
    }

    /**
     * @param mixed $id
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoice_id = $invoiceId;
    }

    /**
     * @return mixed
     */
    public function getCheckID()
    {
        return $this->check_id;
    }

    /**
     * @param mixed $id
     */
    public function setCheckID($checkID)
    {
        $this->check_id = $checkID;
    }

    /**
     * @return mixed
     */
    public function getReceiveStatus()
    {
        return $this->receive_status;
    }

    /**
     * @param mixed $id
     */
    public function setReceiveStatus($receiveStatus)
    {
        $this->receive_status = $receiveStatus;
    }

    /**
     * @return mixed
     */
    public function getPurchaserName()
    {
        return $this->purchaser_name;
    }

    /**
     * @param mixed $id
     */
    public function setPurchaserName($purchaserName)
    {
        $this->purchaser_name = $purchaserName;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $id
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getItem2()
    {
        return $this->item2;
    }

    /**
     * @param mixed $id
     */
    public function setItem2($item2)
    {
        $this->item2 = $item2;
    }

    /**
     * @return mixed
     */
    public function getItem3()
    {
        return $this->item3;
    }

    /**
     * @param mixed $id
     */
    public function setItem3($item3)
    {
        $this->item3 = $item3;
    }

    /**
     * @return mixed
     */
    public function getItem4()
    {
        return $this->item4;
    }

    /**
     * @param mixed $id
     */
    public function setItem4($item4)
    {
        $this->item4 = $item4;
    }

    /**
     * @return mixed
     */
    public function getItem5()
    {
        return $this->item5;
    }

    /**
     * @param mixed $id
     */
    public function setItem5($item5)
    {
        $this->item5 = $item5;
    }

    /**
     * @return mixed
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param mixed $id
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $id
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getAmount2()
    {
        return $this->amount2;
    }

    /**
     * @param mixed $id
     */
    public function setAmount2($amount2)
    {
        $this->amount2 = $amount2;
    }

    /**
     * @return mixed
     */
    public function getAmount3()
    {
        return $this->amount3;
    }

    /**
     * @param mixed $id
     */
    public function setAmount3($amount3)
    {
        $this->amount3 = $amount3;
    }

    /**
     * @return mixed
     */
    public function getAmount4()
    {
        return $this->amount4;
    }

    /**
     * @param mixed $id
     */
    public function setAmount4($amount4)
    {
        $this->amount4 = $amount4;
    }

    /**
     * @return mixed
     */
    public function getAmount5()
    {
        return $this->amount5;
    }

    /**
     * @param mixed $id
     */
    public function setAmount5($amount5)
    {
        $this->amount5 = $amount5;
    }

    /**
     * @return mixed
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param mixed $id
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return mixed
     */
    public function getFinalAmount()
    {
        return $this->final_amount;
    }

    /**
     * @param mixed $id
     */
    public function setFinalAmount($finalAmount)
    {
        $this->final_amount = $finalAmount;
    }

    /**
     * @return mixed
     */
    public function getBankAccountHolder()
    {
        return $this->bank_account_holder;
    }

    /**
     * @param mixed $id
     */
    public function setBankAccountHolder($bankAccountHolder)
    {
        $this->bank_account_holder = $bankAccountHolder;
    }

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bank_name;
    }

    /**
     * @param mixed $id
     */
    public function setBankName($bankName)
    {
        $this->bank_name = $bankName;
    }

    /**
     * @return mixed
     */
    public function getBankAccountNo()
    {
        return $this->bank_account_no;
    }

    /**
     * @param mixed $id
     */
    public function setBankAccountNo($bankAccountNo)
    {
        $this->bank_account_no = $bankAccountNo;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getLoanUid()
    {
        return $this->loan_uid;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setLoanUid($loanUid)
    {
        $this->loan_uid = $loanUid;
    }

    /**
     * @return mixed
     */
    public function getCharges()
    {
        return $this->charges;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setCharges($charges)
    {
        $this->charges = $charges;
    }

    /**
     * @return mixed
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * @param mixed $projectName
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }

    /**
     * @return mixed
     */
    public function getProjectHandler()
    {
        return $this->projectHandler;
    }

    /**
     * @param mixed $projectHandler
     */
    public function setProjectHandler($projectHandler)
    {
        $this->projectHandler = $projectHandler;
    }

    /**
     * @return mixed
     */
    public function getProjectHandlerDefault()
    {
        return $this->projectHandlerDefault;
    }

    /**
     * @param mixed $projectHandler
     */
    public function setProjectHandlerDefault($projectHandlerDefault)
    {
        $this->projectHandlerDefault = $projectHandlerDefault;
    }

    /**
     * @return mixed
     */
    public function getClaimsStatus()
    {
        return $this->claimsStatus;
    }

    /**
     * @param mixed $claimsStatus
     */
    public function setClaimsStatus($claimsStatus)
    {
        $this->claimsStatus = $claimsStatus;
    }

    /**
     * @return mixed
     */
    public function getClaimsDate()
    {
        return $this->claimsDate;
    }

    /**
     * @param mixed $claimsDate
     */
    public function setClaimsDate($claimsDate)
    {
        $this->claimsDate = $claimsDate;
    }

    /**
     * @return mixed
     */
    public function getInvoiceName()
    {
        return $this->invoiceName;
    }

    /**
     * @param mixed $invoiceName
     */
    public function setInvoiceName($invoiceName)
    {
        $this->invoiceName = $invoiceName;
    }

    /**
     * @return mixed
     */
    public function getInvoiceType()
    {
        return $this->invoiceType;
    }

    /**
     * @param mixed $invoiceType
     */
    public function setInvoiceType($invoiceType)
    {
        $this->invoiceType = $invoiceType;
    }

    /**
     * @return mixed
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param mixed $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return mixed
     */
    public function getUnit2()
    {
        return $this->unit2;
    }

    /**
     * @param mixed $2
     */
    public function setUnit2($unit2)
    {
        $this->unit2 = $unit2;
    }

    /**
     * @return mixed
     */
    public function getUnit3()
    {
        return $this->unit3;
    }

    /**
     * @param mixed $unit3
     */
    public function setUnit3($unit3)
    {
        $this->unit3 = $unit3;
    }

    /**
     * @return mixed
     */
    public function getUnit4()
    {
        return $this->unit4;
    }

    /**
     * @param mixed $unit4
     */
    public function setUnit4($unit4)
    {
        $this->unit4 = $unit4;
    }

    /**
     * @return mixed
     */
    public function getUnit5()
    {
        return $this->unit5;
    }

    /**
     * @param mixed $unit5
     */
    public function setUnit5($unit5)
    {
        $this->unit5 = $unit5;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getUnitNo()
    {
        return $this->unitNo;
    }

    /**
     * @param mixed $unitNo
     */
    public function setUnitNo($unitNo)
    {
        $this->unitNo = $unitNo;
    }

    /**
     * @return mixed
     */
    public function getBookingDate()
    {
        return $this->bookingDate;
    }

    /**
     * @param mixed $bookingDate
     */
    public function setBookingDate($bookingDate)
    {
        $this->bookingDate = $bookingDate;
    }

    /**
     * @return mixed
     */
    public function getBranchType()
    {
        return $this->branch_type;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setBranchType($branchType)
    {
        $this->branch_type = $branchType;
    }

    /**
     * @return mixed
     */
    public function getCreditStatus()
    {
        return $this->credit_status;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setCreditStatus($creditStatus)
    {
        $this->credit_status = $creditStatus;
    }

}

function getInvoice($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){

    $dbColumnNames = array("id","invoice_id","purchaser_name","item","remark","amount","project","final_amount","bank_account_holder","bank_name","bank_account_no",
                                "date_created","date_updated","loan_uid","charges","item2","item3","item4","item5","amount2","amount3","amount4","amount5",
                                    "check_id", "receive_status", "project_name","project_handler","project_handler_default","claims_status","claims_date","invoice_name","invoice_type",
                                        "unit","unit2","unit3","unit4","unit5","status","unit_no","booking_date", "branch_type","credit_status");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"invoice");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$invoiceId,$purchaserName,$item,$remark,$amount,$project,$finalAmount,$bankAccountHolder,$bankName,$bankAccountNo,
                                $dateCreated,$dateUpdated,$loanUid,$charges,$item2,$item3,$item4,$item5,$amount2,$amount3,$amount4,$amount5,
                                    $checkID,$receiveStatus,$projectName,$projectHandler,$projectHandlerDefault,$claimsStatus,$claimsDate,$invoiceName,$invoiceType,
                                        $unit,$unit2,$unit3,$unit4,$unit5,$status,$unitNo, $bookingDate,$branchType,$creditStatus);


        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Invoice();
            $class->setID($id);
            $class->setInvoiceId($invoiceId);
            $class->setPurchaserName($purchaserName);
            $class->setItem($item);
            $class->setItem2($item2);
            $class->setItem3($item3);
            $class->setItem4($item4);
            $class->setItem5($item5);
            $class->setRemark($remark);
            $class->setAmount($amount);
            $class->setAmount2($amount2);
            $class->setAmount3($amount3);
            $class->setAmount4($amount4);
            $class->setAmount5($amount5);
            $class->setProject($project);
            $class->setBankAccountHolder($bankAccountHolder);
            $class->setBankName($bankName);
            $class->setBankAccountNo($bankAccountNo);
            $class->setFinalAmount($finalAmount);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
            $class->setLoanUid($loanUid);
            $class->setCharges($charges);
            $class->setCheckID($checkID);
            $class->setReceiveStatus($receiveStatus);

            $class->setProjectName($projectName);
            $class->setProjectHandler($projectHandler);
            $class->setProjectHandlerDefault($projectHandlerDefault);
            $class->setClaimsStatus($claimsStatus);
            $class->setClaimsDate($claimsDate);
            $class->setInvoiceName($invoiceName);
            $class->setInvoiceType($invoiceType);

            $class->setUnit($unit);
            $class->setUnit2($unit2);
            $class->setUnit3($unit3);
            $class->setUnit4($unit4);
            $class->setUnit5($unit5);
            $class->setStatus($status);

            $class->setUnitNo($unitNo);
            $class->setBookingDate($bookingDate);
            $class->setBranchType($branchType);
            $class->setCreditStatus($creditStatus);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
