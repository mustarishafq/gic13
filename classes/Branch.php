<?php
class Branch {
    /* Member variables */
    // var $id, $loanUid, $upline, $commission, $purchaserName, $checkID, $receiveStatus, $dateCreated;
    var $id, $branchName,$branchType,$createdBy, $dateCreated, $dateUpdated;

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setID($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getBranchName()
    {
        return $this->branch_name;
    }

    /**
     * @param mixed $id
     */
    public function setBranchName($branchName)
    {
        $this->branch_name = $branchName;
    }

    /**
     * @return mixed
     */
    public function getBranchType()
    {
        return $this->branch_type;
    }

    /**
     * @param mixed $id
     */
    public function setBranchType($branchType)
    {
        $this->branch_type = $branchType;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->creator;
    }

    /**
     * @param mixed $id
     */
    public function setCreatedBy($createdBy)
    {
        $this->creator = $createdBy;
    }

      /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;
    }

}

function getBranch($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    // $dbColumnNames = array("id","loan_uid","upline","commission","purchaser_name", "check_id", "receive_status", "date_created");
    $dbColumnNames = array("id","branch_name","branch_type","creator","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"branch");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id, $loanUid, $upline, $commission, $purchaserName, $checkID, $receiveStatus, $dateCreated);
        $stmt->bind_result($id, $branchName,$branchType,$createdBy, $dateCreated, $dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Branch();
            $class->setID($id);
            $class->setBranchName($branchName);
            $class->setBranchType($branchType);
            $class->setCreatedBy($createdBy);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
