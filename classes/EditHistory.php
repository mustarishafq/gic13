<?php
class editHistory {
    /* Member variables */
    // var $id, $loanUid, $upline, $commission, $purchaserName, $checkID, $receiveStatus, $dateCreated;
    var $id, $loanUid, $username, $details, $dataBefore, $dataAfter, $branchType, $dateCreated;

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setID($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @param mixed $id
     */
    public function setDetails($details)
    {
        $this->details = $details;
    }

    /**
     * @return mixed
     */
    public function getDataBefore()
    {
        return $this->data_before;
    }

    /**
     * @param mixed $id
     */
    public function setDataBefore($dataBefore)
    {
        $this->data_before = $dataBefore;
    }

    /**
     * @return mixed
     */
    public function getDataAfter()
    {
        return $this->data_after;
    }

    /**
     * @param mixed $id
     */
    public function setDataAfter($dataAfter)
    {
        $this->data_after = $dataAfter;
    }

      /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getLoanUid()
    {
        return $this->loan_uid;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setLoanUid($loanUid)
    {
        $this->loan_uid = $loanUid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getBranchType()
    {
        return $this->branch_type;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setBranchType($branchType)
    {
        $this->branch_type = $branchType;
    }

}

function getEditHistory($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    // $dbColumnNames = array("id","loan_uid","upline","commission","purchaser_name", "check_id", "receive_status", "date_created");
    $dbColumnNames = array("id","loan_uid","username","details","data_before","data_after", "branch_type","date_created");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"edit_history");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id, $loanUid, $upline, $commission, $purchaserName, $checkID, $receiveStatus, $dateCreated);
        $stmt->bind_result($id, $loanUid, $username, $details, $dataBefore, $dataAfter, $branchType, $dateCreated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new editHistory();
            $class->setID($id);
            $class->setLoanUid($loanUid);
            $class->setUsername($username);
            $class->setDetails($details);
            $class->setDataBefore($dataBefore);
            $class->setDataAfter($dataAfter);
            $class->setBranchType($branchType);
            $class->setDateCreated($dateCreated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
