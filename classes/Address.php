<?php
class CompanyAddress {
    /* Member variables */
    // var $id,$referrerId,$referralId,$currentLevel,$topReferrerId,$dateCreated,$dateUpdated;
    var $id,$logo,$companyName,$addressNo,$companyBranch,$companyAddress,$contact,$branchType,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $id
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * @param mixed $referrerId
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;
    }

    /**
     * @return mixed
     */
    public function getAddressNo()
    {
        return $this->address_no;
    }

    /**
     * @param mixed $referralName
     */
    public function setAddressNo($addressNo)
    {
        $this->address_no = $addressNo;
    }

    /**
     * @return mixed
     */
    public function getCompanyBranch()
    {
        return $this->company_branch;
    }

    /**
     * @param mixed $referralName
     */
    public function setCompanyBranch($companyBranch)
    {
        $this->company_branch = $companyBranch;
    }

    /**
     * @return mixed
     */
    public function getCompanyAddress()
    {
        return $this->company_address;
    }

    /**
     * @param mixed $referralId
     */
    public function setCompanyAddress($companyAddress)
    {
        $this->company_address = $companyAddress;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $currentLevel
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getBranchType()
    {
        return $this->branch_type;
    }

    /**
     * @param mixed $referralName
     */
    public function setBranchType($branchType)
    {
        $this->branch_type = $branchType;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getAddress($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    // $dbColumnNames = array("id","referrer_id","referral_id","current_level","top_referrer_id","date_created","date_updated");
    $dbColumnNames = array("id","logo","company_name","address_no","company_branch","company_address","contact","branch_type","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"address");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id, $referrerId, $referralId, $currentLevel, $topReferrerId, $dateCreated, $dateUpdated);
        $stmt->bind_result($id,$logo,$companyName,$addressNo,$companyBranch,$companyAddress,$contact,$branchType,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new CompanyAddress();
            $class->setId($id);
            $class->setLogo($logo);
            $class->setCompanyName($companyName);
            $class->setAddressNo($addressNo);
            $class->setCompanyBranch($companyBranch);
            $class->setCompanyAddress($companyAddress);
            $class->setContact($contact);
            $class->setBranchType($branchType);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
