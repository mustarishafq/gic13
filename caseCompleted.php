<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Invoice.php';
// require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$cntNO = 0;$cntYES = 0;$branchType = $_SESSION['branch_type'];
// $loanUidRows = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$loanUidRows = getLoanStatus($conn);
$invoiceDetails = getInvoice($conn);
// $projectName = "WHERE case_status = 'COMPLETED'";
$projectName = "";$totalSpaPrice = 0;$totalNettPrice = 0;$finalTotalDevComm = 0;$totalAgentComm = 0;$totalUpline1Comm = 0;
$totalUpline2Comm = 0;$totalUpline3Comm = 0;$totalUpline4Comm = 0;$totalUpline5Comm = 0;$totalPlOverride = 0;$totalHosOverride = 0;
$totalListerOverride = 0;$totalGicProfit = 0;$TotalClaimDev = 0;$totalBalanceUnclaim = 0;

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Loan Status | GIC" />
    <title>Loan Status | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<style>
table {
border-spacing: 0;
width: 100%;
border: 1px solid #ddd;
}

th {
cursor: pointer;
}
.th:hover{
  background-color: maroon;
}
.th0:hover{
  background-color: maroon;
}
th, td {
text-align: left;
padding: 16px;
}

/* tr:nth-child(even) {
background-color: #f2f2f2
} */
#agentDetailsBox{
  cursor: pointer;
}
#agentDetailsBox:hover{
  color: red;
}
.th0 {
    vertical-align: middle;
    z-index: 2;
    background-color: #7babff;
}
.td0{

  text-align: center;
}
.th.headerSortUp , .th0.headerSortUp{
  background-color: black;
}
.th.headerSortDown , .th0.headerSortDown{
  background-color: black;
}
.total-amt{
  background-color: lightgrey !important;
  font-weight: bold;
  text-align: center;
}
</style>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Loan Status</h1>
    <div class="short-red-border overflow"></div>

    <!-- This is a filter for the table result -->
    <div class="section-divider width100 overflow">
		<?php $projectDetails = getProject($conn, "WHERE display = 'Yes'"); ?>

      <select id="projectChoose" name="admin2Product" class="clean-select clean pointer">
        <option value="">Select a project</option>
        <?php if ($projectDetails) {
          for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
            if ($projectDetails[$cnt]->getProjectName()) {
              ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
              }
              }
              ?><option value="">SHOW ALL</option><?php
            } ?>
      </select>
        <input class="clean search-btn" type="text" name="" value="" placeholder="Search.." id="searchInput">
    </div>

    <div class="clear"></div>

    <div id="removeAttr" class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table id="myTable" class="shipping-table">
                <thead>
                    <tr>
                        <th class="th0 company-td">NO.</th>
                        <th class="th0 company-td1">ACTION</th>
                        <th class="th0 company-td2"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?></th>
                        <th class="th0 company-td3"><?php echo "PROJECT NAME";?></th>
                        <th class="th"><?php echo wordwrap("PURCHASER NAME",10,"</br>\n");?></th>
                        <th class="th">IC</th>
                        <th class="th">CONTACT</th>
                        <th class="th">E-MAIL</th>
                        <th class="th"><?php echo wordwrap("BOOKING DATE",10,"</br>\n");?></th>
                        <th class="th">SQ FT</th>
                        <th class="th"><?php echo wordwrap("SPA PRICE",8,"</br>\n");?></th>
                        <th class="th">PACKAGE</th>
                        <th class="th">DISCOUNT</th>
                        <th class="th">REBATE</th>
                        <!-- <th class="th"><?php //echo wordwrap("EXTRA REBATE",10,"</br>\n");?></th> -->
                        <th class="th"><?php echo wordwrap("NETT PRICE",8,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("TOTAL DEVELOPER COMMISSION",10,"</br>\n");?></th>
                        <th class="th">AGENT</th>
                        <th class="th"><?php echo wordwrap("LOAN STATUS",10,"</br>\n");?></th>

                        <th class="th">REMARK</th>
                        <th class="th"><?php echo wordwrap("FORM COLLECTED",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("PAYMENT METHOD",10,"</br>\n");?></th>
                        <th class="th">LAWYER</th>
                        <!-- <th class="th"><?php //echo wordwrap("PENDING APPROVAL STATUS",10,"</br>\n");?></th> -->
                        <th class="th"><?php echo wordwrap("BANK APPROVED",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LOAN AMOUNT",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LO SIGNED DATE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LA SIGNED DATE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("SPA SIGNED DATE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("FULLSET COMPLETED",10,"</br>\n");?></th>
                        <th class="th">CASH BUYER</th>
                        <th class="th"><?php echo wordwrap("CANCELLED BOOKING",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("CASE STATUS",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("EVENT / PERSONAL",10,"</br>\n");?></th>
                        <th class="th">RATE</th>
                        <th class="th"><?php echo wordwrap("AGENT COMMISSION",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UP1 NAME",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UP2 NAME",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UP3 NAME",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UP4 NAME",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UP5 NAME",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("PL NAME",5,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("HOS NAME",7,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LISTER NAME",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UP1 OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UP2 OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UP3 OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UP4 OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("UP5 OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("PL OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("HOS OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("LISTER OVERRIDE",10,"</br>\n");?></th>

                        <!-- <th class="th"><?php //echo wordwrap("ADMIN1 OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("ADMIN2 OVERRIDE",10,"</br>\n");?></th>
                        <th class="th"><?php //echo wordwrap("ADMIN3 OVERRIDE",10,"</br>\n");?></th> -->
                        <th class="th"><?php echo wordwrap("GIC PROFIT",8,"</br>\n");?></th>
                          <th class="th"><?php echo wordwrap("TOTAL CLAIMED DEV AMT",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("TOTAL BALANCED DEV AMT",10,"</br>\n");?></th>
                        <th class="th"><?php echo wordwrap("DATE MODIFIED",10,"</br>\n");?></th>
                        <!-- <th class="th">BANK APPROVED</th>
                        <th class="th">LO SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th> -->

                        <th class="th">ARCHIVE</th>
                        <!-- <th>INVOICE</th> -->
                    </tr>
                </thead>
                <tbody id="myFilter">
                    <?php
                    if ($branchType == 1) {
                        $loanDetails = getLoanStatus($conn,"WHERE display = 'Yes' and case_status ='COMPLETED'");
                    }else {
                        $loanDetails = getLoanStatus($conn,"WHERE display = 'Yes' and branch_type = ? and case_status ='COMPLETED'",array("branch_type"),array($_SESSION['branch_type']), "s");
                    }
                        if($loanDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($loanDetails) ;$cntAA++)
                            {
                              if ($loanDetails[$cntAA]->getCancelledBooking() != 'YES') {
                                $cntNO++;
                                $totalSpaPrice += $loanDetails[$cntAA]->getSpaPrice();
                                $totalNettPrice += $loanDetails[$cntAA]->getNettPrice();
                                $finalTotalDevComm += $loanDetails[$cntAA]->getTotalDeveloperComm();
                                $totalAgentComm += $loanDetails[$cntAA]->getAgentComm();
                                $totalUpline1Comm += $loanDetails[$cntAA]->getUlOverride();
                                $totalUpline2Comm += $loanDetails[$cntAA]->getUulOverride();
                                $totalUpline3Comm += $loanDetails[$cntAA]->getUuulOverride();
                                $totalUpline4Comm += $loanDetails[$cntAA]->getUuuuulOverride();
                                $totalUpline5Comm += $loanDetails[$cntAA]->getUuuuulOverride();
                                $totalPlOverride += $loanDetails[$cntAA]->getPlOverride();
                                $totalHosOverride += $loanDetails[$cntAA]->getHosOverride();
                                $totalListerOverride += $loanDetails[$cntAA]->getListerOverride();
                                $totalGicProfit += $loanDetails[$cntAA]->getGicProfit();
                                $TotalClaimDev += $loanDetails[$cntAA]->getTotalClaimDevAmt();
                                $totalBalanceUnclaim += $loanDetails[$cntAA]->getTotalBalUnclaimAmt();

                              ?>
                            <tr>
                                <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                <td class="td company-td"><?php echo $cntNO + $cntYES;?></td>
                                <td class="td company-td1">
                                    <form action="editProduct.php" method="POST">
                                        <button class="edit-btn" type="submit" name="loan_uid" value="<?php echo $loanDetails[$cntAA]->getLoanUid();?>">
                                          Edit
                                        </button>
                                    </form>
                                </td>
                                <td class="td company-td2"><?php echo wordwrap($loanDetails[$cntAA]->getUnitNo(),10,'<br>');?></td>
                                <td class="td company-td3"><?php echo $loanDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getPurchaserName());?></td>
                                <td class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getIc());?></td>
                                <td class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getContact());?></td>

                                <td class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getEmail());?></td>
                                <td class="td"><?php echo date('d-m-Y', strtotime($loanDetails[$cntAA]->getBookingDate()));?></td>
                                <td class="td"><?php
                                if ($loanDetails[$cntAA]->getSqFt()) {
                                  echo number_format($loanDetails[$cntAA]->getSqFt());
                                }
                                ?></td>

                                <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getSpaPrice();?></td> -->

                                <!-- show , inside value -->
                                <?php $spaPrice = $loanDetails[$cntAA]->getSpaPrice();?>
                                <td class="td"><?php
                                if ($spaPrice) {
                                  echo $spaPriceValue = number_format($spaPrice, 2);
                                }
                                 ?></td>

                                <td class="td"><?php echo $loanDetails[$cntAA]->getPackage();?></td>
                                <td class="td"><?php echo $loanDetails[$cntAA]->getRebate();?></td>
                                <td class="td"><?php echo $loanDetails[$cntAA]->getDiscount();?></td>
                                <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getExtraRebate();?></td> -->

                                <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getNettPrice();?></td>
                                <td class="td"><?php //echo $loanDetails[$cntAA]->getTotalDeveloperComm();?></td> -->

                                <!-- show , inside value -->
                                <?php $nettPrice = $loanDetails[$cntAA]->getNettPrice();?>
                                <td class="td"><?php if ($nettPrice) {
                                  echo $nettPriceValue = number_format($nettPrice, 2);
                                } ?></td>
                                <?php $totalDevComm = $loanDetails[$cntAA]->getTotalDeveloperComm();?>
                                <td class="td"><?php if ($totalDevComm) {
                                  echo $totalDevCommValue = number_format($totalDevComm, 2);
                                } ?></td>

                                <td class="td"><a class="pointer" id="<?php echo "agentNameDetails".($cntNO+$cntYES) ?>" value="<?php echo $loanDetails[$cntAA]->getAgent();?>"><?php echo $loanDetails[$cntAA]->getAgent();?></a> </td>
                                <td class="td"><?php echo wordwrap($loanDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>

                                <td class="td"><?php echo $loanDetails[$cntAA]->getRemark();?></td>
                                <td class="td"><?php echo $loanDetails[$cntAA]->getBFormCollected();?></td>
                                <td class="td"><?php echo $loanDetails[$cntAA]->getPaymentMethod();?></td>
                                <td class="td"><?php echo $loanDetails[$cntAA]->getLawyer();?></td>
                                <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getPendingApprovalStatus();?></td> -->
                                <td class="td"><?php echo $loanDetails[$cntAA]->getBankApproved();?></td>
                                <td class="td"><?php echo $loanDetails[$cntAA]->getLoanAmount();?></td>
                                <td class="td"><?php
                                if ($loanDetails[$cntAA]->getLoSignedDate()) {
                                  echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getLoSignedDate()));
                                }
                                 ?></td>
                                 <td class="td"><?php
                                 if ($loanDetails[$cntAA]->getLaSignedDate()) {
                                   echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getLaSignedDate()));
                                 }
                                  ?></td>
                                  <td class="td"><?php
                                  if ($loanDetails[$cntAA]->getSpaSignedDate()) {
                                    echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getSpaSignedDate()));
                                  }
                                   ?></td>
                                <td class="td"><?php echo $loanDetails[$cntAA]->getFullsetCompleted();?></td>

                                <td class="td"><?php echo $loanDetails[$cntAA]->getCashBuyer();?></td>

                                <td class="td"><?php echo $loanDetails[$cntAA]->getCancelledBooking();?></td>
                                <td class="td"><?php echo $loanDetails[$cntAA]->getCaseStatus();?></td>
                                <td class="td"><?php echo $loanDetails[$cntAA]->getEventPersonal();?></td>
                                <td class="td"><?php echo $loanDetails[$cntAA]->getRate();
                                if ($loanDetails[$cntAA]->getRate()) {
                                  echo "%";
                                }?></td>

                                <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getAgentComm();?></td> -->

                                <?php $agentComm = $loanDetails[$cntAA]->getAgentComm();?>
                                <td class="td"><?php if ($agentComm) {
                                  echo $agentCommValue = number_format($agentComm, 2);
                                } ?></td>

                                <td class="td"><a id="agentDetailsBox" class="<?php echo "upline1Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline1();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline1(),20,"<br>");?></a></td>
                                <td class="td"><a id="agentDetailsBox" class="<?php echo "upline2Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline2();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline2(),20,"<br>");?></a></td>
                                <td class="td"><a id="agentDetailsBox" class="<?php echo "upline3Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline3();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline3(),20,"<br>");?></a></td>
                                <td class="td"><a id="agentDetailsBox" class="<?php echo "upline4Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline4();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline4(),20,"<br>");?></a></td>
                                <td class="td"><a id="agentDetailsBox" class="<?php echo "upline5Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline5();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline5(),20,"<br>");?></a></td>
                                <td class="td"><?php echo str_replace(",","<br>",$loanDetails[$cntAA]->getPlName());?></td>
                                <td class="td"><?php echo $loanDetails[$cntAA]->getHosName();?></td>
                                <td class="td"><?php echo $loanDetails[$cntAA]->getListerName();?></td>

                                <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getUlOverride();?></td>
                                <td class="td"><?php //echo $loanDetails[$cntAA]->getUulOverride();?></td>
                                <td class="td"><?php //echo $loanDetails[$cntAA]->getPlOverride();?></td>
                                <td class="td"><?php //echo $loanDetails[$cntAA]->getHosOverride();?></td>
                                <td class="td"><?php //echo $loanDetails[$cntAA]->getListerOverride();?></td>
                                <td class="td"><?php //echo $loanDetails[$cntAA]->getAdmin1Override();?></td>
                                <td class="td"><?php //echo $loanDetails[$cntAA]->getAdmin2Override();?></td>
                                <td class="td"><?php //echo $loanDetails[$cntAA]->getGicProfit();?></td>
                                <td class="td"><?php //echo $loanDetails[$cntAA]->getTotalClaimDevAmt();?></td>
                                <td class="td"><?php //echo $loanDetails[$cntAA]->getTotalBalUnclaimAmt();?></td> -->

                                <!-- show , inside value -->
                                <?php $ulOverride = $loanDetails[$cntAA]->getUlOverride();?>
                                <td class="td"><?php if ($ulOverride) {
                                  echo $ulOverrideValue = number_format($ulOverride, 2);
                                } ?></td>
                                <?php $uulOverride = $loanDetails[$cntAA]->getUulOverride();?>
                                <td class="td"><?php if ($uulOverride) {
                                  echo $uulOverrideValue = number_format($uulOverride, 2);
                                } ?></td>
                                <?php $uuulOverride = $loanDetails[$cntAA]->getUuulOverride();?>
                                <td class="td"><?php if ($uuulOverride) {
                                  echo $uuulOverrideValue = number_format($uuulOverride, 2);
                                } ?></td>
                                <?php $uuuulOverride = $loanDetails[$cntAA]->getUuuulOverride();?>
                                <td class="td"><?php if ($uuuulOverride) {
                                  echo $uuuulOverrideValue = number_format($uuuulOverride, 2);
                                } ?></td>
                                <?php $uuuuulOverride = $loanDetails[$cntAA]->getUuuuulOverride();?>
                                <td class="td"><?php if ($uuuuulOverride) {
                                  echo $uuuuulOverrideValue = number_format($uuuuulOverride, 2);
                                } ?></td>
                                <?php $plOverride = $loanDetails[$cntAA]->getPlOverride();?>
                                <td class="td"><?php if ($plOverride) {
                                  echo $plOverrideValue = number_format($plOverride, 2);
                                } ?></td>
                                <?php $hosOverride = $loanDetails[$cntAA]->getHosOverride();?>
                                <td class="td"><?php if ($hosOverride) {
                                  echo $hosOverrideValue = number_format($hosOverride, 2);
                                } ?></td>
                                <?php $listerOverride = $loanDetails[$cntAA]->getListerOverride();?>
                                <td class="td"><?php if ($listerOverride) {
                                  echo $listerOverrideValue = number_format($listerOverride, 2);
                                } ?></td>
                                <!-- <?php $a1Over = $loanDetails[$cntAA]->getAdmin1Override();?>
                                <td class="td"><?php if ($a1Over) {
                                  echo $a3OverValue = number_format($a1Over, 2);
                                } ?></td>
                                <?php $a2Over = $loanDetails[$cntAA]->getAdmin2Override();?>
                                <td class="td"><?php if ($a2Over) {
                                  echo $a2OverValue = number_format($a2Over, 2);
                                } ?></td>
                                <?php $a3Over = $loanDetails[$cntAA]->getAdmin3Override();?>
                                <td class="td"><?php if ($a3Over) {
                                  echo $a3OverValue = number_format($a3Over, 2);
                                } ?></td> -->
                                <?php $gicProfit = $loanDetails[$cntAA]->getGicProfit();?>
                                <td class="td"><?php if ($gicProfit) {
                                  echo $gicProfitValue = number_format($gicProfit, 2);
                                } ?></td>
                                <?php $totalClaim = $loanDetails[$cntAA]->getTotalClaimDevAmt();?>
                                <td class="td"><?php if ($totalClaim) {
                                  echo $totalClaimValue = number_format($totalClaim, 2);
                                } ?></td>
                                <?php $totalUnclaim = $loanDetails[$cntAA]->getTotalBalUnclaimAmt();?>
                                <td class="td"><?php if ($totalUnclaim) {
                                  echo $totalUnclaimValue = number_format($totalUnclaim, 2);
                                } ?></td>
                                <td id="dateModified" class="td">
                                  <form class="" action="historyIndividual.php" method="post">
                                    <input type="hidden"  id="loanUid" name="loan_uid" value="<?php echo $loanDetails[$cntAA]->getLoanUid() ?>">
                                    <input type="hidden" id="totalLoanDetails" value="<?php echo count($loanDetails) ?>">
                                    <button type="submit" class="clean edit-anc-btn hover1" name="button"><a><?php echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getDateUpdated()));?></a> </button>
                                    </td>
                                  </form>

                                <td class="td">
                                    <!-- <form action="utilities/archiveLoan.php" method="POST"> -->
                                        <button id="archiveBtn" class="clean edit-anc-btn hover1" type="submit" name="loan_uid_archive" value="<?php echo $loanDetails[$cntAA]->getLoanUid();?>">
                                            <img  src="img/archiv0.png" class="edit-announcement-img hover1a" >
                                            <img src="img/archiv3.png" class="edit-announcement-img hover1b" >
                                        </button>
                                    <!-- </form> -->
                                </td>

                            </tr>
                            <?php
                          }}
                          for($cntAA = 0;$cntAA < count($loanDetails) ;$cntAA++)
                          {
                            if ($loanDetails[$cntAA]->getCancelledBooking() == 'YES') {
                              $cntYES++;
                            ?>
                          <tr>
                              <!-- <td><?php //echo ($cntAA+1)?></td> -->
                              <td class="td company-td" style="background-color: pink;"><?php echo ($cntNO+$cntYES)?></td>
                              <td style="background-color: pink " class="td company-td1">
                                  <form action="editProduct.php" method="POST">
                                      <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid" value="<?php echo $loanDetails[$cntAA]->getLoanUid();?>">
                                          <img src="img/edit.png" class="edit-announcement-img hover1a" >
                                          <img src="img/edit3.png" class="edit-announcement-img hover1b" >
                                      </button>
                                  </form>
                              </td>
                              <td class="td company-td2" style="background-color: pink;"><?php echo wordwrap($loanDetails[$cntAA]->getUnitNo(),10,'<br>');?></td>
                              <td class="td company-td3" style="background-color: pink;"><?php echo $loanDetails[$cntAA]->getProjectName();?></td>
                              <td style="background-color: pink " class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getPurchaserName());?></td>
                              <td style="background-color: pink " class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getIc());?></td>
                              <td style="background-color: pink " class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getContact());?></td>

                              <td style="background-color: pink " class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getEmail());?></td>
                              <td style="background-color: pink " class="td"><?php echo date('d-m-Y', strtotime($loanDetails[$cntAA]->getBookingDate()));?></td>
                              <td style="background-color: pink " class="td"><?php
                              if ($loanDetails[$cntAA]->getSqFt()) {
                                echo number_format($loanDetails[$cntAA]->getSqFt());
                              }
                              ?></td>

                              <!-- <td style="background-color: pink " class="td"><?php //echo $loanDetails[$cntAA]->getSpaPrice();?></td> -->

                              <!-- show , inside value -->
                              <?php $spaPrice = $loanDetails[$cntAA]->getSpaPrice();?>
                              <td style="background-color: pink " class="td"><?php
                              if ($spaPrice) {
                                echo $spaPriceValue = number_format($spaPrice, 2);
                              }
                               ?></td>

                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getPackage();?></td>
                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getRebate();?></td>
                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getDiscount();?></td>
                              <!-- <td style="background-color: pink " class="td"><?php //echo $loanDetails[$cntAA]->getExtraRebate();?></td> -->

                              <!-- <td style="background-color: pink " class="td"><?php //echo $loanDetails[$cntAA]->getNettPrice();?></td>
                              <td style="background-color: pink " class="td"><?php //echo $loanDetails[$cntAA]->getTotalDeveloperComm();?></td> -->

                              <!-- show , inside value -->
                              <?php $nettPrice = $loanDetails[$cntAA]->getNettPrice();?>
                              <td style="background-color: pink " class="td"><?php if ($nettPrice) {
                                echo $nettPriceValue = number_format($nettPrice, 2);
                              } ?></td>
                              <?php $totalDevComm = $loanDetails[$cntAA]->getTotalDeveloperComm();?>
                              <td style="background-color: pink " class="td"><?php if ($totalDevComm) {
                                echo $totalDevCommValue = number_format($totalDevComm, 2);
                              } ?></td>

                              <td style="background-color: pink " class="td"><a class="pointer" id=<?php echo "agentNameDetails".($cntNO+$cntYES) ?> value="<?php echo $loanDetails[$cntAA]->getAgent();?>"><?php echo $loanDetails[$cntAA]->getAgent();?></a> </td>
                              <td style="background-color: pink " class="td"><?php echo wordwrap($loanDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>

                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getRemark();?></td>
                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getBFormCollected();?></td>
                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getPaymentMethod();?></td>
                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getLawyer();?></td>
                              <!-- <td style="background-color: pink " class="td"><?php //echo $loanDetails[$cntAA]->getPendingApprovalStatus();?></td> -->
                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getBankApproved();?></td>
                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getLoanAmount();?></td>
                              <td style="background-color: pink " class="td"><?php if ($loanDetails[$cntAA]->getLoSignedDate()) {
                                echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getLoSignedDate()));
                              };?></td>
                              <td style="background-color: pink " class="td"><?php if ($loanDetails[$cntAA]->getLaSignedDate()) {
                                echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getLaSignedDate()));
                              };?></td>
                              <td style="background-color: pink " class="td"><?php if ($loanDetails[$cntAA]->getSpaSignedDate()) {
                                echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getSpaSignedDate()));
                              };?></td>
     <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getFullsetCompleted();?></td>

                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getCashBuyer();?></td>

                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getCancelledBooking();?></td>
                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getCaseStatus();?></td>
                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getEventPersonal();?></td>
                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getRate();
                              if ($loanDetails[$cntAA]->getRate()) {
                                echo "%";
                              }?></td>

                              <!-- <td style="background-color: pink " class="td"><?php //echo $loanDetails[$cntAA]->getAgentComm();?></td> -->

                              <?php $agentComm = $loanDetails[$cntAA]->getAgentComm();?>
                              <td style="background-color: pink " class="td"><?php if ($agentComm) {
                                echo $agentCommValue = number_format($agentComm, 2);
                              } ?></td>

                              <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline1Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline1();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline1(),20,"<br>");?></a></td>
                              <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline2Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline2();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline2(),20,"<br>");?></a></td>
                              <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline3Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline3();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline3(),20,"<br>");?></a></td>
                              <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline4Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline4();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline4(),20,"<br>");?></a></td>
                              <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline5Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline5();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline5(),20,"<br>");?></a></td>
                              <td style="background-color: pink " class="td"><?php echo str_replace(",","<br>",$loanDetails[$cntAA]->getPlName());?></td>
                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getHosName();?></td>
                              <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getListerName();?></td>
                              <?php $ulOverride = $loanDetails[$cntAA]->getUlOverride();?>
                              <td style="background-color: pink " class="td"><?php if ($ulOverride) {
                                echo $ulOverrideValue = number_format($ulOverride, 2);
                              } ?></td>
                              <?php $uUlOverride = $loanDetails[$cntAA]->getUulOverride();?>
                              <td style="background-color: pink " class="td"><?php if ($uUlOverride) {
                                echo $uUlOverrideValue = number_format($uUlOverride, 2);
                              } ?></td>
                              <?php $uuUlOverride = $loanDetails[$cntAA]->getUuulOverride();?>
                              <td style="background-color: pink " class="td"><?php if ($uuUlOverride) {
                                echo $uuUlOverrideValue = number_format($uuUlOverride, 2);
                              } ?></td>
                              <?php $uuuUlOverride = $loanDetails[$cntAA]->getUuuulOverride();?>
                              <td style="background-color: pink " class="td"><?php if ($uuuUlOverride) {
                                echo $uuuUlOverrideValue = number_format($uuuUlOverride, 2);
                              } ?></td>
                              <?php $uuuuUlOverride = $loanDetails[$cntAA]->getUuuuulOverride();?>
                              <td style="background-color: pink " class="td"><?php if ($uuuuUlOverride) {
                                echo $uuuuUlOverrideValue = number_format($uuuuUlOverride, 2);
                              } ?></td>
                              <?php $plOverride = $loanDetails[$cntAA]->getPlOverride();?>
                              <td style="background-color: pink " class="td"><?php if ($plOverride) {
                                echo $plOverrideValue = number_format($plOverride, 2);
                              } ?></td>
                              <?php $hosOverride = $loanDetails[$cntAA]->getHosOverride();?>
                              <td style="background-color: pink " class="td"><?php if ($hosOverride) {
                                echo $hosOverrideValue = number_format($hosOverride, 2);
                              } ?></td>
                              <?php $listerOverride = $loanDetails[$cntAA]->getListerOverride();?>
                              <td style="background-color: pink " class="td"><?php if ($listerOverride) {
                                echo $listerOverrideValue = number_format($listerOverride, 2);
                              } ?></td>
                              <!-- <?php $a1Over = $loanDetails[$cntAA]->getAdmin1Override();?>
                              <td style="background-color: pink " class="td"><?php if ($a1Over) {
                                echo $a3OverValue = number_format($a1Over, 2);
                              } ?></td>
                              <?php $a2Over = $loanDetails[$cntAA]->getAdmin2Override();?>
                              <td style="background-color: pink " class="td"><?php if ($a2Over) {
                                echo $a2OverValue = number_format($a2Over, 2);
                              } ?></td>
                              <?php $a3Over = $loanDetails[$cntAA]->getAdmin3Override();?>
                              <td style="background-color: pink " class="td"><?php if ($a3Over) {
                                echo $a3OverValue = number_format($a3Over, 2);
                              } ?></td> -->
                              <?php $gicProfit = $loanDetails[$cntAA]->getGicProfit();?>
                              <td style="background-color: pink " class="td"><?php if ($gicProfit) {
                                echo $gicProfitValue = number_format($gicProfit, 2);
                              } ?></td>
                              <?php $totalClaim = $loanDetails[$cntAA]->getTotalClaimDevAmt();?>
                              <td style="background-color: pink " class="td"><?php if ($totalClaim) {
                                echo $totalClaimValue = number_format($totalClaim, 2);
                              } ?></td>
                              <?php $totalUnclaim = $loanDetails[$cntAA]->getTotalBalUnclaimAmt();?>
                              <td style="background-color: pink " class="td"><?php if ($totalUnclaim) {
                                echo $totalUnclaimValue = number_format($totalUnclaim, 2);
                              } ?></td>
                              <td style="background-color: pink " id="dateModified" class="td">
                                <form class="" action="historyIndividual.php" method="post">
                                  <input type="hidden"  id="loanUid" name="loan_uid" value="<?php echo $loanDetails[$cntAA]->getLoanUid() ?>">
                                  <button type="submit" class="clean edit-anc-btn hover1" name="button"><a><?php echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getDateUpdated()));?></a> </button>
                                  </td>
                                </form>

                              <td style="background-color: pink " class="td">
                                  <!-- <form action="utilities/archiveLoan.php" method="POST"> -->
                                      <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid_archive" value="<?php echo $loanDetails[$cntAA]->getLoanUid();?>">
                                          <img  src="img/archiv0.png" class="edit-announcement-img hover1a" >
                                          <img src="img/archiv3.png" class="edit-announcement-img hover1b" >
                                      </button>
                                  <!-- </form> -->
                              </td>

                          </tr>
                          <?php
                          }}

                        }else {
                          ?>  <td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="70">
                              No Loan Yet. Press "ADD NEW BOOKING" to Add New Booking.
                            </td><?php
                        }
                    //}
                    ?>
                </tbody>
                <td class="total-amt" colspan="10"><b>TOTAL :</b></td>
                <td class="total-amt" class="td"><?php echo number_format($totalSpaPrice,2); ?></td>
                <td class="total-amt" colspan="3"></td>
                <td class="total-amt" class="td"><?php echo number_format($totalNettPrice,2); ?></td>
                <td class="total-amt" class="td"><?php echo number_format($finalTotalDevComm,2); ?></td>
                <td class="total-amt" colspan="17"></td>
                <td class="total-amt" class="td"><?php echo number_format($totalAgentComm,2); ?></td>
                <td class="total-amt" colspan="8"></td>
                <td class="total-amt" class="td"><?php echo number_format($totalUpline1Comm,2); ?></td>
                <td class="total-amt" class="td"><?php echo number_format($totalUpline2Comm,2); ?></td>
                <td class="total-amt" class="td"><?php echo number_format($totalUpline3Comm,2); ?></td>
                <td class="total-amt" class="td"><?php echo number_format($totalUpline4Comm,2); ?></td>
                <td class="total-amt" class="td"><?php echo number_format($totalUpline5Comm,2); ?></td>
                <td class="total-amt" class="td"><?php echo number_format($totalPlOverride,2); ?></td>
                <td class="total-amt" class="td"><?php echo number_format($totalHosOverride,2); ?></td>
                <td class="total-amt" class="td"><?php echo number_format($totalListerOverride,2); ?></td>
                <td class="total-amt" class="td"><?php echo number_format($totalGicProfit,2); ?></td>
                <td class="total-amt" class="td"><?php echo number_format($TotalClaimDev,2); ?></td>
                <td class="total-amt" class="td"><?php echo number_format($totalBalanceUnclaim,2); ?></td>
                <td class="total-amt" colspan="2"></td>
            </table>


    </div>

    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'detailsBox.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Added New Booking. ";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Updated New Booking.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>
<script type="text/javascript">
$(document).ready(function(){
  $("button[name='loan_uid_archive']").click(function(){
    var loanUid = $(this).val();

    $.ajax({
      url: "utilities/archiveLoan.php",
      type: "post",
      data: {loanUid:loanUid},
      dataType: "json",
      success:function(response){

        var unit = response[0]['unit_no'];
        var test = " Has Been Add To Archive";
        // $("#notyElDiv").fadeIn(function(){
          // $("#notyEl").html(unit+test);
          putNoticeJavascriptReload("Notice !!",""+unit+" Has Been Added To Archived");
        // });
        // alert(unit);
        // location.reload();
      }
    });
  });
  $("#excel").click(function(){
    putNoticeJavascript("Notice !!","Downloading Excel File");
  });

  var No = <?php echo count($loanDetails); ?>;
  var upline = 6;
  for (var i = 0; i < No; i++) {
    for (var j = 1; j < upline; j++) {

      $(".upline"+j+"Box"+i+"").click(function(){
        var agentName = $(this).attr("value");
        // alert(agentName);

        $.ajax({
          url : 'utilities/agent2DetailsFunction.php',
          type : 'post',
          data : {agentNames:agentName},
          dataType : 'json',
          success:function(response){
            var agentNamee = response[0]['agentNamed'];
            // alert(agentNamee);
            $("#ss").text(agentNamee);
          },
        });

         $('.hover_bkgr_fricc').fadeIn(function(){
           $("#myDiv").load(location.href + " #myDiv");
           $(this).show();
         });
      });

    }

  }

  $('.hover_bkgr_fricc').click(function(){
      $('.hover_bkgr_fricc').fadeOut(function(){
        $("#mydiv").load(location.href + " #mydiv");
        $(this).hide();
      });
  });
  $('.popupCloseButton').click(function(){
      $('.hover_bkgr_fricc').fadeOut(function(){
        $("#mydiv").load(location.href + " #mydiv");
        $(this).hide();
      });
  });
  $("#projectChoose").on("change",function(){
    var project = $(this).val();
      // alert("sss");
      $.ajax({
        url : 'loanStatusByProject.php',
        type : 'post',
        data : {project:project},
        success:function(data){
          $("#removeAttr").empty();
          $("#removeAttr").html(data);
        }
      });
  });
  // $("#sel_id").on("change",function(){
  //   var project = $(this).val().toLowerCase();
  //   if (project != 'all') {
  //     $("#myFilter tr").filter(function() {
  //       $(this).toggle($(this).text().toLowerCase().indexOf(project) > -1)
  //     });
  //   }else if(project == 'all') {
  //     $("#myFilter tr").show();
  //   }
  // });
    var totalLoanDetails = +$("#totalLoanDetails").val() + +1;

    for (var i = 0; i < totalLoanDetails; i++) {
      $("#agentNameDetails"+i+"").click(function(){
        var agentNameDetails = $(this).attr("value");
        // alert(agentNameDetails);

        $.ajax({
          url : 'utilities/agent2DetailsFunction.php',
          type : 'post',
          data : {agentNames:agentNameDetails},
          dataType : 'json',
          success:function(response){
            var agentNamee = response[0]['agentNamed'];
            // alert(agentNamee);
            $("#ss").text(agentNamee);
          },
        });

         $('.hover_bkgr_fricc').fadeIn(function(){
           $("#myDiv").load(location.href + " #myDiv");
           $(this).show();
         });
      });
    }
});
</script>
<script>
  // $(function(){
  //   $("#myTable").tablesorter( {dateFormat: 'pt'} );
  // });
$(function()
    {
        $("#myTable").tablesorter( {dateFormat: 'pt'} );
    }
);
</script>
