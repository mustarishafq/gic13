<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/timezone.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/PaymentMethod.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$agentList = getUser($conn, "WHERE user_type = 3");
$userName = getLoanStatus($conn, "WHERE loan_uid = ? ",array("loan_uid"),array($_POST['loan_uid']), "s");
$paymentList = getPaymentList($conn);

$loanUid = $_POST['loan_uid'];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Edit | GIC" />
    <title>Edit | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'admin1Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
  <!-- <form method="POST" action="utilities/editProductFunction.php" enctype="multipart/form-data"> -->

	<h1 class="details-h1" onclick="location.href='uploadBookingForm.php';"> <!-- instead use goback() -->
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back3.png" class="back-btn2 hover1b" alt="back" title="back">
        	Name : <?php echo $userName[0]->getPurchaserName(); ?>
        </a>
    </h1>
	<div class="short-red-border"></div>
    <div class="clear"></div>
    <div class="width100 overflow">
        <table>
          <tr>
            <th style="text-align: left">Name</th>
            <th>:</th>
            <th style="text-align: left"><?php echo $userName[0]->getPurchaserName(); ?></th>
          </tr>
          <tr>
            <th style="text-align: left">Unit No.</th>
            <th>:</th>
            <th style="text-align: left"><?php echo $userName[0]->getUnitNo(); ?></th>
          </tr>
          <tr>
            <th style="text-align: left">B. Form</th>
            <th>:</th>
            <th style="text-align: left">
              <?php if (!$userName[0]->getPdf()) {
                echo "No Pdf Found.";
              }else {
                echo $userName[0]->getPdf();
              } ?></th>
          </tr>
        </table>
    </div>

<?php include 'uploadFile.php' ?>


</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
function goBack() {
  window.history.back();
}
</script>
</body>
</html>
