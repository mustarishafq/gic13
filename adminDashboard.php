<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Commission.php';
require_once dirname(__FILE__) . '/classes/IssuePayroll.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

  $advanceDetails = getAdvancedSlip($conn, "WHERE status = 'PENDING' and branch_type = ? ", array("branch_type"),array($_SESSION['branch_type']), "s");
  $commissionDetails = getCommission($conn, "WHERE receive_status = 'PENDING' and branch_type = ? ", array("branch_type"),array($_SESSION['branch_type']), "s");
  $otherCommission = getIssuePayroll($conn, "WHERE receive_status = 'PENDING' and branch_type = ? ", array("branch_type"),array($_SESSION['branch_type']), "s");

if ($otherCommission) {
  $otherCommissionTot = count($otherCommission);
}else {
  $otherCommissionTot = 0;
}
if ($commissionDetails) {
  $commissionDetailsTot = count($commissionDetails);
}else {
  $commissionDetailsTot = 0;
}

$totalCommission = $commissionDetailsTot + $otherCommissionTot;

$loanDetails = getLoanStatus($conn,"WHERE branch_type =?",array("branch_type"),array($_SESSION['branch_type']), "s");

$claim1st = 0;
$claim2nd = 0;
$claim3rd = 0;
$claim4th = 0;
$claim5th = 0;
$totalAdvance = 0;
$totalCom = 0;
$totalOtherCom = 0;
$caseCompleted = 0;
// $invoiceDetails = getInvoice($conn);
 if ($loanDetails) {
  for ($cnt=0; $cnt <count($loanDetails) ; $cnt++) {
    $proDetails = getProject($conn, "WHERE project_name=?",array("project_name"),array($loanDetails[$cnt]->getProjectName()), "s");
    if ($proDetails) {
      if (!$loanDetails[$cnt]->getCheckNo1() && $proDetails[0]->getProjectClaims() >= 1 && $loanDetails[$cnt]->getClaimAmt1st()) {
        $claim1st += 1;
      }
      if (!$loanDetails[$cnt]->getCheckNo2() && $proDetails[0]->getProjectClaims() >= 2 && $loanDetails[$cnt]->getClaimAmt2nd()) {
        $claim2nd += 1;
      }
      if (!$loanDetails[$cnt]->getCheckNo3() && $proDetails[0]->getProjectClaims() >= 3 && $loanDetails[$cnt]->getClaimAmt3rd()) {
        $claim3rd += 1;
      }
      if (!$loanDetails[$cnt]->getCheckNo4() && $proDetails[0]->getProjectClaims() >= 4 && $loanDetails[$cnt]->getClaimAmt4th()) {
        $claim4th += 1;
      }
      if (!$loanDetails[$cnt]->getCheckNo5() && $proDetails[0]->getProjectClaims() >= 5 && $loanDetails[$cnt]->getClaimAmt5th()) {
        $claim5th += 1;
      }
    }
    if ($loanDetails[$cnt]->getCaseStatus() == 'COMPLETED') {
      $caseCompleted += 1;
    }
  }
  $totalInvoice = $claim1st + $claim2nd + $claim3rd + $claim4th + $claim5th;
}
$totalClaims = 0;$totalDevComm = 0;$totalGicProfit = 0;

$UnitSoldDetails = getLoanStatus($conn, "WHERE case_status = 'COMPLETED' AND cancelled_booking != 'YES' ");
if ($UnitSoldDetails) {
  $totalUnitSold = count($UnitSoldDetails);
}else {
  $totalUnitSold = 0;
}
$totalUnitDetails = getLoanStatus($conn, "WHERE cancelled_booking != 'YES' ");
if ($totalUnitDetails) {
  $totalUnit = count($totalUnitDetails);
}else {
  $totalUnit = 0;
}
$agentDetails = getUser($conn, "WHERE user_type = 3 AND status = 'Active'");
if ($agentDetails) {
  $totalAgent = count($agentDetails);
}else {
  $totalAgent = 0;
}
$claimsDetails = getLoanStatus($conn, "WHERE cancelled_booking != 'YES' ");
if ($claimsDetails) {
  for ($i=0; $i <count($claimsDetails) ; $i++) {
    if ($claimsDetails[$i]->getTotalDeveloperComm()) {
      $totalDevComm += $claimsDetails[$i]->getTotalDeveloperComm();
    }
    if ($claimsDetails[$i]->getGicProfit()) {
      $totalGicProfit += $claimsDetails[$i]->getGicProfit();
    }
    if ($claimsDetails[$i]->getClaimAmt1st() && $claimsDetails[$i]->getCheckNo1()) {
      $totalClaims += $claimsDetails[$i]->getClaimAmt1st();
    }
    if ($claimsDetails[$i]->getClaimAmt2nd() && $claimsDetails[$i]->getCheckNo2()) {
      $totalClaims += $claimsDetails[$i]->getClaimAmt1st();
    }
    if ($claimsDetails[$i]->getClaimAmt3rd() && $claimsDetails[$i]->getCheckNo3()) {
      $totalClaims += $claimsDetails[$i]->getClaimAmt1st();
    }
    if ($claimsDetails[$i]->getClaimAmt4th() && $claimsDetails[$i]->getCheckNo4()) {
      $totalClaims += $claimsDetails[$i]->getClaimAmt1st();
    }
    if ($claimsDetails[$i]->getClaimAmt5th() && $claimsDetails[$i]->getCheckNo5()) {
      $totalClaims += $claimsDetails[$i]->getClaimAmt1st();
    }
  }
}
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Dashboard</h1>
    <div class="short-red-border"></div>
	<div class="width100 overflow section-divider">
        <!-- <a href="adminAddNewProject.php"> -->
        <a href="projectPage.php">
            <div class="five-red-btn-div">
                <p id="newProject" class="short-p five-red-p g-first-3-p first-top-p ow-p1">PROJECT PAGE</p>
            </div>
        </a>
        <a href="editInvoiceGeneral.php">
            <div class="five-red-btn-div left-mid-red">
                <p class="short-p five-red-p f-first-3-p ow-p2">ISSUE INVOICE</p>
            </div>
        </a>
        <a href="issuePayroll.php">
            <div class="five-red-btn-div">
                <p class="short-p five-red-p e-first-3-p ow-p3">ISSUE PAYROLL</p>
            </div>
        </a>
        <!-- <a href="invoiceRecord.php">
            <div class="five-red-btn-div right-mid-red">
                <p class="short-p issue-adv-p five-red-p">INVOICE RECORD</p>
            </div>
        </a> -->

        <a href="uploadBookingForm.php">
            <div class="five-red-btn-div right-mid-red">
                <p class="short-p issue-adv-p five-red-p last-bottom-p2 ow-last-2p">BOOKING FORM</p>
            </div>
        </a>
        <a href="invoiceRecord.php">
            <div class="five-red-btn-div">
                <p class="short-p five-red-p issue-adv-p last-bottom-p ow-last-2p2">INVOICE RECORD</p>
            </div>
        </a>

    </div>
    <div class="clear"></div>
    <div class="circle-big-width overflow ow-five-circle">
          <a class="black" href="agentInfo.php">
            <div class="box-inline">
                <div class="square2">
                  <div class="circle-box content2">
                      <p class="big-text"><?php echo $totalAgent ?></p>
                      <p class="small-big-text">&nbsp;</p>
                      <p class="small-big-text">Agents</p>

                  </div>
                </div>
            </div>
          </a>
          <a class="black" href="admin2Product.php">
            <div class="box-inline second-boxx">
                <div class="square2">
                  <div class="circle-box content2">
                      <p class="big-text"><?php echo $totalUnitSold ?></p>
                      <p class="small-big-text">/<?php echo $totalUnit ?></p>
                      <p class="small-big-text">Units Sold</p>

                  </div>
                </div>
            </div>
          </a>
          <a class="black" href="developerComm.php">
            <div class="box-inline third-box-ow">
                <div class="square2">
                  <div class="circle-box content2">
                      <p class="big-text"><?php echo number_format($totalClaims); ?></p>
                      <p class="small-big-text">/<?php echo number_format($totalDevComm) ?></p>
                      <p class="small-big-text">Claims Collected</p>

                  </div>
                </div>
            </div>
          </a>
          <a class="black" href="gicProfit.php">
            <div class="box-inline">
                <div class="square2 mob-second-circle">
                  <div class="circle-box content2">
                      <p class="big-text"><?php echo number_format($totalGicProfit) ?></p>
                      <p class="small-big-text">/<?php echo number_format($totalDevComm) ?></p>
                      <p class="small-big-text">GIC Profit</p>

                  </div>
                </div>
            </div>
          </a>
          <a class="black" href="caseCompleted.php">
            <div class="box-inline ow-last-box">
                <div class="square2">
                  <div class="circle-box content2">
                      <p class="big-text"><?php echo number_format($caseCompleted) ?></p>
                      <p class="small-big-text">/<?php echo number_format($totalUnit) ?></p>
                      <p class="small-big-text">Case Completed</p>

                  </div>
                </div>
            </div>
          </a>



    </div>
    <div class="clear"></div>
  <div class="width100">
    <?php if ($advanceDetails) {
      foreach ($advanceDetails as $advanceDetailsNew) {
        $projectName = $advanceDetailsNew->getProjectName();
        $displayCheck = getProject($conn, "WHERE project_name =? AND display = 'Yes'",array("project_name"),array($projectName), "s");
        if ($displayCheck) {
          $totalAdvance++;
        }} ?>
        <div class="red-dot"><p class="red-dot-p"><?php echo $totalAdvance; ?></p></div>
  <div class="big-rectangle" id="white-big-box">
        <div class="left-side-title">
              <a style="color: black" href="adminAdvanced.php"> <h3 class="rec-h3">Advance</h3></a>
              <div class="short-red-border shorter"></div>
          </div>
          <?php if ($displayCheck) {
          ?>
          <div class="right-side-title">
            <button class="clean show-all-btn red-link advance-a"  onclick="changeClass()">Show All</button>
          </div>
          <div class="clear"></div>
          <!-- repeat this div -->
          <?php for ($cnt=0; $cnt <count($advanceDetails) ; $cnt++) {
              ?><a href="adminAdvanced.php">
                  <div class="detailss-p red-color-hover">
                      <p class="small-date-p"><?php echo date('d/m/Y h:i a', strtotime( $advanceDetails[$cnt]->getDateCreated())); ?></p>
                      <p class="contents-p">
                          <?php echo "Unit No. ".$advanceDetails[$cnt]->getUnitNo() ?> Case Status Completed. Issue advance now.
                      </p>
                  </div>
              </a><?php
            }}}else {
              ?><div class="big-rectangle" id="white-big-box">
                    <div class="left-side-title">
                          <a style="color: black" href="adminAdvanced.php"> <h3 class="rec-h3">Advance</h3></a>
                          <div class="short-red-border shorter"></div>
                      </div>
                      <div class="clear"></div>
                  <!-- </div> -->
                  <?php
            } ?>
      </div>
</div>

<div class="width100">
  <?php if ($commissionDetails || $otherCommission) {
    if ($commissionDetails) {
    foreach ($commissionDetails as $commissionDetailsNew) {
      $projectNameCom = $commissionDetailsNew->getProjectName();
      $displayCheckCom = getProject($conn, "WHERE project_name =? AND display = 'Yes'",array("project_name"),array($projectNameCom), "s");
      if ($displayCheckCom) {
        $totalCom++;
      }
    }
    }
    if ($otherCommission) {
    foreach ($otherCommission as $otherCommissionNew) {
      $projectNameOtherCom = $otherCommissionNew->getProjectName();
      $displayCheckOtherCom = getProject($conn, "WHERE project_name =? AND display = 'Yes'",array("project_name"),array($projectNameOtherCom), "s");
      if ($displayCheckOtherCom) {
        $totalOtherCom++;
      }
    }
    }
    }
      if ($totalCom || $totalOtherCom) {
        ?><div class="red-dot"><p class="red-dot-p"><?php echo $totalCommission; ?></p></div><?php
      }
    ?>
    	<div class="big-rectangle" id="white-big-boxC">
        	<div class="left-side-title">
                <a style="color: black" href="adminCommission.php"> <h3 class="rec-h3">Commission</h3></a>
                <div class="short-red-border shorter"></div>
            </div>
            <?php //if ($commissionDetails || $otherCommission) {
              if ($totalCom) {
              ?>
            <div class="right-side-title">
            	<button class="clean show-all-btn red-link advance-a2"  onclick="changeClassC()">Show All</button>
            </div>
            <div class="clear"></div>
            <!-- repeat this div -->
            <?php for ($cnt=0; $cnt <count($commissionDetails) ; $cnt++) {
              ?><a href="adminCommission.php">
                  <div class="detailss-p red-color-hover">
                      <p class="small-date-p"><?php echo date('d/m/Y h:i a', strtotime($commissionDetails[$cnt]->getDateCreated())) ?></p>
                      <p class="contents-p">
                          Reminder: Commission of <?php echo $commissionDetails[$cnt]->getUpline() ?> shall be give out now.
                      </p>
                  </div>
              </a><?php
            }}
            if ($totalOtherCom) {
              for ($cnt=0; $cnt <count($otherCommission) ; $cnt++) {
                ?><a href="adminCommission.php">
                    <div class="detailss-p red-color-hover">
                        <p class="small-date-p"><?php echo date('d/m/Y h:i a', strtotime($otherCommission[$cnt]->getDateCreated())) ?></p>
                        <p class="contents-p">
                            Reminder: Commission of <?php echo $otherCommission[$cnt]->getProjectHandler() ?> shall be give out now.
                        </p>
                    </div>
                </a><?php
              }
            }
          // } ?>

            <!-- end of repeat this div -->
        </div>
    </div>

	<div class="width100">
    <?php $totalInvoice = $claim1st + $claim2nd + $claim3rd + $claim4th + $claim5th; ?>
    <?php if ($totalInvoice != 0) {
      ?><div class="red-dot"><p class="red-dot-p"><?php echo $totalInvoice ?></p></div><?php
    } ?>

    	<div class="big-rectangle" id="white-big-boxI">
        	<div class="left-side-title">
                <a href="invoiceFollowUp.php"><h3 style="color: black" class="rec-h3">Invoice Follow-up</h3> </a>
                <div class="short-red-border shorter"></div>
            </div>
            <?php if ($totalInvoice != 0) {
            ?><div class="right-side-title">
            	<button class="clean show-all-btn red-link advance-a4"  onclick="changeClassI()">Show All</button>
            </div><?php
          }?>
            <div class="clear"></div>
            <!-- repeat this div -->
            <?php
             if ($loanDetails && $proDetails) {
              for ($cnt=0; $cnt <count($loanDetails) ; $cnt++) {
                $proDetails = getProject($conn, "WHERE project_name=? AND display = 'Yes'",array("project_name"),array($loanDetails[$cnt]->getProjectName()), "s");
                if($proDetails){
                if (!$loanDetails[$cnt]->getCheckNo1() && $proDetails[0]->getProjectClaims() >= 1 && $loanDetails[$cnt]->getClaimAmt1st()) {
                  $claim1st += 1;
                  ?><a href="invoiceFollowUp.php">
                      <div class="detailss-p red-color-hover">
                          <p class="small-date-p"><?php echo date('d/m/Y h:i a', strtotime($loanDetails[$cnt]->getRequestDate1())) ?></p>
                          <p class="contents-p">
                              Reminder: Follow-up Invoice No.<?php echo date('ymd', strtotime($loanDetails[$cnt]->getRequestDate1()))."1".$loanDetails[$cnt]->getId() ?>
                          </p>
                      </div>
                  </a><?php
                }
                if (!$loanDetails[$cnt]->getCheckNo2() && $proDetails[0]->getProjectClaims() >= 2 && $loanDetails[$cnt]->getClaimAmt2nd()) {
                  $claim2nd += 1;
                  ?><a href="invoiceFollowUp.php">
                      <div class="detailss-p red-color-hover">
                        <p class="small-date-p"><?php echo date('d/m/Y h:i a', strtotime($loanDetails[$cnt]->getRequestDate2())) ?></p>
                        <p class="contents-p">
                            Reminder: Follow-up Invoice No.<?php echo date('ymd', strtotime($loanDetails[$cnt]->getRequestDate2()))."2".$loanDetails[$cnt]->getId() ?>
                        </p>
                      </div>
                  </a><?php
                }
                if (!$loanDetails[$cnt]->getCheckNo3() && $proDetails[0]->getProjectClaims() >= 3 && $loanDetails[$cnt]->getClaimAmt3rd()) {
                  $claim3rd += 1;
                  ?><a href="invoiceFollowUp.php">
                      <div class="detailss-p red-color-hover">
                        <p class="small-date-p"><?php echo date('d/m/Y h:i a', strtotime($loanDetails[$cnt]->getRequestDate3())) ?></p>
                        <p class="contents-p">
                            Reminder: Follow-up Invoice No.<?php echo date('ymd', strtotime($loanDetails[$cnt]->getRequestDate3()))."3".$loanDetails[$cnt]->getId() ?>
                        </p>
                      </div>
                  </a><?php
                }
                if (!$loanDetails[$cnt]->getCheckNo4() && $proDetails[0]->getProjectClaims() >= 4 && $loanDetails[$cnt]->getClaimAmt4th()) {
                  $claim4th += 1;
                  ?><a href="invoiceFollowUp.php">
                      <div class="detailss-p red-color-hover">
                        <p class="small-date-p"><?php echo date('d/m/Y h:i a', strtotime($loanDetails[$cnt]->getRequestDate4())) ?></p>
                        <p class="contents-p">
                            Reminder: Follow-up Invoice No.<?php echo date('ymd', strtotime($loanDetails[$cnt]->getRequestDate4()))."4".$loanDetails[$cnt]->getId() ?>
                        </p>
                      </div>
                  </a><?php
                }
                if (!$loanDetails[$cnt]->getCheckNo5() && $proDetails[0]->getProjectClaims() >= 5 && $loanDetails[$cnt]->getClaimAmt5th()) {
                  $claim5th += 1;
                  ?><a href="invoiceFollowUp.php">
                      <div class="detailss-p red-color-hover">
                        <p class="small-date-p"><?php echo date('d/m/Y h:i a', strtotime($loanDetails[$cnt]->getRequestDate5())) ?></p>
                        <p class="contents-p">
                            Reminder: Follow-up Invoice No.<?php echo date('ymd', strtotime($loanDetails[$cnt]->getRequestDate5()))."5".$loanDetails[$cnt]->getId() ?>
                        </p>
                      </div>
                  </a><?php
                }
              }}

            } ?>

            <!-- end of repeat this div -->
        </div>
        <audio id="audio" src="audio/unconvinced.mp3">

        </audio>
    </div>


</div>
<?php
unset($_SESSION['idPro']);
unset($_SESSION['invoice_id']);
unset($_SESSION['idComm']);
unset($_SESSION['loan_uid']);
unset($_SESSION['commission_id']);
unset($_SESSION['idImp']);
?>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login to The System.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Add New Project";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully Add New Booking";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Successfully Updated Profile !";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong Current Password Entered !";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Successfully Issue Payroll !";
        }
        // echo '<script>
        // $("#audio")[0].play();
        // putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        echo '<script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>

<script>
function changeClass() {
   var element = document.getElementById("white-big-box");
   element.classList.toggle("show-height");
}
</script>
<script>
		$(function(){
		   $(".advance-a").click(function () {
			  $(this).text(function(i, text){
				  return text === "Show All" ? "Hide" : "Show All";
			  })
		   });
		})
</script>
<script>
function changeClassC() {
   var element = document.getElementById("white-big-boxC");
   element.classList.toggle("show-height");
}
</script>
<script>
		$(function(){
		   $(".advance-a2").click(function () {
			  $(this).text(function(i, text){
				  return text === "Show All" ? "Hide" : "Show All";
			  })
		   });
		})
</script>
<script>
function changeClassL() {
   var element = document.getElementById("white-big-boxL");
   element.classList.toggle("show-height");
}
</script>
<script>
		$(function(){
		   $(".advance-a3").click(function () {
			  $(this).text(function(i, text){
				  return text === "Show All" ? "Hide" : "Show All";
			  })
		   });
		})
</script>
<script>
function changeClassI() {
   var element = document.getElementById("white-big-boxI");
   element.classList.toggle("show-height");
}
</script>
<script>
		$(function(){
		   $(".advance-a4").click(function () {
			  $(this).text(function(i, text){
				  return text === "Show All" ? "Hide" : "Show All";
			  })
		   });
		})
</script>
</body>
</html>
