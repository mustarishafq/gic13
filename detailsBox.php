<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();
 ?>
<?php if (isset($_SESSION['fullnameNew'])) {

}else {
  $_SESSION['fullnameNew'] = $_SESSION['fullname'];
} ?>
<?php include 'css.php'; ?>
<!-- <a class="trigger_popup_fricc">Click here to show the popup</a> -->

<div class="hover_bkgr_fricc">
    <span class="helper"></span>
    <div>
        <div class="popupCloseButton">&times;</div>
        <div id="myDiv"><?php
        $fullName = rewrite($_SESSION['fullnameNew']); ?>
        <h3 style="float: left" ><?php echo $_SESSION['fullnameNew']; ?></h3>
        <div class="short-red-border"></div>
        <div class="width100 shipping-div2">
        <table class="shipping-table">
          <thead>
            <tr>
                <th class="th0 company-td">NO.</th>
                <th class="th0 company-td1"><?php echo wordwrap("PROJECT NAME",7,"</br>\n");?></th>
                <th class="th0 company-td2"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?></th>
                <th class="th"><?php echo wordwrap("PURCHASER NAME",10,"</br>\n");?></th>
                <!-- <th>STOCK</th> -->
                <th class="th">IC</th>
                <th class="th">CONTACT</th>
                <th class="th">E-MAIL</th>
                <th class="th"><?php echo wordwrap("BOOKING DATE",10,"</br>\n");?></th>
                <th class="th">SQ FT</th>
                <th class="th"><?php echo wordwrap("SPA PRICE",8,"</br>\n");?></th>
                <th class="th">PACKAGE</th>
                <th class="th">DISCOUNT</th>
                <th class="th">REBATE</th>
                <th class="th"><?php echo wordwrap("EXTRA REBATE",10,"</br>\n");?></th>
                <th class="th"><?php echo wordwrap("NETT PRICE",8,"</br>\n");?></th>
                <th class="th"><?php echo wordwrap("TOTAL DEVELOPER COMMISSION",10,"</br>\n");?></th>
                <th class="th">AGENT</th>
                <th class="th"><?php echo wordwrap("LOAN STATUS",10,"</br>\n");?></th>

                <th class="th">REMARK</th>
                <th class="th"><?php echo wordwrap("FORM COLLECTED",10,"</br>\n");?></th>
                <th class="th"><?php echo wordwrap("PAYMENT METHOD",10,"</br>\n");?></th>
                <th class="th">LAWYER</th>
                <th class="th"><?php echo wordwrap("PENDING APPROVAL STATUS",10,"</br>\n");?></th>
                <th class="th"><?php echo wordwrap("BANK APPROVED",10,"</br>\n");?></th>
                <th class="th"><?php echo wordwrap("LO SIGNED DATE",10,"</br>\n");?></th>
                <th class="th"><?php echo wordwrap("LA SIGNED DATE",10,"</br>\n");?></th>
                <th class="th"><?php echo wordwrap("SPA SIGNED DATE",10,"</br>\n");?></th>
                <th class="th"><?php echo wordwrap("FULLSET COMPLETED",10,"</br>\n");?></th>
                <th class="th">CASH BUYER</th>
                <th class="th"><?php echo wordwrap("CANCELLED BOOKING",10,"</br>\n");?></th>
                <th class="th"><?php echo wordwrap("CASE STATUS",10,"</br>\n");?></th>
                <th class="th"><?php echo wordwrap("EVENT PERSONAL",10,"</br>\n");?></th>
                <th class="th">RATE</th>
                <?php
                if ($_SESSION['usertype_level'] == 2) {
                  ?>
                  <th class="th"><?php echo wordwrap("AGENT COMMISSION",10,"</br>\n");?></th>
                  <?php
                }
                 ?>
            </tr>
          </thead>
          <tbody>
              <?php
              // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
              // {
                  $orderDetails = getLoanStatus($conn, "WHERE display = 'Yes' AND cancelled_booking != 'YES' AND agent = ?",array("agent"),array($fullName), "s");
                  if($orderDetails != null)
                  {
                      for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                      {?>
                        <tr>
                            <td class="td company-td"><?php echo ($cntAA+1)?></td>
                            <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getId();?></td> -->
                            <td class="td company-td1"><?php echo wordwrap($orderDetails[$cntAA]->getProjectName(),20,"<br>");?></td>
                            <td class="td company-td2"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getUnitNo());?></td>
                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getPurchaserName());?></td>
                            <!-- <td><?php //echo $orderDetails[$cntAA]->getStock();?></td> -->
                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getIc());?></td>
                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getContact());?></td>

                            <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getEmail());?></td>
                            <td class="td"><?php echo date('d-m-Y', strtotime($orderDetails[$cntAA]->getBookingDate()));?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getSqFt();?></td>

                            <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getSpaPrice();?></td> -->

                            <!-- show , inside value -->
                            <?php $spaPrice = $orderDetails[$cntAA]->getSpaPrice();?>
                            <td class="td"><?php echo $spaPriceValue = number_format($spaPrice, 2); ?></td>

                            <td class="td"><?php echo $orderDetails[$cntAA]->getPackage();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getDiscount();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getRebate();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getExtraRebate();?></td>

                            <!-- <td class="td"><?php //echo $orderDetails[$cntAA]->getNettPrice();?></td>
                            <td class="td"><?php //echo $orderDetails[$cntAA]->getTotalDeveloperComm();?></td> -->

                            <!-- show , inside value -->
                            <?php $nettPrice = $orderDetails[$cntAA]->getNettPrice();?>
                            <td class="td"><?php echo $nettPriceValue = number_format($nettPrice, 2); ?></td>
                            <?php $totalDevComm = $orderDetails[$cntAA]->getTotalDeveloperComm();?>
                            <td class="td"><?php echo $totalDevCommValue = number_format($totalDevComm, 2); ?></td>

                            <td class="td"><?php echo $orderDetails[$cntAA]->getAgent();?></td>
                            <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getRemark();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getBFormCollected();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getPaymentMethod();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getLawyer();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getPendingApprovalStatus();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getBankApproved();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getLoSignedDate();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getLaSignedDate();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getSpaSignedDate();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getFullsetCompleted();?></td>

                            <td class="td"><?php echo $orderDetails[$cntAA]->getCashBuyer();?></td>

                            <td class="td"><?php echo $orderDetails[$cntAA]->getCancelledBooking();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getCaseStatus();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getEventPersonal();?></td>
                            <td class="td"><?php echo $orderDetails[$cntAA]->getRate();?></td>

                            <?php
                            if ($_SESSION['usertype_level'] == 2) {

                              $agentComm = $orderDetails[$cntAA]->getAgentComm();
                              ?>
                              <td class="td"><?php echo $agentCommValue = number_format($agentComm, 2); ?></td>
                              <?php
                            }
                             ?>
                        </tr>
                        <?php
                        }
                    }else {
                      ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="34">No Personal Sales Found.</td> <?php
                    }
              //}
              ?>
          </tbody>
        </table>
      </div>
    </div>
</div>
</div>
<style media="screen">
/* Popup box BEGIN */
.hover_bkgr_fricc{
  background:rgba(0,0,0,.4);
  cursor:pointer;
  display:none;
  height:100%;
  position:fixed;
  text-align:center;
  top:0;
  width:100%;
  z-index:10000;
}
.hover_bkgr_fricc .helper{
  display:inline-block;
  height:100%;
  vertical-align:middle;
}
.hover_bkgr_fricc > div {
  background-color: #fff;
  /* box-shadow: 10px 10px 60px #555; */
  box-shadow: 0 0 0 4px #EE1E1E;
  display: inline-block;
  height: auto;
  vertical-align: middle;
  width: 80%;
  position: relative;
  /* border-radius: 3px; */
  padding: 15px 5%;
}
/* .popupCloseButton {
  background-color: #fff;
  border: 3px solid #999;
  border-radius: 50px;
  cursor: pointer;
  display: inline-block;
  font-family: arial;
  font-weight: bold;
  position: absolute;
  top: -20px;
  right: -20px;
  font-size: 25px;
  line-height: 30px;
  width: 30px;
  height: 30px;
  text-align: center;
} */
.popupCloseButton {
  /* background-color: #fff; */
  /* border: 3px solid #999; */
  border-radius: 50px;
  cursor: pointer;
  display: inline-block;
  font-family: arial;
  font-weight: bold;
  position: absolute;
  top: -5px;
  right: -3px;
  font-size: 25px;
  line-height: 30px;
  width: 30px;
  height: 30px;
}
.popupCloseButton:hover {
  color: red;
}
.trigger_popup_fricc {
  cursor: pointer;
  font-size: 20px;
  margin: 20px;
  display: inline-block;
  font-weight: bold;
}
/* Popup box BEGIN */
.th0 {
    vertical-align: middle;
    z-index: 2;
    background-color: #7babff;
}
.td0{

  text-align: center;
}
</style>
