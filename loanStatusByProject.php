<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Invoice.php';
// require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $projectName = $_POST['project'];
$conn = connDB();
// $projectName = "Quay West";
$projectName = $_POST['project'];
$cntNO = 0;$cntYES = 0;$branchType = $_SESSION['branch_type'];$totalSpaPrice = 0;$totalNettPrice = 0;$finalTotalDevComm = 0;
$totalAgentComm = 0;$totalUpline1Comm = 0;$totalUpline2Comm = 0;$totalUpline3Comm = 0;$totalUpline4Comm = 0;$totalUpline5Comm = 0;
$totalPlOverride = 0;$totalHosOverride = 0;$totalListerOverride = 0;$totalGicProfit = 0;$TotalClaimDev = 0;$totalBalanceUnclaim = 0;

if (!$projectName) {
  if ($branchType == 1) {
      $loanDetails = getLoanStatus($conn,"WHERE display = 'Yes'");
  }else {
      $loanDetails = getLoanStatus($conn,"WHERE display = 'Yes' and branch_type = ?",array("branch_type"),array($_SESSION['branch_type']), "s");
  }
}else{
  if ($branchType == 1) {
      $loanDetails = getLoanStatus($conn,"WHERE display = 'Yes' AND project_name =?",array("project_name"),array($projectName), "s");
  }else {
      $loanDetails = getLoanStatus($conn,"WHERE display = 'Yes' and branch_type = ? AND project_name =?",array("branch_type","project_name"),array($_SESSION['branch_type'],$projectName), "ss");
  }
}

 ?>
<table id="myTable" class="shipping-table">
    <thead>
        <tr>
            <th class="th0 company-td">NO.</th>
            <th class="th0 company-td1">ACTION</th>
            <th class="th0 company-td2"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?></th>
            <th class="th0 company-td3"><?php echo "PROJECT NAME";?></th>
            <th class="th"><?php echo wordwrap("PURCHASER NAME",10,"</br>\n");?></th>
            <th class="th">IC</th>
            <th class="th">CONTACT</th>
            <th class="th">E-MAIL</th>
            <th class="th"><?php echo wordwrap("BOOKING DATE",10,"</br>\n");?></th>
            <th class="th">SQ FT</th>
            <th class="th"><?php echo wordwrap("SPA PRICE",8,"</br>\n");?></th>
            <th class="th">PACKAGE</th>
            <th class="th">DISCOUNT</th>
            <th class="th">REBATE</th>
            <!-- <th class="th"><?php //echo wordwrap("EXTRA REBATE",10,"</br>\n");?></th> -->
            <th class="th"><?php echo wordwrap("NETT PRICE",8,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("TOTAL DEVELOPER COMMISSION",10,"</br>\n");?></th>
            <th class="th">AGENT</th>
            <th class="th"><?php echo wordwrap("LOAN STATUS",10,"</br>\n");?></th>

            <th class="th">REMARK</th>
            <th class="th"><?php echo wordwrap("FORM COLLECTED",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("PAYMENT METHOD",10,"</br>\n");?></th>
            <th class="th">LAWYER</th>
            <!-- <th class="th"><?php //echo wordwrap("PENDING APPROVAL STATUS",10,"</br>\n");?></th> -->
            <th class="th"><?php echo wordwrap("BANK APPROVED",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("LOAN AMOUNT",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("LO SIGNED DATE",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("LA SIGNED DATE",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("SPA SIGNED DATE",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("FULLSET COMPLETED",10,"</br>\n");?></th>
            <th class="th">CASH BUYER</th>
            <th class="th"><?php echo wordwrap("CANCELLED BOOKING",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("CASE STATUS",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("EVENT / PERSONAL",10,"</br>\n");?></th>
            <th class="th">RATE</th>
            <th class="th"><?php echo wordwrap("AGENT COMMISSION",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("UP1 NAME",7,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("UP2 NAME",7,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("UP3 NAME",7,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("UP4 NAME",7,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("UP5 NAME",7,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("PL NAME",5,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("HOS NAME",7,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("LISTER NAME",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("UP1 OVERRIDE",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("UP2 OVERRIDE",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("UP3 OVERRIDE",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("UP4 OVERRIDE",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("UP5 OVERRIDE",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("PL OVERRIDE",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("HOS OVERRIDE",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("LISTER OVERRIDE",10,"</br>\n");?></th>

            <!-- <th class="th"><?php //echo wordwrap("ADMIN1 OVERRIDE",10,"</br>\n");?></th>
            <th class="th"><?php //echo wordwrap("ADMIN2 OVERRIDE",10,"</br>\n");?></th>
            <th class="th"><?php //echo wordwrap("ADMIN3 OVERRIDE",10,"</br>\n");?></th> -->
            <th class="th"><?php echo wordwrap("GIC PROFIT",8,"</br>\n");?></th>
              <th class="th"><?php echo wordwrap("TOTAL CLAIMED DEV AMT",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("TOTAL BALANCED DEV AMT",10,"</br>\n");?></th>
            <th class="th"><?php echo wordwrap("DATE MODIFIED",10,"</br>\n");?></th>
            <!-- <th class="th">BANK APPROVED</th>
            <th class="th">LO SIGNED DATE</th>
            <th class="th">LA SIGNED DATE</th>
            <th class="th">LA SIGNED DATE</th>
            <th class="th">LA SIGNED DATE</th>
            <th class="th">LA SIGNED DATE</th> -->

            <th class="th">ARCHIVE</th>
            <!-- <th>INVOICE</th> -->
        </tr>
    </thead>
    <tbody id="myFilter">
        <?php
            if($loanDetails != null)
            {
                for($cntAA = 0;$cntAA < count($loanDetails) ;$cntAA++)
                {
                  if ($loanDetails[$cntAA]->getCancelledBooking() != 'YES') {
                    $cntNO++;
                    $totalSpaPrice += $loanDetails[$cntAA]->getSpaPrice();
                    $totalNettPrice += $loanDetails[$cntAA]->getNettPrice();
                    $finalTotalDevComm += $loanDetails[$cntAA]->getTotalDeveloperComm();
                    $totalAgentComm += $loanDetails[$cntAA]->getAgentComm();
                    $totalUpline1Comm += $loanDetails[$cntAA]->getUlOverride();
                    $totalUpline2Comm += $loanDetails[$cntAA]->getUulOverride();
                    $totalUpline3Comm += $loanDetails[$cntAA]->getUuulOverride();
                    $totalUpline4Comm += $loanDetails[$cntAA]->getUuuuulOverride();
                    $totalUpline5Comm += $loanDetails[$cntAA]->getUuuuulOverride();
                    $totalPlOverride += $loanDetails[$cntAA]->getPlOverride();
                    $totalHosOverride += $loanDetails[$cntAA]->getHosOverride();
                    $totalListerOverride += $loanDetails[$cntAA]->getListerOverride();
                    $totalGicProfit += $loanDetails[$cntAA]->getGicProfit();
                    $TotalClaimDev += $loanDetails[$cntAA]->getTotalClaimDevAmt();
                    $totalBalanceUnclaim += $loanDetails[$cntAA]->getTotalBalUnclaimAmt();

                  ?>
                <tr>
                    <!-- <td><?php //echo ($cntAA+1)?></td> -->
                    <td class="td company-td"><?php echo $cntNO + $cntYES;?></td>
                    <td class="td company-td1">
                        <form action="editProduct.php" method="POST">
                            <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid" value="<?php echo $loanDetails[$cntAA]->getLoanUid();?>">
                                <img src="img/edit.png" class="edit-announcement-img hover1a" >
                                <img src="img/edit3.png" class="edit-announcement-img hover1b" >
                            </button>
                        </form>
                    </td>
                    <td class="td company-td2"><?php echo wordwrap($loanDetails[$cntAA]->getUnitNo(),10,'<br>');?></td>
                    <td class="td company-td3"><?php echo $loanDetails[$cntAA]->getProjectName();?></td>
                    <td class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getPurchaserName());?></td>
                    <td class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getIc());?></td>
                    <td class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getContact());?></td>

                    <td class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getEmail());?></td>
                    <td class="td"><?php echo date('d-m-Y', strtotime($loanDetails[$cntAA]->getBookingDate()));?></td>
                    <td class="td"><?php
                    if ($loanDetails[$cntAA]->getSqFt()) {
                      echo number_format($loanDetails[$cntAA]->getSqFt());
                    }
                    ?></td>

                    <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getSpaPrice();?></td> -->

                    <!-- show , inside value -->
                    <?php $spaPrice = $loanDetails[$cntAA]->getSpaPrice();?>
                    <td class="td"><?php
                    if ($spaPrice) {
                      echo $spaPriceValue = number_format($spaPrice, 2);
                    }
                     ?></td>

                    <td class="td"><?php echo $loanDetails[$cntAA]->getPackage();?></td>
                    <td class="td"><?php echo $loanDetails[$cntAA]->getRebate();?></td>
                    <td class="td"><?php echo $loanDetails[$cntAA]->getDiscount();?></td>
                    <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getExtraRebate();?></td> -->

                    <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getNettPrice();?></td>
                    <td class="td"><?php //echo $loanDetails[$cntAA]->getTotalDeveloperComm();?></td> -->

                    <!-- show , inside value -->
                    <?php $nettPrice = $loanDetails[$cntAA]->getNettPrice();?>
                    <td class="td"><?php if ($nettPrice) {
                      echo $nettPriceValue = number_format($nettPrice, 2);
                    } ?></td>
                    <?php $totalDevComm = $loanDetails[$cntAA]->getTotalDeveloperComm();?>
                    <td class="td"><?php if ($totalDevComm) {
                      echo $totalDevCommValue = number_format($totalDevComm, 2);
                    } ?></td>

                    <td class="td"><a class="pointer" id="<?php echo "agentNameDetails".($cntNO+$cntYES) ?>" value="<?php echo $loanDetails[$cntAA]->getAgent();?>"><?php echo $loanDetails[$cntAA]->getAgent();?></a> </td>
                    <td class="td"><?php echo wordwrap($loanDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>

                    <td class="td"><?php echo $loanDetails[$cntAA]->getRemark();?></td>
                    <td class="td"><?php echo $loanDetails[$cntAA]->getBFormCollected();?></td>
                    <td class="td"><?php echo $loanDetails[$cntAA]->getPaymentMethod();?></td>
                    <td class="td"><?php echo $loanDetails[$cntAA]->getLawyer();?></td>
                    <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getPendingApprovalStatus();?></td> -->
                    <td class="td"><?php echo $loanDetails[$cntAA]->getBankApproved();?></td>
                    <td class="td"><?php echo $loanDetails[$cntAA]->getLoanAmount();?></td>
                    <td class="td"><?php
                    if ($loanDetails[$cntAA]->getLoSignedDate()) {
                      echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getLoSignedDate()));
                    }
                     ?></td>
                     <td class="td"><?php
                     if ($loanDetails[$cntAA]->getLaSignedDate()) {
                       echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getLaSignedDate()));
                     }
                      ?></td>
                      <td class="td"><?php
                      if ($loanDetails[$cntAA]->getSpaSignedDate()) {
                        echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getSpaSignedDate()));
                      }
                       ?></td>
                    <td class="td"><?php echo $loanDetails[$cntAA]->getFullsetCompleted();?></td>

                    <td class="td"><?php echo $loanDetails[$cntAA]->getCashBuyer();?></td>

                    <td class="td"><?php echo $loanDetails[$cntAA]->getCancelledBooking();?></td>
                    <td class="td"><?php echo $loanDetails[$cntAA]->getCaseStatus();?></td>
                    <td class="td"><?php echo $loanDetails[$cntAA]->getEventPersonal();?></td>
                    <td class="td"><?php echo $loanDetails[$cntAA]->getRate();
                    if ($loanDetails[$cntAA]->getRate()) {
                      echo "%";
                    }?></td>

                    <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getAgentComm();?></td> -->

                    <?php $agentComm = $loanDetails[$cntAA]->getAgentComm();?>
                    <td class="td"><?php if ($agentComm) {
                      echo $agentCommValue = number_format($agentComm, 2);
                    } ?></td>

                    <td class="td"><a id="agentDetailsBox" class="<?php echo "upline1Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline1();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline1(),20,"<br>");?></a></td>
                    <td class="td"><a id="agentDetailsBox" class="<?php echo "upline2Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline2();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline2(),20,"<br>");?></a></td>
                    <td class="td"><a id="agentDetailsBox" class="<?php echo "upline3Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline3();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline3(),20,"<br>");?></a></td>
                    <td class="td"><a id="agentDetailsBox" class="<?php echo "upline4Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline4();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline4(),20,"<br>");?></a></td>
                    <td class="td"><a id="agentDetailsBox" class="<?php echo "upline5Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline5();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline5(),20,"<br>");?></a></td>
                    <td class="td"><?php echo str_replace(",","<br>",$loanDetails[$cntAA]->getPlName());?></td>
                    <td class="td"><?php echo $loanDetails[$cntAA]->getHosName();?></td>
                    <td class="td"><?php echo $loanDetails[$cntAA]->getListerName();?></td>

                    <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getUlOverride();?></td>
                    <td class="td"><?php //echo $loanDetails[$cntAA]->getUulOverride();?></td>
                    <td class="td"><?php //echo $loanDetails[$cntAA]->getPlOverride();?></td>
                    <td class="td"><?php //echo $loanDetails[$cntAA]->getHosOverride();?></td>
                    <td class="td"><?php //echo $loanDetails[$cntAA]->getListerOverride();?></td>
                    <td class="td"><?php //echo $loanDetails[$cntAA]->getAdmin1Override();?></td>
                    <td class="td"><?php //echo $loanDetails[$cntAA]->getAdmin2Override();?></td>
                    <td class="td"><?php //echo $loanDetails[$cntAA]->getGicProfit();?></td>
                    <td class="td"><?php //echo $loanDetails[$cntAA]->getTotalClaimDevAmt();?></td>
                    <td class="td"><?php //echo $loanDetails[$cntAA]->getTotalBalUnclaimAmt();?></td> -->

                    <!-- show , inside value -->
                    <?php $ulOverride = $loanDetails[$cntAA]->getUlOverride();?>
                    <td class="td"><?php if ($ulOverride) {
                      echo $ulOverrideValue = number_format($ulOverride, 2);
                    } ?></td>
                    <?php $uulOverride = $loanDetails[$cntAA]->getUulOverride();?>
                    <td class="td"><?php if ($uulOverride) {
                      echo $uulOverrideValue = number_format($uulOverride, 2);
                    } ?></td>
                    <?php $uuulOverride = $loanDetails[$cntAA]->getUuulOverride();?>
                    <td class="td"><?php if ($uuulOverride) {
                      echo $uuulOverrideValue = number_format($uuulOverride, 2);
                    } ?></td>
                    <?php $uuuulOverride = $loanDetails[$cntAA]->getUuuulOverride();?>
                    <td class="td"><?php if ($uuuulOverride) {
                      echo $uuuulOverrideValue = number_format($uuuulOverride, 2);
                    } ?></td>
                    <?php $uuuuulOverride = $loanDetails[$cntAA]->getUuuuulOverride();?>
                    <td class="td"><?php if ($uuuuulOverride) {
                      echo $uuuuulOverrideValue = number_format($uuuuulOverride, 2);
                    } ?></td>
                    <?php $plOverride = $loanDetails[$cntAA]->getPlOverride();?>
                    <td class="td"><?php if ($plOverride) {
                      echo $plOverrideValue = number_format($plOverride, 2);
                    } ?></td>
                    <?php $hosOverride = $loanDetails[$cntAA]->getHosOverride();?>
                    <td class="td"><?php if ($hosOverride) {
                      echo $hosOverrideValue = number_format($hosOverride, 2);
                    } ?></td>
                    <?php $listerOverride = $loanDetails[$cntAA]->getListerOverride();?>
                    <td class="td"><?php if ($listerOverride) {
                      echo $listerOverrideValue = number_format($listerOverride, 2);
                    } ?></td>
                    <!-- <?php $a1Over = $loanDetails[$cntAA]->getAdmin1Override();?>
                    <td class="td"><?php if ($a1Over) {
                      echo $a3OverValue = number_format($a1Over, 2);
                    } ?></td>
                    <?php $a2Over = $loanDetails[$cntAA]->getAdmin2Override();?>
                    <td class="td"><?php if ($a2Over) {
                      echo $a2OverValue = number_format($a2Over, 2);
                    } ?></td>
                    <?php $a3Over = $loanDetails[$cntAA]->getAdmin3Override();?>
                    <td class="td"><?php if ($a3Over) {
                      echo $a3OverValue = number_format($a3Over, 2);
                    } ?></td> -->
                    <?php $gicProfit = $loanDetails[$cntAA]->getGicProfit();?>
                    <td class="td"><?php if ($gicProfit) {
                      echo $gicProfitValue = number_format($gicProfit, 2);
                    } ?></td>
                    <?php $totalClaim = $loanDetails[$cntAA]->getTotalClaimDevAmt();?>
                    <td class="td"><?php if ($totalClaim) {
                      echo $totalClaimValue = number_format($totalClaim, 2);
                    } ?></td>
                    <?php $totalUnclaim = $loanDetails[$cntAA]->getTotalBalUnclaimAmt();?>
                    <td class="td"><?php if ($totalUnclaim) {
                      echo $totalUnclaimValue = number_format($totalUnclaim, 2);
                    } ?></td>
                    <td id="dateModified" class="td">
                      <form class="" action="historyIndividual.php" method="post">
                        <input type="hidden"  id="loanUid" name="loan_uid" value="<?php echo $loanDetails[$cntAA]->getLoanUid() ?>">
                        <input type="hidden" id="totalLoanDetails" value="<?php echo count($loanDetails) ?>">
                        <button type="submit" class="clean edit-anc-btn hover1" name="button"><a><?php echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getDateUpdated()));?></a> </button>
                        </td>
                      </form>

                    <td class="td">
                        <!-- <form action="utilities/archiveLoan.php" method="POST"> -->
                            <button id="archiveBtn" class="clean edit-anc-btn hover1" type="submit" name="loan_uid_archive" value="<?php echo $loanDetails[$cntAA]->getLoanUid();?>">
                                <img  src="img/archiv0.png" class="edit-announcement-img hover1a" >
                                <img src="img/archiv3.png" class="edit-announcement-img hover1b" >
                            </button>
                        <!-- </form> -->
                    </td>

                </tr>
                <?php
              }}
              for($cntAA = 0;$cntAA < count($loanDetails) ;$cntAA++)
              {
                if ($loanDetails[$cntAA]->getCancelledBooking() == 'YES') {
                  $cntYES++;
                ?>
              <tr>
                  <!-- <td><?php //echo ($cntAA+1)?></td> -->
                  <td class="td company-td" style="background-color: pink;"><?php echo ($cntNO+$cntYES)?></td>
                  <td style="background-color: pink " class="td company-td1">
                      <form action="editProduct.php" method="POST">
                          <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid" value="<?php echo $loanDetails[$cntAA]->getLoanUid();?>">
                              <img src="img/edit.png" class="edit-announcement-img hover1a" >
                              <img src="img/edit3.png" class="edit-announcement-img hover1b" >
                          </button>
                      </form>
                  </td>
                  <td class="td company-td2" style="background-color: pink;"><?php echo wordwrap($loanDetails[$cntAA]->getUnitNo(),10,'<br>');?></td>
                  <td class="td company-td3" style="background-color: pink;"><?php echo $loanDetails[$cntAA]->getProjectName();?></td>
                  <td style="background-color: pink " class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getPurchaserName());?></td>
                  <td style="background-color: pink " class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getIc());?></td>
                  <td style="background-color: pink " class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getContact());?></td>

                  <td style="background-color: pink " class="td"><?php echo str_replace(',',"<br>", $loanDetails[$cntAA]->getEmail());?></td>
                  <td style="background-color: pink " class="td"><?php echo date('d-m-Y', strtotime($loanDetails[$cntAA]->getBookingDate()));?></td>
                  <td style="background-color: pink " class="td"><?php
                  if ($loanDetails[$cntAA]->getSqFt()) {
                    echo number_format($loanDetails[$cntAA]->getSqFt());
                  }
                  ?></td>

                  <!-- <td style="background-color: pink " class="td"><?php //echo $loanDetails[$cntAA]->getSpaPrice();?></td> -->

                  <!-- show , inside value -->
                  <?php $spaPrice = $loanDetails[$cntAA]->getSpaPrice();?>
                  <td style="background-color: pink " class="td"><?php
                  if ($spaPrice) {
                    echo $spaPriceValue = number_format($spaPrice, 2);
                  }
                   ?></td>

                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getPackage();?></td>
                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getRebate();?></td>
                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getDiscount();?></td>
                  <!-- <td style="background-color: pink " class="td"><?php //echo $loanDetails[$cntAA]->getExtraRebate();?></td> -->

                  <!-- <td style="background-color: pink " class="td"><?php //echo $loanDetails[$cntAA]->getNettPrice();?></td>
                  <td style="background-color: pink " class="td"><?php //echo $loanDetails[$cntAA]->getTotalDeveloperComm();?></td> -->

                  <!-- show , inside value -->
                  <?php $nettPrice = $loanDetails[$cntAA]->getNettPrice();?>
                  <td style="background-color: pink " class="td"><?php if ($nettPrice) {
                    echo $nettPriceValue = number_format($nettPrice, 2);
                  } ?></td>
                  <?php $totalDevComm = $loanDetails[$cntAA]->getTotalDeveloperComm();?>
                  <td style="background-color: pink " class="td"><?php if ($totalDevComm) {
                    echo $totalDevCommValue = number_format($totalDevComm, 2);
                  } ?></td>

                  <td style="background-color: pink " class="td"><a class="pointer" id=<?php echo "agentNameDetails".($cntNO+$cntYES) ?> value="<?php echo $loanDetails[$cntAA]->getAgent();?>"><?php echo $loanDetails[$cntAA]->getAgent();?></a> </td>
                  <td style="background-color: pink " class="td"><?php echo wordwrap($loanDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>

                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getRemark();?></td>
                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getBFormCollected();?></td>
                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getPaymentMethod();?></td>
                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getLawyer();?></td>
                  <!-- <td style="background-color: pink " class="td"><?php //echo $loanDetails[$cntAA]->getPendingApprovalStatus();?></td> -->
                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getBankApproved();?></td>
                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getLoanAmount();?></td>
                  <td style="background-color: pink " class="td"><?php if ($loanDetails[$cntAA]->getLoSignedDate()) {
                    echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getLoSignedDate()));
                  };?></td>
                  <td style="background-color: pink " class="td"><?php if ($loanDetails[$cntAA]->getLaSignedDate()) {
                    echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getLaSignedDate()));
                  };?></td>
                  <td style="background-color: pink " class="td"><?php if ($loanDetails[$cntAA]->getSpaSignedDate()) {
                    echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getSpaSignedDate()));
                  };?></td>
<td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getFullsetCompleted();?></td>

                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getCashBuyer();?></td>

                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getCancelledBooking();?></td>
                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getCaseStatus();?></td>
                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getEventPersonal();?></td>
                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getRate();
                  if ($loanDetails[$cntAA]->getRate()) {
                    echo "%";
                  }?></td>

                  <!-- <td style="background-color: pink " class="td"><?php //echo $loanDetails[$cntAA]->getAgentComm();?></td> -->

                  <?php $agentComm = $loanDetails[$cntAA]->getAgentComm();?>
                  <td style="background-color: pink " class="td"><?php if ($agentComm) {
                    echo $agentCommValue = number_format($agentComm, 2);
                  } ?></td>

                  <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline1Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline1();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline1(),20,"<br>");?></a></td>
                  <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline2Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline2();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline2(),20,"<br>");?></a></td>
                  <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline3Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline3();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline3(),20,"<br>");?></a></td>
                  <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline4Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline4();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline4(),20,"<br>");?></a></td>
                  <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline5Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline5();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline5(),20,"<br>");?></a></td>
                  <td style="background-color: pink " class="td"><?php echo str_replace(",","<br>",$loanDetails[$cntAA]->getPlName());?></td>
                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getHosName();?></td>
                  <td style="background-color: pink " class="td"><?php echo $loanDetails[$cntAA]->getListerName();?></td>
                  <?php $ulOverride = $loanDetails[$cntAA]->getUlOverride();?>
                  <td style="background-color: pink " class="td"><?php if ($ulOverride) {
                    echo $ulOverrideValue = number_format($ulOverride, 2);
                  } ?></td>
                  <?php $uUlOverride = $loanDetails[$cntAA]->getUulOverride();?>
                  <td style="background-color: pink " class="td"><?php if ($uUlOverride) {
                    echo $uUlOverrideValue = number_format($uUlOverride, 2);
                  } ?></td>
                  <?php $uuUlOverride = $loanDetails[$cntAA]->getUuulOverride();?>
                  <td style="background-color: pink " class="td"><?php if ($uuUlOverride) {
                    echo $uuUlOverrideValue = number_format($uuUlOverride, 2);
                  } ?></td>
                  <?php $uuuUlOverride = $loanDetails[$cntAA]->getUuuulOverride();?>
                  <td style="background-color: pink " class="td"><?php if ($uuuUlOverride) {
                    echo $uuuUlOverrideValue = number_format($uuuUlOverride, 2);
                  } ?></td>
                  <?php $uuuuUlOverride = $loanDetails[$cntAA]->getUuuuulOverride();?>
                  <td style="background-color: pink " class="td"><?php if ($uuuuUlOverride) {
                    echo $uuuuUlOverrideValue = number_format($uuuuUlOverride, 2);
                  } ?></td>
                  <?php $plOverride = $loanDetails[$cntAA]->getPlOverride();?>
                  <td style="background-color: pink " class="td"><?php if ($plOverride) {
                    echo $plOverrideValue = number_format($plOverride, 2);
                  } ?></td>
                  <?php $hosOverride = $loanDetails[$cntAA]->getHosOverride();?>
                  <td style="background-color: pink " class="td"><?php if ($hosOverride) {
                    echo $hosOverrideValue = number_format($hosOverride, 2);
                  } ?></td>
                  <?php $listerOverride = $loanDetails[$cntAA]->getListerOverride();?>
                  <td style="background-color: pink " class="td"><?php if ($listerOverride) {
                    echo $listerOverrideValue = number_format($listerOverride, 2);
                  } ?></td>
                  <!-- <?php $a1Over = $loanDetails[$cntAA]->getAdmin1Override();?>
                  <td style="background-color: pink " class="td"><?php if ($a1Over) {
                    echo $a3OverValue = number_format($a1Over, 2);
                  } ?></td>
                  <?php $a2Over = $loanDetails[$cntAA]->getAdmin2Override();?>
                  <td style="background-color: pink " class="td"><?php if ($a2Over) {
                    echo $a2OverValue = number_format($a2Over, 2);
                  } ?></td>
                  <?php $a3Over = $loanDetails[$cntAA]->getAdmin3Override();?>
                  <td style="background-color: pink " class="td"><?php if ($a3Over) {
                    echo $a3OverValue = number_format($a3Over, 2);
                  } ?></td> -->
                  <?php $gicProfit = $loanDetails[$cntAA]->getGicProfit();?>
                  <td style="background-color: pink " class="td"><?php if ($gicProfit) {
                    echo $gicProfitValue = number_format($gicProfit, 2);
                  } ?></td>
                  <?php $totalClaim = $loanDetails[$cntAA]->getTotalClaimDevAmt();?>
                  <td style="background-color: pink " class="td"><?php if ($totalClaim) {
                    echo $totalClaimValue = number_format($totalClaim, 2);
                  } ?></td>
                  <?php $totalUnclaim = $loanDetails[$cntAA]->getTotalBalUnclaimAmt();?>
                  <td style="background-color: pink " class="td"><?php if ($totalUnclaim) {
                    echo $totalUnclaimValue = number_format($totalUnclaim, 2);
                  } ?></td>
                  <td style="background-color: pink " id="dateModified" class="td">
                    <form class="" action="historyIndividual.php" method="post">
                      <input type="hidden"  id="loanUid" name="loan_uid" value="<?php echo $loanDetails[$cntAA]->getLoanUid() ?>">
                      <button type="submit" class="clean edit-anc-btn hover1" name="button"><a><?php echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getDateUpdated()));?></a> </button>
                      </td>
                    </form>

                  <td style="background-color: pink " class="td">
                      <!-- <form action="utilities/archiveLoan.php" method="POST"> -->
                          <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid_archive" value="<?php echo $loanDetails[$cntAA]->getLoanUid();?>">
                              <img  src="img/archiv0.png" class="edit-announcement-img hover1a" >
                              <img src="img/archiv3.png" class="edit-announcement-img hover1b" >
                          </button>
                      <!-- </form> -->
                  </td>

              </tr>
              <?php
              }}

            }else {
              ?>  <td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="70">
                  No Loan Yet. Press "ADD NEW BOOKING" to Add New Booking.
                </td><?php
            }
        //}
        ?>
    </tbody>
    <td class="total-amt" colspan="10"><b>TOTAL :</b></td>
    <td class="total-amt" class="td"><?php echo number_format($totalSpaPrice,2); ?></td>
    <td class="total-amt" colspan="3"></td>
    <td class="total-amt" class="td"><?php echo number_format($totalNettPrice,2); ?></td>
    <td class="total-amt" class="td"><?php echo number_format($finalTotalDevComm,2); ?></td>
    <td class="total-amt" colspan="17"></td>
    <td class="total-amt" class="td"><?php echo number_format($totalAgentComm,2); ?></td>
    <td class="total-amt" colspan="8"></td>
    <td class="total-amt" class="td"><?php echo number_format($totalUpline1Comm,2); ?></td>
    <td class="total-amt" class="td"><?php echo number_format($totalUpline2Comm,2); ?></td>
    <td class="total-amt" class="td"><?php echo number_format($totalUpline3Comm,2); ?></td>
    <td class="total-amt" class="td"><?php echo number_format($totalUpline4Comm,2); ?></td>
    <td class="total-amt" class="td"><?php echo number_format($totalUpline5Comm,2); ?></td>
    <td class="total-amt" class="td"><?php echo number_format($totalPlOverride,2); ?></td>
    <td class="total-amt" class="td"><?php echo number_format($totalHosOverride,2); ?></td>
    <td class="total-amt" class="td"><?php echo number_format($totalListerOverride,2); ?></td>
    <td class="total-amt" class="td"><?php echo number_format($totalGicProfit,2); ?></td>
    <td class="total-amt" class="td"><?php echo number_format($TotalClaimDev,2); ?></td>
    <td class="total-amt" class="td"><?php echo number_format($totalBalanceUnclaim,2); ?></td>
    <td class="total-amt" colspan="2"></td>
</table>
<script>
var totalLoanDetails = +$("#totalLoanDetails").val() + +1;

for (var i = 0; i < totalLoanDetails; i++) {
  $("#agentNameDetails"+i+"").click(function(){
    var agentNameDetails = $(this).attr("value");
    // alert(agentNameDetails);

    $.ajax({
      url : 'utilities/agent2DetailsFunction.php',
      type : 'post',
      data : {agentNames:agentNameDetails},
      dataType : 'json',
      success:function(response){
        var agentNamee = response[0]['agentNamed'];
        // alert(agentNamee);
        $("#ss").text(agentNamee);
      },
    });

     $('.hover_bkgr_fricc').fadeIn(function(){
       $("#myDiv").load(location.href + " #myDiv");
       $(this).show();
     });
  });
}
var No = +$("#totalLoanDetails").val() + +1;
var upline = 6;
for (var i = 0; i < No; i++) {
  for (var j = 1; j < upline; j++) {

    $(".upline"+j+"Box"+i+"").click(function(){
      var agentName = $(this).attr("value");
      // alert(agentName);

      $.ajax({
        url : 'utilities/agent2DetailsFunction.php',
        type : 'post',
        data : {agentNames:agentName},
        dataType : 'json',
        success:function(response){
          var agentNamee = response[0]['agentNamed'];
          // alert(agentNamee);
          $("#ss").text(agentNamee);
        },
      });

       $('.hover_bkgr_fricc').fadeIn(function(){
         $("#myDiv").load(location.href + " #myDiv");
         $(this).show();
       });
    });

  }

}

$('.hover_bkgr_fricc').click(function(){
    $('.hover_bkgr_fricc').fadeOut(function(){
      $("#mydiv").load(location.href + " #mydiv");
      $(this).hide();
    });
});
$('.popupCloseButton').click(function(){
    $('.hover_bkgr_fricc').fadeOut(function(){
      $("#mydiv").load(location.href + " #mydiv");
      $(this).hide();
    });
});
$("button[name='loan_uid_archive']").click(function(){
  var loanUid = $(this).val();

  $.ajax({
    url: "utilities/archiveLoan.php",
    type: "post",
    data: {loanUid:loanUid},
    dataType: "json",
    success:function(response){

      var unit = response[0]['unit_no'];
      var test = " Has Been Add To Archive";
      // $("#notyElDiv").fadeIn(function(){
        // $("#notyEl").html(unit+test);
        putNoticeJavascriptReload("Notice !!",""+unit+" Has Been Added To Archived");
      // });
      // alert(unit);
      // location.reload();
    }
  });
});
</script>
