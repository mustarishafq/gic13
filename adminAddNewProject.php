<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE user_type = 3 AND status = 'Active' ORDER BY full_name ASC");

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Add Project | GIC" />
    <title>Add Project | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  a{
    color: maroon;
  }
  input:focus, textarea:focus, select:focus{
    outline: none;
  }
  #addNewProjectBtn{
    color: black;
  }
</style>
<body class="body">
<?php  if ($_SESSION['usertype_level'] == 1) {
  include 'admin1Header.php';
}elseif ($_SESSION['usertype_level'] == 2) {
  include 'admin2Header.php';
} ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Add New Project | <a href="projectList.php" class="project red-color-swicthing">Project List</a> | <a href="projectListArchived.php" class="project red-color-swicthing">Archived</a> </h1>
    <div class="short-red-border"></div>
<div id="AddNewProject" class="overflow margin-top30">
    <form method="POST" action="utilities/addProjectFunction.php">
    <div class="dual-input-div">
        <label class="labelSize">Project Name : <a>*</a></label>
        <input  class="inputSize input-pattern" type="text"  placeholder="Project Name" name="project_name" id="project_name"><br>
	</div>
    <div class="dual-input-div second-dual-input">
        <label class="labelSize">Person Incharge:</label>
        <input  class="inputSize input-pattern" type="text" placeholder="Person Incharge" name="add_by" id="add_by"><br>
	</div>
    <div class="clear"></div>
        <label class="labelSize">Project Leader: <a>*</a></label><img style="cursor: pointer" id="remBtn" width="13px" align="right" src="img/mmm.png"><img style="cursor: pointer" id="addBtn" width="13px" align="right" src="img/ppp.png">
        <select class="inputSize input-pattern" name="project_leader[]">
          <option value="">Select a option</option>
          <?php if ($userDetails) {
            for ($cnt=0; $cnt <count($userDetails) ; $cnt++) {
              ?><option value="<?php echo $userDetails[$cnt]->getFullName() ?>"><?php echo $userDetails[$cnt]->getFullName() ?></option> <?php
            }
          } ?>
        </select> <br>
        <label style="display: none" class="labelSize1">Project Leader: <a>*</a></label>
        <select id="project_leader1" disabled style="display: none" class="inputSize input-pattern" name="project_leader[]">
          <option value="">Select a option</option>
          <?php if ($userDetails) {
            for ($cnt=0; $cnt <count($userDetails) ; $cnt++) {
              ?><option value="<?php echo $userDetails[$cnt]->getFullName() ?>"><?php echo $userDetails[$cnt]->getFullName() ?></option> <?php
            }
          } ?>
        </select>
        <label style="display: none" class="labelSize2">Project Leader: <a>*</a></label>
        <select id="project_leader2" disabled style="display: none" class="inputSize input-pattern" name="project_leader[]">
          <option value="">Select a option</option>
          <?php if ($userDetails) {
            for ($cnt=0; $cnt <count($userDetails) ; $cnt++) {
              ?><option value="<?php echo $userDetails[$cnt]->getFullName() ?>"><?php echo $userDetails[$cnt]->getFullName() ?></option> <?php
            }
          } ?>
        </select>
        <label style="display: none" class="labelSize3">Project Leader: <a>*</a></label>
        <select id="project_leader3" disabled style="display: none" class="inputSize input-pattern" name="project_leader[]">
          <option value="">Select a option</option>
          <?php if ($userDetails) {
            for ($cnt=0; $cnt <count($userDetails) ; $cnt++) {
              ?><option value="<?php echo $userDetails[$cnt]->getFullName() ?>"><?php echo $userDetails[$cnt]->getFullName() ?></option> <?php
            }
          } ?>
        </select>
        <label style="display: none" class="labelSize4">Project Leader: <a>*</a></label>
        <select id="project_leader4" disabled style="display: none" class="inputSize input-pattern" name="project_leader[]">
          <option value="">Select a option</option>
          <?php if ($userDetails) {
            for ($cnt=0; $cnt <count($userDetails) ; $cnt++) {
              ?><option value="<?php echo $userDetails[$cnt]->getFullName() ?>"><?php echo $userDetails[$cnt]->getFullName() ?></option> <?php
            }
          } ?>
        </select>

        <label class="labelSize">Company Name : <a>*</a></label>
        <input  class="inputSize input-pattern" type="text"  placeholder="Company Name" name="company_name" id="project_name"><br>

        <label class="labelSize">Company Address : <a>*</a></label>
        <input  class="inputSize input-pattern" type="text"  placeholder="Line 1" name="company_address[]" id="project_name">
        <input  class="inputSize input-pattern" type="text"  placeholder="Line 2" name="company_address[]" id="project_name">
        <input  class="inputSize input-pattern" type="text"  placeholder="Line 3" name="company_address[]" id="project_name">
        <br>

        <label class="labelSize">No. of Claim Stage: <a>*</a></label>
        <input class="inputSize input-pattern" type="number" placeholder="numbers of claims stages" name="claims_times" id="claims_times"><br>

        <!-- <input type="hidden" name="add_by" id="add_by" value="<?php //echo $userDetails->getFullName(); ?>"> -->

        <button id="noEnterSubmit" class="button" type="submit" name="loginButton">Add Project</button><br>
    </form>
  </div>
<div style="display: none" id="projectList">
<?php $projectDetails = getProject($conn, "WHERE display = 'Yes'"); ?>

    <!-- <h2>Project List</h2> -->
<div class="width100 shipping-div2">
    <table class="shipping-table">
    <tr>
        <th>No.</th>
        <th>Project Name</th>
        <th>Person In Charge (PIC)</th>
        <th>Project Leader</th>
        <th>Company Name</th>
        <th>Company Address</th>
        <th>Project Claim</th>
        <th>Other Claim (RM)</th>
        <!-- <th>Display</th> -->
        <th>Date Created</th>
        <th>Date Updated</th>
        <th>Created</th>
        <th>Action</th>
        <th>Archived</th>
    </tr>
    <?php if ($projectDetails) {

     ?>
    <?php for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {

     ?>
    <tr>
    <?php
    ?>  <td class="td"><?php echo $cnt+1 ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getProjectName() ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getAddProjectPpl() ?></td>
        <td class="td"><?php echo str_replace(",","<br>",$projectDetails[$cnt]->getProjectLeader()); ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getCompanyName() ?></td>
        <td class="td"><?php echo str_replace(",","<br>",$projectDetails[$cnt]->getCompanyAddress()) ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getProjectClaims() ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getOtherClaims() ?></td>
        <!-- <td class="td"><?php //echo $projectDetails[$cnt]->getDisplay() ?></td> -->
        <td class="td"><?php echo date('d-m-Y', strtotime($projectDetails[$cnt]->getDateCreated())) ?></td>
        <td class="td">
          <form class="" action="historyIndividual.php" method="post">
            <input type="hidden" name="id" value="<?php echo $projectDetails[$cnt]->getProjectId() ?>">
            <button class="clean edit-anc-btn" type="submit" name="button"><a id="buttonUpdate" style="color: blue"><?php echo date('d-m-Y', strtotime($projectDetails[$cnt]->getDateUpdated())) ?><a></button></td>
          </form>
        <td class="td"><?php echo $projectDetails[$cnt]->getCreatorName() ?></td>
        <td class="td">  <form action="editProject.php" method="POST">
              <button class="clean edit-anc-btn hover1" type="submit" name="project_id" value="<?php echo $projectDetails[$cnt]->getId();?>">
                  <img src="img/edit.png" class="edit-announcement-img hover1a" >
                  <img src="img/edit3.png" class="edit-announcement-img hover1b" >
              </button>
          </form></td>
          <td class="td">
              <!-- <form action="utilities/archiveLoan.php" method="POST"> -->
                  <button id="archiveBtn" class="clean edit-anc-btn hover1" type="submit" name="loan_uid_archive" value="<?php echo $projectDetails[$cnt]->getId();?>">
                      <img src="img/archiv0.png" class="edit-announcement-img hover1a" >
                      <img src="img/archiv3.png" class="edit-announcement-img hover1b" >
                  </button>
              <!-- </form> -->
          </td>
    </tr>
    <?php
  }}else {
    ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="13"> No Created Project. </td> <?php
  } ?>
    </table>
  </div>
</div>
<div style="display: none" id="projectListArchived">
<?php $projectDetails = getProject($conn, "WHERE display = 'No'"); ?>
    <!-- <h2>Project List</h2> -->
<div class="width100 shipping-div2">
    <table class="shipping-table">
    <tr>
        <th>No.</th>
        <th>Project Name</th>
        <th>Person In Charge (PIC)</th>
        <th>Project Leader</th>
        <th>Company Name</th>
        <th>Company Address</th>
        <th>Project Claim</th>
        <th>Other Claim (RM)</th>
        <!-- <th>Display</th> -->
        <th>Date Created</th>
        <th>Date Updated</th>
        <th>Created</th>
        <th>Action</th>
        <th>Archived</th>
    </tr>
    <?php if ($projectDetails) {

     ?>
    <?php for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {

     ?>
    <tr>
    <?php
    ?>  <td class="td"><?php echo $cnt+1 ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getProjectName() ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getAddProjectPpl() ?></td>
        <td class="td"><?php echo str_replace(",","<br>",$projectDetails[$cnt]->getProjectLeader()); ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getCompanyName() ?></td>
        <td class="td"><?php echo str_replace(",","<br>",$projectDetails[$cnt]->getCompanyAddress()) ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getProjectClaims() ?></td>
        <td class="td"><?php echo $projectDetails[$cnt]->getOtherClaims() ?></td>
        <!-- <td class="td"><?php //echo $projectDetails[$cnt]->getDisplay() ?></td> -->
        <td class="td"><?php echo date('d-m-Y', strtotime($projectDetails[$cnt]->getDateCreated())) ?></td>
        <td class="td">
          <form class="" action="historyIndividual.php" method="post">
            <input type="hidden" name="id" value="<?php echo $projectDetails[$cnt]->getProjectId() ?>">
            <button class="clean edit-anc-btn" type="submit" name="button"><a id="buttonUpdate" style="color: blue"><?php echo date('d-m-Y', strtotime($projectDetails[$cnt]->getDateUpdated())) ?><a></button></td>
          </form>
        <td class="td"><?php echo $projectDetails[$cnt]->getCreatorName() ?></td>
        <td class="td">  <form action="editProject.php" method="POST">
              <button class="clean edit-anc-btn hover1" type="submit" name="project_id" value="<?php echo $projectDetails[$cnt]->getId();?>">
                  <img src="img/edit.png" class="edit-announcement-img hover1a" >
                  <img src="img/edit3.png" class="edit-announcement-img hover1b" >
              </button>
          </form></td>
          <td class="td">
              <!-- <form action="utilities/archiveLoan.php" method="POST"> -->
                  <button id="archiveBtn" class="clean edit-anc-btn hover1" type="submit" name="loan_uid_archive" value="<?php echo $projectDetails[$cnt]->getId();?>">
                      <img src="img/archiv0.png" class="edit-announcement-img hover1a" >
                      <img src="img/archiv3.png" class="edit-announcement-img hover1b" >
                  </button>
              <!-- </form> -->
          </td>
    </tr>
    <?php
  }}else {
    ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="13"> No Archived Project. </td> <?php
  } ?>
    </table>
  </div>
</div>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login to The System. ";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Add New Project.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Existed Project Name.";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
<?php echo '<script src="jsPhp/adminAddNewProject.js" type="text/javascript"></script>'; ?>
</html>
