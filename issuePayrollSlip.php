<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/IssuePayroll.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Address.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$addressDetails = getAddress($conn, "WHERE branch_type = ?",array("branch_type"),array($_SESSION['branch_type']), "s");

if ($addressDetails) {
  $logo = $addressDetails[0]->getLogo();
  $companyName = $addressDetails[0]->getCompanyName();
  $addressNo = $addressDetails[0]->getAddressNo();
  $companyBranch = $addressDetails[0]->getCompanyBranch();
  $companyAddress = $addressDetails[0]->getCompanyAddress();
  $contact = $addressDetails[0]->getContact();
}

if (isset($_SESSION['invoice_id'])) {

  $invoiceDetails = getIssuePayroll($conn, "WHERE invoice_id =?", array("invoice_id"), array($_SESSION['invoice_id']), "s");
  // $invoiceNo = getInvoice($conn, "WHERE id =? ORDER BY id DESC LIMIT 1",array("id"),array($_POST['id']), "s");
  $finalAmount = 0;
  $finalAmountExcludedSst = 0;
  $uid = $_SESSION['uid'];
  $userDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($uid),"s");

  $invoiceNo = date('ymd', strtotime($userDetails[0]->getDateCreated())).$userDetails[0]->getID();
  $requestDate = $invoiceDetails[0]->getBookingDate();

}elseif(isset($_POST['invoice_id_history'])) {

  $invoiceDetails = getIssuePayroll($conn, "WHERE invoice_id =?", array("invoice_id"), array($_POST['invoice_id_history']), "s");
  // $invoiceNo = getInvoice($conn, "WHERE id =? ORDER BY id DESC LIMIT 1",array("id"),array($_POST['id']), "s");
  $finalAmount = 0;
  $finalAmountExcludedSst = 0;
  $uid = $_SESSION['uid'];
  $userDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($uid),"s");

  $invoiceNo = rewrite($_POST['invoice_no']);
  $requestDate = rewrite($_POST['booking_date']);

}
else {

  $invoiceDetails = getIssuePayroll($conn, "WHERE invoice_id =?", array("invoice_id"), array($_POST['invoice_id']), "s");
  // $invoiceNo = getInvoice($conn, "WHERE id =? ORDER BY id DESC LIMIT 1",array("id"),array($_POST['id']), "s");
  $branchType = $invoiceDetails[0]->getBranchType();
  $finalAmount = 0;
  $finalAmountExcludedSst = 0;
  $uid = $_SESSION['uid'];
  $userDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($uid),"s");

  $invoiceNo = rewrite($_POST['invoice_no']);
  $requestDate = rewrite($_POST['booking_date']);

  $addressDetails = getAddress($conn, "WHERE branch_type = ?",array("branch_type"),array($branchType), "s");

  if ($addressDetails) {
    $logo = $addressDetails[0]->getLogo();
    $companyName = $addressDetails[0]->getCompanyName();
    $addressNo = $addressDetails[0]->getAddressNo();
    $companyBranch = $addressDetails[0]->getCompanyBranch();
    $companyAddress = $addressDetails[0]->getCompanyAddress();
    $contact = $addressDetails[0]->getContact();
  }

}

// $invoiceDetails = getIssuePayroll($conn, "WHERE invoice_id =?", array("invoice_id"), array($_POST['invoice_id']), "s");
// // $invoiceNo = getInvoice($conn, "WHERE id =? ORDER BY id DESC LIMIT 1",array("id"),array($_POST['id']), "s");
// $finalAmount = 0;
// $finalAmountExcludedSst = 0;
// $uid = $_SESSION['uid'];
// $userDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($uid),"s");
//
// $invoiceNo = rewrite($_POST['invoice_no']);
// $requestDate = rewrite($_POST['booking_date']);
// $conn->close();
//
function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Other Commission | GIC" />
    <title>Other Commission | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
  <h1 class="h1-title h1-before-border shipping-h1"  onclick="goBack()">
    	<a  class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back3.png" class="back-btn2 hover1b" alt="back" title="back">
        	Other Commission
        </a>
    </h1>

    <!-- This is a filter for the table result -->


    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->

    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="width100 print-div">
      <div class="text-center">
              <img src="<?php echo "logo/".$logo ?>" class="invoice-logo" alt="GIC Consultancy Sdn. Bhd." title="GIC Consultancy Sdn. Bhd.">
              <p id="companyName" class="invoice-address company-name" value="<?php echo $companyName ?>"><b><?php echo $companyName ?></b></p>
              <p id="companyNameInput" class="invoice-address company-name" style="display: none" ><input id="companyNameInput2" type="text" class="input-text" name="" value="<?php echo $companyName ?>"></p>
              <p id="addressNo" class="invoice-small-p"><?php echo $addressNo ?></p>
              <p id="addressNoInput" class="invoice-small-p" style="display: none" ><input id="addressNoInput2" type="text" class="input-text" name="" value="<?php echo $addressNo ?>"></p>

              <?php if ($companyBranch) {
              ?><p id="companyBranch" class="invoice-address"><?php echo $companyBranch ?></p>
              <p id="companyBranchInput" class="invoice-address" style="display: none" ><input id="companyBranchInput2" type="text" class="input-text" name="" value="<?php echo $companyBranch ?>"></p><?php
              } ?>

              <p id="companyAddress" class="invoice-address"><?php echo $companyAddress ?></p>
              <p id="companyAddressInput" class="invoice-address" style="display: none" ><input id="companyAddressInput2" type="text" class="input-text" name="" value="<?php echo $companyAddress ?>"></p>
              <p id="info" class="invoice-address"><?php echo $contact ?></p>
              <p id="infoInput" class="invoice-address" style="display: none" ><input id="infoInput2" type="text" class="input-text" name="" value="<?php echo $contact ?>"></p>
          </div>
		<h1 class="invoice-title">OTHER COMMISSION</h1>
        <div class="invoice-width50 top-invoice-w50">
        	<p class="invoice-p">
            	<b>Attn: <?php echo $invoiceDetails[0]->getProjectHandler(); ?></b>
            </p>
        </div>
        <div class="invoice-width50 top-invoice-w50">
			<table class="invoice-top-small-table">
            	<tr>
                	<td><b>Commission No</b></td>
                    <td><b>:</b></td>
                    <td><b><?php echo $invoiceNo ?></b></td>
                </tr>
                <tr>
                	<td>Project</td>
                    <td>:</td>
                    <td><?php echo $invoiceDetails[0]->getProjectName()  ?></td>
                </tr>
                <tr>
                	<td>Date</td>
                    <td>:</td>
                    <td><?php echo date('d/m/Y',strtotime($requestDate)); ?></td>
                </tr>
            </table>
        </div>
        <div class="clear"></div>
        <table class="invoice-printing-table">
        	<thead>
                    <tr>
                    	<th style="text-align: center">No.</th>
                        <th style="text-align: center">Items</th>
                        <!-- <th >Unit</th> -->
                        <!-- <th >SPA Price (RM)</th> -->
                        <!-- <th >Nett Price (RM)</th> -->
                        <th style="text-align: center">Amount (RM)</th>
                        <!-- <th >Status</th> -->
                    </tr>
            </thead>

                  <?php
                        $itemDetails = $invoiceDetails[0]->getItem();
                        $itemDetailsExplode = explode(",",$itemDetails);
                        // $unitDetails = $invoiceDetails[0]->getUnitNo();
                        // $unitDetailsExplode = explode(",",$unitDetails);
                        $amountDetails = $invoiceDetails[0]->getAmount();
                        $amountDetailsExplode = explode(",",$amountDetails);
                        $finalAmountDetails = $invoiceDetails[0]->getFinalAmount();
                        $finalAmountDetailsExplode = explode(",",$finalAmountDetails);
                        // $claimsDetails = $invoiceDetails[0]->getClaimsStatus();
                        // $claimsDetailsExplode = explode(",",$claimsDetails);


                        for ($cnt=0; $cnt <count($itemDetailsExplode) ; $cnt++) {
                          // $loanDetails = getLoanStatus($conn, "WHERE unit_no = ?", array("unit_no"), array($unitDetailsExplode[$cnt]), "s");
                          // $spaPrice = $loanDetails[0]->getSpaPrice();
                          // $nettPrice = $loanDetails[0]->getNettPrice();
                          ?>

                          <tr>
                            <td style="text-align: center"><?php echo $cnt+1 ?></td>
                            <!-- <td><?php //echo $itemDetailsExplode[$cnt]; ?></td> -->
                            <td style="text-align: center"><?php echo $itemDetailsExplode[$cnt]; ?></td>
                            <!-- <td><?php //echo number_format($spaPrice,2); ?></td> -->
                            <!-- <td><?php //echo number_format($nettPrice,2); ?></td> -->
                            <td style="text-align: center"><?php
                            $finalAmount += $finalAmountDetailsExplode[$cnt];
                            $finalAmountExcludedSst += $amountDetailsExplode[$cnt];
                            if ($invoiceDetails[0]->getCharges() == 'YES') {
                              echo number_format($amountDetailsExplode[$cnt],2);
                            }elseif ($invoiceDetails[0]->getCharges() == 'NO') {
                              $amountExcluded = $amountDetailsExplode[$cnt] / 1.06;
                              echo number_format($amountExcluded,2);
                            }
                             ?></td>
                            <!-- <td><?php //echo $claimsDetailsExplode[$cnt] ?></td> -->
                          </tr>
                          <?php
                        }
                        if (count($itemDetailsExplode) == 1) {
                         ?>
                         <?php for ($cntA=0; $cntA < 4 ; $cntA++) {
                           ?>
                           <tr>
                           <td></td>
                           <td></td>
                           <td></td>
                         </tr> <?php
                         } ?>
                         <?php
                       }
                           if (count($itemDetailsExplode) == 2) {
                            ?>
                            <?php for ($cntA=0; $cntA < 3 ; $cntA++) {
                              ?>
                              <tr>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr> <?php
                            } ?>
                            <?php
                          }
                          if (count($itemDetailsExplode) == 3) {
                           ?>
                           <?php for ($cntA=0; $cntA < 2 ; $cntA++) {
                             ?>
                             <tr>
                             <td></td>
                             <td></td>
                             <td></td>
                           </tr> <?php
                           } ?>
                           <?php
                         }
                         if (count($itemDetailsExplode) == 4) {
                          ?>
                          <?php for ($cntA=0; $cntA < 1 ; $cntA++) {
                            ?>
                            <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr> <?php
                          } ?>
                          <?php
                        }

                   ?>




        </table>
		<div class="clear"></div>
        <div class="invoice-width50 right-w50">
			<table class="invoice-bottom-small-table">
            	<tr>
                	<td>Sum Amount (excluding Service Tax)</td>
                    <td>:</td>
                    <td><?php
                    if ($invoiceDetails[0]->getCharges() == 'YES') {
                      echo number_format($finalAmountExcludedSst,2);
                    }else {
                      $finalAmountExcluded = $finalAmountExcludedSst / 1.06;
                      echo number_format($finalAmountExcluded,2);
                    }
                     ?></td>
                </tr>
                <tr>
                	<td>Service Tax 6%</td>
                    <td>:</td>
                    <td><?php if ($invoiceDetails[0]->getCharges() == 'YES') {
                    echo  number_format( $aa = $finalAmountExcludedSst * 1.06 - $finalAmount ,2);
                  }elseif($invoiceDetails[0]->getCharges() == 'NO') {
                      echo number_format($aa = $finalAmountExcludedSst - $finalAmountExcluded,2);
                    } ?></td>
                </tr>
                <tr>
                	<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                	<td><b>Total Amount</b></td>
                    <td>:</td>
                    <?php if ($invoiceDetails[0]->getCharges()  == 'YES') {
                      ?><td class="bottom-3rd-td border-td"><b><?php echo number_format($finalAmount + $aa, 2) ?></b></td><?php
                    }else{
                      ?><td class="bottom-3rd-td border-td"><b><?php echo number_format($finalAmountExcludedSst, 2) ?></b></td><?php
                    } ?>
                </tr>
            </table>

        </div>

        <div class="invoice-width50 left-w50">
			<table class="invoice-small-table">
            	<tr>
                	<td><b><u>Payee Details:</u></b></td>
                    <td><b>:</b></td>
                    <td></td>
                </tr>
                <tr>
                	<td>Name</td>
                    <td><b>:</b></td>
                    <td><b><?php echo $invoiceDetails[0]->getBankAccountHolder(); ?></b></td>
                </tr>
                <tr>
                	<td>Bank</td>
                    <td><b>:</b></td>
                    <td><b><?php echo $invoiceDetails[0]->getBankName(); ?></b></td>
                </tr>
                <tr>
                	<td>Account No.</td>
                    <td><b>:</b></td>
                    <td><b><?php echo $invoiceDetails[0]->getBankAccountNo(); ?></b></td>
                </tr>
            </table>
        </div>
        <div class="invoice-print-spacing"></div>
        <div class="signature-div">
        	<div class="signature-border"></div>
            <p class="invoice-p"><b>GIC Consultancy Sdn Bhd</b></p>
            <p class="invoice-p">Eddie Song</p>
        </div>
    </div>
	<div class="clear"></div>
  <?php if ($_SESSION['usertype_level'] == 3) {
    ?><div class="dual-button-div width100">
      <a href="#">
            <button style="margin-left: 475px" class="mid-button red-btn clean"  onclick="window.print()">
                Print
            </button>
        </a>
    </div><?php
  }else {
      ?><div class="dual-button-div width100">
        <form action="editIssuePayroll.php" method="post">
          <?php
          if(isset($_POST['selectedCommSlip']) && isset($_POST['headerCheck'])){
            ?><input type="hidden" name="spa_price" value="<?php //echo $spaPrice ?>">
            <input type="hidden" name="nett_price" value="<?php //echo $nettPrice ?>">
            <input type="hidden" name="invoice_no" value="<?php //echo $invoiceNo ?>">
            <input type="hidden" name="idImplode" value="<?php //echo $idImplode ?>">
            <input type="hidden" name="request_date" value="<?php //echo  date('d/m/Y',strtotime($commissionDetails[0]->getDateCreated()))?>">
            <a>
              <button class="left-button1 white-red-line-btn" type="submit" name="id" value="<?php// echo $commissionDetails[0]->getLoanUid(); ?>">
                Edit
              </button>
              </a><?php
          }else {
            ?><input type="hidden" name="invoice_id" value="<?php echo $invoiceDetails[0]->getInvoiceId() ?>">
            <input type="hidden" name="invoice_no" value="<?php echo $invoiceNo ?>">
            <input type="hidden" name="booking_date" value="<?php echo date('d/m/Y',strtotime($invoiceDetails[0]->getBookingDate())) ?>">
            <a>

              <?php if (!isset($_POST['invoice_id_history'])) {
                ?><button class="left-button1 white-red-line-btn" type="submit" name="id" value="<?php //echo $commissionDetails[0]->getID(); ?>">
                  Edit
                </button><?php
              } ?>
              </a><?php
          }
           ?>
        </form>
        <?php if (isset($_POST['invoice_id_history'])) {
          ?><div class="dual-button-div width100">
          	<a href="#">
                  <button style="margin-left: 475px" class="mid-button red-btn clean"  onclick="window.print()">
                      Print
                  </button>
              </a>
          </div><?php
        }else {
          ?>
          <a href="#">
                <button class="mid-button red-btn clean"  onclick="window.print()">
                    Print
                </button>
            </a><?php
        } ?>
          <form class="" action="utilities/sendToAgentOtherCommission.php" method="post">
            <?php if(isset($_POST['selectedCommSlip']) && isset($_POST['headerCheck'])){
              ?><input type="hidden" name="id" value="<?php //echo $idImplode ?>">
              <input type="hidden" name="claim_type" value="<?php //echo $claimType ?>">
              <input type="hidden" name="claim_id" value="<?php //echo $claimId ?>"><?php
            }else {
              ?><input type="hidden" name="invoice_id" value="<?php echo $invoiceDetails[0]->getInvoiceId() ?>">
              <input type="hidden" name="invoice_no" value="<?php echo $invoiceNo ?>">
              <input type="hidden" name="booking_date" value="<?php echo date('d/m/Y',strtotime($invoiceDetails[0]->getBookingDate())) ?>"><?php
            } ?>
            <?php if (!isset($_POST['invoice_id_history'])) {
              ?><button type="submit" name="sendToAgent" class="clean white-red-line-btn right-button2" >
                  Send Commission
              </button><?php
            } ?>
          </form>
      </div><?php
  } ?>



</div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Updated Invoice Details.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
function goBack() {
  window.location.href = 'adminOtherCommission.php';
  // window.history.back();
}
</script>
</body>
</html>
