<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$totalDownline = 0;

$userRefererDetails = getReferralHistory($conn, "WHERE referral_id =?",array("referral_id"),array($uid), "s");
if ($userRefererDetails) {
  $userCurrentLevel = $userRefererDetails[0]->getCurrentLevel(); // user current level
}else {
  $userCurrentLevel = 0;
}

$directDownlineCurrentLevel = $userCurrentLevel + 1;

$getWho = getWholeDownlineTree($conn, $uid, false);
if ($getWho) {
  for ($j=0; $j <count($getWho) ; $j++) {
    if ($getWho[$j]->getCurrentLevel() == $directDownlineCurrentLevel) {
      $totalDownline += 1;
    }
  }
}
// $projectName = "";
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="My Group | GIC" />
    <title>My Group | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<style media="screen">

  #author,#up1,#up2,#up3,#up4,#up5{
    list-style-image: url('img/li.png');
    font-weight: bold;
    font-size: 16px;
  }
  #up6{
    font-weight: bold;
    font-size: 16px;
  }
  li:hover{
    animation-duration: 0.5s;
    animation-name: li;
    color: maroon;
    font-size: 16px;
  }
  @keyframes li {
    0% {font-size: 15px;}
    100% {font-size: 16px;}
  }
</style>
<body class="body">
<?php  include 'agentHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">

  <div class="modal">

  </div>

  <h1 class="h1-title h1-before-border shipping-h1">Downline</h1>
  <div class="short-red-border"></div>
  <div class="section-divider width100 overflow">

    <select id="downlineSelect" class="gap" name="">
      <option value="">Select a dropdown</option>
      <option value="1">Downline 1</option>
      <option value="2">Downline 2</option>
      <option value="3">Downline 3</option>
      <option value="4">Downline 4</option>
      <option value="5">Downline 5</option>
    </select>

    <a class="total-downline">Total Downline : <?php echo $totalDownline ?></a>

  </div>
    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
        <table id="myTable" class="shipping-table">
            <thead>
                <tr>
                    <th class="th">LEVEL</th>
                    <!-- <th class="th">DOWNLINE 2</th>
                    <th class="th">DOWNLINE 3</th>
                    <th class="th">DOWNLINE 4</th>
                    <th class="th">DOWNLINE 5</th> -->
                    <th class="th">FULL NAME</th>
                    <th class="th">NICKNAME</th>
                    <th class="th">POSITION</th>
                    <th class="th">DOWNLINE 1</th>
                    <th class="th">DOWNLINE 2</th>
                    <th class="th">DOWNLINE 3</th>
                    <th class="th">DOWNLINE 4</th>
                    <th class="th">DOWNLINE 5</th>
                </tr>
            </thead>
            <tbody>
                <?php
                  if ($getWho) {
                    for ($i=0; $i <count($getWho) ; $i++) {
                      $fullName = $getWho[$i]->getReferralName();
                      $downlineCurrentLevel = $getWho[$i]->getCurrentLevel();
                      $currentLevel = $downlineCurrentLevel - $userCurrentLevel;
                      $userDetails = getUser($conn, "WHERE full_name =?",array("full_name"),array($fullName), "s");
                      ?>
                      <tr>
                        <td class="td"><?php echo $currentLevel; ?></td>
                        <td class="td"><?php echo $getWho[$i]->getReferralName(); ?></td>
                        <td class="td"><?php echo $userDetails[0]->getUsername(); ?></td>
                        <?php
                        if ($userDetails[0]->getPosition()) {
                          ?><td class="td"><?php echo $userDetails[0]->getPosition(); ?></td><?php
                        }else {
                          ?><td class="td">-</td><?php
                        }
                        $downlineRefererDetails = getReferralHistory($conn, "WHERE referral_id =?",array("referral_id"),array($userDetails[0]->getUid()), "s");
                        if ($downlineRefererDetails) {
                          $userDownlineCurrentLevel = $downlineRefererDetails[0]->getCurrentLevel(); // user current level
                        }else {
                          $userCurrentLevel = 0;
                        }
                        $getWhoII = getWholeDownlineTree($conn, $userDetails[0]->getUid(), false);
                        if ($getWhoII) {
                          ?>
                            <td class="td">
                              <?php
                              for ($j=0; $j <count($getWhoII) ; $j++) {
                                $downlineCurrentLevelII = $getWhoII[$j]->getCurrentLevel();
                                $currentLevelII = $downlineCurrentLevelII - $userDownlineCurrentLevel;
                                if ($currentLevelII == 1) {
                                  echo $getWhoII[$j]->getReferralName()."<br>";
                                }
                              }
                               ?>
                            </td>
                            <td class="td">
                              <?php
                              for ($j=0; $j <count($getWhoII) ; $j++) {
                                $downlineCurrentLevelII = $getWhoII[$j]->getCurrentLevel();
                                $currentLevelII = $downlineCurrentLevelII - $userDownlineCurrentLevel;
                                if ($currentLevelII == 2) {
                                  echo $getWhoII[$j]->getReferralName()."<br>";
                                }
                              }
                               ?>
                            </td>
                            <td class="td">
                              <?php
                              for ($j=0; $j <count($getWhoII) ; $j++) {
                                $downlineCurrentLevelII = $getWhoII[$j]->getCurrentLevel();
                                $currentLevelII = $downlineCurrentLevelII - $userDownlineCurrentLevel;
                                if ($currentLevelII == 3) {
                                  echo $getWhoII[$j]->getReferralName()."<br>";
                                }
                              }
                               ?>
                            </td>
                            <td class="td">
                              <?php
                              for ($j=0; $j <count($getWhoII) ; $j++) {
                                $downlineCurrentLevelII = $getWhoII[$j]->getCurrentLevel();
                                $currentLevelII = $downlineCurrentLevelII - $userDownlineCurrentLevel;
                                if ($currentLevelII == 4) {
                                  echo $getWhoII[$j]->getReferralName()."<br>";
                                }
                              }
                               ?>
                            </td>
                            <td class="td">
                              <?php
                              for ($j=0; $j <count($getWhoII) ; $j++) {
                                $downlineCurrentLevelII = $getWhoII[$j]->getCurrentLevel();
                                $currentLevelII = $downlineCurrentLevelII - $userDownlineCurrentLevel;
                                if ($currentLevelII == 5) {
                                  echo $getWhoII[$j]->getReferralName()."<br>";
                                }
                              }
                               ?>
                            </td>
                          <?php
                        }else {
                          ?><td></td><td></td><td></td><td></td><td></td><?php
                        }
                         ?>
                      </tr>
                      <?php
                    }
                  }
                 ?>
            </tbody>
        </table><br>
    </div>

</body>
</html>
<script>
  $(document).ready(function(){
    $("#downlineSelect").on('change',function(){
      var downlineSelect = $(this).val();
      // alert(downlineSelect);
      $.ajax({
        url: 'myGroupDownline.php',
        data : {downlineSelect:downlineSelect},
        type: 'post',
        success:function(data){
          // alert("sucess");
          $("#myTable").empty();
          $("#myTable").html(data);
        }
        });
      });
      $(document).ajaxStart(function(){
        $(".modal").show();
      });
      $(document).ajaxStop(function(){
        $(".modal").hide();
      });
    });
</script>
