<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/timezone.php';
// require_once dirname(__FILE__) . '/adminAccess1.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/PaymentMethod.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$con = connDB();

// fetch files
$sql = "select pdf from loan_status";
$result = mysqli_query($con, $sql);
?>
<style>
.buttonsss {
  background-color: #991414;
  border: none;
  color: white;
  padding: 10px 32px;
  border-radius: 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}
.support{
  font-size: 12px;
  font-weight: bold;
  color: red;
}
.support:hover{
  font-size: 15px;
  color: red;
}
</style>
<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" >
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
</head>
<body>
<br/>
<!-- <div class="container"> -->
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2 well">
        <form action="uploads.php" method="post" enctype="multipart/form-data">
            <legend>Select PDF File to Upload:</legend><br>
            <div class="form-group">
                <input type="file" name="file1" />
            </div><br>
            <a class="support">* Support Uploaded File pdf ,doc, docx, png, jpg, jpeg.</a><br><br>
            <div class="form-group">
                <input type="submit" class="buttonsss clean" name="submit" value="Upload" class="btn btn-info"/>
            </div>
            <?php if(isset($_GET['st'])) { ?>
                <div class="alert alert-danger text-center">
                <?php if ($_GET['st'] == 'success') {
                        echo "File Uploaded Successfully!";
                    }
                    else
                    {
                        echo 'Invalid File Extension!';
                    } ?>
                </div>
            <?php } ?>
            <input type="hidden" name="loan_uid" value="<?php echo $loanUid ?>">
        </form>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
            <!-- <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>File Name</th>
                        <th>View</th>
                        <th>Download</th>
                    </tr>
                </thead>
                <tbody>
                <?php
              //  $i = 1;
                //while($row = mysqli_fetch_array($result)) { ?>
                <tr>
                    <td><?php //echo $i++; ?></td>
                    <td><?php// echo $row['pdf']; ?></td>
                    <td><a href="uploads/<?php// echo $row['pdf']; ?>" target="_blank">View</a></td>
                    <td><a href="uploads/<?php //echo $row['pdf']; ?>" download>Download</td>
                </tr>
                <?php //} ?>
                </tbody>
            </table> -->
        </div>
    </div>
<!-- </div> -->
</body>
</html>
