<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Invoice.php';
// require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$cntNO = 0;
$cntYES = 0;

// $loanUidRows = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$loanUidRows = getLoanStatus($conn);
$invoiceDetails = getInvoice($conn);
// $projectName = "WHERE case_status = 'COMPLETED'";
$projectName = "";

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Archive Booking | GIC" />
    <title>Archive Booking | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  th{
    cursor: pointer;
  }
  th:hover{
    background-color: maroon;
  }
  th.headerSortUp{
    background-color: black;
  }
  th.headerSortDown{
    background-color: black;
  }
</style>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php echo '<script type="text/javascript" src="js/jquery.tablesorter.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Archived Booking</h1>
    <div class="short-red-border overflow"></div>

    <!-- This is a filter for the table result -->
    <div class="width100 overflow section-divider">
          <a href="adminAddNewProject.php">
              <div class="five-red-btn-div">
                  <p class="short-p five-red-p g-first-3-p n-p archive-new">NEW PROJECT</p>
              </div>
          </a>
          <a href="adminAddNewProduct.php">
              <div class="five-red-btn-div left-mid-red">
                  <p class="short-p five-red-p f-first-3-p a-p">ADD NEW BOOKING</p>
              </div>
          </a>
          <a href="">
          <form method="post" action="export_data.php">
              <button class="five-red-btn-div clean clean-button" type="submit" name="export">
                  <p class="short-p five-red-p e-first-3-p e-p archive-export">EXPORT TO EXCEL</p>
              </button>
          </form>
          </a>
          <a href="admin2Product.php">
              <div class="five-red-btn-div right-mid-red">
                  <p class="short-p five-red-p f-first-3-p a-p">LOAN STATUS</p>
              </div>
          </a>
      </div>
      <div class="clear"></div>
    <div class="section-divider width100 overflow">
      <?php $projectDetails = getProject($conn, "WHERE display = 'Yes'"); ?>

        <select id="sel_id" class="clean-select clean pointer">
          <option value="">Select a project</option>
          <?php if ($projectDetails) {
            for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
              if ($projectDetails[$cnt]->getProjectName() != $types) {
                ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
                }
                }
                ?><option value="ALL">SHOW ALL</option><?php
              } ?>
        <!-- <option value="-1">Select</option>
        <option value="VIDA">kasper </option>
        <option value="VV">adad </option> -->
        <!-- <option value="14">3204 </option> -->
        </select>
    </form>


    </div>

    <div class="clear"></div>

    <div id="removeAttr" class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table id="myTable" class="shipping-table pointer-th">
                <thead style="position: sticky;top: 0;">
                    <tr>
                      <th class="th">NO. <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("PROJECT NAME",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("PURCHASER NAME",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("BOOKING DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("SPA PRICE",8,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("NETT PRICE",8,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th">AGENT</th>
                      <th class="th"><?php echo wordwrap("LOAN STATUS",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("SPA SIGNED DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th">CASH BUYER</th>
                      <th class="th"><?php echo wordwrap("CANCELLED BOOKING",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("EVENT / PERSONAL",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th">RATE</th>
                      <th class="th"><?php echo wordwrap("UP1 NAME",5,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("UP2 NAME",5,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("UP3 NAME",5,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("UP4 NAME",5,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("UP5 NAME",5,"</br>\n");?> <img src="img/sort.png" class="sort"></th>

                      <th class="th"><?php echo wordwrap("PL NAME",5,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("HOS NAME",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                      <th class="th"><?php echo wordwrap("DATE MODIFIED",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <!-- <th class="th">BANK APPROVED</th>
                        <th class="th">LO SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th>
                        <th class="th">LA SIGNED DATE</th> -->

                        <th class="th">ACTION  <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("UN - ARCHIVED",10,"</br>\n");?>  <img src="img/sort.png" class="sort"></th>
                        <!-- <th>INVOICE</th> -->
                    </tr>
                </thead>
                <tbody id="myFilter">
                    <?php
                    // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                    // {
                        $orderDetails = getLoanStatus($conn,"WHERE display = 'No' and branch_type =?",array("branch_type"),array($_SESSION['branch_type']), "s");
                        if($orderDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                            {
                              if ($orderDetails[$cntAA]->getCancelledBooking() != 'YES') {
                                $cntNO++;
                              ?>
                            <tr>
                                <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                <td class="td"><?php echo ($cntNO+$cntYES)?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                                <td class="td"><?php
                                $a = $orderDetails[$cntAA]->getPurchaserName();
                                 echo str_replace(',',"<br>", $a) ;?></td>
                                <!-- <td><?php //echo $orderDetails[$cntAA]->getStock();?></td> -->
                                <td class="td"><?php echo date('d-m-Y', strtotime($orderDetails[$cntAA]->getBookingDate()));?></td>
                                <?php $spaPrice = $orderDetails[$cntAA]->getSpaPrice();?>
                                <td class="td"><?php echo $spaPriceValue = number_format($spaPrice, 2); ?></td>
                                <?php $nettPrice = $orderDetails[$cntAA]->getNettPrice();?>
                                <td class="td"><?php echo $nettPriceValue = number_format($nettPrice, 2); ?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getAgent();?></td>
                                <td class="td"><?php echo wordwrap($orderDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>
                                <td class="td"><?php echo date('d-m-Y',strtotime($orderDetails[$cntAA]->getSpaSignedDate()));?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getCashBuyer();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getCancelledBooking();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getEventPersonal();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getRate();
                                if ($orderDetails[$cntAA]->getRate()) {
                                  echo "%";
                                }?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUpline1();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUpline2();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUpline3();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUpline4();?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getUpline5();?></td>
                                <td class="td"><?php echo str_replace(",","<br>",$orderDetails[$cntAA]->getPlName());?></td>
                                <td class="td"><?php echo $orderDetails[$cntAA]->getHosName();?></td>
                                <td id="dateModified" class="td">
                                  <form class="" action="historyIndividual.php" method="post">
                                    <input type="hidden"  id="loanUid" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid() ?>">
                                    <button type="submit" class="clean edit-anc-btn hover1" name="button"><a><?php echo date('d-m-Y',strtotime($orderDetails[$cntAA]->getDateUpdated()));?></a> </button>
                                    </td>
                                  </form>

                                <td class="td">
                                    <form action="editProduct.php" method="POST">
                                        <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">
                                            <img src="img/edit.png" class="edit-announcement-img hover1a" >
                                            <img src="img/edit3.png" class="edit-announcement-img hover1b" >
                                        </button>
                                    </form>
                                </td>
                                <td class="td">
                                    <!-- <form action="utilities/archiveLoan.php" method="POST"> -->
                                        <button id="archiveBtn" class="clean edit-anc-btn hover1" type="submit" name="loan_uid_archive" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">
                                            <img  src="img/archiv0.png" class="edit-announcement-img hover1a" >
                                            <img src="img/archiv3.png" class="edit-announcement-img hover1b" >
                                        </button>
                                    <!-- </form> -->
                                </td>

                            </tr>
                            <?php
                          }}
                          for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                          {
                            if ($orderDetails[$cntAA]->getCancelledBooking() == 'YES') {
                              $cntYES++;
                            ?>
                          <tr>
                              <!-- <td><?php //echo ($cntAA+1)?></td> -->
                              <td style="background-color: pink;" class="td red"><?php echo ($cntNO+$cntYES)?></td>
                              <td style="background-color: pink;" class="td red"><?php echo $orderDetails[$cntAA]->getProjectName();?></td>
                              <td style="background-color: pink;" class="td red"><?php echo $orderDetails[$cntAA]->getUnitNo();?></td>
                              <td style="background-color: pink;" class="td red"><?php
                              $a = $orderDetails[$cntAA]->getPurchaserName();
                               echo str_replace(',',"<br>", $a) ;?></td>
                              <!-- <td><?php //echo $orderDetails[$cntAA]->getStock();?></td> -->
                              <td style="background-color: pink;" class="td red"><?php echo date('d-m-Y', strtotime($orderDetails[$cntAA]->getBookingDate()));?></td>
                              <?php $spaPrice = $orderDetails[$cntAA]->getSpaPrice();?>
                              <td style="background-color: pink;" class="td red"><?php echo $spaPriceValue = number_format($spaPrice, 2); ?></td>
                              <?php $nettPrice = $orderDetails[$cntAA]->getNettPrice();?>
                              <td style="background-color: pink;" class="td red"><?php echo $nettPriceValue = number_format($nettPrice, 2); ?></td>
                              <td style="background-color: pink;" class="td red"><?php echo $orderDetails[$cntAA]->getAgent();?></td>
                              <td style="background-color: pink;" class="td red"><?php echo wordwrap($orderDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>
                              <td style="background-color: pink;" class="td red"><?php echo date('d-m-Y',strtotime($orderDetails[$cntAA]->getSpaSignedDate()));?></td>
                              <td style="background-color: pink;" class="td red"><?php echo $orderDetails[$cntAA]->getCashBuyer();?></td>
                              <td style="background-color: pink;" class="td red"><?php echo $orderDetails[$cntAA]->getCancelledBooking();?></td>
                              <td style="background-color: pink;" class="td red"><?php echo $orderDetails[$cntAA]->getEventPersonal();?></td>
                              <td style="background-color: pink;" class="td red"><?php echo $orderDetails[$cntAA]->getRate();
                              if ($orderDetails[$cntAA]->getRate()) {
                                echo "%";
                              }?></td>
                              <td style="background-color: pink;" class="td red"><?php echo $orderDetails[$cntAA]->getUpline1();?></td>
                              <td style="background-color: pink;" class="td red"><?php echo $orderDetails[$cntAA]->getUpline2();?></td>
                              <td style="background-color: pink;" class="td red"><?php echo $orderDetails[$cntAA]->getPlName();?></td>
                              <td style="background-color: pink;" class="td red"><?php echo $orderDetails[$cntAA]->getHosName();?></td>
                              <td style="background-color: pink " id="dateModified" class="td">
                                <form class="" action="historyIndividual.php" method="post">
                                  <input type="hidden"  id="loanUid" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid() ?>">
                                  <button type="submit" class="clean edit-anc-btn hover1" name="button"><a><?php echo date('d-m-Y',strtotime($orderDetails[$cntAA]->getDateUpdated()));?></a> </button>
                                  </td>
                                </form>

                              <td style="background-color: pink " class="td">
                                  <form action="editProduct.php" method="POST">
                                      <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">
                                          <img src="img/edit.png" class="edit-announcement-img hover1a" >
                                          <img src="img/edit3.png" class="edit-announcement-img hover1b" >
                                      </button>
                                  </form>
                              </td>
                              <td style="background-color: pink " class="td">
                                  <!-- <form action="utilities/archiveLoan.php" method="POST"> -->
                                      <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid_archive" value="<?php echo $orderDetails[$cntAA]->getLoanUid();?>">
                                          <img src="img/restore.png" class="edit-announcement-img hover1a" >
                                          <img src="img/restore2.png" class="edit-announcement-img hover1b" >
                                      </button>
                                  <!-- </form> -->
                              </td>

                          </tr>
                          <?php
                          }}

                        }else {
                          ?>  <td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="70">
                              No Archived Booking Found.
                            </td><?php
                        }
                    //}
                    ?>
                </tbody>
            </table><br>


    </div>

    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php// include 'jsAdmin.php'; ?>
<?php include 'js.php'; ?>
</body>
</html>
<script type="text/javascript">
  $(document).ready(function(){
    $("button[name='loan_uid_archive']").click(function(){
      var loanUid = $(this).val();

      $.ajax({
        url: "utilities/archiveLoan.php",
        type: "post",
        data: {loanUid:loanUid},
        dataType: "json",
        success:function(response){

          var unit = response[0]['unit_no'];
          var test = " Has Been Add To Archive";
          // $("#notyElDiv").fadeIn(function(){
            // $("#notyEl").html(unit+test);
                putNoticeJavascriptReload("Notice !!",""+unit+" Successfully Been Restored.");
          // });
          // alert(unit);
          // location.reload();
        }
      });
    });
  });
</script>
<script>
    $(document).ready( function () {
            $("#myTable").tablesorter( {dateFormat: 'pt'} );
    });
</script>
