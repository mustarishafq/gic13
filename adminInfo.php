<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/EditHistory.php';
require_once dirname(__FILE__) . '/classes/Branch.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
// $_SESSION['usernameNew'] = "";
$projectName = "";
// $sql = "select pdf from loan_status";
// $result = mysqli_query($conn, $sql);
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Admin Info | GIC" />
    <title>Admin Info | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style>
.aaa{
  color: maroon;
}
/* a:not(:hover){
  color: maroon;
} */
.th0 {
    vertical-align: middle;
    text-align: center;
    z-index: 2;
    background-color: #7babff;
}
.company-td{
  position:sticky;
  width: 60px;
  min-width: 60px;
  max-width: 60px;
  left:0;
}
.company-td1{
  position:sticky;
  width: 80px;
  min-width: 80px;
  max-width: 80px;
  left:60px;
}
.company-td2{
  position:sticky;
  width: 140px;
  min-width: 140px;
  max-width: 140px;
  left:200px;
}
#agentDetailsBox{
  cursor: pointer;
}
#agentDetailsBox:hover{
  cursor: pointer;
  color: red;
  text-decoration: none;
}
.th:hover, .th0:hover{
  background-color: maroon;
}
.th.headerSortUp , .th0.headerSortUp{
  background-color: maroon;
}
.th.headerSortDown , .th0.headerSortDown{
  background-color: maroon;
}
</style>
<body class="body">
<?php  include 'admin1Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<?php echo '<script type="text/javascript" src="js/jquery.tablesorter.js"></script>'; ?>
<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Admin Info | <a class="aaa" href="agentInfo.php">Agent Info</a> </h1>
    <div class="short-red-border"></div>
    <div class="section-divider width100 overflow">
		<?php $projectDetails = getProject($conn, "WHERE display = 'Yes'"); ?>


        <input class="clean search-btn-left" type="text" name="" value="" placeholder="Search.." id="searchInput">
      <!-- <label>Start Date :</label> <input class="clean-select clean pointer" type="date" name="" value="">
      <label>End Date :</label> <input class="clean-select clean pointer" type="date" name="" value=""> -->
    </div>


    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table id="myTable" class="shipping-table pointer-th">
                <thead>
                    <tr>
                        <th class="th0 company-td">NO. <img src="img/sort.png" class="sort"></th>
                        <th class="th0 company-td1">ACTION <img src="img/sort.png" class="sort"></th>
                        <th class="th">STATUS <img src="img/sort.png" class="sort"></th>
                        <th class="th">ADMIN FULLNAME <img src="img/sort.png" class="sort"></th>
                        <th class="th">COMPANY BRANCH <img src="img/sort.png" class="sort"></th>
                        <!-- <th class="th">DATE CREATED</th> -->
                    </tr>
                </thead>
                <tbody id="myFilter">
                    <?php
                    // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                    // {
                        $adminDetails = getUser($conn, "WHERE user_type = 1 or user_type = 2 ORDER BY date_created DESC" );
                        // $adminDetails = getLoanStatus($conn);
                        if($adminDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($adminDetails) ;$cntAA++){
                              $branch = getBranch($conn,"WHERE branch_type =?",array("branch_type"),array($adminDetails[$cntAA]->getBranchType()), "s");
                              $companyBranch = $branch[0]->getBranchName();
                                ?>
                              <tr>
                                  <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                  <td class="td company-td"><?php echo $cntAA + 1?></td>
                                  <td class="td company-td1">
                                      <form action="editAdmin.php" method="POST">
                                        <button class="edit-btn" type="submit" name="id" value="<?php echo $adminDetails[$cntAA]->getId();?>">
                                          Edit
                                          <!-- <button class="clean edit-anc-btn hover1" type="submit" name="id" value="<?php //echo $adminDetails[$cntAA]->getId();?>"> -->
                                              <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" >
                                              <img src="img/edit3.png" class="edit-announcement-img hover1b" > -->
                                          </button>
                                      </form>
                                  </td>
                                  <td class="td"><?php echo $adminDetails[$cntAA]->getStatus() ?></td>
                                  <td class="td"><?php echo wordwrap($adminDetails[$cntAA]->getFullName(),20,"<br>");?></td>
                                  <td class="td"><?php echo $companyBranch ?></td>
                                  <!-- <td class="td"><?php //echo date('d-m-Y',strtotime($adminDetails[$cntAA]->getDateCreated())) ?></td> -->
                              </tr>
                              <?php


                            }

                    }
                    ?>
                </tbody>
            </table><br>


    </div>


    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php include 'detailsBox.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Added New Agent. ";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Edit Agent Details.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
 ?>
</body>
<script type="text/javascript">
$(window).load(function () {
  var No = <?php echo count($adminDetails); ?>;
  for (var i = 0; i < No; i++) {
    $(".agentDetailsBox"+i+"").click(function(){
      var agentName = $(this).attr("value");
      // alert(agentName);

      $.ajax({
        url : 'utilities/agentDetailsFunction.php',
        type : 'post',
        data : {agentNames:agentName},
        dataType : 'json',
        success:function(response){
          var agentNamee = response[0]['agentNamed'];
          // alert(agentNamee);
          $("#ss").text(agentNamee);
        },
        error:function(response){
          // alert("failed");
        }
      });

       $('.hover_bkgr_fricc').fadeIn(function(){
         $("#myDiv").load(location.href + " #myDiv");
         $(this).show();
       });
    });
  }

  $('.hover_bkgr_fricc').click(function(){
      $('.hover_bkgr_fricc').fadeOut(function(){
        $("#mydiv").load(location.href + " #mydiv");
        $(this).hide();
      });
  });
  $('.popupCloseButton').click(function(){
      $('.hover_bkgr_fricc').fadeOut(function(){
        $("#mydiv").load(location.href + " #mydiv");
        $(this).hide();
      });
  });
});
</script>
<script>
    $(document).ready( function () {
            $("#myTable").tablesorter( {dateFormat: 'pt'} );
    });
</script>
</html>
