<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/Commission.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Address.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
// require_once dirname(__FILE__) . '/utilities/convertingRinggit.php';
require_once dirname(__FILE__) . '/utilities/ringgitMalaysia.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$finalAmountFinal = 0;

$addressDetails = getAddress($conn, "WHERE branch_type = ?",array("branch_type"),array($_SESSION['branch_type']), "s");

if ($addressDetails) {
  $logo = $addressDetails[0]->getLogo();
  $companyName = $addressDetails[0]->getCompanyName();
  $addressNo = $addressDetails[0]->getAddressNo();
  $companyBranch = $addressDetails[0]->getCompanyBranch();
  $companyAddress = $addressDetails[0]->getCompanyAddress();
  $contact = $addressDetails[0]->getContact();
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
if (isset($_POST['idImplode'])) {
  $idImplode = $_POST['idImplode'];
  $idExplode = explode(",",$idImplode);
  $claimType = 'Multi';
  $claimId = md5(uniqid());
  for ($cnt=0; $cnt <count($idExplode) ; $cnt++) {
  $commissionDetails = getCommission($conn, "WHERE id = ?",array("id"),array($idExplode[$cnt]),"s");
  $uplineName = $commissionDetails[0]->getUpline();
  $uplineNameArray[] = $uplineName;
}
if ($uplineNameArray) {
  $uplineNameLatest = implode(",",$uplineNameArray);
  $uplineNameLatestExplode = explode(",",$uplineNameLatest);
  for ($cnt=0; $cnt <count($uplineNameLatestExplode) ; $cnt++) {
  $cntNotAdd = $cnt;
    $cntADD = $cnt+1;
    if (isset($uplineNameLatestExplode[$cntADD])) {
      if ($uplineNameLatestExplode[$cntNotAdd] != $uplineNameLatestExplode[$cntADD]) {
        $_SESSION['messageType'] = 1;
        header('location: ./adminCommission.php?type=7');
      }
    }

  }


}
}elseif (isset($_POST['headerCheck']) && !isset($_POST['selectedCommSlip'])) {
  $_SESSION['messageType'] = 1;
  header('location: ./adminCommission.php?type=6');
}
else {
  $commissionDetails = getCommission($conn, "WHERE id =?",array("id"),array($_POST['id']), "s");

  $userDetails = getUser($conn, "WHERE full_name = ? ", array("full_name"), array($commissionDetails[0]->getUpline()), "s");

  $loanDetails = getLoanStatus($conn, "WHERE loan_uid =?", array("loan_uid"), array($commissionDetails[0]->getLoanUid()), "s");
  $nettPrice = $loanDetails[0]->getNettPrice();
  // $commissionDetails = getInvoice($conn, "WHERE id =? ORDER BY id DESC LIMIT 1",array("id"),array($_POST['invoice']), "s");
  $id = $_POST['id'];
  $claimType = 'Single';
  $claimId = md5(uniqid());
}

// $projectName = $_POST['project_name'];
$status = 'COMPLETED';

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Commission | GIC" />
    <title>Commission | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  input,select{
    border-top-style: hidden;
     border-right-style: hidden;
     border-left-style: hidden;
     border-bottom-style: groove;
     /* background-color: #eee; */
  }
  input:focus, textarea:focus, select:focus{
      outline: none;
  }
</style>
<body class="body">

<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1"   onclick="goBack()">
    	<a  class="black-white-link2 hover1">
    	    <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back3.png" class="back-btn2 hover1b" alt="back" title="back">
        	Commission
        </a>
    </h1>
    <div class="spacing-left short-red-border"></div>
    <!-- This is a filter for the table result -->


    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->

    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="width100 print-div">
      <div class="text-center">
              <img src="<?php echo "logo/".$logo ?>" class="invoice-logo" alt="GIC Consultancy Sdn. Bhd." title="GIC Consultancy Sdn. Bhd.">
              <p id="companyName" class="invoice-address company-name" value="<?php echo $companyName ?>"><b><?php echo $companyName ?></b></p>
              <p id="companyNameInput" class="invoice-address company-name" style="display: none" ><input id="companyNameInput2" type="text" class="input-text" name="" value="<?php echo $companyName ?>"></p>
              <p id="addressNo" class="invoice-small-p"><?php echo $addressNo ?></p>
              <p id="addressNoInput" class="invoice-small-p" style="display: none" ><input id="addressNoInput2" type="text" class="input-text" name="" value="<?php echo $addressNo ?>"></p>

              <?php if ($companyBranch) {
              ?><p id="companyBranch" class="invoice-address"><?php echo $companyBranch ?></p>
              <p id="companyBranchInput" class="invoice-address" style="display: none" ><input id="companyBranchInput2" type="text" class="input-text" name="" value="<?php echo $companyBranch ?>"></p><?php
              } ?>

              <p id="companyAddress" class="invoice-address"><?php echo $companyAddress ?></p>
              <p id="companyAddressInput" class="invoice-address" style="display: none" ><input id="companyAddressInput2" type="text" class="input-text" name="" value="<?php echo $companyAddress ?>"></p>
              <p id="info" class="invoice-address"><?php echo $contact ?></p>
              <p id="infoInput" class="invoice-address" style="display: none" ><input id="infoInput2" type="text" class="input-text" name="" value="<?php echo $contact ?>"></p>
          </div>
		<h1 class="invoice-title">COMMISSION / ALLOWANCE</h1>
    <form class="" action="utilities/editCommissionSlipFunction.php" method="post">
       <div class="invoice-width50 top-invoice-w50">
			<table class="invoice-top-small-table left-table">
            	<tr>
                	<td>Date</td>
                    <td>:</td>
                    <td><?php if ($commissionDetails[0]->getBookingDate()) {
                      ?><input type="date" name="request_date" value="<?php echo date('Y-m-d',strtotime($commissionDetails[0]->getBookingDate())); ?>"> <?php
                    }else {
                      ?><input type="date" name="request_date" value="<?php echo date('Y-m-d'); ?>"> <?php
                    } ?></td>
                </tr>
                <tr>
                	<td>Pay to</td>
                    <td>:</td>
                    <td><?php
                    ?><input type="text" name="upline" value="<?php echo $commissionDetails[0]->getUpline() ?>"> <?php
                      ?></td>
                </tr>
            </table>
        </div>
        <div class="invoice-width50 top-invoice-w50">
			<table class="invoice-top-small-table">
            	<tr>
                	<td>Payslip No</td>
                    <td>:</td>
                    <td><?php
                    if (isset($_POST['idImplode'])) {
                      if ($commissionDetails[0]->getDateCreated()) {
                        echo date('ymd',strtotime($commissionDetails[0]->getDateCreated())).str_replace(",","",$idImplode);
                      }
                    }else {
                      echo date('ymd',strtotime($commissionDetails[0]->getDateCreated())).str_replace(",","",$id);
                    } ?></td>
                </tr>
                <tr>
                	<td>NRIC</td>
                    <td>:</td>
                    <td><?php
            if ($commissionDetails) {
              ?><input type="text" name="ic_no" value="<?php echo $commissionDetails[0]->getIcNo(); ?>"> <?php
            }else {
              ?><input type="text" name="ic_no" value=""> <?php
            }
             ?></td>
                </tr>
            </table>
        </div>





        <div class="clear"></div>
        <table class="invoice-printing-table">
        	<thead>
                    <tr>
                    	<!-- <th >No.</th>
                        <th >Items</th>
                        <th >Amount (RM)</th> -->
                        <th>Project</th>
                        <th>Details</th>
                        <th>Unit</th>
                        <th>Nett Price (RM)</th>
                        <th>Amount (RM)</th>
                    </tr>
            </thead>

<?php if (isset($_POST['idImplode'])) {
  for ($cnt=0; $cnt <count($idExplode) ; $cnt++) {
  $commissionDetails = getCommission($conn, "WHERE id = ?",array("id"),array($idExplode[$cnt]),"s");
  $loanUid = $commissionDetails[0]->getLoanUid();
  $loanDetails = getLoanStatus($conn, "WHERE loan_uid = ?",array("loan_uid"),array($commissionDetails[0]->getLoanUid()), "s");
  $finalAmount = $commissionDetails[0]->getCommission();
  $finalAmountFinal += $finalAmount;
  ?><tr>
    <td class="td"><?php echo $commissionDetails[0]->getProjectName()  ?></td>
  <td class="td"><?php echo $commissionDetails[0]->getDetails()  ?></td>
      <td class="td"><?php echo $commissionDetails[0]->getUnitNo()  ?></td>
    <td class="td"><?php echo number_format($loanDetails[0]->getNettPrice(),2)  ?></td>
    <td class="td"><input type="text" name="comm[]" value="<?php echo number_format($commissionDetails[0]->getDefCommission(), 2)  ?>"> </td>
</tr>
<?php  }
 for ($i=count($idExplode); $i < 5 ; $i++) {
  ?><tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr> <?php
}
?><td class="td"></td>
<td class="td"></td>
<td class="td"></td>
<td class="td"><b>Total :</b></td>
<td class="td"><b><?php echo number_format($finalAmountFinal, 2)  ?></b></td>
<input type="hidden" name="upline_type" value="<?php echo $_POST['upline_type'] ?>">
<input type="hidden" name="uid" value="<?php echo $commissionDetails[0]->getLoanUid() ?>">

</tr><?php
}else {
                ?><tr>
                  <td class="td"><?php echo $commissionDetails[0]->getProjectName()  ?></td>
                <td class="td"><?php echo $commissionDetails[0]->getDetails()  ?></td>
                    <td class="td"><?php echo $commissionDetails[0]->getUnitNo()  ?></td>
                  <td class="td"><?php echo number_format($nettPrice,2)  ?></td>
                  <td class="td"><input type="text" name="comm[]" value="<?php echo number_format($commissionDetails[0]->getDefCommission(), 2)  ?>"> </td>
              </tr>
              <tr>
                <td class="td"></td>
                  <td class="td"></td>
                <td class="td"></td>
                <td class="td"></td>
                <td class="td"></td>
              </tr>
              <tr>
              <td class="td"></td>
                <td class="td"></td>
              <td class="td"></td>
              <td class="td"></td>
              <td class="td"></td>
              </tr>
              <tr>
              <td class="td"></td>
              <td class="td"></td>
              <td class="td"></td>
              <td class="td"></td>
              <td class="td"></td>
              </tr>
              <tr>
              <td class="td"></td>
              <td class="td"></td>
              <td class="td"></td>
              <td class="td"><b>Total :</b></td>
              <td class="td"><b><?php echo number_format($commissionDetails[0]->getCommission(), 2)  ?> </b></td>
              </tr>
              <input type="hidden" name="upline_type" value="<?php echo $_POST['upline_type'] ?>">
              <input type="hidden" name="uid" value="<?php echo $commissionDetails[0]->getLoanUid() ?>">
              <?php
} ?>


        </table>
		<div class="clear"></div>


        <div class="width100">
			<table  class="vtop-data" >
            	<tr>
                	<td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                	<td>Total in Ringggit Malaysia</td>
                    <td><b>:</b></td>
                    <!-- <td><?php //echo mb_strtoupper(numtowords($commissionDetails[0]->getCommission())." Only") ?></td> -->
                      <td><?php
                      if (isset($_POST['idImplode'])) {
                        $num = number_format($finalAmountFinal,2);
                        $num = str_replace(",","",$num);
                        $split = explode('.',$num);
                        $whole = convertNumber($split[0].".0");
                        $cents = convertNumber($split[1].".0");
                        if ($split[1] == '.00') {
                        echo mb_strtoupper($whole." Ringgit ".$cents." only");
                        }else {
                        echo mb_strtoupper($whole." Ringgit and ".$cents." cents only");
                        }
                      }else {
                        $num = number_format($commissionDetails[0]->getCommission(),2);
                        $num = str_replace(",","",$num);
                        $split = explode('.',$num);
                        $whole = convertNumber($split[0].".0");
                        $cents = convertNumber($split[1].".0");
                        if ($split[1] == '.00') {
                        echo mb_strtoupper($whole." Ringgit ".$cents." only");
                        }else {
                        echo mb_strtoupper($whole." Ringgit and ".$cents." cents only");
                        }
                      }?></td>
                </tr>
                <tr>
                  <td><select class="" name="payment_method">
                    <?php if ($commissionDetails[0]->getPaymentMethod() == 'Online Transfer') {
                      ?><option value="<?php echo $commissionDetails[0]->getPaymentMethod() ?>"><?php echo $commissionDetails[0]->getPaymentMethod() ?></option>
                      <option value="Cheque No.">Cheque No.</option> <?php
                    }elseif ($commissionDetails[0]->getPaymentMethod() == 'Cheque No.') {
                      ?><option value="<?php echo $commissionDetails[0]->getPaymentMethod() ?>"><?php echo $commissionDetails[0]->getPaymentMethod() ?></option>
                      <option value="Online Transfer">Online Transfer</option> <?php
                    }else {
                      ?><option value="Online Transfer">Online Transfer</option>
                      <option value="Cheque No.">Cheque No.</option><?php
                    } ?>

                  </select> </td>
                    <td><b>:</b></td>
                    <td><?php if ($commissionDetails[0]->getCheckID()) {
                      ?><input type="text" name="check_id" value="<?php echo $commissionDetails[0]->getCheckID() ?>"><?php
                    }else {
                    ?><input type="text" name="check_id" value=""><?php
                    } ?> </td>
                </tr>

            </table>
        </div>
        <div class="clear"></div>
        <div class="invoice-print-spacing"></div>
        <div style="margin-bottom: 0 auto" class="signature-div left-sign">
        	<div class="signature-border"></div>
            <p class="invoice-p">Approved by:</p>
            <p class="invoice-p"><b>Eddie Song</b></p>
        </div>
        <div style="margin-bottom: 0 auto" class="signature-div right-sign">
        	<div class="signature-border"></div>
            <p class="invoice-p">Received by:</p>
            <p class="invoice-p"><b><?php echo $commissionDetails[0]->getUpline() ?></b></p>
        </div>

	<div class="clear"></div>
  <?php if ($_SESSION['usertype_level'] == 3) {
    ?><div class="dual-button-div width100">
      <a href="#">
            <button style="margin-left: 290px" class="mid-button red-btn clean"  onclick="window.print()">
                Print
            </button>
        </a>
    </div><?php
  }elseif(isset($_POST['submit']) || isset($_POST['idImplode'])) {
    ?><div class="dual-button-div width100 same-padding">
        <input type="hidden" name="id" value="<?php echo $_POST['idImplode'] ?>">
        <input type="hidden" name="loan_uid" value="<?php echo $_POST['loan_uid'] ?>">
        <input type="hidden" name="upline_type" value="<?php echo $_POST['upline_type'] ?>">
        <a href="#">
          <button style="margin-left: 290px" class="mid-button red-btn clean" name="editBtn" type="submit">
              Save
          </button>
          </a>
      </form>
    </div><?php
  }else {
    ?><div class="dual-button-div width100 same-padding">
        <input type="hidden" name="id" value="<?php echo $commissionDetails[0]->getID() ?>">
        <input type="hidden" name="loan_uid" value="<?php echo $_POST['loan_uid'] ?>">
        <input type="hidden" name="upline_type" value="<?php echo $_POST['upline_type'] ?>">
        <a href="#">
          <button style="margin-left: 290px" class="mid-button red-btn clean" name="editBtn" type="submit">
              Save
          </button>
          </a>
      </form>
    </div><?php
  } ?>


</div></div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
}else {
  $_SESSION['messageType'] = 5;
  header('location: ./adminCommission.php');
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
function goBack() {
  // window.location.href = 'adminCommission.php';
  window.history.back();
}
</script>
</body>
</html>
