<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$branchType = $_SESSION['branch_type'];
$bankDetails = getBankName($conn);
// $projectList = getProject($conn, "WHERE display = 'Yes' and branch_type =?",array("branch_type"),array($branchType), "s");
$projectList = getProject($conn, "WHERE display = 'Yes'");
if (isset($_POST['product_name'])) {
$loanDetails = getLoanStatus($conn, "WHERE project_name = ? AND branch_type =?", array("project_name,branch_type"), array($_POST['product_name'],$branchType), "ss");
}
 ?>
<!-- ====================================================================================================== -->

<form action="utilities/otherInvoiceGeneralFunction.php" method="POST">
  <div>
      <div class="three-input-div dual-input-div">
      <p>Project <a>*</a> </p>
        <select class="dual-input clean" placeholder="Project" id="product_name2" name="project_name">
          <option value="">Select a Project</option>
          <?php for ($cntPro=0; $cntPro <count($projectList) ; $cntPro++)
          {
          ?>
          <option value="<?php echo $projectList[$cntPro]->getProjectName(); ?>">
          <?php echo $projectList[$cntPro]->getProjectName(); ?>
          </option>
          <?php
          }
          ?>
        </select>
      <!-- </form> -->

      </div>

    <div class="three-input-div dual-input-div second-three-input">
      <p>To</p>
      <input required id="toWho2" class="dual-input clean" type="text" placeholder="Auto Generated" readonly>
    </div>

    <div class="three-input-div dual-input-div">
      <p>Date</p>
      <input class="dual-input clean" type="date" id="date_of_claims" name="date_of_claims" value="<?php echo date('Y-m-d') ?>" required>
    </div>

      <!-- <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div"> -->

      <div class="tempo-two-input-clear"></div>

      <!-- <div class="dual-input-div second-dual-input"> -->


      <!-- <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div"> -->
      <!-- <div class="three-input-div dual-input-div second-three-input">
        <p>Invoice <a>*</a></p>
        <select class="dual-input clean" name="invoice_name" >
          <option value="">Please Select an Option</option>
          <option value="Proforma">Proforma</option>
          <option value="Invoice">Invoice</option>
        </select>
      </div> -->
      <!-- <div class="dual-input-div second-dual-input"> -->


      <div class="tempo-two-input-clear"></div>
<div id="input02" >
      <div class="three-input-div dual-input-div">
        <p>Unit <a>*</a><img style="cursor: pointer" id="remBtn2" width="13px" align="right" src="img/mmm.png"><img style="cursor: pointer" id="addBtn2" width="13px" align="right" src="img/ppp.png"></p>
        <!-- <form class="" action="" method="post"> -->
          <select class="dual-input clean" id="unit02" name="unit[]" >
            <option value="">Select an Unit</option>
          </select>
    </div>
    <div class="three-input-div second-three-input dual-input-div">
      <p>Amount (RM) <a>*</a></p>
      <input class="dual-input clean" type="number" step="any" id="amountt" placeholder="Amount (RM)" value="" name="amount[]">
    </div>
    <div class="three-input-div dual-input-div">
      <p>Details <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <input class="dual-input clean" type="text" id="detailss" placeholder="Details" value="" name="details[]">
  </div>
    <div class="tempo-two-input-clear"></div>
  </div>

    <div id="input22" style="display: none" >
      <div class="three-input-div dual-input-div">
        <p>Unit <a>*</a></p>
        <!-- <form class="" action="" method="post"> -->
          <select class="dual-input clean" id="unit22" name="unit[]" >
            <option value="">Select an Unit</option>
          </select>
    </div>
    <div class="three-input-div second-three-input dual-input-div">
      <p>Amount (RM) <a>*</a></p>
      <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount22" value="" name="amount[]">
    </div>
    <div class="three-input-div dual-input-div">
      <p>Details <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
  </div>
    <div class="tempo-two-input-clear"></div>
    </div>

  <div id="input32" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit32" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
    </div>
    <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount32" value="" name="amount[]">
    </div>
    <div class="three-input-div dual-input-div">
      <p>Details <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
  </div>
    <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input42" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit42" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount42" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input52" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit52" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount52" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input62" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit62" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount62" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input72" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit72" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount72" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input82" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit82" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount82" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input92" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit92" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount92" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input102" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit102" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount102" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input112" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit112" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount112" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input122" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit122" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount122" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input132" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit132" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount132" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input142" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit142" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount142" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input152" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit152" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount152" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>
    <!-- <p id="addIn"></p> -->
    <div class="tempo-two-input-clear"></div>

    <div class="three-input-div dual-input-div">
      <p>Change Payee Details <input id="checkBoxPayee" type="checkbox" name="" value=""></p>
          <!-- <form class="" action="" method="post"> -->
  </div>
  <div class="tempo-two-input-clear"></div>
  <div style="display: none" id = "payeeDetails">
      <div class="three-input-div dual-input-div">
        <p>Bank Name Account Holder</p>
        <input  disabled class="dual-input clean" type="text" name="bank_account_holder" placeholder="Bank Name Account Holder">
      </div>
        <div class="three-input-div second-three-input dual-input-div">
          <p>Bank Name</p>
          <select  disabled class="dual-input clean input-style" name="bank_name">
            <option class="dual-input clean" value="">Select a option</option>
            <?php if ($bankDetails) {
              foreach ($bankDetails as $bankName) {
                ?><option class="dual-input clean" value="<?php echo $bankName->getBankName() ?>"><?php echo $bankName->getBankName() ?></option> <?php
              }
            } ?>
          </select>
        </div>
          <div class="three-input-div dual-input-div">
            <p>Bank Account No.</p>
            <input  disabled class="dual-input clean" type="number" id="status" name="bank_account_no" placeholder="Bank Account No." value="">
          </div>
  </div>

      <div class="dual-input-div">
        <p>Include Service Tax (6%) <a>*</a></p>
        <input style="cursor: pointer" required class="" type="radio" value = "YES" name="charges" >Yes
        <input style="cursor: pointer" required class="" type="radio" value = "NO" name="charges" >No
      </div>
      <div class="tempo-two-input-clear"></div>
	  <div class="width100 text-center overflow">

      	<button type="submit" name="upload" value="Upload" class="ow-margin-auto confirm-btn text-center white-text clean black-button">Confirm</button>
      </div>
    </form>
    </div>
    <script src="jsPhp/other.js" charset="utf-8"></script>
