<div class="headerline"></div>
<div class="width100 same-padding header-div">
<a href="agentDashboard.php"><div class="logo"><img src="img/gic.png" class="logo-size" alt="GIC Group" title="GIC Group"></div></a>
	<div class="right-menu">
    	<!-- <a href="adminAddNewProject.php" class="menu-right-margin hover1">
        	<img src="img/project.png" class="hover1a gic-menu-icon" alt="Add New Project" title="Add New Project">
            <img src="img/project2.png" class="hover1b gic-menu-icon" alt="Add New Project" title="Add New Project">
        </a> -->
				<div class="dropdown hover1">
	        	<a  class="menu-right-margin dropdown-btn">
	            	<img src="img/project-dropdown.png" class="gic-menu-icon hover1a" alt="Project Leader" title="Project Leader">
	            	<img src="img/project-dropdown2.png" class="gic-menu-icon hover1b" alt="Project Leader" title="Project Leader">
	            </a>
	            <div class="dropdown-content yellow-dropdown-content">
					<p class="dropdown-p"><a href="loanStatus.php"  class="dropdown-a">Loan Status</a></p>
					<p class="dropdown-p"><a href="invoiceRecordPL.php"  class="dropdown-a">Invoice Record</a></p>
					<!-- <p class="dropdown-p"><a href="adminAddNewProduct.php"  class="dropdown-a">Add New Loan</a></p>
					<p class="dropdown-p"><a href="uploadBookingForm.php"  class="dropdown-a">Upload Loan Form</a></p> -->
	            </div>
	    	</div>
    	<div class="dropdown hover1">
        	<a  class="menu-right-margin dropdown-btn">
            	<img src="img/commission.png" class="gic-menu-icon hover1a" alt="Commission" title="Commission">
            	<img src="img/commission2.png" class="gic-menu-icon hover1b" alt="Commission" title="Commission">
            </a>
            <div class="dropdown-content yellow-dropdown-content">
				<p class="dropdown-p"><a href="personalCommission.php"  class="dropdown-a">My Payroll</a></p>
				<!-- <p class="dropdown-p"><a href="adminAddNewProduct.php"  class="dropdown-a">Add New Loan</a></p>
				<p class="dropdown-p"><a href="uploadBookingForm.php"  class="dropdown-a">Upload Loan Form</a></p> -->
            </div>
    	</div>
    	<div class="dropdown hover1">
        	<a  class="menu-right-margin dropdown-btn">
            	<img src="img/invoice.png" class="gic-menu-icon hover1a" alt="My Performance" title="My Performance">
            	<img src="img/invoice2.png" class="gic-menu-icon hover1b" alt="My Performance" title="My Performance">
            </a>
            <div class="dropdown-content yellow-dropdown-content">
				<p class="dropdown-p"><a href="myPerformance.php"  class="dropdown-a">My Performance</a></p>
				<!-- <p class="dropdown-p"><a href="adminAddNewProduct.php"  class="dropdown-a">Add New Loan</a></p>
				<p class="dropdown-p"><a href="uploadBookingForm.php"  class="dropdown-a">Upload Loan Form</a></p> -->
            </div>
    	</div>
			<a href="myGroup.php"  class="menu-right-margin hover1">
				<img src="img/myteam.png" class="hover1a gic-menu-icon" alt="My Group " title="My Group ">
					<img src="img/myteam2.png" class="hover1b gic-menu-icon" alt="My Group " title="My Group ">
			</a>
			<a href="agentSettings.php"  class="menu-right-margin hover1">
					<img src="img/setting1.png" class="hover1a gic-menu-icon" alt="Setting" title="Setting">
						<img src="img/setting2.png" class="hover1b gic-menu-icon" alt="Setting" title="Setting">
				</a>

    	<a href="logout.php" class="hover1">
        	<img src="img/logout.png" class="hover1a gic-menu-icon" alt="logout" title="logout">
            <img src="img/logout2.png" class="hover1b gic-menu-icon" alt="logout" title="logout">
        </a>
    </div>
</div>
<div class="clear"></div>
