<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/PaymentMethod.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$agentList = getUser($conn, "WHERE user_type = 3 AND status = 'Active' ORDER BY username ASC");
$userName = getLoanStatus($conn, "WHERE loan_uid = ? ",array("loan_uid"),array($_POST['loan_uid']), "s");
$paymentList = getPaymentList($conn,"ORDER BY payment_method ASC");
$projectList = getProject($conn, "WHERE display = 'Yes'");
$bankList = getBankName($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Edit | GIC" />
    <title>Edit | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  a{
    color: red;
  }
</style>
<body class="body">
<?php  include 'admin1Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
  <form method="POST" action="utilities/editProductFunction.php" enctype="multipart/form-data">

	<h1 class="details-h1" onclick="location.href='admin1Product.php';"> <!-- instead use goback() -->
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back3.png" class="back-btn2 hover1b" alt="back" title="back">
        	Name : <?php echo $userName[0]->getPurchaserName(); ?>
        </a>
    </h1>

        <?php
            if(isset($_POST['loan_uid']))
            {
                $conn = connDB();
                $productArray = getLoanStatus($conn,"WHERE loan_uid = ?",array("loan_uid"),array($_POST['loan_uid']), "s");
                $claimCheck = $productArray[0]->getCheckNo1();
            ?><?php //echo $productArray[0]->getId() ?>
            <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" placeholder="Product Id" id="id" name="id" value="<?php echo $productArray[0]->getId() ?>">
            </div>
	<div class="big-dual-input-container">
    <!-- The div class pattern PLEASE follow, the second div need to add second-dual-input inside the class -->
    <div class="dual-input-div">
      <p>Project Name <a>*</a></p>
      <select id="projectName" class="dual-input clean" name="project_name" required>
        <?php
        if ($productArray[0]->getProjectName()) {
          ?><option value="<?php echo $productArray[0]->getProjectName(); ?>"><?php echo $productArray[0]->getProjectName(); ?></option><?php
          if ($projectList) {
            for ($i=0; $i <count($projectList) ; $i++) {
              ?>
             <option value="<?php echo $projectList[$i]->getProjectName(); ?>"><?php echo $projectList[$i]->getProjectName(); ?></option>
              <?php
            }
          }
        }else {
          ?><option value="">Select a project</option><?php
          if ($projectList) {
            for ($i=0; $i <count($projectList) ; $i++) {
              ?>
             <option value="<?php echo $projectList[$i]->getProjectName(); ?>"><?php echo $projectList[$i]->getProjectName(); ?></option>
              <?php
            }
          }
        }
         ?>
      </select>
    </div>

    <div class="dual-input-div second-dual-input">
      <p>Unit No. <a>*</a></p>
      <?php $unitNoExplode = explode(",",$productArray[0]->getUnitNo());
      for ($cnt=0; $cnt <count($unitNoExplode) ; $cnt++) {
        if ($cnt > 0) {
          ?>  <input class="dual-input clean" type="text" placeholder="Unt No." id="unit_no" name="unit_no[]" value="<?php echo $unitNoExplode[$cnt] ?>" required><?php

        }else {
          ?>  <input class="dual-input clean" type="text" placeholder="Unt No." id="unit_no" name="unit_no[]" value="<?php echo $unitNoExplode[$cnt] ?>"><?php
        }
      } ?>
    </div>
    <!-- <div class="tempo-two-input-clear"></div> -->
    <?php $purchaserNameExplode = explode(",",$productArray[0]->getPurchaserName());

    for ($cnt=0; $cnt <count($purchaserNameExplode) ; $cnt++) {
    ?>
    <div class="dual-input-div">
      <p>
        Purchaser Name <a>*</a>
        <?php if ($cnt == 0) {
          ?><img  id="remBtn" width="13px" align="right" src="img/mmm.png"><img id="addBtn" width="13px" align="right" src="img/ppp.png"><?php
        } ?>
      </p>
      <?php
        if ($cnt>0) {
          ?><input class="dual-input clean" type="text" placeholder="Purchaser Name" id="<?php echo 'purchaser_name'.$cnt ?>" name="purchaser_name[]" value="<?php echo $purchaserNameExplode[$cnt] ?>" ><?php
        }else {
          ?><input class="dual-input clean" type="text" placeholder="Purchaser Name" id="purchaser_name" name="purchaser_name[]" value="<?php echo $purchaserNameExplode[$cnt] ?>" required><?php
        } ?>
    </div>
    <input type="hidden" id="purchaserCount" name="" value="<?php echo count($purchaserNameExplode); ?>">
    <!-- <input type="hidden" name="project_name" value="<?php //echo $productArray[0]->getProjectName() ?>"> -->

    <!-- <div class="tempo-two-input-clear"></div> -->
    <div class="dual-input-div second-dual-input">
      <p>IC <a>*</a></p>
      <?php $icExplode = explode(",",$productArray[0]->getIc());
        if ($cnt > 0) {
          if (isset($icExplode[$cnt])) {
            ?><input class="dual-input clean" type="text" placeholder="IC" id="<?php echo 'ic'.$cnt ?>" name="ic[]" value="<?php echo $icExplode[$cnt] ?>"><?php
          }else {
            ?><input class="dual-input clean" type="text" placeholder="IC" id="ic" name="ic[]"><?php
          }
        }else {
          ?><input class="dual-input clean" type="text" placeholder="IC" id="ic" name="ic[]" value="<?php echo $icExplode[$cnt] ?>"><?php
        } ?>
    </div>

    <div class="tempo-two-input-clear"></div>

    <div class="dual-input-div">
      <p>Contact <a>*</a></p>
      <?php $contactExplode = explode(",",$productArray[0]->getContact());
        if ($cnt > 0) {
          if (isset($contactExplode[$cnt])) {
            ?><input class="dual-input clean" type="text" placeholder="Contact" id="<?php echo 'contact'.$cnt ?>" name="contact[]" value="<?php echo $contactExplode[$cnt] ?>"><?php
          }else {
            ?><input class="dual-input clean" type="text" placeholder="Contact" id="contact" name="contact[]"><?php
          }
        }else {
          ?><input class="dual-input clean" type="text" placeholder="Contact" id="contact" name="contact[]" value="<?php echo $contactExplode[$cnt] ?>"><?php

        } ?>

    </div>

    <!-- <div class="tempo-two-input-clear"></div> -->
    <div class="dual-input-div second-dual-input">
      <p>E-mail <a>*</a></p>
      <?php $emailExplode = explode(",",$productArray[0]->getEmail());
      if ($cnt > 0) {
        if (isset($emailExplode[$cnt])) {
          ?>  <input class="dual-input clean" type="text" placeholder="E-mail" id="<?php echo 'email'.$cnt ?>" name="email[]" value="<?php echo $emailExplode[$cnt] ?>" ><?php
        }else {
          ?>  <input class="dual-input clean" type="text" placeholder="E-mail" id="email" name="email[]" ><?php
        }
      }else {
        ?>  <input class="dual-input clean" type="text" placeholder="E-mail" id="email" name="email[]" value="<?php echo $emailExplode[$cnt] ?>" ><?php
      }
      ?><div class="tempo-two-input-clear"></div>
    </div><?php
    }?>
    <p id="addIn"></p>

    <div class="tempo-two-input-clear"></div>

    <div class="dual-input-div">
      <p>Booking Date <a>*</a></p>
      <input class="dual-input clean" type="date" id="booking_date" name="booking_date" value="<?php echo $productArray[0]->getBookingDate() ?>" required>
    </div>

    <!-- <div class="tempo-two-input-clear"></div> -->
    <div class="dual-input-div second-dual-input">
      <p>Sq Ft <a>*</a></p>
      <input required class="dual-input clean" type="text" placeholder="Square Feet" id="sq_ft" name="sq_ft" value="<?php
      if ($productArray[0]->getSqFt()) {
        echo number_format($productArray[0]->getSqFt());
      }
       ?>">
    </div>

    <div class="tempo-two-input-clear"></div>

    <div class="dual-input-div">
      <p>SPA Price <a>*</a></p>
      <input required class="dual-input clean" type="text" placeholder="SPA Price" id="spa_price" name="spa_price" value="<?php
      if ($productArray[0]->getSpaPrice()) {
        echo number_format($productArray[0]->getSpaPrice());
      }
       ?>" required>
    </div>

    <div class="dual-input-div second-dual-input">
      <p>Package</p>
      <input class="dual-input clean" type="text" placeholder="Package" id="package" name="package" value="<?php
      if ($productArray[0]->getPackage()) {
        echo number_format($productArray[0]->getPackage());
      }
       ?>">
    </div>

    <div class="tempo-two-input-clear"></div>

    <div class="dual-input-div">
      <p>Discount</p>
      <input class="dual-input clean" type="text" placeholder="Discount" id="discount" name="discount" value="<?php echo $productArray[0]->getDiscount() ?>">
    </div>

    <div class="dual-input-div second-dual-input">
      <p>Rebate</p>
      <input class="dual-input clean" type="text" placeholder="Rebate" id="rebate" name="rebate" value="<?php echo $productArray[0]->getRebate() ?>">
    </div>

    <div class="tempo-two-input-clear"></div>

    <div class="dual-input-div">
      <p>Bank Approved</p>
      <!-- <input class="dual-input clean" type="text" id="bank_approved" name="bank_approved"> -->
      <select class="dual-input clean" name="bank_approved" >
        <?php if ($productArray[0]->getBankApproved()) {
        ?><option value="<?php echo $productArray[0]->getBankApproved() ?>"><?php echo $productArray[0]->getBankApproved() ?></option> <?php
        } ?>
        <option value="">Please Select a Bank</option>
        <?php if ($bankList) {
          for ($cnt=0; $cnt <count($bankList) ; $cnt++) {
          ?><option value="<?php echo $bankList[$cnt]->getBankName() ?>"><?php echo $bankList[$cnt]->getBankName() ?></option> <?php
          }
        } ?>
      </select>
    </div>

    <div class="dual-input-div second-dual-input">
      <p>Loan Amount Type</p>
      <select class="dual-input clean" id="selectAmountType" class="" name="loan_amount_type">
        <?php if (is_numeric($productArray[0]->getLoanAmount())) {
          ?><option value="RM">RM</option>
          <option value="%">%</option><?php
        }elseif (!is_numeric($productArray[0]->getLoanAmount()) && $productArray[0]->getLoanAmount()) {
          ?><option value="%">%</option>
          <option value="RM">RM</option><?php
        }elseif (!$productArray[0]->getLoanAmount()) {
          ?><option value="">Select a Option</option>
          <option value="%">%</option>
          <option value="RM">RM</option><?php
        } ?>

      </select>
    </div>

    <div class="tempo-two-input-clear"></div>

    <div class="dual-input-div">
      <p id="loanAmountName">Loan Amount</p>
      <input class="dual-input clean" type="text" placeholder="Loan Amount" id="email" value="<?php
      if ($productArray[0]->getLoanAmount()) {
        echo number_format(str_replace("%","",$productArray[0]->getLoanAmount()));
      }
       ?>" name="loan_amount">
    </div>

    <div class="dual-input-div second-dual-input">
    <!-- <div class="dual-input-div"> -->
      <p>Nett Price (RM) <a>*</a></p>
      <!-- <input oninput="this.value = this.value.toUpperCase()" class="dual-input clean" type="text" placeholder="Nett Price" id="nettprice" name="nettprice"> -->
      <input required class="dual-input clean" type="text" placeholder="Nett Price" id="nettprice" name="nettprice" value="<?php
      if ($productArray[0]->getNettPrice()) {
        echo number_format($productArray[0]->getNettPrice());
      }
       ?>">
    </div>

    <div class="tempo-two-input-clear"></div>
    <!-- <div class="dual-input-div second-dual-input"> -->
    <div class="dual-input-div">
      <p>Total Developer Commission</p>
      <input  class="dual-input clean" type="text" placeholder="Total Developer Commission" id="totaldevelopercomm" name="totaldevelopercomm" value="<?php
      if ($productArray[0]->getTotalDeveloperComm()) {
        echo number_format($productArray[0]->getTotalDeveloperComm());
      }
       ?>">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Total Developer Commission (%)</p>
      <input  class="dual-input clean" type="text" placeholder="Total Developer Commission (%)" id="totaldevelopercommper" name="totaldevelopercommper" >
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Agent <a>*</a></p>
      <!-- <input  class="dual-input clean" name="agent" type="text" value="<?php //echo $productArray[0]->getAgent() ?>"> -->
      <?php if ($claimCheck) {
        ?><input type="text" class="dual-input clean" name="agent" value="<?php echo $productArray[0]->getAgent() ?>" readonly> <?php
      }else {
        ?><select required class="dual-input clean" name="agent">
          <?php
          if ($productArray[0]->getAgent())
          {
          ?>
            <option value="<?php echo $productArray[0]->getAgent() ?>">
              <?php echo $productArray[0]->getAgent() ?>
            </option>
            <?php for ($cnt=0; $cnt <count($agentList) ; $cnt++)
            {
              if ($productArray[0]->getAgent() != $agentList[$cnt]->getUsername())
              {
              ?>
              <option value="<?php echo $agentList[$cnt]->getUsername() ?>">
              <?php echo $agentList[$cnt]->getUsername() ?>
              </option>
              <?php
              }
            }
          }
          else
          {
          ?>
            <option value="">Select an agent</option>
            <?php
            for ($cnt=0; $cnt <count($agentList) ; $cnt++)
            {
              if ($productArray[0]->getAgent() != $agentList[$cnt]->getUsername())
              {
              ?>
                <option value="<?php echo $agentList[$cnt]->getUsername() ?>">
                  <?php echo $agentList[$cnt]->getUsername() ?>
                </option>
              <?php
              }
            }
          }


          ?>
        </select><?php
      } ?>
    </div>
    <div class="dual-input-div second-dual-input">
    <p>Loan Status</p>
      <input  class="dual-input clean" type="text" placeholder="Loan Status" id="loanstatus" name="loanstatus" value="<?php echo $productArray[0]->getLoanStatus() ?>">
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
    <p>Remark</p>
    <input class="dual-input clean" type="text" id="remark" name="remark" value="<?php echo $productArray[0]->getRemark() ?>">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>B Form Collected</p>
        <!-- <select class="dual-input clean" name="bform_Collected">
          <option value="">Please Select an Option</option>
          <option value="YES">Yes</option>
          <option value="NO">No</option>
        </select> -->

        <select class="dual-input clean" name="bform_Collected" value="<?php echo $productArray[0]->getBFormCollected() ?>">
        <?php if ($productArray[0]->getBFormCollected() == 'YES')
        {
        ?>
          <option value="YES">Yes</option>
          <option value="NO">No</option>
        <?php
        }
        elseif($productArray[0]->getBFormCollected() == 'NO')
        {
        ?>
          <option value="NO">No</option>
          <option value="YES">Yes</option>
        <?php
        }
        elseif(!$productArray[0]->getBFormCollected())
        {
        ?>
          <option value="">Select a option</option>
          <option value="NO">No</option>
          <option value="YES">Yes</option>
        <?php
        }
        ?>
        </select>

    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Payment Method</p>
        <select class="dual-input clean" name="payment_method" value="<?php echo $productArray[0]->getPaymentMethod() ?>">
        <?php
        if ($productArray[0]->getPaymentMethod())
        {
        ?>
            <option value="<?php echo $productArray[0]->getPaymentMethod() ?>">
              <?php echo $productArray[0]->getPaymentMethod() ?>
            </option>
          <?php for ($cnt=0; $cnt <count($paymentList) ; $cnt++)
          {
            if ($productArray[0]->getPaymentMethod() != $paymentList[$cnt]->getPaymentMethod())
            {
            ?>
              <option value="<?php echo $paymentList[$cnt]->getPaymentMethod() ?>">
                <?php echo $paymentList[$cnt]->getPaymentMethod() ?>
              </option>
            <?php
            }
          }
        }
        else
        {
        ?>
          <option value="">Select an option</option>
          <?php
          for ($cnt=0; $cnt <count($paymentList) ; $cnt++)
          {
            if ($productArray[0]->getPaymentMethod() != $paymentList[$cnt]->getPaymentMethod())
            {
            ?>
              <option value="<?php echo $paymentList[$cnt]->getPaymentMethod() ?>">
                <?php echo $paymentList[$cnt]->getPaymentMethod() ?>
              </option>
            <?php
            }
          }
        }
        ?>
        </select>

    </div>
    <div class="dual-input-div second-dual-input">
      <p>Lawyer</p>
      <input class="dual-input clean" type="text" placeholder="" id="lawyer" name="lawyer" value="<?php echo $productArray[0]->getLawyer() ?>">
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Lo Signed Date</p>
      <input class="dual-input clean" type="date" placeholder="" id="lo_signed_date" name="lo_signed_date" value="<?php echo $productArray[0]->getLoSignedDate() ?>">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>La Signed Date</p>
      <input class="dual-input clean" type="date" placeholder="" id="la_signed_date" name="la_signed_date" value="<?php echo $productArray[0]->getLaSignedDate() ?>">
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Spa Signed Date</p>
      <input class="dual-input clean" type="date" placeholder="" id="spa_signed_date" name="spa_signed_date" value="<?php echo $productArray[0]->getSpaSignedDate() ?>">
    </div>
    <div class="dual-input-div second-dual-input">
    <p>Fullset Completed</p>
      <!-- <select class="dual-input clean" name="fullset_completed">
        <option value="">Please Select an Option</option>
        <option value="YES">Yes</option>
        <option value="NO">No</option>
      </select> -->

      <select class="dual-input clean" name="fullset_completed" value="<?php echo $productArray[0]->getFullsetCompleted() ?>">
      <?php if ($productArray[0]->getFullsetCompleted() == 'YES')
      {
      ?>
        <option value="YES">Yes</option>
        <option value="NO">No</option>
      <?php
      }
      elseif($productArray[0]->getFullsetCompleted() == 'NO')
      {
      ?>
        <option value="NO">No</option>
        <option value="YES">Yes</option>
      <?php
      }
      elseif (!$productArray[0]->getFullsetCompleted())
      {
      ?>
        <option value="">Select a option</option>
        <option value="NO">No</option>
        <option value="YES">Yes</option>
      <?php
      } ?>
      </select>

    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
    <p>Cash Buyer</p>
      <!-- <select class="dual-input clean" name="cash_buyer" >
        <option value="">Please Select an Option</option>
        <option value="YES">Yes</option>
        <option value="NO">No</option>
      </select> -->

      <select class="dual-input clean" name="cash_buyer" value="<?php echo $productArray[0]->getCashBuyer() ?>">
      <?php if ($productArray[0]->getCashBuyer() == 'YES')
      {
      ?>
        <option value="YES">Yes</option>
        <option value="NO">No</option>
      <?php
      }
      elseif($productArray[0]->getCashBuyer() == 'NO')
      {
      ?>
        <option value="NO">No</option>
        <option value="YES">Yes</option>
      <?php
      }
      elseif (!$productArray[0]->getCashBuyer())
      {
      ?>
        <option value="">Select a option</option>
        <option value="NO">No</option>
        <option value="YES">Yes</option>
      <?php
      } ?>
      </select>

    </div>
    <div class="dual-input-div second-dual-input">
    <p>Cancelled Booking</p>
      <!-- <select class="dual-input clean" name="cancelled_booking" >
        <option value="">Please Select an Option</option>
        <option value="YES">Yes</option>
        <option value="NO">No</option>
      </select> -->

      <select class="dual-input clean" name="cancelled_booking" value="<?php echo $productArray[0]->getCancelledBooking() ?>">
      <?php if ($productArray[0]->getCancelledBooking() == 'YES')
      {
      ?>
        <option value="YES">Yes</option>
        <option value="NO">No</option>
      <?php
      }elseif($productArray[0]->getCancelledBooking() == 'NO')
      {
      ?>
        <option value="NO">No</option>
        <option value="YES">Yes</option>
      <?php
      }elseif(!$productArray[0]->getCancelledBooking())
      {
      ?>
        <option value="">Select a option</option>
        <option value="NO">No</option>
        <option value="YES">Yes</option>
      <?php
      } ?>
      </select>

    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Case Status</p>
      <!-- <select class="dual-input clean" name="case_status" >
        <option value="">Please Select an Option</option>
        <option value="PENDING LO">Pending LO</option>
        <option value="PENDING LA">Pending LA</option>
        <option value="PENDING SPA">Pending SPA</option>
        <option value="PENDING Loan">Pending Loan</option>
        <option value="COMPLETED">Completed</option>
      </select> -->

      <select class="dual-input clean" name="case_status" value="<?php echo $productArray[0]->getCaseStatus() ?>">
      <?php if ($productArray[0]->getCaseStatus())
      {
      ?>
        <option value="<?php echo $productArray[0]->getCaseStatus() ?>">
          <?php echo $productArray[0]->getCaseStatus() ?>
        </option>
        <option value="PENDING LO">Pending LO</option>
        <option value="PENDING LA">Pending LA</option>
        <option value="PENDING SPA">Pending SPA</option>
        <option value="PENDING Loan">Pending Loan</option>
        <option value="COMPLETED">Completed</option>
      <?php
      }
      else
      {
      ?>
        <option value="">Select An Option</option>
        <option value="PENDING LO">Pending LO</option>
        <option value="PENDING LA">Pending LA</option>
        <option value="PENDING SPA">Pending SPA</option>
        <option value="PENDING Loan">Pending Loan</option>
        <option value="COMPLETED">Completed</option>
      <?php
      }
      ?>
      </select>

    </div>
    <div class="dual-input-div second-dual-input">
    <p>Event or Personal</p>
      <!-- <select class="dual-input clean" name="event_personal">
        <option value="">Please Select an Option</option>
        <option value="EVENT">EVENT</option>
        <option value="PERSONAL">PERSONAL</option>
      </select> -->

      <select class="dual-input clean" name="event_personal" value="<?php echo $productArray[0]->getEventPersonal() ?>">
      <?php if ($productArray[0]->getEventPersonal() == 'EVENT')
      {
      ?>
        <option value="EVENT">Event</option>
        <option value="PERSONAL">Personal</option>
      <?php
      }
      elseif($productArray[0]->getEventPersonal() == 'PERSONAL')
      {
      ?>
        <option value="PERSONAL">Personal</option>
        <option value="EVENT">Event</option>
      <?php
      }
      elseif (!$productArray[0]->getEventPersonal())
      {
      ?>
        <option value="">Select a option</option>
        <option value="PERSONAL">Personal</option>
        <option value="EVENT">Event</option>
      <?php
      }
      ?>
      </select>

    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Rate % <a>*</a></p>
      <?php
      $advanceDetails = getAdvancedSlip($conn,"WHERE loan_uid = ?",array("loan_uid"),array($productArray[0]->getLoanUid()),"s");
       if ($productArray[0]->getCaseStatus() == 'COMPLETED' || $advanceDetails) {
        ?><input readonly class="dual-input clean" type="text" id="rate" name="rate" value="<?php echo $productArray[0]->getRate() ?>"><?php
      }else {
        ?><input class="dual-input clean" type="text" id="rate" name="rate" value="<?php echo $productArray[0]->getRate() ?>"><?php
      } ?>
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Agent Commission</p>
      <!-- <input oninput="this.value = this.value.toUpperCase()" class="dual-input clean" type="text" id="agent_comm" name="agent_comm"> -->
      <input class="dual-input clean" type="text" value="<?php echo number_format($productArray[0]->getAgentComm()) ?>" id="agent_comm" name="agent_comm">
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>UP1 Name</p>
      <input class="dual-input clean" name="upline1" value="<?php echo $productArray[0]->getUpline1() ?>">
    </div>
    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div"> -->
    <div class="dual-input-div second-dual-input">
    <p>UP2 Name</p>
    <input class="dual-input clean" name="upline2" value="<?php echo $productArray[0]->getUpline2() ?>">
    </div>

        <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
    <!-- <div class="dual-input-div second-dual-input"> -->
      <p>PL Name</p>
        <input class="dual-input clean" type="text" placeholder="PL Name" id="pl_name" name="pl_name" value="<?php echo $productArray[0]->getPlName() ?>">
    </div>
    <div class="dual-input-div second-dual-input">
    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div"> -->
      <p>HOS Name</p>
      <input class="dual-input clean" type="text" placeholder="HOS Name" id="hos_name" name="hos_name" value="<?php echo $productArray[0]->getHosName() ?>">
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
    <!-- <div class="dual-input-div second-dual-input"> -->
      <p>Lister Name</p>
      <input class="dual-input clean" type="text" placeholder="Lister Name" id="lister_name" name="lister_name" value="<?php echo $productArray[0]->getListerName() ?>">
    </div>

    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div"> -->
    <div class="dual-input-div second-dual-input">
      <p>UP1 Override</p>
      <!-- <input oninput="this.value = this.value.toUpperCase()" class="dual-input clean" type="text" id="ul_override" name="ul_override"> -->
      <input class="dual-input clean" type="text"  id="ul_override" name="ul_override" value="<?php echo number_format($productArray[0]->getUlOverride()) ?>" readonly>
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
    <!-- <div class="dual-input-div second-dual-input"> -->
      <p>UP2 Override</p>
      <input class="dual-input clean" type="text" id="uul_override" name="uul_override" value="<?php echo number_format($productArray[0]->getUulOverride()) ?>" readonly>
    </div>

    <!-- <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div"> -->
    <div class="dual-input-div second-dual-input">
      <p>PL Override</p>
      <input class="dual-input clean" type="text" id="pl_override" name="pl_override" value="<?php echo number_format($productArray[0]->getPlOverride()) ?>" readonly>
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>HOS Override</p>
      <input class="dual-input clean" type="text" id="hos_override" name="hos_override" value="<?php echo number_format($productArray[0]->getHosOverride()) ?>" readonly>
      <!-- <input oninput="this.value = this.value.toUpperCase()" class="dual-input clean" type="text" id="hos_override" name="hos_override"> -->
    </div>

    <div class="dual-input-div second-dual-input">
      <p>Lister Override</p>
      <input class="dual-input clean" type="text" id="lister_override" name="lister_override" value="<?php echo number_format($productArray[0]->getListerOverride()) ?>">
    </div>

    <div class="tempo-two-input-clear"></div>
    <!-- <div class="dual-input-div">
      <p>Admin1 Override</p>
      <input  class="dual-input clean" type="text" id="admin1_override" name="admin1_override" value="<?php echo number_format($productArray[0]->getAdmin1Override()) ?>">
    </div>
    <div class="dual-input-div second-dual-input">
      <p>Admin2 Override</p>
      <input class="dual-input clean" type="text" id="admin2_override" name="admin2_override" value="<?php echo number_format($productArray[0]->getAdmin2Override()) ?>">
    </div>

    <div class="tempo-two-input-clear"></div>
    <div class="dual-input-div">
      <p>Admin3 Override</p>
      <input class="dual-input clean" type="text" id="admin3_override" name="admin3_override" value="<?php echo number_format($productArray[0]->getAdmin3Override()) ?>">
    </div> -->
    <div class="dual-input-div">
      <p>GIC Profit</p>
      <input class="dual-input clean" type="text"  id="gic_profit" name="gic_profit" value="<?php echo number_format($productArray[0]->getGicProfit()) ?>" readonly>
    </div>

    <!-- <div class="tempo-two-input-clear"></div> -->
    <div class="dual-input-div second-dual-input">
      <p>Total Claimed Dev Amt</p>
      <input class="dual-input clean" type="text" id="total_claimed_dev_amt" name="total_claimed_dev_amt" value="<?php echo number_format($productArray[0]->getTotalClaimDevAmt()) ?>">
    </div>
    <div class="dual-input-div">
      <p>Total Balanced Dev Amt</p>
      <input type="hidden" name="loan_uid" value="<?php echo $productArray[0]->getLoanUid() ?>">
      <!-- <input type="hidden" id="projectName" value="<?php// echo $productArray[0]->getProjectName() ?>"> -->
      <input class="dual-input clean" type="text" id="total_bal_unclaim_amt" name="total_bal_unclaim_amt" value="<?php echo number_format($productArray[0]->getTotalBalUnclaimAmt()) ?>">
    </div>

               <div class="tempo-two-input-clear"></div>

            <?php
            }
        ?>

  </div>

    <div class="clear"></div>

    <div class="three-btn-container extra-margin-top">
        <!-- <button class="shipout-btn-a red-button three-btn-a" type="submit" id = "deleteProduct" name = "deleteProduct" ><b>DELETE</b></a></button> -->
        <button class="shipout-btn-a black-button three-btn-a" type="submit" id = "editSubmit" name = "editSubmit" ><b>CONFIRM</b></a></button>
    </div>

</div>
</form>
</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
function goBack() {
  window.history.back();
}
</script>
<script type="text/javascript">
  $(document).ready( function(){
    $("#selectAmountType").change( function(){
      var type = $(this).val();

      if (type == '%') {
        $("#loanAmountName").text("Loan Amount (%)");
        $('input[name="loan_amount"]').prop('readonly', false);
      }else if (type == 'RM') {
        $("#loanAmountName").text("Loan Amount (RM)");
        $('input[name="loan_amount"]').prop('readonly', false);
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#projectName").change(function(){
      var selectProject = $("#projectName").val();
      $.ajax({
          url: 'getPLName.php',
          type: 'post',
          data: {projectName:selectProject},
          dataType: 'json',
          success:function(response){

              var len = response.length;

            // $("#pl_name").empty();
            // for (var i = 0; i < len; i++) {

            var PLNameTotal = response[0]['totalPLName'];
            alert(PLNameTotal);
            $("#pl_name").val(PLNameTotal);
          // }
          }
        });
    });
      var purchaserCount = $("#purchaserCount").val();
      // alert(purchaserCount);
      for (var i = 0; i < purchaserCount; i++) {
        $("#purchaser_name"+i+"").on("keyup",function(){
        var purchaser = $(this).val();
        if (purchaser == '') {
          $(this).prop("disabled",true);
        }
        });
        $("#contact"+i+"").on("keyup",function(){
        var purchaser = $(this).val();
        if (purchaser == '') {
          $(this).prop("disabled",true);
        }
        });
        $("#ic"+i+"").on("keyup",function(){
        var purchaser = $(this).val();
        if (purchaser == '') {
          $(this).prop("disabled",true);
        }
        });
        $("#email"+i+"").on("keyup",function(){
        var purchaser = $(this).val();
        if (purchaser == '') {
          $(this).prop("disabled",true);
        }
        });
        var purchaser = $("#purchaser_name"+i+"").val();
        if (purchaser == '') {
          $("#purchaser_name"+i+"").prop("disabled",true);
        }
        var purchaser = $("#contact"+i+"").val();
        if (purchaser == '') {
          $("#contact"+i+"").prop("disabled",true);
        }
        var purchaser = $("#ic"+i+"").val();
        if (purchaser == '') {
          $("#ic"+i+"").prop("disabled",true);
        }
        var purchaser = $("#email"+i+"").val();
        if (purchaser == '') {
          $("#email"+i+"").prop("disabled",true);
        }
      }
      var click = 0;
      $("#addBtn").click(function(){
        click++;
        var div = ('<div id="el'+click+'"><div class="dual-input-div"><p>Purchaser Name</p><input class="dual-input clean" type="text" placeholder="Purchaser Name" name="purchaser_name[]" required></div><div class="dual-input-div second-dual-input"><p>IC</p><input class="dual-input clean" type="text" placeholder="IC" id="ic" name="ic[]" required></div><div class="dual-input-div"><p>Contact</p><input class="dual-input clean" type="text" placeholder="Contact" id="contact" name="contact[]" required></div><div class="dual-input-div second-dual-input"><p>E-mail</p><input class="dual-input clean" type="text" placeholder="E-mail" id="email" name="email[]" required></div></div>');
        $("#addIn").slideDown(function(){
          $(div).hide().appendTo("#addIn").slideDown();
        });
      });
      $("#remBtn").click(function(){
        if (click > 0) {
          $("#el"+click+"").slideUp(function(){
            $("#el"+click+"").empty();
          });
          click--;
        }
        // alert(click);
      });
  });
</script>

</body>
</html>
