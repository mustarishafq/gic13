<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();
$a = "";

$branchType = $_SESSION['branch_type'];
$projectName = $_POST['projectName'];   // department id
// $projectName = 'Project A1';
   // department id
// $projectDetails = getProject($conn, "WHERE project_name =? AND branch_type =?", array("project_name,branch_type"), array($projectName,$branchType), "ss");
$projectDetails = getProject($conn, "WHERE project_name =? AND display ='Yes'",array("project_name"),array($projectName),"s");
$projectClaims = $projectDetails[0]->getProjectClaims();
$projectPeople = $projectDetails[0]->getAddProjectPpl();
$loanDetails = getLoanStatus($conn,"WHERE project_name =? AND case_status='COMPLETED' AND cancelled_booking != 'YES' AND display = 'Yes' AND branch_type =?", array("project_name,branch_type"), array($projectName,$branchType), "ss");
if ($loanDetails) {
  for ($cnt=0; $cnt <count($loanDetails) ; $cnt++) {
    $claim1 = $loanDetails[$cnt]->getClaimAmt1st();
    $claim2 = $loanDetails[$cnt]->getClaimAmt2nd();
    $claim3 = $loanDetails[$cnt]->getClaimAmt3rd();
    $claim4 = $loanDetails[$cnt]->getClaimAmt4th();
    $claim5 = $loanDetails[$cnt]->getClaimAmt5th();
    $sst1 = $loanDetails[$cnt]->getSst1();
    $sst2 = $loanDetails[$cnt]->getSst2();
    $sst3 = $loanDetails[$cnt]->getSst3();
    $sst4 = $loanDetails[$cnt]->getSst4();
    $sst5 = $loanDetails[$cnt]->getSst5();

    if ($sst1 == 'YES' || $sst2 == 'YES' || $sst3 == 'YES' || $sst4 == 'YES' || $sst5== 'YES' ) {
      $charges = 'Yes';
    }elseif ($sst1 == 'NO' || $sst2 == 'NO' || $sst3 == 'NO' || $sst4 == 'NO' || $sst5== 'NO' ) {
      $charges = 'No';
    }else {
      $charges = null;
    }

    if ($projectClaims >= 1 && !$claim1) {
      $a = $loanDetails[$cnt]->getUnitNo();
    }if ($projectClaims >= 2 && !$claim2) {
        $a = $loanDetails[$cnt]->getUnitNo();
      }if ($projectClaims >= 3 && !$claim3) {
          $a = $loanDetails[$cnt]->getUnitNo();
        }if ($projectClaims >= 4 && !$claim4) {
            $a = $loanDetails[$cnt]->getUnitNo();
          }if ($projectClaims >= 5 && !$claim5) {
              $a = $loanDetails[$cnt]->getUnitNo();
            }
            if ($a) {
              $f[] = array("unit_no" => $a, "add_projectppl" => $projectPeople, "project_claims" => $projectClaims, "charges" => $charges);
            }
            $a = 0;
  }
}

  // encoding array to json format
  echo json_encode($f);


 ?>
