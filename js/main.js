function putNoticeJavascript(header,text)
{
    document.getElementById('noticeTitle').innerHTML = header;
    document.getElementById('noticeMessage').innerHTML = text;

    // Get the modal
    var modal = document.getElementById('notice-modal');

    // Get the <span> element that closes the modal
    var span = document.getElementById('close-notice');

    var aud = document.createElement("audio");
    aud.src = "audio/unconvinced.mp3";
    aud.play();

    modal.style.display = 'block';


    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = 'none';
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = 'none';
        }
    }
}
function putNoticeJavascriptReload(header,text)
{
    document.getElementById('noticeTitle').innerHTML = header;
    document.getElementById('noticeMessage').innerHTML = text;

    // Get the modal
    var modal = document.getElementById('notice-modal');

    // Get the <span> element that closes the modal
    var span = document.getElementById('close-notice');

    var audio = document.createElement("audio");
    audio.src = "audio/unconvinced.mp3";
    audio.play();

    modal.style.display = 'block';


    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = 'none';
        location.reload();
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = 'none';
            location.reload();
        }
    }
}
function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}
function viewPassword(img,inputPassword)
{
    // var img = document.getElementById('login_view_password');
    // var inputPassword = document.getElementById('login_password');

    img.onclick = function ()
    {
        if(inputPassword.type == "password")
        {
            inputPassword.type = "text";
        }
        else if(inputPassword.type == "text")
        {
            inputPassword.type = "password";
        }
    }
}
//=======================disable enter to submit form======================================
$("form").keypress(function(e) {
  var formExcludedIndex = $(this).attr("id");

  if (formExcludedIndex != 'enableEnter') {
    //Enter key
    if (e.which == 13) {
      return false;
    }
  }
});
