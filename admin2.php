<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];;
$branchType = $_SESSION['branch_type'];

$projectDetails = getProject($conn);
// $projectName = "";
// $conn->close();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  th:hover{
    background-color: maroon;
  }
  th.headerSortUp{
    background-color: black;
  }
  th.headerSortDown{
    background-color: black;
  }
  .hover:hover{
    transition: .1s;
    font-weight: bold;
    transform: scale(1.1);
  }
</style>
<body class="body">
<?php  include 'agentHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<div class="overflow dash-min-height">
<marquee direction="left" class="greeting">
Welcome to GIC Smart Admin System
</marquee>
<div class="width100 overflow">
	<img src="img/gic-cover.jpg" class="cover-img width100"> 
</div>
<div class="width100 same-padding extra-padding-top-bottom">
	<a href="agentProject.php">
        <div class="four-btn red-btn">
        	<p class="four-btn-p extra-top-p"><b>All Project</b></p>
        </div>
    </a>
	<a href="loanStatus.php">
        <div class="four-btn red-btn second-four-btn third-two mob-second-div">
        	<p class="four-btn-p"><b>My Project</b><br>Loan Status</p>
        </div>
    </a>
	<a href="invoiceRecordPL.php">
        <div class="four-btn red-btn third-four-btn">
        	<p class="four-btn-p"><b>My Project</b><br>Invoice Record</p>
        </div>
    </a>
	<a href="myPerformance.php">
        <div class="four-btn red-btn mob-second-div">
        	<p class="four-btn-p extra-top-p special-extra-p"><b>My Performance</b></p>
        </div>
    </a>
	<a href="myGroup.php">
        <div class="four-btn red-btn third-two">
        	<p class="four-btn-p extra-top-p"><b>My Downlines</b></p>
        </div>
    </a>
	<a href="personalCommission.php">
        <div class="four-btn red-btn second-four-btn mob-second-div">
        	<p class="four-btn-p extra-top-p"><b>My Payroll</b></p>
        </div>
    </a>
	<a href="settings.php">
        <div class="four-btn red-btn third-four-btn">
        	<p class="four-btn-p extra-top-p"><b>My Account</b></p>
        </div>
    </a>
    <a href="adminDashboard.php">
          <div class="four-btn red-btn mob-second-div third-two mob-second-div">
          	<p class="four-btn-p extra-top-p special-extra-p last-c-p"><b>Company Performance</b></p>
          </div>
      </a>

</div>
</div>
<div class="clear"></div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
          if ($announcementDetails)
          {
          for ($cnt=0; $cnt <count($announcementDetails) ; $cnt++)
          {
            $dateCreated = date('Y-m-d H:i:s',strtotime($announcementDetails[$cnt]->getDateCreated()."+ 1 days"));
            $now = date('Y-m-d H:i:s');
            if ($dateCreated >= $now ) {
              $messageType = $announcementDetails[$cnt]->getDetails()."<br>";
              echo '
              <script>
                  putNoticeJavascript("Announcement !! ","'.$messageType.'");
              </script>';
              $_SESSION['messageType'] = 0;
            }

          }
          ?>

          <?php
          }
        }
        elseif($_GET['type'] == 2){

          $messageType = "Successfully Change Password.";
          echo '
          <script>
              putNoticeJavascript("Notice !! ","'.$messageType.'");
          </script>
          ';
          $_SESSION['messageType'] = 0;
        }
        $_SESSION['messageType'] = 0;
      }}
 ?>
</body>
<script type="text/javascript">
function showFrontLayer() {
  document.getElementById('bg_mask').style.visibility='visible';
  document.getElementById('frontlayer').style.visibility='visible';
}
function hideFrontLayer() {
  document.getElementById('bg_mask').style.visibility='hidden';
  document.getElementById('frontlayer').style.visibility='hidden';
}
</script>
<script type="text/javascript">
  $(document).ready( function(){
    $("#getAnnouncementTable").click( function(){
      $("#announcementTable").slideToggle();
    });
  });
</script>
<script>
  $(function(){
    $("#myTable").tablesorter( {dateFormat: 'pt'} );
  });
</script>
</html>
