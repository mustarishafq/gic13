<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$username = $_SESSION['username'];

$conn = connDB();

$projectList = getProject($conn, "WHERE display = 'Yes'");

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Add Company Branch | GIC" />
    <title>Add Company Branch | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
a{
  color: maroon;
}
#alert{
  font-size: 13px;
}
  input:focus, textarea:focus, select:focus{
    outline: none;
  }
  #addNewProjectBtn{
    color: black;
  }
  #changePasswordCheck{
    width :15px;
    height :15px;
    vertical-align: middle;
    outline: 2px solid maroon;
    outline-offset: -2px;
  }
  .dual-input-div .maroon-text:hover{

    font-weight: bold;

    /* font-size: 17px; */
  }
  input{
    font-weight: bold;
  }
  .dual-input-div .maroon-text,.dual-input-div .label{
    font-size: 16px;
  }
  .dual-input-div .maroon-text{

    font-weight: bold;

  }
  .positionStyle{
  
    font-weight: bold;

  }
  .positionStyle:hover{
    color: black;
  }


  .support{
    font-size: 12px;
    font-weight: bold;
    color: red;
  }
  .support:hover{
    font-size: 15px;
    color: red;
  }

</style>
<body class="body">
<?php if ($_SESSION['usertype_level'] %4 == 1) {
  include 'admin1Header.php';
}elseif ($_SESSION['usertype_level'] %4 == 2) {
  include 'admin2Header.php';
} ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1"><a href="settings.php" class="red-color-swicthing">Edit Profile</a> | Add Company Branch | <a href="editSlipAddress.php" class="red-color-swicthing">Edit Slip Address</a> </h1>
    <div class="short-red-border"></div><br>
    <div class="clear"></div>
<div id="AddNewProject">
    <form method="POST" action="utilities/addCompBranchFunction.php" enctype="multipart/form-data">
      <div class="dual-input-div">
        <p class="label">Company Name </p>
        <input class="dual-input clean" type="text" name="companyName" placeholder="Company Name">
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">SST No. </p>
        <input class="dual-input clean" type="text" placeholder="e.g : 1363274-D (SST No : P11-1808-31038566)" name="sst_no">
      </div>
      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p class="label">Company Branch (Optional)</p>
        <input class="dual-input clean" type="text" placeholder="Company Branch" id="commission" name="company_branch"  >
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">Company Address </p>
        <input class="dual-input clean" type="text" placeholder="Packages" id="packages" name="address"  >
      </div>
      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p class="label">Contact </p>
        <input class="dual-input clean" type="text" placeholder="Contact" id="video_link" name="contact"  >
      </div>
      <div class="dual-input-div second-dual-input">
        <p class="label">E-mail </p>
        <input class="dual-input clean" type="text" placeholder="E-mail" id="video_link" name="email"  >
      </div>
      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div">
        <p class="label">Company Logo </p>
        <!-- <form class="form" action="announcementFunction.php" method="post" enctype="multipart/form-data"> -->
            <div class="form-group">
                <input type="file" name="logo" />
            </div>
          <!-- </form> -->
      </div>
      <div class="tempo-two-input-clear"></div><br>
      <a class="support">* Support Uploaded File png, jpg, jpeg.</a><br>
        <button id="noEnterSubmit" class="button" type="submit" name="upload">Submit</button>
    </form>
  </div>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Created an Announcement. ";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Wrong Current Password.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully Updated The Project Details";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>
<script>
  $(function(){
    $("#project_name").change(function(){
      var project = $(this).val();

      $.ajax({
        url : 'getPLName.php',
        type : 'post',
        data : {projectName:project},
        dataType : 'json',
        success:function(response){

          var totalPL = response[0]['totalPLName'];
          $("#projectLeader").val(totalPL);
        }
      });
    });
  });
</script>
