<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';
require_once dirname(__FILE__) . '/classes/Commission.php';
require_once dirname(__FILE__) . '/classes/IssuePayroll.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$fullName = $_SESSION['fullname'];

$conn = connDB();
$a = 0;
$adv = 0;
$comm1 = 0;
$comm2 = 0;
$comm3 = 0;
$comm4 = 0;
$comm5 = 0;
$totalAmountFinal = 0;
$userRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($fullName),"s");
$userDetails = $userRows[0];



$projectDetails = getProject($conn);
// $projectName = "";
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Other Commission | GIC" />
    <title>Other Commission | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  th:hover{
    background-color: maroon;
  }
  th.headerSortUp{
    background-color: black;
  }
  th.headerSortDown{
    background-color: black;
  }
</style>
<body class="body">
<?php  include 'agentHeader.php'; ?>
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div class="yellow-body same-padding">
  <div class="modal">

  </div>

  <h1 class="h1-title h1-before-border shipping-h1">My Payroll</h1>


  <div class="short-red-border"></div>
<h3 class="h1-title"> <a href="personalCommission.php" class="red-color-swicthing">Personal Commission</a> | <a href="overridingCommissionAgent.php" class="h1-title red-color-swicthing">Overriding Commission</a> | Other Commission</h3>
  <div class="section-divider width100 overflow">

  <?php
  $conn = connDB();
  $projectDetails = getProject($conn, "WHERE display = 'Yes'"); ?>

    <select id="projectChoose" class="clean-select pointer">
      <option value="">Select a project</option>
      <?php if ($projectDetails) {
        for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
          if ($projectDetails[$cnt]->getProjectName() != $types) {
            ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
            }
            }
            ?><option value="ALL">SHOW ALL</option><?php
          } ?>
    </select>
      <input class="clean search-btn gap" type="text" name="" value="" placeholder="Search.." id="searchInput" autocomplete="off">
      <input class="clean search-btn gap" type="text" name="" value="" placeholder="End Date.." id="searchEndDate" autocomplete="off">
      <input class="clean search-btn gap" type="text" name="" value="" placeholder="Start Date.." id="searchStartDate" autocomplete="off">
  </div>
  <a href="excel/otherCommissionExport.php"><button class="exportBtn red-bg-swicthing" type="button" name="button">Export</button></a>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
        <table id="myTable" class="shipping-table pointer-th">
            <thead>
                <tr>
                    <th class="th">NO. <img src="img/sort.png" class="sort"></th>
                    <!-- <th class="th"><?php //echo wordwrap("PROJECT NAME",7,"</br>\n");?></th> -->
                    <th class="th">UNIT NO. <img src="img/sort.png" class="sort"></th>
                    <th class="th">PROJECT NAME <img src="img/sort.png" class="sort"></th>
                    <!-- <th class="th"><?php //echo wordwrap("UNIT NO.",7,"</br>\n");?></th> -->
                    <!-- <th class="th"><?php //echo wordwrap("TOTAL COMMISSION",10,"</br>\n");?></th> -->
                    <th class="th">TOTAL COMMISSION <img src="img/sort.png" class="sort"></th>
                    <!-- <th class="th"><?php// echo wordwrap("1ST CLAIM COMMISSION (RM)",10,"</br>\n");?></th> -->
                    <!-- <th class="th"><?php //echo wordwrap("PAYSLIP DATE",10,"</br>\n");?></th> -->
                    <th class="th">PAYSLIP DATE <img src="img/sort.png" class="sort"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                // {
                    $otherCommission = getIssuePayroll($conn, "WHERE project_handler_default = ? and receive_date != ''",array("project_handler_default"),array($fullName), "s");
                    if ($otherCommission) {
                      for ($cnt=0; $cnt< count($otherCommission); $cnt++) {
                        $amountExp = explode(",",$otherCommission[$cnt]->getAmount());
                        for ($i=0; $i <count($amountExp) ; $i++) {
                          $totalAmount = $amountExp[$i];
                          $totalAmountFinal = $totalAmountFinal + $totalAmount;
                        }
                        // if ($otherCommission[$cnt]->getReceiveDate()) {
                        ?><tr>
                          <td class="td"><?php echo $cnt + 1?></td>
                          <td class="td"><?php echo $otherCommission[$cnt]->getProjectName() ?></td>
                          <td class="td"><?php echo str_replace(",","<br>",$otherCommission[$cnt]->getUnitNo()) ?></td>
                          <td class="td"><?php echo number_format(str_replace(",","<br>",$totalAmountFinal),2);$totalAmountFinal = 0; ?></td>
                          <td class="td">
                            <form class="" action="issuePayrollSlip.php" method="post">
                              <input type="hidden" name="invoice_no" value="<?php echo date('ymd').$otherCommission[$cnt]->getID() ?>">
                              <input type="hidden" name="invoice_id" value="<?php echo $otherCommission[$cnt]->getID() ?>">
                              <input type="hidden" name="booking_date" value="<?php echo date('d/m/Y',strtotime($otherCommission[$cnt]->getReceiveDate())) ?>">
                              <button class="clean edit-anc-btn hover1" type="submit" name="invoice_id" value="<?php echo $otherCommission[$cnt]->getInvoiceId() ?>">
                                <a><?php echo date('d/m/Y',strtotime($otherCommission[$cnt]->getDateUpdated())) ?></a>
                              </button>
                            </form>
                          </td>
                        </tr> <?php
                      // }
                    }
                    }else {
                      ?><td colspan="5" style="text-align: center;font-weight: bold;font-size: 14px" >No Commission Found !</td> <?php
                    }

                //}
                ?>
            </tbody>
        </table><br>
    </div>

    <?php $conn->close();?>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<script>
$("#searchStartDate,#searchEndDate").datepicker({
  dateFormat: 'dd/mm/yy',
});

$("#searchStartDate,#searchEndDate,#projectChoose").on("change",function(){
  var bookingStartDate = $("#searchStartDate").val();
  var bookingEndDate = $("#searchEndDate").val();
    var project = $("#projectChoose").val();
  $.ajax({
    url: 'otherCommissionFilter.php',
    data: {bookingStartDate:bookingStartDate,bookingEndDate:bookingEndDate,project:project},
    type: 'post',
    success:function(data){
      $(document).ready(function(){
        $(document).ajaxStart(function(){
          $(".modal").show();
        });
        $(document).ajaxStop(function(){
          setTimeout(function(){
            $(".modal").hide();
              $("#myTable").empty();
              $("#myTable").html(data);
          },500);
        });
      });
    }
  });
});
</script>
<?php include 'js.php'; ?>

</body>
<script>
  $(function(){
    $("#myTable").tablesorter( {dateFormat: 'pt'} );
  });
</script>
</html>
