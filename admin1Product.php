<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess1.php';
// require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product2.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$projectName = "";
$cntNO = 0;
$cntYES = 0;
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <style>
table {
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th {
  cursor: pointer;
}

.th:hover{
  background-color: maroon;
}

th td {
  text-align: left;
  padding: 16px;
}
.th0 {
  vertical-align: middle;
  text-align: center;
    z-index: 2;
    background-color: #7babff;
}
.th0:hover{
  background-color: maroon;
}
.company-td{
  position:sticky;
  width: 60px;
  min-width: 60px;
  max-width: 60px;
  left:0;
}
.company-td1{
  position:sticky;
  width: 70px;
  min-width: 70px;
  max-width: 70px;
  left:60px;
}
.company-td2{
  position:sticky;
  width: 100px;
  min-width: 100px;
  max-width: 100px;
  left:130px;
}
.company-td3{
position:sticky;
width: auto;
min-width: auto;
max-width: auto;
  left: 230px;
}
.th.headerSortUp , .th0.headerSortUp{
  background-color: black;
}
.th.headerSortDown , .th0.headerSortDown{
  background-color: black;
}
</style>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php  include 'admin1Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">
    <h1 class="h1-title h1-before-border shipping-h1">Loan Status Form</h1>

    <div class="short-red-border"></div>
	<div class="width100 overflow section-divider">
        <a href="adminAddNewProject.php">
            <div class="five-red-btn-div">
                <p class="short-p five-red-p g-first-3-p n-p first-row-p new-project-p">NEW PROJECT</p>
            </div>
        </a>
        <a href="adminAddNewProduct.php">
            <div class="five-red-btn-div left-mid-red">
                <p class="short-p five-red-p f-first-3-p a-p">ADD NEW BOOKING</p>
            </div>
        </a>
        <a href="#">
        <form method="post" action="export_data.php">
            <button class="five-red-btn-div clean clean-button" type="submit" name="export">
                <p class="short-p five-red-p e-first-3-p e-p first-row-p export-p">EXPORT TO EXCEL</p>
            </button>
        </form>
        </a>
        <a href="archiveBooking1.php">
            <div class="five-red-btn-div right-mid-red">
                <p class="short-p five-red-p f-first-3-p a-p">ARCHIVED BOOKING</p>
            </div>
        </a>
        <a href="agentInfo.php">
            <div class="five-red-btn-div">
                <p class="short-p five-red-p g-first-3-p n-p second-row-p">AGENTS INFO</p>
            </div>
        </a>
    </div>
	<div class="clear"></div>
    <div class="section-divider width100 overflow">
		<?php $projectDetails = getProject($conn, "WHERE display = 'Yes'"); ?>

      <select id="sel_id" class="clean-select clean pointer">
        <option value="">Select a project</option>
        <?php if ($projectDetails) {
          for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
            if ($projectDetails[$cnt]->getProjectName() != $types) {
              ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
              }
              }
              ?><option value="ALL">SHOW ALL</option><?php
            } ?>
      </select>
    </div>
    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table id="myTable" class="shipping-table pointer-th">
                <thead>
                    <tr style="position: sticky;top: 0;">
                        <th class="th0 company-td">NO. <img src="img/sort.png" class="sort"></th>
                        <th class="th0 company-td1">ACTION <img src="img/sort.png" class="sort"></th>
                        <th class="th0 company-td2"><?php echo wordwrap("UNIT NO.",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th0 company-td3"><?php echo "PROJECT NAME";?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("PURCHASER NAME",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("BOOKING DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("SPA PRICE",8,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("NETT PRICE",8,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th">AGENT <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("LOAN STATUS",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("SPA SIGNED DATE",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th">CASH BUYER <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("CANCELLED BOOKING",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("EVENT / PERSONAL",10,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th">RATE <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("UP1 NAME",5,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("UP2 NAME",5,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("UP3 NAME",5,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("UP4 NAME",5,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("UP5 NAME",5,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("PL NAME",5,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("HOS NAME",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th"><?php echo wordwrap("DATE MODIFIED",7,"</br>\n");?> <img src="img/sort.png" class="sort"></th>
                        <th class="th">ARCHIVE <img src="img/sort.png" class="sort"></th>
                    </tr>
                </thead>
                <tbody id="myFilter">
                    <?php
                    // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                    // {
                        if ($_SESSION['branch_type'] == 1) {
                          $loanDetails = getLoanStatus($conn,"WHERE display = 'Yes'");
                        }else {
                          $loanDetails = getLoanStatus($conn,"WHERE display = 'Yes' and branch_type = ?".$projectName,array("branch_type"),array($_SESSION['branch_type']), "s");
                        }

                        if($loanDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($loanDetails) ;$cntAA++)
                            {
                              if ($loanDetails[$cntAA]->getCancelledBooking() != 'YES') {
                                $cntNO++;
                              ?><tr>
                                  <td class="td company-td"><?php echo ($cntNO+$cntYES)?></td>

                                <!-- <td class="td"><?php //echo $loanDetails[$cntAA]->getListerName();?></td> -->
                                <td class="td company-td1">
                                    <form action="editProduct.php" method="POST">
                                        <button class="edit-btn clean" type="submit" name="loan_uid" value="<?php echo $loanDetails[$cntAA]->getLoanUid();?>">
                                          Edit
                                            <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" >
                                            <img src="img/edit3.png" class="edit-announcement-img hover1b" > -->
                                        </button>
                                    </form>
                                </td>
                                  <td class="td company-td2"><?php echo wordwrap($loanDetails[$cntAA]->getUnitNo(),10,'<br>') ;?></td>
                                  <td class="td company-td3"><?php echo $loanDetails[$cntAA]->getProjectName();?></td>
                                  <td class="td"><?php
                                  $a = $loanDetails[$cntAA]->getPurchaserName();
                                   echo str_replace(',',"<br>", $a) ;?></td>
                                  <!-- <td><?php //echo $loanDetails[$cntAA]->getStock();?></td> -->
                                  <td class="td"><?php echo date('d-m-Y', strtotime($loanDetails[$cntAA]->getBookingDate()));?></td>
                                  <?php $spaPrice = $loanDetails[$cntAA]->getSpaPrice();?>
                                  <td class="td"><?php echo $spaPriceValue = number_format($spaPrice, 2); ?></td>
                                  <?php $nettPrice = $loanDetails[$cntAA]->getNettPrice();?>
                                  <td class="td"><?php echo $nettPriceValue = number_format($nettPrice, 2); ?></td>
                                  <td class="td"><a class="pointer" id="<?php echo 'agentNameDetails'.($cntNO+$cntYES) ?>" value="<?php echo $loanDetails[$cntAA]->getAgent();?>"><?php echo $loanDetails[$cntAA]->getAgent();?></a</td>
                                  <td class="td"><?php echo wordwrap($loanDetails[$cntAA]->getLoanStatus(),50,"</br>\n");?></td>
                                  <td class="td"><?php if ($loanDetails[$cntAA]->getSpaSignedDate()) {
                                    echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getSpaSignedDate()));
                                  };?></td>
                                  <td class="td"><?php echo $loanDetails[$cntAA]->getCashBuyer();?></td>
                                  <td class="td"><?php echo $loanDetails[$cntAA]->getCancelledBooking();?></td>
                                  <td class="td"><?php echo $loanDetails[$cntAA]->getEventPersonal();?></td>
                                  <td class="td"><?php echo $loanDetails[$cntAA]->getRate();
                                  if ($loanDetails[$cntAA]->getRate()) {
                                    echo "%";
                                  }?></td>
                                  <td class="td"><a id="agentDetailsBox" class="<?php echo "upline1Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline1();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline1(),20,"<br>");?></a></td>
                                  <td class="td"><a id="agentDetailsBox" class="<?php echo "upline2Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline2();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline2(),20,"<br>");?></a></td>
                                  <td class="td"><a id="agentDetailsBox" class="<?php echo "upline3Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline3();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline3(),20,"<br>");?></a></td>
                                  <td class="td"><a id="agentDetailsBox" class="<?php echo "upline4Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline4();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline4(),20,"<br>");?></a></td>
                                  <td class="td"><a id="agentDetailsBox" class="<?php echo "upline5Box".$cntAA ?>" value="<?php echo $loanDetails[$cntAA]->getUpline5();?>"><?php echo wordwrap($loanDetails[$cntAA]->getUpline5(),20,"<br>");?></a></td>
                                  <td class="td"><?php echo str_replace(",","<br>",$loanDetails[$cntAA]->getPlName());?></td>
                                  <td class="td"><?php echo $loanDetails[$cntAA]->getHosName();?></td>
                                  <td id="dateModified" class="td">
                                    <form class="" action="historyIndividual.php" method="post">
                                      <input type="hidden"  id="loanUid" name="loan_uid" value="<?php echo $loanDetails[$cntAA]->getLoanUid() ?>">
                                      <input type="hidden" id="totalLoanDetails" value="<?php echo count($loanDetails) ?>">
                                      <button type="submit" class="clean edit-anc-btn hover1" name="button"><a><?php echo date('d-m-Y',strtotime($loanDetails[$cntAA]->getDateUpdated()));?></a> </button>
                                      </td>
                                    </form>
                                  <td class="td">
                                      <!-- <form action="utilities/archiveLoan.php" method="POST"> -->
                                          <button id="archiveBtn" class="clean edit-anc-btn hover1" type="submit" name="loan_uid_archive" value="<?php echo $loanDetails[$cntAA]->getLoanUid();?>">
                                              <img src="img/archiv0.png" class="edit-announcement-img hover1a" >
                                              <img src="img/archiv3.png" class="edit-announcement-img hover1b" >
                                          </button>
                                      <!-- </form> -->
                                  </td>
                              </tr><?php
                            } ?>

                            <?php
                            }
                            for($cntBB = 0;$cntBB < count($loanDetails) ;$cntBB++)
                            {
                              if ($loanDetails[$cntBB]->getCancelledBooking() == 'YES') {
                                $cntYES++;
                              ?><tr>
                                <td class="td company-td" style="background-color: pink;"><?php echo ($cntNO+$cntYES)?></td>

                              <!-- <td style="background-color: pink;" class="td"><?php //echo $loanDetails[$cntBB]->getListerName();?></td> -->
                              <td style="background-color: pink;" class="td company-td1 red">
                                  <form action="editProduct.php" method="POST">
                                      <button class="clean edit-anc-btn hover1" type="submit" name="loan_uid" value="<?php echo $loanDetails[$cntBB]->getLoanUid();?>">
                                          <img src="img/edit.png" class="edit-announcement-img hover1a"  >
                                          <img src="img/edit3.png" class="edit-announcement-img hover1b" >
                                      </button>
                                  </form>
                              </td>
                                <td class="td company-td2" style="background-color: pink;"><?php echo $loanDetails[$cntBB]->getUnitNo();?></td>
                                <td class="td company-td3" style="background-color: pink;"><?php echo wordwrap($loanDetails[$cntBB]->getProjectName(),20,"<br>");?></td>
                                <td style="background-color: pink;" class="td"><?php
                                $a = $loanDetails[$cntBB]->getPurchaserName();
                                 echo str_replace(',',"<br>", $a) ;?></td>
                                <!-- <td><?php //echo $loanDetails[$cntBB]->getStock();?></td> -->
                                <td style="background-color: pink;" class="td"><?php echo date('d-m-Y', strtotime($loanDetails[$cntBB]->getBookingDate()));?></td>
                                <?php $spaPrice = $loanDetails[$cntBB]->getSpaPrice();?>
                                <td style="background-color: pink;" class="td"><?php echo $spaPriceValue = number_format($spaPrice, 2); ?></td>
                                <?php $nettPrice = $loanDetails[$cntBB]->getNettPrice();?>
                                <td style="background-color: pink;" class="td"><?php echo $nettPriceValue = number_format($nettPrice, 2); ?></td>
                                <td style="background-color: pink;" class="td"><a class="pointer" id="<?php echo 'agentNameDetails'.($cntNO+$cntYES) ?>" value="<?php echo $loanDetails[$cntBB]->getAgent();?>"><?php echo $loanDetails[$cntBB]->getAgent();?></a></td>
                                <td style="background-color: pink;" class="td"><?php echo wordwrap($loanDetails[$cntBB]->getLoanStatus(),50,"</br>\n");?></td>
                                <td style="background-color: pink;" class="td red" class="td"><?php if ($loanDetails[$cntBB]->getSpaSignedDate()) {
                                  echo date('d-m-Y',strtotime($loanDetails[$cntBB]->getSpaSignedDate()));
                                };?></td>
                                <td style="background-color: pink;" class="td"><?php echo $loanDetails[$cntBB]->getCashBuyer();?></td>
                                <td style="background-color: pink;" class="td"><?php echo $loanDetails[$cntBB]->getCancelledBooking();?></td>
                                <td style="background-color: pink;" class="td"><?php echo $loanDetails[$cntBB]->getEventPersonal();?></td>
                                <td style="background-color: pink;" class="td"><?php echo $loanDetails[$cntBB]->getRate();
                                if ($loanDetails[$cntBB]->getRate()) {
                                  echo "%";
                                }?></td>
                                <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline1Box".$cntBB ?>" value="<?php echo $loanDetails[$cntBB]->getUpline1();?>"><?php echo wordwrap($loanDetails[$cntBB]->getUpline1(),20,"<br>");?></a></td>
                                <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline2Box".$cntBB ?>" value="<?php echo $loanDetails[$cntBB]->getUpline2();?>"><?php echo wordwrap($loanDetails[$cntBB]->getUpline2(),20,"<br>");?></a></td>
                                <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline3Box".$cntBB ?>" value="<?php echo $loanDetails[$cntBB]->getUpline3();?>"><?php echo wordwrap($loanDetails[$cntBB]->getUpline3(),20,"<br>");?></a></td>
                                <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline4Box".$cntBB ?>" value="<?php echo $loanDetails[$cntBB]->getUpline4();?>"><?php echo wordwrap($loanDetails[$cntBB]->getUpline4(),20,"<br>");?></a></td>
                                <td style="background-color: pink;" class="td"><a id="agentDetailsBox" class="<?php echo "upline5Box".$cntBB ?>" value="<?php echo $loanDetails[$cntBB]->getUpline5();?>"><?php echo wordwrap($loanDetails[$cntBB]->getUpline5(),20,"<br>");?></a></td>
                                <td style="background-color: pink;" class="td"><?php echo $loanDetails[$cntBB]->getPlName();?></td>
                                <td style="background-color: pink;" class="td"><?php echo $loanDetails[$cntBB]->getHosName();?></td>
                                <td style="background-color: pink;" id="dateModified" class="td">
                                  <form class="" action="historyIndividual.php" method="post">
                                    <input type="hidden"  id="loanUid" name="loan_uid" value="<?php echo $loanDetails[$cntBB]->getLoanUid() ?>">
                                    <button type="submit" class="clean edit-anc-btn hover1" name="button"><a><?php echo date('d-m-Y',strtotime($loanDetails[$cntBB]->getDateUpdated()));?></a> </button>
                                    </td>
                                  </form>
                                <td style="background-color: pink " class="td">
                                    <!-- <form action="utilities/archiveLoan.php" method="POST"> -->
                                        <button id="archiveBtn" class="clean edit-anc-btn hover1" type="submit" name="loan_uid_archive" value="<?php echo $loanDetails[$cntBB]->getLoanUid();?>">
                                            <img src="img/archiv0.png" class="edit-announcement-img hover1a" >
                                            <img src="img/archiv3.png" class="edit-announcement-img hover1b" >
                                        </button>
                                    <!-- </form> -->
                                </td>
                            </tr><?php
                            } ?>

                            <?php
                            }
                        }else {
                          ?>
                          <td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="20">
                            No Loan Yet. Press "ADD NEW BOOKING" to Add New Booking.
                          </td>
                          <?php
                        }
                    //}
                    ?>
                </tbody>
            </table><br>


    </div>


    <?php $conn->close();?>

</div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'detailsBox.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login to The System. ";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully Add New Booking";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
  $(function(){
    $("#myTable").tablesorter( {dateFormat: 'pt'} );
  });
</script>
<script>
  $(document).ready(function(){
    $("button[name='loan_uid_archive']").click(function(){
      var loanUid = $(this).val();

      $.ajax({
        url: "utilities/archiveLoan.php",
        type: "post",
        data: {loanUid:loanUid},
        dataType: "json",
        success:function(response){

          var unit = response[0]['unit_no'];
          var test = " Has Been Add To Archive";
          // $("#notyElDiv").fadeIn(function(){
            // $("#notyEl").html(unit+test);
            putNoticeJavascriptReload("Notice !!",""+unit+" Has Been Added To Archived");
          // });
          // alert(unit);
          // location.reload();
        }
      });
    });

    var No = <?php echo count($loanDetails); ?>;
    var upline = 6;
    for (var i = 0; i < No; i++) {
      for (var j = 1; j < upline; j++) {

        $(".upline"+j+"Box"+i+"").click(function(){
          var agentName = $(this).attr("value");
          // alert(agentName);

          $.ajax({
            url : 'utilities/agent2DetailsFunction.php',
            type : 'post',
            data : {agentNames:agentName},
            dataType : 'json',
            success:function(response){
              var agentNamee = response[0]['agentNamed'];
              // alert(agentNamee);
              $("#ss").text(agentNamee);
            },
            error:function(response){
              // alert("failed");
            }
          });

           $('.hover_bkgr_fricc').fadeIn(function(){
             $("#myDiv").load(location.href + " #myDiv");
             $(this).show();
           });
        });

      }

    }

    $('.hover_bkgr_fricc').click(function(){
        $('.hover_bkgr_fricc').fadeOut(function(){
          $("#mydiv").load(location.href + " #mydiv");
          $(this).hide();
        });
    });
    $('.popupCloseButton').click(function(){
        $('.hover_bkgr_fricc').fadeOut(function(){
          $("#mydiv").load(location.href + " #mydiv");
          $(this).hide();
        });
    });
    var totalLoanDetails = +$("#totalLoanDetails").val() + +1;

    for (var i = 0; i < totalLoanDetails; i++) {
      $("#agentNameDetails"+i+"").click(function(){
        var agentNameDetails = $(this).attr("value");
        // alert(agentNameDetails);

        $.ajax({
          url : 'utilities/agent2DetailsFunction.php',
          type : 'post',
          data : {agentNames:agentNameDetails},
          dataType : 'json',
          success:function(response){
            var agentNamee = response[0]['agentNamed'];
            // alert(agentNamee);
            $("#ss").text(agentNamee);
          },
        });

         $('.hover_bkgr_fricc').fadeIn(function(){
           $("#myDiv").load(location.href + " #myDiv");
           $(this).show();
         });
      });
    }
  });
</script>
</body>
</html>
