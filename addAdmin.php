<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/PaymentMethod.php';
require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Branch.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$agentList = getUser($conn, "WHERE user_type = 3");
$paymentList = getPaymentList($conn);
$bankNameList = getBankName($conn);
$projectList = getProject($conn, "WHERE display = 'Yes'");
$branch = getBranch($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Add New Admin | GIC" />
    <title>Add New Admin | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
.aaa{
  color: maroon;
}
.aaa:hover{
  color: maroon;
  text-decoration: none;
}
</style>
<body class="body">

<?php  include 'admin1Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body padding-from-menu same-padding">
<h1 class="h1-title h1-before-border shipping-h1">Add New Admin | <a class="red-color-swicthing" href="addAgent.php">Add New Agent</a> </h1>
<div class="short-red-border"></div><br>

  <form  action="utilities/addAdminFunction.php" method="POST" enctype="multipart/form-data">

      <div class="dual-input-div margin-bottom20">
        <p>Admin1 Username <a>*</a></p>
        <input class="dual-input clean" type="text" placeholder="Admin1 Username" id="full_name" name="admin1_username" >
      </div>
      <div class="dual-input-div second-dual-input margin-bottom20">
        <p>Admin2 Username <a>*</a></p>
        <input class="dual-input clean" type="text" placeholder="Admin2 Username" id="full_name" name="admin2_username" >
      </div>

      <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div margin-bottom20">
        <p>Admin1 Branch <a>*</a></p>
        <select class="dual-input clean" name="admin1_branch">
        <option value="">Select a Branch</option>
          <?php if ($branch) {
            foreach ($branch as $branchNew) {
              ?><option value="<?php echo $branchNew->getBranchType(); ?>"><?php echo $branchNew->getBranchName(); ?></option> <?php
            }
          } ?>
        </select>
        <!-- <input class="dual-input clean" type="text" placeholder="Admin1 Branch" id="full_name" name="admin1_branch" > -->
      </div>
      <div class="dual-input-div second-dual-input margin-bottom20">
        <p>Admin2 Branch <a>*</a></p>
        <select class="dual-input clean" name="admin2_branch">
        <option value="">Select a Branch</option>
          <?php if ($branch) {
            foreach ($branch as $branchNew) {
              ?><option value="<?php echo $branchNew->getBranchType(); ?>"><?php echo $branchNew->getBranchName(); ?></option> <?php
            }
          } ?>
        </select>
      </div>

      <div class="clear"></div>
      <div class="tempo-two-input-clear"></div>
      <div class="three-btn-container extra-margin-top">
          <!-- <button class="shipout-btn-a red-button three-btn-a" type="submit" id = "deleteProduct" name = "deleteProduct" ><b>DELETE</b></a></button> -->
          <button class="shipout-btn-a black-button three-btn-a" type="submit" id = "editSubmit" name = "editSubmit" ><b>CONFIRM</b></a></button>
      </div>
    </form>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Product Added Successfully";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "There is an error to add the new product";
        }

        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(document).ready(function(){
  var i = 0;
  $("#addBtn").click(function(){
    i++;
    var div = ('<div id="el'+i+'"><div class="dual-input-div"><p>Purchaser Name</p><input class="dual-input clean" type="text" placeholder="Purchaser Name" name="purchaser_name[]" required></div><div class="dual-input-div second-dual-input"><p>Contact</p><input class="dual-input clean" type="text" placeholder="Contact" id="contact" name="contact[]" required></div><div class="tempo-two-input-clear"></div><div class="dual-input-div"><p>IC</p><input class="dual-input clean" type="text" placeholder="IC" id="ic" name="ic[]" required></div><div class="dual-input-div second-dual-input"><p>E-mail</p><input class="dual-input clean" type="text" placeholder="E-mail" id="email" name="email[]"></div></div>');

    $(div).hide().appendTo("#addIn").slideDown(1000);
  });
  $("#remBtn").click(function(){
    if ($('#el'+i+' input').length > 1) {
                $('#el'+i+'').slideUp(1000, function(){
                  $(this).remove();
                });
                i--;
            }
  });

  $("#selectAmountType").change( function(){
    var type = $(this).val();

    if (type == '%') {
      $("#loanAmountName").text("Loan Amount (%)");
      $('input[name="loan_amount"]').prop('readonly', false);
    }else if (type == 'RM') {
      $("#loanAmountName").text("Loan Amount (RM)");
      $('input[name="loan_amount"]').prop('readonly', false);
    }
  });

});
</script>
</body>
</html>
