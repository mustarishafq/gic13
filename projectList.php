<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn," WHERE user_type=3");

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Add Project | GIC" />
    <title>Add Project | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  a{
    color: maroon;
  }
  input:focus, textarea:focus, select:focus{
    outline: none;
  }
  #addNewProjectBtn{
    color: black;
  }
  th:hover{
    background-color: maroon;
  }
  th.headerSortUp{
    background-color: black;
  }
  th.headerSortDown{
    background-color: black;
  }
</style>
<body class="body">
<?php  include 'admin1Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php //echo '<script type="text/javascript" src="js/jquery.tablesorter.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1"><a class="red-color-swicthing" href="adminAddNewProject.php">Add New Project</a> | Project List | <a href="projectListArchived.php" class="red-color-swicthing">Archived</a> </h1>
    <div class="short-red-border"></div>
    <div class="section-divider width100 overflow">
      <?php
      if ($_SESSION['branch_type'] == 1) {
        $projectDetails = getProject($conn, "WHERE display = 'Yes'");
      }else{
        $projectDetails = getProject($conn, "WHERE display = 'Yes' AND branch_type =?",array("branch_type"),array($_SESSION['branch_type']), "s");
      }
       ?>

        <select id="sel_id" class="clean-select">
          <option value="">Select a project</option>
          <?php if ($projectDetails) {
            for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) {
              if ($projectDetails[$cnt]->getProjectName() != $types) {
                ?><option value="<?php echo $projectDetails[$cnt]->getProjectName()?>"><?php echo $projectDetails[$cnt]->getProjectName() ?></option><?php
                }
                }
                ?><option value="ALL">SHOW ALL</option><?php
              } ?>
        <!-- <option value="-1">Select</option>
        <option value="VIDA">kasper </option>
        <option value="VV">adad </option> -->
        <!-- <option value="14">3204 </option> -->
        </select>
        <input class="clean search-btn" type="text" name="" value="" placeholder="Search.." id="searchInput">
    </div>

    <!-- <h2>Project List</h2> -->
<div class="width100 shipping-div2">
    <table id="projectList" class="shipping-table pointer-th">
    <thead>
      <tr>
          <th>No. <img src="img/sort.png" class="sort"></th>
          <th>Project Name <img src="img/sort.png" class="sort"></th>
          <th>Person In Charge (PIC) <img src="img/sort.png" class="sort"></th>
          <th>Project Leader <img src="img/sort.png" class="sort"></th>
          <th>Company Name <img src="img/sort.png" class="sort"></th>
          <th>Company Address <img src="img/sort.png" class="sort"></th>
          <th>Project Claim <img src="img/sort.png" class="sort"></th>
          <th>Other Claim (RM) <img src="img/sort.png" class="sort"></th>
          <!-- <th>Display</th> -->
          <th>Date Created <img src="img/sort.png" class="sort"></th>
          <th>Date Updated <img src="img/sort.png" class="sort"></th>
          <th>Created <img src="img/sort.png" class="sort"></th>
          <th>Action <img src="img/sort.png" class="sort"></th>
          <th>Archived <img src="img/sort.png" class="sort"></th>
      </tr>
    </thead>
    <tbody id="myFilter">
    <?php
      // $projectDetails = getProject($conn, "WHERE display = 'Yes' and branch_type = ?",array("branch_type"),array($_SESSION['branch_type']), "s");
      // if ($_SESSION['branch_type'] == 1) {
      //   $projectDetails = getProject($conn, "WHERE display = 'Yes'");
      // }else{
      //   $projectDetails = getProject($conn, "WHERE display = 'Yes' AND branch_type =?",array("branch_type"),array($_SESSION['branch_type']), "s");
      // }
      // $agentDetails = getLoanStatus($conn);
      if ($projectDetails) {
        for ($cnt=0; $cnt <count($projectDetails) ; $cnt++) { ?>
          <tr>
          <?php
          ?>  <td class="td"><?php echo $cnt+1 ?></td>
              <td class="td"><?php echo $projectDetails[$cnt]->getProjectName() ?></td>
              <td class="td"><?php echo $projectDetails[$cnt]->getAddProjectPpl() ?></td>
              <td class="td"><?php echo str_replace(",","<br>",$projectDetails[$cnt]->getProjectLeader()); ?></td>
              <td class="td"><?php echo $projectDetails[$cnt]->getCompanyName() ?></td>
              <td class="td"><?php echo str_replace(",","<br>",$projectDetails[$cnt]->getCompanyAddress()) ?></td>
              <td class="td"><?php echo $projectDetails[$cnt]->getProjectClaims() ?></td>
              <td class="td"><?php echo $projectDetails[$cnt]->getOtherClaims() ?></td>
              <!-- <td class="td"><?php //echo $projectDetails[$cnt]->getDisplay() ?></td> -->
              <td class="td"><?php echo date('d-m-Y', strtotime($projectDetails[$cnt]->getDateCreated())) ?></td>
              <td class="td">
                <form class="" action="historyProject.php" method="post">
                  <input type="hidden" name="id" value="<?php echo $projectDetails[$cnt]->getProjectId() ?>">
                  <button class="clean edit-anc-btn" type="submit" name="button"><a id="buttonUpdate" class="blue-link"><?php echo date('d-m-Y', strtotime($projectDetails[$cnt]->getDateUpdated())) ?><a></button></td>
                </form>
              <td class="td"><?php echo $projectDetails[$cnt]->getCreatorName() ?></td>
              <td class="td">  <form action="editProject.php" method="POST">
                    <button class="clean edit-anc-btn hover1" type="submit" name="project_id" value="<?php echo $projectDetails[$cnt]->getId();?>">
                        <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product">
                        <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product">
                    </button>
                </form></td>
                <td class="td">
                    <!-- <form action="utilities/archiveLoan.php" method="POST"> -->
                        <button id="archiveBtn" class="clean edit-anc-btn hover1" type="submit" name="loan_uid_archive" value="<?php echo $projectDetails[$cnt]->getId();?>">
                            <img  src="img/archiv0.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product">
                            <img src="img/archiv3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product">
                        </button>
                    <!-- </form> -->
                </td>
          </tr>
          <?php }}else {
            ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="13"> No Created Project. </td> <?php
          } ?>
    </tbody>
    </table>
  </div>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login to The System. ";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Add New Project.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully Updated The Project Details";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
<script type="text/javascript">
$(function(){
  $('#projectList').tablesorter( {dateFormat: 'pt'} );
});
$("button[name='loan_uid_archive']").click(function(){
  var id = $(this).val();

  $.ajax({
    url: "utilities/archiveProject.php",
    type: "post",
    data: {id:id},
    dataType: "json",
    success:function(response){

      var unit = response[0]['unit_no'];
      var test = " Has Been Add To Archive";
      // $("#notyElDiv").fadeIn(function(){
        // $("#notyEl").html(unit+test);
        putNoticeJavascriptReload("Notice !!",""+unit+" Has Been Added To Archived");
      // });
      // alert(unit);
      // location.reload();
    }
  });
});
</script>
</html>
