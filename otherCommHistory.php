<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/timezone.php';
require_once dirname(__FILE__) . '/adminAccess2.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Commission.php';
require_once dirname(__FILE__) . '/classes/IssuePayroll.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
// require_once dirname(__FILE__) . '/classes/AdvancedSlip.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $loanUidRows = getLoanStatus($conn, "WHERE case_status = 'COMPLETED'");
$loanUidRows = getCommission($conn, "WHERE receive_status = 'PENDING' ");
$otherCommission = getIssuePayroll($conn, "WHERE receive_status = 'PENDING'");
$invoiceDetails = getInvoice($conn);
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Commission History | GIC" />
    <title>Commission History | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
input#checkboxToDisplayTableMulti,#HistoryBtn {
    width :25px;
    height :25px;
    vertical-align: middle;
    outline: 2px solid maroon;
    outline-offset: -2px;
}
input#checkboxClick{
  width :20px;
  height :20px;
  outline: 2px solid maroon;
  outline-offset: -2px;
}
</style>
<style media="screen">
table {
text-align: left;
position: relative;
}

th {
background: white;
position: sticky;
top: 0;
}
.th:hover, .th0:hover{
  background-color: maroon;
}
.th.headerSortUp , .th0.headerSortUp{
  background-color: black;
}
.th.headerSortDown , .th0.headerSortDown{
  background-color: black;
}
</style>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
  <?php if ($loanUidRows || $otherCommission) {
    ?><h1 class="h1-title h1-before-border shipping-h1">Commission History </h1>
      <div class="short-red-border"></div><br>
      <p><a href="commissionHistory.php" class="red-color-swicthing">Commission</a> | Other Commission</p>
  <form action="issuePayrollSlip.php" method="POST">
  <div style="display: none" class="advancedIssue">
    <button style=" border-radius: 5px;padding: 5px" class="red-bg-swicthing" name="submit" type=submit>Issue Multiple Advanced</button>
    <!-- <button class="advancedIssue" style="display: none;color: white;background-color: maroon; border-radius: 5px;padding: 5px" name="preview" type=submit>Preview</button> -->
  </div>
      <!-- This is a filter for the table result -->


      <!-- <select class="filter-select clean">
      	<option class="filter-option">Latest Shipping</option>
          <option class="filter-option">Oldest Shipping</option>
      </select> -->

      <!-- End of Filter -->
      <div class="clear"></div>

      <div class="width100 shipping-div2">
          <?php $conn = connDB();?>
          <table id="tableSingleComm" class="shipping-table pointer-th">
              <thead>
                  <tr>
                      <th class="th">NO. <img src="img/sort.png" class="sort"></th>
                      <th class="th">PROJECT NAME <img src="img/sort.png" class="sort"></th>
                      <th class="th">UNIT NO. <img src="img/sort.png" class="sort"></th>
                      <th class="th">AGENT NAME <img src="img/sort.png" class="sort"></th>
                      <th class="th">DATE <img src="img/sort.png" class="sort"></th>
                      <th class="th">TIME <img src="img/sort.png" class="sort"></th>
                      <th class="th">ACTION <img src="img/sort.png" class="sort"></th>

                      <!-- <th>INVOICE</th> -->
                  </tr>
              </thead>
              <tbody>
                  <?php
                  // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                  // {
                      $otherCommission = getIssuePayroll($conn, "WHERE receive_status != 'PENDING' and branch_type = ?",array("branch_type"),array($_SESSION['branch_type']), "s");
                      if($otherCommission != null)
                      {
                          for($cnt = 0;$cnt < count($otherCommission) ;$cnt++)
                          {
                            // $loanDetails = getLoanStatus($conn, "WHERE project_name = ?",array("project_name"),array($otherCommission[$cnt]->getProjectName()), "s");
                            // if ($loanDetails) {
                              // for ($cntAA=0; $cntAA <count($loanDetails) ; $cntAA++) {
                                ?>

                              <tr>
                                  <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                  <td class="td"><?php echo $cnt + 1;?></td>
                                  <td class="td"><?php echo $otherCommission[$cnt]->getProjectName();?></td>
                                  <td class="td"><?php echo str_replace(",","<br>",$otherCommission[$cnt]->getUnitNo());?></td>
                                  <td class="td"><?php echo $otherCommission[$cnt]->getProjectHandler();?></td>
                                  <td class="td"><?php echo date('d/m/Y', strtotime($otherCommission[$cnt]->getDateCreated())) ?></td>
                                  <td class="td"><?php echo date('h:i a', strtotime($otherCommission[$cnt]->getDateCreated())) ?></td>

                                  <td class="td">
                                        <input type="hidden" name="invoice_id" value="<?php echo $otherCommission[$cnt]->getInvoiceId(); ?>">
                                        <input type="hidden" name="invoice_no" value="<?php echo date('ymd', strtotime($otherCommission[$cnt]->getDateCreated())).$otherCommission[$cnt]->getID(); ?>">
                                        <input type="hidden" name="booking_date" value="<?php echo date('d-m-Y', strtotime($otherCommission[$cnt]->getBookingDate())); ?>">
                                        <a><button class="clean edit-anc-btn hover1 red-link" type="submit" name="id" value="<?php echo $otherCommission[$cnt]->getID();?>">View Commission
                                              <!-- <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Product" title="Edit Product"> -->
                                              <!-- <img src="img/edit3.png" class="edit-announcement-img hover1b" alt="Edit Product" title="Edit Product"> -->
                                          </button></a>

                                  </td>

                              </tr>
                              <?php
                              // }
                            // }
                          }
                      }else {
                        ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="7">
                            No Commission Found.
                          </td> <?php
                      }
                  //}
                  ?>
              </tbody>
          </table>
                </form>

      </div>

      <!-- <div class="three-btn-container">
      <!-- <a href="adminRestoreProduct.php" class="add-a"><button name="restore_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side two-button-side1">Restore</button></a> -->
        <!-- <a href="adminAddNewProduct.php" class="add-a"><button name="add_new_product" class="confirm-btn text-center white-text clean black-button anc-ow-btn two-button-side  two-button-side2">Add</button></a> -->
      <!-- </div> -->
      <?php $conn->close();?><?php
  }else {
    ?><h2 style="text-align: center" class="loan-h2">No Commission Slip Found.</h2>
    <h2 style="text-align: center" class="loan-h2">Make Sure To Send To Agent.</h2> <?php
  } ?>


</div>



<?php unset($_SESSION['commission_id']);
      unset($_SESSION['idImp']);?>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Successfully Updated Commission Slip Details";
        }
        elseif($_GET['type'] == 6)
        {
            $messageType = "Commission Slip Not Selected.";
        }
        elseif($_GET['type'] == 7)
        {
            $messageType = "Upline Name Selected Are Not Same.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script type="text/javascript">
$(document).ready( function(){
  // $("#checkboxToDisplayTableMulti").remove();
  // $("#addBack").append('<input id="checkboxToDisplayTableMulti" type="checkbox" name="headerCheck">');
  $("#checkboxToDisplayTableMulti").on('change', function(){
    if($("#checkboxToDisplayTableMulti").is(':checked')){
      $("#HistoryBtn").prop('checked',false);
      $(".advancedIssue").show();
      $("#tableMultipleComm").show();
      $("#tableSingleComm").hide();
      $("#History").hide();
    }
    if($("#checkboxToDisplayTableMulti").is(':not(:checked)')){
      $(".advancedIssue").hide();
      $("#tableMultipleComm").hide();
      $("#tableSingleComm").show();
      $("#history").hide();
    }
  });
});

$(function(){
  $('#tableSingleComm,#tableMultipleComm').tablesorter( {dateFormat: 'pt'} );
});
</script>
</body>
</html>
