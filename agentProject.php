<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/adminAccess3.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
$branchType = $_SESSION['branch_type'];

$projectDetails = getProject($conn);
// $projectName = "";
// $conn->close();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Dashboard | GIC" />
    <title>Dashboard | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <?php include 'css.php'; ?>
</head>
<style media="screen">
  th:hover{
    background-color: maroon;
  }
  th.headerSortUp{
    background-color: black;
  }
  th.headerSortDown{
    background-color: black;
  }
  .hover:hover{
    transition: .1s;
    font-weight: bold;
    transform: scale(1.1);
  }
</style>
<body class="body">
<?php  include 'agentHeader.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<div class="yellow-body same-padding">

  <h1 class="h1-title h1-before-border shipping-h1">Project List</h1>
  <div class="short-red-border"></div>
<!-- <h3 class="h1-title m-margin-top"> Personal Sales | <a href="personalOverriding.php" class="h1-title">Personal Overriding</a></h3> -->
<!-- <h3 class="h1-title m-margin-top"> Personal Sales</h3><br> -->
<!-- <p style="color: maroon;font-size: 18px" class="h1-title m-margin-top"> Position : <?php //echo $userDetails->getPosition() ?></a> -->
  <div class="section-divider width100 overflow">

  <?php
  $conn = connDB();
?>

  <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
        <table id="myTable" class="shipping-table pointer-th">
            <thead>
                <tr>
                    <th rowspan="2" class="th">NO. <img src="img/sort.png" class="sort"></th>
                    <th colspan="3" class="th"><?php echo wordwrap("PROJECT",7,"</br>\n");?></th>
                    <th rowspan="2" class="th">PROJECT PACKAGES <img src="img/sort.png" class="sort"></th>
                    <th rowspan="2" class="th"><?php echo wordwrap("DATE",10,"</br>\n");?>  <img src="img/sort.png" class="sort"></th>
                    <th rowspan="2" class="th">PRESENTATION FILE <img src="img/sort.png" class="sort"></th>
                    <th rowspan="2" class="th">E-BROSHURE <img src="img/sort.png" class="sort"></th>
                    <th rowspan="2" class="th">BOOKING FORM <img src="img/sort.png" class="sort"></th>
                    <th rowspan="2" class="th">VIDEO LINK <img src="img/sort.png" class="sort"></th>
                    <th rowspan="2" class="th">DATE MODIFIED <img src="img/sort.png" class="sort"></th>

                </tr>
                <tr>
                  <th>Project Name</th>
                  <th>PL Name</th>
                  <th>Commission (RM)</th>
                </tr>
            </thead>
            <tbody>
                <?php
                // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                // {
                    $announcement = getAnnouncement($conn, "WHERE branch_type =?",array("branch_type"),array($branchType), "s");
                    if($announcement != null)
                    {
                        for($cntAA = 0;$cntAA < count($announcement) ;$cntAA++)
                        {?>
                        <tr>
                            <td class="td"><?php echo ($cntAA+1)?></td>
                            <td class="td"><?php echo wordwrap($announcement[$cntAA]->getProjectName(),20,"<br>")?></td>
                            <td class="td"><?php echo str_replace(",","<br>",$announcement[$cntAA]->getPlName());?></td>
                            <td class="td"><?php echo number_format($announcement[$cntAA]->getCommission(),2);?></td>
                            <td class="td"><?php echo $announcement[$cntAA]->getPackage();?></td>
                            <td class="td"><?php echo date('d/m/Y',strtotime($announcement[$cntAA]->getDateCreated()));?></td>
                            <td class="td hover"><?php
                            if ($announcement[$cntAA]->getPresentFile()) {
                              ?><a href="<?php echo "announcement/".$announcement[$cntAA]->getPresentFile() ?>"><button class="view-download-btn" type="button" name="button">View/Download</button></a> <?php
                            } ?>
                            </td>
                            <td class="td hover"><?php
                            if ($announcement[$cntAA]->getEBroshure()) {
                              ?><a href="<?php echo "announcement/".$announcement[$cntAA]->getEBroshure() ?>"><button class="view-download-btn" type="button" name="button">View/Download</button></a> <?php
                            } ?>
                            </td>
                            <td class="td hover"><?php
                            if ($announcement[$cntAA]->getBookingForm()) {
                              ?><a href="<?php echo "announcement/".$announcement[$cntAA]->getBookingForm() ?>"><button class="view-download-btn" type="button" name="button">View/Download</button></a> <?php
                            } ?>
                            </td>
                            <td class="td hover"><?php
                            if ($announcement[$cntAA]->getVideoLink()) {
                              ?><a><?php echo $announcement[$cntAA]->getVideoLink() ?></a> <?php
                            } ?>
                            </td>
                            <td class="td"><?php echo date('d/m/Y',strtotime($announcement[$cntAA]->getDateUpdated()));?></td>
                        </tr>
                        <?php
                        }
                    }else {
                      ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="11">No Announcement.</td> <?php
                    }
                //}
                ?>
            </tbody>
        </table><br>
    </div>

    <?php //$conn->close();?>
</div>
</div>
<div class="clear"></div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
          if ($announcementDetails)
          {
          for ($cnt=0; $cnt <count($announcementDetails) ; $cnt++)
          {
            $dateCreated = date('Y-m-d H:i:s',strtotime($announcementDetails[$cnt]->getDateCreated()."+ 1 days"));
            $now = date('Y-m-d H:i:s');
            if ($dateCreated >= $now ) {
              $messageType = $announcementDetails[$cnt]->getDetails()."<br>";
              echo '
              <script>
                  putNoticeJavascript("Announcement !! ","'.$messageType.'");
              </script>';
              $_SESSION['messageType'] = 0;
            }

          }
          ?>

          <?php
          }
        }
        elseif($_GET['type'] == 2){

          $messageType = "Successfully Change Password.";
          echo '
          <script>
              putNoticeJavascript("Notice !! ","'.$messageType.'");
          </script>
          ';
          $_SESSION['messageType'] = 0;
        }
        $_SESSION['messageType'] = 0;
      }}
 ?>
</body>
<script type="text/javascript">
function showFrontLayer() {
  document.getElementById('bg_mask').style.visibility='visible';
  document.getElementById('frontlayer').style.visibility='visible';
}
function hideFrontLayer() {
  document.getElementById('bg_mask').style.visibility='hidden';
  document.getElementById('frontlayer').style.visibility='hidden';
}
</script>
<script type="text/javascript">
  $(document).ready( function(){
    $("#getAnnouncementTable").click( function(){
      $("#announcementTable").slideToggle();
    });
  });
</script>
<script>
  $(function(){
    $("#myTable").tablesorter( {dateFormat: 'pt'} );
  });
</script>
</html>
