$(document).ready(function(){
var a = 0;
$("#remBtn").hide();
  $("#addBtn").click( function(){
    a++;
    if (a == 1) {
      $(".labelSize1").fadeIn(500);
      $("#project_leader1").fadeIn(500);
      $("#project_leader1").removeAttr('disabled');
      $("#remBtn").show();
    }
    if (a == 2) {
      $(".labelSize2").fadeIn(500);
      $("#project_leader2").fadeIn(500);
      $("#project_leader2").removeAttr('disabled');
    }
    if (a == 3) {
      $(".labelSize3").fadeIn(500);
      $("#project_leader3").fadeIn(500);
      $("#project_leader3").removeAttr('disabled');
    }
    if (a == 4) {
      $(".labelSize4").fadeIn(500);
      $("#project_leader4").fadeIn(500);
      $("#project_leader4").removeAttr('disabled');
    }
    if (a > 3) {
      $("#addBtn").hide();
      $("#remBtn").show();
    }

  });
  // var b = 0;
  $("#remBtn").click( function(){
    a--;
    if (a < 0) {
      a = 1;
    }
    if (a == 0) {
      $(".labelSize1").fadeOut(500);
      $("#project_leader1").fadeOut(500);
      $("#project_leader1").prop('disabled',true);
      $("#addBtn").show();
      $("#remBtn").hide();
    }
    if (a == 1) {
      $(".labelSize2").fadeOut(500);
      $("#project_leader2").fadeOut(500);
      $("#project_leader2").prop('disabled',true);
    }
    if (a == 2) {
      $(".labelSize3").fadeOut(500);
      $("#project_leader3").fadeOut(500);
      $("#project_leader3").prop('disabled',true);
    }
    if (a == 3) {
      $(".labelSize4").fadeOut(500);
      $("#project_leader4").fadeOut(500);
      $("#project_leader4").prop('disabled',true);
      $("#addBtn").show();
    }

  });

  $("#buttonUpdate").mouseenter(function(){
    $(this).fadeIn(function(){
      $(this).css("color","red");
    });
  });
  $("#buttonUpdate").mouseleave(function(){
    $(this).delay(150).fadeIn(function(){
      $(this).css("color","blue");
    });
  });

  $("button[name='loan_uid_archive']").click(function(){
    var id = $(this).val();

    $.ajax({
      url : 'utilities/archiveProject.php',
      type : 'post',
      data : {id:id},
      dataType : 'json',
      success:function(response){

        var project = response[0]['projectName'];
        var display = response[0]['display'];
        if (display == 'Yes') {
          putNoticeJavascriptReload("Notice !!",""+project+" Has Been Restored");
        }else if(display == 'No'){
          putNoticeJavascriptReload("Notice !!",""+project+" Has Been Added To Archived");
        }
        // $(".reload").load(location.href+" .reload>*","");
      }
    });
  });

  $("form").keypress(function(e) {
  //Enter key
  if (e.which == 13) {
    return false;
  }
});

  });
