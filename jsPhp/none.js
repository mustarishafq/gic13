$(document).ready(function(){ //load document until finish first
  $("#checkBoxPayee").click(function(){ // click what happen next
    $("#payeeDetails").fadeToggle(); //toggle show and hide
    $("#payeeDetails input,select").removeAttr('disabled'); //remove disable
    $("#payeeDetails input").prop('required',true);
    $("#payeeDetails select").prop('required',true);
  });
});
$(document).ready(function(){
var a = 0;
$("#remBtn").hide();
  $("#addBtn").click( function(){
    a++;
    if (a == 1) {
      $("#input1").fadeIn(500);
      $("#remBtn").show();
    }
    if (a == 2) {
      $("#input2").fadeIn(500);
    }
    if (a == 3) {
      $("#input3").fadeIn(500);
    }
    if (a == 4) {
      $("#input4").fadeIn(500);
    }
    if (a == 5) {
      $("#input5").fadeIn(500);
    }
    if (a == 6) {
      $("#input6").fadeIn(500);
    }
    if (a == 7) {
      $("#input7").fadeIn(500);
    }
    if (a == 8) {
      $("#input8").fadeIn(500);
    }
    if (a == 9) {
      $("#input9").fadeIn(500);
    }
    if (a == 10) {
      $("#input10").fadeIn(500);
    }
    if (a == 11) {
      $("#input11").fadeIn(500);
    }
    if (a == 12) {
      $("#input12").fadeIn(500);
    }
    if (a == 13) {
      $("#input13").fadeIn(500);
    }
    if (a == 14) {
      $("#input14").fadeIn(500);
      $("#addBtn").hide();
      $("#remBtn").show();
    }

  });
  // var b = 0;
  $("#remBtn").click( function(){
    a--;
    if (a < 0) {
      a = 1;
    }
    if (a == 0) {
      $("#input1").fadeOut(500);
      $("#addBtn").show();
      $("#remBtn").hide();
    }
    if (a == 1) {
      $("#input2").fadeOut(500);
    }
    if (a == 2) {
      $("#input3").fadeOut(500);
    }
    if (a == 3) {
      $("#input4").fadeOut(500);
      // $("#addBtn2").show();
    }
    if (a == 4) {
      $("#input5").fadeOut(500);
    }
    if (a == 5) {
      $("#input6").fadeOut(500);
    }
    if (a == 6) {
      $("#input7").fadeOut(500);
    }
    if (a == 7) {
      $("#input8").fadeOut(500);
    }
    if (a == 8) {
      $("#input9").fadeOut(500);
    }
    if (a == 9) {
      $("#input10").fadeOut(500);
    }
    if (a == 10) {
      $("#input11").fadeOut(500);
    }
    if (a == 11) {
      $("#input12").fadeOut(500);
    }
    if (a == 12) {
      $("#input13").fadeOut(500);
    }
    if (a == 13) {
      $("#input14").fadeOut(500);
    }
    if (a == 14) {
      $("#input15").fadeOut(500);
    }

  });

  $("#product_name").change(function(){
      var productName = $(this).val();

      $("#unit").empty();
      $("#unit").append("<option value=''>No Unit Available</option>");
      for (var i = 1; i < 16; i++) {
        $("#unit"+i+"").empty();
        $("#unit"+i+"").append("<option value=''>No Unit Available</option>");
      }
      $("#invoiceTo").empty();
      $("#invoiceTo").text("Invoice : ");

      $(".yes, .no").prop("checked", false);
      $(".yes, .no").prop("disabled", false);
      $(".no-a").css("color", "black");
      $(".yes-a").css("color", "black");

      $.ajax({
          url: 'getLoanDetails.php',
          type: 'post',
          data: {projectName:productName},
          dataType: 'json',
          success:function(response){

              var len = response.length;
              $("#unit").empty(); // will remove data before this
              $("#unit").append("<option value=''>Select a unit</option>");

              for (var b = 1; b < 16; b++) {
                $("#unit"+b+"").empty(); // will remove data before this
                $("#unit"+b+"").append("<option value=''>Select a unit</option>");
              }

              // $("#unit").empty(); // will remove data before this
              for( var i = 0; i<len; i++){
                  var unit = response[i]['unit_no'];
                  var charges = response[i]['charges'];

                  $("#unit").append("<option value='"+unit+"'>"+unit+"</option>");
                  for (var k = 1; k < 16; k++) {
                    $("#unit"+k+"").append("<option value='"+unit+"'>"+unit+"</option>");
                  }

              }

              if (charges == 'Yes') {
                $(".yes").prop("checked", true);
                $(".no").prop("disabled", true);
                $(".no-a").css("color", "#b8c8b6");
              }else
              if (charges == 'No') {
                $(".no").prop("checked", true);
                $(".yes").prop("disabled", true);
                $(".yes-a").css("color", "#b8c8b6");
              }

              $("#claimsNo").empty();
                  var claims = response[0]['project_claims'];

                  // $("#claimsNo").append("<option value='"+claims+"'>"+claims+"</option>");
                  $("#claimsNo").val(claims);

                  $("#invoiceTo").empty();

                    var invoice = response[0]['add_projectppl'];

                      // $("#claimsNo").append("<option value='"+claims+"'>"+claims+"</option>");
                      $("#invoiceTo").text("Invoice : "+invoice);
                        $("#toWho").val(invoice);
          },
          error:function(response){

                  $("#unit").empty();
                  $("#unit").append("<option value=''>No Unit Available</option>");
                  for (var a = 1; a < 16; a++) {
                    $("#unit"+a+"").empty();
                    $("#unit"+a+"").append("<option value=''>No Unit Available</option>");
                  }

              // }
              $("#invoiceTo").empty();
              $("#invoiceTo").text("Invoice : ");
            }
      });
  });

});
