$(document).ready(function(){
  var loanUid = $("#loanUid").val();

  $.ajax({
    url: 'utilities/sstDetailsFunction.php',
    data: {loanUid:loanUid},
    type:'post',
    dataType: 'json',
    success:function(response){
      var charges = response[0]['charges'];
      // alert(charges);

      if (charges == 'Yes') {
        $(".yes").prop("checked", true);
        $(".no").prop("disabled", true);
        $(".no-a").css("color", "#b8c8b6");
      }else
      if (charges == 'No') {
        $(".no").prop("checked", true);
        $(".yes").prop("disabled", true);
        $(".yes-a").css("color", "#b8c8b6");
      }
    }
  });
});
