$(document).ready(function(){
  // $("#rate, #lister_override, #admin1_override, #admin2_override, #admin3_override, #totaldevelopercommper,#nettprice").on("keyup",function(){
    $("#rate, #lister_override, #totaldevelopercommper,#nettprice").on("keyup",function(){
    var rate = $("#rate").val();
    var listerOverride = $("#lister_override").val();
    // var admin1Override = $("#admin1_override").val();
    // var admin2Override = $("#admin2_override").val();
    // var admin3Override = $("#admin3_override").val();
    var dev = $("#totaldevelopercommper").val();
    var nettPrice = $("#nettprice").val();
    var percentDev = dev / 100;
    var percentRate = rate / 100;
    var agentComm = nettPrice * percentRate;
    var devComm = nettPrice * percentDev;
    var ulOverride = agentComm * 0.05;
    var u2lOverride = agentComm * 0.05;
    var u3lOverride = agentComm * 0.03;
    var u4lOverride = agentComm * 0.01;
    var u5lOverride = agentComm * 0.01;
    var plOverride = agentComm * 0.15;
    var hosOverride = agentComm * 0.05;
    // var totalOverride = +agentComm + +ulOverride + +u2lOverride + +u3lOverride + +u4lOverride + +u5lOverride + +plOverride + +hosOverride
    //                     + +listerOverride + +admin1Override + +admin2Override + +admin3Override;
    var totalOverride = +agentComm + +ulOverride + +u2lOverride + +u3lOverride + +u4lOverride + +u5lOverride + +plOverride + +hosOverride
                        + +listerOverride;

    var gicProfit = devComm - totalOverride;
    $("#agent_comm").val(agentComm);
    $("#ul_override").val(ulOverride);
    $("#u2l_override").val(u2lOverride);
    $("#u3l_override").val(u3lOverride);
    $("#u4l_override").val(u4lOverride);
    $("#u5l_override").val(u5lOverride);
    $("#pl_override").val(plOverride);
    $("#hos_override").val(hosOverride);
    $("#totaldevelopercomm").val(devComm);
    $("#gic_profit").val(gicProfit);
  });
  $("#totaldevelopercomm,#nettprice").on("keyup",function(){
    var devAmount = $("#totaldevelopercomm").val();
    var nettPrice = $("#nettprice").val();
    var devInPercent = devAmount / nettPrice * 100;
    if (!devInPercent) {
      devInPercent = 0;
    }
    $("#totaldevelopercommper").val(devInPercent);
  });
});
