<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess2.php';

require_once dirname(__FILE__) . '/classes/BankName.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$branchType = $_SESSION['branch_type'];

$bankDetails = getBankName($conn);
$userDetails = getUser($conn, "WHERE user_type = 3");
// $projectList = getProject($conn, "WHERE display = 'Yes' AND branch_type =?",array("branch_type"),array($branchType), "s");
$projectList = getProject($conn, "WHERE display = 'Yes'");
if (isset($_POST['product_name'])) {
$loanDetails = getLoanStatus($conn, "WHERE project_name = ? AND branch_type =?", array("project_name,branch_type"), array($_POST['product_name'],$branchType), "ss");
}
// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Other Commission | GIC" />
    <title>Other Commission | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
a{
  color: red;
}
input#checkBoxPayee{
  vertical-align: middle;
  width: 18px;
  height: 18px;
  outline: 2px solid maroon;
  outline-offset: -2px;
}

</style>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php  include 'admin2Header.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
   <h1 id="invoiceTo" class="h1-title h1-before-border shipping-h1 username"  onclick="goBack()">
    	<a  class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back3.png" class="back-btn2 hover1b" alt="back" title="back">
        	Other Commission:
        </a>
    </h1>

    <div class="tempo-two-input-clear"></div>

  <div id="otherType">
      <div class="three-input-div dual-input-div">
      <p>Project <a>*</a> </p>
      <form id="formOtherDisable" action="utilities/issuePayrollFunction.php" method="POST">
        <select class="dual-input clean" placeholder="Project" id="product_name2" name="project_name">
          <option value="">Select a Project</option>
          <?php for ($cntPro=0; $cntPro <count($projectList) ; $cntPro++)
          {
          ?>
          <option value="<?php echo $projectList[$cntPro]->getProjectName(); ?>">
          <?php echo $projectList[$cntPro]->getProjectName(); ?>
          </option>
          <?php
          }
          ?>
        </select>
      <!-- </form> -->

      </div>

    <div class="three-input-div dual-input-div second-three-input">
      <p>To</p>
      <select class="dual-input clean" name="toWho" required>
        <option value="">Select an agent</option>
        <?php if ($userDetails) {
          foreach ($userDetails as $userDetails) {
            ?><option value="<?php echo $userDetails->getUsername() ?>"><?php echo $userDetails->getUsername() ?></option> <?php
          }
        } ?>
      </select>
      <!-- <input   id="toWho2"  type="text" placeholder="To"> -->
    </div>

    <div class="three-input-div dual-input-div">
      <p>Date</p>
      <input class="dual-input clean" type="date" id="date_of_claims" name="date_of_claims" value="<?php echo date('Y-m-d') ?>" required>
    </div>

      <!-- <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div"> -->

      <div class="tempo-two-input-clear"></div>

      <!-- <div class="dual-input-div second-dual-input"> -->


      <!-- <div class="tempo-two-input-clear"></div>
      <div class="dual-input-div"> -->
      <!-- <div class="three-input-div dual-input-div second-three-input">
        <p>Invoice <a>*</a></p>
        <select class="dual-input clean" name="invoice_name" >
          <option value="">Please Select an Option</option>
          <option value="Proforma">Proforma</option>
          <option value="Invoice">Invoice</option>
        </select>
      </div> -->
      <!-- <div class="dual-input-div second-dual-input"> -->


      <div class="tempo-two-input-clear"></div>

      <div class="three-input-div dual-input-div">
        <p>Unit <a>*</a><img style="cursor: pointer" id="remBtn2" width="13px" align="right" src="img/mmm.png"><img style="cursor: pointer" id="addBtn2" width="13px" align="right" src="img/ppp.png"></p>
        <!-- <form class="" action="" method="post"> -->
          <select class="dual-input clean" id="unit02" name="unit[]" >
            <option value="">Select an Unit</option>
          </select>
    </div>
    <div class="three-input-div second-three-input dual-input-div">
      <p>Amount (RM) <a>*</a></p>
      <input class="dual-input clean" type="number" step="any" id="amountt" placeholder="Amount (RM)" value="" name="amount[]">
    </div>
    <div class="three-input-div dual-input-div">
      <p>Details <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <input class="dual-input clean" type="text" id="detailss" placeholder="Details" value="" name="details[]">
  </div>
    <div class="tempo-two-input-clear"></div>

    <div id="input12" style="display: none" >
      <div class="three-input-div dual-input-div">
        <p>Unit <a>*</a></p>
        <!-- <form class="" action="" method="post"> -->
          <select class="dual-input clean" id="unit12" name="unit[]" >
            <option value="">Select an Unit</option>
          </select>
    </div>
    <div class="three-input-div second-three-input dual-input-div">
      <p>Amount (RM) <a>*</a></p>
      <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount12" value="" name="amount[]">
    </div>
    <div class="three-input-div dual-input-div">
      <p>Details <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
  </div>
    <div class="tempo-two-input-clear"></div>
    </div>

  <div id="input22" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit22" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
    </div>
    <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount22" value="" name="amount[]">
    </div>
    <div class="three-input-div dual-input-div">
      <p>Details <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
  </div>
    <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input32" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit32" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount32" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>

  <div id="input42" style="display: none">
    <div class="three-input-div dual-input-div">
      <p>Unit <a>*</a></p>
      <!-- <form class="" action="" method="post"> -->
        <select class="dual-input clean" id="unit42" name="unit[]" >
          <option value="">Select an Unit</option>
        </select>
  </div>
  <div class="three-input-div second-three-input dual-input-div">
    <p>Amount (RM) <a>*</a></p>
    <input class="dual-input clean" type="number" step="any" placeholder="Amount (RM)" id="amount42" value="" name="amount[]">
  </div>
  <div class="three-input-div dual-input-div">
    <p>Details <a>*</a></p>
    <!-- <form class="" action="" method="post"> -->
      <input class="dual-input clean" type="text" placeholder="Details" value="" name="details[]">
</div>
  <div class="tempo-two-input-clear"></div>
  </div>
    <!-- <p id="addIn"></p> -->
    <div class="tempo-two-input-clear"></div>

    <div class="three-input-div dual-input-div">
      <p>Change Payee Details <input id="checkBoxPayee2" type="checkbox" name="" value=""></p>
          <!-- <form class="" action="" method="post"> -->
  </div>
  <div class="tempo-two-input-clear"></div>
  <div style="display: none" id = "payeeDetails2">
      <div class="three-input-div dual-input-div">
        <p>Bank Name Account Holder</p>
        <input  disabled class="dual-input clean" type="text" name="bank_account_holder" placeholder="Bank Name Account Holder">
      </div>
        <div class="three-input-div second-three-input dual-input-div">
          <p>Bank Name</p>
          <select  disabled class="dual-input clean" name="bank_name">
            <option class="dual-input clean" value="">Select a option</option>
            <?php if ($bankDetails) {
              foreach ($bankDetails as $bankName) {
                ?><option class="dual-input clean" value="<?php echo $bankName->getBankName() ?>"><?php echo $bankName->getBankName() ?></option> <?php
              }
            } ?>
          </select>
        </div>
          <div class="three-input-div dual-input-div">
            <p>Bank Account No.</p>
            <input  disabled class="dual-input clean" type="number" name="bank_account_no" placeholder="Bank Account No." value="">
          </div>
  </div>

      <div class="dual-input-div">
        <p>Include Service Tax (6%) <a>*</a></p>
        <input style="cursor: pointer" required class="" type="radio" value = "YES" name="charges" >Yes
        <input style="cursor: pointer" required class="" type="radio" value = "NO" name="charges" >No
      </div>
      <div class="tempo-two-input-clear"></div>

	  <div class="width100 text-center overflow">
      	<button id="buttonSubmit2" input type="submit" name="upload" value="Upload" class="ow-margin-auto confirm-btn text-center white-text clean black-button">Confirm</button>
      </div>
    </form>
    <form id="getInputVal2" action="otherInvoice.php" method="post">
      <!-- <button style="display: none;background-color: grey" id="buttonPreview2" type="submit" name="preview" value="Upload" class="confirm-btn text-center white-text clean black-button">Preview</button> -->
    </form>

    </div>



</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
<?php echo '<script src="jsPhp/issuePayroll.js"></script>'; ?>
<script>
function goBack() {
  window.history.back();
}
</script>
</body>
</html>
