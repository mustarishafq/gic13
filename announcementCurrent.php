<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Project.php';
require_once dirname(__FILE__) . '/classes/LoanStatus.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
$branchType = $_SESSION['branch_type'];

$projectDetails = getProject($conn);

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dcksupreme.asia/" />-->
    <meta property="og:title" content="Announcement | GIC" />
    <title>Announcement | GIC</title>
    <!--<link rel="canonical" href="https://dcksupreme.asia/" />-->
    <?php include 'css.php'; ?>
</head>
<style media="screen">
#alert{
  font-size: 13px;
}
  input:focus, textarea:focus, select:focus{
    outline: none;
  }
  #addNewProjectBtn{
    color: black;
  }
  #changePasswordCheck{
    width :15px;
    height :15px;
    vertical-align: middle;
    outline: 2px solid maroon;
    outline-offset: -2px;
  }
  .dual-input-div .maroon-text:hover{
    color: maroon;
    font-weight: bold;
    transition: 0.1s;
    /* transition-delay: 0.1s; */
    transform: scale(1.01);
    /* font-size: 17px; */
  }
  input{
    color: maroon;
    font-weight: bold;
  }
  .dual-input-div .maroon-text,.dual-input-div .label{
    font-size: 16px;
  }
  /*
  .dual-input-div .maroon-text{
    color: maroon;
    font-weight: bold;
    animation-duration: 4s;
    animation-name: example;
  }
  .positionStyle{
    color: maroon;
    font-weight: bold;
    animation-name: example;
    animation-duration: 4s;
  }
  .positionStyle:hover{
    color: black;
  }
  button{
    transition-duration: 0.1s;
  }
  button:hover{
    width: 155px;
    height: 47px;
  }
  .short-red-border{
    animation-duration: 4s;
    animation-name: examples;
  }
  .support{
    font-size: 12px;
    font-weight: bold;
    color: red;
  }
  .support:hover{
    font-size: 15px;
    color: red;
  }
  @keyframes example {
    0%   {color: black;}
    100% {color: maroon;}
  }
  @keyframes examples {
    0%   {margin-left: 50px;}
    100% {left: 0;}
  }*/
</style>
<body class="body">
<?php if ($_SESSION['usertype_level'] %4 == 1) {
  include 'admin1Header.php';
}elseif ($_SESSION['usertype_level'] %4 == 2) {
  include 'admin2Header.php';
} ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Announcement | <a style="color: maroon" href="announcement.php">New Announcement</a> </h1>
    <div class="short-red-border"></div><br>
    <div class="clear"></div>
    <div class="section-divider width100 overflow">

    <?php
    $conn = connDB();
  ?>

    <div class="clear"></div>

      <div class="width100 shipping-div2">
          <?php $conn = connDB();?>
          <table id="myTable" class="shipping-table pointer-th">
              <thead>
                  <tr>
                      <th rowspan="2" class="th">NO. <img src="img/sort.png" class="sort"></th>
                      <th colspan="3" class="th"><?php echo wordwrap("PROJECT",7,"</br>\n");?></th>
                      <th rowspan="2" class="th">PROJECT PACKAGES <img src="img/sort.png" class="sort"></th>
                      <th rowspan="2" class="th"><?php echo wordwrap("DATE",10,"</br>\n");?>  <img src="img/sort.png" class="sort"></th>
                      <th rowspan="2" class="th">PRESENTATION FILE <img src="img/sort.png" class="sort"></th>
                      <th rowspan="2" class="th">E-BROSHURE <img src="img/sort.png" class="sort"></th>
                      <th rowspan="2" class="th">BOOKING FORM <img src="img/sort.png" class="sort"></th>
                      <th rowspan="2" class="th">VIDEO LINK <img src="img/sort.png" class="sort"></th>
                      <th rowspan="2" class="th">DATE MODIFIED <img src="img/sort.png" class="sort"></th>
                      <th rowspan="2" class="th">ACTION <img src="img/sort.png" class="sort"></th>

                  </tr>
                  <tr>
                    <th>Project Name</th>
                    <th>PL Name</th>
                    <th>Commission (RM)</th>
                  </tr>
              </thead>
              <tbody>
                  <?php
                  // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                  // {
                      $announcement = getAnnouncement($conn, "WHERE branch_type =?",array("branch_type"),array($branchType), "s");
                      if($announcement != null)
                      {
                          for($cntAA = 0;$cntAA < count($announcement) ;$cntAA++)
                          {?>
                          <tr>
                              <td class="td"><?php echo ($cntAA+1)?></td>
                              <td class="td"><?php echo wordwrap($announcement[$cntAA]->getProjectName(),20,"<br>")?></td>
                              <td class="td"><?php echo str_replace(",","<br>",$announcement[$cntAA]->getPlName());?></td>
                              <td class="td"><?php echo number_format($announcement[$cntAA]->getCommission(),2);?></td>
                              <td class="td"><?php echo $announcement[$cntAA]->getPackage();?></td>
                              <td class="td"><?php echo date('d/m/Y',strtotime($announcement[$cntAA]->getDateCreated()));?></td>
                              <td class="td hover"><?php
                              if ($announcement[$cntAA]->getPresentFile()) {
                                ?><a href="<?php echo "announcement/".$announcement[$cntAA]->getPresentFile() ?>"><button class="view-download-btn" type="button" name="button">View/Download</button></a> <?php
                              } ?>
                              </td>
                              <td class="td hover"><?php
                              if ($announcement[$cntAA]->getEBroshure()) {
                                ?><a href="<?php echo "announcement/".$announcement[$cntAA]->getEBroshure() ?>"><button class="view-download-btn" type="button" name="button">View/Download</button></a> <?php
                              } ?>
                              </td>
                              <td class="td hover"><?php
                              if ($announcement[$cntAA]->getBookingForm()) {
                                ?><a href="<?php echo "announcement/".$announcement[$cntAA]->getBookingForm() ?>"><button class="view-download-btn" type="button" name="button">View/Download</button></a> <?php
                              } ?>
                              </td>
                              <td class="td hover"><?php
                              if ($announcement[$cntAA]->getVideoLink()) {
                                ?><a><?php echo $announcement[$cntAA]->getVideoLink() ?></a> <?php
                              } ?>
                              </td>
                              <td class="td"><?php echo date('d/m/Y',strtotime($announcement[$cntAA]->getDateUpdated()));?></td>
                              <td class="td">
                                <form class="" action="announcementEdit.php" method="post">
                                  <button class="edit-btn" type="submit" value="<?php echo $announcement[$cntAA]->getId() ?>" name="edit">Edit</button>
                                </form>
                              </td>
                          </tr>
                          <?php
                          }
                      }else {
                        ?><td style="text-align: center;font-style: normal;font-weight: bold;font-size: 15px;" colspan="11">No Announcement.</td> <?php
                      }
                  //}
                  ?>
              </tbody>
          </table><br>
      </div>

      <?php //$conn->close();?>
  </div>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Created an Announcement. ";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Updated an Announcement. ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully Updated The Project Details";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "New Project Created Successfully !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>';
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>
<script>
  $(function(){
    $("#project_name").change(function(){
      var project = $(this).val();

      $.ajax({
        url : 'getPLName.php',
        type : 'post',
        data : {projectName:project},
        dataType : 'json',
        success:function(response){

          var totalPL = response[0]['totalPLName'];
          $("#projectLeader").val(totalPL);
        }
      });
    });
  });
</script>
